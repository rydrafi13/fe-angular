import {Component, Injectable} from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url, getTokenHeader } from '../../../environments/environment';
import { MasterService } from '../master.service';

interface Packagebundle {
    error: any;
    result: any;
}

@Injectable({
    providedIn: 'root'
})



export class PackagebundleService {
    courses$: Observable<Packagebundle[]>;
    api_url = '';

  constructor(private http: HttpClient, public myService: MasterService) {
      this.api_url = api_url();
  }

  public async getPromoLint() {
      let result;
      const customHeaders = getTokenHeader();
    try {
      const url = 'promo/report';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchPackagebundleLint(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'promo/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

   public async detailPackagebundle(Packagebundle_id) {
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'promo/detail/' + Packagebundle_id;
      // const url = 'promotions/report?request={"search": {"_id":"' + promotion_id + '"}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getPackagebundleLint(params) {
    
    let result;
    const customHeaders = getTokenHeader();
    try {
      // const url   = 'members/all';
      const url = 'promo/report';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async getDropdown() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'products/all';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async deletePackagebundle(params) {
    let result;
    const customHeaders = getTokenHeader();
    console.log(params);
    try {
      const url = 'promo/remove/' + params._id;
      result = await this.myService.delete(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async addPromo(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'promo/add';
      result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }


public async promoUpdate(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'promo/update/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }


  public async getCategoryLint(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'category/all';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

}
