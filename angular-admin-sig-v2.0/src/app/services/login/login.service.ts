import { Injectable,Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import { getAPIKeyHeader, api_url } from '../../../environments/environment';

interface Login {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})



export class LoginService {

  public gerro: string = "test";
  constructor(public myService:MasterService) { 
  }

  
  // public async getMembersLint(){
   
  //   const url   = 'members/all';
  //   let result  = await this.myService.get(null, url);
   
  //   return  result;
  // }

  // public async login(params){
  //   let result;

  //   const customHeaders = getAPIKeyHeader()

  //   try{
  //     const url     = api_url()+'member/admin_login';
  //           result  = await this.myService.post(params, url, customHeaders);
      
  //   } catch (error) {
  //     throw new TypeError(error);
  //   }
  //   return  result;
    
  // }

  public async login(params){
    let result;

    // const customHeaders = getAPIKeyHeader()

    try{
      const url     = api_url()+'auth/token/admin';
      result  = await this.myService.post(params, url, null);
      
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
    
  }

  public async memberLogin(params){
    let result;

    const customHeaders = getAPIKeyHeader()

    try{
      const url     = api_url()+'member/login';
            result  = await this.myService.post(params, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
    
  }

  public async guestLogin(params){
    let result;

    const customHeaders = getAPIKeyHeader()

    try{
      const url     = api_url()+'member/guest_login';
            result  = await this.myService.post(params, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
    
  }

  
}
