import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { api_url, getTokenHeader, getTokenHeaderRetailer, getAPIKeyHeader } from '../../../environments/environment';
import Swal from 'sweetalert2';
import { typeofExpr } from '@angular/compiler/src/output/output_ast';

interface Member {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',

})


export class MemberService {

  public gerro: string = 'test';
  public api_url: string = '';
  public httpReq: any = false;

  constructor(private http: HttpClient, public myService: MasterService) {
    this.api_url = api_url();
  }

  public async getMemberSummaryByDate(curDate) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'members/report/summary';
      result = await this.myService.get(null, url, customHeaders);

    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }



  public async setNewPin(params){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/pin/set';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async changePin(){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/pin/change';
      result = await this.myService.post(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async resetPin(){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/pin/reset';
      result = await this.myService.post(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getAllMembersLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'members/report?request={"search": {},"limit_per_page":50,"current_page":1}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result)

    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async getMembersLint(params) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'members/report';
      result = await this.myService.get(params, url, customHeaders);
      console.log("zz", result)

    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async getMemberLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'members/report?request={"search": {"type":"member"},"limit_per_page":50,"current_page":1}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getMemberAllReport() {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'member/all/report?request={"search": {"show_password":1},"limit_per_page":50,"current_page":1}';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchMemberAllReport(params) {
    const customHeaders = getTokenHeaderRetailer();
    let result;
    try {
      const url = 'member/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getAutoformMember(current_page) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'member/all/report?request={"search": {},"limit_per_page":50,"current_page":' + current_page + '}'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getFormMember() {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'member/find'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchFormMember(params) {
    const customHeaders = getTokenHeaderRetailer();
    let result;
    try {
      const url = 'member/find';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getFormMemberByID(id_pel) {
    // console.log(id_pel);
    const customHeaders = getTokenHeaderRetailer();
    let result;
    try {
      const url = 'member/find?request={"search": {"username":"'+String(id_pel)+'"},"limit_per_page":1,"current_page":1, "order_by": {"updated_date":-1}}';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getMemberReportByID(id_pel) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'member/all/report?request={"search": {"username":"'+String(id_pel)+'"},"limit_per_page":1,"current_page":1, "order_by": {"updated_date":-1}}';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getGuestLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'members/report?request={"search": {"type":"guest"}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async addNewMember(params) {
    // console.log(params);
    let result;
    // const customHeaders = getAPIKeyHeader();
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'member/register';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async searchRegion(payload) {
     let result;
     const customHeaders = getTokenHeaderRetailer();
     try {
       const url = 'regions/search';
       result = await this.myService.post(payload, url, customHeaders);
     } catch (error) {
       throw new TypeError(error);
     }
 
     return result;
  }

  public async loginRedeemMember(params) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'auth/token';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async getTokenMember(params) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'auth/token/member';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async addNewMerchantOutlet(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/newoutlet';
      result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

   public async detailMerchant(){
    let result;

    const customHeaders = getTokenHeader()
    try {
      const url   = 'merchant/detail' 
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {   
      throw new TypeError(error);
    }
    return  result;
  }

  public async updateMember(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'members/all';
      result = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchGuestLint(params) {
    params.search.type="guest";
    console.log(params);
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'members/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  public async searchAllMemberLint(params) {
    // params.search.type="member";
    console.log(params);
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'members/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  public async searchMemberLint(params) {
    params.search.type="member";
    console.log(params);
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'members/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchOutletsLint(params) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'members/all';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getMemberDetail() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/detail';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async getMemberDemographyReport(type = 'age') {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'members/report/summary/demography/'+type;
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async detailMember(id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/detail/' + id;
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async getRegion(match,  usage){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'shippingregion/searchregion';
      result = await this.myService.get({match:match , usage:usage}, url, customHeaders);
      // console.log("REGION",result  );
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getProvince(match,  usage){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'region/province_list';
      result = await this.myService.get({province:match , usage:usage}, url, customHeaders);
      // console.log("REGION",result  );
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getCity(match, usage, province_code){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'region/city_list';
      result = await this.myService.get({match:match , usage:usage, province_code:province_code}, url, customHeaders);
      // console.log("REGION",result  );
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getSubDistrict(match,  usage, city_code){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'region/sub_district_list';
      result = await this.myService.get({match:match , usage:usage, city_code:city_code}, url, customHeaders);
      // console.log("REGION",result  );
    } catch (error) {
      throw new TypeError(error);
    } 
    return result;
  }

  public async getVillage(match,  usage, subdistrict_code){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'region/village_list';
      result = await this.myService.get({match:match , usage:usage, subdistrict_code:subdistrict_code}, url, customHeaders);
      // console.log("REGION",result  );
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }


  public async detailOutlet(params) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/outlet';
      result = await this.myService.add(params, url, customHeaders);
      //console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async updateMemberID(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url = 'members/update/' + params.member_id;
      result = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }

  public async updateMemberAddress(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url = 'admin/update_address?method=single';
      result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }

  public async updateSingleMemberAutoform(data){
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url = 'autoform/update?form=ps-form-1&method=single';
      result = await this.myService.add(data, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }

  public async updateSingleFormMember(data){
    let result: any;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'member/update';
      result = await this.myService.update(data, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }

  public async registerBulkMember(file, obj,payload){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('type',payload.type)
    fd.append('document', file)
    let url: string = api_url()+'member/register/bulk';
    console.log('now',fd);
    let result;
    
    this.httpReq = this.http.post(url,fd,{
      headers: getTokenHeaderRetailer(),
      // headers : new HttpHeaders({
      //   'Authorization': myToken,
      //   'app-label': 'retailer_prg'
      // }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      console.log("event",event);
      console.log(event);
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
        console.log('this is body');
        //obj.firstLoad();
          console.warn("c body", c.body)
          // obj.prodOnUpload = true;
          if (c.status == 200) {
            obj.selectedFile = null;
            obj.startUploading = false;

            obj.Report=c.body.url;
            Swal.fire({
              title: 'Bulk Upload Success',
              text: 'Anda akan mengunduh file csv untuk username dan password member : ' + obj.Report,
              icon: 'success',
              confirmButtonText: 'Ok',
            }).then((result) => {
              if (obj.Report) {
                window.open(obj.Report, "_blank")
              }
            });
          }
          //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
          // obj.process_id = c.body;
          console.log("process_id",obj.Report);
          // console.log(c.body.failed);
          // obj.failed = c.body.failed.length;
         
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
          // Swal.fire(
          //   'Success!',
          //   'Your file has been uploaded.',
          //   'success'
          // )
          // window.open(c.body.url, "_blank");
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      //console.log(result);
      if(result.error) { 
          obj.errorFile = result.error.error;
          obj.cancel    = true;
          obj.selectedFile = null;
          Swal.fire(
            'Failed!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }

  public async updateBulkMember(file, obj,payload){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('type',payload.type)
    fd.append('document', file)
    let url: string = api_url()+'member/update/bulk';
    console.log('now',fd);
    let result;
    
    this.httpReq = this.http.put(url,fd,{
      headers: getTokenHeaderRetailer(),
      // headers : new HttpHeaders({
      //   'Authorization': myToken,
      //   'app-label': 'retailer_prg'
      // }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      console.log("event",event);
      console.log(event);
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
        console.log('this is body');
        console.warn(" obj", obj)
        // obj.firstLoad();
          obj.Report=c.body;
          //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
          obj.process_id = c.body;
          // console.log("process_id",obj.Report);
          // obj.prodOnUpload = true;
          // console.log(c.body.result.failed);
          // obj.failed = c.body.result.failed.length;

          if (c.status == 200) {
            obj.selectedFile = null;
            obj.startUploading = false;
            Swal.fire(
              'Success!',
              'Your file has been uploaded.',
              'success'
            )
          }
         
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
          // Swal.fire(
          //   'Success!',
          //   'Your file has been uploaded.',
          //   'success'
          // )
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      //console.log(result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
          obj.selectedFile = null;
          Swal.fire(
            'Failed!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }

  public async updateProfile(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/update_profile'
      result = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }

  public async addBastSk(params:any){
    let result :any;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'member/additional_info/add';
      result = await this.myService.update(params, url, customHeaders);
    }catch (error){
      throw new TypeError(error);
    }
    return result;
  }

  public async deleteBastSk(params:any){
    let result :any;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'member/additional_info/remove';
      result = await this.myService.update(params, url, customHeaders);
    }catch (error){
      throw new TypeError(error);
    }
    return result;
  }

  public async deleteMember(params) {
    let result
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/' + params._id;
      // console.log(params);
      result = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getDownloadFileLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/?report=csv';
      console.log(url);
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);

    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async setCompleteMember(payload) {
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'member/mark_as_complete';
      await this.myService.update(payload, url, customHeaders);
    } catch (error){
      throw new TypeError(error);
    }
  }

  public async getMerchantList() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'merchant_list';
      console.log(url);
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);

    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }


  public async Courier() {
    let result;
    // console.log(result);
    const customHeaders = getTokenHeader();

    try {
      const url = 'courier/list';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  // public async pagination() {
  //   let result;
  //   const customHeaders = getTokenHeader();
  //   method: GET
  //     "/ v1 / orderhistory / page /@page/@limit "

  //   params: "page" =>, "limit" => limit per page

  //   response: total record and order history data per page
  //   Co

  //   return result;
  // }
  public async resetPasswordRequest(params) {
    // console.log(params);
    let result;
    const customHeaders = getAPIKeyHeader();
    try {
      const url = 'member/password_request';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async resetPasswordCodeVerification(params) {
    // console.log(params);
    let result;
    const customHeaders = getAPIKeyHeader();
    try {
      const url = 'member/pwd_reset_auth';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }
  public async resetPasswordChangePassword(data) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = `member/change_password`;
      result = await this.myService.update(data, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }


  // Download csv file
  // dataExport(parameters) {

  //   const send = {
  //       'id': parameters.ids,
  //       'from': parameters.from,
  //       'to': parameters.to
  //   };

  //   const url = ``

  //   this.myService.post(url, send,
  // { headers: new HttpHeaders({ 'Authorization': `Bearer ${token}`, 'Content-Type': 'application/json'}) })
  //       .do(() => { this.success('success'); })
  //       .catch((error) => {
  //           // this.error(error);
  //           throw new TypeError(error);
  //           // return Observable.of(null);
  //       })
  //       .subscribe(r => {
  //           const blob = new Blob([r], {type: 'text/csv'});
  //           const url = window.URL.createObjectURL(blob);
  //           window.open(url);
  //       });
  // }
}
