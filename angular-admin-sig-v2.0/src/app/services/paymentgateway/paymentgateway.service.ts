import { Injectable,Component, OnInit } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';

import {api_url, getTokenHeader} from '../../../environments/environment';
import { MasterService } from '../master.service';

interface PaymentGateway {
  error:any,
  result:any
}

@Injectable({
  providedIn: 'root',
  
})

export class PaymentGatewayService {

  api_url:string ="";
  constructor(public myService:MasterService) { 
    this.api_url = api_url();
  }

  public async detailPaymentGateway(_id){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'paymentgateway/detail/'+_id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getPaymentGatewayLint(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'paymentgateway/findall';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async searchPaymentGatewayLint(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'paymentgateway/findall';
      result = await this.myService.searchResult(params, url, customHeaders);
      //result = await this.getPaymentGateway(params).toPromise();
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async addPaymentGatewayLint(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'paymentgateway/insert';
      result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async updatePaymentGateway(params){
    let url = 'paymentgateway/update/'+params.previous_id;
    const customHeaders = getTokenHeader();
    // console.log(params);
    let result = await this.myService.update(params, url, customHeaders);
    return  result;
  }

  public async deletePaymentGateway(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'paymentgateway/delete/'+params._id;
      // console.log(params);
      result = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getWithdrawalBankList(params={}){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'payment/withdrawal/available-bank-list';
      console.log(params);
      result = await this.myService.get(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }
}
