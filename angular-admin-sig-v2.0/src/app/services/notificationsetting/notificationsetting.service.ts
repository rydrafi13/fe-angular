import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import {getTokenHeader} from '../../../environments/environment';

interface Notification {
  error: any ,
  result: any
}

@Injectable({
  providedIn: 'root',

})



export class NotificationsettingService {

  public gerro:  string = 'test';
  constructor(public myService:MasterService) { 
  }

  public async getNotificationsettingLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notificationsetting/all';
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewNotificationsetting(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notificationsetting';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updateNotification(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/all';
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async detailNotificationsetting(_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notificationsetting/' + _id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async deleteNotification(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/all';
      result  = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  /* public async searchNotificationLint(params) {
    // console.log(params);
    let result;
    try {
      const url   = 'notifications/all';
      result  = await this.myService.searchResult(params, url);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  } */
  
  public async updateNotificationsettingID(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notificationsetting/' +params._id;
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      // console.log(result);
    return  result;
  }

  public async deleteNotificationSetting(params) {
    let url, result: any;
    const customHeaders = getTokenHeader();
    try {
    url = 'notificationsetting/' + params._id;
    // console.log(params);
    result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
}
