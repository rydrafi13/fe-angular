import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import { getAPIKeyHeader, api_url } from '../../../environments/environment';

interface Login {
    error: any,
    result: any
}

@Injectable({
    providedIn: 'root',

})



export class SwapStorageService {

    keyID = 'swap-merchant-storage';

    constructor(public myService: MasterService) {
    }

    get(){
        return localStorage.getItem(this.keyID)
    }

    set(value){
        return localStorage.setItem(this.keyID, value)

    }

}
