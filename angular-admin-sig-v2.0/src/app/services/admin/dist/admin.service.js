"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.MemberService = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("../../../environments/environment");
var MemberService = /** @class */ (function () {
    function MemberService(http, myService) {
        this.http = http;
        this.myService = myService;
        this.gerro = 'test';
        this.api_url = '';
        this.api_url = environment_1.api_url();
    }
    MemberService.prototype.getMemberSummaryByDate = function (curDate) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report/summary';
                        return [4 /*yield*/, this.myService.get({ request: JSON.stringify({ date: curDate }) }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.setNewPin = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/pin/set';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.changePin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/pin/change';
                        return [4 /*yield*/, this.myService.post(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.resetPin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/pin/reset';
                        return [4 /*yield*/, this.myService.post(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getAllMembersLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMembersLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report';
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log("zz", result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMemberLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report?request={"search": {"type":"member"},"limit_per_page":50,"current_page":1}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getGuestLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report?request={"search": {"type":"guest"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        throw new TypeError(error_8);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.addNewMember = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getAPIKeyHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/register';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_9 = _a.sent();
                        throw new TypeError(error_9);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.addNewMerchantOutlet = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/newoutlet';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_10 = _a.sent();
                        throw new TypeError(error_10);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.detailMerchant = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/detail/';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_11 = _a.sent();
                        throw new TypeError(error_11);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.updateMember = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/all';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_12 = _a.sent();
                        throw new TypeError(error_12);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.searchGuestLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.type = "guest";
                        console.log(params);
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_13 = _a.sent();
                        throw new TypeError(error_13);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.searchMemberLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.type = "member";
                        console.log(params);
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_14 = _a.sent();
                        throw new TypeError(error_14);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.searchOutletsLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/all';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_15 = _a.sent();
                        throw new TypeError(error_15);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMemberDetail = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_16;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/detail';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_16 = _a.sent();
                        throw new TypeError(error_16);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMemberDemographyReport = function (type) {
        if (type === void 0) { type = 'age'; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_17;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report/summary/demography/' + type;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_17 = _a.sent();
                        throw new TypeError(error_17);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.detailMember = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_18;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/detail/' + id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_18 = _a.sent();
                        throw new TypeError(error_18);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getRegion = function (match, usage) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_19;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'shippingregion/searchregion';
                        return [4 /*yield*/, this.myService.get({ match: match, usage: usage }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_19 = _a.sent();
                        throw new TypeError(error_19);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getProvince = function (match, usage) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_20;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'region/province_list';
                        return [4 /*yield*/, this.myService.get({ province: match, usage: usage }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_20 = _a.sent();
                        throw new TypeError(error_20);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getCity = function (match, usage, province_code) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_21;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'region/city_list';
                        return [4 /*yield*/, this.myService.get({ match: match, usage: usage, province_code: province_code }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_21 = _a.sent();
                        throw new TypeError(error_21);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getSubDistrict = function (match, usage, city_code) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_22;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'region/sub_district_list';
                        return [4 /*yield*/, this.myService.get({ match: match, usage: usage, city_code: city_code }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_22 = _a.sent();
                        throw new TypeError(error_22);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getVillage = function (match, usage, subdistrict_code) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_23;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'region/village_list';
                        return [4 /*yield*/, this.myService.get({ match: match, usage: usage, subdistrict_code: subdistrict_code }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_23 = _a.sent();
                        throw new TypeError(error_23);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.detailOutlet = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_24;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/outlet';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_24 = _a.sent();
                        throw new TypeError(error_24);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.updateMemberID = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_25;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/update/' + params.member_id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_25 = _a.sent();
                        throw new TypeError(error_25);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.updateProfile = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_26;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/update_profile';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_26 = _a.sent();
                        throw new TypeError(error_26);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.deleteMember = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_27;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/' + params._id;
                        return [4 /*yield*/, this.myService["delete"](params, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_27 = _a.sent();
                        throw new TypeError(error_27);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getDownloadFileLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_28;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/?report=csv';
                        console.log(url);
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_28 = _a.sent();
                        throw new TypeError(error_28);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMerchantList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_29;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant_list';
                        console.log(url);
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_29 = _a.sent();
                        throw new TypeError(error_29);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.Courier = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_30;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'courier/list';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_30 = _a.sent();
                        throw new TypeError(error_30);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // public async pagination() {
    //   let result;
    //   const customHeaders = getTokenHeader();
    //   method: GET
    //     "/ v1 / orderhistory / page /@page/@limit "
    //   params: "page" =>, "limit" => limit per page
    //   response: total record and order history data per page
    //   Co
    //   return result;
    // }
    MemberService.prototype.resetPasswordRequest = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_31;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getAPIKeyHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/password_request';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_31 = _a.sent();
                        throw new TypeError(error_31);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.resetPasswordCodeVerification = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_32;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getAPIKeyHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/pwd_reset_auth';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_32 = _a.sent();
                        throw new TypeError(error_32);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.resetPasswordChangePassword = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_33;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/change_password';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_33 = _a.sent();
                        throw new TypeError(error_33);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], MemberService);
    return MemberService;
}());
exports.MemberService = MemberService;
