import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { api_url, getTokenHeader,getCustomTokenHeader, getAPIKeyHeader } from '../../../environments/environment';

interface Member {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',

})


export class AdminService {

  public gerro: string = 'test';
  public api_url: string = '';

  constructor(private http: HttpClient, public myService: MasterService) {
    this.api_url = api_url();
  }

  public async getAdmin() {
    let result;
    const customHeaders = getCustomTokenHeader('usr_mngt');
    try {
      const url = 'admin/find';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async updateAdmin(username,payload) {
    let result;
    const customHeaders = getCustomTokenHeader('usr_mngt');
    try {
      const url = 'admin/update/' + username;
      result = await this.myService.update(payload, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async addAdmin(payload) {
    console.log("payload", payload);
    let result;
    const customHeaders = getCustomTokenHeader('usr_mngt');
    try {
      const url = 'admin/add';
      result = await this.myService.post(payload, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async changePasswordAdmin(payload) {
    let result;
    const customHeaders = getCustomTokenHeader('usr_mngt');
    try {
      const url = 'admin/change_password';
      result = await this.myService.post(payload, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async getAdminGroup() {
    let result;
    const customHeaders = getCustomTokenHeader('usr_mngt');
    try {
      const url = 'admin-group/find';
      result = await this.myService.get(null, url, customHeaders);

    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async updateAdminGroup(username,payload) {
    let result;
    const customHeaders = getCustomTokenHeader('usr_mngt');
    try {
      const url = 'admin-group/update/' + username;
      result = await this.myService.update(payload, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async getDownloadFileLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'member/?report=csv';
      console.log(url);
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);

    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async searchAdmin(params) {
    // params.search.key="ps-form-1";
    // console.log(params);
    const customHeaders = getCustomTokenHeader('usr_mngt');
    let result;
    try {
      const url = 'admin/find';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchAdminGroup(params) {
    // params.search.key="ps-form-1";
    // console.log(params);
    const customHeaders = getCustomTokenHeader('usr_mngt');
    let result;
    try {
      const url = 'admin-group/find';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async addAdminGroup(payload) {
    console.log("payload", payload);
    let result;
    const customHeaders = getCustomTokenHeader('usr_mngt');
    try {
      const url = 'admin-group/add';
      result = await this.myService.post(payload, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async getAccessListApp() {
    let result;
    const customHeaders = getCustomTokenHeader('usr_mngt');
    try {
      const url = 'access/list_app';
      result = await this.myService.get(null, url, customHeaders);

    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }

  public async getAdminByID(_id) {
    let result;
    const customHeaders = getCustomTokenHeader('usr_mngt');
    try {
      const url = 'admin/find?request={"search": {"key":"ps-form-1","_id":"'+String(_id)+'"},"limit_per_page":1,"current_page":1, "order_by": {"updated_date":-1}}';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getAdminGroupByID(_id) {
    let result;
    const customHeaders = getCustomTokenHeader('usr_mngt');
    try {
      const url = 'admin-group/find?request={"search": {"key":"ps-form-1","_id":"'+String(_id)+'"},"limit_per_page":1,"current_page":1, "order_by": {"updated_date":-1}}';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }


}
