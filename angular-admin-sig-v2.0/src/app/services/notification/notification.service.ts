import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import {api_url, getTokenHeader} from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';

interface Notification {
  error: any ,
  result: any
}

@Injectable({
  providedIn: 'root',

})



export class NotificationService {

  api_url: string = '';
  public httpReq: any = false;
  public gerro:  string = 'test';
  constructor(public myService:MasterService, private http: HttpClient) 
  { 
    this.api_url = api_url();
  }

  public async getNotificationLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/all';
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async searchMemberIDLint(params) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications-group/all_members';
      result  = await this.myService.searchResult(params, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async getMemberIDLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications-group/all_members';
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updateTokenList(params, _id) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications-group/update_token/' + _id;
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      // console.log(result);
    return  result;
  }

  public async addNewNotification(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updateNotification(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/all';
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async detailNotification(_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/' + _id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async detailNotificationGroup(_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications-group/' + _id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async deleteNotification(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/all';
      result  = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  /* public async searchNotificationLint(params) {
    // console.log(params);
    let result;
    try {
      const url   = 'notifications/all';
      result  = await this.myService.searchResult(params, url);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  } */
  
  public async updateNotificationID(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/update/' +params.notification_id;
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      // console.log(result);
    return  result;
  }

  public async delete(params) {
    let url, result: any;
    const customHeaders = getTokenHeader();
    try {
    url = 'notifications/remove/' + params.notification_id;
    // console.log(params);
    result = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getNotificationGroupLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications-group/all';
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewNotificationGroup(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications-group/';
      console.log(url,params);
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  // public async detailNotificationGroup(_id) {
  //   let result;
  //   try {
  //     const url   = 'notifications-group/' + _id;
  //     result  = await this.myService.get(null, url);
  //   } catch (error) {
  //     throw new TypeError(error);
  //   }

  //   return  result;
  // }

  public async updateNotificationGroupID(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications-group/update/' +params._id;
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      // console.log(result);
    return  result;
  }

  public async getNotificationMessageLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications-message/all';
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewNotificationMessage(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications-message/';
      console.log(url,params);
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async upload(file, obj){
    const fd = new FormData();
    fd.append('image', file, file.name)
    let url: string = api_url()+'media/upload';
    console.log(url);
    let result;
    
    this.httpReq = this.http.post(url,fd,{
      reportProgress: true,
      observe: 'events'
    }).subscribe(event => {
      console.log(event);
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        console.log(obj.cancel);
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      console.log(result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
        }
    }
    );
  }
}
