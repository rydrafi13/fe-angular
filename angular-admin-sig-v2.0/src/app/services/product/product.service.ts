import { Injectable,Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { Observable , of} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { api_url, getTokenHeaderRetailer, getTokenHeader, environment} from '../../../environments/environment';
import { MasterService } from '../master.service';
import Swal from 'sweetalert2';
// import { type } from 'os';

interface Product {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})

export class ProductService {
  courses$: Observable<Product[]>;

  api_url: string = '';
  public httpReq: any = false;
  private serviceUrl = 'https://jsonplaceholder.typicode.com/users';
  constructor(private http: HttpClient, public myService:MasterService) { 
    this.api_url = api_url();
  }

  public async detailProducts(_id){
    let result;

    const customHeaders = getTokenHeaderRetailer();
    try {
      const url   = 'product/detail/' +_id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      console.log("product detail error", error);
      return error;
      // throw new TypeError(error);
    }

    return  result;
  }

  public async getProductsReport(_params?){
    let result;
    let params;

    if(_params){
      params = _params
    }else{
      params = {
        search : {
          type: {
            in:["product","voucher","gold","top-up"]
          }
          // active : '1'
        },
        limit_per_page : 50,
      }
    }
    /**
     * Params example : {type: 'product'}
     */
    const customHeaders = getTokenHeaderRetailer();;
    try{
      // const url = 'products/report?request={%22search%22:{%22type%22:%22product%22,%22get_detail%22:true},%22limit_per_page%22:10}';
      const url = 'product/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
      console.log("API", params,url)
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getEvoucherReport(_params?){
    let result;
    let params;

    if(_params){
      params = _params
    }else{
      params = {
        search : {
          type : 'e-voucher'
        },
        limit_per_page : 50,
      }
    }
    let getDetail = {
      get_detail: true,
    }
    Object.assign(params.search, getDetail)
    /**
     * Params example : {type: 'product'}
     */
    const customHeaders = getTokenHeaderRetailer();;
    try{
      // const url = 'products/report?request={%22search%22:{%22type%22:%22product%22,%22get_detail%22:true},%22limit_per_page%22:10}';
      const url = 'product/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getCategoryLint(){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      let url = 'category/list';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async searchProductsLint(params){
    let result;
    console.log("wadidwa", params)
    const customHeaders = getTokenHeader();
    try {

      
      const url   = 'products/report';
      // params.type = 'evoucher';
      let getDetail = {
        get_detail: true,
      }
      Object.assign(params.search, getDetail)
      // console.log('params', params)
      result  = await this.myService.searchResult(params, url, customHeaders);
      console.log("llll", result)
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }
  public async searchProductsReportLint(params){
    let result;
    // console.log("wadidwa", params)
    const customHeaders = getTokenHeaderRetailer();;
    try {

      
      const url   = 'product/all/report';
      // params.type = 'evoucher';
      // let getDetail = {
      //   get_detail: true,
      // }
      Object.assign(params.search)
      params.search.type = {
        in:["product","voucher","gold","top-up"]
      };
      console.log('params', params)
      result  = await this.myService.searchResult(params, url, customHeaders);
      // console.log("llll", result)
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async searchVouchersReportLint(params){
    let result;
    // console.log("wadidwa", params)
    const customHeaders = getTokenHeaderRetailer();;
    try {

      
      const url   = 'product/all/report';
      
      let getDetail = {
        get_detail: true,
      }
      Object.assign(params.search, getDetail)
      params.search.type = 'e-voucher';
      // console.log('params', params)
      result  = await this.myService.searchResult(params, url, customHeaders);
      // console.log("llll", result)
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  // public async searchEvouchersLint(params){
  //   let result;
  //   const customHeaders = getTokenHeader();
  //   try {
  //     const url   = 'products/all';
  //     console.log("isi dr evoucher code", url)
  //     params.type = 'e_voucher';
  //     result  = await this.myService.searchResult(params, url, customHeaders);
  //   } catch (error) {
  //     throw new TypeError(error);
  //   }
  //     return  result;
  // }

  public async addProductsLint(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'products/add';
      result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      console.log("ERROR , error", (<Error>error).message);
      throw new TypeError((<Error>error).message);
    }
    return  result;
  }

  public async addProducts(params){
    console.log("params", params);
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      let url = 'product/add';
      result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      console.log("ERROR , error", (<Error>error).message);
      throw new TypeError((<Error>error).message);
    }
    return  result;
  }

  public async generateProductsLint(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'generate';
      result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async updateProductData(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      let url = 'product/update';
      // console.log(params);
      result = await this.myService.update(params, url, customHeaders);
    }
    catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async updateProduct(params, _id){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'products/' + _id;
      // console.log(params);
      result = await this.myService.post(params, url, customHeaders);
    }
    catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async configGroup() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      let url = 'configs/keyname/merchant-group-configuration' ;
      // con  ole.log(params);
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  // public async redeemGroup() {
  //   let result;
  //   const customHeaders = getTokenHeader();
  //   try {
  //     let url = 'configs/keyname/products-configuration';
  //     result = await this.myService.get(null, url, customHeaders);
  //   } catch (error) {
  //     throw new TypeError(error);
  //   }
  //   return result;
  // }


  

  public async updateEvoucher(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'products/' +params.previous_product_code;
      // console.log(params);
      result = await this.myService.update(params, url, customHeaders);
    }
    catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async deleteProduct(product_code){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      let url = 'product/delete/' + product_code;
      // console.log(params);
      result = await this.myService.delete(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getDropdown(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'products/all';
      result = await this.myService.get(null,url,customHeaders);
    } catch(error){
      throw new TypeError(error);
    }
    return result;
  }


  public async configuration(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'configs/keyname/products-configuration' ;
      // console.log(params);
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getProductActivityReport(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'products/report/product-activity-summary' ;
      // console.log(params);
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  

  public async getMerchantList(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'merchant_list' ;
      // console.log(params);
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async approvedProducts(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'products/approve';
      
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async statusEdit(params) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'products/set_status';
      result = await this.myService.post(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async packageBundle() {
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'products/create_bundle';
      result = await this.myService.post(null,url,customHeaders);
    } catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async uploadFile(file, obj, fileType, funcOnFinish){
    const fd = new FormData();
    fd.append('image', file, file.name)

    if(fileType == 'product'){
      fd.append('type', 'product')
    } else if(fileType == 'image') {
      fd.append('type', 'image')
    } 
    let url: string = api_url() + 'evoucher/upload';
    // console.log(url);

    let myToken = localStorage.getItem('tokenlogin');
    
    this.httpReq = this.http.post(url,fd,{
      headers: new HttpHeaders({
        'Authorization': myToken,
      }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(event => {
      // console.log(event);
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        // obj.updateProgressBar(prgval);
        // console.log(obj.cancel);
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
       
        console.log('Upload Progress: ' +prgval+"%", event);
      }
      if(event["body"] != undefined){
        console.log("EVENT STATUS body", event['body']);
        funcOnFinish(event['body'].result);
        Swal.fire(
          'Success!',
          'Upload Success',
          'success'
        )
      
      }
    },
    result =>{
      console.log("ERROR result", result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
          Swal.fire(
            'Error!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }


  public async upload(file, obj, fileType, funcOnFinish, newFileName :any = false){
    const fd = new FormData();
    if(newFileName){
      if(environment.production == false) newFileName = 'DEV-'+newFileName;
      if(fileType == 'image'){
        fd.append('image', file, newFileName);
      } else {
        fd.append('document', file, newFileName);
      }
    }else {
      if(fileType == 'image'){
        fd.append('image', file, newFileName);
      } else {
        fd.append('document', file, newFileName);
      }
    }
    let endPoint = 'media/upload/image';
    // if(fileType == 'product'){
    //   fd.append('type', 'product');
    // } else if(fileType == 'image') {
    //   fd.append('type', 'image');
    // } else if(fileType == 'document'){
    //   fd.append('type', 'document');
    // }
    fd.append('type', fileType);
    if(fileType == 'document') endPoint = 'media/upload/document';
    let url: string = api_url() + endPoint;
    // console.log(url);

    let myToken = localStorage.getItem('tokenlogin');
    
    this.httpReq = this.http.post(url,fd,{
      headers: new HttpHeaders({
        'Authorization': myToken,
        'app-label': 'general_dt',
      }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(event => {
      // console.log(event);
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        // obj.updateProgressBar(prgval);
        // console.log(obj.cancel);
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
       
        console.log('Upload Progress: ' +prgval+"%", event);
      }
      if(event["body"] != undefined){
        console.log("EVENT STATUS body", event['body']);
        funcOnFinish(event['body']);
      }
    },
    result =>{
      console.log("ERROR result", result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
          Swal.fire(
            'Error!',
            result.error.error,
            'warning'
          )
        }
     
    }
    );
  }

  public async addProductBulk(file, obj,payload){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('type',payload.type)
    fd.append('document', file)
    let url: string = api_url()+'product/upload/bulk';
    console.log('now',fd);
    let result;
    
    this.httpReq = this.http.post(url,fd,{
      headers: getTokenHeaderRetailer(),
      // headers : new HttpHeaders({
      //   'Authorization': myToken,
      //   'app-label': 'retailer_prg'
      // }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      console.log("event",event);
      console.log(event);
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
        console.log('this is body');
        console.warn(" obj", obj)
        // obj.firstLoad();
          obj.Report=c.body;
          //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
          obj.process_id = c.body;
          // console.log("process_id",obj.Report);
          // obj.prodOnUpload = true;
          // console.log(c.body.result.failed);
          // obj.failed = c.body.result.failed.length;

          if (c.status == 200) {
            obj.selectedFile = null;
            obj.startUploading = false;
            Swal.fire(
              'Success!',
              'Your file has been uploaded.',
              'success'
            )
          }
         
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
          // Swal.fire(
          //   'Success!',
          //   'Your file has been uploaded.',
          //   'success'
          // )
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      //console.log(result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
          obj.selectedFile = null;
          Swal.fire(
            'Failed!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }

  // getContacts(){
  //   return Promise.resolve(CONTACTS);
  // }
  
  // insertContact(contact: Contact){
  //   Promise.resolve(CONTACTS).then((contacts: Contact[]=> contacts.push(contact));
  // }

}
