import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import {api_url, getTokenHeader} from '../../../environments/environment';

interface Membership {
  error: any ,
  result: any
}

@Injectable({
  providedIn: 'root',

})



export class MembershipService {

  public gerro:  string = 'test';
  public apiUrl: string = '';

  constructor( public myService:MasterService) { 
    this.apiUrl = api_url();
  }

  public async getMembershipsLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url     = 'members/memberships/all';
            result  = await this.myService.get(null, url, customHeaders);
      
    } catch (errResp) {
      throw new TypeError(errResp);
    }

    return  result;
  }

 
  public async addNewMembership(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'members/memberships/create';
          result  = await this.myService.add(params, url, customHeaders);
    } catch (errResp) {
      throw new TypeError(errResp);
    }

    return  result;
  }

  public async updateMembership(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'members/memberships/all';
          result  = await this.myService.update(params, url, customHeaders);
    } catch (errResp) {
      throw new TypeError(errResp);
    }
      return  result;
  }

  public async deleteMembership(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url    = 'members/memberships/all';
            result = await this.myService.delete(params, url, customHeaders);
    } catch (errResp) {
      throw new TypeError(errResp);
    }
      return  result;
  }

  public async searchMembershipLint(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url    = 'members/memberships/all';
            result = await this.myService.searchResult(params, url, customHeaders);
    } catch (errResp) {
      throw new TypeError(errResp);
    }
      return  result;
  }

  public async detailMembership(membership_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'members/memberships/detail/' + membership_id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (errResp) {
      throw new TypeError(errResp);
    }

    return  result;
  }

  public async updateMembershipID(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'members/memberships/update/' +params._id;
      result  = await this.myService.update(params, url, customHeaders);
      console.log(url,result);
    } catch (errResp) {
      throw new TypeError(errResp);
    }
    return  result;
  }

  public async delete(params) {
    let url, result: any;
    const customHeaders = getTokenHeader();
    try {
      url     = 'members/memberships/delete/' + params._id;
      // console.log(params);
      result  = await this.myService.delete(params, url, customHeaders);
    } catch (errResp) {
      throw new TypeError(errResp);
    }
    return result;
  }

  public async getDownloadFileLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'membership/?report=csv';
      console.log(url);
          result  = await this.myService.get(null, url, customHeaders);
      console.log(result);
      
    } catch (errResp) {
      throw new TypeError(errResp);
    }

    return  result;
  }
  
}
