"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.OrderhistoryService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("../../../environments/environment");
var OrderhistoryService = /** @class */ (function () {
    function OrderhistoryService(http, myService) {
        this.http = http;
        this.myService = myService;
        this.api_url = '';
        this.api_url = environment_1.api_url();
    }
    OrderhistoryService.prototype.getOrderhistory = function (params) {
        // const uri = 'http://192.168.1.198/f3/v1/orderhistory/all';
        // console.log(uri);
        // return this.http.get<Orderhistory[]>(uri);
        var myToken = localStorage.getItem('tokenlogin');
        var myStatus = localStorage.getItem('isLoggedin');
        if (myStatus == 'true') {
            try {
                if (params) {
                    var httpParams = new http_1.HttpParams();
                    params.search = Object.assign({}, params.search);
                    params.order_by = Object.assign({}, params.order_by);
                    var arrayParams = Object.assign({}, params);
                    httpParams = httpParams.append('request', JSON.stringify(arrayParams));
                    var httpOpt = {
                        headers: new http_1.HttpHeaders({
                            'Content-Type': 'application/json',
                            'Authorization': myToken
                        }),
                        params: httpParams
                    };
                    var uri = this.api_url + 'order-invoice/report';
                    console.log('URI order history', uri);
                    return this.http.get(uri, httpOpt);
                }
            }
            catch (error) {
                throw new TypeError(error);
            }
        }
        else {
            localStorage.removeItem('isLoggedin');
            window.location.reload();
        }
    };
    OrderhistoryService.prototype.getOrderhistoryLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log("resultnya om", result.result.values);
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    /* langkah ke 2 bikin fungsi api */
    OrderhistoryService.prototype.getSalesOrdeReport = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/report?request={"search":{"status":"PAID"},"limit_per_page":"50","current_page":"1"}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log("resultnya om", result.result.values);
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getShippingReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, url3, url2, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = '';
                        url3 = 'orderhistory/shipping-report?request={"search":{"based_on": "products"},"limit_per_page":50,"current_page":1}';
                        url2 = 'orderhistory/shipping-report?request={"search":{"based_on": "order_id"},"limit_per_page":50,"current_page":1}';
                        if (params == 'order_id') {
                            url = url2;
                        }
                        else {
                            url = url3;
                        }
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchShippingProductReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        params.search.based_on = "products";
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/shipping-report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchShippingidReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        params.search.based_on = "order_id";
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/shipping-report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchShippingReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/shipping-report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getSalesOrderReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, url3, url2, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = '';
                        url3 = 'sales-order/admin?request={"search":{"based_on": "products"},"limit_per_page":50,"current_page":1}';
                        url2 = 'sales-order/admin?request={"search":{"based_on": "invoice_id"},"limit_per_page":50,"current_page":1}';
                        if (params == 'order_id') {
                            url = url2;
                        }
                        else {
                            url = url3;
                        }
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchSalesOrderReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        params.search.based_on = "products";
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/admin';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        throw new TypeError(error_8);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchSalesOrderReport2int = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        params.search.based_on = "invoice_id";
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/admin';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_9 = _a.sent();
                        throw new TypeError(error_9);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchSalesOrderReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_10 = _a.sent();
                        throw new TypeError(error_10);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistoryLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                try {
                    // const url = 'order-invoice/report'
                    // result = await this.getOrderhistory(params).toPromise();
                }
                catch (error) {
                    throw new TypeError(error);
                }
                return [2 /*return*/, result];
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistory = function (historyID) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/detail';
                        return [4 /*yield*/, this.myService.post({ order_id: historyID }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_11 = _a.sent();
                        throw new TypeError(error_11);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getDetailOrderhistory = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/booking_detail/' + params;
                        console.log(url);
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_12 = _a.sent();
                        throw new TypeError(error_12);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderHistoryMerchant = function () {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/merchant?request={%22search%22:{},%22current_page%22:1,%22order_by%22:{%22order_date%22:-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_13 = _a.sent();
                        throw new TypeError(error_13);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderHistoryMerchantFilter = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/merchant';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_14 = _a.sent();
                        throw new TypeError(error_14);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistory = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update_status/' + params.status;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result, url);
                        return [3 /*break*/, 4];
                    case 3:
                        error_15 = _a.sent();
                        throw new TypeError(error_15);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getMerchantOrderHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, params, result, url, error_16;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        params = { 'status': 'waiting for confirmation' };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_16 = _a.sent();
                        throw new TypeError(error_16);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistorysummaryLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_17;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/alls';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log('orderhistory/alls', 'result');
                        return [3 /*break*/, 4];
                    case 3:
                        error_17 = _a.sent();
                        throw new TypeError(error_17);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorysummaryLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_18;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/alls';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_18 = _a.sent();
                        throw new TypeError(error_18);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistorysummary = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_19;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_19 = _a.sent();
                        throw new TypeError(error_19);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistorySummary = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, result_1, error_20;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result_1 = _a.sent();
                        console.log(result_1, url);
                        return [3 /*break*/, 4];
                    case 3:
                        error_20 = _a.sent();
                        throw new TypeError(error_20);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistoryallhistoryLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_21;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/alls';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_21 = _a.sent();
                        throw new TypeError(error_21);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderHistorySummaryByDate = function (curDate) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_22;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/summary';
                        return [4 /*yield*/, this.myService.get({ request: JSON.stringify({ date: curDate }) }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_22 = _a.sent();
                        throw new TypeError(error_22);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getAmountOrderHistorySummaryByDate = function (curDate) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_23;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/summary-amount';
                        return [4 /*yield*/, this.myService.get({ request: JSON.stringify({ date: curDate }) }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_23 = _a.sent();
                        throw new TypeError(error_23);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistoryallhistoryLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_24;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_24 = _a.sent();
                        throw new TypeError(error_24);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistoryallhistory = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_25;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_25 = _a.sent();
                        throw new TypeError(error_25);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryAllHistory = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_26;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_26 = _a.sent();
                        throw new TypeError(error_26);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryAllHistoryCancel = function (orderhistory_id, params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_27;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_27 = _a.sent();
                        throw new TypeError(error_27);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.cancelOrder = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_28;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/cancel';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_28 = _a.sent();
                        throw new TypeError(error_28);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.requesting = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/courier_pickup';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        throw new TypeError(err_1);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryAllHistoryMerchant = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_29;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/booking_detail/' + params.booking_id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_29 = _a.sent();
                        throw new TypeError(error_29);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistorysuccessLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_30;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report?request={"search": {"status":"PAID"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_30 = _a.sent();
                        throw new TypeError(error_30);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getCourier = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_31;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'courier/list/all';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_31 = _a.sent();
                        throw new TypeError(error_31);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorysuccessLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_32;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "PAID";
                        console.log(params);
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_32 = _a.sent();
                        throw new TypeError(error_32);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistorysuccess = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_33;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_33 = _a.sent();
                        throw new TypeError(error_33);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistorySuccess = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_34;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_34 = _a.sent();
                        throw new TypeError(error_34);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistorypendingLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_35;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(result);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report?request={"search": {"status":"pending"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_35 = _a.sent();
                        throw new TypeError(error_35);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorypendingLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_36;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // console.log(params);
                        params.search.status = "pending";
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_36 = _a.sent();
                        throw new TypeError(error_36);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistorypending = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_37;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_37 = _a.sent();
                        throw new TypeError(error_37);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryPending = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_38;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_38 = _a.sent();
                        throw new TypeError(error_38);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistorycancelLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_39;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(result);
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report?request={"search": {"status":"cancel"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_39 = _a.sent();
                        throw new TypeError(error_39);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorycancelLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_40;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "cancel";
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_40 = _a.sent();
                        throw new TypeError(error_40);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistorycancel = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_41;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_41 = _a.sent();
                        throw new TypeError(error_41);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryCancel = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_42;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log('params', params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + params.id + '/order_status';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_42 = _a.sent();
                        throw new TypeError(error_42);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistoryfailedLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_43;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(result);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report?request={"search": {"status":"failed"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_43 = _a.sent();
                        throw new TypeError(error_43);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistoryfailedLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_44;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //console.log(params);
                        params.search.status = "failed";
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_44 = _a.sent();
                        throw new TypeError(error_44);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistoryfailed = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, url, error_45;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url)];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 3];
                    case 2:
                        error_45 = _a.sent();
                        throw new TypeError(error_45);
                    case 3: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryFailed = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_46;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_46 = _a.sent();
                        throw new TypeError(error_46);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistoryerrorLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_47;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(result);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report?request={"search": {"status":"error"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_47 = _a.sent();
                        throw new TypeError(error_47);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistoryerrorLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_48;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //console.log(params);
                        params.search.status = "error";
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_48 = _a.sent();
                        throw new TypeError(error_48);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistoryerror = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_49;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_49 = _a.sent();
                        throw new TypeError(error_49);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryError = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_50;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_50 = _a.sent();
                        throw new TypeError(error_50);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //baru
    OrderhistoryService.prototype.getOrderhistorywaitingLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_51;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(result);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report?request={"search": {"status":"waiting for confirmation"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_51 = _a.sent();
                        throw new TypeError(error_51);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorywaitingLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_52;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //console.log(params);
                        params.search.status = "waiting for confirmation";
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_52 = _a.sent();
                        throw new TypeError(error_52);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistorywaiting = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_53;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_53 = _a.sent();
                        throw new TypeError(error_53);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryWaiting = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_54;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_54 = _a.sent();
                        throw new TypeError(error_54);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getCRR = function (period, params) {
        if (params === void 0) { params = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_55;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-history/report/crr/' + period;
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log("DISINI BRO:", result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_55 = _a.sent();
                        throw new TypeError(error_55);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getCNR = function (period, params) {
        if (params === void 0) { params = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_56;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-history/report/cnr/' + period;
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log("CNR", result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_56 = _a.sent();
                        throw new TypeError(error_56);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.holdMerchant = function (booking_id, param) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_57;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/hold/' + booking_id;
                        return [4 /*yield*/, this.myService.post(param, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_57 = _a.sent();
                        throw new TypeError(error_57);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.holdMerchantRefund = function (booking_id, param) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_58;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/refund/' + booking_id;
                        return [4 /*yield*/, this.myService.post(param, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_58 = _a.sent();
                        throw new TypeError(error_58);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.holdMerchantRelease = function (booking_id, param) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_59;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/release/' + booking_id;
                        return [4 /*yield*/, this.myService.post(param, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_59 = _a.sent();
                        throw new TypeError(error_59);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], OrderhistoryService);
    return OrderhistoryService;
}());
exports.OrderhistoryService = OrderhistoryService;
