import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import {getTokenHeader} from '../../../environments/environment';

interface Notification {
  error: any ,
  result: any
}

@Injectable({
  providedIn: 'root',

})



export class BroadcastService {

  public gerro:  string = 'test';
  constructor(public myService:MasterService) { 
  }

  public async getBroadcastLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifbroadcast/all';
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewBroadcast(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifbroadcast';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updateNotification(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/all';
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async getSettingLint(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'notificationsetting/all';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getGroupLint(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'notifications_group/all';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getTemplateLint(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'notifications_message/all';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }
  public async getCTAList(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'notifications/config';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async detailBroadcast(_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifbroadcast/' + _id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }
  public async addNewBroadcastNotif(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/send';
      console.log(url,params);
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }


  public async deleteNotification(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifications/all';
      result  = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async searchBroadcastLint(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifbroadcast/all';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }
  
  public async updateBroadcastID(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'notifbroadcast/' +params._id;
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      // console.log(result);
    return  result;
  }

  public async deleteNotificationSetting(params) {
    let url, result: any;
    const customHeaders = getTokenHeader();
    try {
    url = 'notificationsetting/' + params._id;
    // console.log(params);
    result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
}
