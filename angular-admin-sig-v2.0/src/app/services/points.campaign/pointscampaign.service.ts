import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import {api_url, getTokenHeader} from '../../../environments/environment';

interface PointsCampaign {
  error: any ,
  result: any
}

@Injectable({
  providedIn: 'root',

})


export class PointsCampaignService {

  public gerro:  string = 'test';
  public api_url: string = '';

  constructor(public myService:MasterService) { 
    this.api_url = api_url();
  }

  public async getAllPointsCampaignLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointscampaign/all';
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewPointsCampaign(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointscampaign';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewCampaign(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointscampaign';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updatePointsCampaign(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointscampaign/all';
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async getMerchantLint() {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      let params ={
        'type' : 'merchant'
      }
      const url   = 'member/member_params';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async searchPointsCampaignLint(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointscampaign/all';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async detailPointsCampaign(_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointscampaign/detail/' + _id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updatePointsCampaignID(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointscampaign/' +params._id;
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      // console.log(result);
    return  result;
  }

  public async delete(params) {
    let url, result: any;
    const customHeaders = getTokenHeader();
    try {
    url = 'pointscampaign/' + params._id;
    // console.log(params);
    result = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  
}
