import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import {api_url, getTokenHeader} from '../../../environments/environment';

interface PointsGroup {
  error: any ,
  result: any
}

@Injectable({
  providedIn: 'root',

})


export class PointsGroupService {

  public gerro:  string = 'test';
  public api_url: string = '';

  constructor(public myService:MasterService) { 
    this.api_url = api_url();
  }

  public async getAllPointsGroupLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsgroup/all';
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewPointsGroup(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsgroup';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewMerchantOutlet(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'member/newoutlet';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewOutlet(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsgroup';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async getPointModels(){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsgroup/getmodels';
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async getPointsModelsLint(params) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsgroup/modelslist/'+params.id;
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updatePointsGroup(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsgroup/all';
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async deletePointsGroup(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsgroup/all';
      result  = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async searchPointsGroupLint(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsgroup/all';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async detailPointsGroup(_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsgroup/detail/' + _id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updatePointsGroupID(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsgroup/' +params._id;
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      // console.log(result);
    return  result;
  }

  public async delete(params) {
    let url, result: any;
    const customHeaders = getTokenHeader();
    try {
    url = 'pointsgroup/' + params._id;
    // console.log(params);
    result = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  
}
