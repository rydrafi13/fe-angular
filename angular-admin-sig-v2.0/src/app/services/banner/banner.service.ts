import { Injectable,Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { MasterService } from '../master.service';
import {api_url, getTokenHeader} from '../../../environments/environment';

interface Banner {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})

export class BannerService {

  api_url: string = '';
  public httpReq: any = false;
  public gerro: string = "test";
  constructor(private http: HttpClient, public myService:MasterService) { 
    this.api_url = api_url();
  }

  
  public async getBannerLint(){
    let result;
    const customHeaders = getTokenHeader();
    console.log(result);
    try{
      const url   = 'banner/all';
      result  = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return  result;
  }

  public async getLocationList(){
    //console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url   = 'banner/location-list';
      result  = await this.myService.get({}, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async searchBannerLint(params){
    //console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url   = 'banner/all';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async detailBanner(location){
    let result;
    const customHeaders = getTokenHeader();
    try{  
      const url   = 'banner/' +location;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    //console.log(result);
    return  result;
  }

  public async updateBanner(params){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'banner/update/' + params._id;
      const result  = await this.myService.add(params, url, customHeaders);
      console.log(result,url);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async addNewBanner(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'banner';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

   public async upload(file, obj, fileType,funcOnFinish){
    const fd = new FormData();
    fd.append('image', file, file.name)
    // console.log('form data', fd)
    if(fileType == 'product'){
      fd.append('type', 'product')
    } else if(fileType == 'image') {
      fd.append('type', 'image')
    } 
    let url: string = api_url() + 'media/upload';
    //console.log(url);
    let myToken = localStorage.getItem('tokenlogin');
    this.httpReq = this.http.post(url,fd,{
      headers: new HttpHeaders({
        'Authorization': myToken,
      }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(event => {
      // let c = JSON.parse(JSON.stringify(event));
      // if(c.body){
      //   obj.imageUrl = c.body.result.url;
      // }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        console.log(obj.cancel);
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        console.log('upload Progress: ' +prgval+"%",event);
      }if(event["body"] != undefined){
        console.log("EVENT STATUS body", event['body']);
        funcOnFinish(event['body'].result);
      }
    },
    result =>{
      console.log(result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
        }
    }
    );
  }

  public async deleteBanner(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'banner/remove/' + params._id;
       console.log('ini params',params._id);
      result = await this.myService.delete(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }
}
