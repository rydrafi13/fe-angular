import { Injectable, Component, OnInit } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';

import {api_url, getAPIKeyHeader, getTokenHeader} from '../../../environments/environment';
import { MasterService } from '../master.service';

interface Article {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',

})

export class ArticleService {

  api_url: string = "";
  constructor(public myService: MasterService) {
    this.api_url = this.api_url;
  }

  public async detailArticle(_id) {
    let result, url:String;
    const customHeaders = getTokenHeader();
    try {
      url     = this.api_url + 'article/detail/' + _id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getArticleLint() {
    let result, url:String;
    const customHeaders = getTokenHeader();
    try {
      url    = this.api_url + 'article/findAll';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchArticleLint(params) {
    let result, url:String;
    const customHeaders = getTokenHeader();
    try {
      url = this.api_url + 'article/findAll';
      result = await this.myService.searchResult(params, url, customHeaders);
      //result = await this.getArticle(params).toPromise();
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async addArticleLint(params) {
    let result, url:String;
    const customHeaders = getTokenHeader();
    try {
      let url = this.api_url + 'article/insert';
      result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateArticle(params) {
    let url = this.api_url + 'article/update/' + params.previous_article_id;
    const customHeaders = getTokenHeader();
    // console.log(params);
    let result = await this.myService.update(params, url, customHeaders);
    return result;
  }

  public async deleteArticle(params) {
    let result, url:String;
    const customHeaders = getTokenHeader();
    try {
      url   = this.api_url + 'article/delete/' + params._id;
      result = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
}
