import { Injectable, Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { MasterService } from '../master.service';
import { api_url, getTokenHeader } from '../../../environments/environment';

interface Banner {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',

})


export class CountryService {

  api_url: string = '';
  constructor(private http: HttpClient, public myService: MasterService) {
    this.api_url = api_url();
  }

  public addLog(params: any) {
    let result;
    const customHeaders = getTokenHeader();

    params.platform = "browser";
    params.app = "admin-panel";

    try {
      const url = 'activity/add-log';

      this.myService.post(params, url, customHeaders).then((r) => { }).catch((e) => { });
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async getActivityReport() {
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'activity/report';
      result = await this.myService.get({}, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public getCountryList() {
    const list = [

      "Afghanistan",
      "Albania",
      "Algeria",
      "Andorra",
      "Angola",
      "Antigua and Barbuda",
      "Argentina",
      "Armenia",
      "Australia",
      "Austria",
      "Azerbaijan",
      "Bahamas",
      "Bahrain",
      "Bangladesh",
      "Barbados",
      "Belarus",
      "Belgium",
      "Belize",
      "Benin",
      "Bhutan",
      "Bolivia",
      "Bosnia and Herzegovina",
      "Botswana",
      "Brazil",
      "Brunei",
      "Bulgaria",
      "Burkina Faso",
      "Burundi",
      "Côte d'Ivoire",
      "Cabo Verde",
      "Cambodia",
      "Cameroon",
      "Canada",
      "Central African Republic",
      "Chad",
      "Chile",
      "China",
      "Colombia",
      "Comoros",
      "Congo",
      "Costa Rica",
      "Croatia",
      "Cuba",
      "Cyprus",
      "Czechia(Czech Republic)",
      "Democratic Republic of the Congo",
      "Denmark",
      "Djibouti", ,
      "Dominica",
      "Dominican Republic",
      "Ecuador",
      "Egypt",
      "El Salvador",
      "Equatorial Guinea",
      "Eritrea",
      "Estonia",
      'Eswatini(fmr. "Swaziland")',
      "Ethiopia",
      "Fiji",
      "Finland",
      "France",
      "Gabon",
      "Gambia",
      "Georgia",
      "Germany",
      "Ghana",
      "Greece",
      "Grenada",
      "Guatemala",
      "Guinea",
      "Guinea - Bissau",
      "Guyana",
      "Haiti",
      "Holy See",
      "Honduras",
      "Hungary",
      "Iceland",
      "India",
      "Indonesia",
      "Iran",
      "Iraq",
      "Ireland",
      "Israel",
      "Italy",
      "Jamaica",
      "Japan",
      "Jordan",
      "Kazakhstan",
      "Kenya",
      "Kiribati",
      "Kuwait",
      "Kyrgyzstan",
      "Laos",
      "Latvia",
      "Lebanon",
      "Lesotho",
      "Liberia",
      "Libya",
      "Liechtenstein",
      "Lithuania",
      "Luxembourg",
      "Madagascar",
      "Malawi",
      "Malaysia",
      "Maldives",
      "Mali",
      "Malta",
      "Marshall Islands",
      "Mauritania",
      "Mauritius",
      "Mexico",
      "Micronesia",
      "Moldova",
      "Monaco",
      "Mongolia",
      "Montenegro",
      "Morocco",
      "Mozambique",
      "Myanmar(formerly Burma)",
      "Namibia",
      "Nauru",
      "Nepal",
      "Netherlands",
      "New Zealand",
      "Nicaragua",
      "Niger",
      "Nigeria",
      "North Korea",
      "North Macedonia",
      "Norway",
      "Oman",
      "Pakistan",
      "Palau",
      "Palestine State",
      "Panama",
      "Papua New Guinea",
      "Paraguay",
      "Peru",
      "Philippines",
      "Poland"
      // Portugal,
      // Qatar,
      // Romania,
      // Russia			
      //   Rwanda,
      // Saint Kitts and Nevis,
      // Saint Lucia,
      // Saint Vincent and the Grenadines,
      // Samoa, ,
      // San Marino,
      // Sao Tome and Principe,
      // Saudi Arabia			
      //   Senegal,
      // Serbia,
      // Seychelles,
      // Sierra Leone,
      // Singapore,
      // Slovakia,
      // Slovenia,
      // Solomon Islands, ,
      // Somalia,
      // South Africa			
      //   South Korea,
      // South Sudan,
      // Spain,
      // Sri Lanka,
      // Sudan			
      //   Suriname, ,
      // Sweden,
      // Switzerland,
      // Syria,
      // Tajikistan,
      // Tanzania,
      // Thailand,
      // Timor - Leste,
      // Togo,
      // Tonga,
      // Trinidad and Tobago,
      // Tunisia,
      // Turkey,
      // Turkmenistan,
      // Tuvalu,
      // Uganda,
      // Ukraine,
      // United Arab Emirates,
      // United Kingdom,
      // United States of America			
      //   Uruguay,
      // Uzbekistan,
      // Vanuatu, ,
      // Venezuela,
      // Vietnam,
      // Yemen,
      // Zambia,
      // Zimbabwe

    ]

    return list;
  }
}
