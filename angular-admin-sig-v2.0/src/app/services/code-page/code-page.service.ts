import { Injectable, Component, OnInit } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url, getTokenHeader } from '../../../environments/environment';
import { MasterService } from '../master.service';


interface CodePage {
  error: any,
  result: any,
}

@Injectable({
  providedIn: 'root'
})


export class CodePageService {
  courses$: Observable<CodePage[]>;
  api_url : string = "";
  constructor(private http: HttpClient, public myService: MasterService) {
      this.api_url = api_url();
  }

  getPromoCode(params): Observable<CodePage[]> {
      // const uri = 'http://192.168.1.198/f3/v1/orderhistory/all';
      // console.log(uri);
      // return this.http.get<CodePage[]>(uri);
      let myToken = localStorage.getItem('tokenlogin');
      let myStatus = localStorage.getItem('isLoggedin');

      if (myStatus == 'true') {
        try {
          if (params) {

            let httpParams = new HttpParams();
            params.search = Object.assign({}, params.search);
            params.order_by = Object.assign({}, params.order_by);
            let arrayParams = Object.assign({}, params);
            httpParams = httpParams.append('request', JSON.stringify(arrayParams));

            const httpOpt = {
              headers: new HttpHeaders(
                {
                  'Content-Type': 'application/json',
                  'Authorization': myToken,
                }
              ),
              params: httpParams
            };

            const uri = this.api_url + 'promo-codes/report';
            console.log("URI promo code", uri)
            return this.http.get<CodePage[]>(uri, httpOpt);
          }
        } catch (error) {
          throw new TypeError(error);
        }
      } else {
        localStorage.removeItem('isLoggedin');
        window.location.reload();
      }
  }
  public async getPromoCodeLint() {
      let result;

      const customHeaders = getTokenHeader();


      try {
        const url = 'promo-codes/report';
        result = await this.myService.get(null, url, customHeaders);
        console.log(result);
      } catch (error) {
        throw new TypeError(error);
      }
      console.log(result);
      return result;
    }

  public async searchPromoCodeLint(params) {
      let result;
      const customHeaders = getTokenHeader();

      try {
        const url = 'promo-codes/report';
        result = await this.myService.searchResult(params, url, customHeaders);
      } catch (error) {
        throw new TypeError(error);
      }
      return result;
  }

  public async detailPromoCode(pointstransaction_id) {
      let result;
      const customHeaders = getTokenHeader();

      try {
        const url = 'promo-codes/' + pointstransaction_id;
        // const url = 'promo-codes/report?request={"search": {"_id":"'+pointstransaction_id+'"}}';
        result = await this.myService.get(null, url, customHeaders);
        console.log(result);
      } catch (error) {
        throw new TypeError(error);
      }
      return result;
  }

  public async updatePromoCode(params) {
      let result;
      const customHeaders = getTokenHeader();
      console.log(params);
      try {
        const url = 'promo-codes/update/' + params._id;
        result = await this.myService.update(params, url, customHeaders);
        console.log(url, result);
      } catch (error) {
        throw new TypeError(error);
      }
      return result;
    }
}
