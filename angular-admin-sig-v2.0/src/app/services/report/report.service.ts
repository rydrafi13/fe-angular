import { Injectable,Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import {api_url, getTokenHeader} from '../../../environments/environment';
import Swal from 'sweetalert2';

interface Report {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})



export class ReportService {

  courses$: Observable<Report[]>;

  api_url: string = '';
  public httpReq: any = false;
  public gerro: string = "test";
  constructor(private http: HttpClient,public myService:MasterService) {
    this.api_url = api_url(); 
  }

  
  public async getReportLint(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url   = 'report/all';
      result  = await this.myService.get(null, url, customHeaders);
      //console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }



  public async getReportProcessUploadLint(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url   = 'process/report/all';
      result  = await this.myService.get(null, url, customHeaders);
      //console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }
  public async uploadProduct(file, obj,payload){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('type',payload.type)
    fd.append('product_type',payload.product_type)
    fd.append('file', file)
    let url: string = api_url()+'products/upload';
    console.log('now',fd);
    let result;
    
    this.httpReq = this.http.post(url,fd,{
      headers : new HttpHeaders({
        'Authorization': myToken,
      }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      console.log("event",event);
      console.log(event);
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
        console.log('this is body');
        //obj.firstLoad();
          obj.Report=c.body.result.data;
          //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
          obj.process_id = c.body.result.proc_id;
          console.log("process_id",obj.Report);
          obj.prodOnUpload = true;
          console.log(c.body.result.failed);
          obj.failed = c.body.result.failed.length;
         
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
          Swal.fire(
            'Success!',
            'Your file has been uploaded.',
            'success'
          )
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      //console.log(result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
          Swal.fire(
            'Failed!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }

  public async upload(file, obj){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('process', file, file.name)
    let url: string = api_url()+'report/upload';
    console.log(url);
    let result;
    
    this.httpReq = this.http.post(url,fd,{
      headers : new HttpHeaders({
        'Authorization': myToken,
      }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      console.log("event",event);
      console.log(event);
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
        console.log('this is body');
        //obj.firstLoad();
          obj.Report=c.body.result.data;
          //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
          obj.process_id = c.body.result.proc_id;
          console.log("process_id",obj.Report);
          obj.prodOnUpload = true;
          console.log(c.body.result.failed);
          obj.failed = c.body.result.failed.length;
         
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      //console.log(result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
        }
    }
    );
  }

  public async searchReportLint(params){
    //console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url   = 'report/all';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async detailReport(_id){
    let result;
    const customHeaders = getTokenHeader();
    try{  
      const url   = 'report/' +_id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    //console.log(result);
    return  result;
  }

  public async getProductsCSV(process_id){
    let result;
    const customHeaders = getTokenHeader();
    try{  
      const url   = 'products/process_id/'+process_id;
      result  = await this.myService.get(null, url, customHeaders);
      console.log(url);
    } catch (error) {
      throw new TypeError(error);
    }
    //console.log(result);
    return  result;
  }

  // public async updateReport()
  // {

  // }
  
  public async updateReportExcel(params)
  {
    try{
      let url = 'update/reportexcel/' + params.process_id;
      const customHeaders = getTokenHeader();
      console.log(params);
      let result = await this.myService.update(params, url, customHeaders);
      return  result;
    } catch(error){
      throw new TypeError(error);
    }

  }
}
