import {Component, Injectable} from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url, getTokenHeader } from '../../../environments/environment';

import { MasterService } from '../master.service';

interface Promotion {
    error: any;
    result: any;
}

@Injectable({
    providedIn: 'root'
})



export class PromotionService {
    courses$: Observable<Promotion[]>;
    api_url = '';

  constructor(private http: HttpClient, public myService: MasterService) {
      this.api_url = api_url();
  }

  // getPromotion(params): Observable<Promotion[]> {
  //     const myToken = localStorage.getItem('tokenlogin');
  //     const myStatus = localStorage.getItem('isLoggedin');
  //
  //     if (myStatus == 'true') {
  //     try {
  //       if (params) {
  //
  //         let httpParams = new HttpParams();
  //         params.search = Object.assign({}, params.search);
  //         params.order_by = Object.assign({}, params.order_by);
  //         const arrayParams = Object.assign({}, params);
  //         httpParams = httpParams.append('request', JSON.stringify(arrayParams));
  //
  //         const httpOpt = {
  //           headers: new HttpHeaders(
  //             {
  //               'Content-Type': 'application/json',
  //               'Authorization': myToken,
  //             }
  //           ),
  //           params: httpParams
  //         };
  //
  //         const uri = this.api_url + 'promotions/report';
  //         // console.log('URI order history', uri);
  //         return this.http.get<Promotion[]>(uri, httpOpt);
  //       }
  //     } catch (error) {
  //       throw new TypeError(error);
  //     }
  //   } else {
  //     localStorage.removeItem('isLoggedin');
  //     window.location.reload();
  //   }
  // }

  public async getPromotionLint() {
      let result;
      const customHeaders = getTokenHeader();
    try {
      const url = 'promotions/report';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchPromotionLint(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'promotions/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  public async getPromotionUsageLint() {
    let result;
    const customHeaders = getTokenHeader();
  try {
    const url = 'promo-usage/report?request={"search":{},"limit_per_page":50,"current_page":1}';
    result = await this.myService.get(null, url, customHeaders);
    console.log(result);
  } catch (error) {
    throw new TypeError(error);
  }
  console.log(result);
  return result;
}

public async searchPromotionUsageLint(params) {
  // console.log(params);
  let result;
  const customHeaders = getTokenHeader();

  try {
    const url = 'promo-usage/report';
    result = await this.myService.searchResult(params, url, customHeaders);
  } catch (error) {
    throw new TypeError(error);
  }
  return result;
}
public async getPromotionStatisticLint() {
  let result;
  const customHeaders = getTokenHeader();
try {
  const url = 'promo-statistic/report?request={"search":{},"limit_per_page":50,"current_page":1}';
  result = await this.myService.get(null, url, customHeaders);
  console.log(result);
} catch (error) {
  throw new TypeError(error);
}
console.log(result);
return result;
}

public async searchPromotionStatisticLint(params) {
// console.log(params);
let result;
const customHeaders = getTokenHeader();

try {
  const url = 'promo-statistic/report';
  result = await this.myService.searchResult(params, url, customHeaders);
} catch (error) {
  throw new TypeError(error);
}
return result;
}

   public async detailPromotion(promotion_id) {
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'promotions/' + promotion_id;
      // const url = 'promotions/report?request={"search": {"_id":"' + promotion_id + '"}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updatePromotion(params) {
    let result;
    const customHeaders = getTokenHeader();
    console.log(params);
    try {
      const url = 'promotions/update_status/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }



  // PROMO CAMPAIGN SECTION


  public async getOngoingPromo() {
    let result;
    const customHeaders = getTokenHeader();
  try {
    const url = 'promo-campaign/ongoing';
    result = await this.myService.get(null, url, customHeaders);
    console.log(result);
  } catch (error) {
    throw new TypeError(error);
  }
  console.log(result);
  return result;
}

public async getPromoConfig() {
  let result;
  const customHeaders = getTokenHeader();
try {
  const url = 'promo-campaign/config';
  result = await this.myService.get(null, url, customHeaders);
  console.log(result);
} catch (error) {
  throw new TypeError(error);
}
console.log(result);
return result;
}

public async getPromoDetail(params) {
  let result;
  const customHeaders = getTokenHeader();
try {
  const url = 'promo-campaign/detail/' + params;
  result = await this.myService.get(null, url, customHeaders);
  console.log(result);
} catch (error) {
  throw new TypeError(error);
}
// console.log(result);
return result;
}

public async createPromoCampaign(params) {
  // console.log(params);
  let result;
  const customHeaders = getTokenHeader();
  try {
    const url   = 'promo-campaign/create';
    result  = await this.myService.add(params, url, customHeaders);
  } catch (error) {
    throw new TypeError(error);
  }

  return  result;
}
}
