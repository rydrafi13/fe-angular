import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import {api_url, getTokenHeader} from '../../../environments/environment';

interface PointsModels {
  error: any ,
  result: any
}

@Injectable({
  providedIn: 'root',

})


export class PointsModelsService {

  public gerro:  string = 'test';
  public api_url: string = '';

  constructor(public myService:MasterService) { 
    this.api_url = api_url();
  }

  public async getAllPointsModelsLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsmodels/all';
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewPointsModels(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsmodels';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewCampaign(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsmodels';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updatePointsModels(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsmodels/all';
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async getRuleType() {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsmodels/ruletype';
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async getCalcType() {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsmodels/calctype';
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async getTransactionType() {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsmodels/transactiontype';
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async searchPointsModelsLint(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsmodels/all';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async detailPointsModels(_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsmodels/detail/' + _id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updatePointsModelsID(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'pointsmodels/' +params._id;
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      // console.log(result);
    return  result;
  }

  public async delete(params) {
    let url, result: any;
    const customHeaders = getTokenHeader();
    try {
    url = 'pointsmodels/' + params._id;
    // console.log(params);
    result = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  
}
