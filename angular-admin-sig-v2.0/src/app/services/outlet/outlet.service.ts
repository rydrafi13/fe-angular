import { Injectable,Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { Observable , of} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { api_url, getTokenHeader} from '../../../environments/environment';
import { MasterService } from '../master.service';
// import { type } from 'os';

interface Product {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})

export class OutletService {
  courses$: Observable<Product[]>;

  api_url: string = '';
  public httpReq: any = false;
  constructor(private http: HttpClient, public myService:MasterService) { 
    this.api_url = api_url();
  }

  public async detailMerchant(merchant_username, params = {}){
    let result;

    const customHeaders = getTokenHeader()
    try {
      const url   = 'merchant/detail/' +encodeURIComponent(merchant_username)
      result  = await this.myService.get(params, url, customHeaders);
    } catch (error) {   
      throw new TypeError(error);
    }
    return  result;
  }

  public async getMerchant(params){
    let result;
    const customHeaders = getTokenHeader()
    try{
      const url = 'merchant/find_outlet'
      result = await this.myService.get(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async searchMerchant(params){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'merchant/all';
      params.type = 'merchant';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async showMerchant(params){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'merchant/report';
      result  = await this.myService.get(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async addMerchant(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'merchant/add';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }


  public async generateProductsLint(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'generate';
      result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async updateMerchant(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'merchant/update' + params.previous_product_code;
      console.log(params);
      result = await this.myService.update(params, url, customHeaders);
    }
    catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async deleteProduct(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'products/' + params._id;
      // console.log(params);
      result = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }


  public async configuration(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'configs/keyname/merchant-group-configuration';
      // console.log(params);
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }


  public async statusEdit(params) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'products/set_status';
      result = await this.myService.post(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }



  public async upload(file, obj, fileType, funcOnFinish?){
    const fd = new FormData();
    fd.append('image', file, file.name)

    if(fileType == 'product'){
      fd.append('type', 'product')
    } else if(fileType == 'image') {
      fd.append('type', 'image')
    } 
    let url: string = api_url() + 'products/upload';
    // console.log(url);

    let myToken = localStorage.getItem('tokenlogin');
    
    this.httpReq = this.http.post(url,fd,{
      headers: new HttpHeaders({
        'Authorization': myToken,
      }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(event => {
      // console.log(event);
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        // obj.updateProgressBar(prgval);
        // console.log(obj.cancel);
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
       
        console.log('Upload Progress: ' +prgval+"%", event);
      }
      if(event["body"] != undefined){
        console.log("EVENT STATUS body", event['body']);
        funcOnFinish(event['body'].result);
      }
    },
    result =>{
      console.log("ERROR result", result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
        }
    }
    );
  }


}
