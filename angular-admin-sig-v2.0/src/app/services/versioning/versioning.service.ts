import { Component, Injectable} from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url, getTokenHeader } from '../../../environments/environment';
import { MasterService } from '../master.service';

interface Version {
    error: any;
    result: any;
}

@Injectable({
    providedIn: 'root'
})


export class versioningService {
    courses$: Observable<Version[]>;
    api_url = '';

  constructor(private http: HttpClient, public myService: MasterService) {
      this.api_url = api_url();
  }

  public async getVersion() {
      let result;
      const customHeaders = getTokenHeader();
    try {
      const url = 'version/all';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async detailVersioning(params) {
    let result;
    const customHeaders = getTokenHeader();
  try {
    const url = 'version/' + params;
    result = await this.myService.get(null, url, customHeaders);
  } catch (error) {
    throw new TypeError(error);
  }
  console.log(result);
  return result;
}

public async updateVersioning(params) {
  let result;
  const customHeaders = getTokenHeader();
try {
  const url = 'version/' + params._id;
  result = await this.myService.update(params, url, customHeaders);
  console.log(result);
} catch (error) {
  throw new TypeError(error);
}
console.log(result);
return result;
}

public async deleteVersioning() {
  let result;
  const customHeaders = getTokenHeader();
try {
  const url = 'version/';
  result = await this.myService.delete(null, url, customHeaders);
  console.log(result);
} catch (error) {
  throw new TypeError(error);
}
console.log(result);
return result;
}

public async getCategoryLint(){
  let result;
  const customHeaders = getTokenHeader();
  try{
    let url = 'category/all';
    result = await this.myService.get(null, url, customHeaders);
  } catch (error) {
    throw new TypeError(error);
  }
  return  result;
}

}
