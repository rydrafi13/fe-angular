import { Injectable, Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { api_url, getTokenHeader, getTokenHeaderRetailer } from '../../../environments/environment';
import { MasterService } from '../master.service';
import Swal from 'sweetalert2';


interface EVoucher {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',

})


export class EVoucherService {
  courses$: Observable<EVoucher[]>;

  api_url: string = '';
  public httpReq: any = false;
  constructor(private http: HttpClient, public myService: MasterService) {
    this.api_url = api_url();
  }

  public async detailEVoucher(evoucher_id) {
    let result;
    const customHeaders = getTokenHeader()

    try {
      const url = 'evoucher/detail/' + evoucher_id;
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getEVoucherLint() {
    let result;
    const customHeaders = getTokenHeader()
    try {
      const url = 'evouchers/report';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchEvouchersLint(params) {
    let result;
    console.log('params', params)
    const customHeaders = getTokenHeader();
    try {
      const url = 'evouchers/report';
      result = await this.myService.searchResult(params, url, customHeaders);
      console.log('evouchers', result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getEvoucherList(params) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'evoucher/generated-vouchers/report/' + params.id;
      result = await this.myService.searchResult(params, url, customHeaders);
      // console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getEvoucherStockSummary(params) {
    let result;
    // let params = {'id': 'voucher-1'}
    if (!params.search) {
      params.search = {
        ev_product_code: params.id
      }
    }
    console.log("params_id", params)
    const customHeaders = getTokenHeader();
    try {
      const url = 'evouchers/stock-summary'
      result = await this.myService.searchResult(params, url, customHeaders);
      // console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchEvoucherStockSummary(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'evouchers/stock-summary'
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  public async getEvoucherSummaryReport() {
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try {
      const url = 'evoucher/summary/report';
      result = await this.myService.get(null, url, customHeaders);
      // console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchEvoucherSummaryReport(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      const url = 'evoucher/summary/report'
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  public async getEvoucherStockReport(params) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      // const url = 'evoucher/stock/report';
      let url = '';
      const urlRequested = 'evoucher/stock/report?request={"search":{"status": "REQUESTED"},"limit_per_page":50,"current_page":1}'
      const urlApproved = 'evoucher/stock/report?request={"search":{"status": "APPROVED"},"limit_per_page":50,"current_page":1}'
      
      if (params == 'REQUESTED'){
        url = urlRequested
      } else {
        url = urlApproved
      }
      
      result = await this.myService.get(null, url, customHeaders);
      // console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchEvoucherStockReport(params){
    params.search.status = "REQUESTED";
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      const url = 'evoucher/stock/report'
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  public async searchEvoucherStockReport2(params){
    params.search.status = "APPROVED";
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      const url = 'evoucher/stock/report'
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  public async detailEvoucherStock(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      const url = `evoucher/stock/detail/${params}`
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  public async processEvoucherCode(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      const url = `evoucher/stock/process`
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  public async uploadEvoucherCodeBulk(file, obj,payload){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('type',payload.type)
    fd.append('document', file)
    let url: string = api_url()+'evoucher/stock/upload';
    console.log('now',fd);
    let result;
    
    this.httpReq = this.http.post(url,fd,{
      headers: getTokenHeaderRetailer(),
      // headers : new HttpHeaders({
      //   'Authorization': myToken,
      //   'app-label': 'retailer_prg'
      // }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      console.log("event",event);
      console.log(event);
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
        console.log('this is body');
        //obj.firstLoad();
          console.warn("c body", c.body)
          obj.Report=c.body._id;
          // obj.prodOnUpload = true;

          if (c.body && c.body.status == "success" && obj.Report) {
            obj.selectedFile = null;
            obj.startUploading = false;
            Swal.fire(
              'Success!',
              'Your file has been uploaded.',
              'success'
            )
          }
          //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
          // obj.process_id = c.body;
          // console.log("process_id",obj.Report);
          // obj.prodOnUpload = true;
          // console.log(c.body.failed);
          // obj.failed = c.body.failed.length;
         
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
          
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      // console.warn("RESULT", result);
      if(result.error) {
          obj.errorFile = result.error.error;
          obj.cancel    = true;
          obj.selectedFile = null;
          Swal.fire(
            'Failed!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }

  // 
  // 
  // 

  public async updateEvoucher(params) {
    let result;
    const customHeaders = getTokenHeader();

    try {
      let url = 'evouchers/report' + params._id;
      // console.log(params);
      result = await this.myService.update(params, url, customHeaders);
    }
    catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getEvoucherSalesOrderReport() {
    let result;

    let params = {
      search: {
        type: 'evoucher'
      }
    }

    const customHeaders = getTokenHeader();
    try {
      const url = 'sales-order/find';
      result = await this.myService.searchResult(params, url, customHeaders);
      console.log("resultnya om", result.result.values);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;

  }

  public async searchEvoucherSalesOrderLint(params) {
    let result;
    console.log('params', params)
    const customHeaders = getTokenHeader();
    try {
      const url = 'sales-order/find';
      result = await this.myService.searchResult(params, url, customHeaders);
      console.log('test', result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async upload(file, obj) {
    const fd = new FormData();
    fd.append('image', file, file.name)
    fd.append('type', 'evoucher')
    let url: string = api_url() + 'evoucher/upload';
    console.log(url);

    let myToken = localStorage.getItem('tokenlogin');

    this.httpReq = this.http.post(url, fd, {
      headers: new HttpHeaders({
        'Authorization': myToken,
      }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(event => {
      console.log(event);
      if (event.type == HttpEventType.UploadProgress) {
        let prgval = Math.round(event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        console.log(obj.cancel);
        if (obj.cancel == true) {
          this.httpReq.unsubscribe();
        }
        console.log('Upload Progress: ' + prgval + "%");
      }
    },
      result => {
        console.log(result);
        if (result.error) {
        obj.errorFile = result.error.error;
          obj.cancel = true;
        }
      }
    );
  }

  public async getDropdown() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'products/evoucher';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async generateEVoucherLint(params) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      let url = 'evoucher/generate';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getCategoryLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      let url = 'category/all';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async approvedEvoucher(params) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      let url = 'evoucher/set_active';

      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async config() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      let url = 'configs/keyname/merchant-group-configuration';

      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }


}
