import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MemberService } from '../services/member/member.service';
import { Router} from '@angular/router';
import { timer } from 'rxjs';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  step = 1;
  hide = true;
  email = new FormControl('', [Validators.required, Validators.email]);
  emails;
  errorMessage;
  timeLeft: number = 60;
  interval;
  buttonActive= false;
  subscribeTimer: any;
  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('email') ? 'Not a valid email' :
        '';
  }
  public form1: any = {
    email: ''
  }
  public form2: any = {
    email: '',
    auth_code: ''
  }
  public form3: any = {
    password: '',
    repeat_password: ''
  }

  constructor(
    private memberService: MemberService,
    public router: Router,
  ) { }


  ngOnInit() {
  }

  async sendEmail() {
    let data = this.form1;
    console.log("test", data)
    try {
      let res = await this.memberService.resetPasswordRequest(data);
      if (res.result) {
        alert('request send');
        this.step = 2;
        clearInterval(this.interval);
        this.timeLeft = 60;
        this.buttonActive = false;
        this.startTimer();
      }
    }
    catch (e) {
      setTimeout(() => {
        this.errorMessage = ((<Error>e).message)
      }, 500)
    }
  }
  
  backToLogin(){
    if(localStorage)
    localStorage.removeItem('tokenlogin');
    this.router.navigate(['/login-merchant'])
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.buttonActive = true;
      }
    },1000)
  }

  async submitCode() {
    this.form2.email = this.form1.email;
    let data = this.form2;
    console.log("test2", data)
    try {
      let res = await this.memberService.resetPasswordCodeVerification(data);
      if (res.result) {
        alert('code send');
        localStorage.setItem('tokenlogin', res.result.token);
        this.step = 3;
      }
    }
    catch (e) {
      setTimeout(() => {
        this.errorMessage = ((<Error>e).message)
      }, 500)
    }
  }
  async changePassword() {
    let data = this.form3;
    console.log("test2", data)
    try {
      // let res = await this.memberService.resetPasswordChangePassword(data);
      // console.log("selesai")
      // if (res.result) {
      //   alert('Password Changed');
      //   localStorage.removeItem('tokenlogin');
      //   this.router.navigate(['/login-merchant'])
      // }
    }
    catch (e) {
      setTimeout(() => {
        this.errorMessage = ((<Error>e).message)
      }, 500)
    }
  }
  
}
