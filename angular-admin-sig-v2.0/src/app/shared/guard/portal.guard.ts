import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { MemberService } from '../../services/member/member.service';
import { PermissionObserver } from '../../services/observerable/permission-observer';

@Injectable()
export class PortalGuard implements CanActivate {
    permission

    constructor(private router: Router,
        private memberService: MemberService,
        private castPermission: PermissionObserver
        ) {

        }

    canActivate() {
    
        return this.firstLoad()
        
    }
    async firstLoad(){
        try{
            // let memberResult = await this.memberService.getMemberDetail();
            // this.permission  = memberResult.result.permission;
            
            this.permission  = "admin";
            this.castPermission.updateCurrentPermission(this.permission);
            return true;
        }
        catch(e){
            let errorMessage  = <Error>e.message;
            let n:string = errorMessage.toString();
            
            if(n.indexOf("unauthorized")>=0){
                alert("Your session is expired, please re-login");
                this.router.navigate(['/login/out']);
            }
            return false;
        }
        
    }
}
