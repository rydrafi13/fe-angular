"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = exports.createTranslateLoader = void 0;
var common_1 = require("@angular/common");
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var core_2 = require("@ngx-translate/core");
var http_loader_1 = require("@ngx-translate/http-loader");
var forms_1 = require("@angular/forms");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var shared_1 = require("./shared");
var form_detail_component_1 = require("./component-libs/form-detail/form-detail.component");
var notificationgroup_detail_component_1 = require("./layout/modules/notificationmessage/detail/notificationgroup.detail.component");
var notificationgroup_edit_component_1 = require("./layout/modules/notificationmessage/edit/notificationgroup.edit.component");
var allmember_module_1 = require("./layout/modules/admin_members/allmember/allmember.module");
var form_builder_table_module_1 = require("./component-libs/form-builder-table/form-builder-table.module");
var ngx_webstorage_service_1 = require("ngx-webstorage-service");
var storage_service_module_service_1 = require("./services/storage-service-module/storage-service-module.service");
var ngx_toggle_switch_1 = require("ngx-toggle-switch");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var angular_ng_autocomplete_1 = require("angular-ng-autocomplete");
var menu_1 = require("@angular/material/menu");
var icon_1 = require("@angular/material/icon");
var admin_stock_module_1 = require("./layout/modules/admin-stock.module");
// import { Member } from './layout/modules/member/detail/member.detail.module';
// AoT requires an exported function for factories
exports.createTranslateLoader = function (http) {
    return new http_loader_1.TranslateHttpLoader(http, './assets/i18n/', '.json');
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                // Ng2CompleterModule,
                menu_1.MatMenuModule,
                icon_1.MatIconModule,
                ng_bootstrap_1.NgbModule,
                ngx_toggle_switch_1.UiSwitchModule,
                ngx_webstorage_service_1.StorageServiceModule,
                common_1.CommonModule,
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                animations_1.BrowserAnimationsModule,
                http_1.HttpClientModule,
                angular_ng_autocomplete_1.AutocompleteLibModule,
                core_2.TranslateModule.forRoot({
                    loader: {
                        provide: core_2.TranslateLoader,
                        useFactory: exports.createTranslateLoader,
                        deps: [http_1.HttpClient]
                    }
                }),
                app_routing_module_1.AppRoutingModule,
                allmember_module_1.AllMemberModule,
                form_builder_table_module_1.FormBuilderTableModule,
                admin_stock_module_1.AdminStockModule,
            ],
            declarations: [
                app_component_1.AppComponent,
                form_detail_component_1.FormDetailComponent,
                // ProductDetailComponent,
                // AllMemberComponent, 
                // AllMemberDetailComponent,
                // MemberPointHistoryComponent,
                notificationgroup_detail_component_1.NotificationGroupDetailComponent,
                notificationgroup_edit_component_1.NotificationGroupEditComponent,
            ],
            providers: [shared_1.AuthGuard, storage_service_module_service_1.LocalStorageService,
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: shared_1.AuthInterceptor,
                    multi: true
                }
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
