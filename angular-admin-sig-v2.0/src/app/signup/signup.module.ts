import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { PageHeaderModule } from '../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../layout/modules/bs-component/bs-component.module';

@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule,PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
  ],
  declarations: [SignupComponent]
})
export class SignupModule { }
