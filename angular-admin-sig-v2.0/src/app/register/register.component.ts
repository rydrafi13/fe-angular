import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RegisterService } from '../services/register/register.service';
import { LoginService } from '../services/login/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import {FormControl,FormBuilder, FormGroup, Validators, ReactiveFormsModule} from '@angular/forms';
import { MemberService } from '../services/member/member.service';
import { MatIconModule } from '@angular/material/icon';
import { DatePipe } from '@angular/common';
import {MatDatepickerModule} from '@angular/material/datepicker';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  email = new FormControl('', [Validators.required, Validators.email]);
  errorMessage
  hide = true;
  showLoading = false;
  checked;
  showTnc = false;
  
  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }
  @ViewChild('DOBField', {static: false}) DOBField: ElementRef;
  
  public form : any = {
  full_name: "",
  email: "",
  cell_phone: "",
  password: "",
  gender: "",
  dob: ""
  }

  data: any = [];

  day: string = "";
  month: string = "";
  year: string = "";

  dayArray: any=[];
  monthArray: any=[];
  yearArray: any=[];

  constructor(
    public registerService: RegisterService,
    public icon : MatIconModule,
    public memberService: MemberService,
    public loginService: LoginService,
    public router: Router,
    private route: ActivatedRoute, 
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.dobCount();
    this.registerForm = new FormGroup({
      full_name :  new FormControl('', [Validators.required, Validators.maxLength(255)]),
      email :  new FormControl('', [Validators.required, Validators.email]),
      cell_phone : new FormControl('', [Validators.required]),
      password :  new FormControl('', [Validators.required, Validators.minLength(8)]),
      gender : new FormControl('', [Validators.required]),
      dob : new FormControl('', [Validators.required]),
      checked : new FormControl('', [Validators.requiredTrue]),
    })
  }

  // toggleDOBField(type) {
  //   this.DOBField.nativeElement.type = type;
  // }

  dobCount(){
    for(let i = 1; i <= 31; i++){
      if(i<10){
        this.dayArray.push({value:'0'+i});
      }
      else{
        this.dayArray.push({value:i});
      }
    }
    for(let i = 1; i <= 12; i++){
      if(i<10){
        this.monthArray.push({value:'0'+i});
      }
      else{
        this.monthArray.push({value:i});
      }
    }
    for(let i = 1940; i <= 2015; i++){
      this.yearArray.push({value:i});
    }
  }
  
  async register() {
      this.showLoading = true;
      console.log("Before",this.form);
      let data = this.form
      this.form.dob = new DatePipe('en').transform(this.form.dob, 'yyyy-MM-dd');
      // this.form.dob = this.year+"-"+this.month+"-"+this.day;
      console.log('date', this.form.dob)
      // let cellPhone = this.form.cell_phone;
      // String(cellPhone);
      console.log("Before Num",typeof this.form.cell_phone)
      let cell = this.form.cell_phone.toString()
      this.form.cell_phone = cell
      // console.log("TEST" typeof this.form.cell_phone)
      // this.form.cell_phone = toString();
      // form.value.push(this.dob)
      console.log("After Num",typeof this.form.cell_phone)
      console.log(this.form.dob)
      console.log("After",this.form)
      
      let result

      // if ( !this.checked){
      //   result.error = "Please indicate that you have read and agree to the Terms and Conditions and Privacy Policy";
      // }
      if (this.registerForm.invalid  ) {
        console.log('ERROR BRAY');
        this.errorMessage = "Please fill the form and indicate that you have read and agree to the Terms and Conditions"
        this.showLoading =false;
      }else {

      try{
        // console.log('kesini');
        // this.errorMessage = false;
        result = await this.registerService.registerOwner(data);
        data = result;
      
        if (!result.error) { 
          var login_data = {
            "email": this.form.email,
            "password": this.form.password
          }
  
          await this.autoLogin(login_data);
          this.showLoading = false;
        }
        else{
          this.errorMessage = result.error;
          this.showLoading = false;
        }

      }catch(e){
        
        this.errorMessage = ((<Error>e).message)
        this.showLoading = false
      }
    
    
    }
      
    
  }

  async autoLogin(login_data) {
    console.log(login_data);
    
    let result = await this.loginService.memberLogin(login_data);
    console.log('here')
    if (!result.error) {
      console.log(result);
      localStorage.setItem('isLoggedin', 'true');
      localStorage.setItem('tokenlogin', result.result.token);

      // this.router.navigate(['/merchant-portal/profile'])
      this.router.navigate(['/register/open-merchant'])

    }
  }

  loginClicked(){
    this.router.navigate(['/login-merchant'])    
  }

  openTnc(){
    console.log("test")
    this.showTnc = true;
    console.log(this.showTnc)
  }

  closeTnc(){
    this.showTnc = false;
  }

}
