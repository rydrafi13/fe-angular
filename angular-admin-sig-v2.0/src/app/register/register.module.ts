import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../layout/modules/bs-component/bs-component.module';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import {MatButtonModule} from '@angular/material/button';
import { MatFormFieldModule, MatFormFieldControl, MatFormField } from '@angular/material/form-field';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { OpenMerchantComponent } from './open-merchant/open-merchant.component';



@NgModule({
  declarations: [RegisterComponent, OpenMerchantComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatButtonModule,
    MatFormFieldModule,
    // MatFormField
    MatListModule,
    CKEditorModule,
             // <----- this module will be deprecated in the future version.
    MatDatepickerModule,        // <----- import(must)
    MatNativeDateModule,        // <----- import for date formating(optional)
    
  ],
})
export class RegisterModule { }
