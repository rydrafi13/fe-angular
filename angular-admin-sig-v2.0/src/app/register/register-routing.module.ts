import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register.component';
import { OpenMerchantComponent} from './open-merchant/open-merchant.component'


const routes: Routes = [
  {
    path: '',
    component: RegisterComponent
  },
  {
    path:'open-merchant',
    component: OpenMerchantComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
