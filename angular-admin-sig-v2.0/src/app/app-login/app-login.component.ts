import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import * as content from '../../assets/json/content.json';
// import { LoginService } from '../services/login/login.service';
// import { errorConvertToMessage } from '../../environments/environment';
// import { setTime } from 'ngx-bootstrap/chronos/utils/date-setters';
// import { PermissionObserver } from '../services/observerable/permission-observer';

@Component({
    selector: 'app-login',
    templateUrl: './app-login.component.html',
    styleUrls: ['./app-login.component.scss'],
    animations: [routerTransition()]
})
export class AppLoginComponent implements OnInit {
    public name: string = "";
    public menuOptions : any = (content as any).default;
    Login: any = [];
    service: any;
    loginOpened = false;
    imageLoginData = [
        {img: 'url("/assets/images/malte-wingen-PDX_a_82obo-unsplash-mini.jpg")', text: "The Amazing way to start anything is Today."},
        {img: 'url("/assets/images/alexander-rotker-l8p1aWZqHvE-unsplash.jpg")',    text: "Keep your eyes on the stars."},
        {img: 'url("/assets/images/curology-sR1oAhAT_Uw-unsplash.jpg")',        text: "Your Journey is never ending."},
        {img: 'url("/assets/images/vincent-branciforti-mGh2rjPgUyA-unsplash.jpg")',     text: "Not Impossible<br/> But I'm-possible."},
        {img: 'url("/assets/images/nick-van-den-berg-2gaNlcD75sk-unsplash.jpg")',         text: "Everyhting is not as easy as they said."},
       
    ]
    currentPermission;
    choosenImage;
    errorLabel : any = false;
    email:any;
    password:any;
    form;
    searchForm:any = "";

    // menuOptions:any = [
    //     {
    //         "img":"assets/images/Member.png",
    //         "title": "Admin Management",
    //         "appLabel":"admin"
    //     },
    //     {
    //         "img":"assets/images/logo-dynamix-vector.png",
    //         "title": "PROGRAM DYNAMIX EXTRA POIN 2020",
    //         "appLabel":"dynamix_sbi"
    //     },
    //     {
    //         "img":"assets/images/mci-logo-black.png",
    //         "title": "MCI",
    //         "appLabel":"retail_mci"
    //     },
    //     {
    //         "img":"assets/images/logo-tum.jpeg",
    //         "title": "Timur Usaha Mandiri",
    //         "appLabel":"retail_tum"
    //     },
    //     {
    //         "img":"assets/images/logo-home-credit.jpeg",
    //         "title": "Home Credit",
    //         "appLabel":"retail_home_credit"
    //     },
    //     {
    //         "img":"assets/images/logo-icbc.jpeg",
    //         "title": "ICBC",
    //         "appLabel":"retail_icbc"
    //     },
    //     {
    //         "img":"assets/images/logo-jne.jpg",
    //         "title": "JNE",
    //         "appLabel":"retail_jne"
    //     },
    //     {
    //         "img":"assets/images/logo-mondelez.png",
    //         "title": "Mondelez",
    //         "appLabel":"retail_mondelez"
    //     },
    //     {
    //         "img":"assets/images/logo-ngk.jpg",
    //         "title": "Ngk Spark Plugs",
    //         "appLabel":"retail_ngk_busi"
    //     },
    //     {
    //         "img":"assets/images/logo-uob.png",
    //         "title": "UOB",
    //         "appLabel":"retail_uob"
    //     },
    // ];

    menuSearch:any;

     getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
      }
    
    searchMenu() {
        let _this = this;
        this.menuSearch = this.menuOptions.filter(function(element) {
            return element.title.toLowerCase().includes(_this.searchForm.toLowerCase());
        });
    }

    constructor(
        // public loginService:LoginService, 
        public router: Router,
        // private content : Content,
		// private permission: PermissionObserver,
    ) {
        // this.permission.currentPermission.subscribe((val) => {
		// 	this.currentPermission = val;
		// });
       
        // this.choosenImage = this.imageLoginData[this.getRandomInt(0,4)];
        // console.log("CHOOSEN IMAGE", this.choosenImage);
        // let form_add     : any = [
        //     { label:"Email",  type: "text",   data_binding: 'email'  },
        //     { label:"Password",  type: "password",  data_binding: 'password'  }
        //   ];
    }

    ngOnInit() {
        this.firstLoad();
    }

    async firstLoad() {
        localStorage.removeItem('programName');
        this.menuSearch = this.menuOptions;
        // console.warn("current permission", this.currentPermission)
    }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    //   APP LOGIN
    toMenu(appLabel) {
        // console.warn("enter program", this.router);
        localStorage.setItem('programName', appLabel);
        if(appLabel == "admin") {
            this.router.navigateByUrl('/admin-management/admin');
        } else {
            this.router.navigateByUrl('/administrator/orderhistoryallhistoryadmin');
        }
        // console.warn("after", this.router);
    }

    //   APP LOGIN
    // toProgramRetailer1() {
    //     // console.warn("enter program", this.router);
    //     localStorage.setItem('programName', 'dynamix');
    //     this.router.navigateByUrl('/administrator/orderhistoryallhistoryadmin');
    //     // console.warn("after", this.router);
    // }

    // toProgramMCI() {
    //     // console.warn("enter program", this.router);
    //     localStorage.setItem('programName', 'MCI');
    //     this.router.navigateByUrl('/administrator/orderhistoryallhistoryadmin');
    //     // console.warn("after", this.router);
    // }

    // toAdminManagement() {
    //     this.router.navigateByUrl('/admin-management/admin');
    //     // console.warn("navigate 2", this.router)
    // }
}