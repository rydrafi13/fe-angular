import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DevmodeComponent } from './devmode.component';

const routes: Routes = [
  {path: '', component: DevmodeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevmodeRoutingModule { }
