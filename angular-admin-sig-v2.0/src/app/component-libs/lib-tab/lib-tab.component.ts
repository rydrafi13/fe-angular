import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-lib-tab',
  templateUrl: './lib-tab.component.html',
  styleUrls: ['./lib-tab.component.scss']
})
export class LibTabComponent implements OnInit {

  @Input() tabList:any[];
  @Input() onTabClicked:Function;
  tabs:any[] = []
  constructor() { }

  ngOnInit() {
    
  }

  ngOnChanges(){
    if(this.tabList)
      this.tabList.forEach((val,index)=>{
        if(index == 0){
          this.tabs.push({active:1, val: val})
        }
        else{
          this.tabs.push({active:0, val: val})
        }
      })
      console.log("tab List here", this.tabs)

  }
  onTabWasClicked(index, tabVal){
    this.tabs.forEach((val, index)=>{
      val.active = 0
    })
    this.tabs[index].active = 1

    if(this.onTabClicked){
      this.onTabClicked(index, tabVal)
    }
    console.log("tab was clicked")
  }
}
