"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.FormBuilderTableComponent = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("../../../environments/environment");
// import { ConsoleReporter } from 'jasmine';
var FormBuilderTableComponent = /** @class */ (function () {
    function FormBuilderTableComponent(calendar, sanitizer, router, downloadService) {
        this.sanitizer = sanitizer;
        this.router = router;
        this.downloadService = downloadService;
        this.orderBy = [];
        this.form_input = {};
        this.onSearchActive = false;
        this.showLoading = false;
        this.form_detail = [];
        this.edit = false;
        this.list_data = [];
        this.columnCheck = false;
        this.pagesLimiter = [];
        this.pageLimits = 50;
        this.pageNumbering = [];
        this.setupSetPage = false;
        this.currentPage = 1;
        this.checkBox = [];
        this.activeCheckbox = false;
        this.asc = true;
        this.download_temp = {};
        this.toggler = {};
        this.order = {};
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
        // this.fromDate = calendar.getToday();
        // this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
        // console.log('HadfI', this.table_row);
        // console.log('HsdI', this.table_row);
        this.pagesLimiter = this.pageLimiter();
    }
    // checkboxFunction(cButton:any){
    //   if(this.checkBox.length ==0) return false;
    //   let getData = [];
    //   this.checkBox.forEach((el,i)=>{
    //       getData.push(this.table_data[i]);
    //   })
    //   cButton.func(getData);
    // }
    FormBuilderTableComponent.prototype.ngOnInit = function () {
        var _this_1 = this;
        // console.log("hasil tabel", this.table_data)
        if (this.tableFormat != undefined) {
            this.table_header = this.tableFormat.label_headers;
            this.table_title = this.tableFormat.title;
            this.row_id = this.tableFormat.row_primary_key;
            this.options = this.tableFormat.formOptions;
            if (this.total_page) {
                this.buildPagesNumbers(this.total_page);
            }
        }
        if (!this.options) {
            this.options = null;
        }
        if (!this.options.type) {
            this.options.type = 'row-text';
        }
        if (!this.options.image_data_property) {
            this.options.image_data_property = 'url';
        }
        this.table_header.forEach(function (a, index) {
            if (typeof _this_1.table_header[index] == 'string') {
                _this_1.table_header[index] = {
                    label: _this_1.table_header[index],
                    type: 'string'
                };
            }
            else {
                if (_this_1.table_header[index].type == 'list' || _this_1.table_header[index].type == 'list-escrow' || _this_1.table_header[index].type == 'list-escrow-status') {
                    _this_1.table_header[index].options.forEach(function (b, index_options) {
                        if (typeof _this_1.table_header[index].options[index_options] == 'string') {
                            _this_1.table_header[index].options[index_options] = {
                                label: _this_1.table_header[index].options[index_options],
                                value: _this_1.table_header[index].options[index_options]
                            };
                        }
                    });
                }
            }
            // this.form_input[a] = '';
        });
        this.showTableHead();
    };
    FormBuilderTableComponent.prototype.showTableHead = function () {
        var _this_1 = this;
        this.table_head = [];
        this.table_header.forEach(function (a, index) {
            // console.log('this.form_input[a.data_row_name]', a.data_row_name, a.type, typeof this.form_input[a.data_row_name], a.type == 'number' && this.form_input[a.data_row_name] == undefined);
            if (a.visible != false) {
                if (a.type == 'number' && _this_1.form_input[a.data_row_name] == undefined) {
                    var ob = { option: 'equal' };
                    _this_1.form_input[a.data_row_name] = ob;
                }
                else {
                    _this_1.form_input[a.data_row_name] = '';
                }
                _this_1.table_head.push(a);
                // console.log(' table head' ,this.table_head)
            }
        });
        // console.log('form_input', this.form_input);
    };
    FormBuilderTableComponent.prototype.openLabel = function () {
        this.router.navigate(['administrator/orderhistoryallhistoryadmin/receipt'], { queryParams: { id: this.detail._id } });
    };
    FormBuilderTableComponent.prototype.getValueofCheckbox = function () {
        var _this_1 = this;
        setTimeout(function () {
            var getData = [];
            _this_1.checkBox.forEach(function (el, i) {
                var data = _this_1.table_data[i];
                // console.log(this.activeCheckbox)
                if (!_this_1.activeCheckbox) {
                    getData.push(data._id);
                    // console.log(getData)
                }
                else {
                    var index = getData.indexOf(data._id);
                    if (index !== -1)
                        getData.splice(index, 1);
                }
            });
        }, 100);
    };
    FormBuilderTableComponent.prototype.checkedColumn = function () {
        var _this_1 = this;
        setTimeout(function () {
            _this_1.showTableHead();
        }, 500);
    };
    FormBuilderTableComponent.prototype.checkedAll = function () {
        var _this_1 = this;
        setTimeout(function () {
            _this_1.table_data.forEach(function (e, i) {
                if (_this_1.activeCheckbox) {
                    _this_1.checkBox[i] = _this_1.activeCheckbox;
                }
                else {
                    _this_1.checkBox = [];
                }
            });
        }, 100);
    };
    /* Date Picker Part */
    FormBuilderTableComponent.prototype.datePickerOnDateSelection = function (date, formName) {
        var _this = this.make_This(formName);
        if (!_this.fromDate && !_this.toDate) {
            _this.fromDate = date;
        }
        else if (_this.fromDate && !_this.toDate && this.datePickerDateAfter(date, _this.fromDate)) {
            _this.toDate = date;
        }
        else {
            _this.toDate = null;
            _this.fromDate = date;
        }
        _this.from = (_this.fromDate != undefined && _this.fromDate != null) ? this.convertToDateString(_this.fromDate) : '';
        _this.to = (_this.toDate != undefined && _this.toDate != null) ? this.convertToDateString(_this.toDate) : '';
    };
    FormBuilderTableComponent.prototype["typeof"] = function (object) {
        return typeof object;
    };
    FormBuilderTableComponent.prototype.make_This = function (formName) {
        if (typeof formName == 'object') {
            return formName;
        }
        if (this.form_input[formName] == undefined || this.form_input[formName] == '') {
            this.form_input[formName] = {};
        }
        return this.form_input[formName];
    };
    FormBuilderTableComponent.prototype.dateToggler = function (formName) {
        this.toggler[formName] = (typeof this.toggler == undefined) ? true : !this.toggler[formName];
        if (this.toggler[formName] == false && this.form_input[formName] !== '') {
            this.valuechange({}, false, false);
        }
        // console.log(this.form_input);
    };
    FormBuilderTableComponent.prototype.dateClear = function (event, dataInput, deleteInput) {
        this.form_input[dataInput] = '';
        this.valuechange(event, deleteInput);
    };
    FormBuilderTableComponent.prototype.datePickerOnDateIsInside = function (date, formName) {
        var _this = this.make_This(formName);
        return this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.toDate);
    };
    FormBuilderTableComponent.prototype.datePickerOnDateIsRange = function (date, formName) {
        var _this = this.make_This(formName);
        return this.datePickerDateEquals(date, _this.fromDate) || this.datePickerDateEquals(date, _this.toDate) || this.datePickerOnDateIsInside(date, _this) || this.datePickerOnDateIsHovered(date, _this);
    };
    FormBuilderTableComponent.prototype.datePickerOnDateIsHovered = function (date, formName) {
        var _this = this.make_This(formName);
        return _this.fromDate && !_this.toDate && _this.hoveredDate && this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.hoveredDate);
    };
    FormBuilderTableComponent.prototype.datePickerDateAfter = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '/' + datePrev.month + '/' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '/' + dateAfter.month + '/' + dateAfter.day);
        if (iToDate.getTime() < iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    FormBuilderTableComponent.prototype.convertToDateString = function (datePrev) {
        return datePrev.year + '/' + datePrev.month + '/' + datePrev.day;
    };
    FormBuilderTableComponent.prototype.datePickerDateEquals = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '/' + datePrev.month + '/' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '/' + dateAfter.month + '/' + dateAfter.day);
        if (iToDate.getTime() == iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    FormBuilderTableComponent.prototype.datePickerDateBefore = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '/' + datePrev.month + '/' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '/' + dateAfter.month + '/' + dateAfter.day);
        if (iToDate.getTime() > iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    /* end of datepicker component */
    FormBuilderTableComponent.prototype.ngOnChanges = function (changes) {
        console.log("table data", this.table_data);
        if (this.setupSetPage == false) {
            if (this.total_page > 0) {
                this.buildPagesNumbers(this.total_page);
                // console.log(this.total_page)
                this.setupSetPage = true;
            }
        }
        // console.log('HI', this.total_page);
        // console.log('changes', this.table_row);
    };
    FormBuilderTableComponent.prototype.toggleColoumnCheck = function () {
        this.columnCheck = !this.columnCheck;
    };
    FormBuilderTableComponent.prototype.onChangePage = function (pageNumber) {
        if (pageNumber <= 0) {
            pageNumber = 1;
        }
        if (pageNumber >= this.total_page) {
            pageNumber = this.total_page;
        }
        this.currentPage = pageNumber;
        // console.log(pageNumber, this.currentPage, this.total_page)
        this.valuechange({}, false, false);
    };
    FormBuilderTableComponent.prototype.pageLimitChanges = function (event) {
        // console.log('Test limiter', this.pageLimits);
        this.valuechange({}, false, false);
    };
    FormBuilderTableComponent.prototype.pageLimiter = function () {
        return [
            { id: '10', name: '10' },
            { id: '20', name: '20' },
            { id: '50', name: '50' },
            { id: '100', name: '100' }
        ];
    };
    FormBuilderTableComponent.prototype.isCurrentPage = function (tp) {
        if (tp == this.currentPage) {
            return 'current-page';
        }
        else
            return '';
    };
    FormBuilderTableComponent.prototype.pagination = function (c, m) {
        var current = c, last = m, delta = 2, left = current - delta, right = current + delta + 1, range = [], rangeWithDots = [], l;
        for (var i = 1; i <= last; i++) {
            if (i == 1 || i == last || i >= left && i < right) {
                range.push(i);
            }
        }
        for (var _i = 0, range_1 = range; _i < range_1.length; _i++) {
            var i = range_1[_i];
            if (l) {
                if (i - l === 2) {
                    rangeWithDots.push(l + 1);
                }
                else if (i - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(i);
            l = i;
        }
        return rangeWithDots;
    };
    FormBuilderTableComponent.prototype.buildPagesNumbers = function (totalPage) {
        this.pageNumbering = [];
        var maxNumber = 8;
        this.total_page = totalPage;
        this.pageNumbering = this.pagination(this.currentPage, totalPage);
    };
    FormBuilderTableComponent.prototype.orderChange = function (event, orderBy) {
        // var row_name = orderBy
        if (this.orderBy[orderBy] && this.asc == false) {
            this.orderBy[orderBy].asc = !this.orderBy[orderBy].asc;
            this.asc = true;
            this.order[orderBy] = 1;
        }
        else {
            this.asc = false;
            this.order = [];
            this.orderBy = [];
            this.orderBy[orderBy] = { asc: false };
            this.order[orderBy] = -1;
        }
        this.valuechange(event, orderBy);
    };
    FormBuilderTableComponent.prototype.stringToNumber = function (str) {
        if (typeof str == 'number') {
            str = str + '';
        }
        var s = str.toLowerCase().replace(/[^0-9]/g, '');
        return parseInt(s);
    };
    FormBuilderTableComponent.prototype.valuechange = function (event, input_name, download) {
        var _this_1 = this;
        var searchCallback = this.searchCallback;
        // console.log(searchCallback)
        if (this.onSearchActive == false) {
            this.onSearchActive = true;
            var myVar = setTimeout(function () { return __awaiter(_this_1, void 0, void 0, function () {
                var clearFormInput, filter, property, result, srcDownload;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            clearFormInput = JSON.stringify(this.form_input);
                            clearFormInput = JSON.parse(clearFormInput);
                            this.showLoading = true;
                            // console.log('ini clearforminput',clearFormInput)
                            if (clearFormInput.unique_amount) {
                                clearFormInput.unique_amount = this.stringToNumber(clearFormInput.unique_amount);
                            }
                            if (clearFormInput.total_price) {
                                clearFormInput.total_price = this.stringToNumber(clearFormInput.total_price);
                            }
                            if (!searchCallback) return [3 /*break*/, 2];
                            filter = {
                                search: clearFormInput,
                                order_by: this.order,
                                limit_per_page: this.pageLimits,
                                current_page: this.currentPage,
                                download: false
                            };
                            //  console.log(filter)
                            if (download != undefined && download == true) {
                                // console.log('here1')
                                filter.limit_per_page = 0;
                                filter = __assign(__assign({}, filter), { download: false });
                            }
                            for (property in filter.search) {
                                /** check for type string */
                                if (typeof filter.search[property] == 'string' && filter.search[property].trim() == '') {
                                    delete filter.search[property];
                                }
                                /** check for number */
                                if (typeof filter.search[property] == 'object' && filter.search[property].option) {
                                    if (filter.search[property].value == undefined && filter.search[property].from == undefined) {
                                        delete filter.search[property];
                                    }
                                    else if (filter.search[property].value != undefined) {
                                        if (filter.search[property].value.trim() == '') {
                                            delete filter.search[property];
                                        }
                                    }
                                    else if (filter.search[property].from && filter.search[property].from.trim() == '') {
                                        delete filter.search[property];
                                    }
                                }
                            }
                            return [4 /*yield*/, searchCallback[0][searchCallback[1]](filter)];
                        case 1:
                            result = _a.sent();
                            if (download != undefined && download == true) {
                                if (result.result.values) {
                                    // console.log('here')
                                    this.download_temp = result.result.values;
                                    this.downloadFile(this.download_temp);
                                }
                                else {
                                    this.download_temp = result.result;
                                    this.downloadFile(this.download_temp);
                                }
                                // console.log('here',this.download_temp)
                            }
                            if (result && filter.download != true && download != true) {
                                if (result.result.total_page) {
                                    this.buildPagesNumbers(result.result.total_page);
                                }
                                if (searchCallback[2]) {
                                    if (result.result.values) {
                                        searchCallback[2][this.options.result_var_name] = result.result.values;
                                    }
                                    else {
                                        searchCallback[2][this.options.result_var_name] = result.result;
                                    }
                                    /* for(let data of this.table_data) {
                                      if(this.list_data.includes(data.member_id)) {
                                        data.selected = true;
                                      }
                                    } */
                                }
                                else {
                                    if (result.result.values) {
                                        this.table_data = result.result.values;
                                    }
                                    else {
                                        this.table_data = result.result;
                                    }
                                    /* for(let data of this.table_data) {
                                      if(this.list_data.includes(data.member_id)) {
                                        data.selected = true;
                                      }
                                    } */
                                }
                            }
                            else if (result && filter.download == true) {
                                if (typeof result.result == 'string') {
                                    srcDownload = environment_1.getHost() + '.' + '/' + result.result;
                                    window.open(srcDownload, "_blank");
                                    // this.srcDownload = '';
                                    // setTimeout(() => {
                                    //   this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
                                    // }, 200);
                                }
                            }
                            _a.label = 2;
                        case 2:
                            this.onSearchActive = false;
                            this.showLoading = false;
                            return [2 /*return*/];
                    }
                });
            }); }, 2000);
        }
    };
    FormBuilderTableComponent.prototype.submit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.options.submit_function[0][this.options.submit_function[1]](this.list_data)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    FormBuilderTableComponent.prototype.toggleCheck = function (event, member_id) {
        if (event.target.checked) {
            if (!this.list_data.includes(member_id)) {
                this.list_data.push(member_id);
            }
        }
        else if (!event.target.checked) {
            if (this.list_data.includes(member_id)) {
                this.list_data.splice(this.list_data.indexOf(member_id), 1);
            }
        }
    };
    FormBuilderTableComponent.prototype.removeFromList = function (member_id) {
        this.list_data.splice(this.list_data.indexOf(member_id), 1);
        var index = this.table_data.findIndex(function (obj) { return obj.member_id == member_id; });
        if (index > -1) {
            this.table_data[index].selected = false;
        }
    };
    FormBuilderTableComponent.prototype.clickedRowTable = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.options.check_box) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.options.detail_function[0][this.options.detail_function[1]](data, rowData)];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    FormBuilderTableComponent.prototype.isString = function (data) {
        return typeof data == 'string';
    };
    FormBuilderTableComponent.prototype.include = function (data) {
        if (data != null) {
            var x = data.toString();
            return x.includes('-');
        }
        else {
            var y = '0';
            return y.includes('-');
        }
    };
    FormBuilderTableComponent.prototype.stripHtml = function (value) {
        if (typeof value == 'string')
            return value.replace(/<.*?>/g, ''); // replace tags
        return value;
    };
    FormBuilderTableComponent.prototype.onClickDownload = function ($event) {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.valuechange($event, false, true)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    FormBuilderTableComponent.prototype.downloadFile = function (data, filename) {
        var _this_1 = this;
        if (filename === void 0) { filename = 'data'; }
        // console.log('data', this.table_header)
        var label = [];
        var key = [];
        this.table_header.forEach(function (a, index) {
            label.push(_this_1.table_header[index].label);
            key.push(_this_1.table_header[index].data_row_name);
        });
        // console.log(' label', label)
        // console.log(' key', key)
        var keys = Object.keys(data[0]);
        var csvData = this.ConvertToCSV(data, label, key);
        // console.log(csvData)
        var blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
        var dwldLink = document.createElement("a");
        var url = URL.createObjectURL(blob);
        var isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) { //if Safari open in new window to save file with random filename.
            dwldLink.setAttribute("target", "_blank");
        }
        dwldLink.setAttribute("href", url);
        dwldLink.setAttribute("download", filename + ".csv");
        dwldLink.style.visibility = "hidden";
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);
    };
    FormBuilderTableComponent.prototype.ConvertToCSV = function (objArray, labelHeader, headerList) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
        var row = 'No;';
        for (var index in labelHeader) {
            row += labelHeader[index] + ';';
        }
        row = row.slice(0, -1);
        str += row + '\r\n';
        for (var i = 0; i < array.length; i++) {
            var line = (i + 1) + '';
            for (var index in headerList) {
                var head = headerList[index];
                var headtype = this.table_head[index].type;
                // console.log('head',headtype)
                if ((array[i][head]) instanceof Array && (array[i][head]) != null && head == 'products') {
                    var line2 = '';
                    if (headtype == 'productcode') {
                        // console.log((array[i][head]).length)
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_code + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].product_code + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                    }
                    else if (headtype == 'productdesc') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].description + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].description + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productcat') {
                        if ((array[i][head])[0].categories) {
                            if ((array[i][head]).length == 1) {
                                line2 += (array[i][head])[0].categories + ' ';
                            }
                            else {
                                for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                    line2 += (array[i][head])[j].categories + ' , ';
                                }
                            }
                        }
                        if ((array[i][head])[0].category) {
                            if ((array[i][head]).length == 1) {
                                line2 += (array[i][head])[0].category + ' ';
                            }
                            else {
                                for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                    line2 += (array[i][head])[j].category + ' , ';
                                }
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productmerc') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].merchant_username + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].merchant_username + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productqty') {
                        var qty = 0;
                        if ((array[i][head]).length == 1) {
                            qty += 1;
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                qty += this.stringToNumber((array[i][head])[j].quantity);
                            }
                        }
                        line2 = qty.toString();
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productdim') {
                        // for (let j = 0; j < (array[i][head]).length-1; j++) {      
                        line2 += 'height:' + (array[i][head])[0].dimensions.height + ' length :' + (array[i][head])[0].dimensions.length + ' width:' + (array[i][head])[0].dimensions.width + ', ';
                        // line2 += (array[i][head])[j].product_name +' '+ '(' + (array[i][head])[j].fixed_price + ' ) ' +','
                        // }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productunit') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ' + '(' + (array[i][head])[0].fixed_price + ' )';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].product_name + ' ' + '(' + (array[i][head])[j].fixed_price + ' ) ' + ',';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'producttotal') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ' + '(' + (array[i][head])[0].total_product_price + ' )';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[0].product_name + ' ' + '(' + (array[i][head])[j].total_product_price + ') ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                    }
                    else if (headtype == 'product') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ' + '(' + ' ' + (array[i][head])[0].total_product_price + ' ' + ')' + ' ' + (array[i][head])[0].quantity + ' ' + 'X';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].product_name + ' ' + '(' + ' ' + (array[i][head])[j].total_product_price + ' ' + ')' + ' ' + (array[i][head])[j].quantity + ' ' + 'X';
                            }
                        }
                        line += ';' + ' ' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                    }
                    //  console.log('head2',(array[i][head]).length)
                    // line += ';' +' ' + line2 + ' '
                }
                else if (head == 'description') {
                    var line2 = JSON.stringify(array[i][head]).replace(/;/g, ' ');
                    line += ';' + line2;
                }
                else {
                    line += ';' + JSON.stringify(array[i][head]);
                }
            }
            str += line + '\r\n';
        }
        return str;
    };
    FormBuilderTableComponent.prototype.product = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product = product + params[i].product_name + ' ' + '(' + ' ' + params[i].total_product_price + ' ' + ')' + ' ' + params[i].quantity + ' ' + 'X' + ',';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productcode = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += params[i].product_code + ',';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productdesc = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += params[i].description + ', ';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productdim = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += 'height:' + params[i].dimensions.height + ' length :' + params[i].dimensions.length + ' width:' + params[i].dimensions.width + ', ';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productcat = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].categories) {
                product += params[i].categories + ', ';
            }
            if (params[i].category) {
                product += params[i].category + ', ';
            }
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productmerc = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += params[i].merchant_username + ',';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productqty = function (params) {
        var product = 0;
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += this.stringToNumber(params[i].quantity);
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productunit = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += params[i].product_name + '(' + params[i].fixed_price + ')' + ',';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.producttotal = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product = product + params[i].product_name + ' ' + '(' + ' ' + params[i].total_product_price + ' ' + ')' + ',';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.clickToDownloadReport = function () {
        //reportTable ID
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById('reportTable');
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        // Specify file name
        var filename = 'excel_data.xls';
        var downloadLink = document.createElement("a");
        document.body.appendChild(downloadLink);
        if (navigator.msSaveOrOpenBlob) {
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob(blob, filename);
        }
        else {
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
            // Setting the file name
            downloadLink.download = filename;
            //triggering the function
            downloadLink.click();
        }
    };
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "table_data");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "total_page");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "table_row");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "table_header");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "table_footer");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "table_title");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "searchCallback");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "row_id");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "options");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "table_detail");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "image_data_property");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "tableFormat");
    __decorate([
        core_1.Input()
    ], FormBuilderTableComponent.prototype, "detail");
    FormBuilderTableComponent = __decorate([
        core_1.Component({
            selector: 'app-form-builder-table',
            templateUrl: './form-builder-table.component.html',
            styleUrls: ['./form-builder-table.component.scss']
        })
    ], FormBuilderTableComponent);
    return FormBuilderTableComponent;
}());
exports.FormBuilderTableComponent = FormBuilderTableComponent;
