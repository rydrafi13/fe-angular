import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilderTableComponent } from './form-builder-table.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDatepicker, NgbDatepickerModule, NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../../layout/modules/bs-component/bs-component.module';
import { FormInputComponent } from '../form-input/form-input.component';
import { FormSelectComponent } from '../form-select/form-select.component';
// import { DatePickerComponent, AlertComponent } from 'ngx-bootstrap';
import { BsComponentRoutingModule } from '../../layout/modules/bs-component/bs-component-routing.module';
import { ButtonsComponent } from '../../layout/modules/bs-component/components';
import { PageHeaderModule } from '../../shared';
import {MatCheckboxModule} from '@angular/material/checkbox';
// import { BastComponent } from '../../layout/modules/admin-stock/bast/bast.component';
import { BastComponent } from '../../layout/modules/admin-bast/bast/bast.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    MatCheckboxModule,
    PdfViewerModule,
  ],
  declarations: [
    FormBuilderTableComponent,
     FormInputComponent, 
     FormSelectComponent,BastComponent],

  exports: [FormBuilderTableComponent, FormInputComponent, FormSelectComponent]
})
export class FormBuilderTableModule { }
