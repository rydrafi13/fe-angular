import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { routePath } from './routing.path';
import { PortalGuard } from '../shared/guard/portal.guard';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: routePath,
        canActivate: [PortalGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
