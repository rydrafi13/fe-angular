import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminStockComponent } from './admin-stock.component'
// import { BastComponent} from './bast/bast.component'


const routes: Routes = [
  {
    path: '', component: AdminStockComponent
},
// {
//   path:'bast', component: BastComponent
// }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminStockRoutingModule { }
