import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminAllBalanceComponent} from './admin-all-balance.component'


const routes: Routes = [
  {
    path: '', component: AdminAllBalanceComponent,

}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminAllBalanceRoutingModule { }
