import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { EscrowTransactionService } from '../../../services/escrow-transaction/escrow-transaction.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-admin-all-balance',
  templateUrl: './admin-all-balance.component.html',
  styleUrls: ['./admin-all-balance.component.scss']
})
export class AdminAllBalanceComponent implements OnInit {

  EscrowData: any = [];
  Pointstransaction: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Escrow Report Page',
                                  label_headers   : [
                                    {label: 'ID', visible: true, type: 'date', data_row_name: '_id'},
                                    {label: 'Merchant Name', visible: true, type: 'string', data_row_name: 'merchant_name'},
                                    {label: 'Email' , visible: true, type: 'string', data_row_name: 'email'},
                                    {label: 'Balance', visible: true, type: 'string', data_row_name: 'balance'},
                                    {label: 'Merchant Name', visible: true, type: 'string', data_row_name: 'merchant_name'},
                                    
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'EscrowData',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;
  page = 1;
  pageSize = 50;
  pointstransactionDetail: any = false;
  service: any;
  pages: any;
  currentPage= 1;
  pageNumbering: any[];
  total_page: any;
  orderBy: any;
  searchCallback: any;
  onSearchActive: boolean;
  showLoading: boolean;
  options: any;
  table_data: any;
  merchantUsername: any;
  pageLimits: any;

  constructor(public EscrowTransactionService:EscrowTransactionService,  private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    // console.log(' admin payment')
    try{
      this.service    = this.EscrowTransactionService;
      localStorage.setItem('merchantescrow',this.merchantUsername)
      let result: any = await this.EscrowTransactionService.getBalanceAllMerchant();
      console.log("result", result)
      this.totalPage  = result.result.total_page
      this.pages = result.result.total_page
      console.log(" pages",this.pages)
      this.EscrowData = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
  public async callDetail(pointstransaction_id){
    // try{
    //   let result: any;
    //   this.service    = this.EscrowTransactionService;
    //   result          = await this.EscrowTransactionService.detailEscrowtransaction(pointstransaction_id);
    //   console.log(result);

    //   this.pointstransactionDetail = result.result[0];
    //   console.log('ini',this.pointstransactionDetail)
      
    //   this.router.navigate(['administrator/escrow-transaction/detail'],  {queryParams: {id: pointstransaction_id }})
      
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }

}