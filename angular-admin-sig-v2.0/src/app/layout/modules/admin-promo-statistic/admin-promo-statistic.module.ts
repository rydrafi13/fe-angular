import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminPromoStatisticRoutingModule } from './admin-promo-statistic-routing.module';
import { AdminPromoStatisticComponent } from './admin-promo-statistic.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';


@NgModule({
 
  imports: [
    CommonModule,
    AdminPromoStatisticRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
  declarations: [AdminPromoStatisticComponent],
})
export class AdminPromoStatisticModule { }
