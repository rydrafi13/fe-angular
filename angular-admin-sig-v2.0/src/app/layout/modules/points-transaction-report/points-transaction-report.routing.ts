import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PointsTransationReportComponent } from './points-transaction-report.component';
import { PointsTransationReportAddComponent } from './add/points-transaction-report.add.component';
import { PointsTransactionReportDetailComponent } from './detail/points-transaction-report.detail.component';

const routes: Routes = [
  {
      path: '', component: PointsTransationReportComponent,

  },
  {
      path:'add', component: PointsTransationReportAddComponent
  },
  {
    path:'detail', component: PointsTransactionReportDetailComponent
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PointsTransationReportRoutingModule { }
