import { Component, OnInit, Input } from '@angular/core';
import { del } from 'selenium-webdriver/http';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../../router.animations';
import { EVoucherService } from '../../../../services/e-voucher/e-voucher.service';
import { PointsTransactionService } from '../../../../services/points-transaction/points-transaction.service';
import {FormOptions, TableFormat} from '../../../../object-interface/common.object';

@Component({
  selector: 'app-points-transaction-report-detail',
  templateUrl: './points-transaction-report.detail.component.html',
  styleUrls: ['./points-transaction-report.detail.component.scss'],
  animations: [routerTransition()]
})

export class PointsTransactionReportDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  pointStockDetail;
  processNumber;
  processNow: any = false;
  declineNow: any = false;
  service;
  edit: boolean = false;
  errorLabel: any = false;
  constructor(
    public evoucherService: EVoucherService, 
    private route: ActivatedRoute,
    private router: Router,
    public pointsTransactionService:PointsTransactionService) {
  }

  points       : any = [];
  row_id        : any = "process_number";
  totalPage     : 0;
  errorMessage  : any = false;
  pointDetail  : any = false;
  tableFormat   : TableFormat = {
    title           : 'Point Transaction Report',
    label_headers   : [
                  {label: 'Created Date',     visible: true, type: 'date', data_row_name: 'created_date'},
                  {label: 'Process Number', visible: true, type: 'string', data_row_name: 'process_number'},
                  {label: 'Process ID', visible: true, type: 'string', data_row_name: 'process_id'},
                  {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username'},
                  {label: 'Nama Pelanggan', visible: true, type: 'string', data_row_name: 'full_name'},
                  {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                  {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                  {label: 'Remarks', visible: true, type: 'string', data_row_name: 'remarks'},
                  {label: 'Process Type', visible: true, type: 'string', data_row_name: 'process_type'},
                  {label: 'Status', visible: true, type: 'string', data_row_name: 'status'},
                  {label: 'Transaction Month',     visible: true, type: 'string', data_row_name: 'transaction_month'},

                ],
    row_primary_key : '_id',
    formOptions     : {
                    // addForm   : true,
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'points',
                    detail_function : [this, 'callDetail'] 
                    }
                  };

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    this.pointStockDetail = this.detail;
    this.processNumber = this.detail.process_number;

    try {
      this.service    = this.pointsTransactionService;
      let result = await this.pointsTransactionService.searchPointstransactionProcessReport(this.processNumber);
      console.warn("result??", result);
      this.totalPage = result.total_page;
      this.points = result.values;
    } catch(e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }


    // try {
    //   const result : any= await this.evoucherService.detailEvoucherStock(this.processNumber);
    //   console.warn("result detail stock", result)
    //   if(result.length > 0){
    //     if(result[0].process_number == this.detail.process_number){
    //       this.pointStockDetail = result[0];
    //     }
    //   }
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  backToHere() {
    // this.evoucherStockDetail = false;
    this.back[1](this.back[0]);
    // this.firstLoad();
  }

  isProcessNow() {
    this.processNow = !this.processNow;
  }

  isDeclineNow() {
    this.declineNow = !this.declineNow;
  }

  async processPoint(process_number, status) {
    try {
      let dataProcess = 
      {
        "process_number":process_number,
        "status":status
      }
      let result: any = await this.pointsTransactionService.processPoint(dataProcess);
      console.warn("result process", result)
      if (result) {
        Swal.fire({
          title: 'Success',
          text: 'Point is Processed',
          icon: 'success',
          confirmButtonText: 'Ok',
        }).then((result) => {
          if(result.isConfirmed){
            this.backToHere();
          }
        });
      }
    } catch(e) {
      console.warn("error appove", e);
      // alert("Error");

      this.errorLabel = ((<Error>e).message);
      // conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
  }

  // private async loadDetail(historyID: string){
  //   try {
  //     this.service = this.evoucherService;
  //     let result: any = await this.evoucherService.detailEvoucherStock(historyID);
  //     this.pointStockDetail = result.result[0];
  //     console.log(this.pointStockDetail);
  //   } catch (error) {
      
  //   }
  // }



}
