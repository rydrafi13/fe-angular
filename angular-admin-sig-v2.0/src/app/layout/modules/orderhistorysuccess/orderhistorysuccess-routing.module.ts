import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderhistorysuccessComponent } from './orderhistorysuccess.component';
import { OrderhistorysuccessDetailComponent } from './detail/orderhistorysuccess.detail.component';
import { OrderhistorysuccessEditComponent } from './edit/orderhistorysuccess.edit.component';

// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';


const routes: Routes = [
  {
      path: '', component: OrderhistorysuccessComponent
  },
  // {
  //   path:'detail', component: OrderhistorysuccessDetailComponent
  // },
  // {
  //   path:'edit', component: OrderhistorysummaryEditComponent
  // }

  {
    path:'edit', component: OrderhistorysuccessDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistorysuccessRoutingModule { }
