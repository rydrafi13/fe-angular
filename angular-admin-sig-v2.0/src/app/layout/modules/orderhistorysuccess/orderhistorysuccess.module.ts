import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistorysuccessRoutingModule } from './orderhistorysuccess-routing.module';
import { OrderhistorysuccessComponent } from './orderhistorysuccess.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistorysuccessDetailComponent } from './detail/orderhistorysuccess.detail.component';
import { OrderhistorysuccessEditComponent } from './edit/orderhistorysuccess.edit.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';


@NgModule({
  imports: [
    CommonModule, 
    OrderhistorysuccessRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    NgxBarcodeModule,
    MatButtonModule,
    MatRadioModule
  ],
  declarations: [
    OrderhistorysuccessComponent,
    OrderhistorysuccessDetailComponent,
    OrderhistorysuccessEditComponent
  ],
  exports:[
    OrderhistorysuccessComponent,
    OrderhistorysuccessDetailComponent,
    OrderhistorysuccessEditComponent
  ]
})
export class OrderhistorysuccessModule { }
