import { Component, OnInit, Input } from '@angular/core';
import { del } from 'selenium-webdriver/http';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import {FormOptions, TableFormat} from '../../../../object-interface/common.object';

@Component({
  selector: 'app-evoucher-sales-detail',
  templateUrl: './evoucher-sales.detail.component.html',
  styleUrls: ['./evoucher-sales.detail.component.scss'],
  animations: [routerTransition()]
})

export class EvoucherSalesDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  salesRedemptionDetail;
  orderID;
  processNow: any = false;
  cancelNow: any = false;
  declineNow: any = false;
  declineCancel: any = false;
  cancelRemarks: any = "";
  service;
  edit: boolean = false;
  errorLabel: any = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public OrderhistoryService:OrderhistoryService) {
  }

  salesRedemption       : any = [];
  row_id        : any = "order_id";
  totalPage     : 0;
  errorMessage  : any = false;
  showLoadingSpinner: any = false;
  processCount: any = 0;
  processTotal:any = 0;
  redemptionDetail  : any = false;
  tableFormat   : TableFormat = {
    title           : 'All Redemptions in the Order',
    label_headers   : [
      // {label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date'},
      // {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
      // {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
      // {label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
      // {label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail'},
      {label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail'},
      // {label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail'},
      // {label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
      // {label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail'},
      {label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail'},
      {label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products'},
      {label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products'},
      {label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products'},
      {label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total'},
      {label: 'Status Order', visible: true, type: 'string', data_row_name: 'status'},
      {label: 'Status Pengiriman', visible: true, type: 'last_shipping_info', data_row_name: 'delivery_detail'},
      {label: 'Tanggal Kirim', visible: true, type: 'delivery_date', data_row_name: 'delivery_detail'},
      {label: 'Tanggal Terima', visible: true, type: 'delivered_date', data_row_name: 'delivery_detail'},
      {label: 'Nama Penerima Langsung', visible: true, type: 'receiver_name_sap', data_row_name: 'delivery_detail'},
      {label: 'Status Penerima', visible: true, type: 'relation_name', data_row_name: 'delivery_detail'},
      {label: 'Tanggal Cancel', visible: true, type: 'date', data_row_name: 'cancel_date'},
      {label: 'Cancel Remarks', visible: true, type: 'string', data_row_name: 'cancel_remarks'},
    ],
    row_primary_key : '_id',
    formOptions     : {
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'salesRedemption',
                    detail_function : [this, 'callDetail'],
                    cancelProduct: true
                    }
                  };

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    this.salesRedemptionDetail = this.detail;
    console.warn("detail", this.salesRedemptionDetail);
    this.orderID = this.detail.order_id;

    try {
      this.service = this.OrderhistoryService;
      let result = await this.OrderhistoryService.searchAllRedemptionPerOrder(this.orderID);
      this.totalPage = result.total_page;
      this.salesRedemption = result.values;

      for (let i = 0; i < this.salesRedemption.length; i++) {
        let cancel_detail = [];
        if (this.salesRedemption[i].products.cancel_detail) {
          cancel_detail.push(this.salesRedemption[i].products.cancel_detail);
          for (let j = 0; j < cancel_detail.length; j++) {
            this.salesRedemption[i].cancel_remarks = cancel_detail[j].remarks;
            this.salesRedemption[i].cancel_date = cancel_detail[j].cancel_date;
            // console.warn("result after??", this.salesRedemption);
          }
        }
      }
    } catch(e) {
      console.warn("ERROR");
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }


    // try {
    //   const result : any= await this.evoucherService.detailEvoucherStock(this.orderID);
    //   console.warn("result detail stock", result)
    //   if(result.length > 0){
    //     if(result[0].process_number == this.detail.process_number){
    //       this.salesRedemptionDetail = result[0];
    //     }
    //   }
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  backToHere() {
    // this.evoucherStockDetail = false;
    this.back[1](this.back[0]);
    // this.firstLoad();
  }

  isProcessNow() {
    this.processNow = !this.processNow;
  }

  isCancelNow() {
    this.cancelNow = !this.cancelNow;
  }

  isDeclineCancel() {
    this.declineCancel = !this.declineCancel;
  }

  isDeclineNow() {
    this.declineNow = !this.declineNow;
  }

  async completeOrder(orderID) {
    try {
      let payloadData = {
				"order_id":[
					orderID
				]
			}
			const result = await this.OrderhistoryService.updateStatusOrderCompleted(payloadData,this);

      console.warn("result process", result)
      if (result) {
        Swal.fire({
          title: 'Success',
          text: 'Order is Completed',
          icon: 'success',
          confirmButtonText: 'Ok',
        }).then((result) => {
          if(result.isConfirmed){
            this.backToHere();
          }
        });
      }
    } catch(e) {
      console.warn("error appove", e);
      this.errorLabel = ((<Error>e).message);

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
  }

  public async processSelectedProducts(obj, products){
    // console.warn("products??", products);
    // console.warn("obj??", obj);
    if(products.length > 0){
      Swal.fire({
        title: 'Konfirmasi',
        text: 'Alasan Pembatalan Produk : ',
        // icon: 'success',
        confirmButtonText: 'Ok',
        cancelButtonText:"Kembali",
        showCancelButton:true,
  			input: 'text',
        }).then(async (result) => {
        if(result.isConfirmed){
          this.showLoadingSpinner = true;
          this.processTotal = products.length;
          this.processCount = 0;
          
          const payload = {
            "order_id":this.orderID,
            "product_sku":products,
            "remarks":result.value
          }
          try {
            await obj.OrderhistoryService.cancelDigitalProductOrder(payload).then(() => {
              this.processCount ++;
              
              if(this.processCount >= products.length) { 
                this.showLoadingSpinner = false;

                Swal.fire({
                  title: 'Success',
                  text: 'Produk terpilih telah dibatalkan',
                  icon: 'success',
                  confirmButtonText: 'Ok',
                  showCancelButton:false
                  }).then(async (result) => {
                    if(result.isConfirmed){
                      console.log("confirmed");
                      obj.firstLoad();
                    }
                });
              }
            });
          } catch (e) {
            this.errorLabel = ((<Error>e).message);//conversion to Error type
            this.showLoadingSpinner = false;
            let message = this.errorLabel;
            if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
              message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
            }
            Swal.fire({
              icon: 'error',
              title: message,
            });
          };
        }
      });
    }
  }

}
