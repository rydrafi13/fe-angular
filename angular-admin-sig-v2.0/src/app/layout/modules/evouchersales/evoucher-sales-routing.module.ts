import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EvoucherSalesComponent } from './evoucher-sales.component'
// import { BastComponent} from './bast/bast.component'


const routes: Routes = [
  {
    path: '', component: EvoucherSalesComponent
},
// {
//   path:'bast', component: BastComponent
// }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvoucherSalesRoutingModule { }
