import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationComponent } from './notification.component';
import { NotificationAddComponent } from './add/notification.add.component';
import { NotificationDetailComponent } from './detail/notification.detail.component';

const routes: Routes = [
  {
      path: '', component: NotificationComponent,

  },
   {
      path:'add', component: NotificationAddComponent
  },
  {
    path:'detail', component: NotificationDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationRoutingModule { }
