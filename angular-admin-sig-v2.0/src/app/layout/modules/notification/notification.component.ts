import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { NotificationService } from '../../../services/notification/notification.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  animations: [routerTransition()]
})

export class NotificationComponent implements OnInit {

  Notifications       : any = [];
  table_title   : any = "Active notification detail";
  table_headers : any = ["Name", "Notification Id", "Color", "User Id"];
  table_rows    : any = ["name", "notification_id", 'color', 'user_id'];
  row_id        : any = "_id";
  form_input    : any = {};
  errorLabel : any = false;
  formOptions   : any = {
                      addForm   : true,
                      row_id    : this.row_id,
                      this      : this,
                      result_var_name : 'Notifications',
                      detail_function : [this, 'callDetail'] 
                    }
  notificationDetail: any = false;
  service     : any ;


  constructor(public notificationService:NotificationService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.service    = this.notificationService;
      const result: any  = await this.notificationService.getNotificationLint();
      this.Notifications = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {
    
    try {
      this.service    = this.notificationService;
      let result: any = await this.notificationService.detailNotification(_id);
      this.notificationDetail = result.result[0];
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.notificationDetail = false;
  }

}
