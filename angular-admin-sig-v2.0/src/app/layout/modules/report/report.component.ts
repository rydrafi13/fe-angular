
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { ReportService } from '../../../services/report/report.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
  animations: [routerTransition()]
})

export class ReportComponent implements OnInit {

  Report: any = [];
  tableFormat: TableFormat = {
        title: 'Bulk Upload',
        label_headers: [
            { label: 'Member ID', visible: true, type: 'string', data_row_name: 'member_id' },
            // { label: 'Status File', visible: false, type: 'string', data_row_name: 'status' },
            { label: 'Product Code', visible: true, type: 'string', data_row_name: 'product_code' },
            { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
            // { label: 'Quantity', visible: false, type: 'string', data_row_name: 'quantity' },
            // { label: 'Point Normal', visible: false, type: 'number', data_row_name: 'pts_normal' },
            // { label: 'Redemption Point', visible: false, type: 'number', data_row_name: 'pts_redeemed' },
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            this: this,
            result_var_name: 'Shoppingcart',
            detail_function: [this, 'callDetail']
        }
    };
  row_id: any = 'status';
  errorLabel: any = false;
  // table_title: any = 'Report From File Upload Detail Page';
  // table_headers: any= ['Member ID','Status File','Product Code','Product Name', 'Quantity','Point Normal','Redemption Point'];
  // table_rows: any= ['member_id','status','product_code','product_name','quantity','pts_normal','pts_redeemed'];
  // row_id: any = '_id';
  // // form_input    : any = {};
  // errorLabel : any = false;
  // formOptions   : any = {
  //   row_id    : this.row_id,
  //   this      : this,
  //   result_var_name : 'Report',
  //   detail_function : [this, 'callDetail'] ,
  //   // type      : 'image',
  //   // image_data_property: 'image_url'
  // }
  reportDetail: any = false;
  service: any;


  // this section belongs to report excel detail
  Reportexcel: any = [];
  tableFormatExcel: TableFormat = {
        title: 'Report From File Upload Detail Page',
        label_headers: [
            { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
            { label: 'Report ID', visible: true, type: 'string', data_row_name: '_id' },
            { label: 'Filename', visible: true, type: 'string', data_row_name: 'status' },
            { label: 'Number of Product', visible: false, type: 'string', data_row_name: 'product_code' },
            { label: 'Process ID', visible: true, type: 'string', data_row_name: 'product_name' },
            { label: 'Status', visible: true, type: 'string', data_row_name: 'quantity' },
            { label: 'Failed to Process', visible: false, type: 'number', data_row_name: 'pts_normal' },
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            this: this,
            result_var_name: 'Reportexcel',
            detail_function: [this, 'callDetail']
        }
    };

  reportExcelDetail: any   = false;
  service_report: any;

  prodOnUpload: any     = false;
  selectedFile  = null;
  progressBar = 0;
  cancel = false;
  errorFile: any = false;
  url = '';
  process_id: any = null;
  failed: any     = false;
  constructor(public reportService: ReportService) { }

    // ngOnInit() {
  //   this.orderhistoryService.getOrderhistory().subscribe(data=>{
  //     let result: any = data;
  //     console.log(result.result);
  //     this.Orderhistory = result.result;

  //     //display and convert shoppingcart_id in foreach because type data is ObjectID
  //     this.Orderhistory.forEach((data, index)=>{this.Orderhistory[index].shoppingcart_id = this.Orderhistory[index].shoppingcart_id.$oid})
  //   });
  // }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    console.log('test');
    try {
      // this.service    = this.reportService;
      // let result: any  = await this.reportService.getReportLint();
      let result: any;
      this.service_report    = this.reportService;
      // result = await this.reportService.getReportLint();
      result = await this.reportService.getReportProcessUploadLint();
      this.Reportexcel       = result.result;
      // this.Report = result.result;
      this.prodOnUpload = false;
      this.failed = false;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);
      // conversion to Error type
    }
  }



  public async callReportDetail(_id){
    try{
      let result: any;
      this.service    = this.reportService;
      result          = await this.reportService.detailReport(_id);
      console.log(result);
      
      this.reportDetail = result.result[0];
      
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }



  public async backToHere(obj){
    obj.reportDetail = false;
    obj.reportExcelDetail = false;
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    // var reader = new FileReader();

    // reader.readAsDataURL(event.target.files[0]); //
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    
    this.selectedFile = event.target.files[0];
    console.log('file',this.selectedFile)
    
  } 

  //  onFileSelect(event) {
  //   let af = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel']
  //   if (event.target.files.length > 0) {
  //     const file = event.target.files[0];
  //     // console.log(file);

  //     if (!_.includes(af, file.type)) {
  //       alert('Only EXCEL Docs Allowed!');
  //     } else {
  //       this.fileInputLabel = file.name;
  //       this.fileUploadForm.get('myfile').setValue(file);
  //     }
  //   }
  // }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let _event: any = event;
        this.url = _event.target.result;
      }
    }
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }

  async processThis(){
    try{
      let data = {
        'process_id':this.process_id,
        'status':'Processed'
      }
      //console.log(data.status);
      let result  = await this.reportService.updateReportExcel(data);
      //console.log(data);
      if(result.error == false){
        //this.prodOnUpload = false;
        this.firstLoad();
        //this.updateProductsCSV(this.process_id);
      }

      //this.updateProductsCSV(this.process_id);
    }
    catch(e){
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  async discardThis(){
    try{
      let data = {
        'process_id':this.process_id,
        'status':'Cancelled'
      }
      let result  = await this.reportService.updateReportExcel(data);
      if(result.error == false){
        //this.prodOnUpload = false;
        this.firstLoad();
      }
    }
    catch(e){
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  async updateProductsCSV(process_id)
  {
    try{

      let result: any;
      this.service_report    = this.reportService;
      result = await this.reportService.getProductsCSV(process_id);
      console.log(process_id);
      this.Reportexcel       = result.result;
      //this.Report = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  async onUpload(){
    try
      {
        this.cancel = false;
        let payload = {
          type : 'product',
          product_type :'product'
        }
        if(this.selectedFile)
        {
          console.log('file', this.selectedFile,this)
          await this.reportService.uploadProduct(this.selectedFile,this,payload);
        }
        //this.onRefresh();
      } 
      catch (e) 
      {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
  }

  // onSubmitFile()
  // {
  //   this.firstLoad();
  // }
  // // onRefresh()
  // // {
  // //   window.location.reload();
  // // }

  // // onUploadSuccess(){
  // //   this.firstLoad();
  // // }
  // onUpdateData()
  // {

  // }
}
