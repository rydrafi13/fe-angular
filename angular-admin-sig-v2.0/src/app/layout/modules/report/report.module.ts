import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportRoutingModule } from './report-routing.module';
import { ReportComponent } from './report.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { ReportDetailComponent } from './detail/report.detail.component';
import { ReportEditComponent } from './edit/report.edit.component';
import { ReportExcelDetailComponent } from './detail/report.excel.detail.component';


@NgModule({
  imports: [
    CommonModule, 
    ReportRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
  declarations: [ReportComponent,ReportDetailComponent,ReportEditComponent,
    ReportExcelDetailComponent
  ]
})
export class ReportModule { }
