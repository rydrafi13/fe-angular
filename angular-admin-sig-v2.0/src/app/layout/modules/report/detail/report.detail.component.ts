import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ReportService } from '../../../../services/report/report.service';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report.detail.component.html',
  styleUrls: ['./report.detail.component.scss'],
  animations: [routerTransition()]
})

export class ReportDetailComponent implements OnInit {
  @Input() public detail: any;
  // @Input() public back;

  edit:boolean = false;
  
  constructor(public reportService:ReportService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  // backToTable(){
  //   console.log(this.back);
  //   this.back[1](this.back[0]);
  // }

}
