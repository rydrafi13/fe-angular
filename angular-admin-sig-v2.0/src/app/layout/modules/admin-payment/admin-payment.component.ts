import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PaymentService } from '../../../services/payment/payment.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-admin-payment',
  templateUrl: './admin-payment.component.html',
  styleUrls: ['./admin-payment.component.scss']
})
export class AdminPaymentComponent implements OnInit {
  Pointstransaction: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Payment Report Page',
                                  label_headers   : [
                                    
                                    {label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'Invoice ID', visible: true, type: 'string', data_row_name: 'invoice_id'},
                                    {label: 'Member ID', visible: true, type: 'string', data_row_name: 'member_email'},
                                    {label: 'Name', visible: true, type: 'string', data_row_name: 'member_name'},       
                                    {label: 'Item Code', visible: true, type: 'productcode', data_row_name: 'products'},                        
                                    {label: 'Product Name', visible: true, type: 'productname', data_row_name: 'products'},
                                    {label: 'Category', visible: true, type: 'productcat', data_row_name: 'products'},
                                    {label: 'Qty', visible: true, type: 'productqty', data_row_name: 'products'},
                                    {label: 'Unit Price', visible: true, type: 'productunit', data_row_name: 'products'},  
                                    {label: 'Total Order', visible: true, type: 'producttotal', data_row_name: 'products'}, 
                                    {label: 'Payment Method', visible: true, type: 'string', data_row_name: 'payment_method'},
                                    {label: 'Paid Date', visible: true, type: 'date', data_row_name: 'payment_success_date'},
                                    {label: 'Payment Via', visible: true, type: 'string', data_row_name: 'payment_via'},
                                    {label: 'Status Payment', visible: true, type: 'string', data_row_name: 'status_payment'},
                                   
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Pointstransaction',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;

  pointstransactionDetail: any = false;
  service: any;
  constructor(public PaymentService:PaymentService) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    console.log(' admin payment')
    try{
      var params = {
         search: {start_date:this.addMonths(new Date(), -6),end_date:new Date()},
         current_page:1,
         limit_per_page:50
        }
      this.service    = this.PaymentService;
      let result: any  = await this.PaymentService.getPaymentReportint(params);
      this.totalPage = result.result.total_page
      this.Pointstransaction = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
  // public async callDetail(pointstransaction_id){
  //   try{
  //     let result: any;
  //     this.service    = this.OrderhistoryService;
  //     result          = await this.OrderhistoryService.detailPointstransaction(pointstransaction_id);
  //     console.log(result);

  //     this.pointstransactionDetail = result.result[0];

  //   } catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }
  // }

  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}
