import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import { del } from 'selenium-webdriver/http';
import { getLink } from '../../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import * as content from '../../../../../assets/json/content.json';

@Component({
  selector: 'app-form-member-detail',
  templateUrl: './form-member.detail.component.html',
  styleUrls: ['./form-member.detail.component.scss'],
  animations: [routerTransition()]
})

export class FormMemberDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public contentList : any = (content as any).default;

  mci_project: any = false;
  edit: boolean = false;
  isLoginRedeem: boolean = false;
  errorLabel : any = false;
  errorMessage  : any = false;
  memberDetail : any;
  isAddDocument: boolean = false;
  isNoProcess: boolean = false;
  alertAddress : boolean = false;
  alertKelurahan: boolean = false;
  alertKecamatan: boolean = false;
  alertKota: boolean = false;
  alertProvinsi: boolean = false;
  alertKodePos: boolean = false;
  programType:any = "";
  sig_kontraktual: any = false;

  isValidForRedeem: boolean = true;

  constructor(public memberService: MemberService,private router: Router,private route: ActivatedRoute) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){

    this.alertAddress  = false;
    this.alertKelurahan = false;
    this.alertKecamatan = false;
    this.alertKota = false;
    this.alertProvinsi = false;
    this.alertKodePos = false;
    this.isValidForRedeem = true;

    const program = localStorage.getItem('programName');
    let _this = this;
    this.contentList.filter(function(element) {
        if(element.appLabel == program) {
            if(element.type == "reguler") {
                _this.mci_project = true;
                _this.programType = "reguler";
            } else if(element.type == "custom_kontraktual") {
              _this.programType = "custom_kontraktual";
              _this.sig_kontraktual = true;
            } else {
                _this.programType = "custom";
                _this.mci_project = false;
            }
        }
    });
    
    let id_pel = this.detail.username;
    this.memberDetail = this.detail;
    try {
      const result : any= await this.memberService.getFormMemberByID(id_pel);
      if(result.length > 0){
        result.forEach((element) => {
          if(element.username == this.detail.username){
            this.memberDetail = element;
          }
        })
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
      
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }

    if(this.memberDetail && this.memberDetail.input_form_data) {
      if(!this.memberDetail.input_form_data.alamat_rumah || this.memberDetail.input_form_data.alamat_rumah == "-") {
        this.alertAddress = true;
        this.isValidForRedeem = false;
      }

      if(!this.mci_project && (!this.memberDetail.input_form_data.kelurahan_rumah || this.memberDetail.input_form_data.kelurahan_rumah == "-")) {
        this.alertKelurahan = true;
        this.isValidForRedeem = false;
      }

      if(!this.mci_project && (!this.memberDetail.input_form_data.kecamatan_rumah || this.memberDetail.input_form_data.kecamatan_rumah == "-")) {
        this.alertKecamatan = true;
        this.isValidForRedeem = false;
      }

      if(!this.mci_project && (!this.memberDetail.input_form_data.kota_rumah || this.memberDetail.input_form_data.kota_rumah == "-")) {
        this.alertKota = true;
        this.isValidForRedeem = false;
      }

      if(!this.mci_project && (!this.memberDetail.input_form_data.provinsi_rumah || this.memberDetail.input_form_data.provinsi_rumah == "-")) {
        this.alertProvinsi = true;
        this.isValidForRedeem = false;
      }

      if(!this.mci_project && (!this.memberDetail.input_form_data.kode_pos_rumah || this.memberDetail.input_form_data.kode_pos_rumah == "-")) {
        this.alertKodePos = true;
        this.isValidForRedeem = false;
      }
    }
    // console.log("this.memberDetail", this.memberDetail);
  }

  addDocument() {
    this.isAddDocument = !this.isAddDocument;
    if(this.isAddDocument == false){
      this.firstLoad();
    }
  }

  checkFileType(text : any){
    let splitFileName = text.split('.');
    const extensFile = splitFileName[splitFileName.length - 1];
    const extens = extensFile.toLowerCase();
    return extens == 'pdf' ? 'document':'image';
  }

  noProcess() {
    this.isNoProcess = !this.isNoProcess;
    if(this.isNoProcess == false){
      this.firstLoad();
    }
  }

  downloadImage(url : any) {
    let filename : any;
    filename = this.lastPathURL(url);
    fetch(url).then(function(t) {
        return t.blob().then((b)=>{
            var a = document.createElement("a");
            a.href = URL.createObjectURL(b);
            a.setAttribute("download", filename);
            a.click();
        }
        );
    });
    }

  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    return retDate[0];
  }

  lastPathURL(url:string){
    let resURL : any = url.split("/");
    console.log("resURL", resURL);
    let lastNumber : number = resURL.length - 1;
    return resURL[lastNumber];
  }

  editThis() {
    // console.log(this.edit );
    this.edit = !this.edit;
    if(this.edit == false){
      this.firstLoad();
    }
    // console.log(this.edit );
  }

  // redeemLogin() {
  //   // console.log(this.edit );
  //   this.isLoginRedeem = !this.isLoginRedeem;
  //   if(this.isLoginRedeem == false){
  //     this.firstLoad();
  //   }
  //   // console.log(this.edit );
  // }

  async redeemLogin() {
      try {
        if(this.mci_project == true) {
          let updatePayload = JSON.parse(JSON.stringify(this.memberDetail.input_form_data));

          if(
            updatePayload.nama_penerima_gopay != updatePayload.nama_pemilik ||
            updatePayload.no_wa_pemilik != updatePayload.telp_pemilik ||
            updatePayload.no_wa_penerima != updatePayload.telp_pemilik
          ) {
            updatePayload.nama_penerima_gopay = updatePayload.nama_pemilik;
            updatePayload.no_wa_pemilik = updatePayload.telp_pemilik;
            updatePayload.no_wa_penerima = updatePayload.telp_pemilik;

            await this.memberService.updateSingleFormMember(updatePayload);
          }
        }

        const payload = {
          username: this.memberDetail.username
        };
        console.log("payload", payload);
          const result: any = await this.memberService.getTokenMember(payload);
  
          if (result && result.token) {
            const webLink = getLink();
            const appLabel = localStorage.getItem('programName');
            window.open(`${webLink}?app=${appLabel}&param=${encodeURIComponent(result.token)}`, '_blank');
          }
      } catch(e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type

        let message = this.errorLabel;
        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
        }
        Swal.fire({
          icon: 'error',
          title: message,
        });
      }
  }

  backToTable() {
    console.warn("back member detail",this.back);
    this.back[1](this.back[0]);
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  async delete(){
    try {
      const delResult: any = await this.memberService.deleteMember(this.detail);
      // console.log(delResult);
      if (delResult.error == false) {
        console.log(this.back[0]);
        this.back[0].memberDetail = false;
        this.back[0].firstLoad();
      // delete this.back[0].prodDetail;
      }
    } catch (error) {
      throw new TypeError(error.error.error);
    }
  } catch (e) {
    this.errorLabel = ((<Error>e).message);//conversion to Error type
  }

  async pointHistory(){
    console.log('here')
    // this.router.navigate(['/member-point-history']);
    this.router.navigate(['administrator/memberadmin/point-history'],  {queryParams: {id: this.detail.email }})
  }

  public async setCompleteMember(){
    if(this.detail && this.detail.username){
      Swal.fire({
        title: 'Confirmation',
        text: 'Apakah anda yakin ingin memproses complete member ini?',
        // icon: 'success',
        confirmButtonText: 'Process',
        cancelButtonText:"Cancel",
        showCancelButton:true
        }).then(async (result) => {
        if(result.isConfirmed){
          try {

            // console.log("memberIds", memberIds);

            const payload = {
              "member_id":[
                this.detail.username
              ],
              "status":"ya"
            }

            await this.memberService.setCompleteMember(payload);
          } catch (e) {
            console.log(e);
          }
          // console.log("member ids", memberIds);
          Swal.fire({
            title: 'Success',
            text: 'Member telah complete',
            icon: 'success',
            confirmButtonText: 'Ok',
            showCancelButton:false
            }).then(async (result) => {
              if(result.isConfirmed){
                this.firstLoad();
              }
            });
        }
      });
    }
  }
  
  
}
