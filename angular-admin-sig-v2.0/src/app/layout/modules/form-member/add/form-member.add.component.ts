import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import Swal from 'sweetalert2';
import * as content from '../../../../../assets/json/content.json';

@Component({
  selector: 'app-form-member-add',
  templateUrl: './form-member.add.component.html',
  styleUrls: ['./form-member.add.component.scss'],
  animations: [routerTransition()]
})

export class FormMemberAddComponent implements OnInit {
  @Input() public back;
  @Input() public detail: any;
  @ViewChild('addBulk', {static: false}) addBulk;

  public contentList : any = (content as any).default;

  public name: string = "";
  // mci_project: any = false;
  programType:any = "";
  Members: any = [];
  service: any;
  full_name:any;
  email:any;
  password:any;
  cell_phone:any;
  card_number:any;
  dob:any;
  mcity:any;
  mstate:any;
  mcountry:any;
  maddress1:any;
  mpostcode:any;
  gender:any;
  marital_status:any;
  type:any;
  member_status:any;
  activation_status:any;
  public form: any = {
    id_pel: "",
    nama_toko: "",
    nama_distributor: "",
    alamat_toko:"",
    kelurahan_toko: "-",
    kecamatan_toko: "-",
    kota_toko: "-",
    provinsi_toko: "-",
    kode_pos_toko: "-",
    landmark_toko: "-",
    nama_pemilik: "",
    telp_pemilik: "",
    no_wa_pemilik: "",
    ktp_pemilik:"",
    foto_ktp_pemilik: "-",
    npwp_pemilik:"",
    foto_npwp_pemilik: "-",
    alamat_rumah:"",
    kelurahan_rumah:"",
    kecamatan_rumah:"",
    kota_rumah:"",
    provinsi_rumah:"",
    kode_pos_rumah:"",
    landmark_rumah: "-",
    description: "nonupfront",
    cluster: "-",
    // description_mci: "",
    hadiah_dikuasakan: "TIDAK KUASA",

    nama_penerima_gopay: "-",
    ktp_penerima: "-",
    foto_ktp_penerima: "-",
    npwp_penerima: "-",
    foto_npwp_penerima: "-",
    telp_penerima_gopay: "-",
    no_wa_penerima: "-",
    nama_id_gopay: "-",
    jenis_akun: "-",
    saldo_terakhir: "-",
  }

  public data: any = {
    username: "",
    password: "",
  }
  
  errorLabel : any = false;
  public member_status_type = [
                       {label:'Superuser', value:'superuser'}
                      ,{label:'Member', value:'member', selected:1}
                      ,{label:'Marketing', value:'marketing'}
                      ,{label:'Admin', value:'admin'}
                      ,{label:'Merchant', value:'merchant'}
                    ];
  
  public group_desc: any = [
    {label:'Nonupfront', value:'nonupfront'}
    ,{label:'Upfront', value:'upfront'}
  ];

  public kuasa_option: any = [
    {label:'TIDAK KUASA', value:'TIDAK KUASA'},
    {label:'KUASA', value:'KUASA'}
  ];

  isBulkUpdate : any = false;
  selectedFile  = null;
  cancel = false;
  progressBar = 0;
  errorFile: any = false;
  prodOnUpload: any     = false;
  startUploading: any = false;
  showUploadButton: any = false;
  getPassword: any;
  successUpload: any = false;
  urlDownload: any = "";

  warnIDPelanggan: any = false;
  warnNamaPemilik: any = false;
  warnTelpPemilik: any = false;
  warnWAPemilik: any = false;

  showPasswordPage: any = false;

  keywordProvince = 'province';
  keywordCity = 'city';
  keywordSubDistrict = 'subdistrict';
  keywordVillage = 'village';

  dataProvince:any = [];
  dataCity:any = [];
  dataSubDistrict:any = [];
  dataVillage:any = [];

  province_code:any;
  city_code:any;
  subdistrict_code:any;
  village_code:any;
  
  isInputComplete: any;
  inputTimeOut: any;

  loadingProvince: boolean = false;
  loadingCity: boolean = false;
  loadingSubDistrict: boolean = false;
  loadingVillage: boolean = false;

  public placeholderProvince: string = 'Provinsi Rumah';
  public placeholderCity: string = 'Kabupaten/Kota Rumah';
  public placeholderSubDistrict: string = 'Kecamatan Rumah';
  public placeholderVillage: string = 'Desa/Kelurahan Rumah';

  constructor(public memberService:MemberService) {
    
   }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    const program = localStorage.getItem('programName');
    let _this = this;
    this.contentList.filter(function(element) {
        if(element.appLabel == program) {
          if(element.type == "reguler") {
            _this.programType = "reguler";
          } else if(element.type == "custom_kontraktual") {
            _this.programType = "custom_kontraktual";
            _this.form.description = "-";
          } else {
            _this.programType = "custom";
          }
        }
    });

    this.selectedFile = null;
    this.startUploading = false;

    this.kuasa_option.forEach((element, index) => {
      if (element.value == "TIDAK KUASA") {
          this.kuasa_option[index].selected = 1;
      }
    });

    this.group_desc.forEach((element, index) => {
      if (element.value == "nonupfront") {
          this.group_desc[index].selected = 1;
      }
    });

  }
  async formSubmitAddMember(){
    // console.log(JSON.stringify(form));
    // console.warn(" mask")
    // if (this.form.id_pel == "") {
    //   this.warnIDPelanggan = true;
    // } else if (this.form.nama_pemilik == "") {
    //   this.warnNamaPemilik = true;
    // } else if (this.form.telp_pemilik == "") {
    //   this.warnTelpPemilik = true;
    // } else if (this.form.no_wa_pemilik == "") {
    //   this.warnWAPemilik = true;
    // } else {
    //   console.warn("masukkk")

      let form_add = this.form;

      //check if object from autocomplete component
      if(form_add) {
        if(form_add.provinsi_rumah && form_add.provinsi_rumah && typeof form_add.provinsi_rumah === 'object' && form_add.provinsi_rumah !== null && form_add.provinsi_rumah.province) {
          form_add.provinsi_rumah = form_add.provinsi_rumah.province;
        }

        if(form_add.kota_rumah && form_add.kota_rumah && typeof form_add.kota_rumah === 'object' && form_add.kota_rumah !== null && form_add.kota_rumah.city) {
          form_add.kota_rumah = form_add.kota_rumah.city;
        }

        if(form_add.kecamatan_rumah && form_add.kecamatan_rumah && typeof form_add.kecamatan_rumah === 'object' && form_add.kecamatan_rumah !== null && form_add.kecamatan_rumah.subdistrict) {
          form_add.kecamatan_rumah = form_add.kecamatan_rumah.subdistrict;
        }

        if(form_add.kelurahan_rumah && form_add.kelurahan_rumah && typeof form_add.kelurahan_rumah === 'object' && form_add.kelurahan_rumah !== null && form_add.kelurahan_rumah.village) {
          form_add.kelurahan_rumah = form_add.kelurahan_rumah.village;
        }
      }
      
      try {
        this.service    = this.memberService;
        const result: any  = await this.memberService.addNewMember(form_add);
        console.warn("result add member", result)
  
        if (result && result.data) {
          this.getPassword = result.data.password;
        }

        this.data.username = this.form.id_pel;
        this.data.password = this.getPassword;
        // this.Members = result.result;
        // alert("Member Has Been Added!")
        Swal.fire({
          title: 'Success',
          text: 'Pelanggan berhasil ditambahkan',
          icon: 'success',
          confirmButtonText: 'Ok',
        }).then((result) => {
          if(result.isConfirmed){
            // this.backTo();
            this.showThisPassword();
          }
        });
      } catch (e) {
        console.warn("error", e.message)
        // alert(e.message);
        this.errorLabel = ((<Error>e).message);
        let message = this.errorLabel;
        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
        }

        Swal.fire({
          title: 'Failed',
          text: message,
          icon: 'warning',
          confirmButtonText: 'Ok',
        }).then((result) => {
          if(result.isConfirmed){
            // this.backTo();
            this.firstLoad();
          }
        });
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
    // }

  }

  showThisPassword() {
    this.showPasswordPage = !this.showPasswordPage;
    if(this.showPasswordPage == false){
      this.firstLoad();
    }
    // console.log(this.edit );
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    // var reader = new FileReader();
    // reader.readAsDataURL(event.target.files[0]); //
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    
    this.selectedFile = event.target.files[0];
    this.addBulk.nativeElement.value = '';
    
  } 

  cancelThis(){
    this.cancel = !this.cancel;
  }

  actionShowUploadButton() {
    this.showUploadButton = !this.showUploadButton;
  }

  async updateDataBulk(){
    try
      {
        this.startUploading = true;
        this.cancel = false;
        let payload = {
          type : 'application/form-data',
        }
        console.warn("start upload", this.startUploading);
        if(this.selectedFile)
        {
          console.log('file', this.selectedFile,this);
          const result: any = await this.memberService.registerBulkMember(this.selectedFile,this,payload);      
          if (result) {
            this.firstLoad();
          }
        }
        //this.onRefresh();
      } 
      catch (e) {
        this.startUploading = false;
        this.errorLabel = ((<Error>e).message);//conversion to Error type

        let message = this.errorLabel;
        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
        }
        Swal.fire({
          icon: 'error',
          title: message,
        });
      }
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  backTo(){
    window.history.back();
  }

  selectEvent(item, region) {
    // do something with selected item
    if(region == "province") {
      this.form.provinsi_rumah = item.province;
      this.province_code = item.province_code;
    } else if(region == "city") {
      this.form.kota_rumah = item.city;
      this.city_code = item.city_code;
    } else if(region == "subdistrict") {
      this.form.kecamatan_rumah = item.subdistrict;
      this.subdistrict_code = item.subdistrict_code;
    } else if(region == "village") {
      this.form.kelurahan_rumah = item.village;
      this.village_code = item.village_code;
    }
  }

  async inputRegion(val: string, region) {
    //prevent calling API twice or more
    await clearTimeout(this.inputTimeOut);

    this.isInputComplete = false;

    //set loading indicator
    if(region == "province") {
      this.loadingProvince = true;

      if(this.programType != "reguler") {
        this.form.kota_rumah = "";
        this.form.kecamatan_rumah = "";
        this.form.kelurahan_rumah = "";

        delete this.province_code;
        delete this.city_code;
        delete this.subdistrict_code;
      }
    } else if(region == "city") {
      this.loadingCity = true;

      if(this.programType != "reguler") {
        this.form.kecamatan_rumah = "";
        this.form.kelurahan_rumah = "";

        delete this.city_code;
        delete this.subdistrict_code;
      }
    } else if(region == "subdistrict") {
      this.loadingSubDistrict = true;

      if(this.programType != "reguler") {

        delete this.subdistrict_code;
        this.form.kelurahan_rumah = "";
      }
    } else if(region == "village") {
      this.loadingVillage = true;
    }

    this.inputTimeOut = setTimeout(() => {
      this.isInputComplete = true;

      if(this.isInputComplete) {
        this.filterRegion(val, region);
      }
    } , 300);
  }

  public filterRegion(search, region): void {
      let payload:any = {
        "region":region,
        "search":search
      };

      if(region == "city") {
        payload.province_code = this.province_code;
      } else if(region == "subdistrict") {
        payload.city_code = this.city_code;
      } else if(region == "village") {
        payload.subdistrict_code = this.subdistrict_code;
      }
      console.log(payload);

      // Set val to the value of the searchbar
      if(search.length > 0) {
        this.memberService.searchRegion(payload).then((result) => {
          this.loadingProvince = false;
          this.loadingCity = false;
          this.loadingSubDistrict = false;
          this.loadingVillage = false;

          if(result && result.length > 0) {
            if(region == "province") {
              this.dataProvince = result;
            } else if(region == "city") {
              this.dataCity = result;
            } else if(region == "subdistrict") {
              console.log("result", result);
              this.dataSubDistrict = result;
            } else if(region == "village") {
              this.dataVillage = result;
            }
          }
        }, (err) => {
          console.log(err);
          this.loadingProvince = false;
          this.loadingCity = false;
          this.loadingSubDistrict = false;
          this.loadingVillage = false;

          this.dataProvince = [];
          this.dataCity = [];
          this.dataSubDistrict = [];
          this.dataVillage = [];
        });
      } else {
        this.loadingProvince = false;
        this.loadingCity = false;
        this.loadingSubDistrict = false;
        this.loadingVillage = false;

        this.dataProvince = [];
        this.dataCity = [];
        this.dataSubDistrict = [];
        this.dataVillage = [];
      }
  }
}
