import { AdditonalDocumentComponent } from './../additional-document.component';
import { ProductService } from '../../../../../services/product/product.service';
import { Component, OnInit, Input, NgZone } from '@angular/core';
import { routerTransition } from '../../../../../router.animations';
import { MemberService } from '../../../../../services/member/member.service';
import Swal from 'sweetalert2';
import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';


@Component({
  selector: 'app-additional-document-edit',
  templateUrl: './additional-document.edit.component.html',
  styleUrls: ['./additional-document.edit.component.scss'],
  animations: [routerTransition()]
})

export class EditAdditonalDocumentComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  member_status:any;
  activation_status:any;
  new_form:any = {};

  private allBast : any = [];
  private allSuratKuasa : any = [];
  private allSuratPernyataan : any = [];
  private updateSingle: any;
  
  public loading        : boolean = false;

  errorFile: any = false;
  selectedFile: any;
  selFile: any;
  // loadingKTPPemilik: boolean = false;
  // loadingNPWPPemilik: boolean = false;
  // loadingKTPPenerima: boolean = false;
  // loadingNPWPPenerima: boolean = false;
  loadingBAST: any = [];
  loadingSuratKuasa: any = [];
  loadingSuratPernyataan: any = [];

  constructor(public memberService: MemberService, public productService:ProductService, private zone: NgZone) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  assignArrayBoolean(curVar : any, varCont : any){
    if(this.isArray(curVar)){
      curVar.forEach(element => {
        varCont.push(false);
      });
    }
  }

  async firstLoad() {
    
    if(!this.detail.additional_info) this.detail.additional_info = {bast:[],surat_kuasa:[],surat_pernyataan:[]};
    if(!this.detail.additional_info.bast) this.detail.additional_info.bast = [];
    if(!this.detail.additional_info.surat_kuasa) this.detail.additional_info.surat_kuasa = [];
    if(!this.detail.additional_info.surat_pernyataan) this.detail.additional_info.surat_pernyataan = [];

    this.updateSingle = JSON.parse(JSON.stringify(this.detail.additional_info));
    this.updateSingle.bast = this.isArray(this.updateSingle.bast) ? this.updateSingle.bast: [];
    this.allBast = this.updateSingle.bast;
    this.updateSingle.surat_kuasa = this.isArray(this.updateSingle.surat_kuasa) ? this.updateSingle.surat_kuasa: [];
    this.allSuratKuasa = this.updateSingle.surat_kuasa;
    this.updateSingle.surat_pernyataan = this.isArray(this.updateSingle.surat_pernyataan) ? this.updateSingle.surat_pernyataan: [];
    this.allSuratPernyataan = this.updateSingle.surat_pernyataan;
    this.assignArrayBoolean(this.allBast, this.loadingBAST);
    this.assignArrayBoolean(this.allSuratKuasa, this.loadingSuratKuasa);
    this.assignArrayBoolean(this.allSuratPernyataan, this.loadingSuratPernyataan);
    // console.log("loadingBAST", this.loadingBAST);
    // this.assignArrayBoolean(this.allSuratKuasa);
    // console.log("count", this.allBast, this.allSuratKuasa);

    // console.warn("update single", this.updateSingle);
    
    // this.detail.previous_member_id = this.detail.member_id;

    // this.detail.previous_member_id = this.detail.member_id;
  }

  backToDetail() {
    this.back[0][this.back[1]]();
  }

  validURL(str) {
    return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(str);
  }

  checkFileType(text : any){
    let splitFileName = text.split('.');
    const extensFile = splitFileName[splitFileName.length - 1];
    const extens = extensFile.toLowerCase();
    return extens == 'pdf' ? 'document':'image';
  }

  openUpload(type:any, index:any){
    let upFile : any = document.getElementById(type+index);
    upFile.click();
  }


  async saveThis(){

    var arrAddBAST = [];
    var arrAddSK = [];
    var arrAddSP = [];
    var arrDeleteBAST = [];
    var arrDeleteSK = [];
    var arrDeleteSP = [];

    //validate BAST
    for(let i = 0; i < this.detail.additional_info.bast.length; i++){
      let found = false;
      for(let j = 0; j < this.updateSingle.bast.length; j++){

        if(this.updateSingle.bast[j].id == this.detail.additional_info.bast[i].id){
          found = true;
          break;
        }
      }
      if(!found){
        arrDeleteBAST.push(this.detail.additional_info.bast[i].id);
      }
    }
    
    for(let i = 0; i < this.updateSingle.bast.length; i++){
      if(this.updateSingle.bast[i].id == null && this.updateSingle.bast[i].pic_file_name){
        arrAddBAST.push(this.updateSingle.bast[i].pic_file_name);
      }
    }
    
    // Validate Surat Kuasa
    for(let i = 0; i < this.detail.additional_info.surat_kuasa.length; i++){
      let found = false;
      for(let j = 0; j < this.updateSingle.surat_kuasa.length; j++){
        if(this.updateSingle.surat_kuasa[j].id == this.detail.additional_info.surat_kuasa[i].id){
          found = true;
          break;
        }
      }
      if(!found){
        arrDeleteSK.push(this.detail.additional_info.surat_kuasa[i].id);
      }
    }

    for(let i = 0; i < this.updateSingle.surat_kuasa.length; i++){
      if(this.updateSingle.surat_kuasa[i].id == null && this.updateSingle.surat_kuasa[i].pic_file_name){
        arrAddSK.push(this.updateSingle.surat_kuasa[i].pic_file_name);
      }
    }

    // Validate Surat Pernyataan
    for(let i = 0; i < this.detail.additional_info.surat_pernyataan.length; i++){
      let found = false;
      for(let j = 0; j < this.updateSingle.surat_pernyataan.length; j++){
        if(this.updateSingle.surat_pernyataan[j].id == this.detail.additional_info.surat_pernyataan[i].id){
          found = true;
          break;
        }
      }
      if(!found){
        arrDeleteSP.push(this.detail.additional_info.surat_pernyataan[i].id);
      }
    }

    for(let i = 0; i < this.updateSingle.surat_pernyataan.length; i++){
      if(this.updateSingle.surat_pernyataan[i].id == null && this.updateSingle.surat_pernyataan[i].pic_file_name){
        arrAddSP.push(this.updateSingle.surat_pernyataan[i].pic_file_name);
      }
    }
    

    let payloadRemove = {
      username:this.detail.username,
      bast:arrDeleteBAST,
      surat_kuasa:arrDeleteSK,
      surat_pernyataan:arrDeleteSP
    }

    let payloadAdd = {
       username:this.detail.username,
       bast:arrAddBAST,
       surat_kuasa:arrAddSK,
       surat_pernyataan:arrAddSP
    }

    try {
      if(payloadAdd.bast.length > 0 || payloadAdd.surat_kuasa.length > 0 || payloadAdd.surat_pernyataan.length > 0) await this.memberService.addBastSk(payloadAdd);
      if(payloadRemove.bast.length > 0 || payloadRemove.surat_kuasa.length > 0 || payloadRemove.surat_pernyataan.length > 0) await this.memberService.deleteBastSk(payloadRemove);
      Swal.fire({
        title: 'Success',
        text: 'Edit data pelanggan berhasil',
        icon: 'success',
        confirmButtonText: 'Ok',
      }).then((result) => {
        if(result.isConfirmed){
          this.backToDetail();
        }
      });
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
      // alert("Error");
      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
  }


  // async oldsaveThis() {
  //   this.new_form = {
  //     username: this.detail.username
  //   }

  //   if(this.updateSingle.bast && this.updateSingle.bast.length > 0){
  //     let currentBast = this.updateSingle.bast.filter((element : any) => {
  //       if(element.url && this.validURL(element.url)){
  //         return element;
  //       }
  //     });
  //     this.updateSingle.bast = currentBast;
  //   }
  //   if(this.updateSingle.surat_kuasa && this.updateSingle.surat_kuasa.length > 0){
  //     let currentSuratKuasa = this.updateSingle.surat_kuasa.filter((element:any) => {
  //       if(element.url && this.validURL(element.url)){
  //         return element;
  //       }
  //     });
  //     this.updateSingle.surat_kuasa = currentSuratKuasa;
  //   }
    
  //   for(let prop in this.updateSingle){
  //     if(this.updateSingle[prop] != this.detail[prop] && this.updateSingle[prop] != ""){
  //         this.new_form[prop] = this.updateSingle[prop];
  //     }
  //     if(prop == 'bast' || prop == 'surat_kuasa'){
  //       this.new_form[prop] = this.updateSingle[prop];
  //     }
  //   }

  //   console.warn("new form", this.new_form);
  //   console.warn("updateSingle", this.updateSingle);
  //   console.warn("detail", this.detail);
      
  //   try {
  //     await this.memberService.updateBASTandSK(new_form);
  //     this.detail = JSON.parse(JSON.stringify(this.updateSingle));
  //     //  Swal.fire('Success', 'Edit data pelanggan berhasil', 'success');
  //     Swal.fire({
  //       title: 'Success',
  //       text: 'Edit data pelanggan berhasil',
  //       icon: 'success',
  //       confirmButtonText: 'Ok',
  //     }).then((result) => {
  //       if(result.isConfirmed){
  //         this.backToDetail();
  //       }
  //     });
  //   } catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //     alert("Error");
  //   }
  // }

  // async onFileSelected2(event, img) {
	// 	var reader = new FileReader()
	// 	try {
	// 		this.errorFile = false;
	// 		let fileMaxSize = 3000000; // let say 3Mb
	// 		Array.from(event.target.files).forEach((file: any) => {
	// 			if (file.size > fileMaxSize) {
	// 				this.errorFile = 'Maximum File Upload is 3MB';
	// 			}
	// 		});
	// 		this.selectedFile = event.target.files[0];

	// 		reader.onload = (event: any) => {
	// 			this.selFile = event.target.result;
	// 		}
	// 		if(event.target.files[0]){
  //       reader.readAsDataURL(event.target.files[0]);
  //     }
	// 		if (this.selectedFile) {
  //       let valueImage = this.checkLoading(img, true);
				
	// 			const logo = await this.productService.upload(this.selectedFile, this, 'image', (result) => {
  //         this.updateSingle[valueImage] = result.base_url + result.pic_big_path;
  //         this.checkLoading(img, false);
	// 				// console.log("updateSingle", this.updateSingle);
	// 				// this.callImage(result, img);
	// 			});
	// 		}
			
	// 	} catch (e) {
	// 		this.errorLabel = (<Error>e).message; //conversion to Error type
	// 	}
  // }

  async onFileSelected(event, typeImg: any, idx : any) {
    if(typeImg == 'bast'){
      this.loadingBAST[idx] = true;
    }

    else if(typeImg == 'suratKuasa'){
      this.loadingSuratKuasa[idx] = true;
    }

    else if(typeImg == 'suratPernyataan'){
      this.loadingSuratPernyataan[idx] = true;
    }

		var reader = new FileReader();
    const programName = localStorage.getItem('programName');
    const fileExtens = ['jpg', 'jpeg', 'png', 'gif', 'pdf'];
		try {
			this.errorFile = false;
			let fileMaxSize = 2064000;
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 2MB';
				}
        const splitFileName = file.name.split('.');
        if(splitFileName.length < 1)
        { 
          this.errorFile = "file extention is required";
        } else {
          let extensFile = splitFileName[splitFileName.length - 1];
          const extens = extensFile.toLowerCase();
          if(!fileExtens.includes(extens)) this.errorFile = `extension ${extens} is not Allowed`;
        }
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}
			if(event.target.files[0]){
        reader.readAsDataURL(event.target.files[0]);
      }
			if (this.selectedFile) {
        let splitFileName = this.selectedFile.name.split('.');
        const extensFile = splitFileName[splitFileName.length - 1];
        const extens = extensFile.toLowerCase();
        const typeFile = extens == 'pdf' ? 'document':'image';

        const newFileName =  programName + '-' + this.detail.username + '-' + typeImg + '.' + extens;
				await this.productService.upload(this.selectedFile, this, typeFile, (result) => {

          let dateNow = Date.now();

          const dataImage = {
            pic_file_name: typeof result == 'object' && result.base_url ? result.base_url + result.pic_big_path : result,
            updated_date: dateNow,
            created_date: dateNow,
            id: null
          }

          if(typeImg == 'bast'){
            this.updateSingle.bast[idx] = {...this.updateSingle.bast[idx], ...dataImage};
            this.loadingBAST[idx] = false;

          }

          else if(typeImg == 'suratKuasa'){
            this.updateSingle.surat_kuasa[idx] = {...this.updateSingle.surat_kuasa[idx], ...dataImage};
            this.loadingSuratKuasa[idx] = false;
          }

          else if(typeImg == 'suratPernyataan'){
            this.updateSingle.surat_pernyataan[idx] = {...this.updateSingle.surat_pernyataan[idx], ...dataImage};
            this.loadingSuratPernyataan[idx] = false;
          }

          
				}, newFileName);
			}
			
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
		}
  }

  addMoreUpload(typeImg : any)
  {
    const dummyImg = {
      created_by: null,
      created_date: null,
      id: null,
      pic_file_name: null,
      updated_by: null,
      updated_date: null
    }
    if(typeImg == 'bast'){
      this.updateSingle.bast.push(dummyImg);
      this.loadingBAST.push(false);
    } else if(typeImg == 'suratKuasa'){
      this.updateSingle.surat_kuasa.push(dummyImg);
      this.loadingSuratKuasa.push(false);
    } else if(typeImg == 'suratPernyataan'){
      this.updateSingle.surat_pernyataan.push(dummyImg);
      this.loadingSuratPernyataan.push(false);
    }
  }

  deleteImage(type : any, idx : any, url : any){
    Swal.fire({
      title: "Delete",
      text : "Are you sure want to delete this ?",
      imageUrl : url,
      imageHeight : 250,
      imageAlt : type,
      showCancelButton:true,
      focusConfirm:false,
    }).then(res =>{
      if(res.isConfirmed){
        if(type == "bast"){
          this.updateSingle.bast.splice(idx,1);
          this.loadingBAST.splice(idx,1);
          // console.log("BAST DELETE", this.updateSingle.bast,this.allBast);
        }else if(type="suratKuasa"){
          this.updateSingle.surat_kuasa.splice(idx,1);
          this.loadingSuratKuasa.splice(idx,1);
          // console.log("SURAT KUASA DELETE", this.updateSingle.surat_kuasa,this.allSuratKuasa);
        }else if(type="suratPernyataan"){
          this.updateSingle.surat_pernyataan.splice(idx,1);
          this.loadingSuratPernyataan.splice(idx,1);
          // console.log("SURAT PERNYATAAN DELETE", this.updateSingle.surat_pernyataan,this.allSuratPernyataan);
        }
      }
    });
  }

  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    
    return retDate[0];
  }

  checkLoading(fieldName, fieldValue){
    let valueImage:any;
    switch (fieldName) {      
      case 'bast':
        valueImage = 'bast';
        this.loadingBAST = fieldValue;
        break;
      case 'suratKuasa' :
        valueImage = 'surat_kuasa';
        this.loadingSuratKuasa = fieldValue;
      case 'suratPernyataan' :
        valueImage = 'surat_pernyataan';
        this.loadingSuratPernyataan = fieldValue;
    }
    return valueImage;
  }

}
