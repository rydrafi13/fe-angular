import { ProductService } from '../../../../services/product/product.service';
import { Injectable, Component, OnInit, Input, NgZone } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import { getLink } from '../../../../../environments/environment';
import Swal from 'sweetalert2';
import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';


@Component({
  selector: 'app-redeem-login',
  templateUrl: './redeem-login.component.html',
  styleUrls: ['./redeem-login.component.scss'],
  animations: [routerTransition()]
})

@Injectable({
  providedIn: 'root',
})

export class RedeemLoginComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  member_status:any;
  activation_status:any;
  prodOnUpload: any = false;
  showUploadButton: any = false;
  selectedFile  = null;
  cancel = false;
  progressBar = 0;
  errorFile: any = false;

  private updateSingle: any;
  
  public loading        : boolean = false;

  public activationStatus: any = [
    {label: 'ACTIVATED', value: 'ACTIVATED'},
    {label: 'INACTIVED', value: 'INACTIVED'}
  ];

  public memberStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  selFile: any;
  loadingKTPPemilik: boolean = false;
  loadingNPWPPemilik: boolean = false;
  loadingKTPPenerima: boolean = false;
  loadingNPWPPenerima: boolean = false;
  loadingBAST: any = [];
  loadingSuratKuasa: any = [];
  password: any =  "";
  showPasswordPage: any = false;

  public dataLogin: any = {
    username: "",
    password: "",
  }

  constructor(public memberService: MemberService, public productService:ProductService, private zone: NgZone) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  assignArrayBoolean(curVar : any, varCont : any){
    if(this.isArray(curVar)){
      curVar.forEach(element => {
        varCont.push(false);
      });
    }
  }

  async firstLoad() {
    this.updateSingle = JSON.parse(JSON.stringify(this.detail.input_form_data));
    this.updateSingle.foto_bast = this.isArray(this.updateSingle.foto_bast) ? this.updateSingle.foto_bast: [];
    this.updateSingle.foto_surat_kuasa = this.isArray(this.updateSingle.foto_surat_kuasa) ? this.updateSingle.foto_surat_kuasa: [];
    
    this.detail.previous_member_id = this.detail.member_id;
    this.activationStatus.forEach((element, index, products_ids) => {
        if (element.value == this.detail.activation_status) {
            this.activationStatus[index].selected = 1;
        }
    });

    this.detail.previous_member_id = this.detail.member_id;
    this.memberStatus.forEach((element, index, products_ids) => {
        if (element.value == this.detail.member_status) {
            this.memberStatus[index].selected = 1;
        }
    });

  }

  backToDetail() {
    // console.log("Edit member",this.back);
    this.back[0][this.back[1]]();
    // this.back[0]();
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  openUpload(type:any, index:any){
    let upFile : any = document.getElementById(type+index);
    upFile.click();
  }

  async redeemLogin() {
  //   window.open(
  //     'http://192.168.1.4:8081/?param='+encodeURIComponent('t0lCSTHTBFFQkiNHFDgrd3cJ4s3VfZyZTSJwRFcbqvmMjfkb0aA1I1sRfZJE3MKD.BiJax6/6pCEFfA3jGEwEuTEL5me+roZmoOvjY+6gYQScO3jBbmeBWCPStQu3sft1aYIZzgcWGUEOrpsIk8TCIOcFsmJipkrEpgSh/LxQIW4hTZKoMVAKggh+AlefG5U0Y8cPrXq8+5F7c67NvgRsMVPjpMUZH4+ZPbwlZZTOvTtQ4BiTMch5cMR93cLi8FLlKXJ3Ekfg3LubBB2l1wPPSp+JmmhSnfXo3RKUkMkJx0o=.fdZPw9zR/9gCSZcMBq/DOkS5VuDjVQAnS7wWRmUoj5E='),
  //     '_blank' // <- This is what makes it open in a new window.
  // );
    try {
      let dataLogin = {
        username: this.updateSingle.id_pel,
        password: this.password
      }

      if (dataLogin) {
        const result: any = await this.memberService.loginRedeemMember(dataLogin);

        if (result && result.token) {
          console.warn("result login", result);
          const webLink = getLink();
          
          window.open(`${webLink}?param=${encodeURIComponent(result.token)}`, '_blank')
        }
      }

    } catch(e) {
      console.warn("error login", e);
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  async saveNewPassword(idCustomer) {
    try {
      console.warn("id customer", idCustomer)
      let dataLogin = {
        username: [idCustomer]
      }
      const result: any = await this.memberService.resetPasswordChangePassword(dataLogin);

      if (result && result.data) {
        this.dataLogin.username = result.data.username;
        this.dataLogin.password = result.data.password;
      }

      this.detail = JSON.parse(JSON.stringify(this.updateSingle));
      Swal.fire({
        title: 'Success',
        text: 'Password berhasil disimpan',
        icon: 'success',
        confirmButtonText: 'Ok',
      }).then((result) => {
        if(result.isConfirmed){
          // this.showThisPassword();
          // this.backToDetail();
        }
      });
    } catch (e) {
      console.warn("error", e)
      this.errorLabel = ((<Error>e).message);//conversion to Error type
      // alert("Error");
      let message = this.errorLabel;
      console.warn("message", this.errorLabel);
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      })
    }
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    // var reader = new FileReader();
    // reader.readAsDataURL(event.target.files[0]); //
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    
    this.selectedFile = event.target.files[0];
    console.log('file',this.selectedFile)
    
  } 

  cancelThis(){
    this.cancel = !this.cancel;
  }

  actionShowUploadButton() {
    this.showUploadButton = !this.showUploadButton;
  }

  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    
    return retDate[0];
  }

  async updateDataBulk(){
    try
      {
        this.cancel = false;
        let payload = {
          type : 'application/form-dataLogin',
        }
        if(this.selectedFile)
        {
          console.log('file', this.selectedFile,this)
          await this.memberService.registerBulkMember(this.selectedFile,this,payload);
        }
        this.firstLoad();
        //this.onRefresh();
      } 
      catch (e) 
      {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

}
