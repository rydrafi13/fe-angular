import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShippingBulkProcessComponent } from './shipping-bulk-process.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { ShippingBulkProcessComponentRoutingModule } from './shipping-bulk-process.routing';
import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  imports: [CommonModule,
    ShippingBulkProcessComponentRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
  ],
  
  declarations: [
    ShippingBulkProcessComponent,
  ],

  // exports : [MemberComponent]
})
export class ShippingBulkProcessModule { }
