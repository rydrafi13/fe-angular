import { Component, OnInit, Input, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../router.animations';
import { EVoucherService } from '../../../services/e-voucher/e-voucher.service';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import { PointsTransactionService } from '../../../services/points-transaction/points-transaction.service';
import readXlsxFile from 'read-excel-file';

@Component({
  selector: 'app-shipping-bulk-process',
  templateUrl: './shipping-bulk-process.component.html',
  styleUrls: ['./shipping-bulk-process.component.scss'],
  animations: [routerTransition()]
})

export class ShippingBulkProcessComponent implements OnInit {
  @Input() public back;
  @Input() public detail: any;
  @ViewChild('addBulk', {static: false}) addBulk;

  public name: string = "";
  Evouchers: any = [];
  service: any;
  // type:any;
  Report:any;
  member_status:any;
  activation_status:any;
  
  errorLabel : any = false;
 
  isBulkUpdate : any = false;
  selectedFile  = null;
  cancel = false;
  progressBar = 0;
  errorFile: any = false;
  prodOnUpload: any     = false;
  startUploading: any = false;
  showUploadButton: any = false;
  getPassword: any;
  successUpload: any = false;
  urlDownload: any = "";

  warnIDPelanggan: any = false;
  warnNamaPemilik: any = false;
  warnTelpPemilik: any = false;
  warnWAPemilik: any = false;

  showPasswordPage: any = false;

  shippingType = [
    { label: 'Packaging', value: 'packaging', selected:1 },
    { label: 'Delivery Pickup', value: 'delivery'},
    { label: 'Cancel Pickup', value: 'cancel_pickup'},
    { label: 'Delivered Pickup', value: 'delivered_pickup'},
    { label: 'Completed Order', value: 'completed_order' }
  ];

  type:any = "packaging";


  constructor(public evoucherService:EVoucherService, public orderHistoryService:OrderhistoryService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    this.selectedFile = null;
    this.startUploading = false;
  }

  async updateDataBulk(){
    try
      {
        if(this.type == "packaging") {
          this.startUploading = true;
          this.cancel = false;
          let payload = {
            type : 'application/form-data',
          }
          if(this.selectedFile) {
            const result: any = await this.orderHistoryService.uploadPackagingBulk(this.selectedFile,this,payload, this.type);
            if (result) {
              this.firstLoad();
            }
          }
        } else if(this.type == "delivery") {
          this.startUploading = true;
          this.cancel = false;
          let payload = {
            type : 'application/form-data',
          }
          if(this.selectedFile) {
            const result: any = await this.orderHistoryService.uploadShippingProcessBulk(this.selectedFile,this,payload, this.type);
            if (result) {
              this.firstLoad();
            }
          }
        } else if(this.type == "cancel_pickup") {
          this.startUploading = true;
          this.cancel = false;
          let payload = {
            type : 'application/form-data',
          }
          if(this.selectedFile) {
            const result: any = await this.orderHistoryService.uploadCancelPickupBulk(this.selectedFile,this,payload, this.type);
            if (result) {
              this.firstLoad();
            }
          }
        } else if(this.type == "delivered_pickup") {
          this.startUploading = true;
          this.cancel = false;
          let payload = {
            type : 'application/form-data',
          }
          if(this.selectedFile) {
            console.warn("in")
            const result: any = await this.orderHistoryService.uploadShippingCompleteBulk(this.selectedFile,this,payload, this.type);
            if (result) {
              this.firstLoad();
            }
          }
        } else if(this.type == "completed_order") {
          this.startUploading = true;
          this.cancel = false;
          // LOOPING ARRAY IN EXCEL FILE
          console.log("this.selectedFile", this.selectedFile.name);
          try {
            readXlsxFile(this.selectedFile).then(async (rows) => {
              console.log(rows);

              let arrayOrder = [];

              rows.forEach((_row, index) => {
                if(index > 0) arrayOrder.push(_row[0]);
              });

              console.log("arrayOrder", arrayOrder);

              let dataPost = {
                "order_id": arrayOrder
              }

              const result: any = await this.orderHistoryService.updateStatusOrderCompleted(dataPost,this);
              // `rows` is an array of rows
              // each row being an array of cells.

              // Swal.fire(
              //   'Your file has been uploaded!',
              //   'success'
              // )
            })
          } catch (error) {
            console.log("error upload", error)
            this.errorLabel = ((<Error>error).message);
            Swal.fire(
              'Failed!',
              this.errorLabel,
              'warning'
            )
          }
        }
      } 
      catch (e) {
        this.startUploading = false;
        this.errorLabel = ((<Error>e).message);//conversion to Error type
        Swal.fire(
          'Failed!',
          this.errorLabel,
          'warning'
        )
      }
  }

  // async formSubmitAddMember(){
  //     let form_add = this.form;
  //     try {
  //       this.service    = this.evoucherService;
  //       const result: any  = await this.evoucherService.uploadEvoucherCodeBulk(form_add);
  //       console.warn("result add member", result)
  
  //       if (result && result.data) {
  //         this.getPassword = result.data.password;
  //       }

  //       this.data.username = this.form.id_pel;
  //       this.data.password = this.getPassword;
  //       Swal.fire({
  //         title: 'Success',
  //         text: 'Pelanggan berhasil ditambahkan',
  //         icon: 'success',
  //         confirmButtonText: 'Ok',
  //       }).then((result) => {
  //         if(result.isConfirmed){
  //           this.showThisPassword();
  //         }
  //       });
  //     } catch (e) {
  //       console.warn("error", e.message)
  //       Swal.fire({
  //         title: 'Failed',
  //         text: e.message,
  //         icon: 'warning',
  //         confirmButtonText: 'Ok',
  //       }).then((result) => {
  //         if(result.isConfirmed){
  //           this.firstLoad();
  //         }
  //       });
  //       this.errorLabel = ((<Error>e).message);//conversion to Error type
  //     }
  // }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    // var reader = new FileReader();
    // reader.readAsDataURL(event.target.files[0]); //
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    
    this.selectedFile = event.target.files[0];
    this.addBulk.nativeElement.value = '';
    
  } 

  cancelThis(){
    this.cancel = !this.cancel;
  }

  actionShowUploadButton() {
    this.showUploadButton = !this.showUploadButton;
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  backTo(){
    window.history.back();
  }

}
