import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EwalletSalesReportRoutingModule } from './ewallet-sales-report-routing.module';
import { EwalletSalesReportComponent } from './ewallet-sales-report.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { EwalletSalesReportDetailComponent } from './detail/ewallet-sales-report.detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
// import { BastComponent } from './bast/bast.component';

import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  
  imports: [
    CommonModule,
    EwalletSalesReportRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    ProgressbarModule.forRoot(),
  ],
    declarations: [
      EwalletSalesReportComponent,
      EwalletSalesReportDetailComponent
    ],
  
})
export class EwalletSalesReportModule { }
