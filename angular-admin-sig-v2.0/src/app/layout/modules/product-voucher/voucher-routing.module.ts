
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductAddComponent } from './add/product.add.component';
import { ProductDetailComponent } from './detail/product.detail.component';
import { MerchantEditProductComponent } from '../../merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component';
import { ProductEditComponent } from './edit/product.edit.component';
import { VouchersComponent } from './voucher.component';

const routes: Routes = [
  {
      path: '', component: VouchersComponent
  },
  {
    path:'add', component: ProductAddComponent
  },
  // {
  //   path:'detail', component: ProductDetailComponent
  // },
  {
    path: 'edit', component: ProductEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VouchersRoutingModule { }
