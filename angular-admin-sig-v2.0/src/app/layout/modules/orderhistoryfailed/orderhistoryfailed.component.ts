import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orderhistoryfailed',
  templateUrl: './orderhistoryfailed.component.html',
  styleUrls: ['./orderhistoryfailed.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistoryfailedComponent implements OnInit {

  Orderhistoryfailed: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Order History Failed Detail Page',
                                  label_headers   : [
                                    {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
                                    {label: 'Order date', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'Expired date', visible: true, type: 'string', data_row_name: 'payment_expire_date'},
                                    { label: 'Merchant Name', visible: true, type: 'string', data_row_name: 'merchant_name' },
                                    {label: 'User Id', visible: true, type: 'string', data_row_name: 'user_id'},
                                    // unfinished
                                    {label: 'User Name', visible: true, type: 'string', data_row_name: 'user_name'},
                                    // unfinished
                                    {label: 'User Email', visible: true, type: 'string', data_row_name: 'email'},
                                    {label: 'Status', visible: true, type: 'string', data_row_name: 'status'},
                                    // unfinished
                                    {label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price'}
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Orderhistoryfailed',
                                                    detail_function: [this, 'callDetail']
                                                  },
                                  show_checkbox_options: true
  };

  form_input : any = false;
  errorLabel : any = false;
  totalPage: 0;

  orderhistoryfailedDetail: any=false;
  service: any;
  constructor(public orderhistoryService:OrderhistoryService, private router: Router) { }

  // ngOnInit() {
  //   this.orderhistoryService.getOrderhistory().subscribe(data=>{
  //     let result: any = data;
  //     console.log(result.result);
  //     this.Orderhistory = result.result;

  //     //display and convert shoppingcart_id in foreach because type data is ObjectID
  //     this.Orderhistory.forEach((data, index)=>{this.Orderhistory[index].shoppingcart_id = this.Orderhistory[index].shoppingcart_id.$oid})
  //   });
  // }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    try{
      this.service    = this.orderhistoryService;
      let result: any  = await this.orderhistoryService.getOrderhistoryfailedLint();
      this.totalPage = result.result.total_page;
      console.log(result);
      
      this.Orderhistoryfailed = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async callDetail(orderhistory_id){

    this.router.navigate(['administrator/orderhistoryfailedamin/edit'], {queryParams: {id:orderhistory_id}})
    // try{
    //   let result: any;
    //   this.service    = this.orderhistoryService;
    //   result          = await this.orderhistoryService.detailOrderhistoryfailed(orderhistory_id);
    //   console.log(result);
      
    //   this.orderhistoryfailedDetail = result.result[0];
      
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  public async backToHere(obj){
    obj.orderhistoryfailedDetail = false;
  }

}
