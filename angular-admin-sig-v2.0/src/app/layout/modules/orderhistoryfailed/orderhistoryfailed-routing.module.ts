import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderhistoryfailedComponent } from './orderhistoryfailed.component';
import { OrderhistoryfailedDetailComponent } from './detail/orderhistoryfailed.detail.component';

// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';


const routes: Routes = [
  {
      path: '', component: OrderhistoryfailedComponent
  },
  // {
  //   path:'detail', component: OrderhistorysuccessDetailComponent
  // },
  // {
  //   path:'edit', component: OrderhistorysummaryEditComponent
  // }

  {
    path: 'edit', component: OrderhistoryfailedDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistoryfailedRoutingModule { }
