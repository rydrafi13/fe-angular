import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PointsCampaignService } from '../../../services/points.campaign/pointscampaign.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-points-campaign',
  templateUrl: './pointscampaign.component.html',
  styleUrls: ['./pointscampaign.component.scss'],
  animations: [routerTransition()]
})

export class PointsCampaignComponent implements OnInit {
  @Input() public detail: any;

  PointsCampaign       : any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Active Points Campaign Detail',
                                  label_headers   : [
                                    {label: 'Merchant ID', visible: true, type: 'string', data_row_name: 'merchant_id'},
                                    {label: 'Campaign Name', visible: true, type: 'string', data_row_name: 'campaign_name'},
                                      // {label: 'Campaign Description', visible: false, type: 'string', data_row_name: 'campaign_description'},
                                      {label: 'Status',
                                          options: [
                                              'active',
                                              'inactive',
                                          ],
                                          visible: true, type: 'string', data_row_name: 'status'},
                                      // {label: 'Start Date', visible: false, type: 'date', data_row_name: 'start_date'},
                                      // {label: 'End Date', visible: false, type: 'date', data_row_name: 'start_date'},
                                      ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                      addForm: true,
                                                    this  : this,
                                                    result_var_name: 'PointsCampaign',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  pointsCampaignDetail: any = false;
  service     : any ;


  constructor(public pointsCampaignService:PointsCampaignService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      
      this.service    = this.pointsCampaignService;
      const result: any  = await this.pointsCampaignService.getAllPointsCampaignLint();
      this.PointsCampaign = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {
    
    try {
      
      this.service    = this.pointsCampaignService;
      let result: any = await this.pointsCampaignService.detailPointsCampaign(_id);
      this.pointsCampaignDetail = result.result;
      console.log(this.pointsCampaignDetail);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.pointsCampaignDetail = false;
  }

}
