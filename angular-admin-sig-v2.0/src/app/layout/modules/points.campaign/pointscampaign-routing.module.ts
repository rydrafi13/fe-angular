import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PointsCampaignComponent } from './pointscampaign.component';
import { PointsCampaignAddComponent } from './add/pointscampaign.add.component';
import { PointsCampaignDetailComponent } from './detail/pointscampaign.detail.component';

const routes: Routes = [
  {
      path: '', component: PointsCampaignComponent,

  },
  {
      path:'add', component: PointsCampaignAddComponent
  },
  {
    path:'detail', component: PointsCampaignDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PointsCampaignRoutingModule { }
