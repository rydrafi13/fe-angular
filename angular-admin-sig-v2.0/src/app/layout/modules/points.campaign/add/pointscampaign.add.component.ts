import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PointsCampaignService } from '../../../../services/points.campaign/pointscampaign.service';

@Component({
  selector: 'app-points-campaign-add',
  templateUrl: './pointscampaign.add.component.html',
  styleUrls: ['./pointscampaign.add.component.scss'],
  animations: [routerTransition()]
})

export class PointsCampaignAddComponent implements OnInit {
  public name: string = "";
  public merchantList: any = [];
  PointsCampaign: any = [];
  service: any;
  errorLabel : any = false;
  merchant   : any = false;
  merchant_id: any;
  org_id     : any;
  campaign_name : any;
  campaign_description : any;
  status     : any;
  start_date : any;
  end_date   : any;

  public campaignStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];
                
  constructor(public pointsCampaignService:PointsCampaignService) {
    
   }

  ngOnInit() {
    this.getMerchant();
  
  }

  async getMerchant(){
    try{
      this.merchant = await this.pointsCampaignService.getMerchantLint();
      this.merchant.result.forEach((element) => {
        this.merchantList.push({label:element.merchant_name, value:element.merchant_id});
      });
      console.log('merchant : ', this.merchant);
    }
    catch (e){
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;

  }
  formSubmitAddPointsCampaign(form){
    // console.log(form);
    // console.log(JSON.stringify(form));

    try {
      this.service    = this.pointsCampaignService;
      const result: any  = this.pointsCampaignService.addNewCampaign(form);
      this.PointsCampaign = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

}


}