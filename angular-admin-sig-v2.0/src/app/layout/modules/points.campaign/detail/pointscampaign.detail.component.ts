import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PointsCampaignService } from '../../../../services/points.campaign/pointscampaign.service';
import { del } from 'selenium-webdriver/http';

@Component({
  selector: 'app-points-campaign-detail',
  templateUrl: './pointscampaign.detail.component.html',
  styleUrls: ['./pointscampaign.detail.component.scss'],
  animations: [routerTransition()]
})

export class PointsCampaignDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public merchantList: any = [];
  merchant : any = false;

  edit: boolean = false;
  errorLabel : any = false;
  constructor(public pointscampaignService: PointsCampaignService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){

  }

  editThis() {
    // console.log(this.edit );
    this.getMerchant();
    // console.log(this.edit );
  }
  backToTable() {
    // console.log(this.back);
    this.back[1](this.back[0]);
  }

  async getMerchant(){
    try{
      this.merchant = await this.pointscampaignService.getMerchantLint();
      this.merchant.result.forEach((element) => {
        this.merchantList.push({label:element.merchant_name, value:element.merchant_id});
      });

      this.edit = !this.edit;
    }
    catch (e){
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  async deleteThis(){
    try {
      const delResult: any = await this.pointscampaignService.delete(this.detail);
      console.log(delResult);
      if (delResult.error == false) {
        console.log(this.back[0]);
        this.back[0].pointsgroupDetail = false;
        this.back[0].firstLoad();
      // delete this.back[0].prodDetail;
      }
    } catch (error) {
      throw new TypeError(error.error.error);
    }
  } catch (e) {
    this.errorLabel = ((<Error>e).message);//conversion to Error type
  }

}
