import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../../router.animations';
import { Router,ActivatedRoute } from '@angular/router';
import { PointsTransactionService } from '../../../../../services/points-transaction/points-transaction.service';
import {FormOptions, TableFormat} from '../../../../../object-interface/common.object';

@Component({
  selector: 'app-member-point-history',
  templateUrl: './member-point-history.component.html',
  styleUrls: ['./member-point-history.component.scss']
})
export class MemberPointHistoryComponent implements OnInit {

  Pointstransaction: any = [];
  id;
  tableFormat        : TableFormat = {
                                  title           : 'Point History Report ',
                                  label_headers   : [
                                    {label: 'Updated Date', visible: true, type: 'string', data_row_name: 'updated_date'},
                                    {label: 'ID Customer', visible: true, type: 'string', data_row_name: 'username'},
                                    // {label: 'Reference No', visible: true, type: 'string', data_row_name: 'reference_no'},
                                    {label: 'Nama Pemilik', visible: true, type: 'string', data_row_name: 'nama_pemilik'},
                                    {label: 'Prev Points Balance', visible: true, type: 'string', data_row_name: 'prev_points_balance'},
                                    {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                                    {label: 'Current Points Balance', visible: true, type: 'string', data_row_name: 'current_points_balance'},
                                    {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                                    // {label: 'Payment Via', visible: true, type: 'string', data_row_name: 'payment_via'},
                                    // {label: 'Status Payment', visible: true, type: 'string', data_row_name: 'status_payment'},
                                    // {label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date'},
                                    // {label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Pointstransaction',
                                                    detail_function:[]
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;
  member_email : 'sri.zetka@gmail.com'

  pointstransactionDetail: any = false;
  service: any;
  constructor(public PointService:PointsTransactionService,private _Activatedroute:ActivatedRoute,private route: ActivatedRoute) { }

  ngOnInit() {
    this.id=this._Activatedroute.snapshot.params['id'];
   
    this.firstLoad();

  }
  
  async firstLoad(){
    this.route.queryParams.subscribe(async (params) => {
      this.tableFormat.title += '( user : ' + params.id + ' )'
      let id = params.id;
      // this.loadDetail(historyID)
      try{
        var params1 = {
           search: {start_date:this.addMonths(new Date(), -6),end_date:new Date(), member_email : id,},
           order_by:{},
           current_page:1,
           limit_per_page:50
          }
          // {"search":{"start_date":"2019-01-10T08:19:13.799Z","end_date":"2020-08-13T08:19:13.799Z","member_email":"sri.zetka@gmail.com"},"order_by":{},"limit_per_page":null,"current_page":1,"download":false}
        this.service    = this.PointService;
        let result: any  = await this.PointService.getPointReportLint(params1);
        this.totalPage = result.result.total_page
        this.Pointstransaction = result.result.values;
      } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
    })
    console.log(' admin payment',this.id)
   
  }
  
  // public async callDetail(pointstransaction_id){
  //   try{
  //     let result: any;
  //     this.service    = this.OrderhistoryService;
  //     result          = await this.OrderhistoryService.detailPointstransaction(pointstransaction_id);
  //     console.log(result);

  //     this.pointstransactionDetail = result.result[0];

  //   } catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }
  // }

  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}
