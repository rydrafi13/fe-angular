import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemberComponent } from './member.component';
import { MemberAddComponent } from './add/member.add.component';
import { MemberDetailComponent } from './detail/member.detail.component';
import { MemberPointHistoryComponent } from './detail/member-point-history/member-point-history.component';

const routes: Routes = [
  {
      path: '', component: MemberComponent,

  },
  {
      path:'add', component: MemberAddComponent
  },
  {
    path:'detail', component: MemberDetailComponent
},
  

    // { path: 'detail', loadChildren: () => import(`./detail/member.detail-routing.module`).then(m => m.MemberDetailRoutingModule) },
    {
      path:'point-history', component: MemberPointHistoryComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule { }
