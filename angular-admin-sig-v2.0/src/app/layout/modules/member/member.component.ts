import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { MemberService } from '../../../services/member/member.service';
import { DomSanitizer } from '@angular/platform-browser';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;


@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss'],
  animations: [routerTransition()]
})

export class MemberComponent implements OnInit {

  Members       : any = [];
  row_id        : any = "_id";

  tableFormat   : TableFormat = {
    title           : 'Active Members Detail',
    label_headers   : [
                  {label: 'ID Customer', visible: true, type: 'string', data_row_name: 'username'},
                  {label: 'Name', visible: true, type: 'string', data_row_name: 'full_name'},
                  // {label: 'Email',     visible: true, type: 'string', data_row_name: 'email'},
                  // {label: 'Birthdate',     visible: true, type: 'date', data_row_name: 'dob'},
                  {label: 'Phone Number',     visible: true, type: 'string', data_row_name: 'cell_phone'},
                  // {label: 'Gender',     visible: true, type: 'string', data_row_name: 'gender'},
                  // {label: 'Address',     visible: true, type: 'string', data_row_name: 'address'},
                  // {label: 'Membership',     visible: true, type: 'string', data_row_name: 'membership'},
                  {label: 'Points',     visible: true, type: 'string', data_row_name: 'points'},
                  {label: 'Join Date',     visible: true, type: 'date', data_row_name: 'created_date'},
                ],
    row_primary_key : '_id',
    formOptions     : {
                    addForm   : true,
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'Members',
                    detail_function : [this, 'callDetail'] 
                    }
                  };

  form_input    : any = {};
  errorLabel    : any = false;
  totalPage     : 0;
 
  memberDetail  : any = false;
  service       : any ;
  srcDownload   : any ;
  

  // private options = new RequestOptions(
  //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(public memberService:MemberService , public sanitizer:DomSanitizer) {

    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      // let params = {
      //   'type':'member'
      // }
      this.service    = this.memberService;

      const result: any  = await this.memberService.getMemberLint();
      this.totalPage = result.result.total_page;
      this.Members = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {
    
    try {
      this.service    = this.memberService;
      let result: any = await this.memberService.detailMember(_id);
      this.memberDetail = result.result[0];
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.memberDetail = false;
  }

  onDownload(downloadLint)
  {
    let srcDownload = downloadLint;
    //let srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
    //console.log(this.srcDownload);

    this.getDownloadFileLint();
  }

  public async getDownloadFileLint() {
    try
    {

      let result: any;
      this.service    = this.memberService;
      result= await this.memberService.getDownloadFileLint();
      //console.log(result.result);
      let downloadLint = result.result;
      //console.log(downloadLint);

      //To running other subfunction with together automatically
      this.onDownload(downloadLint);
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  } 

  // public async pagination() {
  //   try
  //   {
  //     let result: any;
  //     this.service = this.memberService;
  //     result = await this.memberService.getDownloadFileLint();
  //     //console.log(result.result);
  //     let downloadLint = result.result;
  //     //console.log(downloadLint);

  //     //To running other subfunction with together automatically
  //     this.onDownload(downloadLint);
  //   }
  //   catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }  
  // }

}
