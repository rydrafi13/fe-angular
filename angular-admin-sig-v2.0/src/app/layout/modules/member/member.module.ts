import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MemberComponent } from './member.component';
import { MemberAddComponent } from './add/member.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { MemberRoutingModule } from './member-routing.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { MemberDetailComponent } from './detail/member.detail.component';
import { MemberEditComponent } from './edit/member.edit.component';
import { MemberPointHistoryComponent } from './detail/member-point-history/member-point-history.component';

@NgModule({
  imports: [CommonModule,
    MemberRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
  ],
  
  declarations: [
    MemberComponent, MemberAddComponent, MemberDetailComponent, MemberEditComponent, MemberPointHistoryComponent],

  // exports : [MemberComponent]
})
export class MemberModule { }
