"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MemberModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var member_component_1 = require("./member.component");
var member_add_component_1 = require("./add/member.add.component");
var shared_1 = require("./../../../shared");
var forms_1 = require("@angular/forms");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var form_builder_table_module_1 = require("../../../component-libs/form-builder-table/form-builder-table.module");
var member_routing_module_1 = require("./member-routing.module");
var bs_component_module_1 = require("../bs-component/bs-component.module");
var member_detail_component_1 = require("./detail/member.detail.component");
var member_edit_component_1 = require("./edit/member.edit.component");
var member_point_history_component_1 = require("./detail/member-point-history/member-point-history.component");
var MemberModule = /** @class */ (function () {
    function MemberModule() {
    }
    MemberModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule,
                member_routing_module_1.MemberRoutingModule,
                shared_1.PageHeaderModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ng_bootstrap_1.NgbModule,
                form_builder_table_module_1.FormBuilderTableModule,
                bs_component_module_1.BsComponentModule
            ],
            declarations: [
                member_component_1.MemberComponent, member_add_component_1.MemberAddComponent, member_detail_component_1.MemberDetailComponent, member_edit_component_1.MemberEditComponent, member_point_history_component_1.MemberPointHistoryComponent
            ]
        })
    ], MemberModule);
    return MemberModule;
}());
exports.MemberModule = MemberModule;
