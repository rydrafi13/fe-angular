import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { AdminService } from '../../../../services/admin/admin.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin-add',
  templateUrl: './admin.add.component.html',
  styleUrls: ['./admin.add.component.scss'],
  animations: [routerTransition()]
})

export class AdminAddComponent implements OnInit {
  public name: string = "";
  Members: any = [];
  service: any;
  full_name:any;
  email:any;
  password:any;
  cell_phone:any;
  card_number:any;
  dob:any;
  mcity:any;
  mstate:any;
  mcountry:any;
  maddress1:any;
  mpostcode:any;
  gender:any;
  marital_status:any;
  type:any;
  member_status:any;
  activation_status:any;
  public form: any = {
    username: "",
    full_name: "",
    email: "",
    password:"",
    group_name:""
  }

  public alertUsername:boolean = false;
  public alertFullname:boolean = false;
  public alertEmail:boolean = false;
  public alertPassword:boolean = false;
  public alertGroupname:boolean = false;

  
  errorLabel : any = false;
  errorMessage : any = false;

  public groupNameData: any = [];
                
  constructor(public adminService:AdminService) {}

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){
    try {
      await this.adminService.getAdminGroup().then((result) => {
        result.forEach((element, index) => {
          this.groupNameData.push(
            {
              label : element.group_name,
              value: element.group_name,
            }
          );
  
          if(index == 0) {
            this.form.group_name = element.group_name;
          }
          // if (element.group_name == this.detail.group_name) {
          //   this.groupNameData[index].selected = 1;
          // }
        });
      });
    } catch (error) {
      this.errorLabel = ((<Error>error).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }

  }
  async formSubmitAddMember(){
    if(!this.form.full_name  || this.form.full_name.length == 0){
      this.alertFullname = true;
    } else {
      this.alertFullname = false;
    }
    
    if(!this.form.username  || this.form.username.length == 0){
      this.alertUsername = true;
    } else {
      this.alertUsername = false;
    }
    
    if(!this.form.email  || this.form.email.length == 0){
      this.alertEmail = true;
    } else {
      this.alertEmail = false;
    }
    
    if(!this.form.password || this.form.password.length == 0){
      this.alertPassword = true;
    } else {
      this.alertPassword = false;
    }
    
    if(!this.form.group_name  || this.form.group_name.length == 0){
      this.alertGroupname = true;
    } else {
      this.alertGroupname = false;
    }
    
    if(!this.alertFullname && !this.alertUsername && !this.alertEmail && !this.alertPassword && !this.alertGroupname) {
      await this.adminService.addAdmin(this.form).then(() => {
        Swal.fire("Member Has Been Added!")
        .then(() => {
          this.backTo();
        });
      }).catch(error => {
        this.errorLabel = ((<Error>error).message);
        let message = this.errorLabel;
        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
        }
        Swal.fire({
          icon: 'error',
          title: message,
        })
        console.log(error);
      });
    }

}

backTo(){
  window.history.back();
}

}
