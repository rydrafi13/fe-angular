import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { AdminService } from '../../../../services/admin/admin.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-admin-change-password',
  templateUrl: './admin.change.password.component.html',
  styleUrls: ['./admin.change.password.component.scss'],
  animations: [routerTransition()]
})

export class AdminChangePasswordComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  member_status:any;
  activation_status:any;
  
  public loading        : boolean = false;

  public memberStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  public groupNameData: any = [];

  public alertUsername:boolean = false;
  public alertPassword:boolean = false;

  errorMessage  : any = false;

  constructor(public adminService : AdminService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    this.detail.password = "";
    try {
      this.memberStatus.forEach((element, index) => {
        if (element.value == this.detail.status) {
            this.memberStatus[index].selected = 1;
        }
      });

      await this.adminService.getAdminGroup().then((result) => {
        result.forEach((element, index) => {
          this.groupNameData.push(
            {
              label : element.group_name,
              value: element.group_name,
            }
          );
          if (element.group_name == this.detail.group_name) {
            this.groupNameData[index].selected = 1;
          }
        });
      });
    } catch(err) {
      this.errorLabel = ((<Error>err).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    if(!this.detail.username  || this.detail.username.length == 0){
      this.alertUsername = true;
    } else {
      this.alertUsername = false;
    }
    
    if(!this.detail.password || this.detail.password.length == 0){
      this.alertPassword = true;
    } else {
      this.alertPassword = false;
    }
    
    if(!this.alertUsername && !this.alertPassword) {
      let new_form = {
        "username" : this.detail.username,
        "password" : this.detail.password,
      }
      
      try {
        await this.adminService.changePasswordAdmin(new_form).then(() => {
          Swal.fire("Admin Password Has Been Changed!")
          .then(() => {
            this.backToDetail();
          });
        }).catch(error => {
          this.errorLabel = ((<Error>error).message);
          let message = this.errorLabel;
          if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
          }
          Swal.fire({
            icon: 'error',
            title: message,
          })
          console.log(error);
        });
      } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
        alert("Error");
      }
    }
  }
}
