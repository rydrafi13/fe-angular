import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminAddComponent } from './add/admin.add.component';
import { AdminDetailComponent } from './detail/admin.detail.component';
// import { UserPointHistoryComponent } from './detail/member-point-history/member-point-history.component';

const routes: Routes = [
  {
      path: '', component: AdminComponent,

  },
  {
      path:'add', component: AdminAddComponent
  },
  {
    path:'detail', component: AdminDetailComponent
},
  

    // { path: 'detail', loadChildren: () => import(`./detail/member.detail-routing.module`).then(m => m.MemberDetailRoutingModule) },
    // {
    //   path:'point-history', component: MemberPointHistoryComponent
    // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
