import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminComponent } from './admin.component';
import { AdminAddComponent } from './add/admin.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { AdminRoutingModule } from './admin-routing.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { AdminDetailComponent } from './detail/admin.detail.component';
import { AdminEditComponent } from './edit/admin.edit.component';
import { AdminChangePasswordComponent } from './change-password/admin.change.password.component';
// import { UserPointHistoryComponent } from './detail/user-point-history/member-point-history.component';

@NgModule({
  imports: [CommonModule,
    AdminRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
  ],
  
  declarations: [
    AdminComponent, AdminAddComponent, AdminDetailComponent, AdminEditComponent, AdminChangePasswordComponent
    // MemberPointHistoryComponent
  ],

  // exports : [MemberComponent]
})
export class AdminModule { }
