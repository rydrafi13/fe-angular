import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { DomSanitizer } from '@angular/platform-browser';
import { TopBarMenuItem, GeneralComponent} from '../../../../object-interface/common.object';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { ActivityService } from '../../../../services/activity/activity.service';
import { ProductService } from '../../../../services/product/product.service';


@Component({
  selector: 'app-product-activity-report-summary',
  templateUrl: './product-activity-report-summary.component.html',
  styleUrls: ['./product-activity-report-summary.component.scss'],
  animations: [routerTransition()]
})


export class ProductActivityReportSummaryComponent implements OnInit , GeneralComponent{

  title: "User Activity Report Page"
  data       : any = [];
  service    ;
  errorLabel : string
  // topBarMenu : TopBarMenuItem[] =[
  //   {label: "Number of Sales", routerLink: '/administrator/order-history-summary'},
  //   {label: "Amount of Sales", routerLink: '/order-amount-history-summary'},
  //   {label: "Abondonment Rate", routerLink: '/cart-abandonment-summary', active: true}
  // ];
  topMembers : []
  
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          callback: this.currencyFormatter,
        }
      }]
    },
  };

  public barChartLabels : any     = [];
  public barChartType   : string  = 'bar';
  public barChartLegend : boolean = true;

  public barChartData: any = [
      { data: [], label: 'user active each platform today' },
  ];

  public optO = {fill:false , borderWidth: 1,}
  public analitycsData:any = {   
    eachPlatformToday : {
      barChartData : [{ data: [], label: 'each platform' , ...this.optO}],
      barChartLabels : []
    },
    eachPageToday : {
      barChartData : [{ data: [], label: 'each page' , ...this.optO}],
      barChartLabels : []
    },
   
  }

  constructor(public productService:ProductService , 
              public sanitizer:DomSanitizer, 
              public activityService:ActivityService) {

    this.service = productService;
    this.activityService.addLog({ page : this.title });
  }

  onUpdateCart(data){
    const clonedData  = JSON.parse(JSON.stringify(data.barChartData));
    const clone       = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data     = [];

    data.barChartLabels.forEach(()=>{ clone[0].data.push(Math.round(Math.random() * 100)) });
    data.barChartData = clone;
    
    setTimeout(()=>{
      data.barChartData   = clonedData;
    },500)
    
  }

  async ngOnInit() {
    let currentDate = new Date();
    console.log(this.barChartLabels);
    this.firstLoad();

  }

  generateChartData(dataValues){

    let barChartLabels = [];
    let newData        = [];


    let allData:any;
    allData = dataValues;

    let total = 0;
    for(let data in allData){
      // console.log('allData data', allData[data])
      barChartLabels.push(data)
      total += parseInt(allData[data]);
      newData.push(allData[data])

    }
    allData = null; //clearing memory
    // console.log("new Data", newData)
   
    
    return [barChartLabels, newData, total]
  }


  async firstLoad() {

    try {
      this.service       = this.productService;
      const result: any  = await this.productService.getProductActivityReport();
      this.data          = result.result;
      
      // if(this.data.user_active_each_platform_today){
        
      //   const arEachPlatformToday:any    = this.generateChartData( this.data.user_active_each_platform_today);
       
      //   // this.avgData.today.all      = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day.value));
      //   // this.avgData.today.paid     = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_paid.value));
      
      //   const clone = JSON.parse(JSON.stringify(this.analitycsData.eachPlatformToday.barChartData));
      //   clone[0].data         = arEachPlatformToday[1];
       
      //   this.analitycsData.eachPlatformToday.barChartLabels = arEachPlatformToday[0]
      //   this.barChartLabels   = arEachPlatformToday[0]
        
      //   this.analitycsData.eachPlatformToday.barChartData = clone;

      // }

      // if(this.data.user_active_each_page_today){
        
      //   const arEachPageToday:any    = this.generateChartData( this.data.user_active_each_page_today);
       
      //   // this.avgData.today.all      = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day.value));
      //   // this.avgData.today.paid     = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_paid.value));
      
      //   const clone = JSON.parse(JSON.stringify(this.analitycsData.eachPageToday.barChartData));

      //   clone[0].data = arEachPageToday[1];

      //   this.analitycsData.eachPageToday.barChartLabels = arEachPageToday[0]

      //   this.barChartLabels = arEachPageToday[0]
      //   this.analitycsData.eachPageToday.barChartData = clone;

      // }
     
    } catch (e) {
      console.log("this e result", e)

      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  calculateTheAverageValue( _value: Object){
    let howMany:number = Object.keys(_value).length
    let sum = 0;

    Reflect.ownKeys(_value).forEach((key)=>{
      sum = sum + _value[key];
    });
    
    let avg = sum/howMany;
    return avg;
  }
  

  currencyFormatter(value, index, values) {

    // add comma as thousand separator
    return value + '%';
  
}

}
