import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemberDemographyReportComponent } from './member-demography-report.component';

const routes: Routes = [
  {
      path: '', component: MemberDemographyReportComponent,

  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberDemographyReportRoutingModule { }
