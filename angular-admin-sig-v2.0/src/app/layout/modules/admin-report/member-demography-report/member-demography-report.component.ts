import { Component, OnInit } from '@angular/core';
import { GeneralComponent, TopBarMenuItem } from '../../../../object-interface/common.object';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivityService } from '../../../../services/activity/activity.service';
import { MemberService } from '../../../../services/member/member.service';

@Component({
  selector: 'app-member-demography-report',
  templateUrl: './member-demography-report.component.html',
  styleUrls: ['./member-demography-report.component.scss']
})
export class MemberDemographyReportComponent implements OnInit, GeneralComponent {


  title: "Member Demography Report"
  data: any = [];
  service:any;
  errorLabel: string
  
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    
  };

  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public optO = { fill: false, borderWidth: 1, }
  public demographyData: any = {
    age:{
      barChartData: [{ data: [], label: 'Age Demography', ...this.optO }],
      barChartLabels: []
    },
    gender:{
      barChartData: [{ data: [], label: 'Gender Demography', ...this.optO }],
      barChartLabels: []
    }
  }

  constructor(public memberReportService: MemberService,
    public sanitizer: DomSanitizer,
    public activityService: ActivityService) {

    this.service = memberReportService;
    this.activityService.addLog({ page: this.title });
  }

  async ngOnInit() {
    let currentDate = new Date();

    this.firstLoad();

  }



  async firstLoad() {

    try {
      this.service = this.memberReportService;
      const resultAge: any = await this.memberReportService.getMemberDemographyReport('age');
      const resultGender: any = await this.memberReportService.getMemberDemographyReport('gender');
      this.data = {
        age: resultAge.result,
        gender: resultGender.result
      }
      
      if (this.data) {
        
        let cloneAge    =  await JSON.parse(JSON.stringify(this.demographyData.age.barChartData));
        let cloneGender =  await JSON.parse(JSON.stringify(this.demographyData.gender.barChartData));
       

        let convertedData:any = {
          age: this.convertToReport(this.data.age),
          gender: this.convertToReport(this.data.gender)
        }
        

        AGE:{
         
          cloneAge[0].data = convertedData.age.data;
          this.demographyData.age.barChartLabels = convertedData.age.label;
          this.demographyData.age.barChartData   = cloneAge;
        }
        
        GENDER:{
          cloneGender[0].data = convertedData.gender.data;
          this.demographyData.gender.barChartLabels = convertedData.gender.label;
          this.demographyData.gender.barChartData   = cloneGender;
        }

        console.log("CLONE convertedData here ", this.demographyData)


      }

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  convertToReport(data) {
    let dt  = [];
    let lbl = [];

    for (let obj in data) {
      dt.push(data[obj])
      lbl.push(obj)
    }

    return { data: dt, label: lbl }
  }

}
