import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationGroupComponent } from './notificationgroup.component';

import { NotificationGroupAddComponent } from './add/notificationgroup.add.component';

import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { NotificationGroupRoutingModule } from './notificationgroup-routing.module';

import { BsComponentModule } from '../bs-component/bs-component.module';
import { NotificationGroupDetailComponent } from './detail/notificationgroup.detail.component';
import { NotificationGroupEditComponent } from './edit/notificationgroup.edit.component';

@NgModule({
  imports: [CommonModule,
    NotificationGroupRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
],
  
  declarations: [

    NotificationGroupComponent,NotificationGroupAddComponent,NotificationGroupDetailComponent,NotificationGroupEditComponent] 

})
export class NotificationGroupModule { }
