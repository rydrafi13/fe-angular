import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import { DomSanitizer } from '@angular/platform-browser';
import {FormOptions, TableFormat} from '../../../../object-interface/common.object';


// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;


@Component({
  selector: 'app-all-member',
  templateUrl: './allmember.component.html',
  styleUrls: ['./allmember.component.scss'],
  animations: [routerTransition()]
})

export class AllMemberComponent implements OnInit {

  AllMember       :any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Active All Members Detail',
                                  label_headers   : [
                                    {label: 'Join At', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'Full Name', visible: true, type: 'string', data_row_name: 'full_name'},
                                    {label: 'Username', visible: true, type: 'string', data_row_name: 'username'},
                                    // {label: 'Owner',     visible: true, type: 'string', data_row_name: 'owner'},
                                    // {label: 'Merchant Code', visible: false, type: 'string', data_row_name: 'merchant_code'},
                                    {label: 'Email', visible: true, type: 'string', data_row_name: 'email'},
                                    {label: 'Phone', visible: true, type: 'string', data_row_name: 'cell_phone'},
                                    // {label: 'State', visible: false, type: 'string', data_row_name: 'state'},
                                    // {label: 'User ID', visible: false, type: 'string', data_row_name: 'user_id'},
                                    {label: 'Type', visible: true, type: 'string', data_row_name: 'type'},
                                    {label: 'Created Date',visible: true, type: 'date', data_row_name: 'created_date'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'AllMember',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input : any = {};
  errorLabel : any = false;
  totalPage : 0;

  allMemberDetail: any = false;
  service     : any ;
  srcDownload  :any ;

  // private options = new RequestOptions(
  //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(public memberService:MemberService , public sanitizer:DomSanitizer) {
    
    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.service    = this.memberService;
      const result: any  = await this.memberService.getAllMembersLint();
      console.log("TEST", result);
      this.totalPage = result.result.total_page;
      this.AllMember = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {
    
    try {
      this.service    = this.memberService;
      let result: any = await this.memberService.detailMember(_id);
      this.allMemberDetail = result.result[0];
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.allMemberDetail = false;
  }

  onDownload()
  {
    let srcDownload="http://localhost:8888/f3/assets/csv/members_report.csv";
    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
    console.log(this.srcDownload);

  }

  public async getDownloadFileLint() {
    try {

      let result:any;
      this.service    = this.memberService;
      result= await this.memberService.getDownloadFileLint();
      console.log(result);
      this.AllMember = result.result;
      console.log(this.AllMember);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }


}
