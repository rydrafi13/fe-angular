import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllMemberComponent } from './allmember.component';
import { AllMemberAddComponent } from './add/allmember.add.component';
import { AllMemberDetailComponent } from './detail/allmember.detail.component';
import { MemberDetailComponent } from '../../member/detail/member.detail.component';

const routes: Routes = [
  {
      path: '', component: AllMemberComponent,

  },
  {
      path:'add', component: AllMemberAddComponent
  },
  {
    path:'detail', component: AllMemberDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllMemberRoutingModule { }
