import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-orderhistorycomplete-detail',
  templateUrl: './orderhistorycomplete.detail.component.html',
  styleUrls: ['./orderhistorycomplete.detail.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistorycompleteDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  orderhistoryDetail: any;
  service;
  errorLabel;

  constructor(
    private orderhistoryService: OrderhistoryService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    this.orderhistoryDetail = JSON.parse(JSON.stringify(this.detail));

    // this.route.queryParams.subscribe(async (params) => {
    //   let historyID = params.id;
    //   this.loadHistoryDetail(historyID)


    // })


  }

  // private async loadHistoryDetail(historyID: string) {
  //   try {
  //     this.service = this.orderhistoryService;
  //     let result: any = await this.orderhistoryService.detailOrderhistory(historyID);
  //     // console.log("result", result);
  //     this.orderhistoryDetail = result.result[0];
  //     console.log(this.orderhistoryDetail);

  //     // this.orderhistoryDetail.merchant = true;
  //   }
  //   catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }
  // }
}
