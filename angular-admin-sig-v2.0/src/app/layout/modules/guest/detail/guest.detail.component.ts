import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';

@Component({
  selector: 'app-guest-detail',
  templateUrl: './guest.detail.component.html',
  styleUrls: ['./guest.detail.component.scss'],
  animations: [routerTransition()]
})

export class GuestDetailComponent implements OnInit {
  @Input() public detail:any;
  @Input() public back;
  errorLabel    :any = false;
  edit:boolean = false;
  
  constructor(public memberService:MemberService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

  async deleteThis(){
    try{
      let delResult:any = await this.memberService.deleteMember(this.detail);
      //console.log(delResult);
      if(delResult.error==false){
          console.log(this.back[0]);
          this.back[0].guestDetail=false;
          this.back[0].firstLoad();
          // delete this.back[0].guestDetail;
      } 
    } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
    }      
    
  }
}
