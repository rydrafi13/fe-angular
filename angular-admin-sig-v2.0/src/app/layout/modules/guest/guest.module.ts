import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuestComponent } from './guest.component';
import { GuestAddComponent } from './add/guest.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { GuestDetailComponent } from './detail/guest.detail.component';
import { GuestRoutingModule } from './guest-routing.module';
import { GuestEditComponent } from './edit/guest.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  imports: [CommonModule,
    GuestRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
],
  
  declarations: [
    GuestComponent, GuestAddComponent, GuestDetailComponent, GuestEditComponent] 
})
export class GuestModule { }
