import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoCampaignEditComponent } from './promo-campaign-edit.component';

describe('PromoCampaignEditComponent', () => {
  let component: PromoCampaignEditComponent;
  let fixture: ComponentFixture<PromoCampaignEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoCampaignEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoCampaignEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
