import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoCampaignAddComponent } from './promo-campaign-add.component';

describe('PromoCampaignAddComponent', () => {
  let component: PromoCampaignAddComponent;
  let fixture: ComponentFixture<PromoCampaignAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoCampaignAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoCampaignAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
