import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderBulkComponent } from './order-bulk.component';

const routes: Routes = [
  {
      path: '', component: OrderBulkComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderBulkComponentRoutingModule { }
