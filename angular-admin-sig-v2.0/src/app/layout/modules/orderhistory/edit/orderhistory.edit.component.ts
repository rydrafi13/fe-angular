import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';


@Component({
  selector: 'app-orderhistory-edit',
  templateUrl: './orderhistory.edit.component.html',
  styleUrls: ['./orderhistory.edit.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistoryEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading        :boolean = false;

  public transactionStatus  : any = [
    {label:"PENDING", value:"PENDING"}
   ,{label:"CANCEL", value:"CANCEL"}
   ,{label:"FAILED", value:"FAILED"}
   ,{label:"SUCCESS", value:"SUCCESS"}
   ,{label:"ERROR", value:"ERROR"}
];
  errorLabel    : any = false;
  transaction_status : any;


  constructor(public orderhistoryService:OrderhistoryService) {}

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    console.log("this.detail", this.transactionStatus);
    this.detail.previous_status = this.detail.status;
    this.transactionStatus.forEach((element, index) => {
        if(element.value == this.detail.transaction_status){
            this.transactionStatus[index].selected = 1;
        }
    });

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });
    
  }

  backToDetail(){
    console.log(this.back, this.back[0] && !this.isFunction(this.back[0]));
    if(this.back[0][this.back[1]] && !this.isFunction(this.back[0])){
        this.back[0][this.back[1]];
    }
    else if(this.isFunction(this.back[0])){
      this.back[0]();
    }
    else {
      window.history.back();
    }
  }

   isFunction(f): f is Function {
      return f instanceof Function;
  }

  async saveThis(){
    try 
    {
      this.loading=!this.loading;
      await this.orderhistoryService.updateOrderHistory(this.detail);
      this.loading=!this.loading;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
}
