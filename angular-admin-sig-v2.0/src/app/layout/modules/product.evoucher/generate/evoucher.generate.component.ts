import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ProductService } from '../../../../services/product/product.service';
import { EVoucherService } from '../../../../services/e-voucher/e-voucher.service';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
// import {Observable} from 'rxjs/Observable'

@Component({
  selector: 'app-evoucher-add',
  templateUrl: './evoucher.generate.component.html',
  styleUrls: ['./evoucher.generate.component.scss'],
  animations: [routerTransition()]
})

export class EvoucherGenerateComponent implements OnInit {
  @Input() products: Observable<any>;
  public name: string = "";
  public categoryProduct  : any = [];
  Evouchers: any = [];
  base_url: string ;
  pic_big_path: string;
  pic_medium_path:any;
  pic_small_path: any;
  pic_file_name:any;
  qty:any;
  supplier_name: any;
  supplier_id: any;
  member_price: any;
  merchant_price:any;
  owner_price:any;
  hashtags:any;
  price_after_discount:any;
  price:any;
  description:any;
  evoucher_code:any;
  voucher_id:any;
  status:any;
  product_code_data : any;
  merchant_group: any = [];
  keyname:any = [];
  service: any;
  product_code = [];
  product_name = [];
  merch_username = [];
  otherData:any;
  userList1 = [];
  lastkeydown1: number = 0;
  rowOfTable;
  currentValue;
  expiry_date;
  title;
  discount
  fixed_value

 selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     : any = false;
  errorLabel    : any = false;
  category      : any = false;
  
  constructor(public evoucherService: EVoucherService) {
   }
   selectedLevel;
   selected(){
     console.log(this.selectedLevel);
   }

  ngOnInit() {
    this.firstLoad();

  }

  async getKeyname(){
    this.keyname = await this.evoucherService.config();
    this.keyname.result.merchant_group.forEach((element)=> {
      this.merchant_group.push({label:element, value:element})
    });
    this.merchant_group = this.merchant_group[0].value
  }

  getCategory(){
    this.category.result.forEach((element) => {
      this.categoryProduct.push({label:element.name, value:element.name});
    });
  }

  async firstLoad(){
    let getProductsData = await this.evoucherService.getDropdown();
    this.rowOfTable = getProductsData.result.values
    let productStringListOfName = getProductsData.result;
    let products = getProductsData.result;
    this.currentValue = productStringListOfName[0].product_name;
    productStringListOfName.forEach((obj, index) => {
      this.product_code.push(obj.product_code)
      // this.product_name.push(obj.product_name)
      // this.merch_username.push(obj.merchant_username)
    })

    let dataDropdown : standardDropDown[] = [];

    products.forEach((data, index)=>{
      dataDropdown.push(
        {
          label: data['product_code'] + " - " +  data['product_name'] + " - "+ data['merchant_username'],
          value : data['product_code'],
          selected : 0}
          );
    })

    this.product_code_data = dataDropdown;
    this.product_code_data[0].selected=1;

    
    this.category = await this.evoucherService.getCategoryLint();
    this.getCategory();
    let params = {
      'usage':'generate'
    }
    let result: any;
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;
    
  }

  async formSubmitAddEvoucher(form){
    //console.log(JSON.stringify(form));
    try 
    {
      // form.expiry_date = '2019-01-01';
      form.qty = Number(form.qty);
      this.service    = this.evoucherService;
      let result: any  = await this.evoucherService.generateEVoucherLint(form);
      // console.log(form.expiry_date);
      this.Evouchers = result.result;
      alert("Generate Success");
      window.location.href = "/evoucheradmin";
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
      // alert((<Error>e).message);

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
  }

  getUserIdsFirstWay($event) {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.userList1 = [];
    this.otherData = this.product_code + ' - ' + this.product_name + ' - ' + this.merch_username;
    // console.log("here", this.otherData);

    if (userId.length > 0 || userId.length == 0) {
      if ($event.timeStamp - this.lastkeydown1 > 200) {
        this.userList1 = this.searchFromArray(this.otherData, userId);
  
      }
    }
  }

  searchFromArray(arr, regex) {
    let matches = [], i;
    for (i = 0; i < arr.length; i++) {
      if (arr[i].match(regex)) {
        matches.push(arr[i]);
      }
    }
    return matches;
  };

  onFileSelected(event){
    try  
    {
      this.errorFile = false;
      let fileMaxSize = 3000000;// let say 3Mb
      Array.from(event.target.files).forEach((file: any) => {
          if(file.size > fileMaxSize){
            this.errorFile="Maximum File Upload is 3 MB";
          }
      });
      this.selectedFile = event.target.files[0];
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
  } 

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }

  async onUpload(){
      try 
      {
        this.cancel = false;
        if(this.selectedFile){
          await this.evoucherService.upload(this.selectedFile,this);
          }
      } 
      catch (e) 
      {
        this.errorLabel = ((<Error>e).message);//conversion to Error type

        let message = this.errorLabel;
        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
        }
        Swal.fire({
          icon: 'error',
          title: message,
        });
      }
  }

}

interface standardDropDown{
  label
  value
  selected
}