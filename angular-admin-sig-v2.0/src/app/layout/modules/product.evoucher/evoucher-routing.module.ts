
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EvoucherComponent } from './evoucher.component';
import { EvoucherGenerateComponent } from './generate/evoucher.generate.component';
import { EvoucherDetailComponent } from '../../merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component';

const routes: Routes = [
  {
      path: '', component: EvoucherComponent
  },
  {
    path:'generate', component: EvoucherGenerateComponent
  },
  {
    // path:'detail', component: EvoucherDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvoucherRoutingModule { }
