import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvoucherRoutingModule } from './evoucher-routing.module';
import { EvoucherComponent } from './evoucher.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { EvoucherGenerateComponent } from './generate/evoucher.generate.component';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormInputComponent } from '../../../component-libs/form-input/form-input.component';
import { EvoucherDetailComponent } from '../../merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component';
// import { NgxEditorModule } from 'ngx-editor';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { ProductsModule } from '../../merchant-portal/separated-modules/merchant-products/products.module';


@NgModule({
  imports: [
    CommonModule, 
    EvoucherRoutingModule, 
    PageHeaderModule, 
    FormsModule, 
    FormBuilderTableModule, 
    ReactiveFormsModule,
    AutocompleteLibModule,
    // NgxEditorModule,
    ProductsModule,
    NgbModule,
  ],
  declarations: [
    EvoucherComponent,
    EvoucherGenerateComponent,
    EvoucherDetailComponent
    ]
})
export class EvoucherModule { }
