import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceInvoiceReportComponent } from './finance-invoice-report.component';

describe('FinanceInvoiceReportComponent', () => {
  let component: FinanceInvoiceReportComponent;
  let fixture: ComponentFixture<FinanceInvoiceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceInvoiceReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceInvoiceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
