import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { del } from 'selenium-webdriver/http';
import { getLink } from '../../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import * as content from '../../../../../assets/json/content.json';

@Component({
  selector: 'app-finance-invoice-report-detail',
  templateUrl: './finance-invoice-report.detail.component.html',
  styleUrls: ['./finance-invoice-report.detail.component.scss'],
  animations: [routerTransition()]
})

export class FinanceInvoiceReportDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public contentList : any = (content as any).default;

  edit: boolean = false;
  errorLabel : any = false;
  errorMessage  : any = false;
  invoiceDetail : any;
  isAddDocument: boolean = false;
  mci_project: any = false;
  programType:any = "";
  sig_kontraktual: any = false;

  constructor(public OrderhistoryService: OrderhistoryService,private router: Router,private route: ActivatedRoute) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    const program = localStorage.getItem('programName');
    let _this = this;
    this.contentList.filter(function(element) {
        if(element.appLabel == program) {
            if(element.type == "reguler") {
                _this.mci_project = true;
            } else if(element.type == "custom_kontraktual") {
              _this.programType = "custom_kontraktual";
              _this.sig_kontraktual = true;
            } else {
                _this.mci_project = false;
            }
        }
    });

    let order_id = this.detail.order_id;
    this.invoiceDetail = this.detail;
    try {
      const result : any= await this.OrderhistoryService.getFinanceInvoiceReportByID(order_id);
      if(result.length > 0){
        if(result[0].order_id == this.detail.order_id){
          this.invoiceDetail = result[0];
        }
      }
      console.warn("invoice detail lagi", this.invoiceDetail)
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  addDocument() {
    this.isAddDocument = !this.isAddDocument;
    if(this.isAddDocument == false){
      this.firstLoad();
    }
  }

  downloadImage(url : any) {
    let filename : any;
    filename = this.lastPathURL(url);
    fetch(url).then(function(t) {
        return t.blob().then((b)=>{
            var a = document.createElement("a");
            a.href = URL.createObjectURL(b);
            a.setAttribute("download", filename);
            a.click();
        }
        );
    });
    }

  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  checkFileType(text : any){
    let splitFileName = text.split('.');
    const extensFile = splitFileName[splitFileName.length - 1];
    const extens = extensFile.toLowerCase();
    return extens == 'pdf' ? 'document':'image';
  }

  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    return retDate[0];
  }

  lastPathURL(url:string){
    let resURL : any = url.split("/");
    console.log("resURL", resURL);
    let lastNumber : number = resURL.length - 1;
    return resURL[lastNumber];
  }

  editThis() {
    // console.log(this.edit );
    this.edit = !this.edit;
    if(this.edit == false){
      console.warn("firstLoad lagi")
      this.firstLoad();
    }
    // console.log(this.edit );
  }

  backToTable() {
    console.warn("back invoice detail",this.back);
    this.back[1](this.back[0]);
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }
  
}
