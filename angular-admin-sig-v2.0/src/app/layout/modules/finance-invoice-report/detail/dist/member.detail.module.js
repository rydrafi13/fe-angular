"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MemberDetailModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var form_builder_table_module_1 = require("../../../../component-libs/form-builder-table/form-builder-table.module");
var member_detail_routing_module_1 = require("./member.detail-routing.module");
// import { MemberPointHistoryComponent } from './member-point-history/member-point-history.component'
var MemberDetailModule = /** @class */ (function () {
    function MemberDetailModule() {
    }
    MemberDetailModule = __decorate([
        core_1.NgModule({
            declarations: [
            // MemberPointHistoryComponent
            ],
            imports: [
                form_builder_table_module_1.FormBuilderTableModule,
                common_1.CommonModule,
                member_detail_routing_module_1.MemberDetailRoutingModule
            ]
        })
    ], MemberDetailModule);
    return MemberDetailModule;
}());
exports.MemberDetailModule = MemberDetailModule;
