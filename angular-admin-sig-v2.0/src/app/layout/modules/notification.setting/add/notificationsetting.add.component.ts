import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { NotificationsettingService } from '../../../../services/notificationsetting/notificationsetting.service';

@Component({
  selector: 'app-notification-setting-add',
  templateUrl: './notificationsetting.add.component.html',
  styleUrls: ['./notificationsetting.add.component.scss'],
  animations: [routerTransition()]
})

export class NotificationsettingAddComponent implements OnInit {
  public name: string = "";
  Notificationsetting: any = [];
  service: any;
  public notificationStatus = [
    {label:"ACTIVE", value:"ACTIVE"}
   ,{label:"INACTIVE", value:"INACTIVE"}
 ];
 
 selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     : any = false;
  errorLabel    : any = false;
  org_id:any;
  id:any;
  description:any;
  api_access_key:any;
  web_url:any;
  
  constructor(public notificationsettingService:NotificationsettingService) {
    let form_add     : any = [
      { label:"notification_id",  type: "text",  value: "", data_binding: 'notification_id'  },
      { label:"name",  type: "text",  value: "", data_binding: 'name'  },
      { label:"color",  type: "text",  value: "", data_binding: 'color'  },
      { label:"user_id",  type: "text",  value: "", data_binding: 'user_id' },
    ];
   }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;
    
  }

  async formSubmitAddNotificationSetting(form){
    //console.log(JSON.stringify(form));
    
    try 
    {
      this.service    = this.notificationsettingService;
      let result: any  = await this.notificationsettingService.addNewNotificationsetting(form);
      console.log(result);
      this.Notificationsetting = result.result;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  /* onFileSelected(event){
    try 
    {
      this.errorFile = false;
      let fileMaxSize = 3000000;// let say 3Mb
      Array.from(event.target.files).forEach((file: any) => {
          if(file.size > fileMaxSize){
            this.errorFile="Maximum File Upload is 3MB";
          }
      });
      this.selectedFile = event.target.files[0];
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }  */

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }

  /* async onUpload(){
      try 
      {
        this.cancel = false;
        if(this.selectedFile){
          await this.notificationService.upload(this.selectedFile,this);
          }
      } 
      catch (e) 
      {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
  } */
}
