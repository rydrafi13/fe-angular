import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { NotificationsettingService } from '../../../services/notificationsetting/notificationsetting.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-notification-setting',
  templateUrl: './notificationsetting.component.html',
  styleUrls: ['./notificationsetting.component.scss'],
  animations: [routerTransition()]
})

export class NotificationsettingComponent implements OnInit {

  Notificationsetting       : any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Notification Setting Detail',
                                  label_headers   : [
                                    {label: 'Organization ID', visible: true, type: 'string', data_row_name: 'org_id'},
                                    {label: 'Name', visible: true, type: 'string', data_row_name: 'name'},
                                      // {label: 'Description', visible: false, type: 'string', data_row_name: 'description'},
                                      {label: 'Status',
                                          options: [
                                            'active',
                                            'inactive',
                                          ],
                                          visible: true, type: 'list', data_row_name: 'status'},
                                      ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Notificationsetting',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  notificationSettingDetail: any = false;
  service     : any ;


  constructor(public notificationsettingService:NotificationsettingService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.service    = this.notificationsettingService;
      const result: any  = await this.notificationsettingService.getNotificationsettingLint();
      this.Notificationsetting = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {
    
    try {
      this.service    = this.notificationsettingService;
      let result: any = await this.notificationsettingService.detailNotificationsetting(_id);
      this.notificationSettingDetail = result.result[0];
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.notificationSettingDetail = false;
  }

}
