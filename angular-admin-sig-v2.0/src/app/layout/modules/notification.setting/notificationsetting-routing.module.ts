import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationsettingComponent } from './notificationsetting.component';
import { NotificationsettingAddComponent } from './add/notificationsetting.add.component';
import { NotificationsettingDetailComponent } from './detail/notificationsetting.detail.component';

const routes: Routes = [
  {
      path: '', component: NotificationsettingComponent,

  },
   {
      path:'add', component: NotificationsettingAddComponent
  },
  {
    path:'detail', component: NotificationsettingDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationsettingRoutingModule { }
