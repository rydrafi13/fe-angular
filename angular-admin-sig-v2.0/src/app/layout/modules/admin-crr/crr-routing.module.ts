import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrrComponent } from './crr.component';

const routes: Routes = [
  {
      path: '', component: CrrComponent,

  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrrRoutingModule { }
