import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PointMovementProductComponent } from './point-movement-product.component'
// import { BastComponent} from './bast/bast.component'


const routes: Routes = [
  {
    path: '', component: PointMovementProductComponent
},
// {
//   path:'bast', component: BastComponent
// }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PointMovementProductRoutingModule { }
