import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PointsTransactionService } from '../../../services/points-transaction/points-transaction.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-point-movement-product',
  templateUrl: './point-movement-product.component.html',
  styleUrls: ['./point-movement-product.component.scss']
})
export class PointMovementProductComponent implements OnInit {

  PointMovementProduct: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Order Report (On Delivery and Delivered Shipping)',
                                  label_headers   : [
                                    {label: 'Order Date', visible: true, type: 'date', data_row_name: 'request_date'},
                                    {label: 'Order Status', visible: true, type: 'string', data_row_name: 'status'},
                                    {label: 'On Process Date', visible: true, type: 'date', data_row_name: 'approve_date'},
                                    // {label: 'On Process Date', visible: true, type: 'on_process_date', data_row_name: 'shipping_info'},
                                    {label: 'Shipping Status', visible: true, type: 'string', data_row_name: 'last_shipping_info'},
                                    {label: 'On Delivery Date', visible: true, type: 'on_delivery_date', data_row_name: 'shipping_info'},
                                    // {label: 'Input AWB Date', visible: true, type: 'input_awb_date', data_row_name: 'shipping_info'},
                                    {label: 'ID Toko', visible: true, type: 'string', data_row_name: 'id_toko'},
                                    {label: 'Nama Toko', visible: true, type: 'string', data_row_name: 'nama_toko'},
                                    {label: 'Alamat Toko', visible: true, type: 'string', data_row_name: 'alamat_toko'},
                                    {label: 'Nama Pemilik', visible: true, type: 'string', data_row_name: 'nama_pemilik'},
                                    {label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products'},
                                    {label: 'Product Redeem', visible: true, type: 'product_redeem', data_row_name: 'products'},
                                    {label: 'SKU', visible: true, type: 'product_code_redeem', data_row_name: 'products'},
                                    {label: 'Product Price', visible: true, type: 'product_price_redeem', data_row_name: 'products'},
                                    // {label: 'Previous Point Balance', visible: true, type: 'string', data_row_name: 'prev_points_balance'},
                                    // {label: 'Mutasi Point', visible: true, type: 'string', data_row_name: 'points'},
                                    // {label: 'Current Points Balance', visible: true, type: 'string', data_row_name: 'current_points_balance'},
                                    // {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                                    {label: 'Group', visible: true, type: 'string', data_row_name: 'group'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'PointMovementProduct',
                                                    detail_function: [this, 'callDetail'],
                                                    pointMovementProduct : true,
                                                  },
                                                  show_checkbox_options: true
  };

  tableFormat2        : TableFormat = {
    title           : 'Order Report (On Delivery and Delivered Shipping)',
    label_headers   : [
      {label: 'Order Date', visible: true, type: 'date', data_row_name: 'request_date'},
      {label: 'Order Status', visible: true, type: 'string', data_row_name: 'status'},
      {label: 'On Process Date', visible: true, type: 'date', data_row_name: 'approve_date'},
      // {label: 'On Process Date', visible: true, type: 'on_process_date', data_row_name: 'shipping_info'},
      {label: 'Shipping Status', visible: true, type: 'string', data_row_name: 'last_shipping_info'},
      {label: 'On Delivery Date', visible: true, type: 'on_delivery_date', data_row_name: 'shipping_info'},
      // {label: 'Input AWB Date', visible: true, type: 'input_awb_date', data_row_name: 'shipping_info'},
      {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
      {label: 'Nama Pelanggan', visible: true, type: 'string', data_row_name: 'nama_pemilik'},
      {label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products'},
      {label: 'Product Redeem', visible: true, type: 'product_redeem', data_row_name: 'products'},
      {label: 'SKU', visible: true, type: 'product_code_redeem', data_row_name: 'products'},
      {label: 'Product Price', visible: true, type: 'product_price_redeem', data_row_name: 'products'},
      // {label: 'Previous Point Balance', visible: true, type: 'string', data_row_name: 'prev_points_balance'},
      // {label: 'Mutasi Point', visible: true, type: 'string', data_row_name: 'points'},
      // {label: 'Current Points Balance', visible: true, type: 'string', data_row_name: 'current_points_balance'},
      // {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
      {label: 'Group', visible: true, type: 'string', data_row_name: 'group'},
    ],
    row_primary_key : '_id',
    formOptions     : {
                      row_id: '_id',
                      this  : this,
                      result_var_name: 'PointMovementProduct',
                      detail_function: [this, 'callDetail'],
                      pointMovementProduct : true,
                    },
                    show_checkbox_options: true
  };

  tableFormat3      : TableFormat = {
    title           : 'Order Report (On Delivery and Delivered Shipping)',
    label_headers   : [
      {label: 'Order Date', visible: true, type: 'date', data_row_name: 'request_date'},
      {label: 'Order Status', visible: true, type: 'string', data_row_name: 'status'},
      {label: 'On Process Date', visible: true, type: 'date', data_row_name: 'approve_date'},
      // {label: 'On Process Date', visible: true, type: 'on_process_date', data_row_name: 'shipping_info'},
      {label: 'Shipping Status', visible: true, type: 'string', data_row_name: 'last_shipping_info'},
      {label: 'On Delivery Date', visible: true, type: 'on_delivery_date', data_row_name: 'shipping_info'},
      // {label: 'Input AWB Date', visible: true, type: 'input_awb_date', data_row_name: 'shipping_info'},
      {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
      {label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail'},
      {label: 'Nama Entitas', visible: true, type: 'string', data_row_name: 'nama_toko'},
      {label: 'Alamat', visible: true, type: 'string', data_row_name: 'alamat_toko'},
      {label: 'Nama PIC', visible: true, type: 'string', data_row_name: 'nama_pemilik'},
      {label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products'},
      {label: 'Product Redeem', visible: true, type: 'product_redeem', data_row_name: 'products'},
      {label: 'SKU', visible: true, type: 'product_code_redeem', data_row_name: 'products'},
      {label: 'Product Price', visible: true, type: 'product_price_redeem', data_row_name: 'products'},
      // {label: 'Previous Point Balance', visible: true, type: 'string', data_row_name: 'prev_points_balance'},
      // {label: 'Mutasi Point', visible: true, type: 'string', data_row_name: 'points'},
      // {label: 'Current Points Balance', visible: true, type: 'string', data_row_name: 'current_points_balance'},
      // {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
    ],
    row_primary_key : '_id',
    formOptions     : {
                      row_id: '_id',
                      this  : this,
                      result_var_name: 'PointMovementProduct',
                      detail_function: [this, 'callDetail'],
                      pointMovementProduct : true,
                    },
                    show_checkbox_options: true
  };


  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;

  SalesRedemptionDetail: any = false;
  service: any;
  mci_project: any = false;
  programType:any = "";

  errorMessage: any = false;

  public contentList : any = (content as any).default;
  
  constructor(public PointsTransactionService:PointsTransactionService) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    try{
      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
          if(element.appLabel == program) {
              if(element.type == "reguler") {
                  _this.mci_project = true;
              } else if(element.type == "custom_kontraktual") {
                _this.programType = "custom_kontraktual";
              } else {
                  _this.mci_project = false;
              }
          }
      });
      
      var params = {
         search: {based_on:"order_id"},
         current_page:1,
         limit_per_page:50
        }
      this.service    = this.PointsTransactionService;
      let result: any  = await this.PointsTransactionService.getPointMovementBasedOnProduct();
      // console.warn("RESULT", result)
      this.totalPage = result.total_page
      this.PointMovementProduct = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			}
    }
  }
  
  public async callDetail(SalesRedemption_id){
    try{
      // let result: any;
      // this.service    = this.PointsTransactionService;
      // result          = await this.PointsTransactionService.detailPointstransaction(SalesRedemption_id);
      // console.log(result);

      // this.SalesRedemptionDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.SalesRedemptionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}
