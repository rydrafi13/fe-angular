import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointMovementProductComponent } from './point-movement-product.component';

describe('PointMovementProductComponent', () => {
  let component: PointMovementProductComponent;
  let fixture: ComponentFixture<PointMovementProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointMovementProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointMovementProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
