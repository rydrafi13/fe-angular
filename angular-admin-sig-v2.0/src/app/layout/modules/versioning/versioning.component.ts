import { versioningService } from '../../../services/versioning/versioning.service';
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-versioning',
  templateUrl: './versioning.component.html',
  styleUrls: ['./versioning.component.scss'],
  animations: [routerTransition()]
})
export class VersioningComponent implements OnInit {

  versioning: any = [];
  tableFormat: TableFormat = {
                                  title           : 'Versioning Page',
                                  label_headers   : [
                                      {label: 'Versioning ID', visible: true, type: 'string', data_row_name: '_id'},
                                      {label: 'App Name', visible: true, type: 'string', data_row_name: 'app_name'},
                                      {label: 'Device', visible: true, type: 'string', data_row_name: 'device'},
                                    {label: 'Version', visible: true, type: 'string', data_row_name: 'version'},
                                    // {label: 'Organization ID', visible: false, type: 'string', data_row_name: 'org_id'},
                                    // {label: 'URL', visible: false, type: 'string', data_row_name: 'url'},
                                    // {label: 'Active', visible: false, type: 'string', data_row_name: 'active'},
                                    // {label: 'Build', visible: false, type: 'string', data_row_name: 'build'},
                                  ],
                                  row_primary_key : 'app_name',
                                  formOptions     : {
                                  // addFormGenerate   : true,
                                                    row_id: 'app_name',
                                                    this  : this,
                                                    result_var_name: 'Versioning',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input: any = {};
  errorLabel: any = false;

  versioningDetail: any = false;
  service: any;

  constructor(public versioningService: versioningService) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    try {
      this.service = this.versioningService;
      const result: any = await this.versioningService.getVersion();
      console.log(result);
      this.versioning = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }


  public async backToHere(obj) {
    obj.versioningDetail = false;
  }

  public async callDetail(version_appname)
  {
    try{
      let result: any;
      result          = await this.versioningService.detailVersioning(version_appname);
      this.versioningDetail = result.result[0];
    } catch (e) {
      this.errorLabel = ((<Error>e).message);
      // conversion to Error type
    }
  }

}
