import { versioningService } from '../../../../services/versioning/versioning.service';
import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

//
@Component({
  selector: 'app-versioning-edit',
  templateUrl: './versioning.edit.component.html',
  styleUrls: ['./versioning.edit.component.scss'],
  animations: [routerTransition()]
})

export class VersioningEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading        :boolean = false;
  public categoryProduct  : any = [];
  public done = false;
  errorLabel    : any = false;
  category      : any = false;
  product_status: any;



  constructor(public versioningService: versioningService) {}

  ngOnInit() {
    this.firstLoad();
    this.getCategory();
  }

  async getCategory(){
    this.category = await this.versioningService.getCategoryLint();
    this.category.result.forEach((element) => {
      this.categoryProduct.push({label:element.name, value:element.name});
    });
  }

  async firstLoad(){
    try 
    {
      this.getCategory();
      this.detail.previous_product_code = await this.detail.product_code;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
   
  }

  backToDetail(){
   
    this.back[0][this.back[1]]();
  }
  
  async saveThis(){
   
    try 
    {
      this.loading = !this.loading;
      await this.versioningService.updateVersioning(this.detail);
      this.loading = !this.loading;
      this.done = true;
      alert("Data Has Been Updated Succesfully")
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
}
