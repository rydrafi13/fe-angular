import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagebaruComponent } from './pagebaru.component';

const routes: Routes = [
    {
        path: '', component: PagebaruComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagebaruRoutingModule {
}
