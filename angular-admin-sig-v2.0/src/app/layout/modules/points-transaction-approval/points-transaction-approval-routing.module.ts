import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PointsTransationApprovalComponent } from './points-transaction-approval.component';
// import { BastComponent} from './bast/bast.component'


const routes: Routes = [
  {
    path: '', component: PointsTransationApprovalComponent
},
// {
//   path:'bast', component: BastComponent
// }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PointsTransationApprovalRoutingModule { }
