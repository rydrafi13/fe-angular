import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { DomSanitizer } from '@angular/platform-browser';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { PointsTransactionService } from '../../../services/points-transaction/points-transaction.service';

// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;


@Component({
  selector: 'app-form-member',
  templateUrl: './points-transaction-approval.component.html',
  styleUrls: ['./points-transaction-approval.component.scss'],
  animations: [routerTransition()]
})

export class PointsTransationApprovalComponent implements OnInit {

  points       : any = [];
  row_id        : any = "process_number";
  swaper = true;

  tableFormat   : TableFormat = {
    title           : 'Point Transaction Report',
    label_headers   : [
                  {label: 'Created Date',     visible: true, type: 'date', data_row_name: 'created_date'},
                  // {label: 'Updated Date',     visible: true, type: 'date', data_row_name: 'updated_date'},
                  {label: 'Process Number', visible: true, type: 'string', data_row_name: 'process_number'},
                  {label: 'Process ID', visible: true, type: 'string', data_row_name: 'process_id'},
                  {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username'},
                  {label: 'Nama Pelanggan', visible: true, type: 'string', data_row_name: 'full_name'},
                  {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                  {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                  {label: 'Remarks', visible: true, type: 'string', data_row_name: 'remarks'},
                  {label: 'Process Type', visible: true, type: 'string', data_row_name: 'process_type'},
                  {label: 'Status', visible: true, type: 'string', data_row_name: 'status'},
                  {label: 'Transaction Month',     visible: true, type: 'string', data_row_name: 'transaction_month'},

                ],
    row_primary_key : '_id',
    formOptions     : {
                    // addForm   : true,
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'points',
                    detail_function : [this, 'callDetail'] 
                    }
                  };

  form_input    : any = {};
  errorLabel    : any = false;
  errorMessage  : any = false;
  totalPage     : 0;
 
  pointDetail  : any = false;
  service       : any ;
  srcDownload   : any ;
  

  // private options = new RequestOptions(
  //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(public sanitizer:DomSanitizer, public PointsTransactionService:PointsTransactionService) {

    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      // let params = {
      //   'type':'member'
      // }
      this.service    = this.PointsTransactionService;

      let result: any;

      if(this.swaper == true){
        result = await this.PointsTransactionService.getPointstransactionApprovalProcessLint('REQUESTED');
      }else{
        result= await this.PointsTransactionService.getPointstransactionApprovalProcessLint('PROCESSED');
      }

      // const result: any  = await this.PointsTransactionService.getPointstransactionReportProcessLint();
      console.warn("form member", result);
      this.totalPage = result.total_page;
      this.points = result.values;
      // console.warn("Members",result.result.values);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }

  }

  public async callDetail(data, rowData) {
    
    // try {
    
    //   this.pointDetail = this.points.find(point => point.process_number == rowData.process_number);
    //   console.log("pointDetail", this.pointDetail);
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  public async backToHere(obj) {
    obj.pointDetail = false;
    obj.firstLoad();
  }

  swapClick(bool) {
    this.swaper = bool;
    this.firstLoad();
  }

  // onDownload(downloadLint)
  // {
  //   let srcDownload = downloadLint;
  //   //let srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
  //   this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
  //   //console.log(this.srcDownload);

  //   this.getDownloadFileLint();
  // }

  // public async getDownloadFileLint() {
  //   try
  //   {

  //     let result: any;
  //     this.service    = this.memberService;
  //     result= await this.memberService.getDownloadFileLint();
  //     //console.log(result.result);
  //     let downloadLint = result.result;
  //     //console.log(downloadLint);

  //     //To running other subfunction with together automatically
  //     this.onDownload(downloadLint);
  //   } 
  //   catch (e) 
  //   {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }
  // } 

  // public async pagination() {
  //   try
  //   {
  //     let result: any;
  //     this.service = this.memberService;
  //     result = await this.memberService.getDownloadFileLint();
  //     //console.log(result.result);
  //     let downloadLint = result.result;
  //     //console.log(downloadLint);

  //     //To running other subfunction with together automatically
  //     this.onDownload(downloadLint);
  //   }
  //   catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }  
  // }

}
