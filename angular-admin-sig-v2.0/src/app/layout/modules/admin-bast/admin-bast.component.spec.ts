import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBastComponent } from './admin-bast.component';

describe('AdminBastComponent', () => {
  let component: AdminBastComponent;
  let fixture: ComponentFixture<AdminBastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
