import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminBastComponent } from './admin-bast.component';
import { BastComponent} from './bast/bast.component'


const routes: Routes = [
  {
    path: '', component: AdminBastComponent
},
{
  path:'bast', component: BastComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminBastRoutingModule { }
