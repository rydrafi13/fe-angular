import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BastRoutingModule } from './bast-routing.module';
import { BastComponent } from './bast.component';
import { NgxBarcodeModule } from 'ngx-barcode';


@NgModule({
  declarations: [],
  // declarations: [ShippingLabelComponent],
  imports: [
    NgxBarcodeModule,
    CommonModule,
    BastRoutingModule
  ]
})
export class BastModule { }
