import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TableFormat, orderHistoriesTableFormat, salesOrderTableFormat } from '../../../object-interface/common.object';
import { getHost } from '../../../../environments/environment';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { MemberService } from '../../../services/member/member.service';
import Swal from 'sweetalert2';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-delivery-process',
  templateUrl: './delivery-process.component.html',
  styleUrls: ['./delivery-process.component.scss'],
  animations: [routerTransition()]
})
export class DeliveryProcessComponent implements OnInit {

  onSearchActive: boolean = false;
  showLoading: boolean = false;
  searchCallback: any;
  allData: any = [];
  allDataToDownload: any = [];
  incomingToDownload: any = [];
  deliveryToDownload: any = [];
  completeToDownload: any = [];
  prodType: any;
  form_input: any = {};
  errorLabel: any = false;
  errorMessage  : any = false;
  allMemberDetail: any = false;
  service: any;
  swaper: any[] = [
    { name: 'All Order', val: true, value: 'all order', values: "all_order" },
    { name: 'Incoming', val: false, value: 'on process', values: 'pending' },
    { name: 'On Packaging', val: false, value: 'on packaging', values: 'on_processing' },
    { name: 'On Delivery', val: false, value: 'on delivery', values: 'on_delivery' },
    { name: 'Delivered', val: false, value: 'delivered', values: 'delivered' },
  ]
  filterBy = [
    { name: 'Reference No', value: 'reference_no' },
    { name: 'Order ID', value: 'order_id' },
    { name: 'Receiver', value: 'receiver' },
    // { name: 'Product Name', value: 'product_name' },
    { name: 'AWB Number', value: 'awb_number' }
  ]
  sortBy = [
    { name: 'Newest', value: 'newest' },
    { name: 'Highest Transactions', value: 'highest transactions' },
    { name: 'Lowest Transactions', value: 'lowest transactions' }
  ]
  public contentList : any = (content as any).default;
  orderDetail: any = false;
  shippingLabel: any = false;
  srcDownload: any;
  toggler: any = {};
  totalPage: any;
  pageNumbering: any = [];
  currentPage = 1;
  pageLimits = 20;
  pagesLimiter = [];
  total_page: 0;
  orderBy: string = "newest";
  filter: string = "reference_no";
  checkBox = [];
  activeCheckbox = false;
  getDataCheck = [];
  filterResult: any = {};
  orderByResult: any = { created_date: -1 };
  totalValuePending;
  currentPermission;
  indexSwap: any = 0;
  searchTextModel: any;

  mci_project: any = false;

  propsDetail: any;

  constructor(private orderhistoryService: OrderhistoryService, private router: Router, private route: ActivatedRoute, public memberService: MemberService) {
    this.service = this.orderhistoryService;
  }

  public async callDetail(order_id) {
    // this.openDetail(order_id)
  }

  private openDetail(order_id) {
    // this.router.navigate(['merchant-portal/order-detail/edit'],  {queryParams: {id: order_id }})
  }

  //Shipping Status Button Swapping Function
  async swapClick(index) {
    this.indexSwap = index;
    this.currentPage = 1;
    this.pageLimits = 20;

    if (this.form_input.transaction_status) {
      delete this.form_input.transaction_status;
    }

    this.swaper.forEach((e) => {
      e.val = false
    })

    this.swaper[index].val = true;

    let status = this.swaper[index].values;

    delete this.form_input.status;
    delete this.form_input.last_shipping_info;

    if (status == "all_order") {
      this.firstLoad();
      return;
    } else if (status == "pending") {
      let process = {
        status: "PENDING",
        // last_shipping_info:"on_processing"
      }
      Object.assign(this.form_input, process);
    } else if (status == "on_processing") {
      let process = {
        status: "ACTIVE",
        last_shipping_info:"on_processing"
      }
      Object.assign(this.form_input, process);
    } else if (status == "on_delivery") {
      let process = {
        status: "ACTIVE",
        last_shipping_info:"on_delivery"
      }
      Object.assign(this.form_input, process);
    } else if (status == "delivered") {
      let process = {
        // status: "COMPLETED",
        // status: {
        //   in:["PROCESSED","COMPLETED","RETURN", "CANCEL"]
        // },
        // last_shipping_info:"delivered"
        last_shipping_info: {
          in:["return","redelivery","delivered"],
        }
      }
      Object.assign(this.form_input, process);
    }
    // else {
    //   let swaper = {
    //     shipping_status: status,
    //     payment_status: 'PROCESSED',
    //   }
    //   Object.assign(this.form_input, swaper)
    // }
    this.valuechange({}, false, false);
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {

      try {
        const program = localStorage.getItem('programName');
        let _this = this;
        this.contentList.filter(function(element) {
            if(element.appLabel == program) {
                if(element.type == "reguler") {
                    _this.mci_project = true;
                } else {
                    _this.mci_project = false;
                }
            }
        });

        let filter = {
          search: {
            status: {
              in:["ACTIVE","PENDING","DELIVERED","RETURN","CANCEL"]
            }
          },
          order_by: this.orderByResult,
          limit_per_page: this.pageLimits,
          current_page: this.currentPage,
          download: false
        }
        let filter_pending = {
          search: {
            status: "PENDING",
            // last_shipping_info:"on_processing"
          },
          order_by: this.orderByResult,
          limit_per_page: this.pageLimits,
          current_page: this.currentPage,
          download: false
        }
        // // Download All Order Report
        // let no_filter = {
        //   search: {
        //     status: {
        //       in:["PROCESSED","COMPLETED"]
        //     }
        //   },
        //   order_by: this.orderByResult,
        // }
        // // Download Incoming Order Report
        // let no_filter_incoming = {
        //   search: {
        //     status: "PROCESSED",
        //     last_shipping_info: "on_processing"
        //   },
        //   order_by: this.orderByResult,
        // }
        // // Download On Delivery Order Report
        // let no_filter_delivery = {
        //   search: {
        //     status: "PROCESSED",
        //     last_shipping_info: "on_delivery"
        //   },
        //   order_by: this.orderByResult,
        // }
        // // Download Complete Order Report
        // let no_filter_complete = {
        //   search: {
        //     status: "COMPLETED",
        //     last_shipping_info: "delivered"
        //   },
        //   order_by: this.orderByResult,
        // }

        this.showLoading = true;
        // let result = await this.orderhistoryService.searchOrderHistory(filter);
        let resultPending = await this.orderhistoryService.searchShippingReport(filter_pending);
        
        this.totalValuePending = resultPending.total_all_values;
        
        // // Download All Order Report
        // let resultToDownload = await this.orderhistoryService.searchOrderHistory(no_filter);
        // console.warn("result download", resultToDownload);

        // // Download Incoming Order Report
        // let downloadIncoming = await this.orderhistoryService.searchOrderHistory(no_filter_incoming);
        // console.warn("result download incoming", downloadIncoming);

        // // Download On Delivery Order Report
        // let downloadOnDelivery = await this.orderhistoryService.searchOrderHistory(no_filter_delivery);
        // console.warn("result download on delivery", downloadOnDelivery);

        // // Download Complete Order Report
        // let downloadComplete = await this.orderhistoryService.searchOrderHistory(no_filter_complete);
        // console.warn("result download complete", downloadComplete);

        // this.showLoading = false;
        // this.allData = result.values;
        // this.allDataToDownload = resultToDownload.values;
        // this.incomingToDownload = downloadIncoming.values;
        // this.deliveryToDownload = downloadOnDelivery.values;
        // this.completeToDownload = downloadComplete.values;

        // this.totalPage = result.total_page;


        // this.buildPagesNumbers(this.totalPage)

        // let productStatus = {
        //   product_status: false
        // }

        // this.allData.forEach((el, i) => {
        //   Object.assign(this.allData[i], productStatus)
        // });

        let status = this.swaper[this.indexSwap].values;

        if (status == "all_order") {
          // let result = await this.orderhistoryService.searchOrderHistory(filter);
          // let resultPending = await this.orderhistoryService.searchShippingReport(filter_pending);
        
          // this.totalValuePending = resultPending.total_all_values;

          // this.allData = result.values;
          // this.totalPage = result.total_page;
          // this.buildPagesNumbers(this.totalPage)

          delete this.form_input.last_shipping_info

          Object.assign(this.form_input, filter.search);
          this.valuechange({}, false, false);
        } else if (status == "pending") {
          let process = {
            status: "PENDING"
            // last_shipping_info:"on_processing"
          }
          Object.assign(this.form_input, process);
          this.valuechange({}, false, false);
        } else if (status == "on_processing") {
          let process = {
            // status: "PROCESSED",
            last_shipping_info:"on_processing"
          }
          Object.assign(this.form_input, process);
          this.valuechange({}, false, false);
        } else if (status == "on_delivery") {
          let process = {
            // status: "PROCESSED",
            last_shipping_info:"on_delivery"
          }
          Object.assign(this.form_input, process);
          this.valuechange({}, false, false);
        } else if (status == "delivered") {
          let process = {
            // status: "COMPLETED",
            // status: {
            //   in:["PROCESSED","COMPLETED","RETURN", "CANCEL"]
            // },
            // last_shipping_info:"delivered"
            last_shipping_info: {
              in:["return","redelivery","delivered"],
            }
          }
          Object.assign(this.form_input, process);
          this.valuechange({}, false, false);
        }

      //   this.orderDetail = {
      //     "_id": "61bab16a8fee047f4b74ab8a",
      //     "origin": {
      //         "name": "CLS System",
      //         "cell_phone": "02122686277",
      //         "address": "Jl. Kebon Jeruk VII No.2E, RT.4/RW.5, Maphar, Kec. Taman Sari, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11160"
      //     },
      //     "destination": {
      //         "_id": "6171303c68af5c219c7da552",
      //         "name": "Gunawarman",
      //         "cell_phone": "0182923819182",
      //         "address": "Jalan Kebon Jeruk No. VII No 2e",
      //         "province_name": "DKI Jakarta",
      //         "city_name": "Jakarta Barat",
      //         "district_name": "Taman Sari",
      //         "village_name": "Maphar",
      //         "postal_code": "11160",
      //         "use_as": "main_address"
      //     },
      //     "product_list": [
      //         {
      //             "quantity": 1,
      //             "product_code": "careforu-fisik",
      //             "sku_code": "careforu-fisik-001",
      //             "product_name": "Voucher Fisik Carefour",
      //             "description": "<p>Voucher Fisik Carefour</p>",
      //             "variation": [],
      //             "type": "voucher",
      //             "category": "12345",
      //             "status": "ACTIVE",
      //             "weight": 1,
      //             "dimensions": {
      //                 "length": 1,
      //                 "width": 1,
      //                 "height": 1
      //             },
      //             "volume_weight": 0.16666666666666666,
      //             "images_gallery": [
      //                 {
      //                     "base_url": "https://res.cloudinary.com/pt-cls-system/image/upload/",
      //                     "pic_file_name": "v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                     "pic_small_path": "q_auto:good,w_150/v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                     "pic_medium_path": "q_auto:good,w_600/v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                     "pic_big_path": "v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg"
      //                 }
      //             ],
      //             "created_date": "2021-08-25 11:40:50",
      //             "updated_date": "2021-12-16 10:19:40",
      //             "sku_price": 100000,
      //             "sku_value": 100000,
      //             "variant": null,
      //             "qty": 455,
      //             "image_gallery": null,
      //             "product_info": [
      //                 {
      //                     "_id": "6125c9d2b114fe47240d3bf9",
      //                     "product_name": "Voucher Fisik Carefour",
      //                     "product_code": "careforu-fisik",
      //                     "description": "<p>Voucher Fisik Carefour</p>",
      //                     "variation": [],
      //                     "type": "voucher",
      //                     "category": "12345",
      //                     "status": "ACTIVE",
      //                     "weight": 1,
      //                     "dimensions": {
      //                         "length": 1,
      //                         "width": 1,
      //                         "height": 1
      //                     },
      //                     "volume_weight": 0.16666666666666666,
      //                     "images_gallery": [
      //                         {
      //                             "base_url": "https://res.cloudinary.com/pt-cls-system/image/upload/",
      //                             "pic_file_name": "v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                             "pic_small_path": "q_auto:good,w_150/v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                             "pic_medium_path": "q_auto:good,w_600/v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                             "pic_big_path": "v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg"
      //                         }
      //                     ],
      //                     "created_date": "2021-08-25 11:40:50",
      //                     "updated_date": "2021-08-25 11:40:50",
      //                     "created_by": "60b0aadc75a3122cf46a66b8",
      //                     "updated_by": "60b0aadc75a3122cf46a66b8",
      //                     "created_at": "178.128.19.191",
      //                     "updated_at": "178.128.19.191"
      //                 }
      //             ],
      //             "product_sku": [
      //                 {
      //                     "_id": "6125c9d2b114fe47240d3bfa",
      //                     "sku_code": "careforu-fisik-001",
      //                     "sku_price": 100000,
      //                     "sku_value": 100000,
      //                     "product_id": "6125c9d2b114fe47240d3bf9",
      //                     "product_code": "careforu-fisik",
      //                     "variant": null,
      //                     "qty": 455,
      //                     "image_gallery": null,
      //                     "status": "ACTIVE",
      //                     "created_date": "2021-08-25 11:40:50",
      //                     "updated_date": "2021-12-16 10:19:40",
      //                     "created_by": "60b0aadc75a3122cf46a66b8",
      //                     "updated_by": "60b0aadc75a3122cf46a66b8",
      //                     "released_date": "2021-12-16 10:19:40",
      //                     "updated_at": "178.128.19.191",
      //                     "evoucher": []
      //                 }
      //             ],
      //             "product_id": "6125c9d2b114fe47240d3bfa",
      //             "total_product_price": 100000
      //         }
      //     ],
      //     "total_item_value": 100000,
      //     "total_item_qty": 1,
      //     "order_id": "MCI-7042135947350351200-DEV",
      //     "reference_no": "MCI-7042135947350351200-DEV-3",
      //     "dimensions": {
      //         "length": 100,
      //         "width": 100,
      //         "height": 100
      //     },
      //     "weight": 0.1,
      //     "volumetric_weight": 166.66666666666666,
      //     "shipping_info": [
      //         {
      //             "label": "on_processing",
      //             "title": "On Packaging and Procesing",
      //             "created_date": "2021-12-16 15:28:24",
      //             "updated_date": "2021-12-16 15:28:24"
      //         },
      //         {
      //             "label": "on_delivery",
      //             "title": "On Delivery Process",
      //             "remarks": "sdsdsds",
      //             "created_date": "2021-12-16 15:45:41",
      //             "updated_date": "2021-12-16 15:45:41"
      //         }
      //     ],
      //     "status": "ACTIVE",
      //     "courier": "REALS-SAP",
      //     "courier_name": "SAP",
      //     "supported": 1,
      //     "available_courier": [
      //         {
      //             "courier": "SAP",
      //             "courier_code": "REALS-SAP",
      //             "delivery_service": {
      //                 "REG": 1169000,
      //                 "ODS": 2004000
      //             },
      //             "delivery_method": [
      //                 "PICKUP"
      //             ]
      //         }
      //     ],
      //     "delivery_type": "voucher",
      //     "delivery_method": "PICKUP",
      //     "delivery_service": "ODS",
      //     "awb_number": "MCI-7042135947350351200-DEV-3",
      //     "delivery_cost": 2004000,
      //     "track_history": []
      // };

      //   this.orderDetail = [
      //     {
      //         "_id": "61bab16a8fee047f4b74ab8a",
      //         "origin": {
      //             "name": "CLS System",
      //             "cell_phone": "02122686277",
      //             "address": "Jl. Kebon Jeruk VII No.2E, RT.4/RW.5, Maphar, Kec. Taman Sari, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11160"
      //         },
      //         "destination": {
      //             "_id": "6171303c68af5c219c7da552",
      //             "name": "Gunawarman",
      //             "cell_phone": "0182923819182",
      //             "address": "Jalan Kebon Jeruk No. VII No 2e",
      //             "province_name": "DKI Jakarta",
      //             "city_name": "Jakarta Barat",
      //             "district_name": "Taman Sari",
      //             "village_name": "Maphar",
      //             "postal_code": "11160",
      //             "use_as": "main_address"
      //         },
      //         "product_list": [
      //             {
      //                 "quantity": 1,
      //                 "product_code": "careforu-fisik",
      //                 "sku_code": "careforu-fisik-001",
      //                 "product_name": "Voucher Fisik Carefour",
      //                 "description": "<p>Voucher Fisik Carefour</p>",
      //                 "variation": [],
      //                 "type": "voucher",
      //                 "category": "12345",
      //                 "status": "ACTIVE",
      //                 "weight": 1,
      //                 "dimensions": {
      //                     "length": 1,
      //                     "width": 1,
      //                     "height": 1
      //                 },
      //                 "volume_weight": 0.16666666666666666,
      //                 "images_gallery": [
      //                     {
      //                         "base_url": "https://res.cloudinary.com/pt-cls-system/image/upload/",
      //                         "pic_file_name": "v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                         "pic_small_path": "q_auto:good,w_150/v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                         "pic_medium_path": "q_auto:good,w_600/v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                         "pic_big_path": "v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg"
      //                     }
      //                 ],
      //                 "created_date": "2021-08-25 11:40:50",
      //                 "updated_date": "2021-12-16 10:19:40",
      //                 "sku_price": 100000,
      //                 "sku_value": 100000,
      //                 "variant": null,
      //                 "qty": 455,
      //                 "image_gallery": null,
      //                 "product_info": [
      //                     {
      //                         "_id": "6125c9d2b114fe47240d3bf9",
      //                         "product_name": "Voucher Fisik Carefour",
      //                         "product_code": "careforu-fisik",
      //                         "description": "<p>Voucher Fisik Carefour</p>",
      //                         "variation": [],
      //                         "type": "voucher",
      //                         "category": "12345",
      //                         "status": "ACTIVE",
      //                         "weight": 1,
      //                         "dimensions": {
      //                             "length": 1,
      //                             "width": 1,
      //                             "height": 1
      //                         },
      //                         "volume_weight": 0.16666666666666666,
      //                         "images_gallery": [
      //                             {
      //                                 "base_url": "https://res.cloudinary.com/pt-cls-system/image/upload/",
      //                                 "pic_file_name": "v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                                 "pic_small_path": "q_auto:good,w_150/v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                                 "pic_medium_path": "q_auto:good,w_600/v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg",
      //                                 "pic_big_path": "v1629866396/admin/image/1024x1024-blue-sapphire-solid-color-background.jpg6125c99aca0f4.jpg"
      //                             }
      //                         ],
      //                         "created_date": "2021-08-25 11:40:50",
      //                         "updated_date": "2021-08-25 11:40:50",
      //                         "created_by": "60b0aadc75a3122cf46a66b8",
      //                         "updated_by": "60b0aadc75a3122cf46a66b8",
      //                         "created_at": "178.128.19.191",
      //                         "updated_at": "178.128.19.191"
      //                     }
      //                 ],
      //                 "product_sku": [
      //                     {
      //                         "_id": "6125c9d2b114fe47240d3bfa",
      //                         "sku_code": "careforu-fisik-001",
      //                         "sku_price": 100000,
      //                         "sku_value": 100000,
      //                         "product_id": "6125c9d2b114fe47240d3bf9",
      //                         "product_code": "careforu-fisik",
      //                         "variant": null,
      //                         "qty": 455,
      //                         "image_gallery": null,
      //                         "status": "ACTIVE",
      //                         "created_date": "2021-08-25 11:40:50",
      //                         "updated_date": "2021-12-16 10:19:40",
      //                         "created_by": "60b0aadc75a3122cf46a66b8",
      //                         "updated_by": "60b0aadc75a3122cf46a66b8",
      //                         "released_date": "2021-12-16 10:19:40",
      //                         "updated_at": "178.128.19.191",
      //                         "evoucher": []
      //                     }
      //                 ],
      //                 "product_id": "6125c9d2b114fe47240d3bfa",
      //                 "total_product_price": 100000
      //             }
      //         ],
      //         "total_item_value": 100000,
      //         "total_item_qty": 1,
      //         "order_id": "MCI-7042135947350351200-DEV",
      //         "reference_no": "MCI-7042135947350351200-DEV-3",
      //         "status": "PENDING",
      //         "delivery_type": "voucher",
      //         "created_date": "2021-12-16 10:24:26",
      //     },
      //     {
      //         "_id": "61bab16a8fee047f4b74ab89",
      //         "origin": {
      //             "name": "CLS System",
      //             "cell_phone": "02122686277",
      //             "address": "Jl. Kebon Jeruk VII No.2E, RT.4/RW.5, Maphar, Kec. Taman Sari, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11160"
      //         },
      //         "destination": {
      //             "_id": "6171303c68af5c219c7da552",
      //             "name": "Gunawarman",
      //             "cell_phone": "0182923819182",
      //             "address": "Jalan Kebon Jeruk No. VII No 2e",
      //             "province_name": "DKI Jakarta",
      //             "city_name": "Jakarta Barat",
      //             "district_name": "Taman Sari",
      //             "village_name": "Maphar",
      //             "postal_code": "11160",
      //             "use_as": "main_address"
      //         },
      //         "product_list": [
      //             {
      //                 "quantity": 1,
      //                 "product_code": "jam123",
      //                 "sku_code": "jam123-001",
      //                 "product_name": "Jam",
      //                 "description": "<p>Jam Dinding ....</p>",
      //                 "variation": [],
      //                 "type": "product",
      //                 "category": "123",
      //                 "status": "ACTIVE",
      //                 "weight": 500,
      //                 "dimensions": {
      //                     "length": 10,
      //                     "width": 10,
      //                     "height": 10
      //                 },
      //                 "volume_weight": 166.66666666666666,
      //                 "images_gallery": [
      //                     {
      //                         "base_url": "https://res.cloudinary.com/pt-cls-system/image/upload/",
      //                         "pic_file_name": "v1630377171/admin/image/589-5899856_png-file-svg-history-icon-svg-transparent-png.png612d94d1bc310.jpg",
      //                         "pic_small_path": "q_auto:good,w_150/v1630377171/admin/image/589-5899856_png-file-svg-history-icon-svg-transparent-png.png612d94d1bc310.jpg",
      //                         "pic_medium_path": "q_auto:good,w_600/v1630377171/admin/image/589-5899856_png-file-svg-history-icon-svg-transparent-png.png612d94d1bc310.jpg",
      //                         "pic_big_path": "v1630377171/admin/image/589-5899856_png-file-svg-history-icon-svg-transparent-png.png612d94d1bc310.jpg"
      //                     }
      //                 ],
      //                 "created_date": "2021-08-31 09:34:33",
      //                 "updated_date": "2021-12-16 10:19:40",
      //                 "sku_price": 20000,
      //                 "sku_value": 20000,
      //                 "variant": null,
      //                 "qty": 93,
      //                 "image_gallery": null,
      //                 "product_info": [
      //                     {
      //                         "_id": "612d9539f25b8577b2145fc4",
      //                         "product_name": "Jam",
      //                         "product_code": "jam123",
      //                         "description": "<p>Jam Dinding ....</p>",
      //                         "variation": [],
      //                         "type": "product",
      //                         "category": "123",
      //                         "status": "ACTIVE",
      //                         "weight": 500,
      //                         "dimensions": {
      //                             "length": 10,
      //                             "width": 10,
      //                             "height": 10
      //                         },
      //                         "volume_weight": 166.66666666666666,
      //                         "images_gallery": [
      //                             {
      //                                 "base_url": "https://res.cloudinary.com/pt-cls-system/image/upload/",
      //                                 "pic_file_name": "v1630377171/admin/image/589-5899856_png-file-svg-history-icon-svg-transparent-png.png612d94d1bc310.jpg",
      //                                 "pic_small_path": "q_auto:good,w_150/v1630377171/admin/image/589-5899856_png-file-svg-history-icon-svg-transparent-png.png612d94d1bc310.jpg",
      //                                 "pic_medium_path": "q_auto:good,w_600/v1630377171/admin/image/589-5899856_png-file-svg-history-icon-svg-transparent-png.png612d94d1bc310.jpg",
      //                                 "pic_big_path": "v1630377171/admin/image/589-5899856_png-file-svg-history-icon-svg-transparent-png.png612d94d1bc310.jpg"
      //                             }
      //                         ],
      //                         "created_date": "2021-08-31 09:34:33",
      //                         "updated_date": "2021-08-31 09:34:33",
      //                         "created_by": "60b0aadc75a3122cf46a66b8",
      //                         "updated_by": "60b0aadc75a3122cf46a66b8",
      //                         "created_at": "178.128.19.191",
      //                         "updated_at": "178.128.19.191"
      //                     }
      //                 ],
      //                 "product_sku": [
      //                     {
      //                         "_id": "612d9539f25b8577b2145fc5",
      //                         "sku_code": "jam123-001",
      //                         "sku_price": 20000,
      //                         "sku_value": 20000,
      //                         "product_id": "612d9539f25b8577b2145fc4",
      //                         "product_code": "jam123",
      //                         "variant": null,
      //                         "qty": 93,
      //                         "image_gallery": null,
      //                         "status": "ACTIVE",
      //                         "created_date": "2021-08-31 09:34:33",
      //                         "updated_date": "2021-12-16 10:19:40",
      //                         "created_by": "60b0aadc75a3122cf46a66b8",
      //                         "updated_by": "60b0aadc75a3122cf46a66b8",
      //                         "released_date": "2021-12-16 10:19:40",
      //                         "updated_at": "178.128.19.191",
      //                         "evoucher": []
      //                     }
      //                 ],
      //                 "product_id": "612d9539f25b8577b2145fc5",
      //                 "total_product_price": 20000
      //             },
      //             {
      //                 "quantity": 1,
      //                 "product_code": "payung",
      //                 "sku_code": "payung-001",
      //                 "product_name": "Payung Hitam",
      //                 "description": "<p>Payung Berwarna Hitam</p>",
      //                 "variation": [],
      //                 "type": "product",
      //                 "category": "123",
      //                 "status": "ACTIVE",
      //                 "weight": 500,
      //                 "dimensions": {
      //                     "length": 1,
      //                     "width": 1,
      //                     "height": 1
      //                 },
      //                 "volume_weight": 0.16666666666666666,
      //                 "images_gallery": [
      //                     {
      //                         "base_url": "https://res.cloudinary.com/pt-cls-system/image/upload/",
      //                         "pic_file_name": "v1629865833/admin/image/payung.jpeg6125c767ece68.jpg",
      //                         "pic_small_path": "q_auto:good,w_150/v1629865833/admin/image/payung.jpeg6125c767ece68.jpg",
      //                         "pic_medium_path": "q_auto:good,w_600/v1629865833/admin/image/payung.jpeg6125c767ece68.jpg",
      //                         "pic_big_path": "v1629865833/admin/image/payung.jpeg6125c767ece68.jpg"
      //                     }
      //                 ],
      //                 "created_date": "2021-08-25 11:31:24",
      //                 "updated_date": "2021-12-16 10:19:40",
      //                 "sku_price": 20000,
      //                 "sku_value": 20000,
      //                 "variant": null,
      //                 "qty": 82,
      //                 "image_gallery": null,
      //                 "product_info": [
      //                     {
      //                         "_id": "6125c79cb114fe47240d3bf5",
      //                         "product_name": "Payung Hitam",
      //                         "product_code": "payung",
      //                         "description": "<p>Payung Berwarna Hitam</p>",
      //                         "variation": [],
      //                         "type": "product",
      //                         "category": "123",
      //                         "status": "ACTIVE",
      //                         "weight": 500,
      //                         "dimensions": {
      //                             "length": 1,
      //                             "width": 1,
      //                             "height": 1
      //                         },
      //                         "volume_weight": 0.16666666666666666,
      //                         "images_gallery": [
      //                             {
      //                                 "base_url": "https://res.cloudinary.com/pt-cls-system/image/upload/",
      //                                 "pic_file_name": "v1629865833/admin/image/payung.jpeg6125c767ece68.jpg",
      //                                 "pic_small_path": "q_auto:good,w_150/v1629865833/admin/image/payung.jpeg6125c767ece68.jpg",
      //                                 "pic_medium_path": "q_auto:good,w_600/v1629865833/admin/image/payung.jpeg6125c767ece68.jpg",
      //                                 "pic_big_path": "v1629865833/admin/image/payung.jpeg6125c767ece68.jpg"
      //                             }
      //                         ],
      //                         "created_date": "2021-08-25 11:31:24",
      //                         "updated_date": "2021-08-25 11:36:25",
      //                         "created_by": "60b0aadc75a3122cf46a66b8",
      //                         "updated_by": "60b0aadc75a3122cf46a66b8",
      //                         "created_at": "178.128.19.191",
      //                         "updated_at": "178.128.19.191",
      //                         "released_date": "2021-08-25 11:36:25"
      //                     }
      //                 ],
      //                 "product_sku": [
      //                     {
      //                         "_id": "6125c79cb114fe47240d3bf6",
      //                         "sku_code": "payung-001",
      //                         "sku_price": 20000,
      //                         "sku_value": 20000,
      //                         "product_id": "6125c79cb114fe47240d3bf5",
      //                         "product_code": "payung",
      //                         "variant": null,
      //                         "qty": 82,
      //                         "image_gallery": null,
      //                         "status": "ACTIVE",
      //                         "created_date": "2021-08-25 11:31:24",
      //                         "updated_date": "2021-12-16 10:19:40",
      //                         "created_by": "60b0aadc75a3122cf46a66b8",
      //                         "updated_by": "60b0aadc75a3122cf46a66b8",
      //                         "released_date": "2021-12-16 10:19:40",
      //                         "updated_at": "178.128.19.191",
      //                         "evoucher": []
      //                     }
      //                 ],
      //                 "product_id": "6125c79cb114fe47240d3bf6",
      //                 "total_product_price": 20000
      //             }
      //         ],
      //         "total_item_value": 40000,
      //         "total_item_qty": 2,
      //         "order_id": "MCI-7042135947350351200-DEV",
      //         "reference_no": "MCI-7042135947350351200-DEV-2",
      //         "status": "PENDING",
      //         "delivery_type": "product",
      //         "created_date": "2021-12-16 10:24:26",
      //     },
      //     {
      //         "_id": "61bab16a8fee047f4b74ab88",
      //         "origin": {
      //             "name": "CLS System",
      //             "cell_phone": "02122686277",
      //             "address": "Jl. Kebon Jeruk VII No.2E, RT.4/RW.5, Maphar, Kec. Taman Sari, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11160"
      //         },
      //         "destination": {
      //             "_id": "6171303c68af5c219c7da552",
      //             "name": "Gunawarman",
      //             "cell_phone": "0182923819182",
      //             "address": "Jalan Kebon Jeruk No. VII No 2e",
      //             "province_name": "DKI Jakarta",
      //             "city_name": "Jakarta Barat",
      //             "district_name": "Taman Sari",
      //             "village_name": "Maphar",
      //             "postal_code": "11160",
      //             "use_as": "main_address"
      //         },
      //         "product_list": [
      //             {
      //                 "quantity": 1,
      //                 "product_code": "EMS-10-GR",
      //                 "sku_code": "EMS-10-GR-EMS-10-GR",
      //                 "product_name": "emas 10 gr",
      //                 "variation": [],
      //                 "type": "gold",
      //                 "category": "",
      //                 "status": "ACTIVE",
      //                 "weight": 15,
      //                 "dimensions": {
      //                     "length": 10,
      //                     "width": 10,
      //                     "height": 10
      //                 },
      //                 "volume_weight": 166.66666666666666,
      //                 "images_gallery": [
      //                     {
      //                         "base_url": "https://res.cloudinary.com/pt-cls-system/image/upload/",
      //                         "pic_file_name": "v1629686230/admin/image/9db64213-fd5b-4d6b-ab0c-3865b98020e2.png612309d5862c2.jpg",
      //                         "pic_small_path": "q_auto:good,w_150/v1629686230/admin/image/9db64213-fd5b-4d6b-ab0c-3865b98020e2.png612309d5862c2.jpg",
      //                         "pic_medium_path": "q_auto:good,w_600/v1629686230/admin/image/9db64213-fd5b-4d6b-ab0c-3865b98020e2.png612309d5862c2.jpg",
      //                         "pic_big_path": "v1629686230/admin/image/9db64213-fd5b-4d6b-ab0c-3865b98020e2.png612309d5862c2.jpg"
      //                     }
      //                 ],
      //                 "created_date": "2021-12-14 14:31:39",
      //                 "updated_date": "2021-12-14 14:31:39",
      //                 "sku_price": null,
      //                 "sku_value": 9000000,
      //                 "variant": null,
      //                 "qty": 100,
      //                 "image_gallery": null,
      //                 "product_info": [
      //                     {
      //                         "_id": "61b8485b1bc38a1a5630e8a4",
      //                         "product_name": "emas 10 gr",
      //                         "product_code": "EMS-10-GR",
      //                         "variation": [],
      //                         "type": "gold",
      //                         "category": "",
      //                         "status": "ACTIVE",
      //                         "weight": 15,
      //                         "dimensions": {
      //                             "length": 10,
      //                             "width": 10,
      //                             "height": 10
      //                         },
      //                         "volume_weight": 166.66666666666666,
      //                         "images_gallery": [
      //                             {
      //                                 "base_url": "https://res.cloudinary.com/pt-cls-system/image/upload/",
      //                                 "pic_file_name": "v1629686230/admin/image/9db64213-fd5b-4d6b-ab0c-3865b98020e2.png612309d5862c2.jpg",
      //                                 "pic_small_path": "q_auto:good,w_150/v1629686230/admin/image/9db64213-fd5b-4d6b-ab0c-3865b98020e2.png612309d5862c2.jpg",
      //                                 "pic_medium_path": "q_auto:good,w_600/v1629686230/admin/image/9db64213-fd5b-4d6b-ab0c-3865b98020e2.png612309d5862c2.jpg",
      //                                 "pic_big_path": "v1629686230/admin/image/9db64213-fd5b-4d6b-ab0c-3865b98020e2.png612309d5862c2.jpg"
      //                             }
      //                         ],
      //                         "created_date": "2021-12-14 14:31:39",
      //                         "updated_date": "2021-12-14 14:31:39",
      //                         "created_by": "60af6c6d303023241652ef82",
      //                         "updated_by": "60af6c6d303023241652ef82",
      //                         "created_at": "178.128.19.191",
      //                         "updated_at": "178.128.19.191"
      //                     }
      //                 ],
      //                 "product_sku": [
      //                     {
      //                         "_id": "61b8485b1bc38a1a5630e8a5",
      //                         "sku_code": "EMS-10-GR-EMS-10-GR",
      //                         "sku_price": null,
      //                         "sku_value": 9000000,
      //                         "product_id": "61b8485b1bc38a1a5630e8a4",
      //                         "product_code": "EMS-10-GR",
      //                         "variant": null,
      //                         "qty": 100,
      //                         "image_gallery": null,
      //                         "status": "ACTIVE",
      //                         "created_date": "2021-12-14 14:31:39",
      //                         "updated_date": "2021-12-14 14:31:39",
      //                         "created_by": "60af6c6d303023241652ef82",
      //                         "updated_by": "60af6c6d303023241652ef82",
      //                         "evoucher": []
      //                     }
      //                 ],
      //                 "product_id": "61b8485b1bc38a1a5630e8a5",
      //                 "total_product_price": 9000000
      //             }
      //         ],
      //         "total_item_value": 9000000,
      //         "total_item_qty": 1,
      //         "order_id": "MCI-7042135947350351200-DEV",
      //         "reference_no": "MCI-7042135947350351200-DEV-1",
      //         "status": "PENDING",
      //         "delivery_type": "gold",
      //         "created_date": "2021-12-16 10:24:26",
      //     }
      // ];
        

        // this.showLoading = false;
      } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type

        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
        }
      }
    // }
  }

  type(array) {
    let res = [];
    this.allData.forEach((el, i) => {
      el.products.forEach((element) => {
        res.push(element.type)
      })
      // Object.assign(this.allData[i], this.prodType)

    });
    // console.log("hasil",res[0])

    return res[array]
  }


  //When Page Changed
  async onChangePage(pageNumber) {
    if (pageNumber <= 0) {
      pageNumber = 1;
    }
    if (pageNumber >= this.total_page) {
      pageNumber = this.total_page;
    }

    this.currentPage = pageNumber;
    console.log(pageNumber, this.currentPage, this.total_page);

    this.valuechange({}, false, false);
  }

  //Order by Function
  async orderBySelected() {
    let params;
    if (this.orderBy == "highest transactions") {
      this.orderByResult = {
        sum_total: -1
      }
      this.valuechange({}, false, false)
    }
    else if (this.orderBy == "lowest transactions") {
      this.orderByResult = {
        sum_total: 1
      }
      this.valuechange({}, false, false)
    }
    else if (this.orderBy == "newest") {
      this.orderByResult = {
        request_date: -1
      }
      this.valuechange({}, false, false)
    }
  }

  //Start of Pagination
  pagination(c, m) {
    var current = c,
      last = m,
      delta = 2,
      left = current - delta,
      right = current + delta + 1,
      range = [],
      rangeWithDots = [],
      l;

    for (let i = 1; i <= last; i++) {
      if (i == 1 || i == last || i >= left && i < right) {
        range.push(i);
      }
    }

    for (let i of range) {
      if (l) {
        if (i - l === 2) {
          rangeWithDots.push(l + 1);
        } else if (i - l !== 1) {
          rangeWithDots.push('...');
        }
      }
      rangeWithDots.push(i);
      l = i;
    }

    return rangeWithDots;
  }

  buildPagesNumbers(totalPage) {
    this.pageNumbering = [];
    this.total_page = totalPage;
    this.pageNumbering = this.pagination(this.currentPage, totalPage);

  }

  //End of Pagination

  //Search by text function
  async searchText($event) {
    let result = $event.target.value
    console.log("Search", result);
    if (result.length > 2) {
      this.currentPage = 1;
      let params
      if (this.filter == "order_id") {
        let order_id = {
          order_id: result,
          get_detail: true
        }
        Object.assign(this.form_input, order_id)
      }
      else if (this.filter == "reference_no") {
        let reference_no = {
          reference_no: result,
          get_detail: true
        }
        Object.assign(this.form_input, reference_no)
      }
      else if (this.filter == "product_name") {
        let product_name = {
          "product_list.product_name": result,
          get_detail: true
        }
        Object.assign(this.form_input, product_name)
      }
      else if (this.filter == "receiver") {
        let receiver = {
          "destination.name": result,
          get_detail: true
        }
        Object.assign(this.form_input, receiver)
      }
      else if (this.filter == "awb_number") {
        let awb_number = {
          awb_number: result,
          get_detail: true
        }
        Object.assign(this.form_input, awb_number)
      }
      this.valuechange({}, false, false);
    }
    else if (!result) {
      this.form_input = {};
      this.valuechange({}, false, false);
    }
  }

  //Get the detail Function
  async getDetail(data:any) {
    // this.orderDetail = await this.orderhistoryService.getOrderDetail(idDetail);
    this.propsDetail = data;
    this.orderDetail = true;
    // await this.orderhistoryService.getShippingDetail(data.order_id);
    // console.log(idDetail)
    // console.log("data needed", this.allData);
    // this.orderDetail = this.allData.find(order => order.order_id == idDetail);
    // this.router.navigate(['administrator/order-detail'],
    //   {
    //     queryParams: {
    //       id: idDetail
    //     }
    //   })
  }

  //When value change
  valuechange(event, input_name, download?: boolean) {

    if (this.onSearchActive == false) {
      this.onSearchActive = true;
      this.showLoading = true;
      let myVar = setTimeout(async () => {
        if (this.form_input) {
          let clearFormInput = this.form_input
          let searchDate = {};

          // if (clearFormInput.request_date) {
          //   if (clearFormInput.request_date.to) {
          //     let request_date_from_to = {
          //       request_date_from: clearFormInput.request_date.from,
          //       request_date_to: clearFormInput.request_date.to
          //     }
          //     Object.assign(clearFormInput, request_date_from_to)
          //   }
          //   else {
          //     let request_date_from = {
          //       request_date_from: clearFormInput.request_date.from,
          //     }
          //     Object.assign(clearFormInput, request_date_from)
          //   }

          //   delete clearFormInput.request_date
          // }

          if(clearFormInput.approve_date && Object.keys(clearFormInput.approve_date).length === 0 && clearFormInput.approve_date.constructor === Object) {
            delete clearFormInput.approve_date;
          }

          // this.showLoading = true;
          // clearFormInput.status = "PROCESSED";
          let filter = {
            search: clearFormInput,
            order_by: this.orderByResult,
            limit_per_page: this.pageLimits,
            current_page: this.currentPage,
            download: false
          }

          // let result = await searchCallback[0][searchCallback[1]](filter);
          let result = await this.orderhistoryService.searchShippingReport(filter);
          this.allData = result.values;
          this.buildPagesNumbers(result.total_page)
        } else {
          this.firstLoad();
        }
        this.onSearchActive = false;
        this.showLoading = false;

      }, 2000);
    }

  }

  //Start of DatePicker Function

  datePickerOnDateSelection(date: NgbDate, formName) {
    let _this: any = this.make_This(formName);

    if (!_this.fromDate && !_this.toDate) {
      _this.fromDate = date;
    }
    else if (_this.fromDate && !_this.toDate && this.datePickerDateAfter(date, _this.fromDate)) {
      _this.toDate = date;
    }
    else {
      _this.toDate = null;
      _this.fromDate = date;
    }

    _this.from = (_this.fromDate != undefined && _this.fromDate != null) ? this.convertToDateString(_this.fromDate) : ''
    _this.to = (_this.toDate != undefined && _this.toDate != null) ? this.convertToDateString(_this.toDate) : ''
  }

  typeof(object) {
    return typeof object;
  }

  make_This(formName) {
    if (typeof formName == 'object') {
      return formName;
    }

    if (this.form_input[formName] == undefined || this.form_input[formName] == '') {
      this.form_input[formName] = {};
    }
    return this.form_input[formName];

  }

  dateToggler(formName) {
    this.toggler[formName] = (typeof this.toggler == undefined) ? true : !this.toggler[formName];
    if (this.toggler[formName] == false && this.form_input[formName] !== '') {
      this.valuechange({}, false, false);
    }
  }

  dateClear(event, dataInput, deleteInput) {
    this.form_input[dataInput] = '';
    this.valuechange(event, deleteInput);
  }

  datePickerOnDateIsInside(date: NgbDate, formName) {
    let _this: any = this.make_This(formName)

    return this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.toDate);
  }

  datePickerOnDateIsRange(date: NgbDate, formName) {
    let _this: any = this.make_This(formName)

    return this.datePickerDateEquals(date, _this.fromDate) || this.datePickerDateEquals(date, _this.toDate) || this.datePickerOnDateIsInside(date, _this) || this.datePickerOnDateIsHovered(date, _this);
  }

  datePickerOnDateIsHovered(date: NgbDate, formName) {
    let _this: any = this.make_This(formName)
    return _this.fromDate && !_this.toDate && _this.hoveredDate && this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.hoveredDate);
  }

  datePickerDateAfter(datePrev: NgbDate, dateAfter: NgbDate) {
    if (datePrev == undefined || dateAfter == undefined) return false

    let iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day)
    let iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day)
    if (iToDate.getTime() < iFromDate.getTime()) {
      return true
    }
    return false
  }

  convertToDateString(datePrev: NgbDate) {
    console.log(datePrev)
    return datePrev.year.toString().padStart(2, "0") + '-' + datePrev.month.toString().padStart(2, "0") + '-' + datePrev.day.toString().padStart(2, "0")
  }
  datePickerDateEquals(datePrev: NgbDate, dateAfter: NgbDate) {
    if (datePrev == undefined || dateAfter == undefined) return false

    let iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day)
    let iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day)
    if (iToDate.getTime() == iFromDate.getTime()) {
      return true
    }
    return false
  }

  datePickerDateBefore(datePrev: NgbDate, dateAfter: NgbDate) {
    if (datePrev == undefined || dateAfter == undefined) return false

    let iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day)
    let iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day)
    if (iToDate.getTime() > iFromDate.getTime()) {
      return true
    }
    return false
  }

  //End of DatePicker Function

  //Start of Checkbox Function
  onChecked() {
    setTimeout(() => {
      let getData = [];
      this.checkBox.forEach((el, i) => {
        var data = this.allData[i];
        console.warn("var data", data);
        console.log(el);
        if (el) {
          getData.push(data.reference_no)
        }
        else {
          var index = getData.indexOf(data.booking_id);
          if (index !== -1) getData.splice(index, 1);
        }
      })
      this.getDataCheck = getData;
      // console.warn("getData", getData);
    }, 100);
  }

  checkedAll() {
    setTimeout(() => {
      let getData = [];
      this.allData.forEach((e, i) => {
        // console.log("result", e);
        if (this.activeCheckbox) {
          this.checkBox[i] = this.activeCheckbox;
          getData.push(e.reference_no);
        }
        else {
          this.checkBox = [];
        }
      })
      this.getDataCheck = getData;
      // console.log(getData);
    }, 100);
  }

  printLabel() {
    try {
      const payload = {
        "reference_no":this.getDataCheck,
        "status":"ya"
      }
      this.orderhistoryService.setPrintedMark(payload);
      if(this.getDataCheck.length > 0) this.shippingLabel = this.getDataCheck.map(list => this.allData.find(delivery => delivery.reference_no == list));
    } catch (error) {
      
    }
  }

  addToPackingList() {
    Swal.fire({
      title: 'Confirmation',
      text: 'Apakah anda yakin ingin menambahkan order yang dipilih ke dalam packing list?',
      confirmButtonText: 'Process',
      cancelButtonText:"Cancel",
      showCancelButton:true
      }).then(async (result) => {
      if(result.isConfirmed){
        try {
          const payload = {
            "reference_no":this.getDataCheck,
          }
          this.orderhistoryService.createPackingList(payload).then((result) =>{
            console.log("result", result);
            Swal.fire({
              title: 'Success',
              text: 'Semua order yang terpilih telah ditambahkan kedalam packing list.',
              icon: 'success',
              confirmButtonText: 'Ok',
              showCancelButton:false
              }).then(async (result) => {
                if(result.isConfirmed){
                  this.activeCheckbox = false;
                  this.checkBox = [];
                  this.getDataCheck = [];
                  this.firstLoad();
                }
              });
          }, (err) => {
            let _error = ((<Error>err).message);this.errorLabel = ((<Error>err).message);
            Swal.fire({
              title: 'Error',
              text: _error,
              icon: 'error',
              confirmButtonText: 'Ok',
              showCancelButton:false
              }).then(async (result) => {
                if(result.isConfirmed){
                  this.activeCheckbox = false;
                  this.checkBox = [];
                  this.getDataCheck = [];
                  this.firstLoad();
                }
              });
          });
        } catch (error) {
          console.log(error);
        }
      }
    });
  }

  async deletePrintLabel() {
    if(this.getDataCheck.length > 0) {
      Swal.fire({
        title: 'Confirmation',
        text: 'Apakah anda yakin ingin menghapus print marked order yang dipilih?',
        // icon: 'success',
        confirmButtonText: 'Process',
        cancelButtonText:"Cancel",
        showCancelButton:true
        }).then(async (result) => {
        if(result.isConfirmed){
          try {
            const payload = {
              "reference_no":this.getDataCheck,
              "status":""
            }
            await this.orderhistoryService.setPrintedMark(payload);

            Swal.fire({
              title: 'Success',
              text: 'Semua order yang terpilih telah dihapus print marked nya.',
              icon: 'success',
              confirmButtonText: 'Ok',
              showCancelButton:false
              }).then(async (result) => {
                if(result.isConfirmed){
                  this.activeCheckbox = false;
                  this.checkBox = [];
                  this.getDataCheck = [];
                  this.firstLoad();
                }
              });
            // if(this.getDataCheck.length > 0) this.shippingLabel = this.getDataCheck.map(list => this.allData.find(order => order.order_id == list));
          } catch (error) {
            
          }
          // console.log("member ids", memberIds);
          
        }
      });
    }
  }

  async clickToDownloadReport() {
    //reportTable ID

    try {      
      // // Download All Order Report
      // let no_filter = {
      //   search: {
      //     status: {
      //       in:["PROCESSED","COMPLETED"]
      //     }
      //   },
      //   order_by: this.orderByResult,
      // }
      // // Download Incoming Order Report
      // let no_filter_incoming = {
      //   search: {
      //     status: "PROCESSED",
      //     last_shipping_info: "on_processing"
      //   },
      //   order_by: this.orderByResult,
      // }
      // // Download On Delivery Order Report
      // let no_filter_delivery = {
      //   search: {
      //     status: "PROCESSED",
      //     last_shipping_info: "on_delivery"
      //   },
      //   order_by: this.orderByResult,
      // }
      // // Download Complete Order Report
      // let no_filter_complete = {
      //   search: {
      //     status: "COMPLETED",
      //     last_shipping_info: "delivered"
      //   },
      //   order_by: this.orderByResult,
      // }

      this.showLoading = true;
      let clearFormInput = this.form_input;
      let filter = {
        search: clearFormInput,
        order_by: this.orderByResult,
      }

      // let result = await searchCallback[0][searchCallback[1]](filter);
      let result = await this.orderhistoryService.searchOrderHistory(filter);
      this.allDataToDownload = result.values;
      console.warn("allDataDownload", this.allDataToDownload);


      setTimeout(async () => {
        let dataType = 'application/vnd.ms-excel';
        let tableSelect = document.getElementById('reportTable');
        let tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

        // Specify file name
        let filename = 'excel_data.xls';
        let downloadLink = document.createElement("a");
        document.body.appendChild(downloadLink);

        if (navigator.msSaveOrOpenBlob) {
          var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
          });
          navigator.msSaveOrOpenBlob(blob, filename);
        } else {
          // Create a link to the file
          downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

          // Setting the file name
          downloadLink.download = filename;

          //triggering the function
          downloadLink.click();
          this.showLoading = false;
        }
      }, 2000);
    } catch(e) {
      this.showLoading = false;
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  //see more detail function
  setSeeMore(index) {
    this.allData[index].product_status = true;
    // console.log(this.allData)
  }

  //see more detail function
  setSeeLess(index) {
    this.allData[index].product_status = false;
    // console.log(this.allData)
  }

  public backToHere(obj){
    obj.shippingLabel = false;
    obj.orderDetail = false;
    obj.checkBox = [];
    obj.activeCheckbox = false;
    obj.getDataCheck = [];
    obj.firstLoad();
  }
}
