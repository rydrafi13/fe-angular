import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeliveryProcessComponent } from './delivery-process.component';
import { ShippingLabelComponent } from './shipping-label/shipping-label.component';
// import { OrderHistoriesAddComponent } from './add/order-histories.add.component';
// import { OrderHistoriesDetailComponent } from './detail/order-histories.detail.component';
import { DeliveryProcessEditComponent } from './edit/delivery-process.edit.component';

const routes: Routes = [
  {
      path: '', component: DeliveryProcessComponent,
  },
  {
    path: 'shipping-label', component: ShippingLabelComponent,
  },
//   {
//       path:'add', component: OrderHistoriesAddComponent
//   },
  // {
  //   path:'edit', component: OrderHistoriesEditComponent 
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryProcessRoutingModule { }
