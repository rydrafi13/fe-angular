import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShippingLabelRoutingModule } from './shipping-label-routing.module';
import { ShippingLabelComponent } from './shipping-label.component';
import { OrderhistoryallhistoryEditModule } from '../../orderhistoryallhistory/edit/orderhistoryallhistory.edit.module';
import { NgxBarcodeModule } from 'ngx-barcode';


@NgModule({
  declarations: [
    ShippingLabelComponent
  ],
  imports: [
    NgxBarcodeModule,
    CommonModule,
    ShippingLabelRoutingModule,
    // OrderhistoryallhistoryEditModule
  ],
  exports: [
    ShippingLabelComponent
  ]
})
export class ShippingLabelModule { }
