import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShippingLabelComponent } from './shipping-label.component';

const routes: Routes = [
  {
    path: '', component: ShippingLabelComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShippingLabelRoutingModule { }
