import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
// import { isFunction } from '../../../../object-interface/common.function';
// import { NgxBarcodeModule } from 'ngx-barcode';
import { Router } from '@angular/router';
// import { MemberService } from '../../../../services/member/member.service';
import Swal from 'sweetalert2';
import { NgbDate, NgbModal, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-delivery-process-edit',
	templateUrl: './delivery-process.edit.component.html',
	styleUrls: [ './delivery-process.edit.component.scss' ],
	animations: [ routerTransition() ]
})
export class DeliveryProcessEditComponent implements OnInit {
	@Input() public detail: any;
	@Input() public back;
	@Input() public propsDetail;

	deliveryHistoryDetail:any;
	orderDetail:any;
	calculate:any;
	otherCourValue: any;
	detailPage:any;
	awb_check = false;
	infoDetail = [];
	moreOrderDetail: any;
	openMoreDetail = false;
	urlReceipt: any;
	public loading: boolean = false;
	urlLabel: any;
	editThis = false;
	public done: boolean = false;
	public notesCategory: any = [
		{
			label: 'Product out of stock',
			value: false
		},
		{
			label: 'Color options are not available',
			value: false
		},
		{
			label: 'Product size is not available',
			value: false
		},
		{
			label: 'Other',
			value: false
		}
	];
	categoryNotes: any;
	public shippingStatusList: any = [
		{
			label: 'on checking and processing',
			value: 'on_processing',
			id: 'on-process',
			checked: true,
			hovered: false,
			values: 'on_processing'
		},
		{
			label: 'On Delivery process',
			value: 'on_delivery',
			id: 'on-delivery',
			hovered: false,
			values: 'on_delivery'
		},
		{
			label: 'Delivered',
			value: 'delivered',
			id: 'delivered',
			hovered: false,
			values: 'delivered'
		}
	];

	public redeliverList: any = [
		{
			label: 'Return Order Process',
			value: 'redeliver',
			checked: true,
			hovered: false,
		},
		{
			label: 'Return Order Redelivery',
			value: 'redelivery',
			checked: false,
			hovered: false,
		},
		{
			label: 'Delivered',
			value: 'delivered',
			id: 'delivered',
			hovered: false,
			values: 'delivered'
		}
	];

	packaging = false;

	statusValue: any;
	statusLabel: any;

	public transactionStatus: any = [
		{ label: 'PAID', value: 'PAID' },
		{ label: 'CANCEL', value: 'CANCEL' },
		{ label: 'WAITING FOR CONFIRMATION', value: 'WAITING FOR CONFIRMATION' }
	];
	service_courier: any = [
		{ label: 'Drop', value: 'DROP'},
		{ label: 'Pickup', value: 'PICKUP'}
	]
	errorLabel: any = false;
	transaction_status: any;
	merchantMode = false;
	sum_total: any;
	currentPermission;
	info = [];
	change = false;
	notes: any;
	isClicked = false;
	isClicked2 = false;
	courier_list = [];
	tempstatus = '';
	openNotes = false;
	editAwb = false;
	openNotes2: boolean;
	holdvalue: any;
	infoDetailLabel: any;
	othercour = false;
	choose_service: any;
	supported= false;
	method_delivery :any;
	courier:any = '' ;
    delivery_service:any = '';
    delivery_method:any = '';
	courier_name:any = '';
    awb_number:any;
	changeAWB: Boolean = false;
	delivery_remarks: any = "";

	isChangeLoading : Boolean = false;
	isChangeAWBLoading : Boolean = false;
	// detailToLowerCase: string = "";

	isEverDelivered : Boolean = false;

	date: NgbDate;
	time: NgbTimeStruct;
	receiver_name:any;
	delivery_status:any;
	remark:any;
	completeButtonConfirmation: Boolean = false;
	isCompleteOrder: Boolean = false;

	returnType:any = "redeliver";

	isRedeliver:boolean = false;
	isChangeRedeliverLoading : Boolean = false;
	redeliverButtonConfirmation : boolean = false;
	return_date: NgbDate;
	return_time: NgbTimeStruct;
	return_courier:any;
	return_awb_number:any;
	return_remarks:any;


	isProcessRedelivery:boolean = false;
	isChangeProcessRedeliveryLoading : Boolean = false;
	processOrderRedeliveryButtonConfirmation : boolean = false;
	redelivery_date: NgbDate;
	redelivery_time: NgbTimeStruct;
	redelivery_courier:any;
	redelivery_awb_number:any;
	redelivery_remarks:any;
	redelivery_method:any;
	redelivery_services:any;

	isRedeliveryCompleted:boolean = false;


	isReorder:boolean = false;
	isChangeReorderLoading : Boolean = false;
	reorderButtonConfirmation : boolean = false;
	reorder_date: NgbDate;
	reorder_time: NgbTimeStruct;
	reorder_courier:any;
	reorder_awb_number:any;
	reorder_remarks:any;

	shippingDetail:any;

	order_id:any;

	constructor(
		public orderhistoryService: OrderhistoryService,
		// private route: ActivatedRoute,
		private router: Router,
		// public memberService: MemberService
		private modalService: NgbModal
	) {}

	async ngOnInit() {
		this.currentPermission = 'admin';
		// this.deliveryHistoryDetail = this.detail.values;
		this.order_id = this.propsDetail.order_id;

		console.log("this.propsDetail", this.propsDetail);
		await this.firstLoad();
	}

	async firstLoad() {
		console.warn("detail", this.detail);
		console.warn("props detail", this.propsDetail);

		let result = await this.orderhistoryService.getShippingDetail(this.order_id);
		if(result && result.values) {
			this.deliveryHistoryDetail = result.values;
		}

		this.orderDetail = await this.orderhistoryService.getOrderDetail(this.order_id);

		console.log("orderDetail", this.orderDetail);
		// await this.updateDetailOrder();
		// console.log("courier name",this.courier);
		this.changeAWB = false;
		if(this.deliveryHistoryDetail.available_courier){
			this.deliveryHistoryDetail.available_courier.push(
				{
					courier:"others",
					courier_code:"others"
				}
			);
		} else {
			this.courier = 'others'
		}
		// if(this.deliveryHistoryDetail.delivery_detail.courier){
		// 	this.courier = this.deliveryHistoryDetail.delivery_detail.courier;
		// 	this.delivery_service = this.deliveryHistoryDetail.delivery_detail.delivery_service;
    	// 	this.delivery_method = this.deliveryHistoryDetail.delivery_detail.delivery_method;
    	// 	this.awb_number = this.deliveryHistoryDetail.delivery_detail.awb_number;
		// 	if(this.deliveryHistoryDetail.delivery_detail.courier == 'others') this.courier_name = this.deliveryHistoryDetail.delivery_detail.courier_name;
		// }

		// this.deliveryHistoryDetail.shipping_info.forEach((element) => {
		// 	if(element.label == "on_delivery") {
		// 		this.delivery_remarks = element.remarks != null? element.remarks : "";
		// 	}

		// 	if(element.label == "return") {
		// 		this.return_remarks = element.remarks;
		// 		this.reorder_remarks = element.remarks;
		// 	}

		// 	if(element.label == "redelivery") {
		// 		this.redelivery_remarks = element.remarks;
		// 	}

		// 	if(element.label == "delivered") {
		// 		this.isEverDelivered = true;
		// 	}
		// });

		if(this.deliveryHistoryDetail.status == "RETURN" && this.deliveryHistoryDetail.return_detail) {
			this.isRedeliver = true;

			this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
			this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
			this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
			this.return_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
			// this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
		}

		if(this.deliveryHistoryDetail.status == "PROCESSED" && this.deliveryHistoryDetail.redelivery_detail && this.deliveryHistoryDetail.return_detail) {
			this.isRedeliver = true;

			this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
			this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
			this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
			this.return_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
			// this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;

			this.isProcessRedelivery = true;
			this.redeliverList[1].checked = true;
			this.redelivery_date = this.convertStringToNgDate(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
			this.redelivery_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
			this.redelivery_courier = this.deliveryHistoryDetail.redelivery_detail.courier;
			this.redelivery_awb_number = this.deliveryHistoryDetail.redelivery_detail.awb_number;
			// this.redelivery_remarks = this.deliveryHistoryDetail.redelivery_detail.remarks;
			this.redelivery_method = this.deliveryHistoryDetail.redelivery_detail.delivery_method;
			this.redelivery_services = this.deliveryHistoryDetail.redelivery_detail.delivery_service;
		}





		if(this.deliveryHistoryDetail.status == "COMPLETED" && this.deliveryHistoryDetail.redelivery_detail && this.deliveryHistoryDetail.return_detail) {
			this.isRedeliver = true;

			this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
			this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
			this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
			this.return_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
			// this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;

			this.isProcessRedelivery = true;
			this.redeliverList[1].checked = true;
			this.redeliverList[2].checked = true;

			this.redelivery_date = this.convertStringToNgDate(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
			this.redelivery_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
			this.redelivery_courier = this.deliveryHistoryDetail.redelivery_detail.courier;
			this.redelivery_awb_number = this.deliveryHistoryDetail.redelivery_detail.awb_number;
			// this.redelivery_remarks = this.deliveryHistoryDetail.redelivery_detail.remarks;
			this.redelivery_method = this.deliveryHistoryDetail.redelivery_detail.delivery_method;
			this.redelivery_services = this.deliveryHistoryDetail.redelivery_detail.delivery_service;

			const completedReturn = this.deliveryHistoryDetail.shipping_info[this.deliveryHistoryDetail.shipping_info.length - 1];
			this.date = this.convertStringToNgDate(completedReturn.created_date);
			this.time = this.convertStringToNgbTimeStruct(completedReturn.created_date);
			this.receiver_name = completedReturn.receiver_name;
			this.delivery_status = completedReturn.delivery_status;
			this.remark = completedReturn.remarks;

			this.isRedeliveryCompleted = true;
		}


		if(this.deliveryHistoryDetail.status == "CANCEL" && this.deliveryHistoryDetail.return_detail) {
			this.isReorder = true;
			this.returnType = "reorder";

			this.reorder_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
			this.reorder_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
			this.reorder_courier = this.deliveryHistoryDetail.return_detail.courier;
			this.reorder_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
			// this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
		}

		
		// STEP_3_GET_MEMBER_DETAIL_AND_SETUP_CURRENT_PERMISSION: {
		// 	/** I need to get Member detail for getting their permission status */


		// 	if (this.currentPermission == 'admin') {
		// 		this.setShippingStatusList();
		// 	} else if (this.currentPermission == 'merchant') {
		// 		this.setShippingStatus();
		// 	} else {
		// 		console.log('error');
		// 	}

		// 	if (this.currentPermission == 'admin') {
		// 		this.deliveryHistoryDetail.order_list.forEach((element, index) => {
		// 			if (element.shipping_info == null) {
		// 				// let no_history = "No History"
		// 				this.info.push('No History');
		// 			} else {
		// 				let array_info = element.shipping_info.length - 1;
		// 				console.log('array', element.shipping_info);
		// 				this.info.push(element.shipping_info[array_info].title);
		// 			}
		// 		});
		// 	}
		// }

		// console.log("test", this.currentPermission)

		// try {
			// this.deliveryHistoryDetailToLowerCase = this.deliveryHistoryDetail.status.toLowerCase();
			if (typeof this.deliveryHistoryDetail.billings != 'undefined') {
				let billings = this.deliveryHistoryDetail.billings;
				let additionalBillings = [];
				if (billings.discount) { 
					additionalBillings.push({
						product_name: billings.discount.product_name,
						value: billings.discount.value,
						discount: true
					});
				}
				if (this.deliveryHistoryDetail.status == 'CHECKOUT') {
					// if (this.deliveryHistoryDetail.payment_detail.payment_via == 'manual banking') {
					if (billings.unique_amount) {
						billings.unique_amount;
					}
					// }
				} else {
					delete billings.unique_amount;
				}
				for (let prop in billings) {
					// console.log("PROP", prop);
					if (prop == 'shipping_fee' || prop == 'unique_amount') {
						additionalBillings.push({
							product_name: prop,
							value: billings[prop],
							discount: false
						});
					}
				}
				console.log(this.deliveryHistoryDetail.billings);
				this.deliveryHistoryDetail.additionalBillings = additionalBillings;
			} else if (typeof this.deliveryHistoryDetail.billings == 'undefined') {
				// let additionalBillings = [];
				// let shipping_services

				// additionalBillings.push(
				//   {
				//     product_name: "Shipping Fee",
				//     value: this.deliveryHistoryDetail.shipping_fee,
				//     discount: false
				//   }
				// )
				// let shipping_service = {
				// 	shipping: {
				// 		service: this.deliveryHistoryDetail.delivery_detail.delivery_service
				// 	}
				// };

				// let sum_total = {
				// 	sum_total: this.deliveryHistoryDetail.total_price
				// };

				// let detail_courier = this.deliveryHistoryDetail.courier
				// 	? this.deliveryHistoryDetail.courier.courier
				// 	: this.deliveryHistoryDetail.shipping_service;

				// let courier = {
				// 	shipping_services: detail_courier
				// };

				// let courier_detail = {
				//   courier : {
				//     courier : "",
				//     awb_number : "",
				//     courier_code : "",
				//   }
				// }
				ASSIGN_OBJECT_TO_ORDER_HISTORY_DETAIL: {
					// Object.assign(this.deliveryHistoryDetail, courier);
					// Object.assign(this.deliveryHistoryDetail, courier_detail)
					// Object.assign(this.deliveryHistoryDetail, sum_total);
					// Object.assign(this.deliveryHistoryDetail, shipping_service);
					// this.deliveryHistoryDetail.additionalBillings = additionalBillings;
				}

				// console.log('Detail', this.deliveryHistoryDetail);
			}

			// if (this.deliveryHistoryDetail.merchant) {
			// 	this.merchantMode = true;
			// }
			// if (!this.deliveryHistoryDetail.shipping.service) {
			// 	this.deliveryHistoryDetail.shipping.service = 'no-services';
			// 	// console.log("Result", this.deliveryHistoryDetail.shipping)
			// }
			// if (this.deliveryHistoryDetail.status) {
			// 	this.deliveryHistoryDetail.status = this.deliveryHistoryDetail.status.trim().toUpperCase();
			// }

			// if (this.deliveryHistoryDetail.products) {
			// 	// console.log("disini", this.deliveryHistoryDetail.products)
			// 	this.deliveryHistoryDetail.products.forEach((element, index) => {});
			// }

			if (this.deliveryHistoryDetail.shipping_info) {
				console.log('checking again', this.deliveryHistoryDetail.shipping_info);
				// console.log('this.deliveryHistoryDetail.shipping_info', this.deliveryHistoryDetail.shipping_info)
				this.shippingStatusList.forEach((element, index) => {
					let check = this.isShippingValueExists(element.value);
					console.log('check', check);
					if (check) {
						this.shippingStatusList[index].checked = check[0];
						this.shippingStatusList[index].created_date = check[1];
					} else {
						this.shippingStatusList[index].checked = false;
					}
				});
			}
			// if (this.deliveryHistoryDetail.courier_list.supported){
				
			// 	this.courier_list = this.deliveryHistoryDetail.courier_list.supported.concat(this.deliveryHistoryDetail.courier_list.unsupported);
			// 	this.listService()
			// }

			// this.deliveryHistoryDetail.previous_status = this.deliveryHistoryDetail.status;
			// this.transactionStatus.forEach((element, index) => {
			// 	if (element.value == this.deliveryHistoryDetail.transaction_status) {
			// 		this.transactionStatus[index].selected = 1;
			// 	}
			// });
		// } catch (e) {
		// 	this.errorLabel = (<Error>e).message; //conversion to Error type
		// }
	}

	isArray(obj : any ) {
		return Array.isArray(obj)
	 }
	
	openScrollableContent(longContent) {
		this.modalService.open(longContent, { centered: true });
	}

	replaceVarian(value) {
		return JSON.stringify(value).replace('{','').replace('}','').replace(/[',]+/g, ', ').replace(/['"]+/g, '')
	}

	async updateComplete() {
		try {
			this.isChangeLoading = true;
			// let delivered_date = this.convertDateToString(this.date, this.time);

			// let payloadData = {
			// 	"order_id":this.orderDetail.order_id,
			// 	"delivered_date":delivered_date,
			// 	"receiver_name":this.receiver_name,
			// 	"delivery_status":this.delivery_status,
			// 	"remarks":this.remark
			// }

			let payloadData = {
				"order_id":[
					this.orderDetail.order_id
				]
			}

			const result = await this.orderhistoryService.updateStatusOrderCompleted(payloadData,this);

			if(result.status == "success") {
				// this.deliveryHistoryDetail = await this.orderhistoryService.getOrderDetail(payloadData.order_id);
				await this.firstLoad();
			}
			
			this.isChangeLoading = false;
			this.completeButtonConfirmation = false;

		} catch (error) {
			console.warn("error???", error);
			this.isChangeLoading = false;
			this.completeButtonConfirmation = false;

			// this.errorLabel = ((<Error>error).message);
			// Swal.fire({
			// 	title:"Error",
			// 	text: this.errorLabel,
			// 	icon: 'error',
			// 	confirmButtonText: 'Ok',
			// });
		}
	}

	convertDateToString(date: NgbDate, time: NgbTimeStruct) {
		return date.year.toString().padStart(2, "0") + '-' + date.month.toString().padStart(2, "0") + '-' + date.day.toString().padStart(2, "0") + " " + time.hour.toString().padStart(2, "0") + ':' + time.minute.toString().padStart(2, "0") + ':' + time.second.toString().padStart(2, "0")
	}

	convertDateOnlyToString(date: NgbDate) {
		return date.year.toString().padStart(2, "0") + '-' + date.month.toString().padStart(2, "0") + '-' + date.day.toString().padStart(2, "0")
	}

	convertStringToNgDate(value: String) {
		const _value = value.split(" ")[0].split("-");

		let result: NgbDate = new NgbDate(0,0,0);
		result.year = parseInt(_value[0]);
		result.month = parseInt(_value[1]);
		result.day = parseInt(_value[2]);

		return result;
	}

	convertStringToNgbTimeStruct(value: String) {
		const _value = value.split(" ")[1].split(":");

		let result: NgbTimeStruct = { hour: parseInt(_value[0]), minute: parseInt(_value[1]), second: parseInt(_value[1]) }
		return result;
	}

	
	backToTable() {
		this.back[1](this.back[0]);
	}

	async backHere(obj){
		obj.shippingDetail = false;
		// obj.detail = await obj.orderhistoryService.getShippingDetail(obj.order_id);
		// obj.orderDetail = obj.detail.values;
		// console.log("obj.orderDetail",obj.orderDetail);
		obj.firstLoad();
	  }

	async cancelOrder(){
		if(!this.detail.order_id) return;
		Swal.fire({
			title: 'Confirmation',
			text: 'Apakah anda yakin ingin membatalkan order : '+this.detail.order_id,
			icon: 'error',
			confirmButtonText: 'Ok',
			cancelButtonText:"cancel",
			showCancelButton:true
		  }).then(async (result) => {
			if(result.isConfirmed){
				const payload = {
					order_id:this.detail.order_id
				}
				try {
					const result = await this.orderhistoryService.cancelOrder(payload);
					Swal.fire({
						title:"Cancel",
						text: 'Order : '+this.detail.order_id+' telah dibatalkan',
						icon: 'error',
						confirmButtonText: 'Ok',
						showCancelButton:false,
					}).then((result) => {
						if(result.isConfirmed){
							this.backToTable();
					}});
				} catch (e) {
					this.errorLabel = ((<Error>e).message);
				}
			}
		  });
	}

	async processOrder(){
		if(!this.detail.order_id) return;
		Swal.fire({
			title: 'Confirmation',
			text: 'Apakah anda yakin ingin memproses order : '+this.detail.order_id,
			icon: 'success',
			confirmButtonText: 'process',
			cancelButtonText:"cancel",
			showCancelButton:true
		  }).then(async (result) => {
			if(result.isConfirmed){
				const payload = {
					order_id:this.detail.order_id
				}
				try {
					const result = await this.orderhistoryService.processOrder(payload);
					Swal.fire({
						title:"Success",
						text: 'Order : '+this.detail.order_id+ ' telah disetujui',
						icon: 'success',
						confirmButtonText: 'Ok',
						showCancelButton:false,
					}).then((result) => {
						if(result.isConfirmed){
							this.backToTable();
					}});
				} catch (e) {
					this.errorLabel = ((<Error>e).message);
				}
			}
		});
	}

	calculateSumPrice(index) {
		let sum_price =
			this.deliveryHistoryDetail.products[index].fixed_price * this.deliveryHistoryDetail.products[index].quantity;

		return sum_price;
	}

	// async getCourier() {
	// 	let couriers = await this.orderhistoryService.getCourier();
	// 	this.courier_list = couriers.result;
	// 	console.log('result', this.courier_list);

	// 	this.courier_list.forEach((element, index) => {
	// 		let values = {
	// 			value: element.courier
	// 		};
	// 		Object.assign(this.courier_list[index], values);
	// 	});
	// 	console.log(this.courier_list);
	// }

	// backToTable() {
	// 	if (this.back[0][this.back[1]] && !isFunction(this.back[0])) {
	// 		this.back[0][this.back[1]];
	// 	} else if (isFunction(this.back[0])) {
	// 		this.back[0]();
	// 	} else {
	// 		window.history.back();
	// 	}
	// }

	backToList() {
		console.log(true);

		if (this.openMoreDetail) {
			this.openMoreDetail = false;
		}
	}

	// setShippingStatusList() {
	// 	// console.log("here", this.deliveryHistoryDetail.shipping_info);
	// 	this.deliveryHistoryDetail.order_list.forEach((element, index) => {
	// 		if (this.shippingStatusList[index].id == 'on-process') {
	// 			this.shippingStatusList[index].checked = true;
	// 		} else if (this.shippingStatusList[index].id == 'warehouse-packaging') {
	// 			this.shippingStatusList[index].checked = true;
	// 		} else if (this.shippingStatusList[index].id == 'on-delivery') {
	// 			this.shippingStatusList[index].checked = true;
	// 		} else if (this.shippingStatusList[index].id == 'delivered') {
	// 			this.shippingStatusList[index].checked = true;
	// 		}
	// 	});
	// }

	setShippingStatus() {
		// console.log("here", this.deliveryHistoryDetail.shipping_info);
		this.deliveryHistoryDetail.shipping_info.forEach((element, index) => {
			if (this.shippingStatusList[index].id == 'on-process') {
				this.shippingStatusList[index].checked = true;
			} else if (this.shippingStatusList[index].id == 'warehouse-packaging') {
				this.shippingStatusList[index].checked = true;
			} else if (this.shippingStatusList[index].id == 'on-delivery') {
				this.shippingStatusList[index].checked = true;
			} else if (this.shippingStatusList[index].id == 'delivered') {
				this.shippingStatusList[index].checked = true;
			}
		});
	}

	changeshippingStatusList(index) {
		//  
		for (let n = 0; n <= index; n++) {
			if (this.shippingStatusList[n].id == 'on-delivery') {
				if(this.deliveryHistoryDetail.last_shipping_info == 'on_processing' || this.deliveryHistoryDetail.last_shipping_info == 'on_delivery'){
					if(this.deliveryHistoryDetail.last_shipping_info == 'on_processing') this.changeAWB = true;
					this.shippingStatusList[n].checked = true;
				}

				// if (this.deliveryHistoryDetail.shipping_services) {
				// 	this.shippingStatusList[n].checked = true;
				// }	
				// if (!this.deliveryHistoryDetail.shipping){
				// 	this.shippingStatusList[n].checked = true;
				// }
			} else if (this.shippingStatusList[n].id == 'delivered') {
				if (this.deliveryHistoryDetail.delivery_detail.awb_number) {
					this.shippingStatusList[n].checked = true;
				}
			}
			//  else if(this.shippingStatusList[n].id == 'on_processing'){

			// 	// else if (this.shippingStatusList[n].id == 'warehouse-packaging') {
			// 	//   let warehouse_packaging = (this.deliveryHistoryDetail.billings != undefined) ? this.deliveryHistoryDetail.shipping_services : this.deliveryHistoryDetail.courier.courier
			// 	//   if (warehouse_packaging) {
			// 	//     this.shippingStatusList[n].checked = true;
			// 	//   }
			// 	// }
			// 	this.shippingStatusList[n].checked = true;
			// }

			// if (this.shippingStatusList[n].id == 'warehouse-packaging') {
			// 	this.packaging = true;
			// 	// this.shippingStatusList[n].checked = true;
			// } else {
			// 	this.packaging = false;
			// }
		}

		for (let n = index + 1; n < this.shippingStatusList.length; n++) {
			this.shippingStatusList[n].checked = false;
		}
	}

	hoverShippingStatusList(index, objectHover) {
		for (let n = 0; n <= index; n++) {
			this.shippingStatusList[n].hovered = true;
		}

		for (let n = index + 1; n < this.shippingStatusList.length; n++) {
			this.shippingStatusList[n].hovered = false;
		}
	}

	convertShippingStatusToShippingInfo() {
		let newData = [];
		this.shippingStatusList.forEach((element, index) => {
			if (element.checked) {
				newData.push({ value: element.values });
			}
		});
		return newData;
	}
	async saveThis() {
		if (this.currentPermission == 'admin' && this.isClicked2) {
			this.deliveryHistoryDetail.status = this.tempstatus;
			this.cancelMerchant();
		} else {
			try {
				this.loading = !this.loading;
				this.deliveryHistoryDetail.status = this.tempstatus;
				if (this.deliveryHistoryDetail.billings != undefined) {
					let frm = JSON.parse(JSON.stringify(this.deliveryHistoryDetail));
					frm.shipping_info = this.convertShippingStatusToShippingInfo();
					// this.deliveryHistoryDetail.shipping_status = this.valu
					// if (frm.status != "CANCEL") {
					delete frm.buyer_detail;
					delete frm.payment_expire_date;
					delete frm.products;
					delete frm.billings;
					delete frm.user_id;
					console.log('frm', frm);
					await this.orderhistoryService.updateOrderHistoryAllHistory(frm);
					
					// setTimeout(function(){window.location.href}, 3000);
					// }
				} else {
					let all_shipping_label = this.convertShippingStatusToShippingInfo();
					let shipping_label = all_shipping_label[all_shipping_label.length - 1].value;
					let awb = this.deliveryHistoryDetail.awb_receipt
						? this.deliveryHistoryDetail.awb_receipt
						: this.deliveryHistoryDetail.courier ? this.deliveryHistoryDetail.courier.awb_number : null;
					let courier = this.deliveryHistoryDetail.shipping_services
						? this.deliveryHistoryDetail.shipping_services
						: 'no-services';
					console.log('courier', this.deliveryHistoryDetail.billings, this.deliveryHistoryDetail.courier);
					if (
						this.deliveryHistoryDetail.billings == undefined &&
						this.deliveryHistoryDetail.courier != undefined &&
						this.deliveryHistoryDetail.courier.courier_code
					) {
						courier = this.deliveryHistoryDetail.courier.courier_code;
					}
					let payload = {
						booking_id: this.deliveryHistoryDetail.booking_id,
						shipping_label: shipping_label,
						courier_code: courier,
						awb_number: awb,
						delivery_method: this.choose_service,
						status: this.deliveryHistoryDetail.status
					};
					console.log('payload', payload);
					if( this.othercour){
						let newpayload = {
							booking_id: this.deliveryHistoryDetail.booking_id,
							courier: this.otherCourValue,
							courier_support :0,
							awb_number: awb,
							shipping_label: shipping_label
						}
						await this.orderhistoryService.updateOrderHistoryAllHistoryMerchant(newpayload);
					} else {
						if(shipping_label == 'on_packaging' && payload.courier_code == 'LO-CR-GR'){ // special case for INSTANT courier , add courier_support =0 and courier name 
							let temp_payload = {
								courier_support : 0,
								courier: 'GRAB'
							}
							Object.assign(payload,temp_payload)
						}
						await this.orderhistoryService.updateOrderHistoryAllHistoryMerchant(payload);
						console.log('choose',this.choose_service)
						if ( this.choose_service == 'PICKUP'){
							Swal.fire(
								{
									title: 'Pemberitahuan Pickup',
									html : 'Barang anda akan segera di pickup oleh kurir minimal<br/> 2 jam setelah anda melakukan permintaan pickup <br/> <a style="color:red">Baca Term of Service pickup Courier</a>',
									imageUrl: 'assets/images/alarm.png',
									imageWidth: 115,
									imageHeight: 129
								}
							);
						}
						
					}
					

					console.log(this.convertShippingStatusToShippingInfo());
					console.log(this.deliveryHistoryDetail);
					console.log(payload);
				}
				// else{
				//   await this.orderhistoryService.updateOrderHistoryAllHistory(frm.status);
				// }
				this.loading = !this.loading;
				this.done = true;
				this.change = false;
				this.editThis = false;
				alert('Edit submitted');
				
				
				this.firstLoad();

				// window.location.reload();
			} catch (e) {
				this.errorLabel = (<Error>e).message; //conversion to Error type
			}
		}
	}

	receipt() {
		this.urlReceipt = this.deliveryHistoryDetail.uploaded_receipt;
		window.open(this.urlReceipt);
	}

	openLabel() {
		this.router.navigate([ 'administrator/orderhistoryallhistoryadmin/receipt' ], {
			queryParams: { id: this.deliveryHistoryDetail._id }
		});
	}

	openNote() {
		this.openNotes = true;
		// this.deliveryHistoryDetail.status = 'CANCEL'

		// if(this.notesCategory[0] == true){
		// 	console.log(this.notesCategory[0].label);
		// } else if(this.notesCategory[1]){
		// 	value: true;
		// } else if(this.notesCategory[2]){
		// 	value: true;
		// }
	}

	onAWBchange(){
		console.log(' here',this.deliveryHistoryDetail.courier.awb_number)
		if(this.deliveryHistoryDetail.courier.awb_number){
			if (this.deliveryHistoryDetail.courier.awb_number.length == 0){
				this.awb_check = false;
			}
			if (this.deliveryHistoryDetail.courier.awb_number.length > 0){
				this.awb_check = true;
			}
		}
	}
	openNote2() {
		this.openNotes2 = true;
		// this.deliveryHistoryDetail.status = 'CANCEL'
	}

	clickCategory(index) {
		this.notesCategory[index].value = true;
		this.notesCategory.forEach((element, i) => {
			if ((this.notesCategory[index].value = true)) {
				this.notesCategory[i].value = false;
				this.notesCategory[index].value = true;
			}
		});
		this.notes = this.notesCategory[index].label;
		console.log(this.notesCategory);
	}

	noteClose() {
		this.openNotes = this.openNotes2 = false;
	}
	async hold() {
		let form = {
			value: this.holdvalue,
			description: this.notes,
			remark: 'hold transaction process'
		};
		try {
			const holdMerchant = await this.orderhistoryService.holdMerchant(this.detailPage.booking_id, form);

			this.closeNote2();
			this.firstLoad();
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}

	closeNote() {
		this.openNotes = this.openNotes2 = false;
		this.firstLoad();
		Swal.fire('Cancelled', 'Your order has been Cancelled', 'success');
	}
	closeNote2() {
		this.openNotes = this.openNotes2 = false;
		this.firstLoad();
		Swal.fire('HOLD', 'The order has been hold', 'success');
	}

	async cancelOrders() {
		let payload;

		if (this.currentPermission == 'merchant') {
			payload = {
				booking_id: this.deliveryHistoryDetail.booking_id,
				note: this.notes
			};
		} else {
			payload = {
				booking_id: this.detailPage.booking_id,
				note: this.notes
			};
			console.log('payload', this.deliveryHistoryDetail);
		}
		let cancel = await this.orderhistoryService.cancelOrder(payload);

		this.closeNote();
	}

	async cancelMerchant() {
		let payload = {
			status: this.tempstatus
		};

		console.log('payload', payload.status);
		try {
			let cancel = await this.orderhistoryService.updateOrderHistoryAllHistoryCancel(
				this.deliveryHistoryDetail._id,
				payload
			);
			if (cancel) {
				this.loading = !this.loading;
				this.done = true;
				this.change = false;
				this.editThis = false;
				alert('Edit submitted');
				this.firstLoad();
			}
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}

	edit() {
		this.editThis = true;
		console.log('edit true');
	}

	doneEdit() {
		this.editThis = false;
		console.log('done');
	}
	changestatus() {
		this.change = true;
	}
	onpaidclick() {
		//   this.deliveryHistoryDetail.status = "PAID";
		this.tempstatus = 'PAID';
		this.isClicked = true;
		this.isClicked2 = false;
		console.log(this.deliveryHistoryDetail);
	}
	oncancelclick() {
		// this.deliveryHistoryDetail.status = "CANCEL";
		this.tempstatus = 'CANCEL';
		this.isClicked2 = true;
		this.isClicked = false;
		console.log(this.deliveryHistoryDetail);
	}

	async openDetail(bookingID,order) {
		this.openMoreDetail = true;

		let result = await this.orderhistoryService.getDetailOrderhistory(bookingID);
		this.moreOrderDetail = order;
		console.log('moreorderdetail', this.moreOrderDetail)
		this.detailPage = result.result[0];

		console.log('detail', this.detailPage);

		// this.deliveryHistoryDetail.order_list.forEach((element, index) =>{

		// })
		if(this.detailPage.shipping_info){
			this.detailPage.shipping_info.forEach((element, index) => {
				if (element == null) {
					console.log('part 1', element);
					this.infoDetail.push('No Service');
				} else {
					console.log('part 2', element);
					let array = this.detailPage.shipping_info.length - 1;
					console.log('test', array);
					this.infoDetail = this.detailPage.shipping_info[array].title;
					this.infoDetailLabel = this.detailPage.shipping_info[array].label;
					console.log('this2', this.infoDetail);
				}
			});
		}
		

		// if(this.detailPage.shipping_info){
		// 	this.detailPage.shipping_info.forEach((element, index) => {
		// 		console.log(element.shipping_info)
		// 	})
		// }

		if (
			this.openMoreDetail &&
			this.deliveryHistoryDetail.status == 'PAID' &&
			this.deliveryHistoryDetail.status != 'COMPLETED' &&
			(this.infoDetailLabel == 'on_delivery' || this.infoDetailLabel == 'delivered')
		) {
			console.log('true bro');
		} else {
			console.log('salah cok');
		}
		console.log('lkawdj', this.deliveryHistoryDetail.status, this.infoDetailLabel); 
		console.log('booking id', bookingID);
	}
	other_cour(){
		console.log('dipilih',this.deliveryHistoryDetail.shipping_services)
		if(this.deliveryHistoryDetail.shipping_services == 'other'){
			console.log("othernih")
			this.othercour = true;
			this.supported = false;
		}else {
			this.supported = true;
			this.othercour = false;
			
		}
		console.log("courier_list",this.courier_list,"method",this.choose_service)
	}

	listService(){
		this.courier_list.forEach((entry) => {
			console.log(entry)
			console.log(entry.delivery_method)
			if(this.deliveryHistoryDetail.shipping_services == entry.courier_code || this.deliveryHistoryDetail.shipping_services == entry.courier){ 
				this.method_delivery = entry.delivery_method}
		})
	}

	checkStatus() {
		// console.log("Status", this.deliveryHistoryDetail.status)
		this.statusValue = this.deliveryHistoryDetail.status;
		this.statusLabel = this.deliveryHistoryDetail.status;
		if (this.statusValue != 'PAID' && this.statusValue != 'CANCEL') {
			this.transactionStatus.push({ label: this.statusLabel, value: this.statusValue });
		}
	}

	isShippingValueExists(value) {
		for (let element of this.deliveryHistoryDetail.shipping_info) {
			console.log('element', element.label);
			if (element.label.trim().toLowerCase() == value.trim().toLowerCase()) {
				return [ true, element.created_date ];
			}
		}
		return false;
	}




	async updateAWB(){
		if(this.deliveryHistoryDetail.last_shipping_info == 'on_delivery' || this.deliveryHistoryDetail.last_shipping_info == 'on_processing'){
			if( this.courier && 
				this.delivery_service &&
				this.delivery_method){
					let payload : any = {
						order_id: this.deliveryHistoryDetail.order_id,
						courier:this.courier,
						delivery_method:this.delivery_method,
						delivery_service:this.delivery_service,
						remarks: this.delivery_remarks
					}

					if(this.courier == 'others' ){
						if(!this.courier_name){
							Swal.fire("Oops", "mohon masukan nama kurir terlebih dahulu", 'warning');
							return;
						}
						// if(!this.awb_number){
						// 	Swal.fire("Oops", "mohon masukan nomor resi terlebih dahulu", 'warning');
						// 	return;
						// }
						payload.courier_name = this.courier_name;
						payload.awb_number = this.awb_number ? this.awb_number : '';
					}

					try {
						this.isChangeAWBLoading = true;
						await this.orderhistoryService.updateStatusOrder(payload, 'delivery');
						await this.updateDetailOrder();
						this.isChangeAWBLoading = false;
						this.firstLoad();

					} catch (e) {
						this.isChangeAWBLoading = false;
						this.errorLabel = (<Error>e).message; //conversion to Error type	
					}
				}else {
					Swal.fire("Oops", "Service dan metode pengiriman harus di input terlebih dahulu", 'warning');
				}
		}
	}

	async updateDetailOrder(){
		try {
			this.deliveryHistoryDetail = await this.orderhistoryService.getOrderDetail(this.deliveryHistoryDetail.order_id);
			// let filter = {
			// 	search: {
			// 	  order_id: this.deliveryHistoryDetail.order_id
			// 	},
			// 	limit_per_page: 2,
			// 	download: false
			//   }

			//   let result = await this.orderhistoryService.searchOrderHistory(filter);
			//   this.deliveryHistoryDetail = result.values[0];
			//   console.log("result update detail", this.deliveryHistoryDetail);
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}

	async updateStatusCompleted(){
		if(this.deliveryHistoryDetail.last_shipping_info == 'on_delivery'){
			try {
				const payload = {
					order_id: this.deliveryHistoryDetail.order_id,
				}
				await this.orderhistoryService.updateStatusOrder(payload, 'complete');
				await this.updateDetailOrder();
				this.firstLoad();
			} catch (e) {
				this.errorLabel = (<Error>e).message; //conversion to Error type
			}
		}
	}



	async processRedeliver(){
		try {
			this.isChangeRedeliverLoading = true;
			const payload = {
				"return_type":"REDELIVER",
				"order_id":this.detail.order_id,
				"courier":this.return_courier,
				"return_date":this.convertDateOnlyToString(this.return_date),
				"remarks":this.return_remarks,
				"awb_number":this.return_awb_number,
			};
			await this.orderhistoryService.processRedeliver(payload);
			await this.updateDetailOrder();
			this.isChangeRedeliverLoading = false;
			this.firstLoad();
		} catch (e) {
			this.isChangeRedeliverLoading = false;
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}

	async processOrderRedelivery(){
		try {
			this.isChangeProcessRedeliveryLoading = true;
			const payload = {
				"order_id":this.detail.order_id,
				"courier":this.redelivery_courier,
				"delivery_method":this.redelivery_method,
				"delivery_service":this.redelivery_services,
				"delivery_date":this.convertDateOnlyToString(this.redelivery_date),
				"remarks":this.redelivery_remarks,
				"awb_number":this.redelivery_remarks,
			}

			await this.orderhistoryService.processOrderRedelivery(payload);
			await this.updateDetailOrder();
			this.isChangeProcessRedeliveryLoading = false;
			this.firstLoad();
		} catch (e) {
			this.isChangeProcessRedeliveryLoading = false;
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}



	async processReorder(){
		try {
			this.isChangeReorderLoading = true;
			const payload = {
				"return_type":"REORDER",
				"order_id":this.detail.order_id,
				"courier":this.reorder_courier,
				"return_date":this.convertDateOnlyToString(this.reorder_date),
				"remarks":this.reorder_remarks,
				"awb_number":this.reorder_awb_number,
			};
			await this.orderhistoryService.processRedeliver(payload);
			await this.updateDetailOrder();
			this.isChangeReorderLoading = false;
			this.firstLoad();
		} catch (e) {
			this.isChangeReorderLoading = false;
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}

	goShippingDetail(value) {
		this.shippingDetail = value;
	}


	// {
	// 	"order_id":"1234567890",
	// 	"courier":"JNE",
	// 	"delivery_method":"DROP"
	// 	"delivery_services":"Regular"
	// 	"delivery_date":"2021-08-30",
	// 	"remarks":"barang rusak",
	// 	"awb_number":"000000000"
	// }
}
