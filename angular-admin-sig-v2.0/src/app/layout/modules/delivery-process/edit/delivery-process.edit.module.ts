import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { BrowserModule } from '@angular/platform-browser';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../../bs-component/bs-component.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeliveryProcessEditComponent } from './delivery-process.edit.component';
import { TranslateModule } from '@ngx-translate/core';
// import { ReceiptComponent } from '../receipt/receipt.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';

@NgModule({
  imports:      [ 
    CommonModule,
    FormsModule, 
    TranslateModule,
    ReactiveFormsModule,
    NgbModule,
    BsComponentModule,
    NgxBarcodeModule,
    MatButtonModule,
    MatRadioModule,
    // ReceiptComponent
  ],
  declarations: [ DeliveryProcessEditComponent ],
  exports:[ DeliveryProcessEditComponent ],
  bootstrap:    [ DeliveryProcessEditComponent ],
})
export class OrderHistoriesEditModule { }