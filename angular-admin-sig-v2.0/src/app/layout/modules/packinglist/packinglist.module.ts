import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistoryallhistoryRoutingModule } from './packinglist-routing.module';
import { OrderhistoryallhistoryComponent } from './packinglist.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent } from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { PackingListReportDetailComponent } from './detail/packing-list.detail.component';
import { PackingListGenerateComponent } from './generate/packing-list.generate.component';
// import { OrderhistoryallhistoryEditComponent } from './edit/orderhistoryallhistory.edit.component';
import { OrderhistoryallhistoryEditModule } from '../orderhistoryallhistory/edit/orderhistoryallhistory.edit.module'
import { NgxBarcodeModule } from 'ngx-barcode';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';



@NgModule({
  imports: [
    CommonModule,
    OrderhistoryallhistoryRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    NgxBarcodeModule,
    MatButtonModule,
    MatRadioModule,
    OrderhistoryallhistoryEditModule
  ],
  declarations: [
    OrderhistoryallhistoryComponent,
    PackingListReportDetailComponent,
    PackingListGenerateComponent
  ],
  exports:[
    OrderhistoryallhistoryComponent,
  ]
})
export class PackingListModule { }
