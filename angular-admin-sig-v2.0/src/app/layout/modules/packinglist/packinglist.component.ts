import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
// import { NumberFormatStyle } from '@angular/common';
import { FormOptions, TableFormat } from '../../../object-interface/common.object';
import Swal from 'sweetalert2';
// import { Router } from '@angular/router';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-packinglist',
  templateUrl: './packinglist.component.html',
  styleUrls: ['./packinglist.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistoryallhistoryComponent implements OnInit {
  @Input() public back;


  Orderhistoryallhistory: any = [];

  errorMessage  : any = false;

  tableFormat: TableFormat = {

    title: 'Packing List',

    label_headers: [
      
      { label: 'Packing Number', visible: true, type: 'string', data_row_name: 'packing_no' },
      { label: 'Courier Code', visible: true, type: 'date', data_row_name: 'courier_code' },
      { label: 'Courier Name', visible: true, type: 'date', data_row_name: 'courier_name' },
      { label: 'Delivery Service', visible: true, type: 'string', data_row_name: 'delivery_service' },
      { label: 'Delivery Method', visible: true, type: 'string', data_row_name: 'delivery_method' },
      { label: 'Total Order', visible: true, type: 'reference_no_length', data_row_name: 'reference_no' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],

    row_primary_key: 'order_id',
    formOptions: {
      row_id: 'order_id',
      this: this,
      result_var_name: 'Orderhistoryallhistory',
      detail_function: [this, 'callDetail'],
    },
    show_checkbox_options: true
  };

  tableFormat2: TableFormat = {

    title: 'Packing List',

    label_headers: [
      
      { label: 'Packing Number', visible: true, type: 'string', data_row_name: 'packing_no' },
      { label: 'Courier Code', visible: true, type: 'date', data_row_name: 'courier_code' },
      { label: 'Courier Name', visible: true, type: 'date', data_row_name: 'courier_name' },
      { label: 'Delivery Service', visible: true, type: 'string', data_row_name: 'delivery_service' },
      { label: 'Delivery Method', visible: true, type: 'string', data_row_name: 'delivery_method' },
      { label: 'Total Order', visible: true, type: 'reference_no_length', data_row_name: 'reference_no' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
      { label: 'Delivery Date', visible: true, type: 'date', data_row_name: 'delivery_date' },
    ],

    row_primary_key: 'order_id',
    formOptions: {
      row_id: 'order_id',
      this: this,
      result_var_name: 'Orderhistoryallhistory',
      detail_function: [this, 'callDetail'],
    },
    show_checkbox_options: true
  };

  form_input: any = {};
  errorLabel: any = false;
  totalPage: 0;
  swaper: any = true;

  packingListDetail: any = false;
  service: any;
  programType:any = "";
  mci_project: any = false;
  sig_kontraktual: any = false;

  public contentList : any = (content as any).default;

  constructor(
    public orderhistoryService: OrderhistoryService,
    ) { }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
          if(element.appLabel == program) {
              if(element.type == "reguler") {
                  _this.mci_project = true;
              } else if(element.type == "custom_kontraktual") {
                _this.programType = "custom_kontraktual";
                _this.sig_kontraktual = true;
              } else {
                  _this.mci_project = false;
              }
          }
      });

      let result: any;
      if(this.swaper == true){
        result = await this.orderhistoryService.getPackingListReport('ACTIVE');
      }else{
        result= await this.orderhistoryService.getPackingListReport('PROCESSED');
      }

      this.service = this.orderhistoryService;
      console.warn('rsult', result);
      this.totalPage = result.total_page;
      
      this.Orderhistoryallhistory = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  public async callDetail(data, rowData) {
    this.packingListDetail = this.Orderhistoryallhistory.find(packing => packing.packing_no == rowData.packing_no);
  }

  public async backToHere(obj) {
    obj.packingListDetail = false;
    obj.firstLoad();
  }

  swapClick(bool) {
    this.swaper = bool;
    this.firstLoad();
  }

  public async processSelectedOrders(obj, orders){
    if(orders.length > 0){
      Swal.fire({
        title: 'Confirmation',
        text: 'Apakah anda yakin ingin memproses semua order yang dipilih',
        icon: 'success',
        confirmButtonText: 'process',
        cancelButtonText:"cancel",
        showCancelButton:true
        }).then(async (result) => {
        if(result.isConfirmed){
          await orders.forEach(async(order_id) => {
            const payload = {
              order_id
            }
            try {
              await obj.orderhistoryService.processOrder(payload);
            } catch (e) {
            }
            Swal.fire('Success', 'Semua order terpilih telah disetujui', 'success');
          });

        }
      });
    }
  }

}
