import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EvoucherStockReportRoutingModule } from './evoucher-stock-report-routing.module';
import { FormBuilderTableModule } from '../../../../component-libs/form-builder-table/form-builder-table.module';
import { PageHeaderModule } from './../../../../shared';
import { EvoucherStockReportComponent } from './evoucher-stock-report.component';
import { EVStockDetailComponent } from './detail/evoucher-stock-report.detail.component';
import { EvoucherStockAddComponent } from './add/evoucher-stock-report.add.component';

@NgModule({
  imports: [
    CommonModule,
    EvoucherStockReportRoutingModule,
    FormBuilderTableModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    EvoucherStockReportComponent,
    EVStockDetailComponent,
    EvoucherStockAddComponent,
  ],
  exports:[
    EvoucherStockReportComponent,
    EVStockDetailComponent,
  ]
})
export class EvoucherStockReportModule { }
