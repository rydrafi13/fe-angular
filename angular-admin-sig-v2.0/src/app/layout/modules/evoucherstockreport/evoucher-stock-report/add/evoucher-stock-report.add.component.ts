import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { routerTransition } from '../../../../../router.animations';
import { EVoucherService } from '../../../../../services/e-voucher/e-voucher.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-evoucher-stock-report-add',
  templateUrl: './evoucher-stock-report.add.component.html',
  styleUrls: ['./evoucher-stock-report.add.component.scss'],
  animations: [routerTransition()]
})

export class EvoucherStockAddComponent implements OnInit {
  @Input() public back;
  @Input() public detail: any;

  @ViewChild('addBulk', {static: false}) addBulk;

  public name: string = "";
  Evouchers: any = [];
  service: any;
  type:any;
  Report:any;
  member_status:any;
  activation_status:any;
  
  errorLabel : any = false;
 
  isBulkUpdate : any = false;
  selectedFile  = null;
  cancel = false;
  progressBar = 0;
  errorFile: any = false;
  prodOnUpload: any     = false;
  startUploading: any = false;
  showUploadButton: any = false;
  getPassword: any;
  successUpload: any = false;
  urlDownload: any = "";

  warnIDPelanggan: any = false;
  warnNamaPemilik: any = false;
  warnTelpPemilik: any = false;
  warnWAPemilik: any = false;

  showPasswordPage: any = false;


  constructor(public evoucherService:EVoucherService) {
    
   }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    this.selectedFile = null;
    this.startUploading = false;
  }

  async updateDataBulk(){
    try
    {
        this.startUploading = true;
        this.cancel = false;
        let payload = {
          type : 'application/form-data',
        }
        if(this.selectedFile) {
          const result: any = await this.evoucherService.uploadEvoucherCodeBulk(this.selectedFile,this,payload);
          
          if (result) {
            this.firstLoad();
          }
        }
      } 
      catch (e) {
        this.startUploading = false;
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
  }

  // async formSubmitAddMember(){
  //     let form_add = this.form;
  //     try {
  //       this.service    = this.evoucherService;
  //       const result: any  = await this.evoucherService.uploadEvoucherCodeBulk(form_add);
  //       console.warn("result add member", result)
  
  //       if (result && result.data) {
  //         this.getPassword = result.data.password;
  //       }

  //       this.data.username = this.form.id_pel;
  //       this.data.password = this.getPassword;
  //       Swal.fire({
  //         title: 'Success',
  //         text: 'Pelanggan berhasil ditambahkan',
  //         icon: 'success',
  //         confirmButtonText: 'Ok',
  //       }).then((result) => {
  //         if(result.isConfirmed){
  //           this.showThisPassword();
  //         }
  //       });
  //     } catch (e) {
  //       console.warn("error", e.message)
  //       Swal.fire({
  //         title: 'Failed',
  //         text: e.message,
  //         icon: 'warning',
  //         confirmButtonText: 'Ok',
  //       }).then((result) => {
  //         if(result.isConfirmed){
  //           this.firstLoad();
  //         }
  //       });
  //       this.errorLabel = ((<Error>e).message);//conversion to Error type
  //     }
  // }

  showThisPassword() {
    this.showPasswordPage = !this.showPasswordPage;
    if(this.showPasswordPage == false){
      this.firstLoad();
    }
    // console.log(this.edit );
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    // var reader = new FileReader();
    // reader.readAsDataURL(event.target.files[0]); //
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    
    this.selectedFile = event.target.files[0];
    this.addBulk.nativeElement.value = '';
    
  } 

  cancelThis(){
    this.cancel = !this.cancel;
  }

  actionShowUploadButton() {
    this.showUploadButton = !this.showUploadButton;
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  backTo(){
    window.history.back();
  }

}
