import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesorderRoutingModule } from './salesorder-routing.module';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { SalesorderComponent } from './salesorder.component';


@NgModule({
  imports: [
    CommonModule, 
    SalesorderRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
    declarations: [SalesorderComponent]
})
export class SalesorderModule { }
