import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PointsTransactionService } from '../../../services/points-transaction/points-transaction.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-point-movement-order',
  templateUrl: './point-movement-order.component.html',
  styleUrls: ['./point-movement-order.component.scss']
})
export class PointMovementOrderComponent implements OnInit {

  PointMovementOrder: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Point Movement Based on Order',
                                  label_headers   : [
                                    {label: 'Tanggal Update', visible: true, type: 'date', data_row_name: 'updated_date'},
                                    {label: 'ID Toko', visible: true, type: 'string', data_row_name: 'id_toko'},
                                    {label: 'Nama Toko', visible: true, type: 'string', data_row_name: 'nama_toko'},
                                    {label: 'Alamat Toko', visible: true, type: 'string', data_row_name: 'alamat_toko'},
                                    {label: 'Nama Pemilik', visible: true, type: 'string', data_row_name: 'nama_pemilik'},
                                    {label: 'Quantity', visible: true, type: 'quantity', data_row_name: 'products'},
                                    {label: 'Hadiah Redeem', visible: true, type: 'product_name', data_row_name: 'products'},
                                    {label: 'SKU', visible: true, type: 'product_code', data_row_name: 'products'},
                                    {label: 'Previous Point Balance', visible: true, type: 'string', data_row_name: 'prev_points_balance'},
                                    {label: 'Mutasi Point', visible: true, type: 'string', data_row_name: 'points'},
                                    {label: 'Current Points Balance', visible: true, type: 'string', data_row_name: 'current_points_balance'},
                                    {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                                    {label: 'Group', visible: true, type: 'string', data_row_name: 'group'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'PointMovementOrder',
                                                    detail_function: [this, 'callDetail'],
                                                    pointMovementOrder : true,
                                                  },
                                                  show_checkbox_options: true
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;

  SalesRedemptionDetail: any = false;
  service: any;

  errorMessage: any = false;

  constructor(public PointsTransactionService:PointsTransactionService) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    console.log(' stock movement')
    try{
      var params = {
        search: {based_on:"invoice"},
        current_page:1,
        limit_per_page:50
       }
      this.service    = this.PointsTransactionService;
      let result: any  = await this.PointsTransactionService.getPointMovementBasedOnOrder(params);
      console.warn("result", result)
      this.totalPage = result.result.total_page
      this.PointMovementOrder = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			}
    }
  }
  
  public async callDetail(SalesRedemption_id){
    try{
      // let result: any;
      // this.service    = this.PointsTransactionService;
      // result          = await this.PointsTransactionService.detailPointstransaction(SalesRedemption_id);
      // console.log(result);

      // this.SalesRedemptionDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.SalesRedemptionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}
