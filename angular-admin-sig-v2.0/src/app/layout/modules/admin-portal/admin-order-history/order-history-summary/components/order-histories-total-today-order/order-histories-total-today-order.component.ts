import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-order-histories-total-today-order',
  templateUrl: './order-histories-total-today-order.component.html',
  styleUrls: ['./order-histories-total-today-order.component.scss']
})
export class OrderHistoriesTotalTodayOrderComponent implements OnInit {

  @Input() data;
  
  constructor() { }

  ngOnInit() {
  }

}
