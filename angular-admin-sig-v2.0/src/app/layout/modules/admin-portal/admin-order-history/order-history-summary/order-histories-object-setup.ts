export const optionNoFill = {fill:false , borderWidth: 1}

export const analitycsData:any = {
    monthly : {
      barChartData : [
        { data: [], label: 'All Order' , ...optionNoFill},
      ],
      barChartLabels : []
    },
    daily : {
      barChartData : [
        { ...{data: [], label: 'All Order'} , ...optionNoFill
      },
      ],
      barChartLabels : []
    },
    hourly : {
      barChartData : [
        { data: [], label: 'All Order' , ...optionNoFill},
      ],
      barChartLabels : []
    },
    yearly : {
      barChartData : [
        { data: [], label: 'All Order' , ...optionNoFill},
      ],
      barChartLabels : []
    }
  }

export function generateChartData(dataValues){
    let barChartLabels = [];
    let newData = [];

    // console.log("generate Chart Data", dataValues)

    let allData: any;
    allData = dataValues;

    let total = 0;
    for (let data in allData) {
      barChartLabels.push(data)
      total += parseInt(allData[data]);
      newData.push(allData[data])

    }
    allData = null; //clearing memory

    return [barChartLabels, newData, total]
}

export function calculateTheAverageValue(_value: Object) {
    let howMany: number = Object.keys(_value).length
    let sum = 0;

    Reflect.ownKeys(_value).forEach((key) => {
      sum = sum + _value[key];
    });

    let avg = sum / howMany;
    return avg;
  }
  