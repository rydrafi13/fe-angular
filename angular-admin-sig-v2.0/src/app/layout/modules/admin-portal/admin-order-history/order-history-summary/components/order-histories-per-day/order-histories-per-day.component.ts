import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-order-histories-per-day',
  templateUrl: './order-histories-per-day.component.html',
  styleUrls: ['./order-histories-per-day.component.scss']
})
export class OrderHistoriesPerDayComponent implements OnInit {

  @Input() analitycsData;
  @Input() barChartOptions;
  @Input() barChartLegend;

  constructor() { }

  ngOnInit() {
  }

}
