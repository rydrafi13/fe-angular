import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderHistorySummaryComponent } from './orderhistorysummary.component';
import { FormInputComponent } from '../../../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../../../component-libs/form-select/form-select.component';
import { OrderHistorySummaryRoutingModule } from './orderhistorysummary-routing.module';
import { BsComponentModule } from '../../../bs-component/bs-component.module';
import { ChartsModule as Ng2Charts, ChartsModule } from 'ng2-charts';
import { OrderHistoriesPerDayComponent } from './components/order-histories-per-day/order-histories-per-day.component';
import { OrderHistoriesTotalTodayOrderComponent } from './components/order-histories-total-today-order/order-histories-total-today-order.component';

@NgModule({
  imports: [CommonModule,
    OrderHistorySummaryRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    ChartsModule
  ],

  declarations: [
    OrderHistorySummaryComponent,
    OrderHistoriesPerDayComponent,
    OrderHistoriesTotalTodayOrderComponent],

  exports: [
    OrderHistorySummaryComponent,
    OrderHistoriesPerDayComponent,
    OrderHistoriesTotalTodayOrderComponent  
  ]
},

)
export class OrderHistorySummaryModule { }
