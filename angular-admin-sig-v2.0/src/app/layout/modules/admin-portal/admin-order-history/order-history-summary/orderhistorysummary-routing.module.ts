import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderHistorySummaryComponent } from './orderhistorysummary.component';

const routes: Routes = [
  {
      path: '', component: OrderHistorySummaryComponent,

  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderHistorySummaryRoutingModule { }
