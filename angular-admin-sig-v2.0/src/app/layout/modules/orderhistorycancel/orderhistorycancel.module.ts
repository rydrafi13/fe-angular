import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistorycancelRoutingModule } from './orderhistorycancel-routing.module';
import { OrderhistorycancelComponent } from './orderhistorycancel.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistorycancelDetailComponent } from './detail/orderhistorycancel.detail.component';
import { OrderhistorycancelEditComponent } from './edit/orderhistorycancel.edit.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';


@NgModule({
  imports: [
    CommonModule, 
    OrderhistorycancelRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    NgxBarcodeModule,
    MatButtonModule,
    MatRadioModule
  ],
  declarations: [
    OrderhistorycancelComponent,
    OrderhistorycancelDetailComponent,
    OrderhistorycancelEditComponent
  ],
  exports:[
    OrderhistorycancelComponent,
    OrderhistorycancelDetailComponent,
    OrderhistorycancelEditComponent
  ]
})
export class OrderhistorycancelModule { }
