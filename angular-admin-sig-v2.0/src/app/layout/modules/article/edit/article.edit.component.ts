import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ArticleService } from '../../../../services/article/article.service';


@Component({
  selector: 'app-article-edit',
  templateUrl: './article.edit.component.html',
  styleUrls: ['./article.edit.component.scss'],
  animations: [routerTransition()]
})



export class ArticleEditComponent implements OnInit {
  @Input() public detail:any;
  @Input() public back;

  public loading: boolean = false;
  errorLabel    :any = false;

  constructor(public articleService: ArticleService) {}

  public articleStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad() {

      this.detail.previous_article_id = this.detail._id;
      this.articleStatus.forEach((element, index) => {
        if (element.value === this.detail.activation_status) {
            this.articleStatus[index].selected = 1;
        }
    });
  }

  backToDetail(){
   
    this.back[0][this.back[1]]();
  }
  
  async saveThis(){
   
    try 
    {
      this.loading=!this.loading;
      await this.articleService.updateArticle(this.detail);
      this.loading=!this.loading;
  
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
}
