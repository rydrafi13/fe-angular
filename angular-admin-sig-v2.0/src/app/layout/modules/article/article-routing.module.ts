
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticleComponent } from './article.component';
import { ArticleAddComponent } from './add/article.add.component';
import { ArticleDetailComponent } from './detail/article.detail.component';

const routes: Routes = [
  {
      path: '', component: ArticleComponent
  },
  {
    path:'add', component: ArticleAddComponent
  },
  {
    path:'detail', component: ArticleDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticleRoutingModule { }
