import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticleRoutingModule } from './article-routing.module';
import { ArticleComponent } from './article.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { ArticleAddComponent } from './add/article.add.component';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormInputComponent } from '../../../component-libs/form-input/form-input.component';
import { ArticleDetailComponent } from './detail/article.detail.component';
import { ArticleEditComponent } from './edit/article.edit.component';
// import { NgxEditorModule } from 'ngx-editor';


@NgModule({
  imports: [
    CommonModule, 
    ArticleRoutingModule, 
    PageHeaderModule, 
    FormsModule, 
    FormBuilderTableModule, 
    ReactiveFormsModule,
    // NgxEditorModule,
    NgbModule,
  ],
  declarations: [ArticleComponent, ArticleAddComponent, ArticleDetailComponent, ArticleEditComponent]
})
export class ArticleModule { }
