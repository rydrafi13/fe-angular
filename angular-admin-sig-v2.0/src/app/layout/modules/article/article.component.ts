
import { Component, OnInit }  from '@angular/core';
import { routerTransition }   from '../../../router.animations';
import { ArticleService }     from '../../../services/article/article.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector    : 'app-article',
  templateUrl : './article.component.html',
  styleUrls   : ['./article.component.scss'],
  animations  : [routerTransition()]
})

export class ArticleComponent implements OnInit {

  Article      :any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Article Detail',
                                  label_headers   : [
                                    {label: 'Title', visible: true, type: 'string', data_row_name: 'title'},
                                    // {label: 'Description', visible: false, type: 'string', data_row_name: 'description'},
                                    {label: 'Status', visible: true, type: 'string', data_row_name: 'status'},
                                    {label: 'Organization ID', visible: true, type: 'string', data_row_name: 'org_id'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Article',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;

  articleDetail    : any = false;
  service       : any;
  selectedFile  = null;
  progressBar   :number = 0;
  cancel        :boolean = false;
  errorFile     :any = false;

  constructor(public articleService:ArticleService) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){
    try{
      let result:any;
      this.service    = this.articleService;
      result          = await this.articleService.getArticleLint();
      this.Article   = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async callDetail(_id)
  {
    try{
      let result:any;
      this.service    = this.articleService;
      result          = await this.articleService.detailArticle(_id);
      console.log(result);
      if(!result.error && result.error == false){
        this.articleDetail = result.result[0];
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.articleDetail = false;
    // delete obj.prodDetail;
    
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }
}
