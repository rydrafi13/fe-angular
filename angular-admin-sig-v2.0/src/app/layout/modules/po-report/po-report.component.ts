import { Component, OnInit } from '@angular/core';
import {TableFormat} from '../../../object-interface/common.object';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-po-report',
  templateUrl: './po-report.component.html',
  styleUrls: ['./po-report.component.scss']
})
export class POReportComponent implements OnInit {

  PointMovementOrder: any = [];
  tableFormat: TableFormat = {

    title: 'PO Number Report',

    label_headers: [
      { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'PO Number', visible: true, type: 'value_po', data_row_name: 'po_no'},
      { label: 'ID Toko', visible: true, type: 'string', data_row_name: 'id_toko' },
      { label: 'Nama Toko', visible: true, type: 'string', data_row_name: 'nama_pemilik' },
      { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
      { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],

    row_primary_key: 'order_id',
    formOptions: {
      row_id: 'order_id',
      this: this,
      result_var_name: 'Orderhistoryallhistory',
      detail_function: [this, 'callDetail'],
    },
    show_checkbox_options: true
  };

  tableFormat2: TableFormat = {

    title: 'PO Number Report',

    label_headers: [
      { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'PO Number', visible: true, type: 'value_po', data_row_name: 'po_no'},
      { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
      { label: 'Nama Pelanggan', visible: true, type: 'string', data_row_name: 'nama_pemilik' },
      { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],

    row_primary_key: 'order_id',
    formOptions: {
      row_id: 'order_id',
      this: this,
      result_var_name: 'Orderhistoryallhistory',
      detail_function: [this, 'callDetail'],
    },
    show_checkbox_options: true
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;

  // SalesRedemptionDetail: any = false;
  orderhistoryallhistoryDetail: any = false;
  service: any;

  errorMessage: any = false;

  Orderhistoryallhistory: any = [];

  mci_project: any = false;

  public contentList : any = (content as any).default;

  constructor(public orderhistoryService: OrderhistoryService,) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    console.log(' stock movement')
    try{
      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
          if(element.appLabel == program) {
              if(element.type == "reguler") {
                  _this.mci_project = true;
              } else {
                  _this.mci_project = false;
              }
          }
      });

       this.service = this.orderhistoryService;
       const result: any = await this.orderhistoryService.getReportBasedOnPO();
       this.totalPage = result.total_page;
       
       this.Orderhistoryallhistory = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			}
    }
  }
  
  public async callDetail(SalesRedemption_id){
    try{
      // let result: any;
      // this.service    = this.PointsTransactionService;
      // result          = await this.PointsTransactionService.detailPointstransaction(SalesRedemption_id);
      // console.log(result);

      // this.SalesRedemptionDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.SalesRedemptionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}
