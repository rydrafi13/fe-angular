import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { POReportComponent } from './po-report.component';

describe('PointMovementOrderComponent', () => {
  let component: POReportComponent;
  let fixture: ComponentFixture<POReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ POReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(POReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
