import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { POReportComponent } from './po-report.component'


const routes: Routes = [
  {
    path: '', component: POReportComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class POReportRoutingModule { }
