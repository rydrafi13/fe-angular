import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminSalesorderComponent } from './admin-salesorder.component';

const routes: Routes = [
  {
    path: '', component: AdminSalesorderComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminSalesorderRoutingModule { }
