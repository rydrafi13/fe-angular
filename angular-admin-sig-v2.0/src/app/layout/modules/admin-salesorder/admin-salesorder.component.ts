import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { Router } from '@angular/router';
@Component({
  selector: 'app-admin-salesorder',
  templateUrl: './admin-salesorder.component.html',
  styleUrls: ['./admin-salesorder.component.scss']
})
export class AdminSalesorderComponent implements OnInit {
  swaper = true;
  Pointstransaction: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Sales Order Report Page (Product)',
                                  label_headers   : [
                                    {label: 'Tgl. Order', visible: true, type: 'date', data_row_name: 'order_date'},
                                    {label: 'No Order', visible: true, type: 'string', data_row_name: 'invoice_id'},
                                    {label: 'Member ID', visible: true, type: 'string', data_row_name: 'member_email'},
                                    {label: 'Name', visible: true, type: 'string', data_row_name: 'member_name'},
                                    {label: 'Item Code', visible: true, type: 'string', data_row_name: 'product_code'},
                                    {label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name'},
                                    {label: 'Category', visible: true, type: 'string', data_row_name: 'categories'},
                                    {label: 'Merchant', visible: true, type: 'string', data_row_name: 'merchant_username'},
                                    {label: 'Qty', visible: true, type: 'string', data_row_name: 'quantity'},
                                    {label: 'Unit Price', visible: true, type: 'string', data_row_name: 'fixed_price'},  
                                    {label: 'Total Order', visible: true, type: 'string', data_row_name: 'total_product_price'}, 
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: 'invoice_id',
                                                    this  : this,
                                                    result_var_name: 'Pointstransaction',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  tableFormat2        : TableFormat = {
    title           : 'Sales Order Report Page (Order ID)',
    label_headers   :   [
      {label: 'Order Date', visible: true, type: 'date', data_row_name: 'order_date'},
      {label: 'No Order', visible: true, type: 'string', data_row_name: 'invoice_id'},
      {label: 'Member ID', visible: true, type: 'string', data_row_name: 'member_email'},
      {label: 'Member Name', visible: true, type: 'string', data_row_name: 'member_name'},
      {label: 'Total Order', visible: true, type: 'string', data_row_name: 'total_price'},
      {label: 'Total Shipping', visible: true, type: 'string', data_row_name: 'total_shipping'},
      {label: 'Lo Point', visible: true, type: 'string', data_row_name: 'lo_points'},
      {label: 'Unique Amount', visible: true, type: 'string', data_row_name: 'unique_amount'},
      {label: 'Total Order Summary', visible: true, type: 'string', data_row_name: 'sum_total'},

      // {label: 'Product', visible: true, type: 'product', data_row_name: 'products'},
      // {label: 'Item Code', visible: true, type: 'productcode', data_row_name: 'products'},
      // {label: 'Item Description', visible: true, type: 'productdesc', data_row_name: 'products'},
      // {label: 'Category', visible: true, type: 'productcat', data_row_name: 'products'},
      // {label: 'Merchant', visible: true, type: 'productmerc', data_row_name: 'products'},
      // {label: 'Qty', visible: true, type: 'productqty', data_row_name: 'products'},
      // {label: 'Unit Price', visible: true, type: 'productunit', data_row_name: 'products'},  
      // {label: 'Total Order', visible: true, type: 'producttotal', data_row_name: 'products'}, 
    ],
    row_primary_key : '_id',
    formOptions     : {
                      row_id: 'invoice_id',
                      this  : this,
                      result_var_name: 'Pointstransaction',
                      detail_function: [this, 'callDetail']
                    }
};
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;

  pointstransactionDetail: any = false;
  service: any;
  constructor(public OrderhistoryService:OrderhistoryService,private router: Router) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    try{
      var params = {
         search: {based_on: "products"},
         current_page:1,
         limit_per_page:50
        }

      this.service    = this.OrderhistoryService;
      let result:any = {}
      if(this.swaper == true){
        result = await this.OrderhistoryService.getSalesOrderReportint('product');
      }else{
        result= await this.OrderhistoryService.getSalesOrderReportint('order_id');
      }
    
      this.totalPage = result.result.total_page
      this.Pointstransaction = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  swapClick(bool) {
    this.swaper = bool;
    this.firstLoad();
  }
  public async callDetail(orderhistory_id) {
    this.router.navigate(['administrator/orderhistoryallhistoryadmin/edit'],  {queryParams: {id: orderhistory_id }})
    // try {
    //   let result: any;
    //   this.service = this.orderhistoryService;
    //   result       = await this.orderhistoryService.detailOrderhistoryallhistory(orderhistory_id);
    //   // console.log(result);
    //   this.orderhistoryallhistoryDetail = result.result[0];

    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message); // conversion to Error type
    // }
  }

  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}
