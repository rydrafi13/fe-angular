import { Component, OnInit, Input } from '@angular/core';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../../router.animations';
import { CategoryService } from '../../../../services/category/category.service';


@Component({
  selector: 'app-category-edit',
  templateUrl: './category.edit.component.html',
  styleUrls: ['./category.edit.component.scss'],
  animations: [routerTransition()]
})

export class CategoryEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading        :boolean = false;

  errorLabel    : any = false;


  nameAlert:boolean = false;
  typeAlert:boolean = false;
  codeAlert:boolean = false;
  keyAlert:boolean = false;

  categoryType = [
    { label: 'Product', value: 'product', selected:1 },
    { label: 'Gold', value: 'gold'},
    { label: 'Voucher Fisik', value: 'voucher' },
    { label: 'E-Voucher', value: 'e-voucher'}
  ]


  constructor(public categoryService:CategoryService) {}

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    // this.detail.previous_status = this.detail.status;
   

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });
    this.categoryType.forEach((element, index) => {
      if (element.value == this.detail.type) {
        this.categoryType[index].selected = 1;
      } else {
        this.categoryType[index].selected = 0;
      }
    });
    
  }

  backToDetail(){
    //console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis(){
    try 
    {
      let isFormValid:boolean = true;

      if(this.detail.name == "") {
        this.nameAlert = true;

        document.getElementById("name").scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
        });

        isFormValid = false;
      } else {
        this.nameAlert = false;
      }

      if(this.detail.type == "") {
        this.typeAlert = true;

        document.getElementById("name").scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
        });

        isFormValid = false;
      } else {
        this.typeAlert = false;
      }

      if(this.detail.code == "") {
        this.codeAlert = true;

        document.getElementById("name").scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
        });

        isFormValid = false;
      } else {
        this.codeAlert = false;
      }

      if(this.detail.key == "") {
        this.keyAlert = true;

        document.getElementById("name").scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
        });

        isFormValid = false;
      } else {
        this.keyAlert = false;
      }

      if(isFormValid == true) {
        this.loading=!this.loading;
        await this.categoryService.updateCategoryID(this.detail);
        this.loading=!this.loading;
        Swal.fire("Category Has Been Edited!")
        .then(() => {
          this.backToDetail();
        });
      }
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      })
    }
  }
  
}
