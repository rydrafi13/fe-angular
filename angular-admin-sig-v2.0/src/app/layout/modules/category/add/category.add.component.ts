import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../../router.animations';
import { CategoryService } from '../../../../services/category/category.service';

@Component({
  selector: 'app-category-add',
  templateUrl: './category.add.component.html',
  styleUrls: ['./category.add.component.scss'],
  animations: [routerTransition()]
})

export class CategoryAddComponent implements OnInit {
  public name: string = "";
  Categorys: any = [];
  service: any;

 selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     : any = false;
  errorLabel    : any = false;
  code:any;
  type:any='product';
  key:any;
  imageUrl:any;

  nameAlert:boolean = false;
  typeAlert:boolean = false;
  codeAlert:boolean = false;
  keyAlert:boolean = false;


  categoryType = [
    { label: 'Product', value: 'product', selected:1 },
    { label: 'Gold', value: 'gold'},
    { label: 'Voucher Fisik', value: 'voucher' },
    { label: 'E-Voucher', value: 'e-voucher'}
  ]
  
  constructor(public categoryService:CategoryService, private router: Router) {
    let form_add     : any = [
      { label:"id",  type: "text",  value: "", data_binding: '_id'  },
      { label:"name",  type: "text",  value: "", data_binding: 'name'  },
      { label:"code",  type: "text",  value: "", data_binding: 'code'  },
      { label:"imageUrl",  type: "text",  value: "", data_binding: 'imageUrl' },
    ];
   }
  
   form: any = {
    name : '',
    type : 'product',
    code : null,
    key : ''
   }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;
    
  }

  async formSubmitAddCategory(){
    try 
    {
      let isFormValid:boolean = true;

      if(this.form.name == "") {
        this.nameAlert = true;

        document.getElementById("name").scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
        });

        isFormValid = false;
      } else {
        this.nameAlert = false;
      }

      if(this.form.type == "") {
        this.typeAlert = true;

        document.getElementById("name").scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
        });

        isFormValid = false;
      } else {
        this.typeAlert = false;
      }

      if(!this.form.code || this.form.code == "") {
        this.codeAlert = true;

        document.getElementById("name").scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
        });

        isFormValid = false;
      } else {
        this.codeAlert = false;
      }

      if(this.form.key == "") {
        this.keyAlert = true;

        document.getElementById("name").scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
        });

        isFormValid = false;
      } else {
        this.keyAlert = false;
      }


      if(isFormValid == true) {
        this.service    = this.categoryService;
        let result: any  = await this.categoryService.addNewCategory(this.form);
        
        Swal.fire("Category has been added!").then(() => {
          this.router.navigate(['administrator/categoryadmin']);
        });
      }
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      })
    }
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }

  codeType() {
    if(this.form.code && this.form.code.length > 5) this.form.code = this.form.code.substring(0,5);
  }

  nameType() {
    if(this.form.name) this.form.key = this.form.name.trimRight().replace(/\s+/g, '-').toLowerCase();
  }

  backTo(){
    window.history.back();
  }
}
