import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminEscrowByMerchantComponent} from './admin-escrow-by-merchant.component'


const routes: Routes = [
  {
    path: '', component: AdminEscrowByMerchantComponent,

}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminEscrowByMerchantRoutingModule { }
