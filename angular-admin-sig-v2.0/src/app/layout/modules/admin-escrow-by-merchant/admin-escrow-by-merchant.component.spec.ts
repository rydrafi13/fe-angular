import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEscrowByMerchantComponent } from './admin-escrow-by-merchant.component';

describe('AdminEscrowByMerchantComponent', () => {
  let component: AdminEscrowByMerchantComponent;
  let fixture: ComponentFixture<AdminEscrowByMerchantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEscrowByMerchantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEscrowByMerchantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
