import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderAmountHistorySummaryComponent } from './orderamounthistorysummary.component';

const routes: Routes = [
  {
      path: '', component: OrderAmountHistorySummaryComponent,

  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderAmountHistorySummaryRoutingModule { }
