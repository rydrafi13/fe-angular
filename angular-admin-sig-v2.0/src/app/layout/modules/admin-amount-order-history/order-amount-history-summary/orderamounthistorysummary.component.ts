import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

import { DomSanitizer } from '@angular/platform-browser';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import {TopBarMenuItem} from '../../../../object-interface/common.object';




@Component({
  selector: 'app-OrderAmountHistorysummary-OrderAmountHistory',
  templateUrl: './orderamounthistorysummary.component.html',
  styleUrls: ['./orderamounthistorysummary.component.scss'],
  animations: [routerTransition()]
})

export class OrderAmountHistorySummaryComponent implements OnInit {

  data       : any = [];
  service    : OrderhistoryService
  topBarMenu : TopBarMenuItem[] =[
    {label: "Number of Sales", routerLink: '/administrator/order-history-summary'},
    {label: "Amount of Sales", routerLink: '/administrator/order-amount-history-summary', active: true},
    {label: "Abondonment Rate", routerLink: '/administrator/cart-abandonment-summary'}
  ];
  errorLabel : string
  topMembers : []
  avgData = {
    monthly:{
      all:0,
      pending:0,
      paid:0,
      checkout:0,
      waiting:0,
      cancel:0
    },
    daily:{
      all:0,
      pending:0,
      paid:0,
      checkout:0,
      waiting:0,
      cancel:0
    },
    yearly:{
      all:0,
      pending:0,
      paid:0,
      checkout:0,
      waiting:0,
      cancel:0
    },
    hourly:{
      all:0,
      pending:0,
      paid:0,
      checkout:0,
      waiting:0,
      cancel:0
    }
  }
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          callback: this.currencyFormatter,
        }
      }]
    },
    // elements: {
    //     line: {
    //         tension: 0 // disables bezier curves
    //     }
    // }
  };
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
      { data: [], label: 'All Order' },
  ];

  public optO = {fill:false , borderWidth: 1,}
  public analitycsData ={
    monthly : {
      barChartData : [
        { data: [], label: 'All Order' ,
        ...this.optO},
      ],
      barChartLabels : []
    },
    daily : {
      barChartData : [
        { ...{data: [], label: 'All Order'} ,
          ...this.optO
      },
      ],
      barChartLabels : []
    },
    hourly : {
      barChartData : [
        { data: [], label: 'All Order' ,
        ...this.optO},
      ],
      barChartLabels : []
    },
    yearly : {
      barChartData : [
        { data: [], label: 'All Order' ,
        ...this.optO},
      ],
      barChartLabels : []
    }
  }
  // private options = new RequestOptions(
  //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(public OrderAmountHistoryService:OrderhistoryService , public sanitizer:DomSanitizer) {
    // this.firstLoad();
    // this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
  }

  currencyFormatter(value, index, values) {

      // add comma as thousand separator
      let val=0;
      let kbm = '';

      if(value > (1000000000 - 1)){
        val = value/1000000000;
        kbm = "B";
      }
      else if(value > (1000000 - 1)){
        val = value/1000000;
        kbm = "M";
      }
      else if(value > (1000 - 1)){
        val = value/1000;
        kbm = "K";
      }
      // console.log("VAL ", val, value);
      return (val).toLocaleString('id-ID', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 0 
      }) + kbm; 
    
  }

  onUpdateCart(data){
    const clonedData = JSON.parse(JSON.stringify(data.barChartData));
    const clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = [];
    data.barChartLabels.forEach(()=>{
        clone[0].data.push(Math.round(Math.random() * 100))
    });
    data.barChartData   = clone;
    
    setTimeout(()=>{
      // this.firstLoad();
      data.barChartData   = clonedData;
    },500)
    
  }

  async ngOnInit() {
    let currentDate = new Date();
    console.log(this.barChartLabels);
    // this.onUpdateCart();
    this.firstLoad();

  }

  generateChartData(dataValues){

    let barChartLabels = [];
    let newData        = [];


    let allData:any[];
    allData = dataValues;

    for(let data in allData){
      // console.log('allData data', allData[data])
      barChartLabels.push(data)
      newData.push(allData[data])

    }
    allData = null; //clearing memory
    // console.log("new Data", newData)
    
    return [barChartLabels, newData]
  }

  async firstLoad() {

    try {
      this.service       = this.OrderAmountHistoryService;
      const result: any  = await this.OrderAmountHistoryService.getAmountOrderHistorySummaryByDate('today');
      this.data          = result.result;
      if(this.data.order_history_by_month){
        
        const convertedDataMonthly        = this.generateChartData(this.data.order_history_by_month.value);
        const convertedDataMonthlyPending = this.generateChartData(this.data.order_history_by_month_pending.value);
        const convertedDataMonthlyPaid    = this.generateChartData(this.data.order_history_by_month_paid.value);
        const convertedDataMonthlyCancel  = this.generateChartData(this.data.order_history_by_month_cancel.value);
        const convertedDataMonthlyWaiting = this.generateChartData(this.data.order_history_by_month_waiting.value);
        const convertedDataMonthlyCheckout= this.generateChartData(this.data.order_history_by_month_checkout.value);

        this.avgData.monthly.all      = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month.value));
        this.avgData.monthly.pending  = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_pending.value));
        this.avgData.monthly.paid     = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_paid.value));
        this.avgData.monthly.cancel   = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_cancel.value));
        this.avgData.monthly.waiting  = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_waiting.value));
        this.avgData.monthly.checkout = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_checkout.value));
        
        // console.log("convertedDataMonthly", convertedDataMonthly);
        const clone   = JSON.parse(JSON.stringify(this.analitycsData.monthly.barChartData));
        clone[0].data = convertedDataMonthly[1];
        // console.log('convertedDataMonthlyPending', convertedDataMonthlyPending);
        clone.push({
          data : convertedDataMonthlyPending[1],
          label: "pending",
          fill:false 
        })
        clone.push({
          data : convertedDataMonthlyPaid[1],
          label: "paid",
          fill:false 
        })
        clone.push({
          data : convertedDataMonthlyCancel[1],
          label: "Cancel",
          fill:false 
        })
        clone.push({
          data : convertedDataMonthlyWaiting[1],
          label: "Waiting",
          fill:false 
        })
        clone.push({
          data : convertedDataMonthlyCheckout[1],
          label: "Checkout",
          fill:false 
        })

        console.log("convertedDataMonthly clone", clone);
        
        this.barChartLabels = convertedDataMonthly[0]
        this.analitycsData.monthly.barChartLabels = convertedDataMonthly[0]

        // this.barChartLabels = convertedDataMonthly[0]
        // this.analitycsData.monthly.barChartLabels = convertedDataMonthly[0]
        // this.barChartData   = clone;
        this.analitycsData.monthly.barChartData = clone;
        
      }
      if(this.data.order_history_by_the_day){
        
        const convertedDataDaily = this.generateChartData(this.data.order_history_by_the_day.value);
        const convertedDataDailyPending = this.generateChartData(this.data.order_history_by_the_day_pending.value);
        const convertedDataDailyPaid    = this.generateChartData(this.data.order_history_by_the_day_paid.value);
        const convertedDataDailyCancel  = this.generateChartData(this.data.order_history_by_the_day_cancel.value);
        const convertedDataDailyWaiting = this.generateChartData(this.data.order_history_by_the_day_waiting.value);
        const convertedDataDailyCheckout= this.generateChartData(this.data.order_history_by_the_day_checkout.value);

        this.avgData.daily.all      = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day.value));
        this.avgData.daily.pending  = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_pending.value));
        this.avgData.daily.paid     = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_paid.value));
        this.avgData.daily.cancel   = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_cancel.value));
        this.avgData.daily.waiting  = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_waiting.value));
        this.avgData.daily.checkout = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_checkout.value));

        
        const clone = JSON.parse(JSON.stringify(this.analitycsData.daily.barChartData));
        console.log("convertedData daily", clone);

        clone[0].data = convertedDataDaily[1];

        clone.push({
          data : convertedDataDailyPending[1],
          label: "pending",
          fill:false 
        })
        clone.push({
          data : convertedDataDailyPaid[1],
          label: "paid",
          fill:false 
        })
        clone.push({
          data : convertedDataDailyCancel[1],
          label: "Cancel",
          fill:false 
        })
        clone.push({
          data : convertedDataDailyWaiting[1],
          label: "Waiting",
          fill:false 
        })
        clone.push({
          data : convertedDataDailyCheckout[1],
          label: "Checkout",
          fill:false 
        })

        this.analitycsData.daily.barChartLabels = convertedDataDaily[0]
        this.barChartLabels = convertedDataDaily[0]
        // this.barChartData   = clone;
        this.analitycsData.daily.barChartData = clone;
        console.log("his.analitycsData", this.analitycsData.daily)

      }
      if(this.data.order_history_hourly){
        
        const convertedDataHourly         = this.generateChartData(this.data.order_history_hourly.value);
        const convertedDataHourlyPending  = this.generateChartData(this.data.order_history_hourly_pending.value);
        const convertedDataHourlyPaid     = this.generateChartData(this.data.order_history_hourly_paid.value);
        const convertedDataHourlyCancel   = this.generateChartData(this.data.order_history_hourly_cancel.value);
        const convertedDataHourlyCheckout = this.generateChartData(this.data.order_history_hourly_checkout.value);
        const convertedDataHourlyWaiting  = this.generateChartData(this.data.order_history_hourly_waiting.value);

        this.avgData.hourly.all      = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly.value));
        this.avgData.hourly.pending  = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_pending.value));
        this.avgData.hourly.paid     = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_paid.value));
        this.avgData.hourly.cancel   = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_cancel.value));
        this.avgData.hourly.waiting  = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_waiting.value));
        this.avgData.hourly.checkout = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_checkout.value));

        // const clone = JSON.parse(JSON.stringify(this.analitycsData.hourly.barChartData));
        const clone:any = this.analitycsData.hourly.barChartData;

        SETUP_DATA_FOR_HOURLY:{
          clone[0].data       = convertedDataHourly[1];
          clone.push({
            data : convertedDataHourlyPending[1],
            label: "pending",
            fill:false 
          })
          clone.push({
            data : convertedDataHourlyPaid[1],
            label: "paid",
            fill:false 
          })
          clone.push({
            data : convertedDataHourlyCancel[1],
            label: "Cancel",
            fill:false 
          })
          clone.push({
            data : convertedDataHourlyWaiting[1],
            label: "Waiting",
            fill:false 
          })
          clone.push({
            data : convertedDataHourlyCheckout[1],
            label: "Checkout",
            fill:false 
          })
        }
        
        this.analitycsData.hourly.barChartLabels = convertedDataHourly[0]
        this.barChartLabels = convertedDataHourly[0]
        // this.barChartData   = clone;
        this.analitycsData.hourly.barChartData = clone;
        console.log("his.analitycsData hourly", this.analitycsData.hourly)

      }

      if(this.data.order_history_yearly){
        
        const convertedDataYearly         = this.generateChartData(this.data.order_history_yearly.value);
        const convertedDataYearlyPending  = this.generateChartData(this.data.order_history_yearly_pending.value);
        const convertedDataYearlyPaid     = this.generateChartData(this.data.order_history_yearly_paid.value);
        const convertedDataYearlyCancel   = this.generateChartData(this.data.order_history_yearly_cancel.value);
        const convertedDataYearlyCheckout = this.generateChartData(this.data.order_history_yearly_checkout.value);
        const convertedDataYearlyWaiting  = this.generateChartData(this.data.order_history_yearly_waiting.value);

        this.avgData.yearly.all      = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly.value));
        this.avgData.yearly.pending  = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_pending.value));
        this.avgData.yearly.paid     = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_paid.value));
        this.avgData.yearly.cancel   = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_cancel.value));
        this.avgData.yearly.waiting  = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_waiting.value));
        this.avgData.yearly.checkout = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_checkout.value));

        // const clone = JSON.parse(JSON.stringify(this.analitycsData.yearly.barChartData));
        const clone:any = this.analitycsData.yearly.barChartData;

        SETUP_DATA_FOR_HOURLY:{
          clone[0].data       = convertedDataYearly[1];
          clone.push({
            data : convertedDataYearlyPending[1],
            label: "pending",
            fill:false 
          })
          clone.push({
            data : convertedDataYearlyPaid[1],
            label: "paid",
            fill:false 
          })
          clone.push({
            data : convertedDataYearlyCancel[1],
            label: "Cancel",
            fill:false 
          })
          clone.push({
            data : convertedDataYearlyWaiting[1],
            label: "Waiting",
            fill:false 
          })
          clone.push({
            data : convertedDataYearlyCheckout[1],
            label: "Checkout",
            fill:false 
          })
        }
        
        this.analitycsData.yearly.barChartLabels = convertedDataYearly[0]
        this.barChartLabels = convertedDataYearly[0]
        // this.barChartData   = clone;
        this.analitycsData.yearly.barChartData = clone;
        console.log("his.analitycsData yearly", this.analitycsData.yearly)

        TOP_MEMBERS_SETUP:{
          this.topMembers = this.data.top_users.value
        }
      }
    } catch (e) {
      console.log("this e result", e)

      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  calculateTheAverageValue( _value: Object){
    let howMany:number = Object.keys(_value).length
    let sum = 0;

    Reflect.ownKeys(_value).forEach((key)=>{
      sum = sum + _value[key];
    });
    
    let avg = sum/howMany;
    return avg;
  }
  



}
