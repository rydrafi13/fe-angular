import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PointstransactionRoutingModule } from './points-transaction.routing';
import { PointstransactionComponent } from './points-transaction.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { PointstransactionDetailComponent } from './detail/pointstransaction.detail.component';
import { PointstransactionEditComponent } from './edit/pointstransaction.edit.component';



@NgModule({
  imports: [
    CommonModule,
    PointstransactionRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
  declarations: [PointstransactionComponent,PointstransactionDetailComponent,PointstransactionEditComponent]
  })

export class PointstransactionModule { }
