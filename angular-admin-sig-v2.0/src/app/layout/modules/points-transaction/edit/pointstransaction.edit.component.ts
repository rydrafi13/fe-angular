import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PointsTransactionService } from '../../../../services/points-transaction/points-transaction.service';


@Component({
  selector: 'app-pointstransaction-edit',
  templateUrl: './pointstransaction.edit.component.html',
  styleUrls: ['./pointstransaction.edit.component.scss'],
  animations: [routerTransition()]
})

export class PointstransactionEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading        :boolean = false;

  public transactionStatus  : any = [
    {label:"PENDING", value:"PENDING"}
   ,{label:"CANCEL", value:"CANCEL"}
   ,{label:"FAILED", value:"FAILED"}
   ,{label:"SUCCESS", value:"SUCCESS"}
   ,{label:"ERROR", value:"ERROR"}
];
  errorLabel    : any = false;
  transaction_status : any;


  constructor(public pointstransactionService:PointsTransactionService) {}

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    try 
    {
      this.detail.previous_status = await this.detail.status;
      this.transactionStatus.forEach((element, index) => {
      if(element.value == this.detail.transaction_status){
              this.transactionStatus[index].selected = 1;
          }
      });
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });
    
  }

  backToDetail(){
    //console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis(){
    try 
    {
      this.loading=!this.loading;
      await this.pointstransactionService.updatePointstransaction(this.detail);
      this.loading=!this.loading;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
}
