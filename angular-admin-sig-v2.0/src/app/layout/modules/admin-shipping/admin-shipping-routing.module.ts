import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminShippingComponent } from './admin-shipping.component';


const routes: Routes = [
  {
    path: '', component: AdminShippingComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminShippingRoutingModule { }
