import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { AdminService } from '../../../../services/admin/admin.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin-group-add',
  templateUrl: './admin-group.add.component.html',
  styleUrls: ['./admin-group.add.component.scss'],
  animations: [routerTransition()]
})

export class AdminGroupAddComponent implements OnInit {
  public name: string = "";
  Members: any = [];
  service: any;
  full_name:any;
  email:any;
  password:any;
  cell_phone:any;
  card_number:any;
  dob:any;
  mcity:any;
  mstate:any;
  mcountry:any;
  maddress1:any;
  mpostcode:any;
  gender:any;
  marital_status:any;
  type:any;
  member_status:any;
  activation_status:any;
  public form: any = {
    group_name: "",
    description: "",
    access_list: {
      "usr_mngt":[]
    },
  }

  public alertGroupname:boolean = false;
  public alertDescription:boolean = false;
  public alertAccessList:boolean = false;

  
  errorLabel : any = false;
  errorMessage : any = false;
                
  constructor(public adminService:AdminService) {}

  public usrMngtData: any = []; 

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){
    try {
      await this.adminService.getAccessListApp().then((result) => {
        result.usr_mngt.forEach((element) => {
          this.usrMngtData.push(
            {
              name : element.name,
              checked : 0
            }
          );
        });
      });
    } catch (error) {
      this.errorLabel = ((<Error>error).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  async formSubmitAddMember(){
    if(!this.form.group_name  || this.form.group_name.length == 0){
      this.alertGroupname = true;
    } else {
      this.alertGroupname = false;
    }

    if(!this.form.description  || this.form.description.length == 0){
      this.alertDescription = true;
    } else {
      this.alertDescription = false;
    }

    let usrMngtForm: any = [];
    this.usrMngtData.forEach((element) => {
      if(element.checked == 1) usrMngtForm.push(element.name);
    });

    if(usrMngtForm.length == 0){
      this.alertAccessList = true;
    } else {
      this.alertAccessList = false;
    }
    
    if(!this.alertGroupname && !this.alertDescription && !this.alertAccessList) {
      this.form.access_list.usr_mngt = usrMngtForm;

      await this.adminService.addAdminGroup(this.form).then(() => {
        Swal.fire("Group Admin Has Been Added!")
        .then(() => {
          this.backTo();
        });
      }).catch(error => {
        this.errorLabel = ((<Error>error).message);
          let message = this.errorLabel;
          if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
          }
          Swal.fire({
            icon: 'error',
            title: message,
          })
      });
    }
}

backTo(){
  window.history.back();
}

checkList(index) {
  if(this.usrMngtData[index].checked == 1) {
    this.usrMngtData[index].checked = 0;
  } else if(this.usrMngtData[index].checked == 0) {
    this.usrMngtData[index].checked = 1;
  }
}

}
