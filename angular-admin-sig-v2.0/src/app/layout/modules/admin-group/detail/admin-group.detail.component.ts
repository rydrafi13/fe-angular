import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { AdminService } from '../../../../services/admin/admin.service';
import { del } from 'selenium-webdriver/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-group-detail',
  templateUrl: './admin-group.detail.component.html',
  styleUrls: ['./admin-group.detail.component.scss'],
  animations: [routerTransition()]
})

export class AdminGroupDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit: boolean = false;
  errorLabel : any = false;
  adminDetail:any;
  constructor(public adminService: AdminService,private router: Router,private route: ActivatedRoute) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    let _id = this.detail._id;
    try {
      const result : any= await this.adminService.getAdminGroupByID(_id);
      if(result.length > 0){
        if(result[0]._id == this.detail._id){
          this.adminDetail = result[0];
          // console.log("data member from firstload detail", this.memberDetail);
        }
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  editThis() {
    // console.log(this.edit );
    this.edit = !this.edit;
    if(this.edit == false){
      this.firstLoad();
    }
    // console.log(this.edit );
  }
  backToTable() {
    // console.log(this.back);
    this.back[1](this.back[0]);
  }

  // async delete(){
  //   try {
  //     const delResult: any = await this.memberService.deleteMember(this.detail);
  //     console.log(delResult);
  //     if (delResult.error == false) {
  //       console.log(this.back[0]);
  //       this.back[0].memberDetail = false;
  //       this.back[0].firstLoad();
  //     // delete this.back[0].prodDetail;
  //     }
  //   } catch (error) {
  //     throw new TypeError(error.error.error);
  //   }
  // } catch (e) {
  //   this.errorLabel = ((<Error>e).message);//conversion to Error type
  // }

  // async pointHistory(){
  //   console.log('here')
  //   // this.router.navigate(['/member-point-history']);
  //   this.router.navigate(['administrator/memberadmin/point-history'],  {queryParams: {id: this.detail.email }})
  // }

  pipeToComma(data) {
    return data.replace(/\|/g, ', ')
  }

}
