import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PointsModelsService } from '../../../../services/points.models/pointsmodels.service';

@Component({
  selector: 'app-points-models-add',
  templateUrl: './pointsmodels.add.component.html',
  styleUrls: ['./pointsmodels.add.component.scss'],
  animations: [routerTransition()]
})

export class PointsModelsAddComponent implements OnInit {
  public name: string = "";
  public ruleType: any = [];
  public calcType: any = [];
  public transactionType: any = [];
  public modelsStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  rule : any = false;
  calculation : any = false;
  transaction : any = false;

  PointsModels: any = [];
  service: any;
  errorLabel : any = false;
  merchant   : any = false;
  rule_type  : any;
  transaction_type : any;
  rule_description : any;
  calc_type  : any;
  percentage : any;
  amt_min    : any;
  amt_max    : any;
  amount     : any;
  points     : any;
  status     : any;
  start_date : any;
  end_date   : any;
  

                
  constructor(public pointsmodelsService:PointsModelsService) {
    
   }

  ngOnInit() {
    this.getParameters();
  
  }

   async getParameters(){
    try{
      this.rule = await this.pointsmodelsService.getRuleType();
      this.rule.result.forEach((element) => {
        this.ruleType.push({label:element.description, value:element.value});
      });
      this.calculation = await this.pointsmodelsService.getCalcType();
      this.calculation.result.forEach((element) => {
        this.calcType.push({label:element.description, value:element.value});
      });
      this.transaction = await this.pointsmodelsService.getTransactionType();
      this.transaction.result.forEach((element) => {
        this.transactionType.push({label:element.description, value:element.value});
      });
    }
    catch (e){
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;

  }
  formSubmitAddPointsModels(form){
    // console.log(form);
    // console.log(JSON.stringify(form));

    try {
      this.service    = this.pointsmodelsService;
      const result: any  = this.pointsmodelsService.addNewPointsModels(form);
      this.PointsModels = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

}


}