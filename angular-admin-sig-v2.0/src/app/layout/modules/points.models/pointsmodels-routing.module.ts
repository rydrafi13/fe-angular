import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PointsModelsComponent } from './pointsmodels.component';
import { PointsModelsAddComponent } from './add/pointsmodels.add.component';
import { PointsModelsDetailComponent } from './detail/pointsmodels.detail.component';

const routes: Routes = [
  {
      path: '', component: PointsModelsComponent,

  },
  {
      path:'add', component: PointsModelsAddComponent
  },
  {
    path:'detail', component: PointsModelsDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PointsModelsRoutingModule { }
