import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PointsModelsComponent } from './pointsmodels.component';
import { PointsModelsAddComponent } from './add/pointsmodels.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { PointsModelsDetailComponent } from './detail/pointsmodels.detail.component';
import { PointsModelsRoutingModule } from './pointsmodels-routing.module';
import { PointsModelsEditComponent } from './edit/pointsmodels.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  imports: [CommonModule,
    PointsModelsRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
],
  
  declarations: [
    PointsModelsComponent, PointsModelsAddComponent, PointsModelsDetailComponent,PointsModelsEditComponent]
})
export class PointsModelsModule { }
