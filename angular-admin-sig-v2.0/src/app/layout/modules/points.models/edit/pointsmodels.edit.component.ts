import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PointsModelsService } from '../../../../services/points.models/pointsmodels.service';
//import { ConsoleReporter } from 'jasmine';


@Component({
  selector: 'app-points-models-edit',
  templateUrl: './pointsmodels.edit.component.html',
  styleUrls: ['./pointsmodels.edit.component.scss'],
  animations: [routerTransition()]
})

export class PointsModelsEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  @Input() public ruleType: any;
  @Input() public calcType: any;
  @Input() public transactionType: any;
  errorLabel : any = false;
  rule_type:any;
  transaction_type:any;
  calc_type:any;
  status:any;


  public loading        : boolean = false;
  public modelsStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  constructor(public pointsmodelsService: PointsModelsService) {
  }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    this.detail.previous_status = this.detail.status;
    this.modelsStatus.forEach((element, index) => {
        if (element.value == this.detail.status) {
            this.modelsStatus[index].selected = 1;
        }
    });

    this.detail.previous_rule_type = this.detail.rule_type;
    this.ruleType.forEach((element, index) => {
         if(element.value == this.detail.rule_type){
            this.ruleType[index].selected = 1;
        } 
    });

    this.detail.previous_calc_type = this.detail.calc_type;
    this.calcType.forEach((element, index) => {
        if(element.value == this.detail.calc_type){
            this.calcType[index].selected = 1;
        }
    });

    this.detail.previous_transaction_type = this.detail.transaction_type;
    this.transactionType.forEach((element, index) => {
        if(element.value == this.detail.transaction_type){
            this.transactionType[index].selected = 1;
        }
    });

  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    try {
      this.loading = !this.loading;
      await this.pointsmodelsService.updatePointsModelsID(this.detail);
      this.loading = !this.loading;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

}
