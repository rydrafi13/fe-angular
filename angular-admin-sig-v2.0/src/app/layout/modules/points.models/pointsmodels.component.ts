import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../router.animations';
// import { PointsTransactionService } from '../../../services/points.models/pointsmodels.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { PointsTransactionService } from '../../../services/points-transaction/points-transaction.service';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-points-models',
  templateUrl: './pointsmodels.component.html',
  styleUrls: ['./pointsmodels.component.scss'],
  animations: [routerTransition()]
})

export class PointsModelsComponent implements OnInit {
  @Input() public detail: any;

  PointsModels       : any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Point Transaction Summary',
                                  label_headers   : [
                                    {label: 'ID Toko', visible: true, type: 'string', data_row_name: 'id_toko'},
                                    {label: 'Nama Toko', visible: true, type: 'string', data_row_name: 'nama_toko'},
                                    {label: 'Nama Pemilik', visible: true, type: 'string', data_row_name: 'nama_pemilik'},
                                    {label: 'No WA Pemilik', visible: true, type: 'string', data_row_name: 'no_wa_pemilik'},
                                    {label: 'Alamat Toko', visible: true, type: 'string', data_row_name: 'alamat_toko'},
                                    // {label: 'Beginning Point', visible: true, type: 'string', data_row_name: 'point_add'},
                                    {label: 'Point Add', visible: true, type: 'string', data_row_name: 'point_add'},
                                    {label: 'Adjust in', visible: true, type: 'string', data_row_name: 'point_adj_in'},
                                    {label: 'Adjust Out', visible: true, type: 'string', data_row_name: 'point_adj_out'},
                                    {label: 'Point Less', visible: true, type: 'string', data_row_name: 'point_subtract'},
                                    {label: 'Total Point', visible: true, type: 'string', data_row_name: 'total_point'},
                                    {label: 'Group', visible: true, type: 'string', data_row_name: 'group'},
                                    // {label: 'Start Date', visible: false, type: 'date', data_row_name: 'start_date'},
                                    // {label: 'End Date', visible: false, type: 'date', data_row_name: 'start_date'},
                                      ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    // addForm   : true,
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'PointsModels',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };

  tableFormat2        : TableFormat = {
    title           : 'Point Transaction Summary',
    label_headers   : [
      {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
      {label: 'Nama Pelanggan', visible: true, type: 'string', data_row_name: 'nama_pemilik'},
      {label: 'No WA Pelanggan', visible: true, type: 'string', data_row_name: 'no_wa_pemilik'},
      // {label: 'Beginning Point', visible: true, type: 'string', data_row_name: 'point_add'},
      {label: 'Point Add', visible: true, type: 'string', data_row_name: 'point_add'},
      {label: 'Adjust in', visible: true, type: 'string', data_row_name: 'point_adj_in'},
      {label: 'Adjust Out', visible: true, type: 'string', data_row_name: 'point_adj_out'},
      {label: 'Point Less', visible: true, type: 'string', data_row_name: 'point_subtract'},
      {label: 'Total Point', visible: true, type: 'string', data_row_name: 'total_point'},
      {label: 'Group', visible: true, type: 'string', data_row_name: 'group'},
      // {label: 'Start Date', visible: false, type: 'date', data_row_name: 'start_date'},
      // {label: 'End Date', visible: false, type: 'date', data_row_name: 'start_date'},
        ],
    row_primary_key : '_id',
    formOptions     : {
                      // addForm   : true,
                      row_id: '_id',
                      this  : this,
                      result_var_name: 'PointsModels',
                      detail_function: [this, 'callDetail']
                    }
  };

  tableFormat3        : TableFormat = {
    title           : 'Point Transaction Summary',
    label_headers   : [
      {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
      {label: 'Nama Entitas', visible: true, type: 'string', data_row_name: 'nama_toko'},
      {label: 'Nama PIC', visible: true, type: 'string', data_row_name: 'nama_pemilik'},
      {label: 'No WA PIC', visible: true, type: 'string', data_row_name: 'no_wa_pemilik'},
      {label: 'Alamat Entitas', visible: true, type: 'string', data_row_name: 'alamat_toko'},
      // {label: 'Beginning Point', visible: true, type: 'string', data_row_name: 'point_add'},
      {label: 'Point Add', visible: true, type: 'string', data_row_name: 'point_add'},
      {label: 'Adjust in', visible: true, type: 'string', data_row_name: 'point_adj_in'},
      {label: 'Adjust Out', visible: true, type: 'string', data_row_name: 'point_adj_out'},
      {label: 'Point Less', visible: true, type: 'string', data_row_name: 'point_subtract'},
      {label: 'Total Point', visible: true, type: 'string', data_row_name: 'total_point'},
      // {label: 'Start Date', visible: false, type: 'date', data_row_name: 'start_date'},
      // {label: 'End Date', visible: false, type: 'date', data_row_name: 'start_date'},
        ],
    row_primary_key : '_id',
    formOptions     : {
                      // addForm   : true,
                      row_id: '_id',
                      this  : this,
                      result_var_name: 'PointsModels',
                      detail_function: [this, 'callDetail']
                    }
  };

  public contentList : any = (content as any).default;
  programType: any = "";

  form_input    : any = {};
  errorLabel : any = false;
  errorMessage  : any = false;
  pointsModelsDetail: any = false;
  service     : any ;
 
  totalPage  = 0;

  mci_project: any = false;


  constructor(public PointsTransactionService:PointsTransactionService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
        if(element.appLabel == program) {
          if(element.type == "reguler") {
              _this.mci_project = true;
              _this.programType = "reguler";
          } else if(element.type == "custom_kontraktual") {
            _this.mci_project = false;
            _this.programType = "custom_kontraktual";
          } else {
              _this.mci_project = false;
              _this.programType = "custom";
          }
        }
      });
     
      this.service    = this.PointsTransactionService;
      const result: any  = await this.PointsTransactionService.getPointstransactionReportLint();
      this.PointsModels = result.values;
      this.totalPage = result.total_page
      console.log('points models : ', this.PointsModels);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  public async callDetail(_id) {
    
    // try {
      
    //   this.service    = this.PointsTransactionService;
    //   let result: any = await this.PointsTransactionService.detailPointsModels(_id);
    //   this.pointsModelsDetail = result.result;
    //   console.log(this.pointsModelsDetail);
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  public async backToHere(obj) {
    obj.pointsModelsDetail = false;
  }

}
