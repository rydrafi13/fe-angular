import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnrComponent } from './cnr.component';

const routes: Routes = [
  {
      path: '', component: CnrComponent,

  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CnrRoutingModule { }
