import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OutletsComponent } from './../outlets/outlets.component';
import { OutletsAddComponent } from './add/outlets.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { OutletsDetailComponent } from './detail/outlets.detail.component';
import { OutletsRoutingModule } from './outlets-routing.module';
import { OutletsEditComponent } from './edit/outlets.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  imports: [
    CommonModule,
    OutletsRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    FormBuilderTableModule,
    ReactiveFormsModule,
    NgbModule,
    BsComponentModule,
    CKEditorModule,
],
  
  declarations: [
    OutletsComponent, 
    OutletsAddComponent, 
    OutletsDetailComponent,
    OutletsEditComponent]
})
export class OutletsModule { }
