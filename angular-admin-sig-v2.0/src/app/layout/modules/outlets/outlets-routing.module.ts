import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OutletsComponent } from './outlets.component';
import { OutletsAddComponent } from './add/outlets.add.component';
import { OutletsDetailComponent } from './detail/outlets.detail.component';

const routes: Routes = [
  {
      path: '', component: OutletsComponent,

  },
  {
      path:'add', component: OutletsAddComponent
  },
  {
    path:'detail', component: OutletsDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OutletsRoutingModule { }
