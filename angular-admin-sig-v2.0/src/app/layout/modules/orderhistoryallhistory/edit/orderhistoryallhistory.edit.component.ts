import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { isFunction } from '../../../../object-interface/common.function';
import { NgxBarcodeModule } from 'ngx-barcode';
import { ActivatedRoute, Router } from '@angular/router';
import { MemberService } from '../../../../services/member/member.service';
import Swal from 'sweetalert2';
import { ProgramNameService } from '../../../../services/program-name.service';
import * as content from '../../../../../assets/json/content.json';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
	selector: 'app-orderhistoryallhistory-edit',
	templateUrl: './orderhistoryallhistory.edit.component.html',
	styleUrls: [ './orderhistoryallhistory.edit.component.scss' ],
	animations: [ routerTransition() ]
})
export class OrderhistoryallhistoryEditComponent implements OnInit {
	@Input() public detail: any;
	@Input() public back;

	public contentList : any = (content as any).default;

    programType:any = "";
	orderHistoryDetail;
	calculate:any;
	otherCourValue: any;
	detailPage;
	awb_check = false;
	infoDetail = [];
	moreOrderDetail: any;
	openMoreDetail = false;
	urlReceipt: any;
	public loading: boolean = false;
	urlLabel: any;
	editThis = false;
	public done: boolean = false;
	contentProgram : any = (content as any).default;
	public notesCategory: any = [
		{
			label: 'Product out of stock',
			value: false
		},
		{
			label: 'Color options are not available',
			value: false
		},
		{
			label: 'Product size is not available',
			value: false
		},
		{
			label: 'Other',
			value: false
		}
	];
	paramAppLabel: any;
	categoryNotes: any;
	public shippingStatusList: any = [
		{
			label: 'on checking and processing',
			value: 'on_processing',
			id: 'on-process',
			checked: true,
			hovered: false,
			values: 'on_processing'
		},
		{
			label: 'Warehouse Packaging',
			value: 'on_packaging',
			id: 'warehouse-packaging',
			hovered: false,
			values: 'on_packaging'
		},
		{
			label: 'On Delivery process',
			value: 'on_delivery',
			id: 'on-delivery',
			hovered: false,
			values: 'on_delivery'
		},
		{
			label: 'Delivered',
			value: 'delivered',
			id: 'delivered',
			hovered: false,
			values: 'delivered'
		}
	];

	packaging = false;

	statusValue: any;
	statusLabel: any;

	public transactionStatus: any = [
		{ label: 'PAID', value: 'PAID' },
		{ label: 'CANCEL', value: 'CANCEL' },
		{ label: 'WAITING FOR CONFIRMATION', value: 'WAITING FOR CONFIRMATION' }
	];
	service_courier: any = [
		{ label: 'Drop', value: 'DROP'},
		{ label: 'Pickup', value: 'PICKUP'}
	]
	errorLabel: any = false;
	transaction_status: any;
	merchantMode = false;
	sum_total: any;
	currentPermission;
	info = [];
	change = false;
	notes: any;
	isClicked = false;
	isClicked2 = false;
	courier_list = [];
	tempstatus = '';
	openNotes = false;
	editAwb = false;
	openNotes2: boolean;
	holdvalue: any;
	infoDetailLabel: any;
	othercour = false;
	choose_service: any;
	supported= false;
	method_delivery :any;
	// detailToLowerCase: string = "";

	constructor(
		public orderhistoryService: OrderhistoryService,
		private route: ActivatedRoute,
		private router: Router,
		private programNameService: ProgramNameService,
		// public memberService: MemberService
		private activatedRoute: ActivatedRoute,
		private modalService: NgbModal
	) {}

	ngOnInit() {
		this.firstLoad();
	}

	async firstLoad() {
		const program = localStorage.getItem('programName');
		this.paramAppLabel = this.activatedRoute.snapshot.queryParamMap.get('app_label');

		if(!this.paramAppLabel) this.paramAppLabel = localStorage.getItem('programName');

		let _this = this;
		this.contentList.filter(function(element) {
			if(element.appLabel == program) {
				if(element.type == "reguler") {
					_this.programType = "reguler";
				} else if(element.type == "custom_kontraktual") {
				  _this.programType = "custom_kontraktual";
				} else {
					_this.programType = "custom";
				}
			}
		});

		this.currentPermission = 'admin';
		this.orderHistoryDetail = this.detail;
			
		// console.log('history detail', this.orderHistoryDetail);
	}

	isArray(obj : any ) {
		return Array.isArray(obj)
	 }

	openScrollableContent(longContent) {
		this.modalService.open(longContent, { centered: true });
	}

	getDetailProgram(value) {
        let detailProgram = this.contentProgram.find(element => element.appLabel == value);
        return detailProgram && detailProgram.title ? detailProgram.title : "";
    }

	replaceVarian(value) {
		return JSON.stringify(value).replace('{','').replace('}','').replace(/[',]+/g, ', ').replace(/['"]+/g, '')
	}

	backToDetail() {
		const programName = localStorage.getItem('programName');
		
        if(programName && programName != this.paramAppLabel) {
            Swal.fire({
                text: `Tab lain sedang mengakses program ${this.getDetailProgram(programName)}.`,
                icon: 'warning',
                confirmButtonText: `Tetap di ${this.getDetailProgram(this.paramAppLabel)}`,
                cancelButtonText: `Pindah ke ${this.getDetailProgram(programName)}`,
                cancelButtonColor:'#3086d6',
                showCancelButton: true,
            }).then((result) => {
                if(result.isConfirmed){
                    this.programNameService.setData(this.paramAppLabel);
					this.back[1](this.back[0]);
                } else {
                    this.programNameService.setData(programName);
					this.back[1](this.back[0]);
                }
            });
        } else if(this.paramAppLabel && !programName) {
			this.programNameService.setData(this.paramAppLabel);
			this.back[1](this.back[0]);
		} else {
			this.back[1](this.back[0]);
		}
		// this.router.navigate(["administrator/orderhistoryallhistoryadmin"]);
	}

	async cancelOrder(){
		if(!this.detail.order_id) return;
		Swal.fire({
			title: 'Confirmation',
			text: 'Apakah anda yakin ingin membatalkan order : '+this.detail.order_id,
			icon: 'error',
			confirmButtonText: 'Ok',
			cancelButtonText:"cancel",
			showCancelButton:true
		  }).then(async (result) => {
			if(result.isConfirmed){
				const payload = {
					order_id:this.detail.order_id
				}
				try {
					const result = await this.orderhistoryService.cancelOrder(payload);
					Swal.fire({
						title:"Cancel",
						text: 'Order : '+this.detail.order_id+' telah dibatalkan',
						icon: 'error',
						confirmButtonText: 'Ok',
						showCancelButton:false,
					}).then((result) => {
						if(result.isConfirmed){
							this.backToDetail();
					}});
				} catch (e) {
					this.errorLabel = ((<Error>e).message);

					let message = this.errorLabel;
					if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
						message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
					}
					Swal.fire({
						icon: 'error',
						title: message,
					});
				}
			}
		  });
	}

	async processOrder(){
		if(!this.detail.order_id) return;
		Swal.fire({
			title: 'Confirmation',
			text: 'Apakah anda yakin ingin memproses order : '+this.detail.order_id,
			icon: 'success',
			confirmButtonText: 'process',
			cancelButtonText:"cancel",
			showCancelButton:true
		  }).then(async (result) => {
			if(result.isConfirmed){
				const payload = {
					order_id:this.detail.order_id
				}
				try {
					const result = await this.orderhistoryService.processOrder(payload);
					Swal.fire({
						title:"Success",
						text: 'Order : '+this.detail.order_id+ ' telah disetujui',
						icon: 'success',
						confirmButtonText: 'Ok',
						showCancelButton:false,
					}).then((result) => {
						if(result.isConfirmed){
							this.backToDetail();
					}});
				} catch (e) {
					this.errorLabel = ((<Error>e).message);

					let message = this.errorLabel;
					if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
						message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
					}
					Swal.fire({
						icon: 'error',
						title: message,
					});
				}
			}
		});
	}


}
