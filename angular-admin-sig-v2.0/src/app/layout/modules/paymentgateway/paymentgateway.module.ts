import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentGatewayRoutingModule } from './paymentgateway-routing.module';
import { PaymentGatewayComponent } from './paymentgateway.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { PaymentGatewayAddComponent } from './add/paymentgateway.add.component';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormInputComponent } from '../../../component-libs/form-input/form-input.component';
import { PaymentGatewayDetailComponent } from './detail/paymentgateway.detail.component';
import { PaymentGatewayEditComponent } from './edit/paymentgateway.edit.component';
// import { NgxEditorModule } from 'ngx-editor';


@NgModule({
  imports: [
    CommonModule, 
    PaymentGatewayRoutingModule, 
    PageHeaderModule, 
    FormsModule, 
    FormBuilderTableModule, 
    ReactiveFormsModule,
    // NgxEditorModule,
    NgbModule,
  ],
  declarations: [PaymentGatewayComponent, PaymentGatewayAddComponent, PaymentGatewayDetailComponent, PaymentGatewayEditComponent]
})
export class PaymentGatewayModule { }
