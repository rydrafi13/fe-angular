import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PaymentGatewayService } from '../../../../services/paymentgateway/paymentgateway.service';


@Component({
  selector: 'app-payment-gateway-edit',
  templateUrl: './paymentgateway.edit.component.html',
  styleUrls: ['./paymentgateway.edit.component.scss'],
  animations: [routerTransition()]
})



export class PaymentGatewayEditComponent implements OnInit {
  @Input() public detail:any;
  @Input() public back;

  public loading        :boolean = false;
  errorLabel    :any = false;

  constructor(public paymentGatewayService:PaymentGatewayService) {}

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){

      this.detail.previous_id = await this.detail._id;
  }

  backToDetail(){
   
    this.back[0][this.back[1]]();
  }
  
  async saveThis(){
   
    try 
    {
      this.loading=!this.loading;
      await this.paymentGatewayService.updatePaymentGateway(this.detail);
      this.loading=!this.loading;
  
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
}
