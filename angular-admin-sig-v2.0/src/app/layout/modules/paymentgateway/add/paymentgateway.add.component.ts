import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PaymentGatewayService } from '../../../../services/paymentgateway/paymentgateway.service';

@Component({
  selector: 'app-payment-gateway-add',
  templateUrl: './paymentgateway.add.component.html',
  styleUrls: ['./paymentgateway.add.component.scss'],
  animations: [routerTransition()]
})

export class PaymentGatewayAddComponent implements OnInit {
  public name:string = "";
  PaymentGateway:any = [];
  service:any;
  public product_type = [
    {label:"ACTIVE", value:"ACTIVE", selected:1}
   ,{label:"INACTIVE", value:"INACTIVE"}
 ];
 selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     :any = false;
  errorLabel    :any = false;
  alias:any;
  logo:any;

  
  constructor(public paymentGatewayService:PaymentGatewayService) {
    let form_add     :any = [
      { label:"name",  type: "text",  value: "", data_binding: 'name'  },
      { label:"alias",  type: "text",  value: "", data_binding: 'alias'  },
      { label:"service",  type: "text",  value: "", data_binding: 'service' },
      { label:"logo",  type: "text",  value: "", data_binding: 'logo' },
    ];
   }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result:any = await this.memberService.getMembersLint();
    // this.Members = result.result;
    
  }

  async formSubmitAddPaymentGateway(form){
    //console.log(JSON.stringify(form));
    
    try 
    {
      this.service    = this.paymentGatewayService;
      //console.log(form);
      let result:any  = await this.paymentGatewayService.addPaymentGatewayLint(form);
      //console.log(result);
      this.PaymentGateway = result.result;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }

}
