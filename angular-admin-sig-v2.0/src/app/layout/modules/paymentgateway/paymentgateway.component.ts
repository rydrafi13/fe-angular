
import { Component, OnInit }  from '@angular/core';
import { routerTransition }   from '../../../router.animations';
import { PaymentGatewayService }     from '../../../services/paymentgateway/paymentgateway.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector    : 'app-payment-gateway',
  templateUrl : './paymentgateway.component.html',
  styleUrls   : ['./paymentgateway.component.scss'],
  animations  : [routerTransition()]
})

export class PaymentGatewayComponent implements OnInit {

  PaymentGateway      :any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Payment Gateway Detail',
                                  label_headers   : [
                                    {label: 'Name', visible: true, type: 'string', data_row_name: 'name'},
                                    {label: 'Alias', visible: true, type: 'string', data_row_name: 'alias'},
                                      {label: 'Service', visible: true, type: 'string', data_row_name: 'service'},
                                      // {label: 'Logo', visible: false, type: 'string', data_row_name: 'logo'}
                                      ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'PaymentGateway',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;

  paymentgatewayDetail    :any = false;
  service       :any;
  selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     :any = false;
  constructor(public paymentGatewayService:PaymentGatewayService) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){
    try{
      let result:any;
      this.service    = this.paymentGatewayService;
      result          = await this.paymentGatewayService.getPaymentGatewayLint();
      this.PaymentGateway   = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async callDetail(_id)
  {
    try{
      let result:any;
      this.service    = this.paymentGatewayService;
      result          = await this.paymentGatewayService.detailPaymentGateway(_id);
      if(!result.error && result.error == false){
        this.paymentgatewayDetail = result.result[0];
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.paymentgatewayDetail = false;
    // delete obj.prodDetail;
    
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }
}
