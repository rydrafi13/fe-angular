import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PaymentGatewayService } from '../../../../services/paymentgateway/paymentgateway.service';
import { del } from 'selenium-webdriver/http';


@Component({
  selector: 'app-payment-gateway-detail',
  templateUrl: './paymentgateway.detail.component.html',
  styleUrls: ['./paymentgateway.detail.component.scss'],
  animations: [routerTransition()]
})

export class PaymentGatewayDetailComponent implements OnInit {
  @Input() public detail:any;
  @Input() public back;

  edit:boolean = false;
  errorLabel    :any = false;
  constructor(public paymentGatewayService:PaymentGatewayService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    this.edit = !this.edit;
  }

  backToTable(){
    this.back[1](this.back[0]);
  }

  async deleteThis(){
    try{
      let delResult:any = await this.paymentGatewayService.deletePaymentGateway(this.detail);
      console.log(delResult);
      if(delResult.error==false){
          //console.log(this.back[0]);
          this.back[0].paymentgatewayDetail=false;
          this.back[0].firstLoad();
          // delete this.back[0].prodDetail;
      } 
    } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
    }      
    
  }
  
}
