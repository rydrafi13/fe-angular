import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReporterrorComponent } from './reporterror.component';


const routes: Routes = [
  {
      path: '', component: ReporterrorComponent
  }
  // {
  //   path:'detail', component: OrderhistoryDetailComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReporterrorRoutingModule { }
