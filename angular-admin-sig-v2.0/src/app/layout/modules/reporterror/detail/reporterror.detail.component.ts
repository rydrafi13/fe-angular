import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ReporterrorService } from '../../../../services/reporterror/reporterror.service';

@Component({
  selector: 'app-reporterror-detail',
  templateUrl: './reporterror.detail.component.html',
  styleUrls: ['./reporterror.detail.component.scss'],
  animations: [routerTransition()]
})

export class ReporterrorDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  
  edit = false;
  

  constructor(public reporterrorService: ReporterrorService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {


  }

  editThis() {
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable() {
    console.log(this.back);
    this.back[1](this.back[0]);
  }

}
