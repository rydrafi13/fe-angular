// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-reporterror',
//   templateUrl: './reporterror.component.html',
//   styleUrls: ['./reporterror.component.scss']
// })
// export class ReporterrorComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { ReporterrorService } from '../../../services/reporterror/reporterror.service';
import { NumberFormatStyle } from '@angular/common';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-reporterror',
  templateUrl: './reporterror.component.html',
  styleUrls: ['./reporterror.component.scss'],
  animations: [routerTransition()]
})

export class ReporterrorComponent implements OnInit {
  Reporterror: any = [];
  tableFormat: TableFormat = {
                                  title           : 'Error Report Detail Page',
                                  label_headers   : [
                                    {label: 'REQ ID', visible: true, type: 'string', data_row_name: 'req_id'},
                                    {label: 'REQ IP Address', visible: true, type: 'string', data_row_name: 'req_ip_address'},
                                    {label: 'Log Type', visible: true, type: 'string', data_row_name: 'log_type'},
                                    {label: 'Message', visible: true, type: 'string', data_row_name: 'msg'},
                                    {label: 'Error Message', visible: true, type: 'string', data_row_name: 'error'},
                                    {label: 'Time', visible: true, type: 'date', data_row_name: 'time'},
                                  ],
                                  row_primary_key : 'req_id',
                                  formOptions     : {
                                                    row_id: 'req_id',
                                                    this  : this,
                                                    result_var_name: 'Reporterror',
                                                    detail_function: [this, 'callDetail']
                                                  },
                                                  show_checkbox_options: true
  };
  form_input: any = false;
  errorLabel: any = false;
  totalPage: 0;

  reporterrorDetail: any = false;
  service: any;

  constructor(public reporterrorService: ReporterrorService) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    try {
      this.service    = this.reporterrorService;
      let result: any  = await this.reporterrorService.getReporterrorLint();
      this.totalPage = result.result.total_page;

      this.Reporterror = result.result.values;
    } catch (e) {
      console.log("ERROR",e);
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  public async callDetail(orderhistory_id) {
    try {
      let result: any;
      console.log("call Detail", orderhistory_id)
      this.service    = this.reporterrorService;
      result          = await this.reporterrorService.detailReporterror(orderhistory_id);
      console.log(result);

      this.reporterrorDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.reporterrorDetail = false;
  }

}

