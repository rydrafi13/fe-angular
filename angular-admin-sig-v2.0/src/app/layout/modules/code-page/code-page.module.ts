import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CodePageComponent } from './code-page.component';
import { CodePageRoutingModule } from './code-page.routing';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { Ng2CompleterModule } from 'ng2-completer';
import {CodepageDetailComponent} from './detail/codepage.detail.component';
import {CodepageEditComponent} from './edit/codepage.edit.component';

@NgModule({
  declarations: [CodePageComponent, CodepageDetailComponent, CodepageEditComponent],
  // imports: [CommonModule, FormRoutingModule, PageHeaderModule],
  imports: [
    CommonModule, 
    CodePageRoutingModule, 
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    Ng2CompleterModule
  ]
})
export class CodePageModule { }
