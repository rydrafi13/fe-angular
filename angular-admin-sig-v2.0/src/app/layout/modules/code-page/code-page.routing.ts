import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CodePageComponent } from './code-page.component';

const routes: Routes = [
    {
        path: '', component: CodePageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CodePageRoutingModule {
}
