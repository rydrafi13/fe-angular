import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PromodealsService } from '../../../../services/promodeals/promodeals.service';

@Component({
    selector: 'app-promodeals-edit',
  templateUrl: './promodeals.edit.component.html',
  styleUrls: ['./promodeals.edit.component.scss'],
  animations: [routerTransition()]
})

export class PromodealsEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading = false;
  public done = false;
  public categoryProduct : any = [];
  category : any = false;
  errorLabel: any = false;
  product_status: any;


  constructor(public PromodealsService: PromodealsService) {}

  ngOnInit() {
    this.firstLoad();
    this.getCategory();
      
  }

  async getCategory() {
    this.category = await this.PromodealsService.getCategoryLint();
    this.category.result.forEach((element) => {
      this.categoryProduct.push({label:element.name, value:element.name});
    });
  }

  async firstLoad() {

    try {
      this.getCategory();
      this.detail.previous_product_code = await this.detail.product_code;
      
      
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }

  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {

    try {
      this.loading = !this.loading;
      if(this.detail.discount > 1){
        alert("Discount must between 0 and 1")
        this.loading = false;
      }
      else{
        if(this.detail.type == null){
          this.detail.type = "";
        }
        if(this.detail.discount == null){
          this.detail.discount = "0";
        }
        let product_variabel = []
        // this.detail.products.forEach((obj, index) => {
        //   product_variabel.push( obj.$oid )
        // })
  
        // this.detail.products = product_variabel
        this.detail.fixed_value = Number(this.detail.fixed_value)
        await this.PromodealsService.promoUpdate(this.detail);
        this.loading = !this.loading;
        this.done = true;
        alert("Data Has Been Updated")
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

}
