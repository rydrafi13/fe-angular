import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromodealsComponent } from './promodeals.component';
import { PromodealsRoutingModule } from './promodeals.routing';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { Ng2CompleterModule } from 'ng2-completer';
import {PromodealsDetailComponent} from './detail/promodeals.detail.component';
import {PromodealsEditComponent} from './edit/promodeals.edit.component';
import { PromodealsGenerateComponent } from './generate/promodeals.generate.component';
// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
@NgModule({
  declarations: [PromodealsComponent, PromodealsDetailComponent, PromodealsEditComponent, PromodealsGenerateComponent],
  // imports: [CommonModule, FormRoutingModule, PageHeaderModule],
  imports: [
    // NgMultiSelectDropDownModule.forRoot(),
    CommonModule,
    PromodealsRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    Ng2CompleterModule
  ]
})
export class PromodealsModule { }
