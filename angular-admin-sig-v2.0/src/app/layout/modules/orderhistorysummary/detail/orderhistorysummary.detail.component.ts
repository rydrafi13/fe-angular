import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';

@Component({
  selector: 'app-orderhistorysummary-detail',
  templateUrl: './orderhistorysummary.detail.component.html',
  styleUrls: ['./orderhistorysummary.detail.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistorysummaryDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit:boolean = false;
  
  constructor(public orderhistoryService:OrderhistoryService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    if(this.detail.billings){
      let billings = this.detail.billings
      let additionalBillings=[];
      if(billings.discount){
        additionalBillings.push({
          product_name: billings.discount.product_name,
          value       : billings.discount.value
        })
      }
      console.log("billings", billings)
      this.detail.additionalBillings = additionalBillings
    }
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

}
