import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderDetailComponent } from './order-detail.component';
// import { OrderHistoriesAddComponent } from './add/order-histories.add.component';
// import { OrderHistoriesDetailComponent } from './detail/order-histories.detail.component';

const routes: Routes = [
  {
      path: '', component: OrderDetailComponent,

  },
//   {
//       path:'add', component: OrderHistoriesAddComponent
//   },
//   {
//     path:'detail', component: OrderHistoriesDetailComponent
// },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderDetailRoutingModule { }
