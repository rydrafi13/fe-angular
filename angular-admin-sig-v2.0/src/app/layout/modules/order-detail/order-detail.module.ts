import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderDetailComponent } from './order-detail.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { OrderDetailRoutingModule } from './order-detail-routing.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistoryallhistoryEditModule } from '../orderhistoryallhistory/edit/orderhistoryallhistory.edit.module'
// import { OrderDetailAddComponent } from './add/order-histories.add.component';
// import { OrderDetailDetailComponent } from './detail/order-histories.detail.component';
// import { OrderDetailEditComponent } from './edit/order-histories.edit.component';

@NgModule({
  imports: [CommonModule,
    OrderDetailRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    OrderhistoryallhistoryEditModule
  ],
  
  declarations: [
    OrderDetailComponent,
  ],

  // exports : [OrderDetailComponent]
})
export class OrderDetailModule { }
