import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataMemberComponent } from './data-member.component';
import { DataMemberAddComponent } from './add/data-member.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { DataMemberRoutingModule } from './data-member-routing.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { DataMemberDetailComponent } from './detail/data-member.detail.component';
import { DataMemberEditComponent } from './edit/data-member.edit.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ShowPasswordComponent } from './show-password/show-password.component';

@NgModule({
  imports: [CommonModule,
    DataMemberRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
  ],
  
  declarations: [
    DataMemberComponent, 
    DataMemberAddComponent, 
    DataMemberDetailComponent, 
    DataMemberEditComponent,
    ChangePasswordComponent,
    ShowPasswordComponent
  ],

  // exports : [MemberComponent]
})
export class DataMemberModule { }
