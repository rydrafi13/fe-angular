import { ProductService } from '../../../../services/product/product.service';
import { Component, OnInit, Input, NgZone } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import Swal from 'sweetalert2';
import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';


@Component({
  selector: 'app-show-password',
  templateUrl: './show-password.component.html',
  styleUrls: ['./show-password.component.scss'],
  animations: [routerTransition()]
})

export class ShowPasswordComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  member_status:any;
  activation_status:any;

  private updateSingle: any;
  
  public loading        : boolean = false;

  public activationStatus: any = [
    {label: 'ACTIVATED', value: 'ACTIVATED'},
    {label: 'INACTIVED', value: 'INACTIVED'}
  ];

  public memberStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  errorFile: any = false;
  selectedFile: any;
  selFile: any;

  constructor(public memberService: MemberService, public productService:ProductService, private zone: NgZone) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  assignArrayBoolean(curVar : any, varCont : any){
    if(this.isArray(curVar)){
      curVar.forEach(element => {
        varCont.push(false);
      });
    }
  }

  async firstLoad() {
    this.updateSingle = JSON.parse(JSON.stringify(this.detail));
    // console.warn("this detail???", this.detail)
    this.detail.previous_member_id = this.detail.member_id;
  }

  backToDetail() {
    this.back[0][this.back[1]]();
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  openUpload(type:any, index:any){
    let upFile : any = document.getElementById(type+index);
    upFile.click();
  }

  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    
    return retDate[0];
  }

}
