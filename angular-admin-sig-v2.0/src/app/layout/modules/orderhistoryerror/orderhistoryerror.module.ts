import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistoryerrorRoutingModule } from './orderhistoryerror-routing.module';
import { OrderhistoryerrorComponent } from './orderhistoryerror.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistoryerrorDetailComponent } from './detail/orderhistoryerror.detail.component';
import { OrderhistoryerrorEditComponent } from './edit/orderhistoryerror.edit.component';

@NgModule({
  imports: [
    CommonModule, 
    OrderhistoryerrorRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
  declarations: [OrderhistoryerrorComponent,OrderhistoryerrorDetailComponent,OrderhistoryerrorEditComponent]
})
export class OrderhistoryerrorModule { }
