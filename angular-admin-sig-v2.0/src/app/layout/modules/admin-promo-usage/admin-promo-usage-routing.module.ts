import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPromoUsageComponent } from './admin-promo-usage.component'

const routes: Routes = [
  {
    path: '', component: AdminPromoUsageComponent,

}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPromoUsageRoutingModule { }
