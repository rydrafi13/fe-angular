import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PointsGroupComponent } from './points-group.component';
import { PointsGroupAddComponent } from './add/points-group.add.component';
import { PointsGroupDetailComponent } from './detail/points-group.detail.component';

const routes: Routes = [
  {
      path: '', component: PointsGroupComponent,

  },
  {
      path:'add', component: PointsGroupAddComponent
  },
  {
    path:'detail', component: PointsGroupDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PointsGroupRoutingModule { }
