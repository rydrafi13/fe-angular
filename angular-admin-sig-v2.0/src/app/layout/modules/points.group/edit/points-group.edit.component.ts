import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PointsGroupService } from '../../../../services/points.group/pointsgroup.service';


@Component({
  selector: 'app-points-group-edit',
  templateUrl: './points-group.edit.component.html',
  styleUrls: ['./points-group.edit.component.scss'],
  animations: [routerTransition()]
})

export class PointsGroupEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  org_id:any;
  campaign_name:any;
  campaign_id:any;
  group_name:any;
  group_code:any;
  description:any;
  status:any;
  pts_model_id:any;
  start_date:any;
  end_date:any;



  public loading        : boolean = false;
  public pointModels : any = [];
  public groupStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  PointsModels      : any = [];
  table_title   : any = "Point Models List";
  table_headers : any = ["Point Model Name", 'Start Date', 'End Date'];
  table_rows    : any = ["rule_description", 'start_date', 'end_date'];
  row_id        : any = "_id";
  formOptions   : any = {
                      row_id    : this.row_id,
                      this      : this,
                      result_var_name : 'PointsModels'
                    }
  service     : any ;

  pointmodels : any = false;

  constructor(public pointsgroupService: PointsGroupService) {
  }

  ngOnInit() {
    this.getPointModels();
    this.firstLoad();

  }

  async getPointModels(){
    try{
      this.pointmodels = await this.pointsgroupService.getPointModels();
      this.pointmodels.result.forEach((element) => {
        this.pointModels.push({label:element.rule_description, value:element.id});
      });
      console.log('point models : ', this.pointModels);
    }
    catch (e){
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  async firstLoad() {

    this.detail.previous_member_id = this.detail.member_id;
    this.groupStatus.forEach((element, index) => {
        if (element.value == this.detail.member_status) {
            this.groupStatus[index].selected = 1;
        }
    });

    try {
      
      this.service    = this.pointsgroupService;
      const result: any  = await this.pointsgroupService.getPointsModelsLint(this.detail);
      this.PointsModels = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    try {
      this.loading = !this.loading;
      await this.pointsgroupService.updatePointsGroupID(this.detail);
      this.loading = !this.loading;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

}
