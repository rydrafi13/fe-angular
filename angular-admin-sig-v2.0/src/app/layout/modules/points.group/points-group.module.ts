import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PointsGroupComponent } from './points-group.component';
import { PointsGroupAddComponent } from './add/points-group.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { PointsGroupDetailComponent } from './detail/points-group.detail.component';
import { PointsGroupRoutingModule } from './points-group-routing.module';
import { PointsGroupEditComponent } from './edit/points-group.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  imports: [CommonModule,
    PointsGroupRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
],
  
  declarations: [
    PointsGroupComponent, PointsGroupAddComponent, PointsGroupDetailComponent,PointsGroupEditComponent]
})
export class PointsGroupModule { }
