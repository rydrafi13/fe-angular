import { PackagebundleService } from '../../../../services/packagebundle/packagebundle.service';
import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

@Component({
    selector: 'app-Packagebundle-edit',
  templateUrl: './packagebundle.edit.component.html',
  styleUrls: ['./packagebundle.edit.component.scss'],
  animations: [routerTransition()]
})

export class PackagebundleEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading = false;
  public done = false;
  public categoryProduct : any = [];
  category : any = false;
  errorLabel: any = false;
  product_status: any;


  constructor(public PackagebundleService:PackagebundleService) {}

  ngOnInit() {
    this.firstLoad();
    this.getCategory();
      
  }

  async getCategory() {
    this.category = await this.PackagebundleService.getCategoryLint();
    this.category.result.forEach((element) => {
      this.categoryProduct.push({label:element.name, value:element.name});
    });
  }

  async firstLoad() {

    try {
      this.getCategory();
      this.detail.previous_product_code = await this.detail.product_code;
      
      
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });

  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {

    try {
      this.loading = !this.loading;
      if(this.detail.type == null){
        this.detail.type = "";
      }
      if(this.detail.discount == ""){
        this.detail.discount = "0";
      }
      if(this.detail.fixed_value == null){
        this.detail.fixed_value = 0;
      }
      if(this.detail.title == null){
        this.detail.title = "";
      }
      if(this.detail.description == null){
        this.detail.description = "";
      }
      let product_variabel = []
      this.detail.products.forEach((obj, index) => {
        product_variabel.push( obj.$oid )
      })

      this.detail.products = product_variabel
      this.detail.fixed_value = Number(this.detail.fixed_value)
      await this.PackagebundleService.promoUpdate(this.detail);
      this.loading = !this.loading;
      this.done = true;
      alert("Edit submitted")
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

}
