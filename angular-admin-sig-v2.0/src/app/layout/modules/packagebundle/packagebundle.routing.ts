import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PackagebundleComponent } from './packagebundle.component';
import { PackagebundleGenerateComponent } from './generate/packagebundle.generate.component';

const routes: Routes = [
    { path: '', component: PackagebundleComponent },
    { path: 'generate', component: PackagebundleGenerateComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PackagebundleRoutingModule {
}
