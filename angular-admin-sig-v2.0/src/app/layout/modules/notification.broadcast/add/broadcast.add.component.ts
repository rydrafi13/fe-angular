import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { BroadcastService } from '../../../../services/notification.broadcast/broadcast.service';
import { ProductService } from '../../../../services/product/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-broadcast-add',
  templateUrl: './broadcast.add.component.html',
  styleUrls: ['./broadcast.add.component.scss'],
  animations: [routerTransition()]
})

export class BroadcastAddComponent implements OnInit {
  public name: string = "";
  Broadcast: any = [];
  service: any;
  org_id:any;
  setting_id:any;
  group_id:any;
  template_id:any;
  status:any;
  
  
  public settingBroadcast  : any = [];
  public groupBroadcast  : any = [];
  public templateBroadcast  : any = [];
  public broadcastStatus  : any = [
    {label:"ACTIVE", value:"ACTIVE"}
   ,{label:"INACTIVE", value:"INACTIVE"}
  ];
 
 selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     : any = false;
  errorLabel    : any = false;
  setting       : any = false;
  group         : any = false;
  template      : any = false;
  selFile;
  cta_list:any;
  activeCheckbox1 = false
  activeCheckbox2 = false
  form   : any = {
    key: '',
    inbox_title: '',
    inbox_message: '',
    banner : '',
    notification_title: '',
    notification_message: '',
    inbox_image : [],
    notification_image: '',
    from: 'LOCARD',
    icon: 'https://res.cloudinary.com/dqcj36zfd/image/upload/v1604047373/admin/image/icon.5f9bd20b43219.png',
    expiry_day: 0,
    cta_type:1,
    reference:{
        product_id:'',
        order_id:'',
        voucher_id:'',
        booking_id: '',
    },
    logo: 'https://res.cloudinary.com/dqcj36zfd/image/upload/v1604047373/admin/image/icon.5f9bd20b43219.png',
    notification_type: 'TOKEN',
    member_list: [],
    option:2,
  }

  public type_cta: any = [
    { label: 'Go to Invoice Detail', value: 1 ,selected:1},
    { label: 'See Voucher Detail', value: 2 },
    { label: 'See Redeem Detail', value: 3 },
    { label: 'Go to My Voucher', value: 4 },
    { label: 'Go to Shopping Cart', value: 5 },
    { label: 'Go to Discovery Deals', value: 6 },
    { label: 'Go to Discovery product', value: 7 },
    { label: 'Go to Discovery', value: 8 },
    { label: 'Go to Wishlist', value: 9 },
    { label: 'Go to Home', value: 10 },
    { label: 'See Vouchers', value: 11 },
    { label: 'Go to Transaction History Detail', value: 12 },
    { label: 'Go to Payment confirmation on invoice detail', value: 13 },
    { label: 'Go to Product Detail', value: 14 },
    { label: 'Go to Search Result', value: 15 },
  ];
  public type_option: any = [
    { label: 'OPT 1', value: 1 },
    { label: 'OPT 2', value: 2 ,selected:1},
    { label: 'OPT 3', value: 3 },
    { label: 'OPT 4', value: 4 },
    { label: 'OPT 5', value: 5 },
  ];
  public type_notif: any = [
    { label: 'Broadcast All', value: 'TOPIC' },
    { label: 'Broadcast to specific Users', value: 'TOKEN',selected:1 },
  
  ];
  showLoading1: boolean = false;
	showLoading2: boolean = false;
	showLoading3: boolean = false;
	showLoading4: boolean = false;
	showLoading5: boolean = false;
  uploads: any = {
		banner: '',
		inbox: '',
		notif: '',
		logo: 'https://res.cloudinary.com/dqcj36zfd/image/upload/v1604047373/admin/image/icon.5f9bd20b43219.png',
		icon: 'https://res.cloudinary.com/dqcj36zfd/image/upload/v1604047373/admin/image/icon.5f9bd20b43219.png',
	};
  
  constructor(public broadcastService:BroadcastService,
    private productService: ProductService) {
  
    
   }

  ngOnInit() {
    this.getCollection();
    this.firstLoad();
  }

  async getCollection(){
    try{

      let result = await this.broadcastService.getCTAList();
      if(result.result.cta_list){
        console.log('result', result.result.cta_list)
        this.type_cta = []
        result.result.cta_list.forEach(element => {
          if(element.type == 1){
            this.type_cta.push({label:element.text, value: element.type,selected: 1})
          }else{
          this.type_cta.push({label:element.text, value: element.type})
          }
        });
      }
     

      console.log("type", result)
    }
    catch (e){
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  firstLoad(){
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;
    
  }
  
  addHashtags(newWord) {
    newWord = newWord.replace(/\s/g, '');
    this.form.member_list.push(newWord);
  }

  hashtagsKeyDown(event) {
    let n = event.target.textContent

    let key = event.key.toLowerCase();
    // console.log("key", key)
    if (key == ' ' || key == 'enter') {

      if (n.trim() !== '') {
        this.addHashtags(n);
      }
      event.target.textContent = ''
      event.preventDefault();
    }

    if (key == 'backspace' && n == '' && this.form.member_list.length > 0) {
      this.form.member_list.pop();
    }

  }
  stringToNumber(str: any) {
    if (typeof str == 'number') {
      str = str + '';
    }

    let s = str.toLowerCase().replace(/[^0-9]/g, '');

    return parseInt(s);
  }
  async formSubmitAddNotificationBroadcast(){
    this.errorLabel = false;
    Swal.fire({
			title: 'Do you want to broadcast the message?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Send It'
		}).then(async (result)  => {
			if (result.isConfirmed) {
        try 
        {
          this.form.inbox_image = []
          console.log('result : ',this.form);
          this.form.banner = this.uploads.banner
          this.form.logo = this.uploads.logo
          this.form.icon = this.uploads.icon
          this.form.inbox_image.push(this.uploads.inbox)
          this.form.notification_image = this.uploads.notif
          this.form.cta_type = this.stringToNumber(this.form.cta_type)
          this.form.expiry_day = this.stringToNumber(this.form.expiry_day)
          this.form.option = this.stringToNumber(this.form.option)
    
          // this.service    = this.broadcastService;
          let result: any  = await this.broadcastService.addNewBroadcastNotif(this.form);
          if( result.result){
            Swal.fire(
              'SUCCESS!',
              'Your notification has been sent',
              'success'
            )
          }
          if( result.error){
            this.errorLabel = result.error;
          }
        } 
        catch (e) 
        {
          this.errorLabel = ((<Error>e).message);//conversion to Error type
        }
			
				// this.loadImageToForm();
			}
		})
  
    
   
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }
  async onFileSelected2(event, img) {
		var reader = new FileReader()
		try {
			this.errorFile = false;
			let fileMaxSize = 3000000; // let say 3Mb
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 3MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}

			if(event.target.files[0]){
				reader.readAsDataURL(event.target.files[0]);
			  }
			if (this.selectedFile) {
				switch (img) {
					case 'banner':
						
						this.showLoading1 = true;
						console.log("ini1", img)
						break;
					case 'inbox':
						
						this.showLoading2 = true;
						console.log("ini2", img)
						break;
					case 'notif':
						
						this.showLoading3 = true;
						console.log("ini3", img)
						break;
					case 'logo':
					
						this.showLoading4 = true;
						console.log("ini4", img)
						break;
					case 'icon':
						
						this.showLoading5 = true;
						console.log("ini5", img)
						break;

				}
				
				const logo = await this.productService.upload(this.selectedFile, this, 'image', (result) => {
					console.log("hasil", result)
					this.callImage(result, img);
				});
			}
			
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
  }
  checkbox1() {
    console.log(this.activeCheckbox1)
    if(this.activeCheckbox1 = true) {
    this.uploads.inbox = this.uploads.banner
    }
  }
  checkbox2() {
    if(this.activeCheckbox2 = true) {
    this.uploads.notif = this.uploads.banner
    }
  }

  callImage(result, img) {
    if(img == 'banner'){
       this.uploads.banner = result.base_url + result.pic_big_path;
      if(this.activeCheckbox1 = true) {
        this.uploads.inbox = this.uploads.banner
      }
      if(this.activeCheckbox2 = true) {
        this.uploads.notif = this.uploads.banner
      }
    }else{
      this.uploads[img] = result.base_url + result.pic_big_path;
    }
    
    this.allfalse();
		
  }
  allfalse() {
		this.showLoading1 = this.showLoading2 = this.showLoading3
      = this.showLoading4 = this.showLoading5 =false
  }
}
