import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ControlValueAccessor } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ProductService } from '../../../../services/product/product.service';
import { MerchantService } from '../../../../services/merchant/merchant.service';
import { MemberService } from '../../../../services/member/member.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CourierServiceService } from '../../../../services/courier/courier.service';


@Component({
  selector: 'app-merchant-add',
  templateUrl: './merchant.add.component.html',
  styleUrls: ['./merchant.add.component.scss'],
  animations: [routerTransition()]
})

export class MerchantAddComponent implements OnInit {
  public name: string = "";
  @Input() public back;
  public categoryProduct: any = [];
  public productType: any = [];
  public merchant: any = [];
  public merchant_group: any = [];
  public available_group = [];
  courierServices: any = [];
  public FORM: any = {
    _id: "",
    merchant_name: "",
    merchant_username: "",
    address: "",
    mprovince: "",
    contact_person: "",
    description: "",
    email: "",
    postal_code: "",
    work_phone: "",
    region_code: "",
    status: "",
    image_owner_url: "",
    background_image: "",
    available_courier: []
  };

  public MEMBER: any = {
    full_name: "",
    email: "",
    password: "",
    cell_phone: "",
    dob: "",
    gender: "",
  };
  Products: any = [];
  service: any;
  public loading: boolean = false;
  public status = [
    { label: "ACTIVE", value: 1 },
    { label: "INACTIVE", value: 0 }];
  public gender = [
    { label: "MALE", value: 'male' },
    { label: "FEMALE", value: 'female' }];

  public editStatus = [
    { label: "ACTIVE", value: "ACTIVE" },
    { label: "INACTIVE", value: "INACTIVE" }
  ]

  provinceList;
  selectedFile = null;
  progressBar: number = 0;
  cancel: boolean = false;
  errorFile: any = false;
  errorLabel: any = false;
  category: any = false;
  base_url: any;
  pic_big_path: any;
  pic_medium_path: any;
  pic_small_path: any;
  pic_file_name: any;
  qty: any;
  owner_name: any;
  owner_id: any;
  hashtags: any;
  discount: any;
  point: any;
  price_after_discount: any;
  price: any;
  description: any;
  product_code: any;
  product_type: any;
  merchant_username: any;
  merchant_name: any;
  merchant_logo: any;
  background_image: any;
  image_owner_url: any;
  full_name: any;
  mcountry: any;
  mstate: any;
  mprovince: any;
  mpostal_code: any;
  maddress: any;
  contact_person: any;
  email_address: any;
  work_phone: any;
  handphone: any;
  email: any;
  dob: any;
  address: any;
  lastkeydown: number = 0;
  product_name = [];
  url: '';
  cell_phone;
  regionList = [];
  match;
  otherData: any;
  keyname: any = [];
  public key: any = [];
  setStatus: any;
  public Editor = ClassicEditor;
  public config = {
    toolbar: [ 'heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ],
    heading: {
        options: [
            { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
            { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
            { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
        ]
    }
}




  constructor(public merchantService: MerchantService, public memberService: MemberService, public courierService: CourierServiceService) { }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    // this.getKeyname();

    await this.getCourier();
    if(this.FORM.available_courier && this.FORM.available_courier.length){
      this.FORM.available_courier.forEach((value) =>{
        this.setCourierService(value);
      })
    }
  }


  async getCourier(){
    let result = await this.courierService.getCourierAll();
    console.log("check", result)
    this.courierServices = result.result;
    this.courierServices.forEach((el, i) => {
      Object.assign(el.courier, {value:false})
    });
  }

  setCourierService(value): boolean {
    for (let n in this.courierServices){
      if (value == this.courierServices[n]. courier_code){
        this.courierServices[n].value = true;
        return true;
      }
    }
  }

  getValueCheckbox(){
    let getData = [];
    this.courierServices.forEach((el, i) => {
      if (el.value == true){
        getData.push(el);
      }
    });
    return getData;
  }

  // async getKeyname() {
  //   this.keyname = await this.merchantService.configuration();
  //   this.keyname.result.merchant_group.forEach((element) => {
  //     this.merchant_group.push({ label: element.name, value: element.name })
  //   });
  //   this.FORM.merchant_group = this.merchant_group[0].value
  //   console.log(this.keyname)
  // }


  onRegionChanged() {
    console.log("ON Region Changed", this.FORM.region_name, this.regionList);
    this.regionList.forEach(element => {
      if (this.FORM.region_name.toLowerCase() == element.region_name.toLowerCase()) {
        this.FORM.region_code = element.region_code
        return
      }

    }
    );
    console.log(this.FORM.region_code)
  }

  getRegionList($event) {
    let result = $event.target.value

    if (result.length > 2)
      this.merchantService.getRegion(result, "origin").then((result) => {
        this.regionList = result.result
      })

  }

  async formSubmitAddMerchant() {

    let form = this.FORM;
    try {
      this.FORM.available_courier = this.getValueCheckbox();
      // this.background_image = "image";
      // this.image_owner_url = "picture";
      let resultMember: any = await this.memberService.addNewMember(this.MEMBER);
      console.log(resultMember)
      this.service = this.merchantService;
      this.FORM._id = resultMember.result._id["$oid"]
      this.FORM.email = this.MEMBER.email
      this.FORM.cell_phone = this.MEMBER.cell_phone
      delete this.FORM.merchant_group
      let result: any = await this.merchantService.addMerchant(this.FORM);
      if(result.result.status =='success'){
      alert("Merchant Added");
      }
      // window.location.href = "administrator/merchant";
      console.log(result);
      // this.Products = result.result;
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  onFileSelected(event) {
    try {
      this.errorFile = false;
      let fileMaxSize = 3000000;// let say 3Mb
      Array.from(event.target.files).forEach((file: any) => {
        if (file.size > fileMaxSize) {
          this.errorFile = "Maximum File Upload is 3MB";
        }
      });
      this.selectedFile = event.target.files[0];
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let _event: any = event;
        this.url = _event.target.result;
      }
    }
  }

  updateProgressBar(value) {
    this.progressBar = value;
  }

  cancelThis() {
    this.cancel = !this.cancel;
  }



  async onUploadLogo() {
    try {
      this.cancel = false;
      let result
      if (this.selectedFile) {
        console.log("result", result)
        result = await this.merchantService.upload(this.selectedFile, this, 'image', (result) => { this.callLogo(result) });
      }


    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  async onUploadBackground() {
    try {
      this.cancel = false;
      let result
      if (this.selectedFile) {
        console.log("result", result)
        result = await this.merchantService.upload(this.selectedFile, this, 'image', (result) => { this.callBackground(result) });
      }


    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }



  callLogo(result) {
    console.log("result upload logo ", result);
    console.log("This detail", this.FORM)
    this.FORM.image_owner_url = result.base_url + result.pic_big_path
    // this.detail = {...this.detail, ...result}
  }

  callBackground(result) {
    console.log("result upload background ", result);
    console.log("This detail", this.FORM)
    this.FORM.background_image = result.base_url + result.pic_big_path
    // this.detail = {...this.detail, ...result}
  }


 

  backTo() {
    // this.back[0][this.back[1]]();
    window.history.back();
  }



}
