import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent } from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';

import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';

import { FormInputComponent } from '../../../component-libs/form-input/form-input.component';
// import { NgxEditorModule } from 'ngx-editor';
import { MerchantRoutingModule } from './merchant-routing.module';
import { MerchantComponent } from './merchant.component';
import { MerchantAddComponent } from './add/merchant.add.component';
import { MerchantDetailComponent } from './detail/merchant.detail.component';
import { MerchantEditComponent } from './edit/merchant.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { MatCheckboxModule } from '@angular/material/checkbox';
// import { OpenMerchantComponent } from '../../../register/open-merchant/open-merchant.component';


@NgModule({
  imports: [
    CommonModule,
    MerchantRoutingModule,
    PageHeaderModule,
    FormsModule,
    FormBuilderTableModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    CKEditorModule,
    MatCheckboxModule,
  ],

  declarations: [
    MerchantComponent, 
    MerchantAddComponent, 
    MerchantDetailComponent, 
    MerchantEditComponent,
    // OpenMerchantComponent
  ],
  exports:[
    // OpenMerchantComponent
  ]
})
export class MerchantModule { }
