import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ControlValueAccessor } from '@angular/forms';
import { MerchantService } from '../../../../services/merchant/merchant.service';
import { setTime } from 'ngx-bootstrap/chronos/utils/date-setters';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
  selector: 'app-merchant-edit',
  templateUrl: './merchant.edit.component.html',
  styleUrls: ['./merchant.edit.component.scss'],
  animations: [routerTransition()]
})



export class MerchantEditComponent implements OnInit {
  selectedFile = null;
  @Input() public detail: any;
  @Input() public back;
  public loading        :boolean = false;
  public categoryProduct  : any = [];
 
  public editStatus: any = [
    { label: "ACTIVE", value: 1 }, 
    { label: "INACTIVE", value: 0 }
  ];

  errorLabel    : any = false;
  category      : any = false;
  product_status: any;
  cancel                  :boolean  = false;
  url: '';
  errorFile     : any = false;
  rowOfTable;
  currentValue;
  product_code = [];
  product_code_data : any;
  service:any;
  // products_ids: any = [];
  status: any;
  product_sku: any = []
  progressBar             :number   = 0;
  public Editor = ClassicEditor;
  public config = {
    toolbar: [ 'heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ],
    heading: {
        options: [
            { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
            { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
            { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
        ]
    }
}
  public merchantData: any = {
    merchant_name: "",
    merchant_username:"",
    merchant_group:"",
    description:"",
    image_owner_url:"",
    background_image:"",
    mcountry:"",
    mcity:"",
    mprovince:"",
    region_code: "",
    region_name: "",
    msubdistrict:"",
    maddress:"",
    contact_person:"",
    email_address:"",
    work_phone:"",
    handphone:"",
    fax_number:"",
    active:0
  }
  
  // listedData: any[];
  regionList = [];




  constructor(public merchantService:MerchantService) {}

  ngOnInit() {
    this.firstLoad();
    // this.setStatus()
    
  }


  async setStatus(event){
    console.log("event : ", event)
    // this.service = await this.merchantService.statusEdit
    this.merchantData.previous_status = this.merchantData.status;
      this.editStatus.forEach((element, index)=> {
        if(element.value == this.detail.status){
          this.editStatus[index].selected = 1;
        }
        if(element.value == this.detail.status){
          this.editStatus[index].selected = 0;
        }
      })

    }


  cancelThis(){
    this.cancel= !this.cancel
  }


  async firstLoad(){

    try 
    {
      Object.keys(this.detail).forEach(key => {
        this.merchantData[key] = this.detail[key]
      });
      this.merchantData.previous_product_code = await this.merchantData.product_code;

      console.log("DISINI" , this.merchantData)
      console.log("TEST", this.detail);
      // if (this.detail._id)
      //   delete this.detail._id;
      this.categoryProduct.forEach((element, index, products_ids) => {
        if(element.value == this.merchantData.status){
            this.categoryProduct[index].selected = 1;
        }
    });

    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
   
  }

  backToDetail(){
   
    this.back[0][this.back[1]]();
  }
  
  async saveThis(){
   
    try 
    {
      // if(this.detail.active) 
        this.merchantData.active = parseInt(this.merchantData.active)
      this.loading= true;
      console.log("test", this.detail);
      let result =  await this.merchantService.updateMerchant(this.merchantData);
      console.log('result this : ', this.loading);

      
      setTimeout(()=>{
        if(result.result.status && result.result.status=='success'){
          alert("Your setting has been saved")
        }
        this.loading= false;
      }, 2000);
  
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    this.selectedFile = event.target.files[0];
  }


  async onUpload() {
    try {
      this.cancel = false;
      let result
      if (this.selectedFile) {
        result = await this.merchantService.upload(this.selectedFile, this, 'image', (result) => { this.callAfterUpload(result) });
        console.log("test?", result)
      }

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    //   this.onUpload.
  }

  callAfterUpload(result) {

    this.merchantData.image_owner_url = result.base_url+result.pic_big_path

  }

  onRegionChanged() {
    console.log("ON Region Changed", this.merchantData.region_name, this.regionList);
    this.regionList.forEach(element => {
      if (this.merchantData.region_name.toLowerCase() == element.region_name.toLowerCase()) {
        this.merchantData.region_code = element.region_code
        return
      }

    }
    );
    console.log(this.merchantData.region_code)
  }

  getRegionList($event) {
    let result = $event.target.value
    if (result.length > 2)
      this.merchantService.getRegion(result, "origin").then((result) => {
        this.regionList = result.result
      })

  }
  
}

