import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrontpageComponent } from './frontpage.component';
import { FrontpageRoutingModule } from './frontpage.routing';
import { PageHeaderModule } from '../../../shared';

@NgModule({
  declarations: [FrontpageComponent],
  // imports: [CommonModule, FormRoutingModule, PageHeaderModule],
  imports: [
    CommonModule, FrontpageRoutingModule, PageHeaderModule
  ]
})
export class FrontpageModule { }
