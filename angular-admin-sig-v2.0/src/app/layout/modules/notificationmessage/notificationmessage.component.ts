import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { NotificationService } from '../../../services/notification/notification.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-notificationsmessage',
  templateUrl: './notificationmessage.component.html',
  styleUrls: ['./notificationmessage.component.scss'],
  animations: [routerTransition()]
})

export class NotificationMessageComponent implements OnInit {

  NotificationMessage       : any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Active Notifications Message Detail',
                                  label_headers   : [
                                      {label: 'Notification Message ID', visible: true, type: 'string', data_row_name: '_id'},
                                    {label: 'Organization ID', visible: true, type: 'string', data_row_name: 'org_id'},
                                    {label: 'Title', visible: true, type: 'string', data_row_name: 'title'},
                                      // {label: 'Message', visible: false, type: 'string', data_row_name: 'message'},
                                      // {label: 'Message From', visible: false, type: 'string', data_row_name: 'message_from'},
                                      ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'NotificationMessage',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  notificationmessageDetail: any = false;
  service     : any ;
  selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     : any = false;
  url = '';


  constructor(public notificationService:NotificationService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.service    = this.notificationService;
      const result: any  = await this.notificationService.getNotificationMessageLint();
      this.NotificationMessage = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  // public async callDetail(_id) {    
  //   try {
  //     this.service    = this.notificationService;
  //     let result: any = await this.notificationService.detailNotificationGroup(_id);
  //     this.notificationgroupDetail = result.result[0];
  //   } catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }

  // }


  public async backToHere(obj) {
    obj.notificationmessageDetail = false;
  }

}
