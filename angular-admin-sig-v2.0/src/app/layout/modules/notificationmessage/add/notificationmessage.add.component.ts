import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { NotificationService } from '../../../../services/notification/notification.service';

@Component({
  selector: 'app-notificationmessage-add',
  templateUrl: './notificationmessage.add.component.html',
  styleUrls: ['./notificationmessage.add.component.scss'],
  animations: [routerTransition()]
})

export class NotificationMessageAddComponent implements OnInit {
  public name: string = "";
  NotificationMessage: any = [];
  service: any;
  title:any;
  message:any;
  from:any;
  status:any;
  icon:any;
  expiry_day:any;
  onUpload:any;
  
//   public notification_topic = [
//     {label:"YES", value:"YES", selected:1}
//    ,{label:"NO", value:"NO"}
//  ]; 
 public notification_status = [
  {label:"ACTIVE", value:"ACTIVE", selected:1}
 ,{label:"INACTIVE", value:"INACTIVE"}
]; 
 selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     : any = false;
  errorLabel    : any = false;

  url = '';

  
  constructor(public notificationService:NotificationService) {
    let form_add     : any = [
      // { label:"notification_id",  type: "text",  value: "", data_binding: 'notification_id'  },
      { label:"title",  type: "text",  value: "", data_binding: 'title'  },
      { label:"message",  type: "text",  value: "", data_binding: 'message'},
      { label:"from",  type: "text",  value: "", data_binding: 'from'},
      { label:"url_logo",  type: "text",  value: "", data_binding: 'url_logo'},
      { label:"url_web",  type: "text",  value: "", data_binding: 'url_web'},
      { label:"icon",  type: "text",  value: "", data_binding: 'icon'},
      { label:"url_logo",  type: "text",  value: "", data_binding: 'url_logo'},
      { label:"expiry_day",  type: "number",  value: "", data_binding: 'expiry_day'},
      // { label:"topic",  type: "textarea", value: [{label:"label", value:"label", selected:1},{label:"label1", value:"label2"}], data_binding: 'topic' },
      { label:"status",  type: "textarea", value: [{label:"label", value:"label", selected:1},{label:"label1", value:"label2"}], data_binding: 'status' }
    ];
   }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;
    
  }

  async formSubmitAddNotificationGroup(form){
    //console.log(JSON.stringify(form));
    
    try 
    {
      this.service    = this.notificationService;
      let result: any  = await this.notificationService.addNewNotification(form);
      console.log(result);
      this.NotificationMessage = result.result;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }


  cancelThis(){
    this.cancel = !this.cancel;
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    // var reader = new FileReader();

    // reader.readAsDataURL(event.target.files[0]); //
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    
    this.selectedFile = event.target.files[0];
  } 

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let _event: any = event;
        this.url = _event.target.result;
      }
    }
  }

  updateProgressBar(value){
    this.progressBar = value;
  }


  // async onUpload(){
  //     try{
  //     this.cancel = false;
  //     if(this.selectedFile){
  //       await this.mediaService.upload(this.selectedFile,this);
  //       }
  //     } catch (e) {
  //       this.errorLabel = ((<Error>e).message);//conversion to Error type
  //     }
  // }

}
