import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { NotificationService } from '../../../../services/notification/notification.service';


@Component({
  selector: 'app-notificationgroup-edit',
  templateUrl: './notificationgroup.edit.component.html',
  styleUrls: ['./notificationgroup.edit.component.scss'],
  animations: [routerTransition()]
})

export class NotificationGroupEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading        :boolean = false;
  topic:any;
  status:any;
  
  public notificationStatus  : any = [
    {label:"ACTIVE", value:"ACTIVE"}
   ,{label:"INACTIVE", value:"INACTIVE"}
];

public notificationTopic  : any = [
  {label:"yes", value:"yes"}
 ,{label:"no", value:"no"}
];
  errorLabel    : any = false;


  constructor(public notificationService:NotificationService) {}

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    this.detail.previous_status = this.detail.status;
    this.notificationStatus.forEach((element, index) => {
        if(element.value == this.detail.status){
            this.notificationStatus[index].selected = 1;
        }
    });

    this.detail.previous_topic = this.detail.topic;
    this.notificationTopic.forEach((element, index) => {
        if(element.value == this.detail.topic){
            this.notificationTopic[index].selected = 1;
        }
    });
    
  }

  backToDetail(){
    //console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis(){
    try 
    {
      this.loading=!this.loading;
      await this.notificationService.updateNotificationGroupID(this.detail);
      this.loading=!this.loading;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
}
