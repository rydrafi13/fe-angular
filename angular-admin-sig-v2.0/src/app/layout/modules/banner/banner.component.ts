
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { BannerService } from '../../../services/banner/banner.service';
import { TableFormat } from '../../../object-interface/common.object';
import { SwiperComponent, SwiperDirective, SwiperConfigInterface,
  SwiperScrollbarInterface, SwiperPaginationInterface } from 'ngx-swiper-wrapper';
import { stripTags } from '../../../object-interface/common.function';


@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
  animations: [routerTransition()]
})


export class BannerComponent implements OnInit {

  public show: boolean = true;

  popUpForm: boolean = false;

  public type: string = 'component';

  public disabled: boolean = false;

  public config: SwiperConfigInterface = {
    // a11y: true,
    direction: 'horizontal',
    slidesPerView: 1,
    keyboard: true,
    spaceBetween: 30,
    mousewheel: true,
    // scrollbar: true,
    // navigation: true,
    pagination: false,
    observer: true,
    freeMode: true,
    // loop: true,
    centeredSlides: false
  };

  private scrollbar: SwiperScrollbarInterface = {
    el: '.swiper-scrollbar',
    hide: false,
    draggable: true
  };

  private pagination: SwiperPaginationInterface = {
    el: '.swiper-pagination',
    clickable: true,
    hideOnClick: false
  };

  @ViewChild(SwiperComponent, { static: false }) componentRef?: SwiperComponent;
  @ViewChild(SwiperDirective, { static: false }) directiveRef?: SwiperDirective;
  counter: any;
  
  constructor(public bannerService: BannerService) {
  }

  public toggleType(): void {
    this.type = (this.type === 'component') ? 'directive' : 'component';
  }

  public toggleDisabled(): void {
    this.disabled = !this.disabled;
  }

  

  public toggleSlidesPerView(): void {
    if (this.config.slidesPerView !== 1) {
      this.config.slidesPerView = 1;
    } else {
      this.config.slidesPerView = 2;
    }
  }

  public toggleOverlayControls(): void {
    if (this.config.navigation) {
      this.config.scrollbar = false;
      this.config.navigation = false;

      this.config.pagination = this.pagination;
    } else if (this.config.pagination) {
      this.config.navigation = false;
      this.config.pagination = false;

      this.config.scrollbar = this.scrollbar;
    } else {
      this.config.scrollbar = false;
      this.config.pagination = false;

      this.config.navigation = true;
    }

    if (this.type === 'directive' && this.directiveRef) {
      // this.directiveRef.setIndex(0);
    } else if (this.type === 'component' && this.componentRef && this.componentRef.directiveRef) {
      // this.componentRef.directiveRef.setIndex(0);
    }
  }

  public toggleKeyboardControl(): void {
    this.config.keyboard = !this.config.keyboard;
  }

  public toggleMouseWheelControl(): void {
    this.config.mousewheel = !this.config.mousewheel;
  }

  public onIndexChange(index: number): void {
    console.log('Swiper index: ', index);
  }

  public onSwiperEvent(event: string): void {
    console.log('Swiper event: ', event);
  }
  
  selected ='front';
  data : any;
  add = false;
  edit = false;
  delete = false;
  // tabs = ['front','flash-popup','deals','products'];
  bannerData: any = [];
  location = {
    'front' : 'Halaman Depan',
    'flash-popup' : 'Halaman Flash Popup',
    'deals' : 'Halaman Deals',
    'product' : 'Halaman Product'
  }
  tableFormat: TableFormat = {
    title: 'Banner Detail Page',
    label_headers: [
      { label: 'Banner ID', visible: true, type: 'string', data_row_name: '_id' },
      { label: 'Image URL', visible: true, type: 'string', data_row_name: 'imageUrl' },
      { label: 'Hash URL', visible: true, type: 'string', data_row_name: 'hashUrl' },
      { label: 'Location', visible: true, type: 'string', data_row_name: 'location' },
      { label: 'Active', visible: true, type: 'number', data_row_name: 'active' },
      { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
      // { label: 'Updated Date', visible: false, type: 'date', data_row_name: 'updated date' },

    ],
    row_primary_key: '_id',
    formOptions: {
      row_id: 'location',
      this: this,
      result_var_name: 'bannerData',
      detail_function: [this, 'callDetail']
    }
  };

  activeTabContainer: any = {
    bannerList: []
  }
  form_input: any = {};
  errorLabel: any = false;
  tabList: any[];

  bannerDetail: any = false;
  service: any;
  position: any = [];
  
  ngOnInit() {
    this.firstLoad();

  }
  filterBannerOfLoc(type){
    return this.bannerData.filter(x => x.location == type);
  }
  // getUniqueLoc(type){
  //   // this.position = [...new Set(type)]
  // }
  // onTabClicked(index, tabVal) {
  //   console.log("tab is clicked index, tabVal : ", index, tabVal)
  // }

  getindexes(type){
    this.counter = this.filterBannerOfLoc(type).length;
    console.log('INI COUNTER', this.counter);
  }
  stripTags(value){
    return stripTags(value).substring(0,40)
  }
  onAddNew(){
    this.edit = this.delete = false;
    this.add = true;
    this.popUpForm = true;
  }
  onDoneNew = () => {
    this.edit = this.delete = this.add = false ;
    this.popUpForm = false;
    this.firstLoad();
    //console.log("adsa")
  }
  onEdit(row){
    this.add = this.delete = false;
    this.edit = this.popUpForm = true;
    this.data = row;
  }
  onDelete(row){
    this.add = this.edit = false;
    this.delete = this.popUpForm= true;
    this.data = row;
    console.log('iini data delete', this.data._id)
  }
  getKeyByValue(object, value) { 
    for (var prop in object) { 
        if (object.hasOwnProperty(prop)) { 
            if (object[prop] === value) 
            return prop; 
        } 
    } 
} 
  onConfirmDelete(){
   
    try {
      const result: any = this.bannerService.deleteBanner(this.data);
      console.log(result);
      this.onDoneNew();
      
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  test(a){
    console.log(a.tab.textLabel);
   
    this.selected = this.getKeyByValue(this.location,a.tab.textLabel);
    console.log('ini selected', this.selected)
    this.getindexes(this.selected)
  }
  async firstLoad() {
    try {
      this.service = this.bannerService;
      let locationList = await this.bannerService.getLocationList().then((result) => {
        let val = result.result
        if (val && Array.isArray(val)) {
          this.tabList = [];
          val.forEach((value) => {
            this.tabList.push(value.location)
          })
          console.log("Val is here", val)
        }
      })

      let result: any = await this.bannerService.getBannerLint();

      this.bannerData = result.result;
      console.log('Locatin' , this.tabList)
      setTimeout(()=>{
        // this.swiperON = true;
      }, 1500)
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  

  public async callDetail(location) {
    try {
      let result: any;
      this.service = this.bannerService;
      result = await this.bannerService.detailBanner(location);

      this.bannerDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.bannerDetail = false;
  }

}
