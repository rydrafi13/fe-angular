import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { BannerService } from '../../../../services/banner/banner.service';

@Component({
  selector: 'app-banner-detail',
  templateUrl: './banner.detail.component.html',
  styleUrls: ['./banner.detail.component.scss'],
  animations: [routerTransition()]
})

export class BannerDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit:boolean = false;
  
  constructor(public bannerService:BannerService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

}
