import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { BannerService } from '../../../../services/banner/banner.service';
import { MediaService } from '../../../../services/media/media.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { isString } from '../../../../../app/object-interface/common.object';
import { isArray } from 'ngx-bootstrap/chronos';


@Component({
  selector: 'app-banner-edit',
  templateUrl: './banner.edit.component.html',
  styleUrls: ['./banner.edit.component.scss'],
  animations: [routerTransition()]
})

export class BannerEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  @Input() public ok;
  public name: string = "";
  Banner: any = [];
  service: any;
  errorLabel : any = false;
  selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     : any = false;
  imageUrl     : any = false;
  selFile;
  hashUrl:any;
  description: any;
  indexes: any;
  location:any;
  active:any;
  isFileUploaded = false;
  url: '';
  form_add: any = {
    imageUrl:'',
    hashUrl: 'cls',
    location: '',
    active: 0,
    target:{
      imageURL:'',
      description: '',
      value : '',
      type:'',
    }
  }
  public Editor = ClassicEditor;
  config = {
    toolbar: [ 'heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ],
    heading: {
        options: [
            { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
            { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
            { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
        ]
    }
}
  public status: any = [
    {label: 'ACTIVE', value: 1},
    {label: 'INACTIVE', value: 0}
  ];
  public position: any = [
    { label: 'Halaman Home', value: 'front' },
    { label: 'Halaman Discovery Flash Popup', value: 'flash-popup' },
    { label: 'Halaman Discovery Deals', value: 'deals' },
    { label: 'Halaman Discovery Products', value: 'product' }
  ];
  public index: any = [
    {label: 'Row no. 1', value: '1'},
    {label: 'Row no. 2', value: '2'},
    {label: 'Row no. 3', value: '3'},
    {label: 'Row no. 4', value: '4'},
    {label: 'Row no. 5', value: '5'},
  ];
  // locations = {
  //   'Halaman Depan': 'front',
  //   'Halaman Discovery Flash Popup': 'flash-popup',
  //   'Halaman Discovery Deals': 'deals',
  //   'Halaman Discovery Products': 'products'
  // }
  public loading:boolean = false;
  
//   public transactionStatus  : any = [
//     {label:"PENDING", value:"PENDING"}
//    ,{label:"CANCEL", value:"CANCEL"}
//    ,{label:"FAILED", value:"FAILED"}
//    ,{label:"SUCCESS", value:"SUCCESS"}
//    ,{label:"ERROR", value:"ERROR"}
// ];
 

  constructor(public bannerService:BannerService,public mediaService: MediaService) {
    
  }
  
  ngOnInit() {
    this.firstLoad();
    console.log('INI DATANYA ',this.detail)
    this.form_add = this.detail
  }

  async firstLoad(){
    // this.detail.previous_status = this.detail.status;
    // this.transactionStatus.forEach((element, index) => {
    //     if(element.value == this.detail.transaction_status){
    //         this.transactionStatus[index].selected = 1;
    //     }
    // });

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });
  }
  stringToNumber(str: any) {
    if (typeof str == 'number') {
      str = str + '';
    }
    let s = str.toLowerCase().replace(/[^0-9]/g, '');
    return parseInt(s);
  }


  parseStringToArray(str: any) {
    let splitR;
    if (!isString(str)) return [];
    else {
      splitR = str.split(/[\,\.]/);
    }
    return splitR;
  }
 
  formSubmitEditBanner() {
    // form.imageUrl = this.imageUrl;
    // console.log(JSON.stringify(form));
    // this.form_add.location = this.locations[this.form_add.location];
    let form_add = this.form_add;
    
    form_add.active = this.stringToNumber(form_add.active)
    form_add.target.index = this.stringToNumber(form_add.target.index)
    try {
      console.log('form : ', form_add);

      const result: any = this.bannerService.updateBanner(form_add);
      this.Banner = result.result;
      this.ok()
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }
  backToDetail(){
    //console.log(this.back);
    this.back[0][this.back[1]]();
  }

  // async saveThis(){
  //   try 
  //   {
  //     this.loading=!this.loading;
  //     await this.bannerService.updateBanner(this.detail);
  //     this.loading=!this.loading;
  //   } 
  //   catch (e) 
  //   {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }
  // }
  


  onFileSelected(event) {
    this.isFileUploaded = false;
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    var reader = new FileReader();

    reader.onload = (event: any) => {
      this.selFile = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0]);

    Array.from(event.target.files).forEach((file: any) => {
      if (file.size > fileMaxSize) {
        this.errorFile = "maximum file is 3Mb";
      }
    });

    console.log('INI FILENYA ', this.selectedFile)
    if (this.errorLabel == false) {
      this.selectedFile = event.target.files[0];

      setTimeout(() => {
        this.onUpload();
      }, 1500)
    }
  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let _event: any = event;
        this.url = _event.target.result;
      }
    }
  }
  updateProgressBar(value) {
    this.progressBar = value;
  }

  cancelThis() {
    this.cancel = !this.cancel;
  }


  async onUpload() {
    try {
      this.cancel = false;
      let result
      console.log('INI YANG AKAN ',this.selectedFile)
      if (this.selectedFile) {
        result = await this.bannerService.upload(this.selectedFile, this, 'image', (result) => { this.callAfterUpload(result) });
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    //   this.onUpload.
    return false;
  }
  callAfterUpload(result) {
    if (result) {
      // let file = result.base_url.concat(result.pic_medium_path);
      // let banner = this.form_add
      this.form_add.imageUrl =this.form_add.target.imageURL = result.base_url.concat(result.pic_big_path);
      console.log('ini hasilnya', this.form_add)
      this.isFileUploaded = true;
    }
  }

}
