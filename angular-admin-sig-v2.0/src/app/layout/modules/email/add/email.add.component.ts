import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { EmailService } from '../../../../services/email/email.service';

@Component({
  selector: 'app-email-add',
  templateUrl: './email.add.component.html',
  styleUrls: ['./email.add.component.scss'],
  animations: [routerTransition()]
})

export class EmailAddComponent implements OnInit {
  public name: string = "";
  Email: any = [];
  service: any;
  public email_type = [
    {label:"ACTIVE", value:"ACTIVE", selected:1}
   ,{label:"INACTIVE", value:"INACTIVE"}
 ];
 selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     : any = false;
  errorLabel    : any = false;
  sender_email:any;
  recipient_email:any;
  CC:any;
  subject:any;
  body:any;
  
  constructor(public emailService:EmailService) {
    let form_add     : any = [
      { label:"sender_email",  type: "text",  value: "", data_binding: 'sender_email'  },
      { label:"recipient_email",  type: "text",  value: "", data_binding: 'recipient_email'  },
      { label:"CC",  type: "text",  value: "", data_binding: 'CC'  },
      { label:"subject",  type: "text",  value: "", data_binding: 'subject' },
      { label:"body",  type: "text",  value: "", data_binding: 'body' },
      { label:"email_type",  type: "text",  value: "", data_binding: 'email_type' },
    ];
   }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;
    
  }

  async formSubmitAddEmail(form){
    //console.log(JSON.stringify(form));
    
    try 
    {
      this.service    = this.emailService;
      let result: any  = await this.emailService.addNewEmail(form);
      console.log(result);
      this.Email = result.result;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  /* onFileSelected(event){
    try 
    {
      this.errorFile = false;
      let fileMaxSize = 3000000;// let say 3Mb
      Array.from(event.target.files).forEach((file: any) => {
          if(file.size > fileMaxSize){
            this.errorFile="Maximum File Upload is 3MB";
          }
      });
      this.selectedFile = event.target.files[0];
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }  */

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }

  /* async onUpload(){
      try 
      {
        this.cancel = false;
        if(this.selectedFile){
          await this.emailService.upload(this.selectedFile,this);
          }
      } 
      catch (e) 
      {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
  } */
}
