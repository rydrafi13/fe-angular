import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmailComponent } from './email.component';
import { EmailAddComponent } from './add/email.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { EmailDetailComponent } from './detail/email.detail.component';
import { EmailRoutingModule } from './email-routing.module';
import { EmailEditComponent } from './edit/email.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  imports: [CommonModule,
    EmailRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
],
  
  declarations: [
    EmailComponent, EmailAddComponent, EmailDetailComponent, EmailEditComponent] 
})
export class EmailModule { }
