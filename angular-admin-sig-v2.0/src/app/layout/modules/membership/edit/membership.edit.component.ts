import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MembershipService } from '../../../../services/membership/membership.service';


@Component({
  selector: 'app-membership-edit',
  templateUrl: './membership.edit.component.html',
  styleUrls: ['./membership.edit.component.scss'],
  animations: [routerTransition()]
})

export class MembershipEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  membership_status:any;
  activation_status:any;
  

  public loading        : boolean = false;

  public activationStatus: any = [
    {label: 'ACTIVATED', value: 'ACTIVATED'},
    {label: 'INACTIVED', value: 'INACTIVED'}
  ];

  public membershipStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  constructor(public membershipService: MembershipService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    this.detail.previous_membership_id = this.detail.membership_id;
    this.activationStatus.forEach((element, index) => {
        if (element.value == this.detail.activation_status) {
            this.activationStatus[index].selected = 1;
        }
    });

    this.detail.previous_membership_id = this.detail.membership_id;
    this.membershipStatus.forEach((element, index) => {
        if (element.value == this.detail.membership_status) {
            this.membershipStatus[index].selected = 1;
        }
    });

  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    try {
      this.loading = !this.loading;
      await this.membershipService.updateMembershipID(this.detail);
      console.log(this.membershipService);
      this.loading = !this.loading;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

}
