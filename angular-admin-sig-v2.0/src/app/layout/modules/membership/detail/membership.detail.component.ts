import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MembershipService } from '../../../../services/membership/membership.service';
import { del } from 'selenium-webdriver/http';

@Component({
  selector: 'app-membership-detail',
  templateUrl: './membership.detail.component.html',
  styleUrls: ['./membership.detail.component.scss'],
  animations: [routerTransition()]
})

export class MembershipDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit: boolean = false;
  errorLabel : any = false;
  constructor(public membershipService: MembershipService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){


  }

  editThis() {
    // console.log(this.edit );
    this.edit = !this.edit;
    // console.log(this.edit );
  }
  backToTable() {
    // console.log(this.back);
    this.back[1](this.back[0]);
  }

  async deleteThis(){
    try {
      const delResult: any = await this.membershipService.delete(this.detail);
      console.log(delResult);
      if (delResult.error == false) {
        console.log(this.back[0]);
        this.back[0].membershipDetail = false;
        this.back[0].firstLoad();
      // delete this.back[0].prodDetail;
      }
    } catch (error) {
      throw new TypeError(error.error.error);
    }
  } 
  //catch (e) {
  //   this.errorLabel = ((<Error>e).message);//conversion to Error type
  // }

}
