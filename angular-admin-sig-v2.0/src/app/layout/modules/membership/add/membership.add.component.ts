import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MembershipService } from '../../../../services/membership/membership.service';

@Component({
  selector: 'app-membership-add',
  templateUrl: './membership.add.component.html',
  styleUrls: ['./membership.add.component.scss'],
  animations: [routerTransition()]
})

export class MembershipAddComponent implements OnInit {
  public name: string = "";
  Memberships: any = [];
  service: any;
  errorLabel : any = false;
  transaction_reach:any;
  description:any;
  
  public membership_type = [
                       {label:"superuser", value:"superuser"}
                      ,{label:"membership", value:"membership", selected:1}
                      ,{label:"marketing", value:"marketing"}
                      ,{label:"admin", value:"admin"}
                      ,{label:"merchant", value:"merchant"}
                    ];
                
  constructor(public membershipService:MembershipService) {
    let form_add     : any = [
      { label:"Full Name",  type: "text",  value: "", data_binding: 'full_name'  },
      { label:"Email",  type: "text",  value: "", data_binding: 'email'  },
      { label:"Password",  type: "password",  value: "", data_binding: 'password'  },
      { label:"PIN",  type: "password",  value: "", data_binding: 'pin'  },
      { label:"Cell Phone",  type: "password",  value: "", data_binding: 'cell_phone'  },
      { label:"Card Number ",  type: "text",  value: "", data_binding: 'card_number' },
      { label:"Date of Birth",  type: "text",  value: "", data_binding: 'dob' },
      { label:"City",  type: "text",  value: "", data_binding: 'mcity' },
      { label:"State",  type: "text",  value: "", data_binding: 'mstate' },
      { label:"Country",  type: "text",  value: "", data_binding: 'mcountry' },
      { label:"Address",  type: "textarea",  value: "", data_binding: 'maddress1' },
      { label:"Post Code",  type: "text",  value: "", data_binding: 'mpostcode' },
      { label:"Gender",  type: "text",  value: "", data_binding: 'gender' },
      { label:"Marital Status",  type: "text",  value: "", data_binding: 'marital_status' },
      { label:"type ",  type: "textarea",  value: [{label:"label", value:"label", selected:1},{label:"label1", value:"label2"}], data_binding: 'address2' },
      { label:"address 2 ",  type: "textarea",  value: "", data_binding: 'address2' },
    ];
   }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    // this.service = this.membershipService;
    // let result: any = await this.membershipService.getMembershipsLint();
    // this.Memberships = result.result;

  }
  formSubmitAddMembership(form){
    // console.log(form);
    // console.log(JSON.stringify(form));

    try {
      this.service    = this.membershipService;
      const result: any  = this.membershipService.addNewMembership(form);
      this.Memberships = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

}


}
