import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistorywaitingRoutingModule } from './orderhistorywaiting-routing.module';
import { OrderhistorywaitingComponent } from './orderhistorywaiting.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistorywaitingDetailComponent } from './detail/orderhistorywaiting.detail.component';
import { OrderhistorywaitingEditComponent } from './edit/orderhistorywaiting.edit.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';


@NgModule({
  imports: [
    CommonModule, 
    OrderhistorywaitingRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    BsComponentModule,
    NgxBarcodeModule,
    MatButtonModule,
    MatRadioModule,
    ProgressbarModule.forRoot(),
  ],
  declarations: [
    OrderhistorywaitingComponent,
    OrderhistorywaitingDetailComponent,
    OrderhistorywaitingEditComponent
  ],
  exports:[
    OrderhistorywaitingComponent,
    OrderhistorywaitingDetailComponent,
    OrderhistorywaitingEditComponent
  ]
})
export class OrderhistorywaitingModule { }
