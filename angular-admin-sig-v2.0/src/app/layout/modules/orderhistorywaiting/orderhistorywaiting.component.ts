import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ProgramNameService } from '../../../services/program-name.service';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-orderhistorywaiting',
  templateUrl: './orderhistorywaiting.component.html',
  styleUrls: ['./orderhistorywaiting.component.scss'],
  animations: [routerTransition()]
})
export class OrderhistorywaitingComponent implements OnInit {

  Orderhistorywaiting: any = [];
  tableFormat: TableFormat = {
    title: 'Order History Waiting For Approval',
    label_headers: [
        { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
        { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
        { label: 'ID Toko', visible: true, type: 'string', data_row_name: 'username' },
        { label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
        { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
        { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
        { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
        { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],
    row_primary_key: '_id',
    formOptions: {
      approveButton: true,
      row_id: 'order_id',
      this: this,
      result_var_name: 'Orderhistorywaiting',
      detail_function: [this, 'callDetail']
    },
    show_checkbox_options: true
  };

  tableFormat2: TableFormat = {
    title: 'Order History Waiting For Approval',
    label_headers: [
        { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
        { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
        { label: 'Nomor PO', visible: true, type: 'value_po', data_row_name: 'po_no'},
        { label: 'ID Bisnis', visible: true, type: 'form_business_id', data_row_name: 'member_detail' },
        { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
        { label: 'Nama Pelanggan', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
        { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
        { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
        { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],
    row_primary_key: '_id',
    formOptions: {
      approveButton: true,
      row_id: 'order_id',
      this: this,
      result_var_name: 'Orderhistorywaiting',
      detail_function: [this, 'callDetail']
    },
    show_checkbox_options: true
  };

  tableFormat3: TableFormat = {
    title: 'Order History Waiting For Approval',
    label_headers: [
        { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
        { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
        { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
        { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
        { label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
        { label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
        { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
        { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
        { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],
    row_primary_key: '_id',
    formOptions: {
      approveButton: true,
      row_id: 'order_id',
      this: this,
      result_var_name: 'Orderhistorywaiting',
      detail_function: [this, 'callDetail']
    },
    show_checkbox_options: true
  };

  form_input: any = false;
  errorLabel: any = false;
  totalPage: 0;

  orderhistorywaitingDetail: any = false;
  service: any;

  mci_project: any = false;

  errorMessage: any = false;

  showLoadingSpinner: any = false;
  processCount: any = 0;
  processTotal:any = 0;

  programType:any = "";

  public contentList : any = (content as any).default;

  constructor(public orderhistoryService: OrderhistoryService, private router: Router, private activatedRoute: ActivatedRoute, private programNameService: ProgramNameService) { }

  ngOnInit() {
    this.firstLoad()
  }

  public ngOnDestroy() {
    const programName = localStorage.getItem('programName');
    this.programNameService.setData(programName);
  }

  async firstLoad() {
    try {
      let paramId = this.activatedRoute.snapshot.queryParamMap.get('order_id');
      let paramAppLabel = this.activatedRoute.snapshot.queryParamMap.get('app_label');

      if(paramId && paramAppLabel) {
        // localStorage.setItem('programName', paramAppLabel);
        this.programNameService.setData(paramAppLabel);
        
        const resultDetail: any = await this.orderhistoryService.getOrderDetail(paramId);
        console.log("resultDetail", resultDetail);
        if(resultDetail && resultDetail.order_id) this.orderhistorywaitingDetail = resultDetail;
      }
      
      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
          if(element.appLabel == program) {
          if(element.type == "reguler") {
              _this.mci_project = true;
              _this.programType = "reguler";
          } else if(element.type == "custom_kontraktual") {
            _this.mci_project = false;
            _this.programType = "custom_kontraktual";
          } else {
              _this.mci_project = false;
              _this.programType = "custom";
          }
        }
      });
      
      this.service = this.orderhistoryService;
      let result: any = await this.orderhistoryService.getOrderhistorywaitingLint();
      this.totalPage = result.total_page;

      this.Orderhistorywaiting = result.values;
    } catch (e) {
      console.log("ERROR",e);
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			}
    }
  }

  public async callDetail(data, rowData) {
    this.orderhistorywaitingDetail = this.Orderhistorywaiting.find(order => order.order_id == rowData.order_id);

  }

  public async backToHere(obj) {
    const programName = localStorage.getItem('programName');
    obj.programNameService.setData(programName);

    obj.orderhistorywaitingDetail = false;
    obj.router.navigate([]).then(() => {
      obj.firstLoad();
    });
  }

  public async processSelectedOrders(obj, orders){
    if(orders.length > 0){
      Swal.fire({
        title: 'Confirmation',
        text: 'Apakah anda yakin ingin memproses semua order yang dipilih?',
        // icon: 'success',
        confirmButtonText: 'Process',
        cancelButtonText:"Cancel",
        showCancelButton:true
        }).then(async (result) => {
        if(result.isConfirmed){
          this.showLoadingSpinner = true;
          this.processTotal = orders.length;
          this.processCount = 0;

          await orders.forEach(async(order_id, index) => {
            const payload = {
              order_id
            }
            try {
              await obj.orderhistoryService.processOrder(payload).then(() => {
                this.processCount ++;
                
                if(this.processCount >= orders.length) { 
                  this.showLoadingSpinner = false;

                  Swal.fire({
                    title: 'Success',
                    text: 'Semua order terpilih telah diproses',
                    icon: 'success',
                    confirmButtonText: 'Ok',
                    showCancelButton:false
                    }).then(async (result) => {
                      if(result.isConfirmed){
                        console.log("confirmed");
                        obj.firstLoad();
                      }
                  });
                }
              });
              

              // Swal.fire({
              //   title: 'Success',
              //   text: 'Semua order terpilih telah diproses',
              //   icon: 'success',
              //   confirmButtonText: 'Ok',
              //   showCancelButton:false
              //   }).then(async (result) => {
              //     if(result.isConfirmed){
              //       obj.firstLoad();
              //     }
              //   });

              // setTimeout(() => {
              //   this.processCount ++;
              //   console.log(this.processCount);
              //   console.log("orders.length", orders.length);
              //   if(this.processCount >= orders.length) { 
              //     this.showLoadingSpinner = false;
              //   }
              // }, index * 500);
            } catch (e) {
              this.errorLabel = ((<Error>e).message);//conversion to Error type

              let message = this.errorLabel;
              if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
              }
              Swal.fire({
                icon: 'error',
                title: message,
              });
            }
          });
        }
      });
    }
  }

}
