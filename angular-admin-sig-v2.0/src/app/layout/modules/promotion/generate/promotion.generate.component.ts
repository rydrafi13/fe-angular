import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-generate',
  templateUrl: './promotion.generate.component.html',
  styleUrls: ['./promotion.generate.component.scss']
})
export class PromotionGenerateComponent implements OnInit {

  public status: any = [
    { label: 'ACTIVE', value: 1 },
    { label: 'INACTIVE', value: 0 }
  ];
  public use: any = [
    { label: 'CURRENTLY USED', value: 1 },
    { label: 'UNUSED', value: 0 }
  ];
  public type: any = [
    { label: 'PERCENTAGE', value: 1 },
    { label: 'NOMINAL', value: 0 }
  ];
  public category: any = [
    { label: 'SEMUA', value: 1 },
    { label: 'TRANSAKSI PEMBELIAN', value: 2 },
    { label: 'ONGKOS KIRIM', value: 3 }
  ];
  form: any = {
		type: 'product',
		min_order: 1,
		product_code: '',
		product_name: '',
		category: 'public',
		price: 0,
		fixed_price: 0,
		qty: 0,
		description: '',
		tnc: '',
		location_for_redeem: '',
		dimensions: {
			width: 0,
			length: 0,
			height: 0
		},
		weight: 1,
		images_gallery: [],
		weightInGram: 1000,
		pic_file_name: '',
		need_outlet_code: 1,
	};
  selectedLevel(){}
  formSubmitAddEvoucher(d){}
  constructor() { }

  ngOnInit() {
  }
  selected(){}

}
