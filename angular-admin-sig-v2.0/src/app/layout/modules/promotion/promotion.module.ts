import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromotionComponent } from './promotion.component';
import { PromotionRoutingModule } from './promotion.routing';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { Ng2CompleterModule } from 'ng2-completer';
import {PromotionDetailComponent} from './detail/promotion.detail.component';
import {PromotionEditComponent} from './edit/promotion.edit.component';
import { PromotionGenerateComponent } from './generate/promotion.generate.component';
@NgModule({
  declarations: [PromotionComponent, PromotionDetailComponent, PromotionEditComponent, PromotionGenerateComponent],
  // imports: [CommonModule, FormRoutingModule, PageHeaderModule],
  imports: [
    CommonModule,
    PromotionRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    Ng2CompleterModule
  ]
})
export class PromotionModule { }
