import { Component, OnInit } from '@angular/core';
import {routerTransition} from '../../../router.animations';
import { PromotionService } from '../../../services/promotion/promotion.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.scss'],
    animations: [routerTransition()]
})
export class PromotionComponent implements OnInit {

    Promotion: any = [];
  tableFormat: TableFormat = {
                                  title           : 'Promotion Detail Page',
                                  label_headers   : [
                                      // {label: 'Promotion ID', visible: false, type: 'date', data_row_name: '_id'},
                                    {label: 'Name', visible: true, type: 'string', data_row_name: 'name'},
                                    {label: 'Expired Date', visible: true, type: 'string', data_row_name: 'expired_date'},
                                    {label: 'Type',
                                        options : [
                                            'deal',
                                            'product',
                                        ],
                                        visible: true,
                                        type: 'list',
                                        data_row_name: 'type'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                    addFormGenerate   : true,
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Promotion',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input: any = {};
  errorLabel: any = false;

  promotionDetail: any = false;
  service: any;

  constructor(
      public promotionService: PromotionService
  ) { }

  ngOnInit() {
      this.firstLoad();
  }

  async firstLoad() {
    try {
      this.service    = this.promotionService;
      const result: any  = await this.promotionService.getPromotionLint();
      console.log(result);
      this.Promotion = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  public async callDetail(promotion_id) {
    try {
      let result: any;
      this.service    = this.promotionService;
      result          = await this.promotionService.detailPromotion(promotion_id);
      console.log(result);

      this.promotionDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.promotionDetail = false;
  }

}
