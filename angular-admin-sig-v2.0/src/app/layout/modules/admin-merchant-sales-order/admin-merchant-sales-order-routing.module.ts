import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminMerchantSalesOrderComponent} from './admin-merchant-sales-order.component'


const routes: Routes = [
  {
    path: '', component: AdminMerchantSalesOrderComponent,

}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminMerchantSalesOrderRoutingModule { }
