import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PermissionObserver } from '../../services/observerable/permission-observer';

@Component({
  selector: 'app-router-master',
  templateUrl: './router-master.component.html',
  styleUrls: ['./router-master.component.scss']
})
export class RouterMasterComponent implements OnInit {

  constructor(private router: Router, private castPermission: PermissionObserver) { }

  ngOnInit() {
    this.castPermission.currentPermission.subscribe((permissionVal)=>{
        // if(permissionVal == 'merchant'){
        //     this.router.navigate(['/merchant-portal/homepage']);
        //     return true;
        // }
        if(permissionVal == 'admin'){
          // this.router.navigate(['/app-login']);
            // this.router.navigate(['/administrator/order-history-summary']);
        }
        else{
          this.router.navigate(['/login/out']);
        }
        
    })

    const isLoggedin =localStorage.getItem('isLoggedin');
      if (isLoggedin == 'true') {
          this.router.navigateByUrl('/app-login');
      }
  }
  // return;
}
