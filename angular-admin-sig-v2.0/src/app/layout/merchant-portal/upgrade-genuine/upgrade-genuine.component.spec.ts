import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradeGenuineComponent } from './upgrade-genuine.component';

describe('UpgradeGenuineComponent', () => {
  let component: UpgradeGenuineComponent;
  let fixture: ComponentFixture<UpgradeGenuineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradeGenuineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradeGenuineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
