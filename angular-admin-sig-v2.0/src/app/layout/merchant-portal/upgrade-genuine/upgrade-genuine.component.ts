import { Component, OnInit } from '@angular/core';
import { MerchantService } from '../../../services/merchant/merchant.service';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-upgrade-genuine',
	templateUrl: './upgrade-genuine.component.html',
	styleUrls: ['./upgrade-genuine.component.scss']
})
export class UpgradeGenuineComponent implements OnInit {
	progressBar: number = 0;
	showLoading: boolean = false;
	showLoading2: boolean = false;
	showLoading3: boolean = false;
	showLoading4: boolean = false;
	showLoading5: boolean = false;
	showLoading6: boolean = false;
	showLoading7: boolean = false;
	errorLabel: any = false;
	uploadnow;
	selectGenuine = false;
	selectOfficial = false;
	selectedFile = null;
	selectedFile2 = null;
	errorFile;
	selFile;
	form: any = {

		id_image: '',
		npwp_image: '',
		siup_image: '',
		sampul_image: '',
		tdp_image: '',
		nip_image: '',
		domisili_image: ''

	};
	complete = false;
	constructor(
		private merchantService: MerchantService
	) { }

	ngOnInit() {
	}
	updateProgressBar(value) {
		this.progressBar = value;
	}
	async onFileSelected(event, img) {
		var reader = new FileReader()
		try {
			this.errorFile = false;
			let fileMaxSize = 3000000; // let say 3Mb
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 3MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}

			if(event.target.files[0]){
				reader.readAsDataURL(event.target.files[0]);
			  }
			if (this.selectedFile) {
				switch (img) {
					case 'id_image':
						
						this.showLoading = true;
						console.log("ini1", img)
						break;
					case 'npwp_image':
						
						this.showLoading2 = true;
						console.log("ini2", img)
						break;
					case 'siup_image':
						
						this.showLoading3 = true;
						console.log("ini3", img)
						break;
					case 'sampul_image':
					
						this.showLoading4 = true;
						console.log("ini4", img)
						break;
					case 'tdp_image':
						
						this.showLoading5 = true;
						console.log("ini5", img)
						break;
					case 'nib_image':
						
						this.showLoading6 = true;
						console.log("ini6", img)
						break;
					case 'domisili_image':
						
						this.showLoading7 = true;
						console.log("ini7", img)
						break;

				}
				
				const logo = await this.merchantService.upload(this.selectedFile, this, 'image', (result) => {
					console.log("hasil", result)
					this.callImage(result, img);
				});
			}
			console.log("test lah", this.callImage)
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}
	allfalse() {
		this.showLoading = this.showLoading2 = this.showLoading3
			= this.showLoading4 = this.showLoading5 = this.showLoading6
			= this.showLoading7 = false
	}

	callImage(result, img) {
		console.log('result upload logo ', result);
		this.form[img] = result.base_url + result.pic_big_path;
		this.allfalse();
		console.log('This detail', this.form);
		
	}
	genuineClick() {
		this.selectGenuine = true;
		this.selectOfficial = false;
	}
	officialClick() {
		this.selectOfficial = true;
		this.selectGenuine = false;
	}
	saveProfile() {
		this.complete = true;
	}
}
