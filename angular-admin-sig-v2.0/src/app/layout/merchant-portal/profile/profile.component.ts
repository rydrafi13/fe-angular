import { MerchantService } from './../../../services/merchant/merchant.service';
import { Component, OnInit, Input, ElementRef, NgZone, ViewChild } from '@angular/core';
import { Routes, Router, ActivatedRoute } from '@angular/router';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { MemberService } from '../../../services/member/member.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ReadVarExpr } from '@angular/compiler';
import { CourierServiceService } from '../../../services/courier/courier.service';
import { CountryService } from '../../../services/country/country.service';
import { PaymentGatewayService } from '../../../services/paymentgateway/paymentgateway.service';
import Swal from 'sweetalert2';
import { FormControl } from '@angular/forms';
import { Placeholder } from '@angular/compiler/src/i18n/i18n_ast';
import { MapsAPILoader } from '@agm/core';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})


export class ProfileComponent implements OnInit {

	public latitude: number;
	public longitude: number;
	public searchControl: FormControl;
	zoom: number;
	address: string;
	private geoCoder;
	@ViewChild('search', { static: false })
	public searchElementRef: ElementRef;
	latitude2: number;
	longitude2: number;
	latlongs: any = {};
	latlong: any = {};
	Editor = ClassicEditor;
	config = {
		toolbar: ['heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote'],
		heading: {
			options: [
				{ model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
				{ model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
				{ model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
			]
		}
	}
	service: any;
	errorLabel: any = false;
	autoFill = true;
	complete = false;
	edit: boolean = false;
	swaper: any[] = [{ name: 'Profile Information', val: true }, { name: 'Store Information', val: false }];
	progressBar: any;

	public merchant_group: any = [];
	public available_courier: any = [];

	bgUrl = '';
	max: any;
	countryList = [];
	courier;
	pin: any = {
		password: '',
		new_pin: '',
		repeat_new_pin: ''
	}
	showMap: boolean = false;
	map_active :boolean =false;
	form: any = {
		cell_phone: '',
		dob: '',
		email: '',
		full_name: '',
		gender: 'male',
		merchant_name: '',
		merchant_username: '',
		description: '',
		image_owner_url: '',
		background_image: '',
		mcountry: 'Indonesia',
		mcity: '',
		mstate: '',
		mprovince: '',
		mdistrict: '',
		mpostal_code: '',
		region_code: '',
		mprovince_code: '',
		mcity_code: '',
		msubdistrict_code: '',
		mvillage_code: '',
		mvillage: '',
		msubdistrict: '',
		address: '',
		latitude: '',
		longitude: '',
		google_address: '',
		maddress: '',
		contact_person: '',
		email_address: '',
		work_phone: '',
		handphone: '',
		fax_number: '',
		active: 0,
		_id: '',
		slogan: '',
		available_courier: []
	};

	formEditable: any = {};
	courierServices: any = [];
	couriers: any = [];
	showModal = false;
	setPin = false;
	successPin = false;
	retypePin = false;
	passwordField = false;
	othersForm = false;
	regionList = [];
	provinceList = [];
	cityList = [];
	bankList: any = [];
	subDistrictList = [];
	villageList = [];
	merchantGroup: any = [];
	errorFile;
	selFile;
	openModal = false;
	hide = true;
	selectedFile = null;
	selectedFile2 = null;
	cancel: boolean = false;
	hasToko: boolean = false;
	bukaTokoBtn: boolean = true;
	upload: boolean = false;
	addMerchantDetail: boolean = false;
	storeInfoBtn: boolean = true;
	storeProfile: boolean = false;
	open: boolean = false;
	openStore: boolean = false;
	keyname: any = [];
	activeCheckbox = false;
	checkBox = [];
	errorMessage;

	constructor(
		private memberService: MemberService,
		private countryService: CountryService,
		private courierService: CourierServiceService,
		private paymentGatewayService: PaymentGatewayService,
		private mapsAPILoader: MapsAPILoader,
		private ngZone: NgZone,
		private router: Router,
		private route: ActivatedRoute,
		private merchantService: MerchantService
	) { }

	ngOnInit() {
		this.firstLoad();

	}

	getCountryList() {
		let n = this.countryService.getCountryList();
		let r = [];
		n.forEach((element) => {
			r.push({ value: element, name: element })
		})
		this.countryList = r;
		this.form.mcountry = 'Indonesia'
	}
	swapClick(index) {
		this.swaper.forEach((e) => {
			e.val = false;
		});
		this.swaper[index].val = true;
	}

	editableForm(label, val?: boolean) {
		this.formEditable[label] = val;
	}

	async firstLoad() {
		let member = await this.memberService.getMemberDetail();
		console.log("member", member)

		if (member.result) {
			let data = member.result;
			this.form = { ...this.form, ...data };
		}

		if (member.result.permission == 'merchant') {
			let member = await this.merchantService.getDetail();
			let data = member.result;
			console.log("data", data)
			this.form = { ...this.form, ...data };
		}
		var i

		for (i = 0; i < this.form.available_courier.length; i++) {
			this.courier += this.form.available_courier[i];
		}
		// console.log("text", this.courier, 'available cour', this.form.available_courier)

		this.zoom = 4;
		if(this.form.latitude != '' && this.form.longitude != ''){
			this.map_active = true;
			this.showMap = true;
			this.latitude = this.form.latitude;
		this.longitude = this.form.longitude;
		}
		
		
		
		//create search FormControl
		this.searchControl = new FormControl();
		this.geoCoder = new google.maps.Geocoder;
		//set current position
		// this.setCurrentLocation();

		//load Places Autocomplete
		this.mapsAPILoader.load().then(() => {
			let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
				types: [],
				componentRestrictions: { 'country': 'ID' }
			});
			autocomplete.addListener("place_changed", () => {
				this.ngZone.run(() => {
					//get the place result
					let place: google.maps.places.PlaceResult = autocomplete.getPlace();

					//verify result
					if (place.geometry === undefined || place.geometry === null) {
						return;
					}

					//set latitude, longitude and zoom
					this.latitude = place.geometry.location.lat();
					this.longitude = place.geometry.location.lng();
					this.zoom = 12;
					this.map_active = true;
					this.showMap = false;
					this.getAddress(this.latitude, this.longitude);
				});
			});
		});
		this.getAddress(this.latitude, this.longitude);
		if(this.form.withdrawal_bank_name.account_name){
			this.form.withdrawal_account_name = this.form.withdrawal_bank_name.account_name
			delete this.form.withdrawal_bank_name.account_name
			console.log(this.form)
		}
		await this.getCountryList();
		// await this.getProvinceList();
		// await this.getKeyname();
		console.log(this.form)
		// this.getCourier();
		await this.checkCourier(true);
		await this.getBankList();
		// console.log(this.getBankList);

		if (this.form.available_courier && this.form.available_courier.length) {
			this.form.available_courier.forEach((value) => {
				console.log('value', value)
				this.setCourierService(value);

			});

		}
		console.log('coureiers', this.couriers)
	}
	// Get Current Location Coordinates
	private setCurrentLocation() {
		if ('geolocation' in navigator) {
			navigator.geolocation.getCurrentPosition((position) => {
				this.latitude = position.coords.latitude;
				this.longitude = position.coords.longitude;
				this.zoom = 8;
				this.getAddress(this.latitude, this.longitude);
			});
		}
	}

	markerDragEnd($event: any) {
		// console.log(' event',$event.latLng.lat());
		this.latitude = $event.latLng.lat()
		this.longitude = $event.latLng.lng()
		this.getAddress(this.latitude, this.longitude);
	}

	getAddress(latitude, longitude) {
		this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
			//   console.log(results);
			//   console.log(status);
			if (status === 'OK') {
				if (results[0]) {
					this.zoom = 12;
					this.address = results[0].formatted_address;
				} else {
					window.alert('No results found');
				}
			} else {
				window.alert('Geocoder failed due to: ' + status);
			}

		});
	}

	ngAfterViewInit() {
		// this.getCurrentPosition();
		// this.findAdress();
	}
	findAdress() {
		this.mapsAPILoader.load().then(() => {
			var autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
			autocomplete.addListener("place_changed", () => {
				this.ngZone.run(() => {
					// some details
					this.map_active = true;
					let place: google.maps.places.PlaceResult = autocomplete.getPlace();
					//set latitude, longitude and zoom
					this.latitude2 = place.geometry.location.lat();
					this.longitude2 = place.geometry.location.lng();
					this.zoom = 12;
				});
			});
		});
	}

	// async getCourier() {

	// 	let required = {
	// 		province_code: this.form.mprovince_code,
	// 		city_code: this.form.mcity_code,
	// 		subdistrict_code: this.form.mdistrict_code,
	// 		village_code: this.form.mvillage_code
	// 	}
	// 	console.log("required", required)


	// 	let result = await this.courierService.getCourier(required);
	// 	this.courierServices = result.result;
	// 	this.courierServices.forEach((el, i) => {
	// 		Object.assign(el.courier, { value: false });
	// 	});
	// }


	async getBankList() {
		console.log('bank')
		this.form.withdrawal_bank_name = JSON.stringify(this.form.withdrawal_bank_name)
		console.log(this.form.withdrawal_bank_name, " HEREEEEEE")
		this.bankList = []
		try {
			let result = await this.paymentGatewayService.getWithdrawalBankList();
			console.log(result)
			if (result.result) {
				this.bankList.push({ name: 'choose your bank', value: 'none' })
				result.result.forEach((element) => {
					this.bankList.push({ name: element.bank_name, value: JSON.stringify(element) })
				})
			}

			if (!this.form.withdrawal_bank_name || this.form.withdrawal_bank_name == '') {
				this.form.withdrawal_bank_name = this.bankList[0].value;
			}

		} catch (e) {
			console.log(((<Error>e).message))
		}
	}


	setCourierService(value) {

		for (let n in this.couriers.result) {

			if (value == this.couriers.result[n].courier_code) {
				this.couriers.result[n].value = true;
			}
		}
	}

	datePickerOnDateSelection(date: NgbDate){
		console.log(date, date.day)
		
	  }
	maxLength($event) {
		let value = this.form.mpostal_code
		if (value.length > 5) {
			console.log(value.length);
			this.form.mpostal_code = parseInt(value.toString().substring(0, 2));
			// this.form.mpostal_code = parseInt(value.toString().substring(0,2));
		}
		// console.log("test", value);
	}

	getValueCheckbox() {
		let getData = [];
		this.couriers.result.forEach((el, i) => {
			if (el.value == true) {
				getData.push(el);
			}
		});
		console.log("courier", getData)
		return getData;
	}

	async checkCourier(load) {
		let props = {
			province_code: this.form.mprovince_code,
			city_code: this.form.mcity_code,
			subdistrict_code: this.form.msubdistrict_code,
			village_code: this.form.mvillage_code
		}

		this.couriers = await this.courierService.getCourier(props)
		if(load == false){
			if (!this.couriers.result) {
				Swal.fire("Warning", "Your area is not supported by courier services", "warning")
		} else {
			Swal.fire("Success", "Please choose available couriers", "success")
		}
		}
		

		console.log("courier", this.couriers)
	}

	// onChecked(){
	//     let getData = [];
	//     this.courierServices.forEach((el) =>{
	//         var data = this.courierServices;
	//         console.log("result", data)
	//     })
	// }

	// async getKeyname() {
	// 	this.merchantGroup = await this.merchantService.configuration();
	// 	this.merchantGroup.result.merchant_group.forEach((element) => {
	// 		this.merchant_group.push({ label: element.name, value: element.name });
	// 	});
	// 	// this.form.merchant_group = this.merchant_group[0].value
	// }

	saveThis() {
		let data = this.formValidation();

		if (data != false) {
			this.proceedToSave();
		}
	}

	showMaps() {
		this.showMap = true;
		this.form.google_address = this.address,
			this.form.latitude = this.latitude,
			this.form.longitude = this.longitude,
			Swal.fire("Success", "Your Pickup Address Has Been Pointed on Google Map", "success")
	}
	closeMaps() {
		this.showMap = false;
		this.form.google_address= this.address = '',
			this.form.latitude = '',
			this.form.longitude = '',
			this.latitude = this.longitude = 0,
			this.map_active = false,
			Swal.fire("Warning", "Your Pickup Address Has Been Deleted", "warning")
	}
	async saveMerchant() {
		this.errorMessage = '';
		let courier = []
		console.log(" DOB ", this.form.dob)
		for (let n of this.couriers.result) {
			if (n.value == true) {
				courier.push(n.courier_code)
			}
		}

		this.form.available_courier = courier
		if (this.showMap == false) {
			delete this.form.latitude
			delete this.form.longitude
			delete this.form.google_address
		}
		// console.log('couriereer', this.form.available_courier)
		let data: any = JSON.parse(JSON.stringify(this.form));

		let save = {
			gender: this.form.gender
		}

		data.mpostal_code = data.mpostal_code.toString();

		try {
			let result
			if (this.form.withdrawal_bank_name && this.form.withdrawal_bank_name != "none") {

				data.withdrawal_bank_name = JSON.parse(this.form.withdrawal_bank_name)
				if(data.withdrawal_account_name){
					data.withdrawal_bank_name.account_name = data.withdrawal_account_name
					delete data.withdrawal_account_name
				}
			}
			else {
				data.withdrawal_bank_name = '';
			}
			data.available_courier = this.getValueCheckbox();
			if (this.form.permission == 'member') {
				const valid = this.formValidation();

				let resultMerchant = await this.merchantService.addMerchant(data);
			} else if (this.form.permission == 'merchant') {
				console.log(typeof this.form.mpostal_code)
				console.log(data)
				let result = await this.merchantService.updateMerchant(data);


			}
			Swal.fire("Success", "Your profile has been updated", "success")






		}
		catch (e) {
			setTimeout(() => {
				this.errorMessage = ((<Error>e).message)
			}, 500)
		}
		// window.location.reload();

	}

	async savePayment() {

		const data: any = JSON.parse(JSON.stringify(this.form));

		try {
			let save = await this.merchantService.updateMerchant(data);
			if (save.result) {
				alert("your profile has been saved")
			}
			console.log("result", save)
		}
		catch (e) {
			this.errorMessage = ((<Error>e).message)
		}
	}

	async proceedToSave() {
		this.errorMessage = '';
		
		const profile = {
			address: this.form.google_address,
			cell_phone: this.form.cell_phone,
			dob: this.form.dob,
			full_name: this.form.full_name,
			gender: this.form.gender,
			email: this.form.email
		}

		try {
			let res = await this.memberService.updateProfile(profile);
			if (res.result) {
				Swal.fire("Success", "Your profile has been updated", "success")
			}
			console.log("result", res)
		}
		catch (e) {
			this.errorMessage = ((<Error>e).message)

		}
	}

	nameInput($event) {
		// if (this.autoFill == true) {
		// 	let fill = this.form.merchant_name.toLowerCase().replace(/[\s\?\*]/g, '-');
		// 	this.form.merchant_username = fill;
		// }
	}

	formValidation(): boolean {
		console.log(this.form);
		return true;
	}

	openUploadLegalDocument() {
		this.upload = true;
		this.bukaTokoBtn = false;
	}

	openAddMerchantDetail() {
		this.upload = false;
		this.addMerchantDetail = true;
	}

	openStoreInfo() {
		this.openStore = true;
		this.storeInfoBtn = false;
	}

	openStoreProfile() {
		this.openStore = false;
		this.storeProfile = true;
	}



	getSubDistrictList($event) {
		let result = $event.target.value;
		console.log('ini biang kerok', $event)
		if (result.length > 2) {
			this.memberService.getSubDistrict(result, result, 'origin').then((result) => {
				this.subDistrictList = result.result;
				console.log('INI PROVINCE', this.subDistrictList)
			})
		}
	}



	getProvinceList($event) {
		let result = $event.target.value;

		if (result.length > 2) {
			this.memberService.getProvince(result, 'origin').then((result) => {
				this.provinceList = result.result;
				console.log('hasil reg', this.provinceList);
				this.onProvinceChange()
			})
		}
	}

	onProvinceChange() {
		console.log("province", this.form.mprovince, this.provinceList)
		this.form.mcity = this.form.mcity_code = this.form.msubdistrict = this.form.msubdistrict_code = this.form.mvillage = this.form.mvillage_code = ''
		this.provinceList.forEach((element) => {
			if (this.form.mprovince.toLowerCase() == element.province.toLowerCase()) {
				this.form.mprovince_code = element.province_code;
			}
			console.log("code", this.form.mprovince_code)
		})
	}

	// getCityList($event) {
	// 	let result = $event.target.value;

	// 	const payload = {
	// 		province_code: this.form.mprovince_code,
	// 		city: result

	// 	}
	// 	if (result.length > 2) {
	// 		this.memberService.getCity(result, origin, this.form.mprovince_code).then((result) => {
	// 			console.log(payload)
	// 			this.cityList = result.result;
	// 			console.log('hasil city', this.cityList)
	// 			this.onCityChange();
	// 		})
	// 	}
	// }

	// onCityChange() {
	// 	this.cityList.forEach((element) => {
	// 		if (this.form.mcity.toLowerCase() == element.city.toLowerCase()) {
	// 			this.form.mcity_code = element.city_code;
	// 		}
	// 	})
	// }
	getCityList($event) {
		console.log('sekarang di city list')
		let province_code = this.form.mprovince_code;
		let city = $event.target.value;
		console.log()
		if (city.length > 2) {
			this.memberService.getCity(city, 'origin', province_code).then((result) => {
				this.cityList = result.result;
				console.log('INI City', this.cityList);
				this.onCityChange();
			})
		}
	}

	onCityChange() {
		this.form.msubdistrict = this.form.msubdistrict_code = this.form.mvillage = this.form.mvillage_code = ''
		console.log('ON City Changed', this.form.mcity, this.cityList);
		this.cityList.forEach((element) => {
			if (this.form.mcity.toLowerCase() == element.city.toLowerCase()) {
				this.form.city_code = this.form.mcity_code = element.city_code;
			}
		});
		console.log("kode kota", this.form.city_code);
	}



	// getSubDistList($event) {
	// 	let result = $event.target.value
	// 	if (result.length > 2) {
	// 		this.memberService.getSubDistrict(result, origin, this.form.mcity_code).then((result) => {
	// 			this.subDistrictList = result.result
	// 			this.onSubChange();

	// 		})
	// 		console.log("district", this.subDistrictList)
	// 		console.log("dist", this.formEditable)
	// 	}
	// }


	// onSubChange() {
	// 	this.subDistrictList.forEach((element) => {
	// 		if (this.form.msubdistrict.toLowerCase() == element.subdistrict.toLowerCase()) {
	// 			this.form.msubdistrict_code = element.subdistrict_code
	// 		}
	// 	})
	// }
	getSubDistList($event) {
		let city_code = this.form.mcity_code;
		let result = $event.target.value;
		console.log('ini biang kerok', $event)
		if (result.length > 2) {
			this.memberService.getSubDistrict(result, 'origin', city_code).then((result) => {
				this.subDistrictList = result.result;
				console.log('INI Subdistrict', this.subDistrictList);
				this.onSubDistrictChange();
			})
		}
	}

	onSubDistrictChange() {
		this.form.mvillage = this.form.mvillage_code = ''
		console.log('ON Subdistrict Changed', this.form.msubdistrict, this.subDistrictList);
		this.subDistrictList.forEach((element) => {
			if (this.form.msubdistrict.toLowerCase() == element.subdistrict.toLowerCase()) {
				this.form.msubdistrict_code = this.form.msubdistrict_code = element.subdistrict_code;
			}
		});
		console.log("kode subdistrict", this.form.mcity_code);
	}

	getVillageList($event) {

		let result = $event.target.value;
		if (result.length > 2) {
			this.memberService.getVillage(result, 'origin', this.form.msubdistrict_code).then((result) => {
				this.villageList = result.result
			})

		}
		console.log("village", this.villageList)
	}

	onVillageChange() {
		this.villageList.forEach((element) => {
			if (this.form.mvillage.toLowerCase() == element.village.toLowerCase()) {
				this.form.mvillage_code = element.village_code;
				this.checkForm();
			}
		})
	}



	getRegionList($event) {
		let result = $event.target.value;
		console.log("ini contoh ", result)
		if (result.length > 2)
			this.memberService.getRegion(result, 'origin').then((result) => {
				this.regionList = result.result;
				console.log('hasil reg', this.regionList);
				this.onRegionChange()
			});
	}


	onRegionChange() {
		console.log('ON Region Changed', this.form.region_name, this.regionList);
		this.regionList.forEach((element) => {
			if (this.form.region_name.toLowerCase() == element.region_name.toLowerCase()) {
				this.form.region_code = element.region_code;
			}
		});
		console.log(this.form.region_code);
	}

	dateformater(params){

		var res = params.substring(0, 10);
		console.log('this is form.dob', params , res)
		return res
	}



	async onFileSelected(event, img) {
		var reader = new FileReader()
		try {
			this.errorFile = false;
			let fileMaxSize = 3000000; // let say 3Mb
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 3MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}

			reader.readAsDataURL(event.target.files[0])

			if (this.selectedFile) {
				const logo = await this.merchantService.upload(this.selectedFile, this, 'image', (result) => {
					console.log("hasil", result)
					this.callImage(result, img);
				});
			}
			console.log("test lah", this.callImage)

			// if (this.selectedFile) {
			// 	const background = this.merchantService.upload(this.selectedFile, this, 'image', (result) => {
			// 		this.callBackground(result);
			// 	});
			// }
			// console.log('test', this.callLogo);
			// console.log('test2', this.callBackground);
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}

	cancelThis() {
		this.cancel = !this.cancel;
	}

	callImage(result, img) {
		console.log('result upload logo ', result);
		this.form[img] = result.base_url + result.pic_big_path;
		console.log('This detail', this.form);
		// this.detail = {...this.detail, ...result}
	}

	// maxlength(){

	// }



	async verifyPin() {
		console.log("tes")
	}

	// regex(event){
	// 	this.pin.new_pin = this.pin.new_pin.replace(/d/g);
	// 	console.log("regex jalan")
	// }

	numeric(event) {
		let ch = String.fromCharCode(event.which);

		if (!(/[0-9]/).test(ch)) {
			event.preventDefault();
		}
	}


	// nextStep(){
	// 	this.retypePin = true;
	// }


	async verify() {
		const settingPin = this.pin;

		var password = settingPin.password.toString();
		var newPin = settingPin.new_pin.toString();
		var repeatNewPin = settingPin.repeat_new_pin.toString();

		// settingPin.password = settingPin.password.toString();


		// console.log(settingPin)

		console.log(password)
		console.log(newPin)
		console.log(repeatNewPin)


		try {
			if (!newPin) {
				alert("Input Your Pin")
			} else if (newPin) {
				this.retypePin = true;
			} else if (!repeatNewPin) {
				alert("Retype your pin")
			} else if (newPin != repeatNewPin) {
				alert("Your pin not matched")
			} else if (repeatNewPin == newPin) {
				this.passwordField = true;
			}


			// if(!settingPin.repeat_new_pin){
			// 	this.insertPassword = false;
			// } else {
			// 	this.insertPassword = true;
			// }

			// if(!settingPin.password){
			// 	this.
			// }



			let result = await this.memberService.setNewPin(settingPin)

			if (result.result) {
				if (this.form.access_pin == 'enabled') {
					// Swal.fire("Congratulations", "Your new PIN has been registered to LOCARD", "success");
					Swal.fire(
						{
							title: 'Congratulation',
							html: 'Your new PIN has been registered to LOCARD',
							imageUrl: 'assets/images/check.png',
							imageWidth: 120,
							imageHeight: 120
						}
					);
				} else {
					// Swal.fire("Congratulations", "Your PIN has been registered to LOCARD", "success");
					Swal.fire(
						{
							title: 'Congratulation',
							html: 'Your PIN has been registered to LOCARD',
							imageUrl: 'assets/images/check.png',
							imageWidth: 120,
							imageHeight: 120
						}
					);
				}
				this.retypePin = false;
				this.setPin = false;

				// this.router.navigate(['merchant-portal/profile']);
			}
		} catch (error) {
			console.log(((<Error>error).message))
		}

		// if(result = true){
		// 	this.closeDialog = true;
		// }

	}

	checkForm() {
		if (this.form.mprovince_code && this.form.mcity_code && this.form.msubdistrict_code && this.form.mvillage_code) {
			this.complete = true;
			this.checkCourier(false)
		} else {
			this.complete = false;
		}
	}

	openDialog() {
		// this.showModal = true;
		if (this.setPin == false) {
			console.log("berhasil")
			this.setPin = true;
		} else {
			console.log("error")
		}

	}

	closeDialog() {
		this.setPin = false;
		this.retypePin = false;
		this.passwordField = false;
		this.successPin = false;
	}

}
