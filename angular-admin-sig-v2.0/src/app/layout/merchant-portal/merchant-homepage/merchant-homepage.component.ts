import {Component, OnInit} from '@angular/core';
import {TableFormat} from '../../../object-interface/common.object';
import {ProductService} from '../../../services/product/product.service';
import {Router} from '@angular/router';
// import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';

@Component({
    selector: 'app-merchant-homepage',
    templateUrl: './merchant-homepage.component.html',
    styleUrls: ['./merchant-homepage.component.scss']
})
export class MerchantHomepageComponent implements OnInit {
    myProducts;
    myOrderHistory;

    constructor(
        private productService: ProductService,
        private router: Router
        // ,private orderHistory: OrderhistoryService
        ) {
    }

    ngOnInit() {
        this.firstLoad();
    }
    goGenuine() {
        this.router.navigate(['/merchant-portal/homepage/upgrade-genuine'])
    }
    async firstLoad() {
        let result = await this.productService.getProductsReport();
        // let result2 = await this.orderHistory.getOrderHistoryMerchant();
        if (result.result) {
            this.myProducts = result.result;
            // this.myOrderHistory = result2.result
            // console.log("ini",this.myOrderHistory)
        }

    }

}
