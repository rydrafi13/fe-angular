import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MerchantPortalRoutingModule} from './merchant-portal-routing.module';
import {MerchantHomepageComponent} from './merchant-homepage/merchant-homepage.component';
import {MerchantPortalComponent} from './merchant-portal.component';
import {MerchantPortalAppHeaderComponent} from './merchant-portal-app-header/merchant-portal-app-header.component';
import {MerchantPortalAppSidebarComponent} from './merchant-portal-app-sidebar/merchant-portal-app-sidebar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BsComponentModule} from '../modules/bs-component/bs-component.module';

import {FormBuilderTableModule} from '../../component-libs/form-builder-table/form-builder-table.module';
import {MerchantProductsComponent} from './separated-modules/merchant-products/merchant-products.component';

import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {ProfileComponent} from './profile/profile.component';
import {OrderHistoriesComponent} from './order-histories/order-histories.component';
import {PhotoProfileComponent} from '../../component-libs/photo-profile/photo-profile.component';
import {OrderDetailComponent} from './order-detail/order-detail.component';
import {OrderhistoryEditComponent} from '../modules/orderhistory/edit/orderhistory.edit.component';
import {OrderhistoryallhistoryEditComponent} from '../modules/orderhistoryallhistory/edit/orderhistoryallhistory.edit.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {OrderHistorySummaryComponent} from '../modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component';
import {PageHeaderComponent} from '../../shared/modules/page-header/page-header.component';
import {PageHeaderModule} from '../../shared';
import {OrderHistorySummaryModule} from '../modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.module';
import {OrderhistoryallhistoryModule} from '../modules/orderhistoryallhistory/orderhistoryallhistory.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbDatepicker, NgbCalendar, NgbDatepickerModule, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EvoucherListComponent} from './separated-modules/merchant-products/products/evoucher-list/evoucher-list.component';
import {MerchantSalesOrderComponent} from './merchant-sales-order/merchant-sales-order.component';
import {SalesorderComponent} from '../modules/salesorder/salesorder.component';
import {MerchantEvoucherSalesReportComponent} from './merchant-evoucher-sales-report/merchant-evoucher-sales-report.component';
import { MerchantPortalOrderhistoriesTabComponent } from './merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component';
import { EvoucherSalesReportModule } from '../modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.module';
import { PortalGuard } from '../../shared/guard/portal.guard';
import { ProductsModule } from './separated-modules/merchant-products/products.module';
import { EvoucherModule } from '../modules/product.evoucher/evoucher.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MerchantPortalBannerComponent } from './merchant-portal-banner/merchant-portal-banner.component';
import {MatTabsModule} from '@angular/material/tabs';
import { ShippingLabelComponent } from './order-histories/shipping-label/shipping-label.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import { MerchantPortalInboxComponent } from './merchant-portal-inbox/merchant-portal-inbox.component';
import { MerchantPortalInboxModule } from './merchant-portal-inbox/merchant-portal-inbox.module';
import { MerchantMyEscrowComponent } from './merchant-my-escrow/merchant-my-escrow.component';
import { MerchantWithdrawalComponent } from './merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component';
import { MerchantEscrowDetailComponent } from './merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component';
import { MatInputModule, MatIconModule, MatListModule } from '@angular/material';
import { NgOtpInputModule } from 'ng-otp-input';
import { AgmCoreModule} from '@agm/core';
import { UpgradeGenuineComponent } from './upgrade-genuine/upgrade-genuine.component';
import { MerchantPortalDetailInboxComponent } from './merchant-portal-detail-inbox/merchant-portal-detail-inbox.component';
import { ReportComponent } from '../modules/report/report.component'
import { ReportDetailComponent } from '../modules/report/detail/report.detail.component';
import { ReportEditComponent } from '../modules/report/edit/report.edit.component';
import { ReportExcelDetailComponent } from '../modules/report/detail/report.excel.detail.component';


@NgModule({
    declarations: [
        MerchantHomepageComponent,
        MerchantPortalComponent,
        MerchantPortalAppHeaderComponent,
        MerchantPortalAppSidebarComponent,
        MerchantProductsComponent,
        ProfileComponent,
        OrderHistoriesComponent,
        ShippingLabelComponent,
        PhotoProfileComponent,
        OrderDetailComponent,
        StatisticsComponent,
        MerchantSalesOrderComponent,
        MerchantPortalOrderhistoriesTabComponent,
        MerchantPortalBannerComponent,
        MerchantPortalInboxComponent,
        MerchantPortalDetailInboxComponent,
        MerchantMyEscrowComponent,
        MerchantWithdrawalComponent,
        MerchantEscrowDetailComponent,
        UpgradeGenuineComponent,
        ReportComponent,
        ReportEditComponent,
        ReportDetailComponent,
        ReportExcelDetailComponent,
        // ReceiptComponent,
        
    ],
    imports: [
        CommonModule,
        MerchantPortalRoutingModule,
        NgbDatepickerModule,
        BsComponentModule,
        FormBuilderTableModule,
        FormsModule,
        CKEditorModule,
        OrderHistorySummaryModule,
        ProductsModule,
        OrderhistoryallhistoryModule,
        PageHeaderModule,
        EvoucherSalesReportModule,
        EvoucherModule,
        MerchantPortalInboxModule,
        NgbModule,
        MatCheckboxModule,
        MatTabsModule,
        ReactiveFormsModule,
        NgxBarcodeModule,
        MatInputModule,
        MatIconModule,
        MatListModule,
        NgOtpInputModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyChHtMJJ6RE3Bg-SFeTKQvOsks7MzGQ5VA',
            libraries: ["places"]
        })
        // EvoucherSalesReportModule
    ],
    providers:[
        PortalGuard
    ]
})
export class MerchantPortalModule {
}
