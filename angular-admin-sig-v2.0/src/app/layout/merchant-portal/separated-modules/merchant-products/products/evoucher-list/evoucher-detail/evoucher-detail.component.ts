import { EVoucherService } from '../../../../../../../services/e-voucher/e-voucher.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { evoucherListDetail, TableFormat } from '../../../../../../../object-interface/common.object';
import { PermissionObserver } from '../../../../../../../services/observerable/permission-observer';

@Component({
  selector: 'app-evoucher-detail',
  templateUrl: './evoucher-detail.component.html',
  styleUrls: ['./evoucher-detail.component.scss']
})
export class EvoucherDetailComponent implements OnInit {


  service: any;
  tableFormat: TableFormat;
  productID;
  evoucherDetail;
  detail;
  currentPermission;
  totalPage: 0;
  // form: any = {
  //   merchant_username: '',
  //   product_code: '',
  //   expiry_date: ''
  // }

  constructor(private EVoucherService: EVoucherService, private router: Router, private route: ActivatedRoute, private permission: PermissionObserver, ) {
    this.permission.currentPermission.subscribe((val) => {
      this.currentPermission = val;

    })
    this.service = this.EVoucherService;

    if(this.currentPermission == 'merchant'){
      this.tableFormat = evoucherListDetail(this)
      this.tableFormat.label_headers.splice(0, 1)
      this.tableFormat.formOptions.addForm = false;  
      this.tableFormat.formOptions.customButtons;
      this.tableFormat.show_checkbox_options = true;
    }
    else{
      this.tableFormat = evoucherListDetail(this)
      this.tableFormat.label_headers.splice(0, 1)
      this.tableFormat.formOptions.addForm = false;  
      this.tableFormat.formOptions.customButtons;
      this.tableFormat.show_checkbox_options = true;
    }
  }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    this.route.queryParams.subscribe(async (params) => {
      // this.form.product_code = params.ev_prod_code;
      // this.form.expiry_date = params.exp_d;
      // this.form.merchant_username = params.m_username;
      //       this.productID = params.ev_prod_code;
      //       this.productID = params.expiry_date;
      //       this.productID = params.merchant_username;
      // // 
      let searchObject = {
        search: {
          ev_product_code: params.ev_prod_code,
          // expiry_date : params.exp_d,
          merchant_username: params.m_username
        }
      }

      let result = await this.EVoucherService.searchEvouchersLint(searchObject)

      this.evoucherDetail = result.result.values
      this.detail = result.result.values._id
      this.totalPage = result.result.total_page

      // let evoucherID = params.id
      // console.log(evoucherID);
      // let result = await this.EVoucherService.getEvoucherList(evoucherID)
      // this.evoucherDetail = result.result.values
    });
  }
  async evoucherStatus(listedData: any[]){
    if (listedData == undefined || listedData.length == 0) {
      return false;
    }
    let listOfData = [];

    listedData.forEach((element, index) => {
      listOfData.push(element._id);
    });
    listedData = [];
    if (listOfData.length == 0) return false;

    try {
      //Don't know the if it's needed an API or not
    } catch (error) {
      alert("error")
    }
  }

  async approval(listedData: any[]) {
    if (listedData == undefined || listedData.length == 0) {
      return false;
    }
    let listOfData = [];

    listedData.forEach((element, index) => {
      listOfData.push(element._id);
    });
    listedData = [];
    if (listOfData.length == 0) return false;

    try {

      let params = {
        voucher_id: listOfData,
        method: 1
      }
      this.service = this.EVoucherService;
      let result = await this.EVoucherService.approvedEvoucher(params);
      this.firstLoad();
      alert("the Current Data Below has been approved")
    }
    catch (error) {
      alert("error")
    }
  }
}
