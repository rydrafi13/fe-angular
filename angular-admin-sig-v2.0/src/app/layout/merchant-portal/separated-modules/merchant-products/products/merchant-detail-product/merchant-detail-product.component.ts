import { Component, OnInit } from '@angular/core';
import { formatCurrency } from '@angular/common';
import { ProductService } from '../../../../../../services/product/product.service';
import { isArray } from 'ngx-bootstrap/chronos';

import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CategoryService } from '../../../../../../services/category/category.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PermissionObserver } from '../../../../../../services/observerable/permission-observer';
import { EVoucherService } from '../../../../../../services/e-voucher/e-voucher.service';
import { MerchantService } from '../../../../../../services/merchant/merchant.service';
import { isString } from '../../../../../../object-interface/common.object';



@Component({
  selector: 'app-merchant-detail-product-product',
  templateUrl: './merchant-detail-product.component.html',
  styleUrls: ['./merchant-detail-product.component.scss'],
})

export class MerchantDetailProductComponent implements OnInit {

  // images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  genVoucher = {
    expiry_date: null,
    qty: 1
  };
  toggleDelete = false;
  oldProductCodeName;
  old_id;
  _id
  errorFile: any = false;
  progressBar;
  productID;
  selFile;
  cancel;
  selectedFile: any;
  generateVoucherToggler = 0;
  // edit_status = true
  edit = false;

  listOfProductType = [
    { name: 'Product', active: true },
    { name: 'evoucher', active: false }
  ]

  listOfRedeemType = [
    { name: 'Voucher Link', active: false },
    { name: 'Voucher Code', active: false }
  ]

  listOfVendorType = []
  listOfMerchant = []

  Editor = ClassicEditor;
  autoSKU = false;
  hashtagsKey = "";
  form: any = {
    type: 'product',
    min_order: 1,
    product_code: '',
    product_name: '',
    category: 'public',
    price: 0,
    fixed_price: 0,
    qty: 0,
    description: '',
    tnc: '',
    dimensions: {
      width: 0,
      length: 0,
      height: 0
    },
    weight: 1,
    weightInGram: 1000,
    pic_file_name: '',
    
  }
  labels = {
    type: 'product type',
    min_order: 'minimum order',
    product_code: 'product code',
    product_name: 'product name',
    category: 'category',
    price: 'price',
    fixed_price: 'public price',
    qty: 'quantity',
    description: 'description',
    tnc: 'term and conditions',
    dimensions: 'dimensions',
    weight: 'weight',

  }

  errorLabel: any = false;
  isFileUploaded = false;
  multipleCategories = [];
  currentPermission;

  constructor(private productService: ProductService,
    private categoryService: CategoryService,
    private router: Router,
    private route: ActivatedRoute,
    private permission: PermissionObserver,
    private eVoucherService: EVoucherService,
    private merchantService: MerchantService
  ) {
    this.permission.currentPermission.subscribe((val) => {
      this.currentPermission = val;

    })
  }

  async ngOnInit() {

    this.inLoadedProduct();

    let mGroup = await this.merchantService.getVoucherGroup()

    if (mGroup.result) {
      this.listOfVendorType = []
    }

    mGroup.result.forEach((el, i) => {
      this.listOfVendorType.push({ name: el.name })
    })
    // this.listOfVendorType.unshift({name: 'MANUAL', active:true})

    let params = {
      'type': 'merchant'
    }
    let mUsername = await this.merchantService.getMerchant(params);

    mUsername.result.values.forEach((el, i) => {
      this.listOfMerchant.push({ name: el.merchant_username })
    })

    this.setVendorType(this.listOfVendorType[0])
    this.setRedeemType(this.listOfRedeemType[0])
  }

  editThis(){
    this.router.navigate(['/merchant-portal/product-list/edit'], { queryParams: { id: this.productID } });
    console.log("param", this.productID)
  }


  setVendorType(object) {
    this.listOfVendorType.forEach((el) => {
      el.active = false;
    })
    console.log(this.listOfMerchant);
    object.active = true;
    this.form.merchant_group = object.name
  }

  setRedeemType(object) {
    this.listOfRedeemType.forEach((el) => {
      el.active = false;
    })
    object.active = true;
    this.form.redeem_type = object.name
  }

  inLoadedProduct() {
    // console.log(this.permission.getCurrentPermission());
    this.route.queryParams.subscribe(async (params) => {
      this.productID = params.id;
      let result = await this.productService.detailProducts(this.productID);
      let data = result.result[0];
      console.log("data", data)
      var html = data.description;
      var div = document.createElement('div')
      div.innerHTML = html;
      console.log("result text", div.innerText)
      data.description = div.innerText
      console.log("test", data.description)

      if (isString(data.hashtags)) {
        data.hashtags = []
      }

      if (!data.condition) {
        data.condition = 'new'
      }
      if (!data.hashtags) {
        data.hashtags = []
      }

      // console.log(params.newData);

      // console.log("HASHTAGS", data)
      this.oldProductCodeName = data.product_code;
      this.form = {
        type: data.type,
        merchant_group: data.merchant_group ? data.merchant_group : null,
        merchant_username: data.merchant_username ? data.merchant_username : null,
        min_order: data.min_order,
        product_code: data.product_code,
        product_name: data.product_name,
        category: data.category,
        price: data.price,
        fixed_price: data.fixed_price,
        qty: data.qty,
        description: data.description,
        tnc: data.tnc,
        dimensions: {
          width: data.dimensions ? data.dimensions.width : 0,
          length: data.dimensions ? data.dimensions.length : 0,
          height: data.dimensions ? data.dimensions.height : 0
        },
        condition: data.condition,
        weight: data.weight ? data.weight : 0,
        active: data.active,
        base_url: data.base_url,
        pic_big_path: data.pic_big_path,
        pic_small_path: data.pic_small_path,
        pic_medium_path: data.pic_medium_path,
        pic_file_name: data.pic_file_name,
        hashtags: data.hashtags

      };
      console.log(this.form)
      if (this.currentPermission == 'admin') {
        this.form.merchant_username = data.merchant_username
      }
      //by default weight is in Kg, need to convert it in gram
      this.form.weightInGram = this.form.weight * 1000;
      this.formPriceReplacer(null, 'price');
      this.formPriceReplacer(null, 'fixed_price');
      this.formPriceReplacer(null, 'weightInGram');

      this.afterProductLoaded();
      // console.log(result.result[0])
      // this.productData = result.result[0];
    });
  }

  genVoucherToggler(val) {
    this.generateVoucherToggler = val;

  }

  async afterProductLoaded() {
    let mCategories = await this.categoryService.getCategoryList();
    console.log(mCategories);
    mCategories.result.category_list.forEach((element, index) =>{
      if(this.form.category == element.code){
        this.form.category  = element.name
        console.log("test array", this.form.category[0])
      }
    } )
    // if (mCategories.result) {

      






        
      // console.log(mCategories);
      // mCategories.result.forEach((element:any )=> {
      //   if(this.form.category && isArray(this.form.category)){
      //     // console.log(this.form.category)
      //     this.form.category.forEach((el)=>{
      //     console.log(el, element.code, el == element.code);
      //       if(el == element.code){
      //         console.log(el, element.code);
      //         return ;
      //         }

      //         // element.value = true;
      //     })
      //   }
      //   else{
      //     element.value = false
      //   }
      // });
      // this.multipleCategories = mCategories.result.category_list;
      // console.log(this.multipleCategories)
    // }
  }
  addHashtags(newWord) {

    this.form.hashtags.push(newWord);
  }
  hashtagsKeyDown(event) {
    let n = event.target.textContent
    
    let key = event.key.toLowerCase();
    // console.log("key", key)
    if (key == " " || key == "enter") {

      if (n.trim() !== '') {
        this.addHashtags(n);
      }
      event.target.textContent = ''
      event.preventDefault();
    }

    if (key == 'backspace' && n == '' && this.form.hashtags.length > 0) {
      this.form.hashtags.pop();
    }

  }


  productNameInputed($event) {
    if (this.autoSKU == true) {
      let s = this.form.product_name.toLowerCase().replace(/[\s\?\*]/g, '-');
      this.form.product_code = s;
    }
  }



  async deleteThis() {
    let r = await this.productService.deleteProduct({ _id: this.productID });

    if (r.result) {
      alert("Your Product " + this.form.product_name + " Has been deleted");
      this.router.navigate(['/merchant-portal/product-list']);
      // window.location.reload();
    }
  }

  toggleDeleteConfirm(l: boolean) {
    this.toggleDelete = l;
  }

  formPriceReplacer($event, label: string) {
    if (this.form[label]) {
      if (typeof this.form[label] == 'number')
        this.form[label] = this.form[label].toString()

      let s = this.form[label].toLowerCase().replace(/[^0-9]/g, '');
      let c = parseInt(s);
      if (isNaN(c)) {
        this.form[label] = 0;
      }
      else {
        this.form[label] = c.toLocaleString('en');
      }
    }
  }

  onFileSelected(event) {
    this.isFileUploaded = false;

    this.errorLabel = false;
    let fileMaxSize = 3000000;// let say 3Mb
    var reader = new FileReader();

    reader.onload = (event: any) => {
      this.selFile = event.target.result;
    }

    reader.readAsDataURL(event.target.files[0]);

    Array.from(event.target.files).forEach((file: any) => {
      if (file.size > fileMaxSize) {
        this.errorLabel = "maximum file is 3Mb";
      }
    });
    // console.log(event.target.files, this.selFile);
    if (this.errorLabel == false) {
      this.selectedFile = event.target.files[0];

      setTimeout(() => {
        this.onUpload();
      }, 1500)
    }

  }

  // onFileSelected(event){
  //   this.errorFile = false;
  //   let fileMaxSize = 3000000;// let say 3Mb
  //   Array.from(event.target.files).forEach((file: any) => {
  //       if(file.size > fileMaxSize){
  //         this.errorFile="maximum file is 3Mb";
  //       }
  //   });
  //   this.selectedFile = event.target.files[0];

  // } 

  async onUpload() {
    try {
      this.cancel = false;
      let result
      console.log(this.selectedFile)
      if (this.selectedFile) {
        result = await this.productService.upload(this.selectedFile, this, 'image', (result) => { this.callAfterUpload(result) });
        console.log(result)
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    //   this.onUpload.
    return false;
  }

  async generateVoucher() {
    if (this.genVoucher.expiry_date && this.genVoucher.expiry_date.year) {
      let params = {
        expiry_date: this.genVoucher.expiry_date.year + "/" + this.genVoucher.expiry_date.month + "/" + this.genVoucher.expiry_date.day,
        qty: this.genVoucher.qty,
        product_code: this.form.product_code
      }
      await this.eVoucherService.generateEVoucherLint(params).then((result) => {
        if (result.result && result.result.status == 'success') {
          if (this.currentPermission == 'admin') {
            this.router.navigate(['administrator/evoucheradmin'])
          }
          else if (this.currentPermission == 'merchant') {
            this.router.navigate(['merchant-portal/product-list/generated-voucher/list'], )
          }
        }
      })

    }
  }

  generatedVoucher() {
    console.log("Test");
    if (this.currentPermission == 'admin') {
      this.router.navigate(['administrator/evoucheradmin'],{ queryParams: { id: this.form.product_code } })
      console.log("Admin");
    }
    else if(this.currentPermission == 'merchant') {
      this.router.navigate(['merchant-portal/product-list/generated-voucher/list'],{ queryParams: { id: this.form.product_code } })
      console.log("Merchant");
    }
  }

  callAfterUpload(result) {
    console.log("the result is here", result);
    if (result) {
      let product = this.form
      this.form = {
        ...product, ...result
      }

      this.isFileUploaded = true;

    }
  }

  stringToNumber(str: any) {
    if (typeof str == 'number') {
      str = str + '';
    }

    let s = str.toLowerCase().replace(/[^0-9]/g, '');

    return parseInt(s);
  }


  parseStringToArray(str: any) {
    let splitR;
    if (!isString(str)) return [];
    else {
      splitR = str.split(/[\,\.]/);
    }
    return splitR;
  }


  async saveThis() {
    if(this.currentPermission == 'merchant'){
      delete this.form.merchant_username;
      // this.form.tnc = "no tnc"
    }

    try {
      this.validateForm(this.form);
      let data = this.form;

      data.price = this.stringToNumber(data.price);
      data.fixed_price = this.stringToNumber(data.fixed_price);
      data.category = this.parseStringToArray(data.category);
      data.min_order = parseInt(data.min_order)

      data.weightInGram = this.stringToNumber(data.weightInGram);
      data.weight = data.weightInGram / 1000;

      data.dimensions = {
        length: parseFloat(data.dimensions.length),
        width: parseFloat(data.dimensions.width),
        height: parseFloat(data.dimensions.height),
      }

      if (data.currentPermission == 'admin' && this.form.merchant_username) {
        data.merchant_username = this.form.merchant_username
      }

      data.qty = parseInt(data.qty)
      data.category = [];
      data.weight = data.weightInGram / 1000;
      this.multipleCategories.forEach((el) => {
        if (el.value)
          data.category.push(el.code);
      })
      console.log("Data",data);
      await this.productService.updateProduct(data, this.productID);
      this.errorLabel = false;
      console.log(this.old_id);
      console.log(data);
      alert("Product/Voucher has been updated!");
      this.router.navigate(['/merchant-portal/product-list/']);

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
      console.log("ERROR", this.errorLabel);
    }
  }

  validateForm(form) {
    for (var prop in form) {

      if (!form.hasOwnProperty(prop) || prop == 'tnc' || prop == 'category' || prop == 'weightInGram' || prop == 'hashtags')
        continue;

      if (prop == 'base_url') {
        if (!form[prop]) {
          throw new Error("Please upload new image for your product ")
        }
      }
      // console.log(prop, form[prop] == undefined, form[prop] == '')
      if (form[prop] == undefined){
        console.log("PROP ERROR", prop, form)
        throw new Error("you need to completing this " + this.labels[prop])
      }

      if (isString(form[prop]) && form[prop].trim() == '')
        throw new Error("you need to completing this input " + this.labels[prop])

      if (isArray(form[prop]) && form[prop].length == 0) {
        console.log("PROP ERROR 2:", prop, form)
        throw new Error("Hi, you need to completing this " + this.labels[prop])

      }

    }
  }

  if(listOfProductType: 'product') {

  }

  toggleCategories(index) {
    this.multipleCategories.forEach((el) => {
      el.value = false;
    })
    this.multipleCategories[index].value = true;
  }

  async uploadFile() {
    try {
      if (this.selectedFile) {
        let res = await this.productService.uploadFile(this.selectedFile, this, 'evoucher', this.callAfterUpload);
        console.log(res)
      }
      
    } catch (e) {
      this.errorLabel = ((<Error>e).message);
    }
    if (this.selectedFile) { }
  }

  // editThis(){
  //   this.router.navigate(['/merchant-portal/product-list']);
  // }

  // setEdit(){
  //   if(this.edit_status){
  //     this.edit_status = false
  //   }
  //   else{
  //     this.edit_status = true
  //   }
  // }

  async back(){
    console.log("trure")
    this.router.navigate(['merchant-portal/product-list'])
  }

}

