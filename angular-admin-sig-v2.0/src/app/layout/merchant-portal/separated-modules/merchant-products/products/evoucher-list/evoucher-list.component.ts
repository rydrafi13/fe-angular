import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EVoucherService } from '../../../../../../services/e-voucher/e-voucher.service';
import { TableFormat, productsTableFormat, eVouchersTableFormat } from '../../../../../../object-interface/common.object';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-evoucher-list',
  templateUrl: './evoucher-list.component.html',
  styleUrls: ['./evoucher-list.component.scss']
})
export class EvoucherListComponent implements OnInit {
  generatedVouchers : any = [];
  closeResult: string;
  productID;
  evoucherDetail;
  service: any ;
  totalPage: 0;
  tableFormat : TableFormat;
  errorLabel : any = false
  popUpForm: boolean = false;
  data: any = [];
  inputedVoucher: number = 0;
  approveAll: boolean = false;

  constructor(private route:ActivatedRoute, 
              private router:Router,
              private evoucherService: EVoucherService,
              private modalService: NgbModal,
              ) {
                this.service      = this.evoucherService;
                this.tableFormat  = eVouchersTableFormat(this)

                // this.tableFormat.label_headers.splice(0,1)
                this.tableFormat.formOptions.addForm = false;
                this.tableFormat.formOptions.customButtons;
               }

  async ngOnInit() {
   
    this.inLoadedProduct();
   
  }

  inLoadedProduct(){
    // console.log(this.permission.getCurrentPermission());
    this.route.queryParams.subscribe( async (params) =>{
      this.productID    = params.id;
      let result = await this.evoucherService.getEvoucherStockSummary({id:params.id})
      console.log(result);
      
      this.totalPage = result.result.total_page;
      this.generatedVouchers = result.result.values
      console.log(this.generatedVouchers);
      this.evoucherDetail = result.result.values._id

    });
    // try{
     
    //   let rQuery = { 
    //         column_request :"",
    //         // search: {'type':'product'}
    //       }
          
    //   let request = JSON.stringify(rQuery);
    //   let queryParams = {
    //         request: request            
    //   }

    //   let result: any;
    //   this.service    = this.evoucherService;
    //   // result          = await this.evoucherService.getProductsReport(queryParams);
      
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  approveAllClicked(data){
    this.approveAll = true;
    this.approval(data);
  }

  async approval(evoucherData) {
    console.log(evoucherData);
    if (evoucherData == undefined) {
      return false;
    }
    
    let totalVoucher;
    let expiry_date = new Date(evoucherData.expiry_date)

    let month = (expiry_date.getMonth()+ 1).toString().padStart(2,"0");
    let day = (expiry_date.getDate() + 0).toString().padStart(2,"0")
    let stringDate = expiry_date.getFullYear()+'-'+month+'-'+day;

    if(this.inputedVoucher != 0 && this.approveAll == false){
      totalVoucher = this.inputedVoucher
    }
    else if(this.approveAll == true){
      this.approveAll = false;
      totalVoucher = evoucherData.qty_inactive
    }

    let payloadData = {
      product_code: evoucherData.ev_product_code,
      expiry_date:stringDate,
      qty: totalVoucher,
      method: 2
    };

    this.service = this.evoucherService;
    let result = await this.evoucherService.approvedEvoucher(payloadData);
    if (result.result.status == 'success') {
      alert("the Current Data Below has been approved");
      this.inLoadedProduct();
    }
  }


  async disabled(evoucherData){
    console.log(evoucherData);
    if (evoucherData == undefined) {
      return false;
    }

    let totalVoucher;
    
    let expiry_date = new Date(evoucherData.expiry_date)

    let month = (expiry_date.getMonth()+ 1).toString().padStart(2,"0");
    let day = (expiry_date.getDate() + 0).toString().padStart(2,"0")
    let stringDate = expiry_date.getFullYear()+'-'+month+'-'+day;
    
    if(this.inputedVoucher != 0){
      totalVoucher = this.inputedVoucher
    }
    else if(this.inputedVoucher == 0){
      totalVoucher = evoucherData.qty_inactive
    }

    console.log(this.inputedVoucher);


    let payloadData = {
      product_code: evoucherData.ev_product_code,
      expiry_date:stringDate,
      qty: totalVoucher,
      method: 1
    };

    console.log("payload", payloadData)
    this.service = this.evoucherService;
    let result = await this.evoucherService.approvedEvoucher(payloadData);
    if (result.result.status == 'success') {
      alert("The stock has been inactive");
      this.inLoadedProduct();
    }
  }

  openDetail(evoucher){
    this.data = evoucher
    console.log("result", evoucher)
    this.popUpForm = true;

  }

  popUpClose() {
    this.popUpForm = false;
  }

  public async callDetail(_id, evoucherData){
    this.openDetail(evoucherData)
    // this.openDetail(generatedVouchers)
  }

  async evoucherStatus(listedData: any[]){
    if (listedData == undefined || listedData.length == 0) {
      return false;
    }
    let listOfData = [];

    listedData.forEach((element, index) => {
      listOfData.push(element._id);
    });
    listedData = [];
    if (listOfData.length == 0) return false;

    console.log("listdata",listOfData)


    // try {
    //   //Don't know the if it's needed an API or not
    // } catch (error) {
    //   alert("error")
    // }
  }

  open(content){
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason:any): string {
    if(reason === ModalDismissReasons.ESC){
      return 'by pressing ESC'
    } else if(reason === ModalDismissReasons.BACKDROP_CLICK){
      return 'by clicking backdrop'
    } else {
      return ''
    }
  }

}
