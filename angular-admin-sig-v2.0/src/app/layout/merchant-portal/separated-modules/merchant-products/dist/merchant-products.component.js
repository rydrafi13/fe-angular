"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.MerchantProductsComponent = void 0;
var core_1 = require("@angular/core");
var common_object_1 = require("../../../../object-interface/common.object");
var MerchantProductsComponent = /** @class */ (function () {
    function MerchantProductsComponent(productService, router) {
        this.productService = productService;
        this.router = router;
        this.Products = [];
        this.form_input = {};
        this.errorLabel = false;
        this.orderBy = "newest";
        this.filteredBy = "order_id";
        this.orderByResult = { order_date: -1 };
        this.allMemberDetail = false;
        this.swaper = true;
        this.loading = false;
        this.filterBy = [
            { name: 'Order ID', value: 'order_id' },
            { name: 'Buyer Name', value: 'buyer_name' },
            { name: 'Product Name', value: 'product_name' },
            { name: 'No. Resi', value: 'no_resi' }
        ];
        this.sortBy = [
            { name: 'Newest', value: 'newest' },
            { name: 'Highest Transactions', value: 'highest transactions' },
            { name: 'Lowest Transactions', value: 'lowest transactions' }
        ];
        this.page = 1;
        this.totalPage = 0;
        this.pageSize = 50;
        this.searchProduct = {};
        this.service = this.productService;
        this.tableFormat = common_object_1.productsTableFormat(this);
        console.log(this.tableFormat);
        this.tableFormat.label_headers.splice(0, 1); // Remove the Table Format Label for Merchant Username
        this.tableFormat.formOptions.addForm = false;
        delete this.tableFormat.formOptions.customButtons;
    }
    MerchantProductsComponent.prototype.callDetail = function (product_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.openDetail(product_id);
                return [2 /*return*/];
            });
        });
    };
    MerchantProductsComponent.prototype.openDetail = function (product_id) {
        this.router.navigate(['merchant-portal/product-list/detail'], { queryParams: { id: product_id } });
    };
    MerchantProductsComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
    };
    MerchantProductsComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MerchantProductsComponent.prototype.addProduct = function () {
        console.log('add product clicked');
        this.router.navigate(['merchant-portal/add']);
    };
    MerchantProductsComponent.prototype.loadPage = function (page) {
        return __awaiter(this, void 0, void 0, function () {
            var search, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.loading = true;
                        if (!(page !== this.previousPage)) return [3 /*break*/, 4];
                        console.log('prev page', this.previousPage, page);
                        this.previousPage = page;
                        search = {
                            search: { type: 'product' },
                            order_by: null,
                            limit_per_page: 50,
                            current_page: page,
                            download: false
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.productService.searchProductsLint((search))];
                    case 2:
                        result = _a.sent();
                        if (result) {
                            this.Products = result.result.values;
                            console.log('product berubah ', this.Products);
                            this.loading = false;
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MerchantProductsComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var rQuery, request, result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        rQuery = {
                            column_request: "product_code,product_name,price,fixed_price,qty,active,type,approved,pic_small_path,pic_medium_path,pic_big_path,base_url"
                        };
                        request = JSON.stringify(rQuery);
                        result = void 0;
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.getProductsReport()];
                    case 1:
                        result = _a.sent();
                        console.log("isi produk", result);
                        this.valueAll = result.result.total_all_values;
                        this.Products = result.result.values;
                        this.totalPage = result.result.total_page;
                        console.log("result", this.Products);
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MerchantProductsComponent.prototype.search = function ($event) {
        return __awaiter(this, void 0, void 0, function () {
            var search, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        search = $event.target.value;
                        console.log("test", search);
                        this.searchProduct = {
                            current_page: 1,
                            download: false,
                            limit_per_page: 50,
                            order_by: {},
                            search: { product_name: search }
                        };
                        console.log("test");
                        return [4 /*yield*/, this.productService.searchProductsLint(this.searchProduct)];
                    case 1:
                        result = _a.sent();
                        this.Products = result.result.values;
                        console.log("tst", this.searchProduct);
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantProductsComponent.prototype.searchSortFilter = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var order_by, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('awdawd', params);
                        order_by = params;
                        this.searchProduct = {
                            current_page: 1,
                            download: false,
                            limit_per_page: 50,
                            order_by: order_by,
                            search: {}
                        };
                        console.log("test");
                        return [4 /*yield*/, this.productService.searchProductsLint(this.searchProduct)];
                    case 1:
                        result = _a.sent();
                        this.Products = result.result.values;
                        console.log("tst", this.searchProduct);
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantProductsComponent.prototype.orderBySelected = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                if (this.orderBy == "highest transactions") {
                    this.orderByResult = {
                        total_price: -1
                    };
                    this.searchSortFilter(this.orderByResult);
                    // this.valuechange({}, false, false)
                }
                else if (this.orderBy == "lowest transactions") {
                    this.orderByResult = {
                        total_price: 1
                    };
                    this.searchSortFilter(this.orderByResult);
                    // this.valuechange({}, false, false)
                }
                else if (this.orderBy == "newest") {
                    this.orderByResult = {
                        order_date: -1
                    };
                    this.searchSortFilter(this.orderByResult);
                    // this.valuechange({}, false, false)
                }
                return [2 /*return*/];
            });
        });
    };
    MerchantProductsComponent = __decorate([
        core_1.Component({
            selector: 'app-merchant-products',
            templateUrl: './merchant-products.component.html',
            styleUrls: ['./merchant-products.component.scss']
        })
    ], MerchantProductsComponent);
    return MerchantProductsComponent;
}());
exports.MerchantProductsComponent = MerchantProductsComponent;
