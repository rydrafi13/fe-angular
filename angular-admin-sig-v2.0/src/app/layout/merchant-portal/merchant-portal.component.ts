import {Component, OnInit, HostBinding} from '@angular/core';
import {trigger, state, transition, animate, style} from '@angular/animations';
import {slideInAnimation} from './animation';
import {RouterOutlet} from '@angular/router';

@Component({
    selector: 'app-merchant-portal',
    templateUrl: './merchant-portal.component.html',
    styleUrls: ['./merchant-portal.component.scss'],
    animations: [slideInAnimation]
})

export class MerchantPortalComponent implements OnInit {
    devclass = 'dev-mode-merchant-portal';
    currentState = 'initial';
    items = [];

    constructor() {
    }

    addItem(val) {
        this.items.push(val);
    }

    removeItem(n, items) {
        this.items.splice(n, 1);
    }

    changeState() {
        this.currentState = this.currentState === 'initial' ? 'final' : 'initial';
    }

    ngOnInit() {
    }

    ngDoCheck() {
        // console.log("ITS CHANGED");
        if (localStorage.getItem('devmode') == 'active') {
            this.devclass = 'dev-mode-merchant-portal';
        } else {
            this.devclass = 'prod-mode-merchant-portal';
        }
    }

    prepareRoute(outlet: RouterOutlet) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
    }
}
