import {Component, OnInit} from '@angular/core';
import {sidebarmenu} from './sidebarmenu';
import {logout} from '../../../object-interface/common.function';
import {Router} from '@angular/router';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
    selector: 'merchant-portal-app-sidebar',
    templateUrl: './merchant-portal-app-sidebar.component.html',
    styleUrls: ['./merchant-portal-app-sidebar.component.scss']
})
export class MerchantPortalAppSidebarComponent implements OnInit {

    sidebarMenu = sidebarmenu;
    
    constructor(private router: Router,
        private matIconRegistry: MatIconRegistry,
        private domSanitizer: DomSanitizer) {
            this.matIconRegistry.addSvgIcon(
                `icon-1`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-dashboard.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-2`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-account-balance-wallet.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-3`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-filter-list.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-4`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-headset.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-5`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-insert-photo.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-6`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-local-offer.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-7`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-local-shipping.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-8`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-payment.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-9`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-receipt.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-10`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-shopping-cart.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-11`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-view-carousel.svg")
              );
              this.matIconRegistry.addSvgIcon(
                `icon-12`,
                this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-email.svg")
              );


    }

    ngOnInit() {
        this.firstLoad();
    }

    firstLoad() {
        // if (this.sideMenu.length <= 7) {
        //     let nextMenu = {
        //         icon: '<i class="fa fa-sign-out-alt"></i>',
        //         label: 'Logout',
        //         router_link: '/merchant-portal',
        //         func: this.logout
        //     };
        //     this.sideMenu.push(nextMenu);
        // }
    }

    logout() {
        logout();
        this.router.navigate(['/']);
    }
}
