export const sidebarmenu = [
    // {
    //     icon: '<i class="icon-dashboard"></i>', label: 'Dashboard', router_link: '/merchant-portal', func: () => {
    //     }
    // },
    {
        icon: '<i class="icon-merchant"></i>',icons:'icon-1', label: 'Front Explorer', router_link: '/merchant-portal/homepage', func: () => {
        }
    },
    

    {
        icon: ' <i class="icon-email-history"></i>',icons:'icon-12', label: 'Inbox', router_link: '/merchant-portal/inbox', func: () => {
        }
    },
    
    {
        icon: '<i class="icon-order_history_merchant"></i>',icons:'icon-9', label: 'Order Histories', router_link: '/merchant-portal/order-histories', func: () => {
        }
    },
    {
        icon: '<i class="icon-my_rupiah"></i>', 
        icons:'icon-2',
        label: 'My Money', 
        router_link: '/merchant-portal/my-money', 
        func: () => {}
    },
    {
        icon: '<i class="icon-shopping-cart"></i>',icons:'icon-10', label: 'Sales Order', router_link: '/merchant-portal/sales-order', func: () => {
        }
    },
    // { icon: '<i class="fa fa-envelope"></i>', label: 'Inbox', router_link: '/merchant-portal' , func:()=>{}},
    {
        icon: '<i class="icon-product-list-merchant"></i>',icons:'icon-4', label: 'Product List', router_link: '/merchant-portal/product-list/', func: () => {
        }
    },
    // {
    //     icon: '<i class="icon-product-list-merchant"></i>', label: 'Report Product', router_link: '/merchant-portal/report', func: () => {
    //     }
    // },
    // {
    //     icon: '<i class="fa fa-chart-bar"></i>',
    //     label: 'Evoucher Report',
    //     router_link: '/merchant-portal/evoucher-sales-report',
    //     func: () => {
    //     }
    // },
    
    {
        icon: '<i class="icon-member"></i>',icons:'icon-7', label: 'Your Profile', router_link: '/merchant-portal/profile', func: () => {
        }
    },
    // {
    //     icon: '<i class="fa fa-chart-bar"></i>', label: 'Statistics', router_link: '/merchant-portal/statistics', func: () => {
    //     }
    // },
];
