import { routePath } from './../../routing.path';
import { query } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { TableFormat, salesOrderTableFormat, merchantSalesOrderTableFormat } from '../../../object-interface/common.object';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';

@Component({
  selector: 'app-merchant-sales-order',
  templateUrl: './merchant-sales-order.component.html',
  styleUrls: ['./merchant-sales-order.component.scss']
})
export class MerchantSalesOrderComponent implements OnInit {

 
  service         : any;
  errorLabel      : any;
  allData         : any = [];
  tableFormat: TableFormat = {
    title: 'Sales Order Page',
    label_headers: [
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },{ label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
      { label: 'Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
      { label: 'Price', visible: true, type: 'string', data_row_name: 'total_product_price' },
      // { label: 'Discount Price', visible: false, type: 'string', data_row_name: 'discount_price' },
      
      // { label: 'Email', visible: false, type: 'string', data_row_name: 'user_email' },
      // { label: 'Order Date', visible: false, type: 'string', data_row_name: 'order_date' },
      // { label: 'Username', visible: false, type: 'string', data_row_name: 'username' },
      // { label: 'Payment Method', visible: false, type: 'string', data_row_name: 'payment_method' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
      { label: 'Buyer Name', visible: true, type: 'string', data_row_name: 'buyer_name' },

      // unfinished
      // unfinished
      // unfinished
      // {
      //   label: 'Type', visible: false,
      //   options: ['product', 'voucher'],
      //   type: 'list', data_row_name: 'type'
      // },
      // { label: 'Payment Method', visible: true, type: 'string', data_row_name: 'payment_method' },
    ],
    row_primary_key: '_id',
    formOptions: {
      row_id: 'order_id',
      this: this,
      result_var_name: 'allData',
      detail_function: [this, 'callDetail'],
    },
    show_checkbox_options: true


  };
  productID;
  // salesOrder: any = []
  totalPage: 0;
  constructor(
    private router              : Router,
    private route               : ActivatedRoute,
    private orderhistoryService : OrderhistoryService 
  ) {
    // this.service = this.orderhistoryService;
    // this.tableFormat = merchantSalesOrderTableFormat(this)
    // this.tableFormat.formOptions.addForm       = false;
    // delete this.tableFormat.formOptions.customButtons;
   }

  ngOnInit() {
    this.inLoadedProduct()
  }

  async inLoadedProduct(){
    try{ 
      this.service = this.orderhistoryService;
      // let payload = {
      //   search: {},
      //   limit_per_page: 10,
      //   page: 1,
      //   order_by: this.orderByResult,
      // }
      let result = await this.orderhistoryService.getSalesOrdeReport()
      this.allData = result.result.values
      this.totalPage = result.result.total_page
      console.log("DISINI BRAY", result)

    }catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
    // this.route.queryParams.subscribe(async(params)=>{
     
    // })
  }

  callDetail(result){
    console.log(result)
  }

}
