import { Component, OnInit } from '@angular/core';
import { TableFormat, eVouchersSalesReportTableFormat } from '../../../object-interface/common.object';
import { ActivatedRoute, Router } from '@angular/router';
import { EVoucherService } from '../../../services/e-voucher/e-voucher.service';
import { PermissionObserver } from '../../../services/observerable/permission-observer';

@Component({
  selector: 'app-merchant-evoucher-sales-report',
  templateUrl: './merchant-evoucher-sales-report.component.html',
  styleUrls: ['./merchant-evoucher-sales-report.component.scss']
})
export class MerchantEvoucherSalesReportComponent implements OnInit {

  service: any;
  evouchers: any = [];
  tableFormat: TableFormat;
  errorLabel: any = false
  evoucherID;
  currentPermission;

  constructor(private route: ActivatedRoute,
    private permission: PermissionObserver,
    private evoucherService: EVoucherService,
    private router: Router, ) {
    this.service = this.evoucherService;
    this.tableFormat = eVouchersSalesReportTableFormat(this)

    this.tableFormat.label_headers.splice(0, 1)
    this.tableFormat.formOptions.addForm = false;
    delete this.tableFormat.formOptions.customButtons;

    this.permission.currentPermission.subscribe((val) => {
      this.currentPermission = val;
    })
  }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    let result = await this.evoucherService.getEvoucherSalesOrderReport()
    console.log("test", result);
    this.evouchers = result.result.values
    // this.evoucherID = this.evouchers._id

    // if()
    //   console.log(result);

    //   this.generatedVouchers = result.result.values
    //   console.log(this.generatedVouchers);
    //   this.evoucherDetail = result.result.values._id
  }

  callDetail(evoucher_id) {
    if (this.currentPermission == 'admin') {
      this.router.navigate(['administrator/productadmin/edit'], {queryParams: {id:evoucher_id}})
    }
    else if(this.currentPermission == 'merchant'){
      this.router.navigate(['merchant-portal/product-list/edit'], { queryParams: { id: evoucher_id } })
    }
  }
}
