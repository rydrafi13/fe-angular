import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantPortalDetailInboxComponent } from './merchant-portal-detail-inbox.component';

describe('MerchantPortalDetailInboxComponent', () => {
  let component: MerchantPortalDetailInboxComponent;
  let fixture: ComponentFixture<MerchantPortalDetailInboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchantPortalDetailInboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantPortalDetailInboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
