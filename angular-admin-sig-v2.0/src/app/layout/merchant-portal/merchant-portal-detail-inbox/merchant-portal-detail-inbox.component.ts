import { async } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';

@Component({
  selector: 'app-merchant-portal-detail-inbox',
  templateUrl: './merchant-portal-detail-inbox.component.html',
  styleUrls: ['./merchant-portal-detail-inbox.component.scss']
})
export class MerchantPortalDetailInboxComponent implements OnInit {


  order_id;

  constructor(private route: ActivatedRoute, private router:Router, private orderHistoryService: OrderhistoryService) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){
    this.route.queryParams.subscribe(async (params) =>{
      console.log("params", params)
      this.order_id = params.id;
    })
  }
  

}
