import { Component, OnInit } from '@angular/core';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

  orderhistoryDetail:any
  service
  errorLabel
  orderId

  constructor(
    private orderhistoryService: OrderhistoryService,
    private route: ActivatedRoute,
    private router:Router
    ) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){
    this.route.queryParams.subscribe( async (params) =>{
      console.log("params : ", params)
      this.orderId = params.id;
      // let result = await this.orderhistoryService.getDetailOrderhistory(request)
      // this.orderhistoryDetail = result.result[0]
      // console.log("hasil", this.orderhistoryDetail)
      // this.loadHistoryDetail(request)
    })
    // console.log('firstload', this.firstLoad)
  }

  // private async loadHistoryDetail(params: string){
  //   try{
  //     console.log("Test", params)
  //     this.service    = this.orderhistoryService;
  //     let result:any  = await this.orderhistoryService.getDetailOrderhistory(params);
  //     this.orderhistoryDetail = result.result[0];
  //     // console.log("hasil",this.orderhistoryDetail)
  //     console.log("hasil",this.orderhistoryDetail.status)
  //     this.orderhistoryDetail.merchant = true;
  //   }
  //   catch(e){
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }
  // }
}
