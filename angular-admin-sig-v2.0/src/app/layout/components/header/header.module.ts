import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { BrowserModule } from '@angular/platform-browser';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports:      [ 
    CommonModule,
    FormsModule, 
    TranslateModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  declarations: [ HeaderComponent ],
  exports:[ HeaderComponent ],
  bootstrap:    [ HeaderComponent ],
})
export class HeaderModule { }