import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { sidebarmenu } from './nested-sidebarmenu';

import * as crypto from 'crypto-js'
import { LocalStorageService } from '../../../services/storage-service-module/storage-service-module.service';
import Swal from 'sweetalert2';
import { ProgramNameService } from '../../../services/program-name.service';
import * as content from '../../../../assets/json/content.json';

@Component({
    selector: 'app-nested-sidebar',
    templateUrl: './nested-sidebar.component.html',
    styleUrls: ['./nested-sidebar.component.scss']
})
export class NestedSidebarComponent {
    isActive: boolean = false;
    collapsed: boolean = false;
    showMenu: string = '';
    pushRightClass: string = 'push-right';
    className: any;

    sideBarMenu: any = sidebarmenu;

    mci_project: any = false;

    paramAppLabel: any;

    public contentProgram : any = (content as any).default;

    public contentList : any = (content as any).default;

    @Output() collapsedEvent = new EventEmitter<boolean>();
    
    constructor(private translate: TranslateService, public router: Router, public lStorage: LocalStorageService, private activatedRoute: ActivatedRoute, private programNameService: ProgramNameService) {
        
        // let $secretKey = localStorage.getToken();
        // let encodedStr = JSON.stringify({data:'here'})
        // let AesCrypto = crypto.AES.encrypt(encodedStr, $secretKey);

        // let result = AesCrypto.toString();
        // let decrypt = crypto.AES.decrypt(result, $secretKey );

        // console.log("CRYPTO", result,decrypt.toString(crypto.enc.Utf8)) ;
        
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });

        const program = localStorage.getItem('programName');
        let _this = this;
        this.contentList.filter(function(element) {
            if(element.appLabel == program) {
                if(element.type == "reguler") {
                    _this.mci_project = true;
                } else {
                    _this.mci_project = false;
                }
            }
        });
    }

    ngOnInit() {
        this.paramAppLabel = this.activatedRoute.snapshot.queryParamMap.get('app_label');

        if(!this.paramAppLabel) this.paramAppLabel = localStorage.getItem('programName');
    }

    getDetailProgram(value) {
        let detailProgram = this.contentProgram.find(element => element.appLabel == value);
        return detailProgram && detailProgram.title ? detailProgram.title : "";
    }

    handleChangePage(value) {
        const programName = localStorage.getItem('programName');

        if(programName && programName != this.paramAppLabel) {

            Swal.fire({
                text: `Tab lain sedang mengakses program ${this.getDetailProgram(programName)}.`,
                icon: 'warning',
                confirmButtonText: `Tetap di ${this.getDetailProgram(this.paramAppLabel)}`,
                cancelButtonText: `Pindah ke ${this.getDetailProgram(programName)}`,
                cancelButtonColor:'#3086d6',
                showCancelButton: true,
            }).then((result) => {
                if(result.isConfirmed){
                    
                    this.programNameService.setData(this.paramAppLabel);
                    this.router.navigate([value]);
                } else {
                    this.programNameService.setData(programName);
                    this.paramAppLabel = localStorage.getItem('programName');
                    this.router.navigate([value]);
                }
            });
        } else if(this.paramAppLabel && !programName) {
            this.programNameService.setData(this.paramAppLabel);
            this.router.navigate([value]);
        } else {
            this.router.navigate([value]);
        }
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    toggleCollapsed() {
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
        localStorage.removeItem('devmode');
    }
    orderhistory(){
        // this.router.navigate(["merchant-portal/order-histories"])
    }
}
