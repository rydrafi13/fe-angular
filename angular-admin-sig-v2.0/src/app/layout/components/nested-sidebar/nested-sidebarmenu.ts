export let sidebarmenu ={
    "Order History":[
                    // {label: "Summary", routerLink:'/administrator/order-history-summary'},
                    {label: "All History", routerLink:'/administrator/orderhistoryallhistoryadmin'},
                    {label: "Processed", routerLink:'/administrator/orderhistorysuccessadmin'},
                    // {label: "Pending", routerLink:'/administrator/orderhistorypendingadmin'},
                    {label: "Cancel", routerLink:'/administrator/orderhistorycanceladmin'},
                    // {label: "Failed", routerLink:'/administrator/orderhistoryfailedadmin'},
                    {label: "Waiting", routerLink:'/administrator/orderhistorywaitingadmin'},
                    {label: "Completed", routerLink:'/administrator/orderhistorycompleteadmin'},
                    {label: "Return", routerLink:'/administrator/orderhistoryreturnadmin'},
                    // {label: "Error", routerLink:'/orderhistoryerroradmin'},
                    // {label: "Wait for confirm", routerLink:'/orderhistoryerroradmin'},
                ],
    "PO Number":[
                    {label: "Update", routerLink:'/administrator/po-bulk-update'},
                    {label: "Report", routerLink:'/administrator/po-report'},
                ],
    "Sales Redemption":[
                    {label: "Report", routerLink:'/administrator/salesredemption'},
                    {label: "Generate BAST", routerLink:'/administrator/salesredemptionbast'}
                ],
    
    "Sales Redemption MCI":[
        {label: "Report", routerLink:'/administrator/salesredemption'},
    ],

    "Member":[
                    // {label: "Summary", routerLink:'/administrator/membersummary'},
                    // {label: "All Members", routerLink:'/administrator/allmembersadmin'},
                    // {label: "Member Registration", routerLink:'/administrator/datamember'},
                    {label: "Report", routerLink:'/administrator/datamember'},
                    {label: "Form Member", routerLink:'/administrator/formmember'},
                    // {label: "Bulky Order", routerLink:'/administrator/order-bulk'},
                    // {label: "Member Point", routerLink:'/administrator/memberadmin'},
                    
                    // {label: "Guests", routerLink:'/administrator/guestadmin'},
                    // {label: "Memberships", routerLink:'/administrator/membershipadmin'},
                ],
    "Reportnew":[
        {label: "Order Report", routerLink:'/administrator/pointmovement'},
        {label: "Finance Invoice Report", routerLink:'/administrator/financeinvoicereport'},
    ],
    "Products":[
                    // {label: "Upload Product CSV", routerLink:'/administrator/testproductadmin'},
                    {label: "Add", routerLink:'/administrator/productadmin/add'},
                    {label: "Product", routerLink:'/administrator/productadmin'},
                    {label: "Evoucher", routerLink:'/administrator/vouchers'},
                ],
    "Category":[
        {label: "Add", routerLink:'/administrator/category/add'},
        {label: "Category", routerLink:'/administrator/categoryadmin'},
    ],
    "Payment":[
                    {label: "Payment Gateway", routerLink:'/administrator/paymentgatewayadmin'}
                ],
    "Loyalty":[
                    // {label: "Points Model Group", routerLink:'/administrator/pointsgroup'},
                    {label: "Points Models Setup", routerLink:'/administrator/pointsmodels'},
                    // {label: "Campaign Management", routerLink:'/administrator/pointscampaign'},
                    // {label: "Promo Campaign", routerLink:'/administrator/promocampaign'},
                ],
    "Point Movement":[
                    {label: "Based on Product", routerLink:'/administrator/pointmovement'},
                    // {label: "Based on Order", routerLink:'/administrator/pointmovementorder'},
                ],
    "Notification":[
                    {label: "Notification Group", routerLink:'/administrator/notificationgroupadmin'},
                    {label: "Notification Setting", routerLink:'/administrator/notificationsettingadmin'},
                    {label: "Notification Broadcast", routerLink:'/administrator/notificationbroadcastadmin'},
                    {label: "Notification Message", routerLink:'/administrator/notificationmessageadmin'},
                ],
    "Evoucher":[
                    {label: "Evoucher Stock Report", routerLink:'/administrator/evoucherstockreport'},
                    {label: "Evoucher Summary Report", routerLink:'/administrator/evouchersummaryreport'},
                    {label: "Sales Report", routerLink:'/administrator/evouchersales'},
                    // {label: "Evoucher Sales Report", routerLink:'/administrator/evouchersalesreport'},
                ],
    "Shipping":[
        // {label: "Report", routerLink:'/administrator/shippingreport'},
        // {label: "Process Shipping", routerLink:'/administrator/shippingprocess'},
        {label: "Process Shipping", routerLink:'/administrator/deliveryprocess'},
        {label: "Packing List", routerLink:'/administrator/packinglist'},
        {label: "Update Shipping Status", routerLink:'/administrator/shippingbulkprocess'}

        // {label: "Report", routerLink:'/administrator/evouchersalesreport'},
        // {label: "Shipping Processing", routerLink:'/administrator/evoucheradmin'},
    ],
    "E-Wallet":[
        {label: "Sales Report", routerLink:'/administrator/ewalletsalesreport'},
    ],
    "Report":[
                    {label: "Sales Summary report", routerLink:'/administrator/order-history-summary'},
                    {label: "Member Demography", routerLink:'/administrator/member-demography-report'},
                    {label: "Sales Order", routerLink:'/administrator/salesorder'},
                    {label: "Traffic Activity", routerLink:'/administrator/activity-report'},
                    {label: "Product Activity", routerLink:'/administrator/product-activity-report'},
                    {label: "Report", routerLink:'/administrator/reportadmin'},
                    {label: "Report Error", routerLink:'/administrator/reporterror'},
                    {label: "CRR", routerLink:'/administrator/crr'},
                    {label: "CNR", routerLink:'/administrator/cnr' },
                ],

    "Product":[
        {label:"Product", routerLink:'/administrator/product'},
        {label:"Evoucher", routerLink:'/administrator/evoucher'}
    ]
};