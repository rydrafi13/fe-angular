import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminManagementRoutingModule } from './admin-management-routing.module';
import { AdminManagementComponent } from './admin-management.component';
import { AdminSidebarComponent } from '../components/admin-sidebar/admin-sidebar.component';
import { HeaderModule } from '../components/header/header.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../component-libs/form-builder-table/form-builder-table.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsComponentModule } from '../modules/bs-component/bs-component.module';
// import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
// import { OrderhistoryallhistoryModule } from '../modules/orderhistoryallhistory/orderhistoryallhistory.module';

@NgModule({
  declarations: [AdminManagementComponent, AdminSidebarComponent],
  imports: [
    CommonModule,
    AdminManagementRoutingModule,
    NgbDropdownModule,
    TranslateModule,
    FormBuilderTableModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    // FormBuilderTableModule,
    BsComponentModule,
    // CKEditorModule,
    // OrderhistoryallhistoryModule,
    HeaderModule
  ]
})
export class AdminManagementModule { }
