import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminManagementComponent } from './admin-management.component';
import { adminManagementRoutePath } from './admin-management-route-path';

const routes: Routes = [
  {path: '',  component: AdminManagementComponent, children: adminManagementRoutePath },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminManagementRoutingModule { }
