import { Component, ViewEncapsulation, ViewChild, ElementRef, PipeTransform, Pipe, OnInit, HostBinding } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    host: {'class': ''}
})
export class AppComponent implements OnInit {
    @HostBinding('class') developmentMode ="development-mode";

    constructor() {
    }

    ngOnInit() {
        // console.log("ngoninit", localStorage.getItem('devmode'));
       
    }

    ngDoCheck(){
        // console.log("ITS CHANGED");  
        if(localStorage.getItem('devmode')=='active'){
            this.developmentMode = 'development-mode';
        }
        else{
            this.developmentMode = 'production';
        }
    }
}
