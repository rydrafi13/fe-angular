export const logout = function (){
    try{
      localStorage.removeItem('isLoggedin');
      localStorage.removeItem('tokenlogin');
      localStorage.removeItem('devmode');
    }
    catch(e){
      
    }
  }

export const clearSession = function(){
  logout();
}

export const isFunction  = function(f){
  return f instanceof Function;
  
}

export const stripTags = function(value){
  var html = value;
          var div = document.createElement('div');
          div.innerHTML = html;
          
          return div.innerText
}