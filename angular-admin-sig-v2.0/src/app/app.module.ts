import { VersioningComponent } from './layout/modules/versioning/versioning.component';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard,AuthInterceptor } from './shared';

import { FeedService } from './services/feed.service';
import { FormDetailComponent } from './component-libs/form-detail/form-detail.component';
import { MasterService } from './services/master.service';
import { MemberService } from './services/member/member.service';
import { AllMemberComponent } from './layout/modules/admin_members/allmember/allmember.component';
import { ArticleService } from './services/article/article.service';
import { BannerService } from './services/banner/banner.service';
import { CategoryService } from './services/category/category.service';
import { EmailService } from './services/email/email.service';
import { MediaService } from './services/media/media.service'; 
import { MembershipService } from './services/membership/membership.service';
import { NotificationService } from './services/notification/notification.service';
import { BroadcastService } from './services/notification.broadcast/broadcast.service';
import { NotificationsettingService } from './services/notificationsetting/notificationsetting.service';
import { NotificationGroupDetailComponent } from './layout/modules/notificationmessage/detail/notificationgroup.detail.component';
import { NotificationGroupEditComponent } from './layout/modules/notificationmessage/edit/notificationgroup.edit.component';
import { OrderhistoryService } from './services/orderhistory/orderhistory.service';
import { OutletsService } from './services/outlets/outlets.service';
import { PaymentGatewayService } from './services/paymentgateway/paymentgateway.service';
import { PointsCampaignService } from './services/points.campaign/pointscampaign.service';
import { PointsGroupService } from './services/points.group/pointsgroup.service';
import { PointsModelsService } from './services/points.models/pointsmodels.service';
import { ProductService } from './services/product/product.service';
import { ReportService } from './services/report/report.service';
import { ShoppingcartService } from './services/shoppingcart/shoppingcart.service';
import { ProductReportService } from './services/product-report/product-report.service';
import { LoginService } from './services/login/login.service';
import { RegisterService } from './services/register/register.service';
import { AllMemberDetailComponent } from './layout/modules/admin_members/allmember/detail/allmember.detail.component';
import { AllMemberModule } from './layout/modules/admin_members/allmember/allmember.module';
import { FormBuilderTableModule } from './component-libs/form-builder-table/form-builder-table.module';
import { MemberSummaryModule } from './layout/modules/admin_members/member-summary/membersummary.module';
import { ReporterrorService } from './services/reporterror/reporterror.service';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { LocalStorageService } from './services/storage-service-module/storage-service-module.service';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { AgmCoreModule} from '@agm/core';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { versioningService } from './services/versioning/versioning.service';
import { MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import { LoginMerchantComponent } from './login-merchant/login-merchant.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AdminStockModule } from './layout/modules/admin-stock.module';
import { MemberPointHistoryComponent } from './layout/modules/member/detail/member-point-history/member-point-history.component'
import { ProgramNameService } from './services/program-name.service';
// import { Member } from './layout/modules/member/detail/member.detail.module';

// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
    // Ng2CompleterModule,
        MatMenuModule,
        MatIconModule,
        NgbModule,
        UiSwitchModule,
        StorageServiceModule,
        CommonModule,
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AutocompleteLibModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        AllMemberModule,
        FormBuilderTableModule,
        AdminStockModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyChHtMJJ6RE3Bg-SFeTKQvOsks7MzGQ5VA',
            libraries: ["places"]
        })
        // Member.DetailModule
    ],
    declarations: [
        AppComponent,
        FormDetailComponent,
        // ProductDetailComponent,
        // AllMemberComponent, 
        // AllMemberDetailComponent,
        // MemberPointHistoryComponent,
        NotificationGroupDetailComponent,
        NotificationGroupEditComponent,
        // ResetPasswordComponent,
        // LoginMerchantComponent,
        // ProfileComponent
        // PaymentGatewayService,
        // PointsCampaignService,
        // PointsGroupService,
        // PointsModelsService,
        // ProductService,
        // ReportService,
        // MasterService, 
        // MemberService,
        // ShoppingcartService,
        // ProductReportService,
        // ProductService,
        // RegisterService,
        // OrderhistoryService,
        // OutletsService, 
        // EmailService,
        // NotificationService,
        // BroadcastService,
        // MembershipService,
        // NotificationsettingService,
        // ReporterrorService,
        // versioningService,
        // VersioningComponent
        // LocalStorageComponent,
        // ReporterrorComponent,
                
    ],
    providers: [AuthGuard,LocalStorageService,
       {
           provide: HTTP_INTERCEPTORS,
           useClass: AuthInterceptor,
           multi: true
       },

       ProgramNameService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
