export const environment = {
  production: true
};

export function api_url(){
  return getHost()+'v1/';
  // return 'http://localhost/locard/f3/v1/';
}

export function getHost(){
  if(localStorage.getItem('devmode')== 'active'){
      // return 'https://api2.dev.locard.co.id/f3/';
      return 'http://api.dev.retailer1.form-sig.id/';
      
  }
  else{
      return 'https://be.reals.form-sig.id/';
  }
  //  return 'https://api2.locard.co.id/f3/';
  return 'https://192.168.56.4/f3/';
}

export function getAPIKeyHeader(){
  //Locard API Key
  let Api_Key = '87E12038912E1E8387F99677EA16387B6FB6DE93929055D519561DB4AD5149A6'
  let customHeaders
  // return customHeaders

  if(localStorage.getItem('devmode') == 'active'){
       Api_Key = 'A490D9FD3F1DB33C94C7E484BB03CBE93C4830EE9D4389F78070ED24C9AC1DBF'
       customHeaders = {'Api_key': Api_Key }
       return customHeaders
  } else {
       Api_Key = '87E12038912E1E8387F99677EA16387B6FB6DE93929055D519561DB4AD5149A6'
      customHeaders = { 'Api_key': Api_Key } 
  } return customHeaders
}

export function getLink(){
  const program = localStorage.getItem('programName');
  return 'https://redemption.reals.cls-indo.co.id';

  // if(localStorage.getItem('devmode')== 'active'){
  //   if(localStorage.getItem('programName')== 'dynamix') {
  //     return 'http://dev.dynamix.cls-indo.co.id/';
  //   } else if (localStorage.getItem('programName')=='MCI') {
  //     return 'http://dev.mci.cls-indo.co.id/';
  //   }
  // }
  // else{
  //   if(localStorage.getItem('programName')== 'dynamix') {
  //     return 'https://dynamix.cls-indo.co.id/';
  //   } else if (localStorage.getItem('programName')=='MCI') {
  //     return 'https://mci.cls-indo.co.id/';
  //   }
  // }
}



export function getTokenHeader(){
  const myToken = localStorage.getItem('tokenlogin')
  const customHeaders = { 'Authorization': myToken }
  return customHeaders
}

export function getCustomTokenHeader(app_label) {
  const myToken = localStorage.getItem('tokenlogin')
  const customHeaders = { 'Authorization': myToken, 'app-label': app_label }
  return customHeaders
}

export function getTokenHeaderRetailer() {
  const myToken = localStorage.getItem('tokenlogin');
  const program = localStorage.getItem('programName');
  // console.warn("program", program);

  let appLabel: any;
  if (program) {
    appLabel = program;
  }

  const customHeaders = { 'Authorization': myToken, 'app-label': appLabel }
  
  return customHeaders
}

export function errorConvertToMessage(errorMessage: string){
  errorMessage = errorMessage.replace("Error:", "");
  let errorJSON:any = JSON.parse(errorMessage);
  return errorJSON.error;
}
