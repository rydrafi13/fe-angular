(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["admin-management-admin-management-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/admin-management/admin-management.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/admin-management/admin-management.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<app-admin-sidebar (collapsedEvent)=\"receiveCollapsed($event)\"></app-admin-sidebar>\r\n<section class=\"main-container\" [ngClass]=\"{collapsed: collapedSideBar}\">\r\n    <router-outlet></router-outlet>\r\n</section>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/components/admin-sidebar/admin-sidebar.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/components/admin-sidebar/admin-sidebar.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"sidebar\" [ngClass]=\"{sidebarPushRight: isActive, collapsed: collapsed}\" xmlns=\"\">\r\n  <div class=\"list-group\">\r\n    <div class=\"nested-menu child-menu\">\r\n      <li class=\"new-line\">\r\n        <a routerLink=\"/admin-management/admin\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n          <span class=\"new-line\">{{ 'Admin' | translate }}</span>\r\n        </a>\r\n    \r\n        <a routerLink=\"/admin-management/admin-group\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n          <span class=\"new-line\">{{ 'Admin Group' | translate }}</span>\r\n        </a>\r\n      </li>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/layout/admin-management/admin-management-route-path.ts":
/*!************************************************************************!*\
  !*** ./src/app/layout/admin-management/admin-management-route-path.ts ***!
  \************************************************************************/
/*! exports provided: adminManagementRoutePath */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "adminManagementRoutePath", function() { return adminManagementRoutePath; });
var adminManagementRoutePath = [
    { path: 'admin', loadChildren: '../modules/admin/admin.module#AdminModule', label: '' },
    { path: 'admin-group', loadChildren: '../modules/admin-group/admin-group.module#AdminGroupModule', label: '' },
];


/***/ }),

/***/ "./src/app/layout/admin-management/admin-management-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/layout/admin-management/admin-management-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: AdminManagementRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminManagementRoutingModule", function() { return AdminManagementRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_management_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-management.component */ "./src/app/layout/admin-management/admin-management.component.ts");
/* harmony import */ var _admin_management_route_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin-management-route-path */ "./src/app/layout/admin-management/admin-management-route-path.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: '', component: _admin_management_component__WEBPACK_IMPORTED_MODULE_2__["AdminManagementComponent"], children: _admin_management_route_path__WEBPACK_IMPORTED_MODULE_3__["adminManagementRoutePath"] },
];
var AdminManagementRoutingModule = /** @class */ (function () {
    function AdminManagementRoutingModule() {
    }
    AdminManagementRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AdminManagementRoutingModule);
    return AdminManagementRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/admin-management/admin-management.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/layout/admin-management/admin-management.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  transition: margin-left 0.2s ease-in-out;\n  font-family: \"Poppins\", sans-serif;\n}\n\n.main-container {\n  top: 0px;\n  margin-left: 200px;\n  padding: 40px 15px 0;\n  -ms-overflow-x: hidden;\n  overflow-x: hidden;\n  overflow-y: scroll;\n  overflow: hidden;\n  background-color: #252B37;\n  height: 100%;\n}\n\n.collapsed {\n  margin-left: 100px;\n}\n\n@media screen and (max-width: 992px) {\n  .main-container {\n    margin-left: 0px;\n    padding-top: 40px;\n    margin-top: -10vw;\n  }\n}\n\n@media print {\n  .main-container {\n    margin-top: 0px !important;\n    margin-left: 0px !important;\n  }\n}\n\n.multiselect-dropdown .dropdown-btn {\n  background-color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2FkbWluLW1hbmFnZW1lbnQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxhZG1pbi1tYW5hZ2VtZW50XFxhZG1pbi1tYW5hZ2VtZW50LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvYWRtaW4tbWFuYWdlbWVudC9hZG1pbi1tYW5hZ2VtZW50LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBS0ksd0NBQUE7RUFDQSxrQ0FBQTtBQ0NKOztBREdBO0VBQ0ksUUFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtBQ0FKOztBREVBO0VBQ0ksa0JBQUE7QUNDSjs7QURDQTtFQUNJO0lBQ0ksZ0JBQUE7SUFDQSxpQkFBQTtJQUNBLGlCQUFBO0VDRU47QUFDRjs7QURBQTtFQUNJO0lBQ0ksMEJBQUE7SUFDQSwyQkFBQTtFQ0VOO0FBQ0Y7O0FEQ0E7RUFDSSx1QkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L2FkbWluLW1hbmFnZW1lbnQvYWRtaW4tbWFuYWdlbWVudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIioge1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBtYXJnaW4tbGVmdCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gICAgLW1vei10cmFuc2l0aW9uOiBtYXJnaW4tbGVmdCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gICAgLW1zLXRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgICAtby10cmFuc2l0aW9uOiBtYXJnaW4tbGVmdCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gICAgdHJhbnNpdGlvbjogbWFyZ2luLWxlZnQgMC4ycyBlYXNlLWluLW91dDtcclxuICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucycsIHNhbnMtc2VyaWY7XHJcbiAgICAvLyBmb250LXdlaWdodDogMjAwO1xyXG59XHJcblxyXG4ubWFpbi1jb250YWluZXIge1xyXG4gICAgdG9wOiAwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMjAwcHg7XHJcbiAgICBwYWRkaW5nOiA0MHB4IDE1cHggMDtcclxuICAgIC1tcy1vdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI1MkIzNztcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uY29sbGFwc2VkIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMDBweDtcclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xyXG4gICAgLm1haW4tY29udGFpbmVyIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiA0MHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0xMHZ3O1xyXG4gICAgfVxyXG59XHJcbkBtZWRpYSBwcmludCB7XHJcbiAgICAubWFpbi1jb250YWluZXIge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwcHggIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuLm11bHRpc2VsZWN0LWRyb3Bkb3duIC5kcm9wZG93bi1idG4ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcblxyXG5cclxuIiwiKiB7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogbWFyZ2luLWxlZnQgMC4ycyBlYXNlLWluLW91dDtcbiAgLW1vei10cmFuc2l0aW9uOiBtYXJnaW4tbGVmdCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtbXMtdHJhbnNpdGlvbjogbWFyZ2luLWxlZnQgMC4ycyBlYXNlLWluLW91dDtcbiAgLW8tdHJhbnNpdGlvbjogbWFyZ2luLWxlZnQgMC4ycyBlYXNlLWluLW91dDtcbiAgdHJhbnNpdGlvbjogbWFyZ2luLWxlZnQgMC4ycyBlYXNlLWluLW91dDtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiLCBzYW5zLXNlcmlmO1xufVxuXG4ubWFpbi1jb250YWluZXIge1xuICB0b3A6IDBweDtcbiAgbWFyZ2luLWxlZnQ6IDIwMHB4O1xuICBwYWRkaW5nOiA0MHB4IDE1cHggMDtcbiAgLW1zLW92ZXJmbG93LXg6IGhpZGRlbjtcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICBvdmVyZmxvdy15OiBzY3JvbGw7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNTJCMzc7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLmNvbGxhcHNlZCB7XG4gIG1hcmdpbi1sZWZ0OiAxMDBweDtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLm1haW4tY29udGFpbmVyIHtcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgIHBhZGRpbmctdG9wOiA0MHB4O1xuICAgIG1hcmdpbi10b3A6IC0xMHZ3O1xuICB9XG59XG5AbWVkaWEgcHJpbnQge1xuICAubWFpbi1jb250YWluZXIge1xuICAgIG1hcmdpbi10b3A6IDBweCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbi1sZWZ0OiAwcHggIWltcG9ydGFudDtcbiAgfVxufVxuLm11bHRpc2VsZWN0LWRyb3Bkb3duIC5kcm9wZG93bi1idG4ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/admin-management/admin-management.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/layout/admin-management/admin-management.component.ts ***!
  \***********************************************************************/
/*! exports provided: AdminManagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminManagementComponent", function() { return AdminManagementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/observerable/permission-observer */ "./src/app/services/observerable/permission-observer.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminManagementComponent = /** @class */ (function () {
    function AdminManagementComponent(route, castPermission) {
        var _this = this;
        this.route = route;
        this.castPermission = castPermission;
        this.castPermission.currentPermission.subscribe(function (permissionVal) {
            if (permissionVal != 'admin') {
                _this.route.navigateByUrl("/");
            }
        });
    }
    AdminManagementComponent.prototype.ngOnInit = function () { };
    AdminManagementComponent.prototype.receiveCollapsed = function ($event) {
        this.collapedSideBar = $event;
    };
    AdminManagementComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__["PermissionObserver"] }
    ]; };
    AdminManagementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-administrator',
            template: __webpack_require__(/*! raw-loader!./admin-management.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/admin-management/admin-management.component.html"),
            styles: [__webpack_require__(/*! ./admin-management.component.scss */ "./src/app/layout/admin-management/admin-management.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__["PermissionObserver"]])
    ], AdminManagementComponent);
    return AdminManagementComponent;
}());



/***/ }),

/***/ "./src/app/layout/admin-management/admin-management.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/layout/admin-management/admin-management.module.ts ***!
  \********************************************************************/
/*! exports provided: AdminManagementModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminManagementModule", function() { return AdminManagementModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _admin_management_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-management-routing.module */ "./src/app/layout/admin-management/admin-management-routing.module.ts");
/* harmony import */ var _admin_management_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin-management.component */ "./src/app/layout/admin-management/admin-management.component.ts");
/* harmony import */ var _components_admin_sidebar_admin_sidebar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/admin-sidebar/admin-sidebar.component */ "./src/app/layout/components/admin-sidebar/admin-sidebar.component.ts");
/* harmony import */ var _components_header_header_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/header/header.module */ "./src/app/layout/components/header/header.module.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../modules/bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











// import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
// import { OrderhistoryallhistoryModule } from '../modules/orderhistoryallhistory/orderhistoryallhistory.module';
var AdminManagementModule = /** @class */ (function () {
    function AdminManagementModule() {
    }
    AdminManagementModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_admin_management_component__WEBPACK_IMPORTED_MODULE_3__["AdminManagementComponent"], _components_admin_sidebar_admin_sidebar_component__WEBPACK_IMPORTED_MODULE_4__["AdminSidebarComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _admin_management_routing_module__WEBPACK_IMPORTED_MODULE_2__["AdminManagementRoutingModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbDropdownModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_8__["FormBuilderTableModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                // FormBuilderTableModule,
                _modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_10__["BsComponentModule"],
                // CKEditorModule,
                // OrderhistoryallhistoryModule,
                _components_header_header_module__WEBPACK_IMPORTED_MODULE_5__["HeaderModule"]
            ]
        })
    ], AdminManagementModule);
    return AdminManagementModule;
}());



/***/ }),

/***/ "./src/app/layout/components/admin-sidebar/admin-sidebar.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/layout/components/admin-sidebar/admin-sidebar.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-line {\n  white-space: pre-wrap;\n  list-style-type: none;\n}\n\n.sidebar {\n  font-size: small;\n  box-shadow: 0 4px 1px 0 rgba(0, 0, 0, 0.01), 0 3px 5px 0 rgba(0, 0, 0, 0.19);\n  border-radius: 0;\n  position: fixed;\n  z-index: 1000;\n  left: 235px;\n  width: 200px;\n  height: 100%;\n  margin-left: -235px;\n  margin-bottom: 48px;\n  border: none;\n  border-radius: 0;\n  overflow-y: auto;\n  background-color: #2b3240;\n  padding-bottom: 40px;\n  padding-top: 22px;\n  white-space: nowrap;\n  transition: all 0.2s ease-in-out;\n  text-align: center;\n  margin-top: 19px;\n}\n\n.sidebar .list-group {\n  padding-bottom: 20px;\n}\n\n.sidebar .list-group a.list-group-item {\n  background: #2b3240;\n  border: 0;\n  border-radius: 0;\n  color: #ffffff;\n  text-decoration: none;\n  padding-top: 15px;\n  padding-bottom: 15px;\n  padding-right: 5px;\n  padding-left: 10px;\n  text-align: left;\n  vertical-align: 10px;\n}\n\n.sidebar .list-group a.list-group-item > i {\n  display: block;\n  font-family: \"fontello\";\n  font-style: normal;\n}\n\n.sidebar .list-group a.list-group-item .fa {\n  margin-right: 10px;\n}\n\n.sidebar .list-group a.list-group-item > p {\n  margin-top: 5px;\n}\n\n.sidebar .list-group a.list-group-item > p:active {\n  color: #56a4ff;\n}\n\n.sidebar .list-group a:hover {\n  background: #3b4558;\n  color: #56a4ff;\n  border-left: 2px #56a4ff solid;\n}\n\n.sidebar .list-group a.router-link-active {\n  background: #3b4558;\n  color: #56a4ff;\n  border-left: 2px #56a4ff solid;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n}\n\n.sidebar .list-group .header-fields {\n  padding-top: 10px;\n}\n\n.sidebar .list-group .header-fields > .list-group-item:first-child {\n  border-top: 1px solid rgba(255, 255, 255, 0.2);\n}\n\n.sidebar .list-group img {\n  width: 35px;\n}\n\n.sidebar p {\n  margin-bottom: 0px;\n  margin-top: 0px;\n}\n\n.sidebar .sidebar-dropdown *:focus {\n  border-radius: none;\n  border: none;\n}\n\n.sidebar .sidebar-dropdown .panel-title {\n  font-size: 1rem;\n  height: 50px;\n  margin-bottom: 0;\n}\n\n.sidebar .sidebar-dropdown .panel-title a {\n  color: #999;\n  text-decoration: none;\n  font-weight: 400;\n  background: #2b3240;\n}\n\n.sidebar .sidebar-dropdown .panel-title a span {\n  position: relative;\n  display: block;\n  padding: 0.75rem 1.5rem;\n  padding-top: 1rem;\n}\n\n.sidebar .sidebar-dropdown .panel-title a:hover,\n.sidebar .sidebar-dropdown .panel-title a:focus {\n  color: #fff;\n  outline: none;\n  outline-offset: -2px;\n}\n\n.sidebar .sidebar-dropdown .panel-title:hover {\n  background: #212631;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse {\n  border-radius: 0;\n  border: none;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item {\n  border-radius: 0;\n  background-color: #2b3240;\n  border: 0 solid transparent;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a {\n  color: #999;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a:hover {\n  color: #fff;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item:hover {\n  background: #212631;\n}\n\n.sidebar .list-group > a.list-group-item .fa {\n  display: block;\n  clear: both;\n  font-size: 36px;\n  margin-right: 0px;\n}\n\n.sidebar .nested-menu .list-group-item {\n  cursor: pointer;\n}\n\n.sidebar .nested-menu .parent-menu {\n  color: white;\n  text-transform: uppercase;\n  font-size: 11px;\n  text-align: left;\n  padding-left: 10px;\n}\n\n.sidebar .nested-menu i img {\n  width: 45px;\n  margin-bottom: 5px;\n}\n\n.sidebar .nested-menu > a.list-group-item .fa {\n  display: block;\n  clear: both;\n  font-size: 36px;\n  margin-right: 0px;\n}\n\n.sidebar .nested-menu .nested {\n  list-style-type: none;\n}\n\n.sidebar .nested-menu ul.submenu {\n  display: none;\n  height: 0;\n  margin: 0;\n  padding: 0;\n  background: #212631;\n}\n\n.sidebar .nested-menu .expand ul.submenu {\n  display: block;\n  height: auto;\n  background: #212631;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item {\n  border-bottom: 1px solid rgba(255, 255, 255, 0.1);\n  padding-left: 20px;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item .circle-custom {\n  width: 1px;\n  border-radius: 50%;\n  background-color: #247de5;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item span {\n  margin-bottom: 5px;\n  color: #9ea6b7;\n  padding: 0px;\n  list-style: circle;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item.router-link-active {\n  background: #3b4558;\n  color: #56a4ff;\n  border-left: 2px #56a4ff solid;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item.router-link-active span {\n  color: #56a4ff;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item:hover p {\n  color: white;\n}\n\n.sidebar .nested-menu .expand ul.submenu li p {\n  color: #627190;\n  padding: 10px;\n  display: block;\n}\n\n.sidebar .nested-menu .expand ul.submenu p {\n  color: #ffffff;\n}\n\n.sidebar .child-menu {\n  margin-left: 10px;\n  margin-bottom: 20px;\n}\n\n@media (max-width: 992px) {\n  /* smartphones, iPhone, portrait 480x320 phones */\n  .sidebar {\n    top: 38px;\n    left: 0px;\n    padding-bottom: 3em;\n  }\n}\n\n@media (max-width: 600px) {\n  /* smartphones, iPhone, portrait 480x320 phones */\n  .sidebar {\n    margin-top: -3%;\n    top: 68px;\n    left: 0px;\n    height: 97%;\n    width: 25%;\n    padding-bottom: 3em;\n    position: fixed;\n  }\n  .sidebar .list-group > a.list-group-item .fa {\n    font-size: 40px;\n  }\n  .sidebar .list-group img {\n    width: 40px;\n  }\n  .sidebar .nested-menu > a.list-group-item .fa {\n    font-size: 40px;\n  }\n}\n\n@media print {\n  .sidebar {\n    display: none !important;\n  }\n}\n\n::-webkit-scrollbar {\n  width: 8px;\n}\n\n::-webkit-scrollbar-track {\n  -webkit-box-shadow: inset 0 0 0px white;\n  border-radius: 3px;\n}\n\n::-webkit-scrollbar-thumb {\n  border-radius: 3px;\n  -webkit-box-shadow: inset 0 0 3px white;\n}\n\n.toggle-button {\n  position: fixed;\n  width: 236px;\n  cursor: pointer;\n  padding: 12px;\n  bottom: 0;\n  color: #999;\n  background: #212529;\n  border-top: 1px solid #999;\n  transition: all 0.2s ease-in-out;\n}\n\n.toggle-button i {\n  font-size: 23px;\n}\n\n.toggle-button:hover {\n  background: #212631;\n  color: #56a4ff;\n}\n\n.collapsed {\n  width: 50px;\n}\n\n.collapsed span {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2NvbXBvbmVudHMvYWRtaW4tc2lkZWJhci9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXGNvbXBvbmVudHNcXGFkbWluLXNpZGViYXJcXGFkbWluLXNpZGViYXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9jb21wb25lbnRzL2FkbWluLXNpZGViYXIvYWRtaW4tc2lkZWJhci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFLHFCQUFBO0VBQ0EscUJBQUE7QUNBRjs7QURHQTtFQUNFLGdCQUFBO0VBQ0EsNEVBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBcEJ3QjtFQXFCeEIsb0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBS0EsZ0NBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDQUY7O0FERUU7RUFDRSxvQkFBQTtBQ0FKOztBREVJO0VBQ0UsbUJBcENvQjtFQXFDcEIsU0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFhQSxnQkFBQTtFQUNBLG9CQUFBO0FDWk47O0FEQU07RUFDRSxjQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQ0VSOztBREVNO0VBQ0Usa0JBQUE7QUNBUjs7QURPSTtFQUNFLGVBQUE7QUNMTjs7QURRSTtFQUNFLGNBQUE7QUNOTjs7QURTSTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDUE47O0FEVUk7RUFDRSxtQkFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtFQUNBLDRFQUFBO0FDUk47O0FEV0k7RUFDRSxpQkFBQTtBQ1ROOztBRFdNO0VBQ0UsOENBQUE7QUNUUjs7QURhSTtFQUNFLFdBQUE7QUNYTjs7QURlRTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtBQ2JKOztBRGlCSTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtBQ2ZOOztBRGtCSTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNoQk47O0FEa0JNO0VBQ0UsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFuSGtCO0FDbUcxQjs7QURrQlE7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0FDaEJWOztBRG9CTTs7RUFFRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0FDbEJSOztBRHNCSTtFQUNFLG1CQUFBO0FDcEJOOztBRHVCSTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtBQ3JCTjs7QUR3QlE7RUFDRSxnQkFBQTtFQUNBLHlCQWhKZ0I7RUFpSmhCLDJCQUFBO0FDdEJWOztBRHdCVTtFQUNFLFdBQUE7QUN0Qlo7O0FEeUJVO0VBQ0UsV0FBQTtBQ3ZCWjs7QUQyQlE7RUFDRSxtQkFBQTtBQ3pCVjs7QURpQ007RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQy9CUjs7QURxQ0k7RUFDRSxlQUFBO0FDbkNOOztBRHNDSTtFQUNFLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDcENOOztBRHdDTTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtBQ3RDUjs7QUQyQ007RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ3pDUjs7QUQ2Q0k7RUFDRSxxQkFBQTtBQzNDTjs7QUQ4Q0k7RUFDRSxhQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7QUM1Q047O0FEZ0RNO0VBQ0UsY0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQzlDUjs7QURnRFE7RUFFRSxpREFBQTtFQUVBLGtCQUFBO0FDaERWOztBRGlEVTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FDL0NaOztBRGlEVTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQy9DWjs7QURtRFE7RUFDRSxtQkFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtFQUNBLDRFQUFBO0FDakRWOztBRGtEVTtFQUNFLGNBQUE7QUNoRFo7O0FEcURVO0VBQ0UsWUFBQTtBQ25EWjs7QUR3RFU7RUFDRSxjQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUN0RFo7O0FEMERRO0VBQ0UsY0FBQTtBQ3hEVjs7QUQ4REU7RUFDRSxpQkFBQTtFQUNBLG1CQUFBO0FDNURKOztBRGlFQTtFQUNFLGlEQUFBO0VBQ0E7SUFDRSxTQUFBO0lBQ0EsU0FBQTtJQUNBLG1CQUFBO0VDOURGO0FBQ0Y7O0FEZ0VBO0VBQ0UsaURBQUE7RUFDQTtJQUNFLGVBQUE7SUFDQSxTQUFBO0lBQ0EsU0FBQTtJQUNBLFdBQUE7SUFDQSxVQUFBO0lBQ0EsbUJBQUE7SUFDQSxlQUFBO0VDOURGO0VEa0VNO0lBQ0UsZUFBQTtFQ2hFUjtFRG9FSTtJQUNFLFdBQUE7RUNsRU47RUR3RU07SUFDRSxlQUFBO0VDdEVSO0FBQ0Y7O0FEd0ZBO0VBQ0U7SUFDRSx3QkFBQTtFQ3RGRjtBQUNGOztBRHlGQTtFQUNFLFVBQUE7QUN2RkY7O0FEMEZBO0VBQ0UsdUNBQUE7RUFDQSxrQkFBQTtBQ3ZGRjs7QUQwRkE7RUFDRSxrQkFBQTtFQUNBLHVDQUFBO0FDdkZGOztBRDBGQTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBV0EsMEJBQUE7RUFLQSxnQ0FBQTtBQ2pHRjs7QURtRkU7RUFDRSxlQUFBO0FDakZKOztBRG9GRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtBQ2xGSjs7QUQ2RkE7RUFDRSxXQUFBO0FDMUZGOztBRDRGRTtFQUNFLGFBQUE7QUMxRkoiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvY29tcG9uZW50cy9hZG1pbi1zaWRlYmFyL2FkbWluLXNpZGViYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkdG9wbmF2LWJhY2tncm91bmQtY29sb3I6ICMyYjMyNDA7XHJcbi5uZXctbGluZSB7XHJcbiAgd2hpdGUtc3BhY2U6IHByZS13cmFwO1xyXG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcclxufVxyXG5cclxuLnNpZGViYXIge1xyXG4gIGZvbnQtc2l6ZTogc21hbGw7XHJcbiAgYm94LXNoYWRvdzogMCA0cHggMXB4IDAgcmdiYSgwLCAwLCAwLCAwLjAxKSwgMCAzcHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcclxuICBib3JkZXItcmFkaXVzOiAwO1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB6LWluZGV4OiAxMDAwO1xyXG4gIGxlZnQ6IDIzNXB4O1xyXG4gIHdpZHRoOiAyMDBweDtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgbWFyZ2luLWxlZnQ6IC0yMzVweDtcclxuICBtYXJnaW4tYm90dG9tOiA0OHB4O1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBib3JkZXItcmFkaXVzOiAwO1xyXG4gIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xyXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xyXG4gIHBhZGRpbmctdG9wOiAyMnB4O1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIC1tcy10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICAtby10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLXRvcDogMTlweDtcclxuXHJcbiAgLmxpc3QtZ3JvdXAge1xyXG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcblxyXG4gICAgYS5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICBiYWNrZ3JvdW5kOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XHJcbiAgICAgIGJvcmRlcjogMDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgcGFkZGluZy10b3A6IDE1cHg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuXHJcbiAgICAgID4gaSB7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiZm9udGVsbG9cIjtcclxuICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vcGFkZGluZy1sZWZ0OiA0cHg7XHJcbiAgICAgIC5mYSB7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICB2ZXJ0aWNhbC1hbGlnbjogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICBhLmxpc3QtZ3JvdXAtaXRlbSA+IHAge1xyXG4gICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgYS5saXN0LWdyb3VwLWl0ZW0gPiBwOmFjdGl2ZSB7XHJcbiAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgfVxyXG5cclxuICAgIGE6aG92ZXIge1xyXG4gICAgICBiYWNrZ3JvdW5kOiBsaWdodGVuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgOCUpO1xyXG4gICAgICBjb2xvcjogIzU2YTRmZjtcclxuICAgICAgYm9yZGVyLWxlZnQ6IDJweCAjNTZhNGZmIHNvbGlkO1xyXG4gICAgfVxyXG5cclxuICAgIGEucm91dGVyLWxpbmstYWN0aXZlIHtcclxuICAgICAgYmFja2dyb3VuZDogbGlnaHRlbigkdG9wbmF2LWJhY2tncm91bmQtY29sb3IsIDglKTtcclxuICAgICAgY29sb3I6ICM1NmE0ZmY7XHJcbiAgICAgIGJvcmRlci1sZWZ0OiAycHggIzU2YTRmZiBzb2xpZDtcclxuICAgICAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcclxuICAgIH1cclxuXHJcbiAgICAuaGVhZGVyLWZpZWxkcyB7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG5cclxuICAgICAgPiAubGlzdC1ncm91cC1pdGVtOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDM1cHg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxuICB9XHJcblxyXG4gIC5zaWRlYmFyLWRyb3Bkb3duIHtcclxuICAgICo6Zm9jdXMge1xyXG4gICAgICBib3JkZXItcmFkaXVzOiBub25lO1xyXG4gICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICB9XHJcblxyXG4gICAgLnBhbmVsLXRpdGxlIHtcclxuICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcblxyXG4gICAgICBhIHtcclxuICAgICAgICBjb2xvcjogIzk5OTtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XHJcblxyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICBwYWRkaW5nOiAwLjc1cmVtIDEuNXJlbTtcclxuICAgICAgICAgIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgYTpob3ZlcixcclxuICAgICAgYTpmb2N1cyB7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICBvdXRsaW5lLW9mZnNldDogLTJweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5wYW5lbC10aXRsZTpob3ZlciB7XHJcbiAgICAgIGJhY2tncm91bmQ6IGRhcmtlbigkdG9wbmF2LWJhY2tncm91bmQtY29sb3IsIDUlKTtcclxuICAgIH1cclxuXHJcbiAgICAucGFuZWwtY29sbGFwc2Uge1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICBib3JkZXI6IG5vbmU7XHJcblxyXG4gICAgICAucGFuZWwtYm9keSB7XHJcbiAgICAgICAgLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xyXG4gICAgICAgICAgYm9yZGVyOiAwIHNvbGlkIHRyYW5zcGFyZW50O1xyXG5cclxuICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICBjb2xvcjogIzk5OTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBhOmhvdmVyIHtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAubGlzdC1ncm91cC1pdGVtOmhvdmVyIHtcclxuICAgICAgICAgIGJhY2tncm91bmQ6IGRhcmtlbigkdG9wbmF2LWJhY2tncm91bmQtY29sb3IsIDUlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5saXN0LWdyb3VwIHtcclxuICAgID4gYS5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICAuZmEge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGNsZWFyOiBib3RoO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLm5lc3RlZC1tZW51IHtcclxuICAgIC5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLnBhcmVudC1tZW51IHtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICBpIHtcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogNDVweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICA+IGEubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgLmZhIHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBjbGVhcjogYm90aDtcclxuICAgICAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmVzdGVkIHtcclxuICAgICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIHVsLnN1Ym1lbnUge1xyXG4gICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICBoZWlnaHQ6IDA7XHJcbiAgICAgIG1hcmdpbjogMDtcclxuICAgICAgcGFkZGluZzogMDtcclxuICAgICAgYmFja2dyb3VuZDogZGFya2VuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgNSUpO1xyXG4gICAgfVxyXG5cclxuICAgICYgLmV4cGFuZCB7XHJcbiAgICAgIHVsLnN1Ym1lbnUge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yLCA1JSk7XHJcblxyXG4gICAgICAgIGEubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgICAgIC8vIGJhY2tncm91bmQ6ICMzOTQ0NTg7XHJcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjEpO1xyXG4gICAgICAgICAgLy8gYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcclxuICAgICAgICAgIHBhZGRpbmctbGVmdDogMjBweDtcclxuICAgICAgICAgIC5jaXJjbGUtY3VzdG9tIHtcclxuICAgICAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjQ3ZGU1O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgICAgICAgICAgY29sb3I6ICM5ZWE2Yjc7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcclxuICAgICAgICAgICAgbGlzdC1zdHlsZTogY2lyY2xlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYS5saXN0LWdyb3VwLWl0ZW0ucm91dGVyLWxpbmstYWN0aXZlIHtcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICMzYjQ1NTg7XHJcbiAgICAgICAgICBjb2xvcjogIzU2YTRmZjtcclxuICAgICAgICAgIGJvcmRlci1sZWZ0OiAycHggIzU2YTRmZiBzb2xpZDtcclxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYigwIDAgMCAvIDIwJSksIDAgNnB4IDIwcHggMCByZ2IoMCAwIDAgLyAxOSUpO1xyXG4gICAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYS5saXN0LWdyb3VwLWl0ZW06aG92ZXIge1xyXG4gICAgICAgICAgcCB7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxpIHtcclxuICAgICAgICAgIHAge1xyXG4gICAgICAgICAgICBjb2xvcjogIzYyNzE5MDtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBwIHtcclxuICAgICAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmNoaWxkLW1lbnUge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG5cclxuICAgIFxyXG4gIH1cclxufVxyXG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAvKiBzbWFydHBob25lcywgaVBob25lLCBwb3J0cmFpdCA0ODB4MzIwIHBob25lcyAqL1xyXG4gIC5zaWRlYmFyIHtcclxuICAgIHRvcDogMzhweDtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAzZW07XHJcbiAgfVxyXG59XHJcbkBtZWRpYSAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gIC8qIHNtYXJ0cGhvbmVzLCBpUGhvbmUsIHBvcnRyYWl0IDQ4MHgzMjAgcGhvbmVzICovXHJcbiAgLnNpZGViYXIge1xyXG4gICAgbWFyZ2luLXRvcDogLTMlO1xyXG4gICAgdG9wOiA2OHB4O1xyXG4gICAgbGVmdDogMHB4O1xyXG4gICAgaGVpZ2h0OiA5NyU7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDNlbTtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuXHJcbiAgICAubGlzdC1ncm91cCB7XHJcbiAgICAgID4gYS5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICAgIC5mYSB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDQwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLm5lc3RlZC1tZW51IHtcclxuICAgICAgPiBhLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgICAgLmZhIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi8vQG1lZGlhIChtaW4td2lkdGg6IDk2MXB4KSB7XHJcbi8vICAgIC8qIHRhYmxldCwgbGFuZHNjYXBlIGlQYWQsIGxvLXJlcyBsYXB0b3BzIGFuZHMgZGVza3RvcHMgKi9cclxuLy99XHJcbi8vXHJcbi8vQG1lZGlhIChtaW4td2lkdGg6IDEwMjVweCkge1xyXG4vLyAgICAvKiBiaWcgbGFuZHNjYXBlIHRhYmxldHMsIGxhcHRvcHMsIGFuZCBkZXNrdG9wcyAqL1xyXG4vL31cclxuLy9cclxuLy9AbWVkaWEgKG1pbi13aWR0aDogMTI4MXB4KSB7XHJcbi8vICAgIC8qIGhpLXJlcyBsYXB0b3BzIGFuZCBkZXNrdG9wcyAqL1xyXG4vL31cclxuXHJcbkBtZWRpYSBwcmludCB7XHJcbiAgLnNpZGViYXIge1xyXG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG4gIH1cclxufVxyXG5cclxuOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDhweDtcclxufVxyXG5cclxuOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgMHB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMSk7XHJcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG59XHJcblxyXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgM3B4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMSk7XHJcbn1cclxuXHJcbi50b2dnbGUtYnV0dG9uIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgd2lkdGg6IDIzNnB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBwYWRkaW5nOiAxMnB4O1xyXG4gIGJvdHRvbTogMDtcclxuICBjb2xvcjogIzk5OTtcclxuICBiYWNrZ3JvdW5kOiAjMjEyNTI5O1xyXG5cclxuICBpIHtcclxuICAgIGZvbnQtc2l6ZTogMjNweDtcclxuICB9XHJcblxyXG4gICY6aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZDogZGFya2VuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgNSUpO1xyXG4gICAgY29sb3I6ICM1NmE0ZmY7XHJcbiAgfVxyXG5cclxuICBib3JkZXItdG9wOiAxcHggc29saWQgIzk5OTtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgLW1zLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG4uY29sbGFwc2VkIHtcclxuICB3aWR0aDogNTBweDtcclxuXHJcbiAgc3BhbiB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxufSIsIi5uZXctbGluZSB7XG4gIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xufVxuXG4uc2lkZWJhciB7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDFweCAwIHJnYmEoMCwgMCwgMCwgMC4wMSksIDAgM3B4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogMTAwMDtcbiAgbGVmdDogMjM1cHg7XG4gIHdpZHRoOiAyMDBweDtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtYXJnaW4tbGVmdDogLTIzNXB4O1xuICBtYXJnaW4tYm90dG9tOiA0OHB4O1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIG92ZXJmbG93LXk6IGF1dG87XG4gIGJhY2tncm91bmQtY29sb3I6ICMyYjMyNDA7XG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xuICBwYWRkaW5nLXRvcDogMjJweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbiAgLW1vei10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbiAgLW1zLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtby10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMTlweDtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIHtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCBhLmxpc3QtZ3JvdXAtaXRlbSB7XG4gIGJhY2tncm91bmQ6ICMyYjMyNDA7XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgdmVydGljYWwtYWxpZ246IDEwcHg7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCBhLmxpc3QtZ3JvdXAtaXRlbSA+IGkge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1mYW1pbHk6IFwiZm9udGVsbG9cIjtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgYS5saXN0LWdyb3VwLWl0ZW0gLmZhIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgYS5saXN0LWdyb3VwLWl0ZW0gPiBwIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgYS5saXN0LWdyb3VwLWl0ZW0gPiBwOmFjdGl2ZSB7XG4gIGNvbG9yOiAjNTZhNGZmO1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgYTpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMzYjQ1NTg7XG4gIGNvbG9yOiAjNTZhNGZmO1xuICBib3JkZXItbGVmdDogMnB4ICM1NmE0ZmYgc29saWQ7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCBhLnJvdXRlci1saW5rLWFjdGl2ZSB7XG4gIGJhY2tncm91bmQ6ICMzYjQ1NTg7XG4gIGNvbG9yOiAjNTZhNGZmO1xuICBib3JkZXItbGVmdDogMnB4ICM1NmE0ZmYgc29saWQ7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCAuaGVhZGVyLWZpZWxkcyB7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgLmhlYWRlci1maWVsZHMgPiAubGlzdC1ncm91cC1pdGVtOmZpcnN0LWNoaWxkIHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGltZyB7XG4gIHdpZHRoOiAzNXB4O1xufVxuLnNpZGViYXIgcCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgbWFyZ2luLXRvcDogMHB4O1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gKjpmb2N1cyB7XG4gIGJvcmRlci1yYWRpdXM6IG5vbmU7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLXRpdGxlIGEge1xuICBjb2xvcjogIzk5OTtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBmb250LXdlaWdodDogNDAwO1xuICBiYWNrZ3JvdW5kOiAjMmIzMjQwO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLXRpdGxlIGEgc3BhbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmc6IDAuNzVyZW0gMS41cmVtO1xuICBwYWRkaW5nLXRvcDogMXJlbTtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC10aXRsZSBhOmhvdmVyLFxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLXRpdGxlIGE6Zm9jdXMge1xuICBjb2xvcjogI2ZmZjtcbiAgb3V0bGluZTogbm9uZTtcbiAgb3V0bGluZS1vZmZzZXQ6IC0ycHg7XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtdGl0bGU6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMjEyNjMxO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLWNvbGxhcHNlIHtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgYm9yZGVyOiBub25lO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLWNvbGxhcHNlIC5wYW5lbC1ib2R5IC5saXN0LWdyb3VwLWl0ZW0ge1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmIzMjQwO1xuICBib3JkZXI6IDAgc29saWQgdHJhbnNwYXJlbnQ7XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtY29sbGFwc2UgLnBhbmVsLWJvZHkgLmxpc3QtZ3JvdXAtaXRlbSBhIHtcbiAgY29sb3I6ICM5OTk7XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtY29sbGFwc2UgLnBhbmVsLWJvZHkgLmxpc3QtZ3JvdXAtaXRlbSBhOmhvdmVyIHtcbiAgY29sb3I6ICNmZmY7XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtY29sbGFwc2UgLnBhbmVsLWJvZHkgLmxpc3QtZ3JvdXAtaXRlbTpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMyMTI2MzE7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCA+IGEubGlzdC1ncm91cC1pdGVtIC5mYSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBjbGVhcjogYm90aDtcbiAgZm9udC1zaXplOiAzNnB4O1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbn1cbi5zaWRlYmFyIC5uZXN0ZWQtbWVudSAubGlzdC1ncm91cC1pdGVtIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5wYXJlbnQtbWVudSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMXB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgaSBpbWcge1xuICB3aWR0aDogNDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51ID4gYS5saXN0LWdyb3VwLWl0ZW0gLmZhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNsZWFyOiBib3RoO1xuICBmb250LXNpemU6IDM2cHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5uZXN0ZWQge1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgdWwuc3VibWVudSB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIGhlaWdodDogMDtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBiYWNrZ3JvdW5kOiAjMjEyNjMxO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBoZWlnaHQ6IGF1dG87XG4gIGJhY2tncm91bmQ6ICMyMTI2MzE7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmV4cGFuZCB1bC5zdWJtZW51IGEubGlzdC1ncm91cC1pdGVtIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBhLmxpc3QtZ3JvdXAtaXRlbSAuY2lyY2xlLWN1c3RvbSB7XG4gIHdpZHRoOiAxcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI0N2RlNTtcbn1cbi5zaWRlYmFyIC5uZXN0ZWQtbWVudSAuZXhwYW5kIHVsLnN1Ym1lbnUgYS5saXN0LWdyb3VwLWl0ZW0gc3BhbiB7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgY29sb3I6ICM5ZWE2Yjc7XG4gIHBhZGRpbmc6IDBweDtcbiAgbGlzdC1zdHlsZTogY2lyY2xlO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBhLmxpc3QtZ3JvdXAtaXRlbS5yb3V0ZXItbGluay1hY3RpdmUge1xuICBiYWNrZ3JvdW5kOiAjM2I0NTU4O1xuICBjb2xvcjogIzU2YTRmZjtcbiAgYm9yZGVyLWxlZnQ6IDJweCAjNTZhNGZmIHNvbGlkO1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBhLmxpc3QtZ3JvdXAtaXRlbS5yb3V0ZXItbGluay1hY3RpdmUgc3BhbiB7XG4gIGNvbG9yOiAjNTZhNGZmO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBhLmxpc3QtZ3JvdXAtaXRlbTpob3ZlciBwIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBsaSBwIHtcbiAgY29sb3I6ICM2MjcxOTA7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBwIHtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4uc2lkZWJhciAuY2hpbGQtbWVudSB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLyogc21hcnRwaG9uZXMsIGlQaG9uZSwgcG9ydHJhaXQgNDgweDMyMCBwaG9uZXMgKi9cbiAgLnNpZGViYXIge1xuICAgIHRvcDogMzhweDtcbiAgICBsZWZ0OiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDNlbTtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC8qIHNtYXJ0cGhvbmVzLCBpUGhvbmUsIHBvcnRyYWl0IDQ4MHgzMjAgcGhvbmVzICovXG4gIC5zaWRlYmFyIHtcbiAgICBtYXJnaW4tdG9wOiAtMyU7XG4gICAgdG9wOiA2OHB4O1xuICAgIGxlZnQ6IDBweDtcbiAgICBoZWlnaHQ6IDk3JTtcbiAgICB3aWR0aDogMjUlO1xuICAgIHBhZGRpbmctYm90dG9tOiAzZW07XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICB9XG4gIC5zaWRlYmFyIC5saXN0LWdyb3VwID4gYS5saXN0LWdyb3VwLWl0ZW0gLmZhIHtcbiAgICBmb250LXNpemU6IDQwcHg7XG4gIH1cbiAgLnNpZGViYXIgLmxpc3QtZ3JvdXAgaW1nIHtcbiAgICB3aWR0aDogNDBweDtcbiAgfVxuICAuc2lkZWJhciAubmVzdGVkLW1lbnUgPiBhLmxpc3QtZ3JvdXAtaXRlbSAuZmEge1xuICAgIGZvbnQtc2l6ZTogNDBweDtcbiAgfVxufVxuQG1lZGlhIHByaW50IHtcbiAgLnNpZGViYXIge1xuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcbiAgfVxufVxuOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiA4cHg7XG59XG5cbjo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xuICAtd2Via2l0LWJveC1zaGFkb3c6IGluc2V0IDAgMCAwcHggd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgM3B4IHdoaXRlO1xufVxuXG4udG9nZ2xlLWJ1dHRvbiB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDIzNnB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHBhZGRpbmc6IDEycHg7XG4gIGJvdHRvbTogMDtcbiAgY29sb3I6ICM5OTk7XG4gIGJhY2tncm91bmQ6ICMyMTI1Mjk7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjOTk5O1xuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtbXMtdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbn1cbi50b2dnbGUtYnV0dG9uIGkge1xuICBmb250LXNpemU6IDIzcHg7XG59XG4udG9nZ2xlLWJ1dHRvbjpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMyMTI2MzE7XG4gIGNvbG9yOiAjNTZhNGZmO1xufVxuXG4uY29sbGFwc2VkIHtcbiAgd2lkdGg6IDUwcHg7XG59XG4uY29sbGFwc2VkIHNwYW4ge1xuICBkaXNwbGF5OiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/components/admin-sidebar/admin-sidebar.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/layout/components/admin-sidebar/admin-sidebar.component.ts ***!
  \****************************************************************************/
/*! exports provided: AdminSidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminSidebarComponent", function() { return AdminSidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/storage-service-module/storage-service-module.service */ "./src/app/services/storage-service-module/storage-service-module.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminSidebarComponent = /** @class */ (function () {
    function AdminSidebarComponent(translate, router, lStorage) {
        // let $secretKey = localStorage.getToken();
        // let encodedStr = JSON.stringify({data:'here'})
        // let AesCrypto = crypto.AES.encrypt(encodedStr, $secretKey);
        var _this = this;
        this.translate = translate;
        this.router = router;
        this.lStorage = lStorage;
        this.isActive = false;
        this.collapsed = false;
        this.showMenu = '';
        this.pushRightClass = 'push-right';
        // sideBarMenu: any = sidebarmenu
        this.collapsedEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // let result = AesCrypto.toString();
        // let decrypt = crypto.AES.decrypt(result, $secretKey );
        // console.log("CRYPTO", result,decrypt.toString(crypto.enc.Utf8)) ;
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        var browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');
        this.router.events.subscribe(function (val) {
            if (val instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"] &&
                window.innerWidth <= 992 &&
                _this.isToggled()) {
                _this.toggleSidebar();
            }
        });
    }
    AdminSidebarComponent.prototype.eventCalled = function () {
        this.isActive = !this.isActive;
    };
    AdminSidebarComponent.prototype.addExpandClass = function (element) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        }
        else {
            this.showMenu = element;
        }
    };
    AdminSidebarComponent.prototype.toggleCollapsed = function () {
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    };
    AdminSidebarComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    AdminSidebarComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    AdminSidebarComponent.prototype.rltAndLtr = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    };
    AdminSidebarComponent.prototype.changeLang = function (language) {
        this.translate.use(language);
    };
    AdminSidebarComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
        localStorage.removeItem('devmode');
    };
    AdminSidebarComponent.prototype.orderhistory = function () {
        // this.router.navigate(["merchant-portal/order-histories"])
    };
    AdminSidebarComponent.ctorParameters = function () { return [
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], AdminSidebarComponent.prototype, "collapsedEvent", void 0);
    AdminSidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-sidebar',
            template: __webpack_require__(/*! raw-loader!./admin-sidebar.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/components/admin-sidebar/admin-sidebar.component.html"),
            styles: [__webpack_require__(/*! ./admin-sidebar.component.scss */ "./src/app/layout/components/admin-sidebar/admin-sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"]])
    ], AdminSidebarComponent);
    return AdminSidebarComponent;
}());



/***/ })

}]);
//# sourceMappingURL=admin-management-admin-management-module.js.map