(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-product-product-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product/add-bulk/product.add-bulk.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product/add-bulk/product.add-bulk.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n    <app-page-header [heading]=\"'Add Product Bulk'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n    <div>\r\n        <div class=\"card card-detail\">\r\n            <div class=\"card-header\">\r\n                <!-- <button class=\"btn save_button\" (click)=\"formSubmitAddMember()\" type=\"submit\"\r\n                    [routerLinkActive]=\"['router-link-active']\"><i class=\"fa fa-fw fa-save\"\r\n                        style=\"transform: translate(-3px, -1px)\"></i>Submit</button> -->\r\n                <button class=\"btn back_button\" (click)=\"backTo()\"><i class=\"fa fa-fw fa-angle-left\"></i>Back</button>\r\n            </div>\r\n\r\n            <div class=\"card-content\">\r\n                <div class=\"member-detail\">\r\n                    <div class=\"row\">\r\n                        <!-- <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2>Data Pemilik &amp; Data Toko</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n\r\n                                        <label>ID Pelanggan</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.id_pel\" [placeholder]=\"'ID Pelanggan'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Nama Toko</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.nama_toko\" [placeholder]=\"'Nama Toko'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Nama Distributor</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.nama_distributor\" [placeholder]=\"'Nama Distributor'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Alamat Toko</label>\r\n                                        <form-input name=\"id\" [type]=\"'textarea'\" [(ngModel)]=\"form.alamat_toko\" [placeholder]=\"'Alamat Toko'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Nama Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.nama_pemilik\" [placeholder]=\"'Nama Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>No. Telp Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.telp_pemilik\" [placeholder]=\"'No. Telp Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>No. WA Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.no_wa_pemilik\" [placeholder]=\"'No. WA Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>No. KTP Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.ktp_pemilik\" [placeholder]=\"'No. KTP Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>No. NPWP Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.npwp_pemilik\" [placeholder]=\"'No. NPWP Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Alamat Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'textarea'\" [(ngModel)]=\"form.alamat_rumah\" [placeholder]=\"'Alamat Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Desa/Kelurahan Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kelurahan_rumah\" [placeholder]=\"'Desa/Kelurahan Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Kecamatan Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kecamatan_rumah\" [placeholder]=\"'Kecamatan Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Kabupaten/Kota Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kota_rumah\" [placeholder]=\"'Kabupaten/Kota Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Provinsi Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.provinsi_rumah\" [placeholder]=\"'Provinsi Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Kode Pos Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kode_pos_rumah\" [placeholder]=\"'Kode Pos Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <div class=\"form-group\">\r\n                                            <label>Hadiah Dikuasakan</label>\r\n                                            <form-select [(ngModel)]=\"form.hadiah_dikuasakan\" [data]=\"kuasa_option\"></form-select>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group\">\r\n                                            <label>Group</label>\r\n                                            <form-select [(ngModel)]=\"form.description\" [data]=\"group_desc\"></form-select>\r\n                                        </div>\r\n                                        \r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div> -->\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <input #addBulk type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event)\"\r\n                                />\r\n\r\n                                <div class=\"card-header\">\r\n                                    <h2>Add Product Bulk</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n                                        <label>Upload Your File Here</label>\r\n                                        <div class=\"member-bulk-update-container\">\r\n                                            <span *ngIf=\"selectedFile && prodOnUpload == false\">{{selectedFile.name}}</span>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group uploaded\" *ngIf=\"prodOnUpload == false && startUploading == true && cancel == false\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <div class=\"container-two-button\">\r\n                                            <div class=\"img-upload\" *ngIf=\"!selectedFile; else upload_image\">\r\n                                                <button class=\"btn btn-upload\" (click)=\"addBulk.click()\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span>upload</span>\r\n                                                </button>\r\n                                            </div>\r\n    \r\n                                            <div class=\"img-upload\" *ngIf=\"selectedFile\">\r\n                                                <button class=\"btn btn-submit\" (click)=\"addBulkProduct()\">\r\n                                                    <i class=\"fa fa-fw fa-save\"></i>\r\n                                                    <span>submit</span>\r\n                                                </button>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <ng-template #upload_image>\r\n                                        <button class=\"btn btn-upload\" (click)=\"addBulk.click()\">\r\n                                            <i class=\"fa fa-fw fa-upload\"></i>\r\n                                            <span>re-upload</span>\r\n                                        </button>\r\n                                    </ng-template>\r\n\r\n                                    <div class=\"col-md-12 col-sm-12 error-msg-upload\">\r\n                                        <div *ngIf=\"errorFile\"><mark>Error: {{errorFile}}</mark></div>                       \r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n                        \r\n                        <!-- <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2>Member &amp; Status Member</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n\r\n                                        <label>Password</label>\r\n                                        <form-input name=\"password\" [type]=\"'password'\" [placeholder]=\"'Password'\"\r\n                                            [(ngModel)]=\"form.password\" autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <label>Email</label>\r\n                                        <form-input name=\"email\" [(ngModel)]=\"form.email\" [type]=\"'text'\" [placeholder]=\"'Email'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Cell Phone</label>\r\n                                        <form-input name=\"cell_phone\" [type]=\"'number'\" [(ngModel)]=\"form.cell_phone\" [placeholder]=\"'Cell Phone'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div> -->\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <!-- <form #form=\"ngForm\" (ngSubmit)=\"formSubmitAddMember(form.value)\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-6\">\r\n                        <label>Full Name</label>\r\n                        <form-input name=\"full_name\" [type]=\"'text'\" [placeholder]=\"'Full Name'\" [(ngModel)]=\"full_name\"\r\n                            required></form-input>\r\n\r\n                        <label>Email</label>\r\n                        <form-input name=\"email\" [placeholder]=\"'Email'\" [type]=\"'text'\" [(ngModel)]=\"email\" required>\r\n                        </form-input>\r\n\r\n                        <label>Password</label>\r\n                        <form-input name=\"password\" [placeholder]=\"'Password'\" [type]=\"'password'\"\r\n                            [(ngModel)]=\"password\" required></form-input>\r\n\r\n                    \r\n                        <label>Cell Phone</label>\r\n                        <form-input name=\"cell_phone\" [placeholder]=\"'Cell Phone'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"cell_phone\" required></form-input>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>City</label>\r\n                        <form-input name=\"mcity\" [placeholder]=\"'City'\" [type]=\"'text'\" [(ngModel)]=\"mcity\" required>\r\n                        </form-input>\r\n                    </div>\r\n\r\n                    <div class=\"col-md-6\">\r\n                        <label>State</label>\r\n                        <form-input name=\"mstate\" [placeholder]=\"'State'\" [type]=\"'text'\" [(ngModel)]=\"mstate\" required>\r\n                        </form-input>\r\n\r\n                        <label>Country</label>\r\n                        <form-input name=\"mcountry\" [placeholder]=\"'Country'\" [type]=\"'text'\" [(ngModel)]=\"mcountry\"\r\n                            required></form-input>\r\n\r\n                        <label>Address</label>\r\n                        <form-input name=\"maddress1\" [placeholder]=\"'Address'\" [type]=\"'textarea'\"\r\n                            [(ngModel)]=\"maddress1\" required></form-input>\r\n\r\n                        <label>Post Code</label>\r\n                        <form-input name=\"mpostcode\" [placeholder]=\"'Post Code'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"mpostcode\" required></form-input>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label>Gender</label>\r\n                            <select class=\"form-control\" name=\"gender\" [(ngModel)]=\"gender\">\r\n                                <option *ngFor=\"let d of gender_type\" value=\"{{d.value}}\">{{d.label}}</option>\r\n                            </select>\r\n                        </div>\r\n\r\n\r\n                        <label>Marital Status</label>\r\n                        <form-select name=\"marital_status\" [(ngModel)]=\"marital_status\" [data]=\"marital_status_type\">\r\n                        </form-select>\r\n\r\n                        <label>Member Type</label>\r\n                        <form-select name=\"type\" [(ngModel)]=\"type\" [data]=\"member_type\"></form-select>\r\n                    </div>\r\n                </div>\r\n\r\n            </form> -->\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product/add/product.add.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product/add/product.add.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n  <h3 class=\"heading\">Product Management</h3>\r\n  <!-- <app-page-header [heading]=\"'Product Management'\" [icon]=\"'fa-table'\"></app-page-header> -->\r\n  <div class=\"add-product\">\r\n      <div class=\"e-wrapper\">\r\n          <app-merchant-add-new-product></app-merchant-add-new-product>\r\n      </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- <div [@routerTransition]>\r\n  <app-page-header [heading]=\"'Add New Product'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  <div>\r\n    <div class=\"card card-detail\">\r\n      <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\">\r\n          <i class=\"fa fa-fw fa-save\"></i>\r\n          <span *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span>\r\n        </button>\r\n      </div>\r\n\r\n      <div class=\"card-content\">\r\n        <div class=\"product-detail\">\r\n          <div class=\"row\">\r\n            <div class=\" col-md-8\">\r\n              <div class=\"card mb-3\">\r\n                <div class=\"card-header\">\r\n                  <h2> Product Details </h2>\r\n                </div>\r\n                <div class=\"card-content\">\r\n                  <div class=\"col-md-12\">\r\n\r\n                    <label>Type</label>\r\n                    <form-select name=\"type\" [disabled]=\"false\" [(ngModel)]=\"data.type\" \r\n                      [data]=\"productType\" autofocus required>                      \r\n                    </form-select>\r\n\r\n                    <label>Merchant Group</label>\r\n                    <form-select name=\"merchant_group\" [disabled]=\"false\" [(ngModel)]=\"data.merchant_group\"\r\n                      [data]=\"merchant_group\" autofocus required>\r\n                    </form-select>\r\n\r\n                    <label>Choose Merchant </label>\r\n                    <form-select name=\"merchant_username\" [disabled]=\"false\" [(ngModel)]=\"data.merchant_username\"\r\n                      [data]=\"merchantList\" autofocus required>\r\n                    </form-select>\r\n\r\n                    <label>Product Code</label>\r\n                    <form-input name=\"product_code\" [type]=\"'text'\" [placeholder]=\"'Product Code'\"\r\n                      [(ngModel)]=\"data.product_code\" autofocus required></form-input>\r\n\r\n                    <label>Product Name</label>\r\n                    <form-input name=\"product_name\" [type]=\"'text'\" [placeholder]=\"'Product Name'\"\r\n                      [(ngModel)]=\"data.product_name\" autofocus required></form-input>\r\n\r\n                    <label>Price</label>\r\n                    <form-input name=\"price\" [type]=\"'number'\" [placeholder]=\"'Price'\" [(ngModel)]=\"data.price\"\r\n                      autofocus required></form-input>\r\n\r\n                    <label>Fixed Price</label>\r\n                    <form-input name=\"fixed_price\" [type]=\"'number'\" [placeholder]=\"'Fixed Price'\"\r\n                      [(ngModel)]=\"data.fixed_price\" autofocus required></form-input>\r\n\r\n                    <label>Category</label>\r\n                      <form-select name=\"category\" [disabled]=\"false\" [(ngModel)]=\"category\"\r\n                        [data]=\"categoryProduct\" autofocus required>\r\n                      </form-select>\r\n\r\n                    <label>Hashtags</label>\r\n                    <form-input name=\"hashtags\" [type]=\"'text'\" [placeholder]=\"'Hashtags'\" [(ngModel)]=\"data.hashtags\"\r\n                      autofocus required></form-input>\r\n\r\n\r\n\r\n                    <label>Description</label>\r\n                    <ckeditor [editor]=\"Editor\" [(ngModel)]=\"data.description\"></ckeditor>\r\n\r\n\r\n                    <label>TNC</label>\r\n                    <ckeditor [editor]=\"Editor\" [(ngModel)]=\"data.tnc\"></ckeditor>\r\n\r\n                    <div *ngIf=\"data.type === 'product'\" class=\"condition-wrapper\">\r\n\r\n                      <label>Quantity</label>\r\n                      <form-input name=\"quantity\" [type]=\"'number'\" [placeholder]=\"'Quantity'\" [(ngModel)]=\"data.qty\"\r\n                        autofocus required></form-input>\r\n\r\n                    </div>\r\n\r\n                    <label>Minimal Order</label>\r\n                    <form-input name=\"order\" [type]=\"'text'\" [(ngModel)]=\"data.min_order\" autofocus required>\r\n                    </form-input>\r\n\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n            <div class=\" col-md-4\">\r\n              <div class=\"card mb-9\">\r\n                <div class=\"card-header\">\r\n                  <h2>Others</h2>\r\n                </div>\r\n                <div class=\"card-content\">\r\n                  <div class=\"col-md-12\">\r\n\r\n                    <div *ngIf=\"data.type == 'evoucher'\" class=\"evoucher-condition-wrapper\">\r\n\r\n                      <label>Redeem Type</label>\r\n                      <form-select name=\"redeem_type\" [disabled]=\"false\" [(ngModel)]=\"data.redeem_type\"\r\n                        [data]=\"redeem_type\" autofocus required>\r\n                      </form-select>\r\n                    </div>\r\n\r\n                    <div *ngIf=\"data.type == 'product'\" class=\"product-condition-wrapper\">\r\n\r\n                      <label>Condition</label>\r\n                      <form-select width=\"50%\" name=\"condition\" [disabled]=\"false\" [(ngModel)]=\"data.condition\"\r\n                       [data]=\"condition\" autofocus required>\r\n                      </form-select>\r\n\r\n                      <label>Weight</label>\r\n                      <form-input name=\"weight\" [type]=\"'number'\" [(ngModel)]=\"data.weight\" autofocus required>\r\n                      </form-input>\r\n\r\n                      <label>Dimension Length</label>\r\n                      <form-input name=\"length\" [type]=\"'number'\" [placeholder]=\"'length'\"\r\n                        [(ngModel)]=\"data.dimensions.length\" autofocus required></form-input>\r\n\r\n                      <label>Dimension Width</label>\r\n                      <form-input name=\"width\" [type]=\"'number'\" [(ngModel)]=\"data.dimensions.width\" autofocus required>\r\n                      </form-input>\r\n\r\n                      <label>Dimension Height</label>\r\n                      <form-input name=\"height\" [type]=\"'number'\" [(ngModel)]=\"data.dimensions.height\" autofocus\r\n                        required></form-input>\r\n\r\n\r\n\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n              <div class=\"card mb-3\" id=\"img\">\r\n                <div class=\"card-header\">\r\n                  <h2>Images</h2>\r\n                </div>\r\n                <div class=\"card-content\">\r\n                  <div class=\"  image\">\r\n                    <div class=\"\">\r\n                      <div class=\"card\">\r\n                        <div class=\"card-header\">\r\n                          <h3>Big </h3>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                          <img *ngIf=\"data.pic_big_path\" src=\"{{data.base_url+data.pic_big_path}}\" />\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"\">\r\n                      <div class=\"card\">\r\n                        <div class=\"card-header\">\r\n                          <h3>Medium </h3>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                          <img *ngIf=\"data.pic_medium_path\" src=\"{{data.base_url+data.pic_medium_path}}\" />\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"\">\r\n                      <div class=\"card\">\r\n                        <div class=\"card-header\">\r\n                          <h3>Small </h3>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                          <img *ngIf=\"data.pic_small_path\" src=\"{{data.base_url+data.pic_small_path}}\" />\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n\r\n\r\n                  <div class=\"col-md-3 col-sm-3\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n                      </div>\r\n                      <span *ngIf=\"progressBar <58&&progressBar>0\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"file-upload\">\r\n                    <div *ngIf=\"errorFile\" class=\"error_label\"><mark>Error: {{errorFile}}</mark></div>\r\n                    <img *ngIf=\"data.pic_big_path\" [src]=\"data.base_url+data.pic_big_path\" height=\"200\"> <br />\r\n                    <input type=\"file\" (change)=\"onFileSelected($event)\" accept=\"image/*\">\r\n                    <br> <br>\r\n                    <button class=\"btn rounded-btn\" (click)=\"onUpload()\">Upload</button>\r\n                    <button class=\"btn rounded-btn-danger\" (click)=\"cancelThis()\">Cancel</button>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product/detail/product.detail.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product/detail/product.detail.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div [@routerTransition]>\r\n    <app-page-header [heading]=\"'Order Detail for'\" [icon]=\"'fa-table'\"></app-page-header> -->\r\n    <div *ngIf=\"this.productDetail\">\r\n        <app-merchant-edit-product-product></app-merchant-edit-product-product>\r\n    </div>\r\n\r\n<!-- <div *ngIf=\"!edit\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <h1>{{detail.product_name}}</h1>\r\n        <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"product-detail\">\r\n\r\n            <div class=\"row\">\r\n                <div class=\" col-md-8\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2> Product Details </h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <label>Product Name</label>\r\n                                <div name=\"product_name\">{{detail.product_name}}</div>\r\n\r\n                                <label>Product Code</label>\r\n                                <div name=\"product_code\">{{detail.product_code}}</div>\r\n\r\n                                <label>Description</label>\r\n                                <div name=\"description\">{{detail.description}}</div>\r\n\r\n                                <label>Terms and Conditions</label>\r\n                                <div name=\"tnc\">{{detail.tnc}}</div>\r\n -->\r\n                                <!-- <label>Point Price</label>\r\n                                <div name=\"point_price\">{{detail.point_price}}</div> -->\r\n<!-- \r\n                                <label>Price</label>\r\n                                <div name=\"price\">{{detail.price}}</div>\r\n\r\n                                 <label>Fixed Price</label>\r\n                                <div name=\"fixed_price\">{{detail.fixed_price}}</div>\r\n -->\r\n                                <!-- <label>Discount Value</label>\r\n                                <div name=\"discount_value\">{{detail.discount_value}}</div> -->\r\n                                \r\n                                <!-- <label>Earned Points</label>\r\n                                <div name=\"earned_points\">{{detail.earned_points}}</div> -->\r\n<!-- \r\n                                <label>Quantity</label>\r\n                                <div name=\"qty\">{{detail.qty}}</div>\r\n\r\n                                <label>Type</label>\r\n                                <div name=\"type\">{{detail.type}}</div>\r\n\r\n                                \r\n\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\" col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Merchant &amp; status</h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                \r\n                                <label>Status</label>\r\n                                <div name=\"status\">{{detail.active}}</div>\r\n\r\n                                <label>Category</label>\r\n                                <div name=\"status\">{{detail.category}}</div>\r\n\r\n                                <label>Hashtags</label>\r\n                                <div name=\"hashtags\">{{detail.hashtags}}</div>\r\n\r\n                                <label *ngIf=\"detail.merchant_name\">Merchant Name</label>\r\n                                <div *ngIf=\"detail.merchant_name\" name=\"owner_name\">{{detail.merchant_name}}</div>\r\n\r\n                                <label>Favorite</label>\r\n                                <div name=\"favorite\">{{detail.favorite}}</div>\r\n\r\n                                <label>Process ID</label>\r\n                                <div name=\"process_id\">{{detail.process_id}}</div>\r\n\r\n                             \r\n\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Images</h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"  image\">\r\n                                <div class=\"\">\r\n                                    <div class=\"card\">\r\n                                        <div class=\"card-header\">\r\n                                            <h3>Big </h3>\r\n                                        </div>\r\n                                        <div class=\"card-content\">\r\n                                            <img src=\"{{detail.base_url+detail.pic_big_path}}\" />\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"\">\r\n                                    <div class=\"card\">\r\n                                        <div class=\"card-header\">\r\n                                            <h3>Medium </h3>\r\n                                        </div>\r\n                                        <div class=\"card-content\">\r\n                                            <img src=\"{{detail.base_url+detail.pic_medium_path}}\" />\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"\">\r\n                                    <div class=\"card\">\r\n                                        <div class=\"card-header\">\r\n                                            <h3>Small </h3>\r\n                                        </div>\r\n                                        <div class=\"card-content\">\r\n                                            <img src=\"{{detail.base_url+detail.pic_small_path}}\" />\r\n                                        </div>\r\n                                    </div>\r\n                                </div> -->\r\n                                <!-- {{form.value | json}}  -->\r\n                            <!-- </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\"><button class=\"btn delete-button float-right btn-danger\"\r\n                                (click)=\"deleteThis()\">delete</button></div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-product-edit [back]=\"[this, 'editThis']\" [detail]=\"detail\"></app-product-edit>\r\n</div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product/edit/product.edit.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product/edit/product.edit.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <app-page-header [heading]=\"'Product Detail'\" [icon]=\"'fa-table'\"></app-page-header>\r\n    <div class=\"edit-product\">\r\n        <div class=\"e-wrapper\">\r\n            <app-merchant-edit-product-product></app-merchant-edit-product-product>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product/product.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product/product.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <div [@routerTransition]>\r\n           \r\n            <!-- <app-page-header [heading]=\"'Product'\" [icon]=\"'fa-table'\"></app-page-header>\r\n            <h3 style=\"color: white\">File Upload</h3>\r\n            <div class=\"col-md-3 col-sm-3\">\r\n                  <div class=\"pb-framer\">\r\n                              <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n                  \r\n                              </div>\r\n                              <span *ngIf=\"progressBar<58&&progressBar>0\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                              <span class=\"clr-white\" *ngIf=\"progressBar>58\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                        \r\n                        </div>\r\n            \r\n            </div>\r\n            <div class=\"col-md-3 col-sm-3\">\r\n                  <div *ngIf=\"errorFile\" class=\"error_label\"><mark>Error: {{errorFile}}</mark></div>                       \r\n            </div>\r\n            <input class=\"custom-content\" title=\" \" type=\"file\" style=\"color: white;\" (change)=\"onFileSelected($event)\" accept=\".csv,.xlsx,.xls\" multiple/>\r\n            <button class=\"btn rounded-btn\" (click)=\"onUpload()\" >Upload</button>\r\n            <button class=\"btn rounded-btn-danger\" (click)=\"cancelThis()\" >Cancel</button>\r\n            <br><br> -->\r\n            <!-- this section belongs to product item list -->\r\n            <div *ngIf=\"Products && prodDetail==false &&!errorMessage && mci_project == false\">\r\n                  <app-form-builder-table\r\n                  [table_data]  = \"Products\" \r\n                  [searchCallback]= \"[service, 'searchProductsReportLint',this]\" \r\n                  [tableFormat]=\"tableFormat\"\r\n                  [total_page]=\"totalPage\"\r\n                  >\r\n                      \r\n                  </app-form-builder-table>\r\n            </div>\r\n            <div *ngIf=\"Products && prodDetail==false && !errorMessage && mci_project == true\">\r\n                  <app-form-builder-table\r\n                  [table_data]  = \"Products\" \r\n                  [searchCallback]= \"[service, 'searchProductsReportLint',this]\" \r\n                  [tableFormat]=\"tableFormat2\"\r\n                  [total_page]=\"totalPage\"\r\n                  >\r\n                      \r\n                  </app-form-builder-table>\r\n            </div>\r\n            <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n                  <div class=\"text-message\">\r\n                        <i class=\"fas fa-ban\"></i>\r\n                        {{errorMessage}}\r\n                  </div>\r\n            </div>\r\n            <!-- <div *ngIf=\"prodDetail\">\r\n                  <app-product-detail [back]=\"[this,backToHere]\" [detail]=\"prodDetail\"></app-product-detail>\r\n            </div> -->\r\n\r\n           \r\n          </div>\r\n          \r\n"

/***/ }),

/***/ "./src/app/layout/modules/product/add-bulk/product.add-bulk.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/product/add-bulk/product.add-bulk.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n.pb-framer .clr-white {\n  color: white;\n}\n.file-selected {\n  color: red;\n}\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n.container-two-button {\n  display: flex;\n}\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n.btn-upload, .btn-submit {\n  margin-right: 10px;\n}\n.btn-upload {\n  border-color: black;\n}\n.btn-submit {\n  background-color: #3498db;\n  color: white;\n}\n.member-bulk-update-container {\n  padding: 10px !important;\n}\n.bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n.ng-pristine {\n  color: white !important;\n}\n.ng-pristine {\n  color: black !important;\n  width: 100%;\n  border-radius: 5px;\n  padding: 6px 0px;\n}\nlabel {\n  color: black;\n}\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 10px;\n  background-color: #3498db;\n  color: white;\n  height: 30px;\n  align-items: center;\n  display: flex;\n  right: 10px;\n  padding-top: 0px;\n  padding-right: 10px;\n  padding-bottom: 0;\n  padding-left: 10px;\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #000;\n  margin-bottom: 10px;\n  margin-top: 20px;\n}\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 0px;\n  border: 1px solid #f8f9fa;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC9hZGQtYnVsay9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXHByb2R1Y3RcXGFkZC1idWxrXFxwcm9kdWN0LmFkZC1idWxrLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0L2FkZC1idWxrL3Byb2R1Y3QuYWRkLWJ1bGsuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUNDSjtBREFJO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNFUjtBREFJO0VBQ0kscUJBQUE7QUNFUjtBREFJO0VBQ0ksWUFBQTtBQ0VSO0FERUE7RUFDSSxVQUFBO0FDQ0o7QURFQTtFQUNJLGdCQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQ0NKO0FERUE7RUFDSSxhQUFBO0FDQ0o7QURFQTtFQUNJLG1CQUFBO0FDQ0o7QURFQTtFQUNJLGtCQUFBO0FDQ0o7QURFQTtFQUNJLG1CQUFBO0FDQ0o7QURFQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtBQ0NKO0FERUE7RUFDSSx3QkFBQTtBQ0NKO0FERUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7QUNDSjtBREVBO0VBQ0ksdUJBQUE7QUNDSjtBREVBO0VBQ0ksdUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0NKO0FERUE7RUFDSSxZQUFBO0FDQ0o7QURFSTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7QUNDUjtBREFRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0NaO0FEQVk7RUFDSSxpQkFBQTtBQ0VoQjtBRENRO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFFQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDQVo7QURHSTtFQUNJLGFBQUE7QUNEUjtBREVRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0FaO0FESUk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBRUEsbUJBQUE7RUFDQSxnQkFBQTtBQ0hSO0FETVE7RUFDSSxrQkFBQTtBQ0paO0FETVE7RUFDSSxnQkFBQTtBQ0paO0FETVE7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDSlo7QURRUTtFQUNJLHNCQUFBO0FDTlo7QURPWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDTGhCO0FEV0E7RUFDSSxnQkFBQTtFQUVBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ1JKO0FEYUE7RUFDSSxnQkFBQTtFQUVBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUNWSjtBRFlBO0VBQ1EsbUJBQUE7RUFFQSxhQUFBO0FDVlIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0L2FkZC1idWxrL3Byb2R1Y3QuYWRkLWJ1bGsuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGItZnJhbWVye1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgY29sb3I6ICNlNjdlMjI7XHJcbiAgICAucHJvZ3Jlc3NiYXJ7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgei1pbmRleDogLTE7XHJcbiAgICB9XHJcbiAgICAudGVybWluYXRlZC5wcm9ncmVzc2JhcntcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbiAgICB9XHJcbiAgICAuY2xyLXdoaXRle1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxufVxyXG5cclxuLmZpbGUtc2VsZWN0ZWQge1xyXG4gICAgY29sb3I6IHJlZDtcclxufVxyXG5cclxuLmVycm9yLW1zZy11cGxvYWQge1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIHBhZGRpbmctbGVmdDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNvbnRhaW5lci10d28tYnV0dG9uIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5idG4tYWRkLWJ1bGsge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLmJ0bi11cGxvYWQsIC5idG4tc3VibWl0IHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuLmJ0bi11cGxvYWQge1xyXG4gICAgYm9yZGVyLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLmJ0bi1zdWJtaXQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLm1lbWJlci1idWxrLXVwZGF0ZS1jb250YWluZXIge1xyXG4gICAgcGFkZGluZzogMTBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYmctZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5uZy1wcmlzdGluZXtcclxuICAgIGNvbG9yIDogd2hpdGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm5nLXByaXN0aW5le1xyXG4gICAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIHBhZGRpbmc6IDZweCAwcHg7XHJcbn1cclxuXHJcbmxhYmVse1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcbi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDQ4cHg7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICAgICAgLy8gbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzAwMDtcclxuICAgICAgICAvLyB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICAgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYWw7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2Y4ZjlmYTtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuLnJvdW5kZWQtYnRuIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDQwJTtcclxuICAgIC8vIG1hcmdpbi1yaWdodDogNTAlO1xyXG4gICAgfVxyXG5cclxuLnJvdW5kZWQtYnRuLWRhbmdlciB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgY29sb3I6ICM1NTU7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIH1cclxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2QTRGRiwgMTUlKTtcclxuICAgICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4gICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB9XHJcbi8vIC5kYXRlcGlja2VyLWlucHV0e1xyXG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlOyAgICBcclxuLy8gICAgIH1cclxuLy8gLmZvcm0tY29udHJvbHtcclxuLy8gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuLy8gICAgIHRvcDogMTBweDtcclxuLy8gICAgIGxlZnQ6IDBweDtcclxuLy8gfVxyXG4iLCIucGItZnJhbWVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGhlaWdodDogMjVweDtcbiAgY29sb3I6ICNlNjdlMjI7XG59XG4ucGItZnJhbWVyIC5wcm9ncmVzc2JhciB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJhY2tncm91bmQ6ICMzNDk4ZGI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAyNXB4O1xuICB6LWluZGV4OiAtMTtcbn1cbi5wYi1mcmFtZXIgLnRlcm1pbmF0ZWQucHJvZ3Jlc3NiYXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG59XG4ucGItZnJhbWVyIC5jbHItd2hpdGUge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5maWxlLXNlbGVjdGVkIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLmVycm9yLW1zZy11cGxvYWQge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcbn1cblxuLmNvbnRhaW5lci10d28tYnV0dG9uIHtcbiAgZGlzcGxheTogZmxleDtcbn1cblxuLmJ0bi1hZGQtYnVsayB7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5idG4tdXBsb2FkLCAuYnRuLXN1Ym1pdCB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLmJ0bi11cGxvYWQge1xuICBib3JkZXItY29sb3I6IGJsYWNrO1xufVxuXG4uYnRuLXN1Ym1pdCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLm1lbWJlci1idWxrLXVwZGF0ZS1jb250YWluZXIge1xuICBwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5iZy1kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubmctcHJpc3RpbmUge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuLm5nLXByaXN0aW5lIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDZweCAwcHg7XG59XG5cbmxhYmVsIHtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1pbi1oZWlnaHQ6IDQ4cHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG4gIGhlaWdodDogMzBweDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZGlzcGxheTogZmxleDtcbiAgcmlnaHQ6IDEwcHg7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMDAwO1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBwYWRkaW5nOiAwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmOGY5ZmE7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5yb3VuZGVkLWJ0biB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG59XG5cbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBjb2xvcjogIzU1NTtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi5yb3VuZGVkLWJ0bjpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMwYTdiZmY7XG4gIG91dGxpbmU6IG5vbmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/product/add-bulk/product.add-bulk.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/product/add-bulk/product.add-bulk.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ProductAddBulkComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductAddBulkComponent", function() { return ProductAddBulkComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ProductAddBulkComponent = /** @class */ (function () {
    function ProductAddBulkComponent(productService) {
        this.productService = productService;
        this.name = "";
        this.Members = [];
        this.startUploading = false;
        this.form = {
            id_pel: "",
            nama_toko: "",
            nama_distributor: "",
            alamat_toko: "",
            kelurahan_toko: "-",
            kecamatan_toko: "-",
            kota_toko: "-",
            provinsi_toko: "-",
            kode_pos_toko: "-",
            landmark_toko: "-",
            nama_pemilik: "",
            telp_pemilik: "",
            no_wa_pemilik: "",
            ktp_pemilik: "",
            foto_ktp_pemilik: "-",
            npwp_pemilik: "",
            foto_npwp_pemilik: "-",
            alamat_rumah: "",
            kelurahan_rumah: "",
            kecamatan_rumah: "",
            kota_rumah: "",
            provinsi_rumah: "",
            kode_pos_rumah: "",
            landmark_rumah: "-",
            description: "nonupfront",
            hadiah_dikuasakan: "TIDAK KUASA",
            nama_penerima_gopay: "-",
            ktp_penerima: "-",
            foto_ktp_penerima: "-",
            npwp_penerima: "-",
            foto_npwp_penerima: "-",
            telp_penerima_gopay: "-",
            no_wa_penerima: "-",
            nama_id_gopay: "-",
            jenis_akun: "-",
            saldo_terakhir: "-",
        };
        this.errorLabel = false;
        this.member_status_type = [
            { label: 'Superuser', value: 'superuser' },
            { label: 'Member', value: 'member', selected: 1 },
            { label: 'Marketing', value: 'marketing' },
            { label: 'Admin', value: 'admin' },
            { label: 'Merchant', value: 'merchant' }
        ];
        this.group_desc = [
            { label: 'Nonupfront', value: 'nonupfront' },
            { label: 'Upfront', value: 'upfront' }
        ];
        this.kuasa_option = [
            { label: 'TIDAK KUASA', value: 'TIDAK KUASA' },
            { label: 'KUASA', value: 'KUASA' }
        ];
        this.isBulkUpdate = false;
        this.showUploadButton = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.prodOnUpload = false;
    }
    ProductAddBulkComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ProductAddBulkComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                // this.service = this.productService;
                // let result: any = await this.productService.getMembersLint();
                // this.Members = result.result;
                this.selectedFile = null;
                // this.prodOnUpload = false;
                this.startUploading = false;
                return [2 /*return*/];
            });
        });
    };
    ProductAddBulkComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        // var reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]); //
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
        this.addBulk.nativeElement.value = '';
    };
    ProductAddBulkComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    ProductAddBulkComponent.prototype.addBulkProduct = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, result, e_1, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.startUploading = true;
                        this.cancel = false;
                        payload = {
                            type: 'application/form-data',
                        };
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productService.addProductBulk(this.selectedFile, this, payload)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            this.firstLoad();
                        }
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.startUploading = false;
                        this.errorLabel = (e_1.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ProductAddBulkComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    ProductAddBulkComponent.prototype.backTo = function () {
        window.history.back();
    };
    ProductAddBulkComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('addBulk', { static: false }),
        __metadata("design:type", Object)
    ], ProductAddBulkComponent.prototype, "addBulk", void 0);
    ProductAddBulkComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-add-bulk',
            template: __webpack_require__(/*! raw-loader!./product.add-bulk.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product/add-bulk/product.add-bulk.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./product.add-bulk.component.scss */ "./src/app/layout/modules/product/add-bulk/product.add-bulk.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"]])
    ], ProductAddBulkComponent);
    return ProductAddBulkComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product/add/product.add.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/product/add/product.add.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".add-product {\n  border: 1px solid rgba(0, 0, 0, 0.125);\n  border-radius: 0.25rem;\n  background-color: white;\n  margin: 20px;\n}\n.add-product .e-wrapper {\n  position: relative;\n}\n.heading {\n  color: white;\n  font-weight: bold;\n  letter-spacing: 0.8px;\n  margin: 50px 20px 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC9hZGQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxwcm9kdWN0XFxhZGRcXHByb2R1Y3QuYWRkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0L2FkZC9wcm9kdWN0LmFkZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHNDQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7QUNDSjtBREFJO0VBQ0ksa0JBQUE7QUNFUjtBREVBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EscUJBQUE7RUFDQSxzQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC9hZGQvcHJvZHVjdC5hZGQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYWRkLXByb2R1Y3R7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTI1KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIG1hcmdpbjogMjBweDtcclxuICAgIC5lLXdyYXBwZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgfVxyXG59XHJcblxyXG4uaGVhZGluZyB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAuOHB4O1xyXG4gICAgbWFyZ2luOiA1MHB4IDIwcHggMjBweDtcclxufVxyXG5cclxuLy8gLmNhcmQtaGVhZGVye1xyXG4vLyAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgICAgbWluLWhlaWdodDogNDhweDtcclxuXHJcbi8vICAgICAuYmFja19idXR0b257XHJcbi8vICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4vLyAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbi8vICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4vLyAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4vLyAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4vLyAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICBcclxuLy8gICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4vLyAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbi8vICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbi8vICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuLy8gICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4vLyAgICAgICAgIGl7XHJcbi8vICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4vLyAgICAgICAgIH1cclxuLy8gICAgIH1cclxuLy8gICAgIC5zYXZlX2J1dHRvbntcclxuLy8gICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbi8vICAgICAgICAgdG9wOiA3cHg7XHJcbi8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuLy8gICAgICAgICBjb2xvcjogI2ZmZjtcclxuLy8gICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgXHJcbi8vICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbi8vICAgICAgICAgZGlzcGxheTogZmxleDtcclxuLy8gICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuLy8gICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgXHJcbi8vICAgICAgICAgaXtcclxuLy8gICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4vLyAgICAgfVxyXG4vLyAgICAgLnNhdmVfYnV0dG9uLnRydWV7XHJcbi8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuLy8gICAgIH1cclxuLy8gfVxyXG4vLyAucGItZnJhbWVye1xyXG4vLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgICAgaGVpZ2h0OiAyNXB4O1xyXG4vLyAgICAgY29sb3I6ICNlNjdlMjI7XHJcbi8vICAgICAucHJvZ3Jlc3NiYXJ7XHJcbi8vICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuLy8gICAgICAgICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xyXG4vLyAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuLy8gICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbi8vICAgICAgICAgei1pbmRleDogLTE7XHJcbi8vICAgICB9XHJcbi8vICAgICAudGVybWluYXRlZC5wcm9ncmVzc2JhcntcclxuLy8gICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbi8vICAgICB9XHJcbi8vICAgICAuY2xyLXdoaXRle1xyXG4vLyAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIH1cclxuLy8gfVxyXG5cclxuLy8gbGFiZWx7XHJcbi8vICAgICBjb2xvcjpibGFjaztcclxuLy8gfVxyXG5cclxuLy8gLm5nLXByaXN0aW5le1xyXG4vLyAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuXHJcbi8vIC5yb3VuZGVkLWJ0bi1zdWJtaXQge1xyXG4vLyAgICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuLy8gICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIGZvbnQtc2l6ZTogMThweDtcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4vLyAgICAgcGFkZGluZzogMCA0MHB4O1xyXG4vLyAgICAgYmFja2dyb3VuZDogIzU2YTRmZjtcclxuLy8gbWFyZ2luLWxlZnQ6IDQwJTtcclxuLy8gbWFyZ2luLXJpZ2h0OiA1MCU7XHJcbi8vIH1cclxuLy8gLnJvdW5kZWQtYnRuLXN1Ym1pdDpob3ZlciB7XHJcbi8vICAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2QTRGRiwgMTUlKTtcclxuLy8gICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbi8vICAgICBvdXRsaW5lOiBub25lO1xyXG4vLyB9XHJcblxyXG4vLyAuYXV0by1uYW1le1xyXG4vLyAgICAgd2lkdGg6IDE1MiUgIWltcG9ydGFudDtcclxuLy8gfVxyXG5cclxuLy8gLm5nLXByaXN0aW5le1xyXG4vLyAgICAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XHJcbi8vICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuLy8gICAgIHBhZGRpbmc6IDZweCAxMnB4O1xyXG4vLyB9XHJcblxyXG4vLyAucm91bmRlZC1idG4ge1xyXG4vLyAgICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuLy8gICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIGZvbnQtc2l6ZTogMThweDtcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4vLyAgICAgcGFkZGluZzogMCAxNXB4O1xyXG4vLyAgICAgYmFja2dyb3VuZDogIzU2YTRmZjtcclxuLy8gfVxyXG4vLyAucm91bmRlZC1idG46aG92ZXIge1xyXG4vLyAgICAgYmFja2dyb3VuZDogZGFya2VuKCM1NkE0RkYsIDE1JSk7XHJcbi8vICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4vLyAgICAgb3V0bGluZTogbm9uZTtcclxuLy8gfVxyXG5cclxuLy8gLnJvdW5kZWQtYnRuLWRhbmdlciB7XHJcbi8vICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbi8vICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4vLyAgICAgY29sb3I6IHdoaXRlO1xyXG4vLyAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4vLyAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbi8vICAgICBwYWRkaW5nOiAwIDE1cHg7XHJcbi8vICAgICBiYWNrZ3JvdW5kOiAjZGMzNTQ1O1xyXG4vLyB9XHJcbi8vIC5yb3VuZGVkLWJ0bi1kYW5nZXI6aG92ZXIge1xyXG4vLyAgICAgYmFja2dyb3VuZDogZGFya2VuKCNkYzM1NDUsIDE1JSk7XHJcbi8vICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4vLyAgICAgb3V0bGluZTogbm9uZTtcclxuLy8gfVxyXG5cclxuLy8gLmNhcmQtZGV0YWlse1xyXG4vLyAgICAgPi5jYXJkLWhlYWRlcntcclxuLy8gICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vICAgICB9XHJcbi8vICAgICAuY2FyZC1jb250ZW50e1xyXG4vLyAgICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICBcclxuLy8gICAgICAgICA+LmNvbC1tZC0xMntcclxuLy8gICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbi8vICAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuLy8gICAgICAgICB9XHJcbi8vICAgICB9XHJcbiAgICBcclxuLy8gICAgIGgxe1xyXG4vLyAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuLy8gICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuLy8gICAgICAgICBjb2xvcjogIzU1NTtcclxuLy8gICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4vLyAgICAgfVxyXG4vLyAgICAgLnByb2R1Y3QtZGV0YWlse1xyXG4gICAgICAgXHJcbi8vICAgICAgICAgbGFiZWx7XHJcbi8vICAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgICAgIGxhYmVsK2RpdntcclxuLy8gICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuLy8gICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbi8vICAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbi8vICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbi8vICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbi8vICAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbi8vICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuLy8gICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgICAgIC5pbWFnZXtcclxuLy8gICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuLy8gICAgICAgICAgICAgPmRpdntcclxuLy8gICAgICAgICAgICAgICAgIHdpZHRoOiAzMy4zMzMzJTtcclxuLy8gICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuLy8gICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4vLyAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgICAgIC5jYXJkLWhlYWRlcntcclxuLy8gICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbi8vICAgICAgICAgICAgIH1cclxuLy8gICAgICAgICAgICAgaDN7XHJcbi8vICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbi8vICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbi8vICAgICAgICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuLy8gICAgICAgICAgICAgfVxyXG4vLyAgICAgICAgICAgICAuY2FyZC1jb250ZW50e1xyXG4vLyAgICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuLy8gICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4vLyAgICAgICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4vLyAgICAgICAgICAgICAgICAgaW1ne1xyXG4vLyAgICAgICAgICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuLy8gICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIFxyXG4vLyAgICAgICAgICAgICAgICAgfVxyXG4vLyAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgfVxyXG5cclxuLy8gICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4vLyAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4vLyAgICAgICAgICAgICBoMntcclxuLy8gICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuLy8gICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuLy8gICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbi8vICAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuLy8gICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4vLyAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgfVxyXG4vLyB9XHJcblxyXG4vLyAuZmlsZS11cGxvYWR7XHJcbi8vICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vIH1cclxuXHJcbi8vICNpbWd7XHJcbi8vICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4vLyB9IiwiLmFkZC1wcm9kdWN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyNSk7XG4gIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBtYXJnaW46IDIwcHg7XG59XG4uYWRkLXByb2R1Y3QgLmUtd3JhcHBlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmhlYWRpbmcge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBsZXR0ZXItc3BhY2luZzogMC44cHg7XG4gIG1hcmdpbjogNTBweCAyMHB4IDIwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/product/add/product.add.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/product/add/product.add.component.ts ***!
  \*********************************************************************/
/*! exports provided: ProductAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductAddComponent", function() { return ProductAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var ProductAddComponent = /** @class */ (function () {
    function ProductAddComponent(productService, router) {
        this.productService = productService;
        this.router = router;
        this.name = "";
        this.categoryProduct = [];
        this.productType = [];
        this.merchantList = [];
        this.merchant_group = [];
        this.condition = [
            { label: "NEW", value: "new" },
            { label: "SECOND", value: "second" },
        ];
        this.redeem_type = [
            { label: "Voucher Link", value: "voucher_link" },
            { label: "Voucher Code", value: "voucher_code" }
        ];
        // condition:any;
        this.Products = [];
        this.loading = false;
        /* public productStatus = [
          {label:"ACTIVE", value:"ACTIVE", selected:1}
         ,{label:"INACTIVE", value:"INACTIVE"}
       ]; */
        this.toggleDelete = 0;
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
        this.errorLabel = false;
        this.errorMessage = false;
        this.newData = "JUmp start";
        this.data = {
            merchant_username: '',
            type: '',
            category: [],
            price: 0,
            fixed_price: 0,
            tnc: '',
            description: '',
            qty: 0,
            product_name: '',
            product_code: '',
            condition: '',
            weight: '',
            dimensions: {
                length: 0,
                width: 0,
                height: 0
            },
            // dimension_length: '',
            // dimension_width: '',
            // dimension_height: '',
            min_order: '',
        };
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default.a;
    }
    ProductAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ProductAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    ProductAddComponent.prototype.getCategory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, e_1;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.productService.getCategoryLint()];
                    case 1:
                        _a.category = _b.sent();
                        this.category.result.forEach(function (element) {
                            _this.categoryProduct.push({ label: element.name, value: element.name });
                        });
                        this.category = this.categoryProduct[0].value;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _b.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.getKeyname = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.configGroup()];
                    case 1:
                        _a.keyname = _b.sent();
                        // console.log("PERBEDAAN MERCHANT GROUP/ PRODUCTCONFIG", this.keyname)l
                        this.keyname.result.merchant_group.forEach(function (element) {
                            _this.merchant_group.push({ label: element.name, value: element.name });
                        });
                        this.data.merchant_group = this.merchant_group[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    // async redeemGroup(){
    //   this.keyname = await this.productService.redeemGroup();
    // }
    ProductAddComponent.prototype.getProductType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.configuration()];
                    case 1:
                        _a.product_type = _b.sent();
                        console.log("KELUAR GA:", this.product_type);
                        this.product_type.result.product_type.forEach(function (element) {
                            _this.productType.push({ label: element, value: element });
                        });
                        this.data.type = this.productType[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.getRedeemType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.configuration()];
                    case 1:
                        _a.product_type = _b.sent();
                        // console.log("KELUAR GA:", this.product_type);
                        this.product_type.result.evoucher_redeem.forEach(function (element) {
                            _this.productType.push({ label: element, value: element });
                        });
                        this.data.type = this.productType[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.getConditionType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.configuration()];
                    case 1:
                        _a.product_type = _b.sent();
                        this.product_type.result.product_condition.forEach(function (element) {
                            _this.productType.push({ label: element, value: element });
                            console.log("DISINI", _this.productType);
                        });
                        this.data.type = this.productType[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.getMerchant = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.getMerchantList()];
                    case 1:
                        _a.merchant_username = _b.sent();
                        this.merchant_username.result.forEach(function (element) {
                            _this.merchantList.push({ label: element.merchant_name, value: element.merchant_username });
                        });
                        this.data.merchant_username = this.merchantList[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.selectCategory = function (event) {
        this.category = event.target.value;
        this.data.category = [event.target.value];
    };
    ProductAddComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        // console.log(this.dimensions)
                        this.data.price = parseInt(this.data.price);
                        this.data.fixed_price = parseInt(this.data.fixed_price);
                        this.data.min_order = parseInt(this.data.min_order);
                        if (this.data.type == 'product') {
                            this.data.weight = parseFloat(this.data.weight);
                            this.data.dimensions.length = parseFloat(this.data.dimensions.length);
                            this.data.dimensions.width = parseFloat(this.data.dimensions.width);
                            this.data.dimensions.height = parseFloat(this.data.dimensions.height);
                            delete this.data.merchant_group;
                        }
                        else {
                            delete this.data.weight;
                            delete this.data.dimensions;
                            delete this.data.condition;
                        }
                        this.data.qty = parseInt(this.data.qty);
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.addProductsLint(this.data)];
                    case 1:
                        result = _a.sent();
                        this.Products = result.result;
                        alert("Product has been added!");
                        this.router.navigate(['administrator/productadmin/edit'], { queryParams: { id: this.data._id } });
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        try {
            this.errorFile = false;
            var fileMaxSize_1 = 3000000; // let say 3Mb
            Array.from(event.target.files).forEach(function (file) {
                if (file.size > fileMaxSize_1) {
                    _this.errorFile = "Maximum File Upload is 3MB";
                }
            });
            this.selectedFile = event.target.files[0];
        }
        catch (e) {
            this.errorLabel = (e.message); //conversion to Error type
            var message = this.errorLabel;
            if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
            }
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                icon: 'error',
                title: message,
            });
        }
    };
    ProductAddComponent.prototype.onSelectFile = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]); // read file as data url
            reader.onload = function (event) {
                var _event = event;
                _this.url = _event.target.result;
            };
        }
    };
    ProductAddComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    ProductAddComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    ProductAddComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, product, e_3, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        result = void 0;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        console.log("result", result);
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) { _this.callAfterUpload(result); })];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2:
                        if (result) {
                            product = this.detail;
                            this.detail = __assign({}, product, result);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_3 = _a.sent();
                        this.errorLabel = (e_3.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: 
                    //   this.onUpload.
                    return [2 /*return*/, false];
                }
            });
        });
    };
    ProductAddComponent.prototype.callAfterUpload = function (result) {
        this.data = __assign({}, this.data, result);
    };
    ProductAddComponent.prototype.backToDetail = function () {
        window.history.back();
        // this.back[0][this.back[1]]();
    };
    ProductAddComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductAddComponent.prototype, "back", void 0);
    ProductAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-add',
            template: __webpack_require__(/*! raw-loader!./product.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product/add/product.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./product.add.component.scss */ "./src/app/layout/modules/product/add/product.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], ProductAddComponent);
    return ProductAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product/detail/product.detail.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/layout/modules/product/detail/product.detail.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  top: 8px;\n  left: 15px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 8px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  right: 10px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .product-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .product-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .product-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .product-detail .image {\n  overflow: hidden;\n}\n\n.card-detail .product-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n\n.card-detail .product-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n\n.card-detail .product-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n\n.card-detail .product-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n\n.card-detail .product-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n\n.card-detail .product-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .product-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC9kZXRhaWwvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxwcm9kdWN0XFxkZXRhaWxcXHByb2R1Y3QuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0L2RldGFpbC9wcm9kdWN0LmRldGFpbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0EsYUFBQTtBQ0NKOztBREdJO0VBQ0ksa0JBQUE7QUNBUjs7QURDUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDQVo7O0FEQ1k7RUFDSSxpQkFBQTtBQ0NoQjs7QURFUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ0FaOztBREVZO0VBQ0ksaUJBQUE7QUNBaEI7O0FES0k7RUFDSSxhQUFBO0FDSFI7O0FESVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDRlo7O0FETUk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0pSOztBRE9RO0VBQ0ksa0JBQUE7QUNMWjs7QURPUTtFQUNJLGdCQUFBO0FDTFo7O0FET1E7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDTFo7O0FEUVE7RUFDSSxnQkFBQTtBQ05aOztBRE9ZO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDTGhCOztBRE9ZO0VBQ0kseUJBQUE7QUNMaEI7O0FET1k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDTGhCOztBRE9ZO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0xoQjs7QURNZ0I7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQ0pwQjs7QURVUTtFQUNJLHNCQUFBO0FDUlo7O0FEU1k7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ1BoQiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3Byb2R1Y3QvZGV0YWlsL3Byb2R1Y3QuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWRlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4uY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA4cHg7XHJcbiAgICAgICAgICAgIGxlZnQ6IDE1cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuZWRpdF9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA4cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLnByb2R1Y3QtZGV0YWlse1xyXG4gICAgICAgIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5pbWFnZXtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgPmRpdntcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzMy4zMzMzJTtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jYXJkLWhlYWRlcntcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaDN7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiLmJnLWRlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG5cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogOHB4O1xuICBsZWZ0OiAxNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogOHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICByaWdodDogMTBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5wcm9kdWN0LWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLnByb2R1Y3QtZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAucHJvZHVjdC1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLnByb2R1Y3QtZGV0YWlsIC5pbWFnZSB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uY2FyZC1kZXRhaWwgLnByb2R1Y3QtZGV0YWlsIC5pbWFnZSA+IGRpdiB7XG4gIHdpZHRoOiAzMy4zMzMzJTtcbiAgcGFkZGluZzogNXB4O1xuICBmbG9hdDogbGVmdDtcbn1cbi5jYXJkLWRldGFpbCAucHJvZHVjdC1kZXRhaWwgLmltYWdlIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG4uY2FyZC1kZXRhaWwgLnByb2R1Y3QtZGV0YWlsIC5pbWFnZSBoMyB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAucHJvZHVjdC1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLmNhcmQtZGV0YWlsIC5wcm9kdWN0LWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCBpbWcge1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNhcmQtZGV0YWlsIC5wcm9kdWN0LWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5wcm9kdWN0LWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/product/detail/product.detail.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/layout/modules/product/detail/product.detail.component.ts ***!
  \***************************************************************************/
/*! exports provided: ProductDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailComponent", function() { return ProductDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ProductDetailComponent = /** @class */ (function () {
    function ProductDetailComponent(productService, route, router) {
        this.productService = productService;
        this.route = route;
        this.router = router;
        this.edit = false;
        this.errorLabel = false;
    }
    ProductDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ProductDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                    var historyID;
                    return __generator(this, function (_a) {
                        historyID = params.id;
                        this.loadDetail(historyID);
                        return [2 /*return*/];
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    ProductDetailComponent.prototype.loadDetail = function (historyID) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.detailProducts(historyID)];
                    case 1:
                        result = _a.sent();
                        this.productDetail = result.result[0];
                        console.log(this.productDetail);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductDetailComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductDetailComponent.prototype, "back", void 0);
    ProductDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-detail',
            template: __webpack_require__(/*! raw-loader!./product.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product/detail/product.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./product.detail.component.scss */ "./src/app/layout/modules/product/detail/product.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ProductDetailComponent);
    return ProductDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product/edit/product.edit.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/layout/modules/product/edit/product.edit.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".edit-product {\n  border: 1px solid rgba(0, 0, 0, 0.125);\n  border-radius: 0.25rem;\n  background-color: white;\n}\n.edit-product .e-wrapper {\n  position: relative;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC9lZGl0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xccHJvZHVjdFxcZWRpdFxccHJvZHVjdC5lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0L2VkaXQvcHJvZHVjdC5lZGl0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksc0NBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0FDQ0o7QURBSTtFQUNJLGtCQUFBO0FDRVIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0L2VkaXQvcHJvZHVjdC5lZGl0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVkaXQtcHJvZHVjdHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMjUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgLmUtd3JhcHBlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB9XHJcbn0iLCIuZWRpdC1wcm9kdWN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyNSk7XG4gIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuLmVkaXQtcHJvZHVjdCAuZS13cmFwcGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/product/edit/product.edit.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/product/edit/product.edit.component.ts ***!
  \***********************************************************************/
/*! exports provided: ProductEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductEditComponent", function() { return ProductEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ProductEditComponent = /** @class */ (function () {
    // listedData: any[];
    function ProductEditComponent(productService, modalService) {
        this.productService = productService;
        this.modalService = modalService;
        this.selectedFile = null;
        this.loading = false;
        this.categoryProduct = [];
        this.editStatus = [
            { label: "ACTIVE", value: 1 },
            { label: "INACTIVE", value: 0 }
        ];
        this.errorLabel = false;
        this.cancel = false;
        this.errorFile = false;
        this.product_code = [];
        this.progressBar = 0;
        this.product_sku = [];
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default.a;
        this.productData = {};
    }
    ProductEditComponent.prototype.ngOnInit = function () {
        this.getCategory();
        this.firstLoad();
        // this.setStatus()
    };
    ProductEditComponent.prototype.getCategory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.getCategoryLint()];
                    case 1:
                        _a.category = _b.sent();
                        this.category.forEach(function (element) {
                            _this.categoryProduct.push({ label: element.name, value: element.name });
                        });
                        this.category = this.categoryProduct[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductEditComponent.prototype.selectCategory = function (event) {
        this.category = event.target.value;
        this.productData.category = [event.target.value];
    };
    ProductEditComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        result = void 0;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        console.log("result", result);
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) { _this.callAfterUpload(result); })];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ProductEditComponent.prototype.callAfterUpload = function (result) {
        this.productData = __assign({}, this.productData, result);
        console.log("THE RESULT", result);
        console.log("this.productData", this.productData);
    };
    ProductEditComponent.prototype.setStatus = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.statusEdit];
                    case 1:
                        _a.service = _b.sent();
                        this.productData.previous_status = this.productData.status;
                        this.editStatus.forEach(function (element, index) {
                            if (element.value == _this.productData.active) {
                                _this.editStatus[index].selected = 1;
                            }
                            if (element.value == _this.productData.active) {
                                _this.editStatus[index].selected = 0;
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductEditComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    ProductEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    this.detail.previous_product_code = this.detail.product_code;
                    if (this.detail._id)
                        delete this.detail._id;
                    this.categoryProduct.forEach(function (element, index, products_ids) {
                        if (element.value == _this.detail.status) {
                            _this.categoryProduct[index].selected = 1;
                        }
                    });
                    console.log("SKU", this.detail.product_sku);
                    Object.keys(this.detail).forEach(function (key) {
                        _this.productData[key] = _this.detail[key];
                    });
                    console.log("product data : ", this.productData);
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    ProductEditComponent.prototype.removeSKU = function (sku) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log("sku : ", sku);
                return [2 /*return*/];
            });
        });
    };
    ProductEditComponent.prototype.backToDetail = function () {
        history.back();
    };
    ProductEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var prevProductCode, result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.productData.min_order = parseInt(this.productData.min_order);
                        if (this.productData.type == 'Product') {
                            this.productData.dimensions.length = parseInt(this.productData.dimensions.length);
                            this.productData.dimensions.width = parseInt(this.productData.dimensions.width);
                            this.productData.dimensions.height = parseInt(this.productData.dimensions.height);
                        }
                        this.productData.price = parseInt(this.productData.price);
                        this.productData.fixed_price = parseInt(this.productData.fixed_price);
                        this.productData.qty = parseInt(this.productData.qty);
                        prevProductCode = this.productData.previous_product_code;
                        delete this.productData.previous_product_code;
                        delete this.productData.status;
                        console.log("RESPON", this.productData);
                        if (this.productData.active)
                            this.productData.active = parseInt(this.productData.active);
                        this.loading = !this.loading;
                        return [4 /*yield*/, this.productService.updateProduct(this.productData, prevProductCode)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            alert("Saved!");
                        }
                        // console.log("this.detail", this.detail)
                        this.loading = !this.loading;
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        alert(this.errorLabel = (e_2.message)); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // async saveSKU(sku){
    //   var item = this.detail.product_sku;
    //   var push = item.push(sku);
    //   console.log("Berhasil ga", push)
    // }
    //  
    ProductEditComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
    };
    ProductEditComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductEditComponent.prototype, "back", void 0);
    ProductEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-edit',
            template: __webpack_require__(/*! raw-loader!./product.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product/edit/product.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./product.edit.component.scss */ "./src/app/layout/modules/product/edit/product.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"]])
    ], ProductEditComponent);
    return ProductEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product/product-routing.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/modules/product/product-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: ProductRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductRoutingModule", function() { return ProductRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _product_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./product.component */ "./src/app/layout/modules/product/product.component.ts");
/* harmony import */ var _add_product_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/product.add.component */ "./src/app/layout/modules/product/add/product.add.component.ts");
/* harmony import */ var _add_bulk_product_add_bulk_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./add-bulk/product.add-bulk.component */ "./src/app/layout/modules/product/add-bulk/product.add-bulk.component.ts");
/* harmony import */ var _edit_product_edit_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit/product.edit.component */ "./src/app/layout/modules/product/edit/product.edit.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '', component: _product_component__WEBPACK_IMPORTED_MODULE_2__["ProductComponent"]
    },
    {
        path: 'add', component: _add_product_add_component__WEBPACK_IMPORTED_MODULE_3__["ProductAddComponent"]
    },
    {
        path: 'add-bulk', component: _add_bulk_product_add_bulk_component__WEBPACK_IMPORTED_MODULE_4__["ProductAddBulkComponent"]
    },
    // {
    //   path:'detail', component: ProductDetailComponent
    // },
    {
        path: 'edit', component: _edit_product_edit_component__WEBPACK_IMPORTED_MODULE_5__["ProductEditComponent"]
    }
];
var ProductRoutingModule = /** @class */ (function () {
    function ProductRoutingModule() {
    }
    ProductRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ProductRoutingModule);
    return ProductRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/product/product.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/layout/modules/product/product.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n.pb-framer .clr-white {\n  color: white;\n}\nh1 {\n  color: white;\n}\n#approve {\n  border: 1px solid #4b5872;\n  display: inline-block;\n  margin-right: 15px;\n  font-weight: 400;\n  text-align: center;\n  white-space: nowrap;\n  vertical-align: middle;\n  padding: 0.375rem 0.75rem;\n  font-size: 1rem;\n  line-height: 1.5;\n  border-radius: 0.25rem;\n}\n.fa {\n  margin-right: 5px;\n}\n.error_label {\n  color: white;\n}\n.custom-content::-webkit-file-upload-button {\n  visibility: hidden;\n}\n.custom-content::before {\n  content: \"Choose File\";\n  margin-right: 5px;\n  display: inline-block;\n  border: 1px solid #999;\n  border-radius: 5px;\n  padding: 8px;\n  outline: none;\n  white-space: nowrap;\n  -webkit-user-select: none;\n  cursor: pointer;\n  font-weight: 700;\n  font-size: 10pt;\n  color: white;\n  letter-spacing: 0.5px;\n}\nmark {\n  background-color: #dc3545;\n  color: white;\n}\n.rounded-btn {\n  border-radius: 30px;\n  color: white;\n  font-size: 15px;\n  line-height: 40px;\n  padding: 0 15px;\n  background: #56a4ff;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n.rounded-btn-danger {\n  border-radius: 30px;\n  color: white;\n  font-size: 15px;\n  line-height: 40px;\n  padding: 0 15px;\n  background: #dc3545;\n}\n.rounded-btn-danger:hover {\n  background: #a71d2a;\n  outline: none;\n}\n@media screen and (max-width: 600px) {\n  .rounded-btn {\n    border-radius: 30px;\n    color: white;\n    font-size: 15px;\n    line-height: 30px;\n    padding: 0 12px;\n    background: #56a4ff;\n    margin-left: 25%;\n    margin-top: 10px;\n    margin-right: 10px;\n  }\n\n  .rounded-btn:hover {\n    background: #0a7bff;\n    outline: none;\n  }\n\n  .rounded-btn-danger {\n    border-radius: 30px;\n    color: white;\n    font-size: 15px;\n    line-height: 30px;\n    padding: 0 12px;\n    background: #dc3545;\n    margin-right: 25%;\n    margin-top: 10px;\n  }\n\n  .rounded-btn-danger:hover {\n    background: #a71d2a;\n    outline: none;\n  }\n}\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 40px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXHByb2R1Y3RcXHByb2R1Y3QuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3Byb2R1Y3QvcHJvZHVjdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ0NGO0FEQUU7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ0VKO0FEQUU7RUFDRSxxQkFBQTtBQ0VKO0FEQUU7RUFDRSxZQUFBO0FDRUo7QURFQTtFQUNFLFlBQUE7QUNDRjtBREVBO0VBQ0UseUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0FDQ0Y7QURDQTtFQUNFLGlCQUFBO0FDRUY7QURFQTtFQUNFLFlBQUE7QUNDRjtBREVBO0VBQ0ksa0JBQUE7QUNDSjtBRENBO0VBQ0Usc0JBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUNFRjtBRENBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0FDRUY7QURDQTtFQUVFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ0VGO0FEQUE7RUFDRSxtQkFBQTtFQUVBLGFBQUE7QUNFRjtBRENBO0VBRUUsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDRUY7QURBQTtFQUNFLG1CQUFBO0VBRUEsYUFBQTtBQ0VGO0FEQ0E7RUFDRTtJQUVFLG1CQUFBO0lBQ0EsWUFBQTtJQUNBLGVBQUE7SUFDQSxpQkFBQTtJQUNBLGVBQUE7SUFDQSxtQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxrQkFBQTtFQ0VGOztFREFBO0lBQ0UsbUJBQUE7SUFFQSxhQUFBO0VDRUY7O0VEQ0E7SUFFRSxtQkFBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0lBQ0EsaUJBQUE7SUFDQSxlQUFBO0lBQ0EsbUJBQUE7SUFDQSxpQkFBQTtJQUNBLGdCQUFBO0VDRUY7O0VEQ0E7SUFDRSxtQkFBQTtJQUVBLGFBQUE7RUNDRjtBQUNGO0FER0E7RUFDRSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0RGO0FER0U7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDRE4iLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0L3Byb2R1Y3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGItZnJhbWVyIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGhlaWdodDogMjVweDtcclxuICBjb2xvcjogI2U2N2UyMjtcclxuICAucHJvZ3Jlc3NiYXIge1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGJhY2tncm91bmQ6ICMzNDk4ZGI7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICB6LWluZGV4OiAtMTtcclxuICB9XHJcbiAgLnRlcm1pbmF0ZWQucHJvZ3Jlc3NiYXIge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gIH1cclxuICAuY2xyLXdoaXRlIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbn1cclxuXHJcbmgxIHtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbiNhcHByb3ZlIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjNGI1ODcyO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gIHBhZGRpbmc6IDAuMzc1cmVtIDAuNzVyZW07XHJcbiAgZm9udC1zaXplOiAxcmVtO1xyXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxufVxyXG4uZmEge1xyXG4gIG1hcmdpbi1yaWdodDogNXB4O1xyXG4gIC8vIC8vY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uZXJyb3JfbGFiZWwge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmN1c3RvbS1jb250ZW50Ojotd2Via2l0LWZpbGUtdXBsb2FkLWJ1dHRvbiB7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbn1cclxuLmN1c3RvbS1jb250ZW50OjpiZWZvcmUge1xyXG4gIGNvbnRlbnQ6IFwiQ2hvb3NlIEZpbGVcIjtcclxuICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgIzk5OTtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgcGFkZGluZzogOHB4O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGZvbnQtc2l6ZTogMTBwdDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xyXG59XHJcblxyXG5tYXJrIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGMzNTQ1O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLnJvdW5kZWQtYnRuIHtcclxuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gIHBhZGRpbmc6IDAgMTVweDtcclxuICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG59XHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgYmFja2dyb3VuZDogZGFya2VuKCM1NmE0ZmYsIDE1JSk7XHJcbiAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxuICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4ucm91bmRlZC1idG4tZGFuZ2VyIHtcclxuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gIHBhZGRpbmc6IDAgMTVweDtcclxuICBiYWNrZ3JvdW5kOiAjZGMzNTQ1O1xyXG59XHJcbi5yb3VuZGVkLWJ0bi1kYW5nZXI6aG92ZXIge1xyXG4gIGJhY2tncm91bmQ6IGRhcmtlbigjZGMzNTQ1LCAxNSUpO1xyXG4gIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcclxuICAucm91bmRlZC1idG4ge1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICAgcGFkZGluZzogMCAxMnB4O1xyXG4gICAgYmFja2dyb3VuZDogIzU2YTRmZjtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNSU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gIH1cclxuICAucm91bmRlZC1idG46aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZDogZGFya2VuKCM1NmE0ZmYsIDE1JSk7XHJcbiAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICB9XHJcblxyXG4gIC5yb3VuZGVkLWJ0bi1kYW5nZXIge1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICAgcGFkZGluZzogMCAxMnB4O1xyXG4gICAgYmFja2dyb3VuZDogI2RjMzU0NTtcclxuICAgIG1hcmdpbi1yaWdodDogMjUlO1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICB9XHJcblxyXG4gIC5yb3VuZGVkLWJ0bi1kYW5nZXI6aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZDogZGFya2VuKCNkYzM1NDUsIDE1JSk7XHJcbiAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICB9XHJcbn1cclxuXHJcblxyXG4uZXJyb3ItbWVzc2FnZSB7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgaGVpZ2h0OjEwMHZoO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LXNpemU6MjZweDtcclxuICBwYWRkaW5nLXRvcDogNDBweDtcclxuXHJcbiAgLnRleHQtbWVzc2FnZSB7XHJcbiAgICAgIGJhY2tncm91bmQ6ICNFNzRDM0M7XHJcbiAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgIG1hcmdpbjogMCAxMHB4O1xyXG4gIH1cclxufSIsIi5wYi1mcmFtZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiAyNXB4O1xuICBjb2xvcjogI2U2N2UyMjtcbn1cbi5wYi1mcmFtZXIgLnByb2dyZXNzYmFyIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYmFja2dyb3VuZDogIzM0OThkYjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDI1cHg7XG4gIHotaW5kZXg6IC0xO1xufVxuLnBiLWZyYW1lciAudGVybWluYXRlZC5wcm9ncmVzc2JhciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbn1cbi5wYi1mcmFtZXIgLmNsci13aGl0ZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuaDEge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbiNhcHByb3ZlIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzRiNTg3MjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgcGFkZGluZzogMC4zNzVyZW0gMC43NXJlbTtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBsaW5lLWhlaWdodDogMS41O1xuICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xufVxuXG4uZmEge1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmVycm9yX2xhYmVsIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uY3VzdG9tLWNvbnRlbnQ6Oi13ZWJraXQtZmlsZS11cGxvYWQtYnV0dG9uIHtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xufVxuXG4uY3VzdG9tLWNvbnRlbnQ6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiQ2hvb3NlIEZpbGVcIjtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYm9yZGVyOiAxcHggc29saWQgIzk5OTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiA4cHg7XG4gIG91dGxpbmU6IG5vbmU7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgZm9udC1zaXplOiAxMHB0O1xuICBjb2xvcjogd2hpdGU7XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbn1cblxubWFyayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkYzM1NDU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnJvdW5kZWQtYnRuIHtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDAgMTVweDtcbiAgYmFja2dyb3VuZDogIzU2YTRmZjtcbn1cblxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzBhN2JmZjtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuLnJvdW5kZWQtYnRuLWRhbmdlciB7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDE1cHg7XG4gIGJhY2tncm91bmQ6ICNkYzM1NDU7XG59XG5cbi5yb3VuZGVkLWJ0bi1kYW5nZXI6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjYTcxZDJhO1xuICBvdXRsaW5lOiBub25lO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xuICAucm91bmRlZC1idG4ge1xuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgcGFkZGluZzogMCAxMnB4O1xuICAgIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG4gICAgbWFyZ2luLWxlZnQ6IDI1JTtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgfVxuXG4gIC5yb3VuZGVkLWJ0bjpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogIzBhN2JmZjtcbiAgICBvdXRsaW5lOiBub25lO1xuICB9XG5cbiAgLnJvdW5kZWQtYnRuLWRhbmdlciB7XG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICBwYWRkaW5nOiAwIDEycHg7XG4gICAgYmFja2dyb3VuZDogI2RjMzU0NTtcbiAgICBtYXJnaW4tcmlnaHQ6IDI1JTtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICB9XG5cbiAgLnJvdW5kZWQtYnRuLWRhbmdlcjpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogI2E3MWQyYTtcbiAgICBvdXRsaW5lOiBub25lO1xuICB9XG59XG4uZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDQwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/product/product.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/layout/modules/product/product.component.ts ***!
  \*************************************************************/
/*! exports provided: ProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductComponent", function() { return ProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var ProductComponent = /** @class */ (function () {
    function ProductComponent(productService, router) {
        var _this_1 = this;
        this.productService = productService;
        this.router = router;
        // this section belongs to product item list
        this.Products = [];
        this.tableFormat = {
            title: 'Products Data',
            label_headers: [
                { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
                { label: 'Product Code', visible: true, type: 'string', data_row_name: 'product_code' },
                { label: 'Variation', visible: true, type: 'variation', data_row_name: 'variation' },
                { label: 'Type', visible: true, type: 'string', data_row_name: 'type' },
                { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_qty' },
                { label: 'Category', visible: true, type: 'string', data_row_name: 'category' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Weight (gr)', visible: true, type: 'string', data_row_name: 'weight' },
                { label: 'Dimensions', visible: true, type: 'dimensions', data_row_name: 'dimensions' },
                { label: 'Images Gallery', visible: true, type: 'images_gallery', data_row_name: 'images_gallery' },
                { label: 'Created Date', visible: true, type: 'string', data_row_name: 'created_date' },
                { label: 'Updated Date', visible: true, type: 'string', data_row_name: 'updated_date' },
            ],
            row_primary_key: 'product_code',
            formOptions: {
                row_id: 'product_code',
                addForm: true,
                addBulk: true,
                this: this,
                result_var_name: 'Products',
                detail_function: [this, 'callDetail'],
                customButtons: [
                    { label: 'Approve', func: function (f) { _this_1.approval(f); } },
                ]
            },
            show_checkbox_options: true
        };
        this.tableFormat2 = {
            title: 'Products Data',
            label_headers: [
                { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
                { label: 'Product Code', visible: true, type: 'string', data_row_name: 'product_code' },
                { label: 'Variation', visible: true, type: 'variation', data_row_name: 'variation' },
                { label: 'Type', visible: true, type: 'string', data_row_name: 'type' },
                { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_qty' },
                { label: 'Category', visible: true, type: 'string', data_row_name: 'category' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Weight (gr)', visible: true, type: 'string', data_row_name: 'weight' },
                { label: 'Dimensions', visible: true, type: 'dimensions', data_row_name: 'dimensions' },
                { label: 'Images Gallery', visible: true, type: 'images_gallery', data_row_name: 'images_gallery' },
                { label: 'Created Date', visible: true, type: 'string', data_row_name: 'created_date' },
                { label: 'Updated Date', visible: true, type: 'string', data_row_name: 'updated_date' },
            ],
            row_primary_key: 'product_code',
            formOptions: {
                row_id: 'product_code',
                addForm: true,
                addBulk: true,
                this: this,
                result_var_name: 'Products',
                detail_function: [this, 'callDetail'],
                customButtons: [
                    { label: 'Approve', func: function (f) { _this_1.approval(f); } },
                ]
            },
            show_checkbox_options: true
        };
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__;
        this.form_input = {};
        this.errorLabel = false;
        this.errorMessage = false;
        this.prodDetail = false;
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
        this.totalPage = 0;
        this.page = 1;
        this.pageSize = 50;
        this.mci_project = false;
    }
    ProductComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ProductComponent.prototype.approval = function (listedData) {
        return __awaiter(this, void 0, void 0, function () {
            var listOfData, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // console.log("masuk coy")
                        if (listedData == undefined || listedData.length == 0) {
                            return [2 /*return*/, false];
                        }
                        listOfData = [];
                        //
                        listedData.forEach(function (element, index) {
                            listOfData.push(element._id);
                        });
                        listedData = [];
                        if (listOfData.length == 0)
                            return [2 /*return*/, false];
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.approvedProducts({ product_ids: listOfData })];
                    case 1:
                        result = _a.sent();
                        if (result.result.status == 'success') {
                            alert("the Current Data Below has been approved");
                            this.firstLoad();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program_1, _this_2, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        program_1 = localStorage.getItem('programName');
                        _this_2 = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program_1) {
                                if (element.type == "reguler") {
                                    _this_2.mci_project = true;
                                }
                                else {
                                    _this_2.mci_project = false;
                                }
                            }
                        });
                        result = void 0;
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.getProductsReport()];
                    case 1:
                        result = _a.sent();
                        this.totalPage = result.total_page;
                        this.Products = result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductComponent.prototype.callDetail = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.router.navigate(['administrator/productadmin/edit'], { queryParams: { product_code: _id } });
                return [2 /*return*/];
            });
        });
    };
    ProductComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.prodDetail = false;
                return [2 /*return*/];
            });
        });
    };
    ProductComponent.prototype.onFileSelected = function (event) {
        var _this_1 = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this_1.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
    };
    ProductComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    ProductComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    ProductComponent.prototype.callAfterUpload = function (result) {
        console.log("THE RESULT", result);
    };
    ProductComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productService.uploadFile(this.selectedFile, this, 'product', this.callAfterUpload)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        alert("Upload Success");
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4:
                        if (this.selectedFile) { }
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    ProductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product',
            template: __webpack_require__(/*! raw-loader!./product.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product/product.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./product.component.scss */ "./src/app/layout/modules/product/product.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ProductComponent);
    return ProductComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product/product.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/layout/modules/product/product.module.ts ***!
  \**********************************************************/
/*! exports provided: ProductModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductModule", function() { return ProductModule; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _product_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-routing.module */ "./src/app/layout/modules/product/product-routing.module.ts");
/* harmony import */ var _product_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./product.component */ "./src/app/layout/modules/product/product.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _add_product_add_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./add/product.add.component */ "./src/app/layout/modules/product/add/product.add.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ckeditor/ckeditor5-angular */ "./node_modules/@ckeditor/ckeditor5-angular/fesm5/ckeditor-ckeditor5-angular.js");
/* harmony import */ var _detail_product_detail_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./detail/product.detail.component */ "./src/app/layout/modules/product/detail/product.detail.component.ts");
/* harmony import */ var _edit_product_edit_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./edit/product.edit.component */ "./src/app/layout/modules/product/edit/product.edit.component.ts");
/* harmony import */ var _add_bulk_product_add_bulk_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./add-bulk/product.add-bulk.component */ "./src/app/layout/modules/product/add-bulk/product.add-bulk.component.ts");
/* harmony import */ var _merchant_portal_separated_modules_merchant_products_products_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../merchant-portal/separated-modules/merchant-products/products.module */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










// import { NgxEditorModule } from 'ngx-editor';





var ProductModule = /** @class */ (function () {
    function ProductModule() {
    }
    ProductModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _product_routing_module__WEBPACK_IMPORTED_MODULE_3__["ProductRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_5__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                // NgxEditorModule,
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["NgbModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClientModule"],
                _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_10__["CKEditorModule"],
                _merchant_portal_separated_modules_merchant_products_products_module__WEBPACK_IMPORTED_MODULE_14__["ProductsModule"]
            ],
            declarations: [_product_component__WEBPACK_IMPORTED_MODULE_4__["ProductComponent"], _add_product_add_component__WEBPACK_IMPORTED_MODULE_8__["ProductAddComponent"],
                _detail_product_detail_component__WEBPACK_IMPORTED_MODULE_11__["ProductDetailComponent"],
                _edit_product_edit_component__WEBPACK_IMPORTED_MODULE_12__["ProductEditComponent"],
                _add_bulk_product_add_bulk_component__WEBPACK_IMPORTED_MODULE_13__["ProductAddBulkComponent"]
            ]
        })
    ], ProductModule);
    return ProductModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-product-product-module.js.map