(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["merchant-portal-merchant-portal-module"],{

/***/ "./node_modules/ng-otp-input/fesm5/ng-otp-input.js":
/*!*********************************************************!*\
  !*** ./node_modules/ng-otp-input/fesm5/ng-otp-input.js ***!
  \*********************************************************/
/*! exports provided: NgOtpInputModule, ɵa, ɵb, ɵc */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgOtpInputModule", function() { return NgOtpInputModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return NgOtpInputComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return KeysPipe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵc", function() { return NumberOnlyDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");




/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var KeysPipe = /** @class */ (function () {
    function KeysPipe() {
    }
    /**
     * @param {?} value
     * @return {?}
     */
    KeysPipe.prototype.transform = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return Object.keys(value);
    };
    KeysPipe.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"], args: [{
                    name: 'keys'
                },] }
    ];
    return KeysPipe;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var Config = /** @class */ (function () {
    function Config() {
    }
    return Config;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NgOtpInputComponent = /** @class */ (function () {
    function NgOtpInputComponent(keysPipe) {
        this.keysPipe = keysPipe;
        this.config = { length: 4 };
        // tslint:disable-next-line: no-output-on-prefix
        this.onInputChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.inputControls = new Array(this.config.length);
        this.componentKey = Math.random()
            .toString(36)
            .substring(2) + new Date().getTime().toString(36);
    }
    /**
     * @return {?}
     */
    NgOtpInputComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.otpForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({});
        for (var index = 0; index < this.config.length; index++) {
            this.otpForm.addControl(this.getControlName(index), new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]());
        }
        this.inputType = this.getInputType();
    };
    /**
     * @return {?}
     */
    NgOtpInputComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.config.disableAutoFocus) {
            /** @type {?} */
            var containerItem = document.getElementById("c_" + this.componentKey);
            if (containerItem) {
                containerItem.addEventListener('paste', (/**
                 * @param {?} evt
                 * @return {?}
                 */
                function (evt) { return _this.handlePaste(evt); }));
                /** @type {?} */
                var ele = containerItem.getElementsByClassName('otp-input')[0];
                if (ele && ele.focus) {
                    ele.focus();
                }
            }
        }
    };
    /**
     * @private
     * @param {?} idx
     * @return {?}
     */
    NgOtpInputComponent.prototype.getControlName = /**
     * @private
     * @param {?} idx
     * @return {?}
     */
    function (idx) {
        return "ctrl_" + idx;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    NgOtpInputComponent.prototype.ifLeftArrow = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return this.ifKeyCode(event, 37);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    NgOtpInputComponent.prototype.ifRightArrow = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return this.ifKeyCode(event, 39);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    NgOtpInputComponent.prototype.ifBackspaceOrDelete = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return (event.key === 'Backspace' ||
            event.key === 'Delete' ||
            this.ifKeyCode(event, 8) ||
            this.ifKeyCode(event, 46));
    };
    /**
     * @param {?} event
     * @param {?} targetCode
     * @return {?}
     */
    NgOtpInputComponent.prototype.ifKeyCode = /**
     * @param {?} event
     * @param {?} targetCode
     * @return {?}
     */
    function (event, targetCode) {
        /** @type {?} */
        var key = event.keyCode || event.charCode;
        // tslint:disable-next-line: triple-equals
        return key == targetCode ? true : false;
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    NgOtpInputComponent.prototype.onKeyDown = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        /** @type {?} */
        var isSpace = this.ifKeyCode($event, 32);
        if (isSpace) { // prevent space
            return false;
        }
    };
    /**
     * @param {?} $event
     * @param {?} inputIdx
     * @return {?}
     */
    NgOtpInputComponent.prototype.onKeyUp = /**
     * @param {?} $event
     * @param {?} inputIdx
     * @return {?}
     */
    function ($event, inputIdx) {
        /** @type {?} */
        var nextInputId = this.appendKey("otp_" + (inputIdx + 1));
        /** @type {?} */
        var prevInputId = this.appendKey("otp_" + (inputIdx - 1));
        if (this.ifRightArrow($event)) {
            this.setSelected(nextInputId);
            return;
        }
        if (this.ifLeftArrow($event)) {
            this.setSelected(prevInputId);
            return;
        }
        /** @type {?} */
        var isBackspace = this.ifBackspaceOrDelete($event);
        if (isBackspace && !$event.target.value) {
            this.setSelected(prevInputId);
            this.rebuildValue();
            return;
        }
        if (!$event.target.value) {
            return;
        }
        if (this.ifValidEntry($event)) {
            this.setSelected(nextInputId);
        }
        this.rebuildValue();
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NgOtpInputComponent.prototype.appendKey = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return id + "_" + this.componentKey;
    };
    /**
     * @param {?} eleId
     * @return {?}
     */
    NgOtpInputComponent.prototype.setSelected = /**
     * @param {?} eleId
     * @return {?}
     */
    function (eleId) {
        this.focusTo(eleId);
        /** @type {?} */
        var ele = document.getElementById(eleId);
        if (ele && ele.setSelectionRange) {
            setTimeout((/**
             * @return {?}
             */
            function () {
                ele.setSelectionRange(0, 1);
            }), 0);
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    NgOtpInputComponent.prototype.ifValidEntry = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var inp = String.fromCharCode(event.keyCode);
        /** @type {?} */
        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        return (isMobile ||
            /[a-zA-Z0-9-_]/.test(inp) ||
            (this.config.allowKeyCodes &&
                this.config.allowKeyCodes.includes(event.keyCode)) ||
            (event.keyCode >= 96 && event.keyCode <= 105));
    };
    /**
     * @param {?} eleId
     * @return {?}
     */
    NgOtpInputComponent.prototype.focusTo = /**
     * @param {?} eleId
     * @return {?}
     */
    function (eleId) {
        /** @type {?} */
        var ele = document.getElementById(eleId);
        if (ele) {
            ele.focus();
        }
    };
    // method to set component value
    // method to set component value
    /**
     * @param {?} value
     * @return {?}
     */
    NgOtpInputComponent.prototype.setValue = 
    // method to set component value
    /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        var _this = this;
        if (this.config.allowNumbersOnly && isNaN(value)) {
            return;
        }
        this.otpForm.reset();
        if (!value) {
            this.rebuildValue();
            return;
        }
        value = value.toString().replace(/\s/g, ''); // remove whitespace
        Array.from(value).forEach((/**
         * @param {?} c
         * @param {?} idx
         * @return {?}
         */
        function (c, idx) {
            if (_this.otpForm.get(_this.getControlName(idx))) {
                _this.otpForm.get(_this.getControlName(idx)).setValue(c);
            }
        }));
        if (!this.config.disableAutoFocus) {
            /** @type {?} */
            var containerItem = document.getElementById("c_" + this.componentKey);
            /** @type {?} */
            var indexOfElementToFocus = value.length < this.config.length ? value.length : (this.config.length - 1);
            /** @type {?} */
            var ele = containerItem.getElementsByClassName('otp-input')[indexOfElementToFocus];
            if (ele && ele.focus) {
                ele.focus();
            }
        }
        this.rebuildValue();
    };
    /**
     * @return {?}
     */
    NgOtpInputComponent.prototype.rebuildValue = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var val = '';
        this.keysPipe.transform(this.otpForm.controls).forEach((/**
         * @param {?} k
         * @return {?}
         */
        function (k) {
            if (_this.otpForm.controls[k].value) {
                val += _this.otpForm.controls[k].value;
            }
        }));
        this.onInputChange.emit(val);
    };
    /**
     * @return {?}
     */
    NgOtpInputComponent.prototype.getInputType = /**
     * @return {?}
     */
    function () {
        return this.config.isPasswordInput
            ? 'password'
            : this.config.allowNumbersOnly
                ? 'tel'
                : 'text';
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgOtpInputComponent.prototype.handlePaste = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        // Get pasted data via clipboard API
        /** @type {?} */
        var clipboardData = e.clipboardData || window['clipboardData'];
        if (clipboardData) {
            /** @type {?} */
            var pastedData = clipboardData.getData('Text');
        }
        // Stop data actually being pasted into div
        e.stopPropagation();
        e.preventDefault();
        if (!pastedData) {
            return;
        }
        this.setValue(pastedData);
    };
    NgOtpInputComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'ng-otp-input',
                    template: "<div class=\"wrapper {{config.containerClass}}\" id=\"c_{{componentKey}}\" *ngIf=\"otpForm?.controls\"\r\n  [ngStyle]=\"config.containerStyles\">\r\n  <input [pattern]=\"config.allowNumbersOnly ? '\\\\d*' : ''\" [type]=\"inputType\" numberOnly [placeholder]=\"config?.placeholder || ''\"\r\n    [disabledNumberOnly]=\"!config.allowNumbersOnly\" [ngStyle]=\"config.inputStyles\" maxlength=\"1\"\r\n    class=\"otp-input {{config.inputClass}}\" autocomplete=\"off\" *ngFor=\"let item of otpForm?.controls | keys;let i=index\"\r\n    [formControl]=\"otpForm.controls[item]\" id=\"otp_{{i}}_{{componentKey}}\" (keydown)=\"onKeyDown($event)\"\r\n    (keyup)=\"onKeyUp($event,i)\">\r\n</div>",
                    styles: [".otp-input{width:50px;height:50px;border-radius:4px;border:1px solid #c5c5c5;text-align:center;font-size:32px}.wrapper .otp-input:not(:last-child){margin-right:8px}@media screen and (max-width:767px){.otp-input{width:40px;font-size:24px;height:40px}}@media screen and (max-width:420px){.otp-input{width:30px;font-size:18px;height:30px}}"]
                }] }
    ];
    /** @nocollapse */
    NgOtpInputComponent.ctorParameters = function () { return [
        { type: KeysPipe }
    ]; };
    NgOtpInputComponent.propDecorators = {
        config: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        onInputChange: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }]
    };
    return NgOtpInputComponent;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NumberOnlyDirective = /** @class */ (function () {
    function NumberOnlyDirective(_elRef, _renderer) {
        this._elRef = _elRef;
        this._renderer = _renderer;
    }
    /**
     * @return {?}
     */
    NumberOnlyDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (!this.disabledNumberOnly) {
            this._renderer.setAttribute(this._elRef.nativeElement, 'onkeypress', 'return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 0');
        }
    };
    NumberOnlyDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[numberOnly]'
                },] }
    ];
    /** @nocollapse */
    NumberOnlyDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"] }
    ]; };
    NumberOnlyDirective.propDecorators = {
        disabledNumberOnly: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }]
    };
    return NumberOnlyDirective;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NgOtpInputModule = /** @class */ (function () {
    function NgOtpInputModule() {
    }
    NgOtpInputModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    imports: [
                        _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"]
                    ],
                    declarations: [NgOtpInputComponent, KeysPipe, NumberOnlyDirective],
                    exports: [NgOtpInputComponent],
                    providers: [KeysPipe]
                },] }
    ];
    return NgOtpInputModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */


//# sourceMappingURL=ng-otp-input.js.map


/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/component-libs/photo-profile/photo-profile.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/component-libs/photo-profile/photo-profile.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"photo-profile\">\r\n    <div *ngIf=\"bgUrl\" class=\"img\" [ngStyle]=\"{'background-image': 'url('+bgUrl+')'}\"></div>\r\n    <div *ngIf=\"!bgUrl\" class=\"no-img\" >no photo</div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-homepage/merchant-homepage.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-homepage/merchant-homepage.component.html ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row _container\">\r\n    <div class=\"col-md-12\">\r\n        <div class=\"row cl-row-list\">\r\n            <div class=\"col-md-3 col-md-6 col-lg-3\">\r\n                <div class=\"card cl-list\">\r\n                    <div class=\"card-header profile\">\r\n                        <span class=\"card-header-title\">Profile</span>\r\n                        <hr />\r\n                        <div class=\"card-header-body\">\r\n                            <img src=\"assets/images/Pelanggan_aktif.svg\">\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card-body with-upgrade\">\r\n                        <div class=\"center\">Get and Update your profile here</div>\r\n                        <div class=\"button-container\">\r\n                            <button routerLink=\"/merchant-portal/profile\" class=\"btn btn-2 btn-primary\">\r\n                                Update My Profile\r\n                            </button>\r\n                            <button routerLink=\"/merchant-portal/homepage/upgrade-genuine\" class=\"btn-1 btn upgrade\">\r\n                                <!-- <img src=\"assets/images/best-seller.png\"> -->\r\n                                <span>Upgrade Merchant</span>\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3 col-md-6 col-lg-3\">\r\n                <div class=\"card cl-list\">\r\n                    <div class=\"card-header sales-order\">\r\n                        <span class=\"card-header-title\">Sales Order</span>\r\n                        <hr />\r\n                        <div class=\"card-header-body\">\r\n                            <img src=\"assets/images/Group_5917.svg\">\r\n                            <!-- <div>\r\n                                <span class=\"title-sub\">Total</span>\r\n                                <br>\r\n                                <span class=\"body-sub\" *ngIf=\"!myProducts || myOrderHistory.total_all_values <=0 \">0</span>\r\n                                <span class=\"body-sub\"\r\n                                    *ngIf=\"myProducts && myOrderHistory.total_all_values >0 \">{{myOrderHistory.total_all_values}}</span>\r\n                            </div> -->\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <div class=\"center\">You can get detail statistics of your products and orders here</div>\r\n                        <div class=\"button-container\">\r\n                        <button routerLink=\"/merchant-portal/sales-order\" class=\"btn btn-1 btn-primary\">\r\n                            &nbsp; See My Sales Order\r\n                        </button>\r\n                    </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3 col-md-6 col-lg-3\">\r\n                <div class=\"card cl-list\">\r\n                    <div class=\"card-header product\">\r\n                        <span class=\"card-header-title\">Products</span>\r\n                        <hr />\r\n                        <div class=\"card-header-body\">\r\n                            <img src=\"assets/images/tshirt.svg\">\r\n                            <div>\r\n                                <span class=\"title-sub\">Total</span>\r\n                                <br>\r\n                                <span class=\"body-sub\" *ngIf=\"!myProducts || myProducts.total_all_values <=0 \">0</span>\r\n                                <span class=\"body-sub\"\r\n                                    *ngIf=\"myProducts && myProducts.total_all_values >0 \">{{myProducts.total_all_values}}</span>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <div *ngIf=\"!myProducts || myProducts.total_all_values <=0 \" class=\"center\">You have no single\r\n                            Product yet. Lets upload new product!.\r\n                        </div>\r\n                        <div *ngIf=\"myProducts && myProducts.total_all_values>0\" class=\"center\">See uploaded products\r\n                            here.\r\n                        </div>\r\n                        <div class=\"button-container\">\r\n                        <button *ngIf=\"!myProducts || myProducts.total_all_values <=0 \" routerLink=\"/merchant-portal/add\"\r\n                            class=\"btn btn-1 btn-primary\"> Upload New Products\r\n                        </button>\r\n                        <button *ngIf=\"myProducts && myProducts.total_all_values>0\" routerLink=\"/merchant-portal/product-list\"\r\n                            class=\"btn btn-1 btn-primary\"> My Products\r\n                        </button>\r\n                    </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3 col-md-6 col-lg-3\">\r\n                <div class=\"card cl-list\">\r\n                    <div class=\"card-header order-history\">\r\n                        <span class=\"card-header-title\">Order History</span>\r\n                        <hr />\r\n                        <div class=\"card-header-body\">\r\n                            <img src=\"assets/images/Path_2304.svg\">\r\n                            <!-- <div>\r\n                                <span class=\"title-sub\">Total</span>\r\n                                <br>\r\n                                <span class=\"body-sub\" *ngIf=\"!myProducts || myOrderHistory.total_all_values <=0 \">0</span>\r\n                                <span class=\"body-sub\"\r\n                                    *ngIf=\"myProducts && myOrderHistory.total_all_values >0 \">{{myOrderHistory.total_all_values}}</span>\r\n                            </div> -->\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <div class=\"center\">You have no to process Order yet</div>\r\n                        <div class=\"button-container\">\r\n                        <button routerLink=\"/merchant-portal/order-histories\" class=\"btn btn-1 btn-primary\">\r\n                            See My Order History\r\n                        </button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <!-- <div class=\"col-md-3\">\r\n                <div class=\"card cl-list\">\r\n                    <div class=\"card-header\">\r\n                      <em class=\"fa store fa-store\"></em> Store Front\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                      <div class=\"left\">Check your Storefront looks like</div>\r\n                      <button routerLink=\"/merchant-portal/add\" class=\"btn btn-primary\"><span class=\"fa fa-eye\"></span> &nbsp; watch</button>\r\n                    </div>\r\n                </div>\r\n            </div> -->\r\n            <!-- <div class=\"col-md-3 col-md-6 col-lg-3\">\r\n                <div class=\"card cl-list\">\r\n                    <div class=\"card-header statistics\">\r\n                        <span class=\"card-header-title\">Statistics</span>\r\n                        <hr />\r\n                        <div class=\"card-header-body\">\r\n                            <img src=\"assets/images/statistics.svg\">\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <div class=\"left\">You can get detail statistics of your products and orders here</div>\r\n                        <div class=\"button-container\">\r\n                        <button routerLink=\"/merchant-portal/statistics\" class=\"btn btn-1 btn-primary\">\r\n                            See More Detail\r\n                        </button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div> -->\r\n\r\n        </div>\r\n\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component.html":
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component.html ***!
  \**************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"data\" class=\"card card-detail\">\r\n    <div class=\"card-header\" id=\"header\">\r\n        <button class=\"btn back_button\" (click)=\"back()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <!-- <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i>\r\n            <span *ngIf=\"!loading\">Save</span>\r\n            <span *ngIf=\"loading\">Please Wait</span>\r\n        </button> -->\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"orderhistoryallhistory-detail\">\r\n            <div class=\"form-group head-one\">\r\n                <label class=\"order-id\"><strong>TEST</strong> |\r\n                    <span class=\"buyer-name\" style=\"color:#0EA8DB\"><strong>\r\n                            LO Official</strong></span></label>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"form-group col-md-6\" style=\"font-size: 20px;\">\r\n                    <label><span style=\"color: #0EA8DB;font-weight: bold;\">Request Date : </span>\r\n                        <strong><span>{{data.created_date | date:'dd MMM yyyy'}}</span><span> |\r\n                            </span>{{data.created_date | date: 'hh:mm:ss a'}}</strong></label>\r\n                </div>\r\n\r\n                <!-- <div class=\"form-group col-md-6\">\r\n                                        <label>User ID : {{data._id}} </label>\r\n                                    </div>  -->\r\n            </div>\r\n            <div class=\"row\">\r\n                <!-- <div *ngIf=\"errorLabel!==false\">\r\n                    {{errorLabel}}\r\n                </div> -->\r\n\r\n                <div class=\" col-md-8\">\r\n                    <div class=\"card mb-3\">\r\n\r\n\r\n                        <div class=\"card-content\">\r\n                            <div class=\"form-group\"><img style=\"width: 40px;\"\r\n                                    src=\"/assets/images/locard_icon_circle.png\" alt=\"Locard Icon\">\r\n                                <span class=\"buyer-name\" style=\"color:#0EA8DB\"><strong>\r\n                                        LO Official</strong></span> </div>\r\n                            <div class=\"col-md-12\">\r\n\r\n\r\n                                <div class=\"product-group\">\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-5\">\r\n                                            <div class=\"card mb-2\">\r\n                                                <div class=\"money-container\">\r\n                                                    <div class=\"icon-container\">\r\n                                                        <i style=\"color : #3E8CF3;\r\n                                                background-color: #DAF0FE;\" class=\"icon-my-money-merchant\"></i>\r\n                                                    </div>\r\n                                                    <div class=\"number-container\">\r\n                                                        <span>Current Balance</span>\r\n                                                        <br>\r\n                                                        <label\r\n                                                            style=\"color : #3E8CF3\">{{data.prev_balance | currency: 'Rp ' }}</label>\r\n                                                    </div>\r\n                                                </div>\r\n                                                <div class=\"money-container buyer-detail\">\r\n                                                    <div class=\"icon-container\">\r\n                                                        <i style=\"color : #CB97A3;\r\n                                                    background-color: #FCECEF;\" class=\"icon-my-money-merchant\"></i>\r\n                                                    </div>\r\n                                                    <div class=\"number-container\">\r\n                                                        <span>Withdrawal Request</span>\r\n                                                        <br>\r\n                                                        <label\r\n                                                            style=\"color : #D9446B\">{{data.how_much * -1 | currency: 'Rp '}}</label>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-md-7\">\r\n                                            <div class=\"card mb-2\" style=\"height: 96.5%;\">\r\n                                                <div class=\"card-content\">\r\n                                                    <strong>Description :</strong>\r\n                                                    <p>{{data.description}}</p>\r\n                                                </div>\r\n                                            </div>\r\n\r\n                                        </div>\r\n\r\n\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n\r\n                                <div class=\"buyer-detail\">\r\n                                    <div class=\"form-group\">\r\n                                        <div class=\"spacing\">\r\n                                            <label id=\"buyer-detail\">Invoice Detail</label>\r\n                                            <table>\r\n                                                <tr>\r\n                                                    <th>Notes</th>\r\n                                                    <th> </th>\r\n                                                </tr>\r\n                                                <tr>\r\n                                                    <td>Penarikan Dana Tunai</td>\r\n                                                    <td>{{data.how_much * -1 | currency: 'Rp '}}</td>\r\n                                                </tr>\r\n                                                <tr>\r\n                                                    <!-- <td>Potongan MDR 30%</td>\r\n                                                    <td>{{data.how_much*30/100 | currency: 'Rp '}}</td> -->\r\n                                                </tr>\r\n                                                <tr>\r\n                                                    <td>Biaya Transfer Bank</td>\r\n                                                    <td><span>Rp .</span></td>\r\n                                                </tr>\r\n                                                <tr>\r\n                                                    <th>Total Penarikan</th>\r\n                                                    <th><span>Rp. </span> </th>\r\n                                                </tr>\r\n\r\n                                                <tr>\r\n\r\n\r\n                                                </tr>\r\n                                            </table>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\" col-md-4\">\r\n                    <div class=\"card mb-2\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Status</h2>\r\n                            <!-- <button *ngIf=\"!data.withdrawal_status == 'PENDING' && change\" class=\"save\" (click)=\"saveThis()\">Save Change</button> -->\r\n                            <!-- <button *ngIf=\"orderHistoryDetail.status == 'WAITING FOR CONFIRMATION' && change\" class=\"save\" (click)=\"saveThis()\">Save Change</button> -->\r\n                            <!-- <button *ngIf=\"data.withdrawal_status == 'PENDING' && !change\" class=\"save change\"\r\n                                (click)=\"change = true\">Change Status</button> -->\r\n\r\n                        </div>\r\n                        <div class=\"card-content\" *ngIf=\"data.withdrawal_status\">\r\n                            <label>Status :\r\n                                <strong><span style=\"color:#0EA8DB\">{{data.withdrawal_status}}\r\n                                    </span></strong></label>\r\n                            <br>\r\n                            <div *ngIf=\"change\">\r\n                                <label>Status</label>\r\n                                <div *ngIf=\"data.withdrawal_status == 'PENDING' && change\" name=\"transaction_status\"\r\n                                    class=\"status-button-container\">\r\n\r\n                                    <button class=\"common-button option paid\" (click)=\"onpaidclick()\" target=\"_blank\">\r\n                                        <span *ngIf=\"isClicked\"><i class=\"material-icons\">\r\n                                                done\r\n                                            </i></span>Release</button>\r\n                                    <button class=\"common-button option cancel\" (click)=\"oncancelclick()\"\r\n                                        target=\"_blank\"><span *ngIf=\"isClicked2\"><i class=\"material-icons\">\r\n                                                done\r\n                                            </i></span> Reject</button>\r\n                                </div>\r\n                            </div>\r\n                            <div *ngIf=\"merchantMode\" name=\"transaction_status\">\r\n                                <div class=\"\">\r\n                                    status lagi\r\n                                </div>\r\n                            </div>\r\n               \r\n                        </div>\r\n\r\n                        <!-- <div *ngIf=\"orderHistoryDetail.status == 'WAITING FOR CONFIRMATION'\">\r\n                        <span style=\"text-align: center;\">Payer Detail</span>\r\n                        <div class=\"card-content payer\">\r\n                            <table align=\"center\">\r\n                                <th>Payer Name</th>\r\n                                <th>Account Number</th>\r\n                                <th>Payer Note</th>\r\n                                <tr>\r\n                                    <td>{{orderHistoryDetail.payer_detail.payer_name}}</td>\r\n                                    <td>{{orderHistoryDetail.payer_detail.payer_acc_number}}</td>\r\n                                    <td>{{orderHistoryDetail.payer_detail.payment_notes}}</td>\r\n                                </tr>\r\n\r\n\r\n                            </table>\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    </div>\r\n                </div>\r\n\r\n                <!-- <div class=\"card mb-3\">\r\n                     \r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Email </label>\r\n                            <form-input name=\"email\" [type]=\"'text'\" [placeholder]=\"'Email'\"  [(ngModel)]=\"orderHistoryDetail.email\" autofocus required></form-input>\r\n\r\n                            <label>Cell Phone </label>\r\n                            <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Cell Phone'\"  [(ngModel)]=\"orderHistoryDetail.cell_phone\" autofocus required></form-input>\r\n                            \r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>  -->\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-my-escrow/merchant-my-escrow.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-my-escrow/merchant-my-escrow.component.html ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"_container\">\r\n    <div class=\"col-md-12\">\r\n        <div class=\"card\" id=\"card-transaction\">\r\n            <div class=\"div-transaction\" *ngIf=\"myBalance\">\r\n                <div id=\"transaction\">\r\n                    <img src=\"assets/images/withdrawal.png\" class=\"logo\">\r\n                    <label>Current Balance : <br /></label>\r\n                    <span><strong>{{myBalance | currency: 'Rp'}}</strong></span>\r\n                    <button class=\"btn btn-outline-primary withdraw\" (click)=\"checkAccount()\"\r\n                        [routerLinkActive]=\"['router-link-active']\">Withdraw</button>\r\n                </div>\r\n\r\n                <!-- <div class=\"searchbar\">\r\n                    <div id=\"search\">\r\n                        <span style=\"padding-right:10px;\">Search</span>\r\n                        <input type=\"text\" style=\"height: 50%;\" class=\"form-control\"\r\n                            placeholder=\"Search\">\r\n                    </div>\r\n                    <div id=\"search\">\r\n                        <span style=\"padding-right: 10px;\">Type</span>\r\n                        <select class=\"form-control\" [(ngModel)]=\"type\">\r\n                            <option *ngFor=\"let t of types\" [ngValue]=\"t.value\">{{t.name}}</option>\r\n                        </select>\r\n                    </div>\r\n                    <div id=\"search\">\r\n                        <span style=\"padding-right:10px;\">Status</span>\r\n                        <select class=\"form-control\" [(ngModel)]=\"status\">\r\n                            <option *ngFor=\"let s of status\" [ngValue]=\"s.value\">{{s.name}}</option>\r\n                        </select>\r\n                    </div>\r\n\r\n\r\n                    <button class=\"common-button blue-fill download\">Download</button>\r\n\r\n\r\n                </div> -->\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <br />\r\n    <div class=\"money-table\">\r\n        <app-form-builder-table [searchCallback]=\"[service, 'searchEscrowtransactionLint',this]\"\r\n        [tableFormat]=\"tableFormat\" [table_data]=\"EscrowData\" [total_page]=\"totalPage\">\r\n\r\n      </app-form-builder-table>\r\n        <!-- <table>\r\n            <tr>\r\n                <th>No</th>\r\n                <th>Dates</th>\r\n                <th>Descriptions</th>\r\n                <th>Debit</th>\r\n                <th>Credit</th>\r\n                <th>Balance</th>\r\n                <th>Type</th>\r\n                <th>Status</th>\r\n            </tr>\r\n            <tr *ngFor=\"let data of EscrowData; let i = index\" (click)=\"callDetail(data._id);\" style=\"cursor:pointer;\">\r\n                <td>{{number[i]}}</td>\r\n                <td>{{data.created_date}}</td>\r\n                <td>{{data.description}}</td>\r\n                <td><strong *ngIf=\"data.how_much > 0\">{{data.how_much| currency: 'Rp '}}</strong>\r\n                    <strong *ngIf=\"data.how_much <= 0\">-</strong>\r\n                </td>\r\n                <td><strong *ngIf=\"data.how_much < 0\" style=\"color:red;\">-\r\n                        ({{(data.how_much * -1)| currency: 'Rp '}})</strong>\r\n                    <strong *ngIf=\"data.how_much >= 0\">-</strong>\r\n                </td>\r\n\r\n\r\n\r\n                \r\n                <td>{{data.current_balance | currency : 'Rp '}}</td>\r\n                <td *ngIf=\"data.type == '+'\">Debit</td>\r\n                <td *ngIf=\"data.type == '-'\">Credit</td>\r\n                <td *ngIf=\"data.type == 'withdrawal'\">{{data.type}}</td>\r\n                <td *ngIf=\"data.type == 'hold'\">{{data.type}}</td>\r\n        \r\n                <td *ngIf=\"data.transaction_status == 'PENDING'\">\r\n                    <button class=\"pending\">{{data.transaction_status}}</button>\r\n                </td>\r\n                <td *ngIf=\"data.transaction_status == 'SUCCESS'\">\r\n                    <button class=\"success\">{{data.transaction_status}}</button>\r\n                </td>\r\n                <td *ngIf=\"data.transaction_status == 'HOLD'\">\r\n                    <button class=\"hold\">{{data.transaction_status}}</button>\r\n                </td>\r\n                <td *ngIf=\"data.transaction_status == 'REFUND'\">\r\n                    <button class=\"refunded\"> {{data.transaction_status}}</button>\r\n                </td>\r\n                <td *ngIf=\"data.transaction_status == 'REJECT'\">\r\n                    <button class=\"rejected\"> {{data.transaction_status}}</button>\r\n                </td>\r\n                <td *ngIf=\"data.transaction_status == '-' \"> {{data.transaction_status}} </td>\r\n            </tr>\r\n        </table> -->\r\n\r\n    </div>\r\n    <!-- <div class=\"div-footer\" *ngIf=\"EscrowData && EscrowData.length!=0\" style=\"margin-top:10px;float:right;\">\r\n        <div class=\"pagination\">\r\n            <button class=\"btn\" (click)=\"onChangePage(currentPage = 1)\">\r\n                &laquo; </button> <button class=\"btn\" (click)=\"onChangePage(currentPage - 1)\">\r\n                &lsaquo; </button> <button *ngFor=\"let tp of pageNumbering\" class=\"btn\"\r\n                (click)=\"onChangePage(tp)\">{{tp}}\r\n            </button>\r\n            <button class=\"btn\" (click)=\"onChangePage(currentPage + 1)\">&rsaquo;</button>\r\n            <button class=\"btn\" (click)=\"onChangePage(currentPage = total_page)\">&raquo;</button>\r\n        </div>\r\n    </div> -->\r\n</div>\r\n\r\n\r\n<div id=\"myModal\" class=\"modal {{setPin}}\">\r\n    <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n            <span class=\"close\" (click)=\"closeDialog()\">&times;</span>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n            <h3>Register Pin</h3>\r\n            <span><strong> Input 6-Digit number. Remember, don't use predictable combinations of numbers\r\n                    like date of birth, etc</strong></span>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n            <div class=\"form-group\">\r\n\r\n                <!-- <input placeholder=\"Password\" (click)=\"errorMessage = false\" [type]=\"hide ? 'password' : 'text'\"\r\n                [(ngModel)]=\"form.password\" name=\"password\">\r\n            <button mat-icon-button matSuffix (click)=\"hide = !hide\" [attr.aria-label]=\"'Hide password'\"\r\n                [attr.aria-pressed]=\"hide\" style=\"background:transparent; border: none;\">\r\n                <mat-icon >{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                </button> -->\r\n\r\n                <div class=\"wrapper-input\">\r\n                    <label>Set Pin</label>\r\n                    <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.new_pin\" size=\"6\" maxlength=\"6\"\r\n                        pattern=\"[0-9]\" [type]=\"hide ? 'password': 'text'\" inputmode=\"numeric\">\r\n                    <label>Retype New Pin</label>\r\n                    <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.repeat_new_pin\" size=\"6\" maxlength=\"6\"\r\n                        pattern=\"[0-9]\" [type]=\"hide ? 'password': 'text'\" inputmode=\"numeric\">\r\n                    <label>Password</label>\r\n                    <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.password\" pattern=\"[0-9]\"\r\n                        [type]=\"hide ? 'password': 'text'\" inputmode=\"numeric\">\r\n                    <button mat-icon-button matSuffix (click)=\"hide = !hide\" [attr.aria-label]=\"'Hide password'\"\r\n                        [attr.aria-pressed]=\"hide\">\r\n                        <mat-icon>{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                    </button>\r\n                </div>\r\n                <button class=\"btn btn-primary\" type=\"submit\" id=\"button-verify\" (click)=\"verify()\">Verify</button>\r\n            </div>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<!-- <div class=\"_container\">\r\n      <div class=\"col-md-12\" style=\"background-color:white;\">\r\n        <div class=\"menu-bar\" *ngIf=\"myBalance\" >\r\n            My Current Balance : <strong>{{myBalance | currency: 'Rp '}}</strong> \r\n            <a class=\"common-button blue-fill withdraw\" routerLink=\"./withdrawal\" [routerLinkActive]=\"['router-link-active']\">\r\n                <i class=\"fa fa-arrow-alt-circle-down\t\"></i>Withdraw</a>\r\n        </div>\r\n        <div *ngIf=\"EscrowData&&pointstransactionDetail==false\">\r\n          <app-form-builder-table [searchCallback]=\"[service, 'searchEscrowtransactionLint',this]\"\r\n            [tableFormat]=\"tableFormat\" [table_data]=\"EscrowData\" [total_page]=\"totalPage\">\r\n\r\n          </app-form-builder-table>\r\n        </div>\r\n      </div>\r\n      </div> -->\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component.html":
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component.html ***!
  \********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"-container\">\r\n<div class=\"col-md-12\">\r\n  <div class=\"row\" style=\"place-content: center;\">\r\n    <div class=\"col-md-6\">\r\n      <h2 style=\"text-align: center; font-weight:bold;\">Withdrawal</h2><br/>\r\n      <div class=\"alert\" *ngIf=\"errorMessage\">\r\n        <span >{{errorMessage}}</span>\r\n      </div>\r\n      <div class=\"card\">\r\n        <img src=\"/assets/images/withdrawal.png\" style=\"width: 100px;height: auto;\">\r\n        <div class=\"amount\">\r\n      My Current Balance <br/>\r\n      <span style=\"font-size:32px; font-weight: bold;\">{{myBalance | currency: 'Rp '}}</span>  \r\n      </div>\r\n      </div>\r\n    </div>\r\n    </div> <br/>\r\n    <div class=\"row\" style=\"place-content: center;\">\r\n    <div class=\"col-md-6\">\r\n      <div class=\"card\">\r\n        <label required><strong>Input Amount*</strong></label>\r\n        <div class=\"inputs\">\r\n        <mat-form-field appearance=\"legacy\"  [hideRequiredMarker]=\"hideRequiredControl.value\"\r\n        [floatLabel]=\"floatLabelControl.value\" style=\"width:100%;font-size: 25px;\">\r\n          <input matInput [(ngModel)]=\"form.how_much\" placeholder=\"50.000,00\" type=\"number\" />\r\n          <span matPrefix>Rp&nbsp;</span>\r\n        </mat-form-field>\r\n        </div>\r\n        <!-- <span>{{form.how_much | currency: 'Rp '}}</span> -->\r\n        <!-- <button class=\"btn btn-primary\">Request</button> -->\r\n        <span style=\"font-size:12px\">*Minimum Withdraw is Rp 50.000,00</span> \r\n        <span style=\"font-size:12px\">*Max processing 3x24 hours (operational hours)</span> \r\n        <br/>\r\n        <div>\r\n          <span>Description*</span>\r\n          <textarea class=\"form-control\" style=\"min-height: 150px;\" [(ngModel)]=\"form.description\" required></textarea> <br/>\r\n        </div>\r\n        <div class=\"button-container\">\r\n        <button mat-button matSuffix class=\"btn btn-primary\" id=\"request\" (click)=\"sendRequest()\">Request</button>\r\n      </div>\r\n      </div>\r\n     \r\n    </div>\r\n   \r\n  </div>\r\n</div>\r\n</div>\r\n\r\n<div id=\"myModal\" class=\"modal {{othersForm}}\">\r\n  <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n          <span class=\"close\" (click)=\"closeDialog()\">&times;</span>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n          <h3>Enter Your PIN</h3>\r\n          <span><strong> If you enter a wrong PIN 3 times, your pin automatically blocked</strong></span>\r\n               \r\n      </div>\r\n      <div class=\"modal-body\">\r\n            <ng-otp-input (onInputChange)=\"onOtpChange($event)\" [config]=\"config\"></ng-otp-input>\r\n            <span class=\"forgot\" style=\"margin-top: 20px;color: blue;\" (click)=\"forgotPin()\">Forgot your PIN ?</span>\r\n      </div>\r\n      <div class=\"form-group uploaded\" *ngIf=\"loading\">\r\n        <div class=\"loading\">\r\n          <div class=\"sk-fading-circle\">\r\n            <div class=\"sk-circle1 sk-circle\"></div>\r\n            <div class=\"sk-circle2 sk-circle\"></div>\r\n            <div class=\"sk-circle3 sk-circle\"></div>\r\n            <div class=\"sk-circle4 sk-circle\"></div>\r\n            <div class=\"sk-circle5 sk-circle\"></div>\r\n            <div class=\"sk-circle6 sk-circle\"></div>\r\n            <div class=\"sk-circle7 sk-circle\"></div>\r\n            <div class=\"sk-circle8 sk-circle\"></div>\r\n            <div class=\"sk-circle9 sk-circle\"></div>\r\n            <div class=\"sk-circle10 sk-circle\"></div>\r\n            <div class=\"sk-circle11 sk-circle\"></div>\r\n            <div class=\"sk-circle12 sk-circle\"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button class=\"btn btn-primary\" *ngIf=\"!loading\" id=\"button-verify\" (click)=\"confirm()\">Confirm</button>\r\n      </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<!-- <div class=\"_container\">\r\n  <div class=\"col-md-12\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"form-group\">\r\n          My Current Balance : <strong>{{myBalance | currency: 'Rp '}}</strong> <br />\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-8\">\r\n        <div class=\"form-group\">\r\n          How much you want to withdraw ?* \r\n          <input class=\"form-control {{formEditable.how_much}}\" (focus)=\"editableForm('how_much', true)\"\r\n            (blur)=\"editableForm('how_much', false)\" type=\"number\" name=\"how_much\" [(ngModel)]=\"form.how_much\" /> <br/>\r\n            {{form.how_much | currency: 'Rp '}}\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-8\">\r\n        <div class=\"form-group\">\r\n          Give us some note :\r\n          <textarea class=\"form-control {{formEditable.description}}\" (focus)=\"editableForm('description', true)\"\r\n            (blur)=\"editableForm('description', false)\" type=\"text\" name=\"description\"\r\n            [(ngModel)]=\"form.description\"></textarea>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"notif error-message\">\r\n          {{errorMessage}}\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6\"><button (click)=\"sendRequest()\" class=\"common-button blue-fill\"> Send Request</button></div>\r\n    </div>\r\n\r\n  </div>\r\n</div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-app-header/merchant-portal-app-header.component.html":
/*!***************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-portal-app-header/merchant-portal-app-header.component.html ***!
  \***************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #content let-modal>\r\n   <div class=\"modal-header\">\r\n       <h4 class=\"modal-title\" id=\"modal-basic-title\">Enter Password</h4>\r\n       <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\r\n   </div>\r\n   <div class=\"modal-body\">\r\n       <form (submit)=\"onSubmit()\">\r\n           <div class=\"form-group\">\r\n               <label for=\"dateOfBirth\">Password</label>\r\n               <div class=\"input-group\">\r\n                   <input [(ngModel)]=\"password\" id=\"dateOfBirth\" type=\"password\" class=\"form-control\"\r\n                       placeholder=\"Password\" name=\"dp\">\r\n               </div>\r\n           </div>\r\n           <button type=\"submit\" class=\"btn btn-outline-dark\">Submit</button>\r\n       </form>\r\n   </div>\r\n</ng-template>\r\n\r\n\r\n<hr>\r\n\r\n<pre>{{closeResult}}</pre>\r\n\r\n<nav id=\"navbar-menu\" class=\"navbar navbar-expand-lg fixed-top\">\r\n   <button class=\"navbar-toggler\" type=\"button\" (click)=\"toggleSidebar()\">\r\n       <i class=\"fa fa-bars text-muted\" aria-hidden=\"true\"></i>\r\n   </button>\r\n   <ul id=\"navbarright\" class=\"navbar-nav ml-auto\" style=\"flex-direction: row;\">\r\n       <li class=\"datetimecontainer navbar-toggler\">\r\n           <a class=\"time-custom \">\r\n               <img src=\"assets/images/clock.svg\" style=\"width: 10px;\">\r\n               {{ timeCurrent }}\r\n           </a>\r\n           <a class=\"date-custom \">\r\n               <img src=\"assets/images/calendar icon.svg\" style=\"width: 10px;\">\r\n               {{ dateCurrent }}\r\n           </a>\r\n       </li>\r\n       <!-- RESPONSIVE -->\r\n      \r\n       <li class=\"nav-item navbar-toggler\" style=\"list-style-type: none;\" id=\"administrator\">\r\n           <a href=\"javascript:void(0)\" class=\"nav-link\">\r\n               <!-- <img src=\"assets/images/pikachu.png\"> -->\r\n               <span>Merchant</span>\r\n           </a>\r\n       </li>\r\n       <a class=\"nav-link logout resp navbar-toggler\" routerLink=\"/login-merchant\" (click)=\"onLoggedout()\">\r\n           <img src=\"assets/images/logout.png\"> {{ 'Log Out'  }}\r\n       </a>\r\n   </ul>\r\n   <!-- CLOSE-RESPONSIVE -->\r\n   <div class=\"collapse navbar-collapse\">\r\n       <div class=\"form-inline\">\r\n           <datalist class=\"search-menu\" id=\"browsers\">\r\n               <option *ngFor=\"let dt of routePath\" [value]=\"dt.path\">{{dt.label}}</option>\r\n           </datalist>\r\n       </div>\r\n       <a class=\"admin-portal\" routerLink=\"/login\" routerLink=\"/merchant-portal/homepage\">\r\n           <img src=\"assets/images/logo.png\"> {{ 'Merchant Portal'  }}\r\n       </a>\r\n       <ul class=\"navbar-nav ml-auto\">\r\n           <!-- NON-RESPONSIVE -->\r\n           <a class=\"time-custom\">\r\n               <img src=\"assets/images/clock.svg\" style=\"width: 20px;\">{{ time | date: 'hh:mm:ss a' }}\r\n           </a>\r\n           <a class=\"date-custom\">\r\n               <img src=\"assets/images/calendar icon.svg\" style=\"width: 20px;\">{{ dateCurrent }}\r\n           </a>\r\n           <li class=\"nav-item dropdown disable\" id=\"upgrade\">\r\n            <a routerLink=\"/merchant-portal/homepage/upgrade-genuine\" class=\"nav-link\" Toggle>\r\n                <!-- <img src=\"assets/images/best-seller.png\"> -->\r\n                <span>Upgrade Merchant ?</span>\r\n            </a>\r\n        </li>\r\n           <li class=\"nav-item dropdown\" id=\"administrator\">\r\n               <a href=\"javascript:void(0)\" class=\"nav-link\" routerLink=\"/merchant-portal/profile\" Toggle>\r\n                   <!-- <img src=\"assets/images/pikachu.png\"> -->\r\n                   <span>Merchant</span>\r\n               </a>\r\n           </li>\r\n           <a class=\"logout\" routerLink=\"/login-merchant\" (click)=\"onLoggedout()\">\r\n               <img src=\"assets/images/logout.png\"> {{ 'Log Out'  }}\r\n           </a>\r\n           <!-- CLOSE-NON-RESPONSIVE -->\r\n       </ul>\r\n   </div>\r\n</nav>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-app-sidebar/merchant-portal-app-sidebar.component.html":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-portal-app-sidebar/merchant-portal-app-sidebar.component.html ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar\">\r\n    <!-- <div class=\"item\">\r\n        <a routerLink=\"./add\" [routerLinkActive]=\"['router-link-active']\" class=\"add btn btn-primary\"> <i\r\n            class=\"fa fa-plus\"></i>\r\n            <span class=\"value\">&nbsp;New Product</span>\r\n            \r\n        </a>\r\n    </div> -->\r\n    <!-- <div class=\"item\" *ngFor=\"let item of sidebarMenu \">\r\n        <a [routerLink]=\"item.router_link\" [routerLinkActive]=\"['router-link-active']\" class=\"btn sidebar-btn\"\r\n           (click)=\"item.func()\" style=\"width: 100% !important;\"> \r\n           <span [innerHTML]=\"item.icon\" style=\"font-size: 35px;\"></span>\r\n                <br>\r\n                <span class=\"new-line\">{{item.label}}</span>\r\n        </a>\r\n    </div> -->\r\n    <div class=\"item\" *ngFor=\"let item of sidebarMenu \">\r\n      \r\n        <a [routerLink]=\"item.router_link\" [routerLinkActive]=\"['router-link-active']\" class=\"btn sidebar-btn flexs\"\r\n           (click)=\"item.func()\" style=\"width: 100% !important;\"> \r\n           <span [innerHTML]=\"item.icon\" style=\"font-size: 35px;\"></span>\r\n           <!-- <mat-icon [svgIcon]=\"item.icons\"></mat-icon> -->\r\n           <!-- <span>Icon 3</span> -->\r\n                <span class=\"new-line\">{{item.label}}</span>\r\n        </a>\r\n    </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-banner/merchant-portal-banner.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-portal-banner/merchant-portal-banner.component.html ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>merchant-portal-banner works!</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-detail-inbox/merchant-portal-detail-inbox.component.html":
/*!*******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-portal-detail-inbox/merchant-portal-detail-inbox.component.html ***!
  \*******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-orderhistoryallhistory-edit *ngIf=\"order_id\" [detail]=\"order_id\"></app-orderhistoryallhistory-edit>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.component.html ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n    <h2 style=\"margin-left:10px;margin-top:10px;\"><strong>Inbox</strong></h2>\r\n\r\n    <div class=\"background\">\r\n        <div class=\"mail-box {{hideColumnWhenOpen}}\">\r\n            <aside class=\"lg-side\">\r\n\r\n                <div class=\"inbox-body\">\r\n                    <div>\r\n                        <!-- <input type=\"checkbox\" class=\"mail-checkbox\"><span><i class=\"far fa-trash-alt fa-1x\"\r\n                                (click)=\"delete(id)\">Delete\r\n                                Icon</i></span> -->\r\n                    </div>\r\n                    <div class=\"table table-inbox table-hover\">\r\n                        <div class=\"tbody\">\r\n                            <div class=\"empty\" *ngIf=\"inboxData.length == 0 \">\r\n                                <img src=\"assets/images/mail.png\"><br />\r\n                                <br />\r\n                                <label style=\"font-size:30px;font-weight: bold;color:#6F6F6F\">Inbox Empty</label><br />\r\n                                <label style=\"font-size:20px;color:#B5B3B3;\">You don't have any incoming email</label>\r\n                            </div>\r\n\r\n                            <div *ngFor=\"let inbox of inboxData\" class=\"tr {{inbox.read}}\"\r\n                                (click)=\"messageDetail(inbox)\">\r\n                                <div class=\"td inbox-small-cells\">\r\n                                    <input type=\"checkbox\" class=\"mail-checkbox\">\r\n                                </div>\r\n                                <!--- we don't need stars -->\r\n                                <!-- <td class=\"inbox-small-cells\"><i class=\"fa fa-star\"></i></td> -->\r\n                                <div class=\"td view-message title\">\r\n                                    <div *ngIf=\"inbox.notification_status == 'unread'\">\r\n                                        <div style=\"font-weight: bold;\">Ada pesanan baru nih dengan no Invoice\r\n                                            {{inbox.reference.order_id}}</div>\r\n                                    </div>\r\n                                    <div *ngIf=\"inbox.notification_status == 'read'\">\r\n                                        <div>Ada pesanan baru nih dengan no Invoice {{inbox.reference.order_id}}</div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"td view-message description \">\r\n                                    <div [innerHTML]=\"inbox.description\"></div>\r\n                                </div>\r\n                                <!--- we don't need paperclip -->\r\n                                <!-- <td class=\"view-message  inbox-small-cells\"><i class=\"fa fa-paperclip\"></i></td> -->\r\n                                <!-- <div class=\"td view-message time   text-right\">\r\n                            <div>{{inbox.updated_date}}</div>\r\n                        </div> -->\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </aside>\r\n\r\n            <div class=\"email-content h-100 \">\r\n                <div class=\"h-100 scrollable pos-r\" *ngIf=\"selectedData\">\r\n                    <div class=\"bgc-grey-100 peers ai-c jc-sb p-20 fxw-nw d-n@md+\">\r\n                        <div class=\"peer\">\r\n                            <!-- <div class=\"btn-group\" role=\"group\">\r\n                        <button type=\"button\" class=\"back-to-mailbox btn bgc-white bdrs-2 mR-3 cur-p\">\r\n                            <i class=\"ti-angle-left\"></i>\r\n                        </button>\r\n                        <button type=\"button\" class=\"btn bgc-white bdrs-2 mR-3 cur-p\">\r\n                            <i class=\"ti-folder\"></i>\r\n                        </button>\r\n                        <button type=\"button\" class=\"btn bgc-white bdrs-2 mR-3 cur-p\">\r\n                            <i class=\"ti-tag\"></i>\r\n                        </button>\r\n                        <div class=\"btn-group\" role=\"group\">\r\n                            <button id=\"btnGroupDrop1\" type=\"button\"\r\n                                class=\"btn cur-p bgc-white no-after dropdown-toggle\" data-toggle=\"dropdown\"\r\n                                aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                                <i class=\"ti-more-alt\"></i>\r\n                            </button>\r\n                            <ul class=\"dropdown-menu fsz-sm\" aria-labelledby=\"btnGroupDrop1\">\r\n                                <li>\r\n                                    <a href=\"\" class=\"d-b td-n pY-5 pX-10 bgcH-grey-100 c-grey-700\">\r\n                                        <i class=\"ti-trash mR-10\"></i>\r\n                                        <span>Delete</span>\r\n                                    </a>\r\n                                </li>\r\n                                <li>\r\n                                    <a href=\"\" class=\"d-b td-n pY-5 pX-10 bgcH-grey-100 c-grey-700\">\r\n                                        <i class=\"ti-alert mR-10\"></i>\r\n                                        <span>Mark as Spam</span>\r\n                                    </a>\r\n                                </li>\r\n                                <li>\r\n                                    <a href=\"\" class=\"d-b td-n pY-5 pX-10 bgcH-grey-100 c-grey-700\">\r\n                                        <i class=\"ti-star mR-10\"></i>\r\n                                        <span>Star</span>\r\n                                    </a>\r\n                                </li>\r\n                            </ul>\r\n                        </div>\r\n                    </div> -->\r\n                        </div>\r\n                        <!-- <div class=\"peer\">\r\n                    <div class=\"btn-group\" role=\"group\">\r\n                        <button type=\"button\" class=\"fsz-xs btn bgc-white bdrs-2 mR-3 cur-p\">\r\n                            <i class=\"ti-angle-left\"></i>\r\n                        </button>\r\n                        <button type=\"button\" class=\"fsz-xs btn bgc-white bdrs-2 mR-3 cur-p\">\r\n                            <i class=\"ti-angle-right\"></i>\r\n                        </button>\r\n                    </div>\r\n                </div> -->\r\n                    </div>\r\n                    <div class=\"email-content-wrapper\">\r\n                        <!-- Header -->\r\n                        <div class=\"peers ai-c jc-sb pX-40 pY-30\">\r\n                            <button class=\"close\" (click)=\"closeData()\" style=\"margin-right:10px;margin-top:10px;\"><i\r\n                                    class=\"far fa-window-close\"></i></button>\r\n                            <div class=\"peers peer-greed\">\r\n                                <div class=\"peer\">\r\n                                    <small>{{selectedData.updated_date}}</small>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n\r\n                        <!-- Content -->\r\n                        <div class=\"bdT pX-40 pY-30\" *ngIf=\"selectedData\">\r\n                            <h3>{{selectedData.title_message}}</h3>\r\n                            <div class=\"message\" [innerHTML]=\"selectedData.message\"></div>\r\n                        </div>\r\n                        <div id=\"button\">\r\n                            <button class=\"btn btn-primary\" (click)=\"goToOrderDetail(selectedData.reference.booking_id)\">Go To Order Detail</button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component.html":
/*!*******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component.html ***!
  \*******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>merchant-portal-orderhistories-tab works!</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-portal.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"merchant-portal\" class=\"{{devclass}}\">\r\n    <merchant-portal-app-header></merchant-portal-app-header>\r\n    <merchant-portal-app-sidebar></merchant-portal-app-sidebar>\r\n    <section class=\"main-container\">\r\n        <div class=\"animate\" [@routeAnimations]=\"prepareRoute(outlet)\">\r\n            <router-outlet #outlet=\"outlet\"></router-outlet>\r\n        </div>\r\n    </section>\r\n</section>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-sales-order/merchant-sales-order.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-sales-order/merchant-sales-order.component.html ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-page-header [heading]=\"'Merchant Sales Order'\" [icon]=\"'fa-table'\"></app-page-header> -->\r\n<app-form-builder-table\r\n    [table_data]=\"allData\"\r\n    [searchCallback]=\"[service,'searchSalesOrderReport',this]\"\r\n    [tableFormat]=\"tableFormat\"\r\n    [total_page]=\"totalPage\"\r\n>\r\n</app-form-builder-table>\r\n  <!-- <div *ngIf=\"Orderhistory&&orderhistoryDetail==false\"> -->\r\n        <!-- <app-form-builder-table \r\n        [searchCallback]=\"[service,'searchOrderhistoryLint',this]\" \r\n        [tableFormat]=\"tableFormat\"\r\n        [table_data]=\"Orderhistory\"\r\n      >\r\n        </app-form-builder-table> -->\r\n  <!-- </div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/order-detail/order-detail.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/order-detail/order-detail.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-orderhistoryallhistory-edit *ngIf=\"orderId\" [back]=\"[this]\" [detail]=\"orderId\">\r\n</app-orderhistoryallhistory-edit>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/order-histories/shipping-label/shipping-label.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/order-histories/shipping-label/shipping-label.component.html ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 id=\"header\"><strong>Shipping Label</strong></h1>\r\n<div class=\"btn-print\">\r\n    <button mat-raised-button color=\"primary\" class=\"btn btn-primary\" (click)=\"prints()\">Print Label<i\r\n            class=\"fa fa-print\" aria-hidden=\"true\"></i></button>\r\n</div>\r\n<div class=\"notif error-message\">{{errorMessage}}</div>\r\n<div class=\"loading\" *ngIf='loading'>\r\n    <div class=\"sk-fading-circle\">\r\n      <div class=\"sk-circle1 sk-circle\"></div>\r\n      <div class=\"sk-circle2 sk-circle\"></div>\r\n      <div class=\"sk-circle3 sk-circle\"></div>\r\n      <div class=\"sk-circle4 sk-circle\"></div>\r\n      <div class=\"sk-circle5 sk-circle\"></div>\r\n      <div class=\"sk-circle6 sk-circle\"></div>\r\n      <div class=\"sk-circle7 sk-circle\"></div>\r\n      <div class=\"sk-circle8 sk-circle\"></div>\r\n      <div class=\"sk-circle9 sk-circle\"></div>\r\n      <div class=\"sk-circle10 sk-circle\"></div>\r\n      <div class=\"sk-circle11 sk-circle\"></div>\r\n      <div class=\"sk-circle12 sk-circle\"></div>\r\n    </div>\r\n  </div>\r\n<div class=\"page-break\" *ngIf=\"!loading\">\r\n    <div class=\"_container\" *ngFor=\"let data of totalResult; let i = index\">\r\n        <div class=\"content\">\r\n            <div class=\"table-order\">\r\n                <table class=\"header\" align=\"center\">\r\n                    <tr>\r\n                        <td class=\"locard-logo\">\r\n                            <img src=\"assets/images/logo.png\" class=\"logo-avatar\" /><br>\r\n                            \r\n                        </td>\r\n                        <td class=\"awb\">\r\n                            <p>AWB</p>\r\n                            <ngx-barcode [bc-value]=\"data.courier.awb_number\" [bc-display-value]=\"false\" [bc-width]=\"1.5\"\r\n                                [bc-height]=\"70\" [bc-font-size]=\"10\">\r\n                            </ngx-barcode>\r\n                            <label style=\"font-size:14px;\">No.Resi : <strong>{{data.courier.awb_number}}</strong></label>\r\n                        </td>\r\n                        <!-- <td *ngIf=\"same\" class=\"locard-logo\">\r\n                            <img src=\"{{courier_image}}\" class=\"logo-avatar\" />\r\n                        </td>\r\n                        <td *ngIf=\"!same\" class=\"locard-logo\">\r\n                            <span style=\"font-size: 20px;\">{{data.courier.courier}}</span>\r\n                        </td> -->\r\n                        <td class=\"locard-logo\">\r\n                            <img *ngIf=\"courier_image_array[i].same == true\" src=\"{{courier_image_array[i].image}}\" class=\"logo-avatar\" />\r\n                            <span *ngIf=\"courier_image_array[i].same == false\" style=\"font-size: 20px;\">{{courier_image_array[i].courier}}</span>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <table align=\"center\" class=\"detail-header\">\r\n                    <div class=\"row\">\r\n                        <div class=\" detail-person\">\r\n                            <div id=\"order\">\r\n                            <strong>No id : {{data.booking_id}} </strong> <br />\r\n                            </div>\r\n                            <div class=\"row sender\">\r\n                                <div style=\"padding: 0;width: 50%;\">\r\n                                    <p><strong>Pengirim: </strong><br>\r\n                                        {{merchant_data.merchant_name}}<br>\r\n                                        {{merchant_data.maddress}} <br>\r\n                                        {{merchant_data.cell_phone}}<br>\r\n                                    </p>\r\n                                </div>\r\n                                <div style=\"padding: 0;width: 50%;\">\r\n                                    <p><strong>Penerima : </strong><br>\r\n                                        {{data.buyer_detail.name}}<br>\r\n                                        {{data.buyer_detail.address}}<br>\r\n                                        {{data.buyer_detail.cell_phone}}<br>\r\n                                    </p>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <div class=\"receiver\"></div> -->\r\n                            <!-- <div class=\"send\">\r\n                            <label id=\"non\">NON-COD</label> <br />\r\n                            <label id=\"shipping\">{{data.shipping.service}}</label>\r\n                            </div> -->\r\n                        </div>\r\n                        <div class=\" detail-order\">\r\n                            <p>\r\n                                <strong>Tanggal: </strong>\r\n                                {{data.created_date}}<br><br>\r\n                                <strong>Kota Asal: </strong>\r\n                                {{memberCity}}<br><br>\r\n                                <!-- <strong class=\"tlc\" *ngIf=\"data.courier.courier == 'SAP'\">TLC: </strong><span class=\"tlc\" *ngIf=\"data.courier.courier == 'SAP' && data.tlc_branch_origin\">{{data.tlc_branch_origin}}<br><br></span> -->\r\n                                <strong>Kota Tujuan: </strong><br>\r\n                                {{data.buyer_detail.city_name}}<br><br>\r\n                                <strong class=\"tlc\" *ngIf=\"data.courier.courier == 'SAP'\">TLC: </strong><span class=\"tlc\" *ngIf=\"data.courier.courier == 'SAP' && data.tlc_branch_dest\">{{data.tlc_branch_dest}}<br><br></span>\r\n                                <strong>Jumlah: </strong>\r\n                                {{data.total_item}}<br><br>\r\n                                <strong>Berat Total: </strong>\r\n                                <span>{{totalWeight[i]}} Kg</span> <br>\r\n                                <br/>\r\n                                <strong>Asuransi : - </strong>\r\n                                <br/> <br/>\r\n                                <strong>Service : </strong>\r\n                                {{data.delivery.delivery_detail.service}}\r\n                                \r\n                            </p>\r\n                        </div>\r\n                    </div>\r\n                </table>\r\n                <table align=\"center\" class=\"detail-barang-pengiriman\">\r\n                    <div class=\"row row-eq-height\">\r\n                        <div class=\"detail-barang\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-8 daftar-barang\">\r\n                                    <strong>Daftar Barang : </strong> <br>\r\n                                    <span class=\"data-products\"\r\n                                        *ngFor=\"let b of data.products\">{{b.product_name}}({{b.quantity}}),\r\n                                    </span><br>\r\n                                </div>\r\n                                <!-- <div class=\"col-md-6 catatan\">\r\n                                    <label>Terima Kasih sudah berbelanja di locard!</label>\r\n                                </div> -->\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"detail-pengiriman\">\r\n                            <div class=\"row\" style=\"margin-right: 0px;\">\r\n                                <div class=\"col-md-12 jenis-pengiriman\">\r\n                                    <strong>Catatan: </strong><br>\r\n                                    <span  *ngFor=\"let b of data.products;let i = index\">\r\n                                        {{i+1}}. {{b.note_to_merchant}}\r\n                                    </span>\r\n                                  \r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </table>\r\n            </div>\r\n            <p class=\"page-number\">\r\n                Page {{i+1}} of {{totalResult.length}}\r\n            </p>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/profile/profile.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/profile/profile.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input #inputFile2 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'image_owner_url')\"\r\n    accept=\"image/*\">\r\n<input #inputFile1 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'background_image')\"\r\n    accept=\"image/*\">\r\n\r\n<div class=\"form\">\r\n    <div class=\"error\" *ngIf=\"errorLabel\">{{errorLabel}}</div>\r\n    <div class=\"row\">\r\n        <div class=\"col-md-9 head-form\">\r\n            <div class=\"photo-profile\">\r\n                <app-photo-profile-component [bgUrl]=\"form.image_owner_url\"></app-photo-profile-component>\r\n            </div>\r\n            <div class=\"detail\">\r\n                <label class=\"name\">{{form.full_name}}</label>\r\n                <label class=\"email\">{{form.email}}</label>\r\n                <label class=\"dob\">{{form.dob}}</label>\r\n            </div>\r\n            <div class=\"detail\">\r\n                <label class=\"membership\">{{form.membership}}</label>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"nav toolbar\">\r\n        <div style=\"display:flex\">\r\n        </div>\r\n    </div>\r\n    <div class=\"form-body\">\r\n        <mat-tab-group>\r\n            <mat-tab label=\"Display\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-8\">\r\n                        <div class=\"card-content\">\r\n                            <div class=\"card\" style=\"margin-top:20px;\">\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"row\">\r\n                                        <div class=\"image-part\">\r\n                                            <div class=\"background\">\r\n                                                <img style=\"background-size:cover; width:100%\"\r\n                                                    src=\"{{form.background_image}}\">\r\n                                                <div class=\"photo\">\r\n                                                    <img style=\"width:240px;border-radius:50%;\"\r\n                                                        src=\"{{form.image_owner_url}}\">\r\n                                                </div>\r\n                                            </div>\r\n                                            <span\r\n                                                style=\"font-weight: bold; font-size:29px;\">{{form.merchant_name}}</span>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"card\" style=\"margin-top:10px;\">\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n                                        <label style=\"font-size:18px;color:#545454;\">Store Info\r\n                                            (Description)</label><br />\r\n                                        <span\r\n                                            style=\"font-size:16px;color:#777777;\">{{form.description}}</span><br /><br />\r\n                                        <label style=\"font-size:18px;color:#545454;\">Store Slogan </label><br />\r\n                                        <span style=\"font-size:16px;color:#777777;\">{{form.slogan}}</span>\r\n\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n\r\n\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-4\" style=\"margin-top:20px;\">\r\n                        <div class=\"card mb-3\" style=\"padding:10px;\">\r\n                            <span style=\"font-size:29px;color:#545454;\"> Store Information </span>\r\n                            <div class=\"content\">\r\n                                <label>Store Name</label><br />\r\n                                <span style=\"color:#777777\">{{form.merchant_name}}</span>\r\n                                <br />\r\n                                <br />\r\n                                <label>Store Address</label><br />\r\n                                <span style=\"color:#777777\">{{form.maddress}}</span>\r\n                                <br />\r\n                                <br />\r\n                                <label>Province</label><br />\r\n                                <span style=\"color:#777777\">{{form.mprovince}}</span>\r\n                                <br />\r\n                                <br />\r\n                                <label>City</label><br />\r\n                                <span style=\"color:#777777\">{{form.mcity}}</span>\r\n                                <br />\r\n                                <br />\r\n                                <label>Districts/Kecamatan</label><br />\r\n                                <span style=\"color:#777777\">{{form.msubdistrict}}</span>\r\n                                <br />\r\n                                <br />\r\n                                <label>Kelurahan/Desa</label><br />\r\n                                <span style=\"color:#777777\">{{form.mvillage}}</span>\r\n                                <br />\r\n                                <br />\r\n                                <label>Postal Code</label><br />\r\n                                <span style=\"color:#777777\">{{form.mpostal_code}}</span>\r\n                                <br />\r\n                                <br />\r\n                                <label>Another Contact Person</label><br />\r\n                                <span style=\"color:#777777\">{{form.contact_person}}</span>\r\n                                <br />\r\n                                <br />\r\n                                <label>Courier Service</label><br />\r\n                                <span *ngFor=\"let cr of couriers.result\" style=\"color:#777777\"><span *ngIf=\"cr.value\">{{cr.courier}} </span></span>\r\n                                <br />\r\n                                <br />\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <!-- <iframe [src]=\"https://www.youtube.com/\">\r\n                    <p>Testtt</p>\r\n                    </iframe> -->\r\n\r\n                </div>\r\n            </mat-tab>\r\n            <mat-tab label=\"Store Information\">\r\n                <div class=\"col-md-12\">\r\n\r\n                    <div class=\"card error\" *ngIf=\"errorMessage\">\r\n                        <span>{{errorMessage}}</span>\r\n                    </div>\r\n\r\n                    <h3 class=\"title-diff\">Store Detail</h3>\r\n\r\n\r\n                    <div class=\"form-group\">\r\n                        <label>Merchant Name</label>\r\n                        <input class=\"form-control\" (focus)=\"editableForm('merchant_name',true)\"\r\n                            (blur)=\"editableForm('merchant_name', false)\" type=\"text\" name=\"store name\"\r\n                            (input)=\"nameInput($event)\" [(ngModel)]=\"form.merchant_name\" />\r\n                        <div *ngIf=\"!formEditable.merchant_name\" class=\"iconic\"\r\n                            (click)=\"editableForm('merchant_name', true)\">\r\n                            <i class=\"fa fa-pen\"></i>\r\n                        </div>\r\n                    </div>\r\n\r\n\r\n                    <div class=\"form-group\" *ngIf=\"this.form.merchant_username != 'undefined'\">\r\n                        <label>Merchant Username</label>\r\n                        <!-- <span class=\"check\">\r\n                            <mat-checkbox [(ngModel)]=\"autoFill\" value=\"autoFill\">\r\n                                Auto Fill based on Merchant Name\r\n                            </mat-checkbox>\r\n                        </span> -->\r\n                        <input class=\"form-control\" (focus)=\"editableForm('merchant_username',true)\"\r\n                            (blur)=\"editableForm('merchant_username', false)\" type=\"text\" name=\"store name\"\r\n                            [(ngModel)]=\"form.merchant_username\" [disabled]=\"true\" />\r\n                        <div *ngIf=\"!formEditable.merchant_username\" class=\"iconic\"\r\n                            (click)=\"editableForm('merchant_username', true)\">\r\n                            <i class=\"fa fa-pen\"></i>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <!-- <div class=\"form-group\">\r\n                                <label>Merchant Group</label>\r\n                                <select class=\"form-control {{formEditable.merchant_group}}\"\r\n                                    (focus)=\"editableForm('merchant_group',true)\" (blur)=\"editableForm('merchant_group', false)\"\r\n                                    type=\"text\" name=\"store name\" [(ngModel)]=\"form.merchant_group\">\r\n                                    <option *ngFor=\"let mg of merchant_group\" [ngValue]=\"mg.value\">{{mg.label}}</option>\r\n                                </select>\r\n                                <div *ngIf=\"!formEditable.merchant_group\" class=\"iconic\"\r\n                                    (click)=\"editableForm('merchant_group', true)\">\r\n                                    <i class=\"fa fa-pen\"></i>\r\n                                </div>\r\n                            </div> -->\r\n\r\n\r\n                    <!-- <div class=\"form-group\">\r\n                        <label>Email</label>\r\n                        <input class=\"form-control {{formEditable.email}}\" (focus)=\"editableForm('email',true)\"\r\n                            (blur)=\"editableForm('email', false)\" type=\"text\" name=\"store name\"\r\n                            [(ngModel)]=\"form.email\" />\r\n                        <div *ngIf=\"!formEditable.email\" class=\"iconic\" (click)=\"editableForm('emailx', true)\">\r\n                            <i class=\"fa fa-pen\"></i>\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    <div class=\"form-group\">\r\n                        <label>Email Merchant</label>\r\n                        <input class=\"form-control\" (focus)=\"editableForm('email_address', true)\"\r\n                            (blur)=\"editableForm('email_address', false)\" type=\"text\" name=\"email\"\r\n                            [(ngModel)]=\"form.email_address\" />\r\n                        <div *ngIf=\"!formEditable.email_address\" class=\"iconic\"\r\n                            (click)=\"editableForm('email_address', true)\"><i class=\"fa fa-pen\"></i></div>\r\n                    </div>\r\n\r\n                    <div class=\"form-group\">\r\n                        <label>Store Description</label>\r\n                        <!-- <textarea class=\"form-control {{formEditable.description}}\"\r\n                            (focus)=\"editableForm('description', true)\" (blur)=\"editableForm('description', false)\"\r\n                            type=\"text\" name=\"description\" [(ngModel)]=\"form.description\"></textarea> -->\r\n                        <ckeditor [editor]=\"Editor\" [config]=\"config\" (focus)=\"editableForm('description', true)\"\r\n                            (blur)=\"editableForm('description', false)\" type=\"text\" name=\"description\"\r\n                            [(ngModel)]=\"form.description\"></ckeditor>\r\n\r\n                    </div>\r\n\r\n                    <div class=\"form-group\">\r\n                        <label>Store Address</label>\r\n                        <input class=\"form-control\" (focus)=\"editableForm('maddress',true)\"\r\n                            (blur)=\"editableForm('maddress',false)\" type=\"text\" name=\"maddress\"\r\n                            [(ngModel)]=\"form.maddress\" />\r\n                        <div *ngIf=\"!formEditable.maddress\" class=\"iconic\" (click)=\"editableForm('maddress', true)\">\r\n                            <i class=\"fa fa-pen\"></i>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-6\">\r\n\r\n                            <div class=\"form-group\">\r\n                                <label>Province</label>\r\n                                <input type=\"text\" class=\"form-control\" (focus)=\"editableForm('mprovince', true)\"\r\n                                    (blur)=\"editableForm('mprovince', false)\" name=\"state\" [(ngModel)]=\"form.mprovince\"\r\n                                    (keyup)=\"getProvinceList($event)\" list=\"dynmicProvinceList\"\r\n                                    (change)=\"onProvinceChange()\" autocomplete=\"off\" />\r\n                                <datalist id=\"dynmicProvinceList\" name=\"province\" ngDefaultControl>\r\n                                    <option *ngFor=\"let item of provinceList\" [ngValue]=\"item.province\">\r\n                                        {{item.province}}\r\n                                    </option>\r\n                                </datalist>\r\n                                <div *ngIf=\"!formEditable.mprovince\" class=\"iconic\"\r\n                                    (click)=\"editableForm('mprovince', true)\">\r\n                                    <i class=\"fa fa-pen\"></i>\r\n                                </div>\r\n                            </div>\r\n\r\n\r\n                            <!-- <div class=\"form-group\">\r\n                                <label> Country </label>\r\n                                <select class=\"form-control\" (focus)=\"editableForm('mcountry', true)\"\r\n                                    (blur)=\"editableForm('mcountry', false)\" [(ngModel)]=\"form.mcountry\">\r\n                                    <option *ngFor=\"let f of countryList\" [ngValue]=\"f.value\">{{f.name}}</option>\r\n\r\n                                </select>\r\n\r\n\r\n                            </div> -->\r\n\r\n                            <!-- <div class=\"form-group\">\r\n                                <label> District </label>\r\n                                <input type=\"text\" class=\"form-control {{formEditable.mdistrict}}\"\r\n                                    (focus)=\"editableForm('mdistrict', true)\" (blur)=\"editableForm('mdistrict', false)\"\r\n                                    name=\"country\" [(ngModel)]=\"form.mdistrict\" />\r\n                                <div *ngIf=\"!formEditable.mdistrict\" class=\"iconic\"\r\n                                    (click)=\"editableForm('mdistrict', true)\">\r\n                                    <i class=\"fa fa-pen\"></i>\r\n                                </div>\r\n                            </div> -->\r\n\r\n                            <div class=\"form-group\">\r\n                                <label> Sub District </label>\r\n                                <input type=\"text\" class=\"form-control\" (focus)=\"editableForm('msubdistrict', true)\"\r\n                                    (blur)=\"editableForm('msubdistrict', false)\" name=\"country\"\r\n                                    [(ngModel)]=\"form.msubdistrict\" list=\"dynmicSubDistList\"\r\n                                    (keyup)=\"getSubDistList($event)\" autocomplete=\"off\"/>\r\n                                <datalist id=\"dynmicSubDistList\" name=\"sub_district\" ngDefaultControl>\r\n                                    <option *ngFor=\"let item of subDistrictList\" [ngValue]=\"item.subdistrict\">\r\n                                        {{item.subdistrict}}\r\n                                    </option>\r\n                                </datalist>\r\n                                <div *ngIf=\"!formEditable.msubdistrict\" class=\"iconic\"\r\n                                    (click)=\"editableForm('msubdistrict', true)\">\r\n                                    <i class=\"fa fa-pen\"></i>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"form-group\">\r\n                                <label> Postal Code </label>\r\n                                <input type=\"number\" class=\"form-control\" (focus)=\"editableForm('mpostal_code', true)\"\r\n                                    (blur)=\"editableForm('mpostal_code', false)\" name=\"postal code\"\r\n                                    [(ngModel)]=\"form.mpostal_code\" maxlength=\"3\" autocomplete=\"off\" />\r\n                                <div *ngIf=\"!formEditable.mpostal_code\" class=\"iconic\"\r\n                                    (click)=\"editableForm('mpostal_code', true)\">\r\n                                    <i class=\"fa fa-pen\"></i>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\"form-group\">\r\n                                <label>City</label>\r\n                                <input type=\"text\" class=\"form-control\" (focus)=\"editableForm('mcity', true)\"\r\n                                    (blur)=\"editableForm('mcity', false)\" name=\"city\" [(ngModel)]=\"form.mcity\"\r\n                                    (keyup)=\"getCityList($event)\" list=\"dynmicCityList\" autocomplete=\"off\"/>\r\n                                <datalist id=\"dynmicCityList\" name=\"city\" ngDefaultControl>\r\n                                    <option *ngFor=\"let item of cityList\" [ngValue]=\"item.city\">\r\n                                        {{item.city}}\r\n                                    </option>\r\n                                </datalist>\r\n\r\n                                <div *ngIf=\"!formEditable.mcity\" class=\"iconic\" (click)=\"editableForm('mcity', true)\">\r\n                                    <i class=\"fa fa-pen\"></i>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"form-group\">\r\n                                <label> Village </label>\r\n                                <input type=\"text\" class=\"form-control\" (focus)=\"editableForm('village', true)\"\r\n                                    list=\"dynmicVillageList\" (blur)=\"editableForm('village', false)\" name=\"village\"\r\n                                    [(ngModel)]=\"form.mvillage\" (keyup)=\"getVillageList($event)\"\r\n                                    (change)=\"onVillageChange()\" autocomplete=\"off\"/>\r\n                                <datalist id=\"dynmicVillageList\" name=\"region_code\" ngDefaultControl>\r\n                                    <option *ngFor=\"let item of villageList\" [ngValue]=\"item.village\">\r\n                                        {{item.village}}\r\n                                    </option>\r\n                                </datalist>\r\n                            </div>\r\n\r\n                            <div class=\"form-group\">\r\n                                <label>Another Contact Person</label>\r\n                                <input type=\"'number'\" class=\"form-control\"\r\n                                    (focus)=\"editableForm('contact_person', true)\"\r\n                                    (blur)=\"editableForm('contact_person', false)\" name=\"contact_person\"\r\n                                    [(ngModel)]=\"form.contact_person\" />\r\n                                <div *ngIf=\"!formEditable.contact_person\" class=\"iconic\"\r\n                                    (click)=\"editableForm('contact_person', true)\">\r\n                                    <i class=\"fa fa-pen\"></i>\r\n                                </div>\r\n                            </div>\r\n\r\n\r\n                            <!-- <div class=\"form-group\">\r\n                                <label>State</label>\r\n                                <input type=\"text\" class=\"form-control {{formEditable.mstate}}\"\r\n                                    (focus)=\"editableForm('mstate', true)\" (blur)=\"editableForm('mstate', false)\"\r\n                                    name=\"state\" [(ngModel)]=\"form.mstate\" />\r\n                                <div *ngIf=\"!formEditable.mstate\" class=\"iconic\" (click)=\"editableForm('mstate', true)\">\r\n                                    <i class=\"fa fa-pen\"></i>\r\n                                </div>\r\n                            </div> -->\r\n\r\n\r\n\r\n\r\n                            <!-- <div class=\"form-group\">\r\n                                <label>Region Code</label>\r\n                                <input type=\"text\" class=\"form-control {{formEditable.region_code}}\"\r\n                                    (focus)=\"editableForm('region_code', true)\"\r\n                                    (blur)=\"editableForm('region_code', false)\" name=\"city\"\r\n                                    [(ngModel)]=\"form.region_code\" />\r\n                                <div *ngIf=\"!formEditable.region_code\" class=\"iconic\"\r\n                                    (click)=\"editableForm('region_code', true)\">\r\n                                    <i class=\"fa fa-pen\"></i>\r\n                                </div>\r\n                            </div> -->\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"\">\r\n                        <div class=\"form-group\">\r\n                            <label>Store Slogan</label>\r\n                            <textarea class=\"form-control\" (focus)=\"editableForm('slogan', true)\"\r\n                                (blur)=\"editableForm('slogan', false)\" type=\"text\" name=\"slogan\"\r\n                                [(ngModel)]=\"form.slogan\"></textarea>\r\n\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-6\">\r\n\r\n                        </div>\r\n\r\n                        <div class=\"col-md-6\">\r\n                            <!-- <div class=\"form-group\">\r\n                                <label>Fax Number</label>\r\n                                <input type=\"number\" class=\"form-control {{formEditable.fax_number}}\"\r\n                                    (focus)=\"editableForm('fax_number', true)\"\r\n                                    (blur)=\"editableForm('fax_number', false)\" name=\"fax\"\r\n                                    [(ngModel)]=\"form.fax_number\" />\r\n                                <div *ngIf=\"!formEditable.fax_number\" class=\"iconic\"\r\n                                    (click)=\"editableForm('fax_number', true)\">\r\n                                    <i class=\"fa fa-pen\"></i>\r\n                                </div>\r\n                            </div> -->\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                        <!-- <div class=\"col-md-4\">\r\n                            <div class=\"form-group\">\r\n                                <label>Operational Hours</label>\r\n                                <div class=\"form-group row\">\r\n                                    <label class=\"col-sm-3 col-form-label\">Senin</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                    <label class=\"col-form-label\">-</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group row\">\r\n                                    <label class=\"col-sm-3 col-form-label\">Selasa</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                    <label class=\"col-form-label\">-</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group row\">\r\n                                    <label class=\"col-sm-3 col-form-label\">Rabu</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                    <label class=\"col-form-label\">-</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group row\">\r\n                                    <label class=\"col-sm-3 col-form-label\">Kamis</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                    <label class=\"col-form-label\">-</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group row\">\r\n                                    <label class=\"col-sm-3 col-form-label\">Jumat</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                    <label class=\"col-form-label\">-</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group row\">\r\n                                    <label class=\"col-sm-3 col-form-label\">Sabtu</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                    <label class=\"col-form-label\">-</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group row\">\r\n                                    <label class=\"col-sm-3 col-form-label\">Minggu</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                    <label class=\"col-form-label\">-</label>\r\n                                    <div class=\"col-sm-3\">\r\n                                        <input type=\"time\" class=\"form-control\">\r\n                                    </div>\r\n                                </div>ava\r\n                            </div>\r\n                        </div> -->\r\n\r\n                        <!-- <div class=\"vl\"></div> -->\r\n\r\n                        <div class=\"col-md-12\">\r\n                            <div class=\"mr-5\">\r\n                                <div class=\"form-group courier-service\">\r\n                                    <label>Available Courier Service</label>\r\n                                      <span class=\"d-inline-block\" tabindex=\"0\" data-toggle=\"tooltip\" title=\"To enable this button, please fill Province,City,Sub District, and Village\">\r\n                                    <span class=\"d-inline-block\" tabindex=\"0\" data-toggle=\"tooltip\"\r\n                                        title=\"Lakukan Pengisian Provinsi sampai Desa\">\r\n                                        <!-- <button class=\"check-button common-button\"\r\n                                            [class.darkwhite]=\"!complete\" [class.blue-fill]=\"complete\"\r\n                                            (click)=\"checkCourier()\">Check</button> -->\r\n                                    </span>\r\n                                  </span>\r\n                                    <!-- <span *ngIf=\"this.form.mvillage\"><button class=\"btn btn-primary btn-sm\" (click)=\"checkCourier()\" style=\"margin-left:5px;border-radius: 5px;\">Check</button></span> -->\r\n                                    <div>\r\n                                        <div *ngFor=\"let cr of couriers.result; let i = index\">\r\n                                            <mat-checkbox [(ngModel)]=\"cr.value\">\r\n                                                {{cr.courier}} </mat-checkbox>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n\r\n                        <!-- <div class=\"vl\"></div> -->\r\n\r\n                        <!-- <div class=\"col-md-2\">\r\n                            <label>Open/Close Store</label>\r\n                            <div class=\"form-group\">\r\n                                <select class=\"custom-select\">\r\n                                    <option>Open</option>\r\n                                    <option>Close</option>\r\n                                </select>\r\n                            </div>\r\n                        </div> -->\r\n                    </div>\r\n                  \r\n                    <br/>\r\n                    <div class=\"col-md-12\">\r\n                        <h3 class=\"title-diff\">Store Address (Optional)</h3>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-6\">\r\n                                <div class=\"form-group\">\r\n                                    <label>Search Location Adress</label>\r\n                                    <!-- <button *ngIf=\"!showMap\" class=\"check-button common-button\" [disabled]=\"showMap\"\r\n                                    [class.blue-fill]=\"!showMap\"\r\n                                    (click)=\"showMaps()\">Search My Current Location</button> -->\r\n                                    <input placeholder=\"search for location\" autocorrect=\"off\" autocapitalize=\"off\"\r\n                                        spellcheck=\"off\" type=\"text\" class=\"form-control\" #search\r\n                                        [formControl]=\"searchControl\" [(ngModel)]=\"form.google_address\">\r\n                                </div>\r\n                                <!-- <div class=\"form-group\">\r\n                                 \r\n                                    <span>Lat</span><input class=\"form-control\" type=\"text\" name=\"curlatitude\"\r\n                                        [(ngModel)]=\"form.latitude\" />\r\n                                    <span>Long</span><input class=\"form-control\" type=\"text\" name=\"curlongitude\"\r\n                                        [(ngModel)]=\"form.longitude\" />\r\n                                </div> -->\r\n                                <!-- <div *ngIf=\"map_active\"> -->\r\n                                    <!-- <label *ngIf=\"!showMap\" style=\"font-weight: bolder;\">Set This Location As Your Pickup Point ? <span style=\"color:red \">(Ignore this option If you don't want to set any address by map)</span></label>\r\n                                    <label *ngIf=\"showMap\" style=\"font-weight: bolder; color:green\">Your Pick-up Point Has Been Set</label>\r\n                                    \r\n                                    <br/> -->\r\n                                   \r\n                                <!-- </div> -->\r\n                            </div>\r\n                            <div class=\"col-md-6\" [class.display-none]=\"!map_active\">\r\n                                \r\n                                <label style=\"font-weight: bolder; color:green\">CURRENT LOCATION <span *ngIf=\"showMap\">( SET )</span></label>\r\n                               <br/>\r\n                                <span>Lat : {{latitude}}</span> <span>  Long : {{longitude}}</span>\r\n                                <label>{{address}}</label>\r\n                                <agm-map [latitude]=\"latitude\" [longitude]=\"longitude\" [zoom]=\"zoom\">\r\n                                    <agm-marker [latitude]=\"latitude\" [longitude]=\"longitude\" [markerDraggable]=\"true\"\r\n                                        (dragEnd)=\"markerDragEnd($event)\"></agm-marker>\r\n                                </agm-map>\r\n                                <div style=\"margin-top:20px;display:flex; justify-content:space-between; align-items:center; width:100%;\">\r\n                                <button class=\"check-button common-button blue-fill\"\r\n                                (click)=\"showMaps()\">Set Location</button>\r\n                                <button class=\"common-button btn-danger\"\r\n                                (click)=\"closeMaps()\">Remove Location</button> \r\n                            </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-12\">\r\n                    <h3 class=\"title-diff\">Store Images</h3>\r\n                    <div class=\"image-upload-group\">\r\n                      \r\n                        <div class=\"img-upload\">\r\n                            <div class=\"card\">\r\n                                <div class=\"card-title\">\r\n                                    <h2> Logo</h2>\r\n                                </div>\r\n                                <div class=\"card-content\" *ngIf=\"!form.image_owner_url\">\r\n                                    <div class=\"image\">\r\n                                        <div class=\"card-content custom\">\r\n                                            <div>\r\n                                                <img *ngIf=\"form.image_owner_url\" class=\"resize\"\r\n                                                    src=\"{{form.image_owner_url}}\" />\r\n                                                <img *ngIf=\"!form.image_owner_url\" class=\"set-opacity\"\r\n                                                    src=\"/assets/images/cloud.png\" height=\"200\" width=\"200\"> <br />\r\n                                            </div>\r\n                                            <div class=\"browse-files\">\r\n                                                <div class=\"pb-framer\">\r\n                                                    <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                        [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                                    </div>\r\n                                                    <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                                    <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n                                                </div>\r\n\r\n                                                <button class=\"common-button blue-outline fileupload\"\r\n                                                    (click)=\"inputFile2.click()\">Browse File</button>\r\n                                                <br>\r\n                                                <br>\r\n                                                <!-- <span style=\"color:red; font-size: 10px;\">*Requried file size max. 5MB\r\n                                                    and file format must be .JPG/.JPEG/.PNG/</span> -->\r\n                                                <!-- <button class=\"btn rounded-btn\" (click)=\"onUploadLogo()\">Upload</button>\r\n                                                <button class=\"btn rounded-btn-danger\"\r\n                                                    (click)=\"cancelThis()\">Cancel</button> -->\r\n                                            </div>\r\n\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                    <div *ngIf=\"errorLabel!==false\">\r\n                                        {{errorLabel}}\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"form-group uploaded\" *ngIf=\"form.image_owner_url\">\r\n                                    <div class=\"uploaded-image\" (click)=\"inputFile2.click()\">\r\n                                        <div class=\"img\"\r\n                                            [ngStyle]=\"{'background-image':'url('+(form.image_owner_url)+')'}\">\r\n                                            <div class=\"editor-tool\">\r\n                                                <i class=\"fa fa-pen\"></i>\r\n                                            </div>\r\n                                            <!-- <img class=\"resize\" src=\"{{form.image_owner_url}}\"> -->\r\n                                        </div>\r\n                                        <!-- <a class=\"common-button blue-outline\" (click)=\"inputFile2.click()\">Re-Upload\r\n                                            Image</a> -->\r\n                                    </div>\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"img-upload\">\r\n                            <div class=\"card\">\r\n                                <div class=\"card-title\">\r\n                                    <h2>Background</h2>\r\n                                </div>\r\n                                <div class=\"card-content\" *ngIf=\"!form.background_image\">\r\n                                    <div class=\"image\">\r\n                                        <div class=\" custom\">\r\n                                            <div>\r\n                                                <img *ngIf=\"form.image_owner_url\" class=\"resize\"\r\n                                                    src=\"{{form.background_image}}\" />\r\n                                                <img *ngIf=\"!form.image_owner_url\" class=\"set-opacity\"\r\n                                                    src=\"/assets/images/cloud.png\"> <br />\r\n                                            </div>\r\n                                            <div class=\"browse-files\">\r\n                                                <div class=\"pb-framer\">\r\n                                                    <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                        [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                                    </div>\r\n                                                    <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                                    <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n                                                </div>\r\n\r\n                                                <button class=\"common-button blue-outline fileupload\"\r\n                                                    (click)=\"inputFile1.click()\">Browse File</button>\r\n\r\n                                                <br>\r\n                                                <br>\r\n                                                <!-- <span style=\"color:red; font-size: 10px;\">*Requried file size max. 5MB\r\n                                                    and\r\n                                                    file format must be .JPG/.JPEG/.PNG/</span> -->\r\n\r\n                                                <!-- <input type=\"file\" (change)=\"onFileSelected($event)\" accept=\"image/*\">\r\n                                                <br>\r\n                                                <button class=\"btn rounded-btn\"\r\n                                                    (click)=\"onUploadBackground()\">Upload</button>\r\n                                                <button class=\"btn rounded-btn-danger\"\r\n                                                    (click)=\"cancelThis()\">Cancel</button> -->\r\n                                            </div>\r\n\r\n                                        </div>\r\n\r\n\r\n                                        <!-- {{form.value | json}}  -->\r\n\r\n                                    </div>\r\n                                    <div *ngIf=\"errorLabel!==false\">\r\n                                        {{errorLabel}}\r\n                                    </div>\r\n\r\n\r\n                                </div>\r\n                                <div class=\"form-group uploaded\" *ngIf=\"form.background_image\">\r\n                                    <div class=\"uploaded-image\" (click)=\"inputFile1.click()\">\r\n                                        <div class=\"img\"\r\n                                            [ngStyle]=\"{'background-image':'url('+(form.background_image)+')'}\">\r\n                                            <div class=\"editor-tool\">\r\n                                                <i class=\"fa fa-pen\"></i>\r\n                                            </div>\r\n                                            <!-- <img class=\"resize\" src=\"{{form.image_owner_url}}\"> -->\r\n                                        </div>\r\n                                        <!-- <a class=\"common-button blue-outline\" (click)=\"inputFile2.click()\">Re-Upload\r\n                                            Image</a> -->\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"form-group uploaded\" *ngIf=\"form.background_image\">\r\n                                    <div class=\"uploaded-image\" (click)=\"inputFile1.click()\">\r\n                                        <div class=\"img\"\r\n                                            [ngStyle]=\"{'background_image':'url('+(form.background_image+form.pic_big_path)+')'}\">\r\n                                            <div class=\"editor-tool\">\r\n                                                <i class=\"fa fa-pen\"></i>\r\n                                            </div>\r\n                                            <img class=\"resize\" src=\"{{form.background_image}}\">\r\n                                        </div>\r\n                                        <a class=\"text-fileupload\" (click)=\"inputFile1.click()\">Re-Upload Image</a>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <button class=\"btn btn-primary\" (click)=\"addMerchant()\">Add Merchant</button> -->\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                    <div>\r\n                        <div class=\"notif error-message\">{{errorMessage}}</div>\r\n\r\n                        <button class=\"common-button blue-fill save float-right\" (click)=\"saveMerchant()\">Save\r\n                            Profile</button>\r\n                    </div>\r\n                </div>\r\n\r\n            </mat-tab>\r\n            <mat-tab label=\"Personal Information\">\r\n                <h3 class=\"title-diff\">Personal Detail</h3>\r\n                <div class=\"row edit or-group\">\r\n                    <div class=\"col-md-6\">\r\n                        <div class=\"form-group\">\r\n                            <label>Full Name</label>\r\n                            <input class=\"form-control\" (focus)=\"editableForm('full_name', true)\"\r\n                                (blur)=\"editableForm('full_name', false)\" type=\"text\" name=\"full_name\"\r\n                                [(ngModel)]=\"form.full_name\" />\r\n                            <div *ngIf=\"!formEditable.full_name\" class=\"iconic\"\r\n                                (click)=\"editableForm('full_name', true)\"><i class=\"fa fa-pen\"></i></div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <label>Date Of Birth {{form.dob}}</label>\r\n                            <input class=\"form-control\" (focus)=\"editableForm('dob', true)\"\r\n                                (blur)=\"editableForm('dob', false)\"  type=\"date\" name=\"dob\" (ngModel)=\"form.dob\"  [value]=\"dateformater(form.dob)\"/>\r\n                            <div *ngIf=\"!formEditable.dob\" class=\"iconic\" (click)=\"editableForm('dob', true)\"><i\r\n                                    class=\"fa fa-pen\"></i></div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <label>Email</label>\r\n                            <input class=\"form-control\" (focus)=\"editableForm('email', true)\"\r\n                                (blur)=\"editableForm('email', false)\" type=\"text\" name=\"email\"\r\n                                [(ngModel)]=\"form.email\" />\r\n                            <div *ngIf=\"!formEditable.email\" class=\"iconic\" (click)=\"editableForm('email', true)\"><i\r\n                                    class=\"fa fa-pen\"></i></div>\r\n                        </div>\r\n\r\n                    </div>\r\n                    <div class=\"col-md-6\">\r\n                        <div class=\"form-group\">\r\n                            <label> gender</label>\r\n                            <div *ngIf=\"!formEditable.gender\" class=\"form-control\"\r\n                                (click)=\"editableForm('gender', true)\" type=\"text\" name=\"gender\">{{form.gender}}</div>\r\n                            <select class=\"form-control\" (focus)=\"editableForm('gender', true)\"\r\n                                (blur)=\"editableForm('gender', false)\" [(ngModel)]=\"form.gender\"\r\n                                *ngIf=\"formEditable.gender\">\r\n                                <option [ngValue]=\"'male'\">Male</option>\r\n                                <option [ngValue]=\"'female'\">Female</option>\r\n                            </select>\r\n\r\n                            <div *ngIf=\"!formEditable.gender\" class=\"iconic\" (click)=\"editableForm('gender', true)\"><i\r\n                                    class=\"fa fa-pen\"></i></div>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label>Cell Phone (WhatsApp)</label>\r\n                            <input class=\"form-control\" (focus)=\"editableForm('cell_phone', true)\"\r\n                                (blur)=\"editableForm('cell_phone', false)\" type=\"text\" name=\"full_name\"\r\n                                [(ngModel)]=\"form.cell_phone\" />\r\n                            <div *ngIf=\"!formEditable.cell_phone\" class=\"iconic\"\r\n                                (click)=\"editableForm('cell_phone', true)\">\r\n                                <i class=\"fa fa-pen\"></i></div>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label>address*</label>\r\n                            <textarea class=\"form-control\" (focus)=\"editableForm('address', true)\"\r\n                                (blur)=\"editableForm('address', false)\" type=\"text\" name=\"address\"\r\n                                [(ngModel)]=\"form.address\"></textarea>\r\n                            <div *ngIf=\"!formEditable.address\" class=\"iconic\" (click)=\"editableForm('address', true)\"><i\r\n                                    class=\"fa fa-pen\"></i></div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div>\r\n                    <div class=\"notif error-message\">{{errorMessage}}</div>\r\n                    <button class=\"common-button blue-fill save float-right\" (click)=\"saveMerchant()\">Save\r\n                        Profile</button>\r\n                </div>\r\n                <br />\r\n                <br />\r\n\r\n                <!-- <div class=\"col-md-12\"> -->\r\n                <h4 class=\"title-diff\">Security</h4>\r\n                <!-- <div id=\"line\"></div> -->\r\n                <div class=\"row\">\r\n                    <img src=\"assets/images/padlock.png\" class=\"shield\">\r\n                    <div class=\"security-content\">\r\n                        <h5>My Pin</h5>\r\n                        <span *ngIf=\"form.access_pin != 'enabled'\">You don't have any PIN. Lets make one!</span>\r\n                        <span *ngIf=\"form.access_pin == 'enabled'\"\r\n                            style=\"color: gray;font-size: 20px;\">******</span><br />\r\n                        <button class=\"btn btn-primary\" (click)=\"openDialog();\">\r\n                            <span *ngIf=\"form.access_pin != 'enabled'\">Make PIN</span>\r\n                            <span *ngIf=\"form.access_pin == 'enabled'\">Change PIN</span>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n                <!-- </div> -->\r\n\r\n                <!-- <div id=\"myModal\" class=\"modal {{othersForm}}\">\r\n                    <div class=\"modal-content\">\r\n                        <div class=\"modal-header\">\r\n                            <span class=\"close\" (click)=\"closeDialog()\">&times;</span>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <h3>Register Pin</h3>\r\n                            <span><strong> Input 6-Digit number. Remember, don't use predictable combinations of numbers\r\n                                    like date of birth, etc</strong></span>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <div class=\"form-group\">\r\n\r\n                                <input placeholder=\"Password\" (click)=\"errorMessage = false\" [type]=\"hide ? 'password' : 'text'\"\r\n                            [(ngModel)]=\"form.password\" name=\"password\">\r\n                        <button mat-icon-button matSuffix (click)=\"hide = !hide\" [attr.aria-label]=\"'Hide password'\"\r\n                            [attr.aria-pressed]=\"hide\" style=\"background:transparent; border: none;\">\r\n                            <mat-icon >{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                            </button> \r\n\r\n                                <div class=\"wrapper-input\">\r\n                                    <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.new_pin\" size=\"6\"\r\n                                        maxlength=\"6\" pattern=\"[0-9]\" [type]=\"hide ? 'password': 'text'\"\r\n                                        inputmode=\"numeric\">\r\n                                    <button mat-icon-button matSuffix (click)=\"hide = !hide\"\r\n                                        [attr.aria-label]=\"'Hide password'\" [attr.aria-pressed]=\"hide\">\r\n                                        <mat-icon>{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                                    </button>\r\n                                </div>\r\n                                <button class=\"btn btn-primary\" type=\"submit\" id=\"button-verify\"\r\n                                    (click)=\"verify()\">Verify</button>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"modal-footer\">\r\n\r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n\r\n                <div id=\"myModal\" class=\"modal {{setPin}}\">\r\n                    <div *ngIf=\"!successPin\" class=\"modal-content\">\r\n                        <div class=\"modal-header\">\r\n                            <span class=\"close\" (click)=\"closeDialog()\" style=\"color: black;\">&times;</span>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <h3 *ngIf=\"form.access_pin != 'enabled'\">Register Pin</h3>\r\n                            <h3 *ngIf=\"form.access_pin == 'enabled'\">Reset Pin</h3>\r\n                            <span><strong> Input 6-Digit number. Remember, don't use predictable combinations of numbers\r\n                                    like date of birth, etc</strong></span>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <div class=\"form-group\">\r\n                                <label>Insert PIN</label>\r\n                                <!-- <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.new_pin\"> -->\r\n                                <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.new_pin\" size=\"15\"\r\n                                    maxlength=\"6\" pattern=\"[0-9]\" [type]=\"hide ? 'password': 'text'\" inputmode=\"numeric\"\r\n                                    autocomplete=\"off\">\r\n                                <label>Retype PIN</label>\r\n                                <!-- <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.repeat_new_pin\"> -->\r\n                                <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.repeat_new_pin\" size=\"6\"\r\n                                    maxlength=\"6\" pattern=\"[0-9]\" [type]=\"hide ? 'password': 'text'\" inputmode=\"numeric\"\r\n                                    autocomplete=\"off\">\r\n                                <label>Password</label>\r\n                                <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.password\"\r\n                                    autocomplete=\"off\">\r\n                                <!-- <button mat-icon-button matSuffix (click)=\"hide = !hide\"\r\n                            [attr.aria-label]=\"'Hide password'\" [attr.aria-pressed]=\"hide\">\r\n                            <mat-icon>{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                        </button> -->\r\n                                <button class=\"btn btn-primary\" id=\"button-verify\" (click)=\"verify()\">Verify</button>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"modal-footer\">\r\n\r\n                        </div>\r\n                    </div>\r\n                    <div *ngIf=\"successPin\" class=\"modal-content\">\r\n                        <div class=\"modal-header\">\r\n                            <span class=\"close\" (click)=\"closeDialog()\" style=\"color: black;\">&times;</span>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <img src=\"assets/images/check.png\" class=\"shield\">\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n\r\n\r\n                <!-- <div id=\"myModal\" class=\"modal {{donePin}}\">\r\n                    <div class=\"modal-content\">\r\n                        <div class=\"modal-header\">\r\n                            <span class=\"close\" (click)=\"closeDialog()\">&times;</span>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <h3>Retype your PIN</h3>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <div class=\"form-group\">\r\n                            <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.repeat_new_pin\">\r\n                            <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.password\">\r\n                            <button mat-icon-button matSuffix (click)=\"hide = !hide\"\r\n                            [attr.aria-label]=\"'Hide password'\" [attr.aria-pressed]=\"hide\">\r\n                            <mat-icon>{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                        </button>\r\n                            <button class=\"btn btn-primary\" id=\"button-verify\" (click)=\"verify()\">Verify</button>\r\n                        </div>\r\n                    </div>\r\n                        <div class=\"modal-footer\">\r\n                \r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n                <!-- <div id=\"myModal\" class=\"modal {{retypePin}}\">\r\n                    <div class=\"modal-content\">\r\n                        <div class=\"modal-header\">\r\n                            <span class=\"close\" (click)=\"closeDialog()\">&times;</span>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <h3>Retype your PIN</h3>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <div class=\"form-group\">\r\n                                <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.repeat_new_pin\" size=\"6\"\r\n                                    maxlength=\"6\">\r\n                                <label>Input Your Password</label>\r\n                                <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.password\"> -->\r\n                <!-- <button mat-icon-button matSuffix (click)=\"hide = !hide\"\r\n                            [attr.aria-label]=\"'Hide password'\" [attr.aria-pressed]=\"hide\">\r\n                            <mat-icon>{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                        </button> -->\r\n                <!-- <button class=\"btn btn-primary\" id=\"button-verify\" (click)=\"verify()\">Verify</button>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"modal-footer\">\r\n\r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n\r\n                <!-- <div id=\"myModal\" class=\"modal {{passwordField}}\">\r\n                    <div class=\"modal-content\">\r\n                        <div class=\"modal-header\">\r\n                            <span class=\"close\" (click)=\"closeDialog()\">&times;</span>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <h3>Enter Your Password</h3>\r\n                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <div class=\"form-group\">\r\n                                <input type=\"password\" class=\"form-control\" [(ngModel)]=\"pin.repeat_new_pin\"> -->\r\n                <!-- <button mat-icon-button matSuffix (click)=\"hide = !hide\"\r\n                            [attr.aria-label]=\"'Hide password'\" [attr.aria-pressed]=\"hide\">\r\n                            <mat-icon>{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                        </button> -->\r\n                <!-- <button class=\"btn btn-primary\" id=\"button-verify\" (click)=\"verify()\">Verify</button>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"modal-footer\">\r\n\r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n\r\n\r\n\r\n\r\n            </mat-tab>\r\n            <mat-tab label=\"Payment Info\">\r\n                <h3 class=\"title-diff\">Payment Information</h3>\r\n                <div class=\"row edit or-group\" *ngIf=\"form.merchant_mdr\">\r\n\r\n                    <div class=\"col-md-6\">\r\n                        <div class=\"form-group\">\r\n                            <label>Your MDR (Merchant Discount Rate)</label>\r\n                            <div *ngIf=\"form.merchant_mdr.mdr_type=='percentage'\">\r\n                                {{form.merchant_mdr.mdr_value}}%</div>\r\n                            <div *ngIf=\"form.merchant_mdr.mdr_type=='fixed_rate'\">\r\n                                {{form.merchant_mdr.mdr_value| currency: 'Rp '}} </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <h3 class=\"title-diff\">Withdrawal info</h3>\r\n                <div class=\"row edit or-group\">\r\n                    <div class=\"col-md-6\">\r\n                        <div class=\"form-group\">\r\n                            <label> Bank Account </label>\r\n                            <select class=\"form-control\" [(ngModel)]=\"form.withdrawal_bank_name\">\r\n                                <option *ngFor=\"let f of bankList\" [ngValue]=\"f.value\">{{f.name}}</option>\r\n                            </select>\r\n                        </div>\r\n\r\n                    </div>\r\n                    <div class=\"col-md-6\">\r\n                        <div class=\"form-group\">\r\n                            <label>Account Name</label>\r\n                            <input class=\"form-control\" (focus)=\"editableForm('withdrawal_account_name',true)\"\r\n                                (blur)=\"editableForm('withdrawal_account_name', false)\" type=\"text\" name=\"store name\"\r\n                                (input)=\"nameInput($event)\" [(ngModel)]=\"form.withdrawal_account_name\" />\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-6\">\r\n                        <div class=\"form-group\">\r\n                            <label>Account Number</label>\r\n                            <input class=\"form-control\" (focus)=\"editableForm('withdrawal_bank_account',true)\"\r\n                                (blur)=\"editableForm('withdrawal_bank_account', false)\" type=\"text\" name=\"store name\"\r\n                                (input)=\"nameInput($event)\" [(ngModel)]=\"form.withdrawal_bank_account\" />\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div>\r\n                    <div class=\"notif error-message\">{{errorMessage}}</div>\r\n                    <button class=\"common-button blue-fill save float-right\" (click)=\"saveMerchant()\">Save\r\n                        Profile</button>\r\n                </div>\r\n            </mat-tab>\r\n\r\n        </mat-tab-group>\r\n    </div>\r\n    <!-- <input #fileinput type=\"file\" (change)=\"onFileSelected($event)\" [(ngModel)]=\"selFile\" accept=\"image/png, image/jpeg, image/jpg\"> -->\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/merchant-products.component.html":
/*!***************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/separated-modules/merchant-products/merchant-products.component.html ***!
  \***************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12\"><h2><strong>PRODUCT LIST</strong></h2></div>\r\n<div class=\"col-md-12 col-xs-12\">\r\n  <div class=\"card\">\r\n    <div class=\"innner-card\">\r\n<div class=\"nav toolbar\">\r\n  \r\n  <div class=\"container-swapper-option\">\r\n    <span style=\"margin-right: 10px;\">View  </span> \r\n    <div class=\"button-container\">\r\n  <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n    <span class=\"fa fa-grip-horizontal\"></span>\r\n  </button>\r\n  <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n    <span class=\"fa fa-table\"></span>\r\n  </button>\r\n</div>\r\n</div>\r\n<div class=\"option-container\">\r\n<div class=\"search\">\r\n  <span> Search </span>\r\n<input id=\"search_text\" type=\"text\" (keyup)=\"search($event)\" placeholder=\"All Products\" >\r\n</div>\r\n\r\n</div>\r\n<div class=\"option-container\">\r\n  <div class=\"search\">\r\n    <select  [(ngModel)]=\"orderBy\" (change)=\"orderBySelected()\">\r\n      <option *ngFor=\"let s of sortBy\" [ngValue]=\"s.value\">{{s.name}}</option>\r\n    </select> \r\n  </div>\r\n  <div class=\"search\">\r\n    <select  [(ngModel)]=\"filteredBy\" (change)=\"orderBySelected()\">\r\n      <option *ngFor=\"let s of filterBy\" [ngValue]=\"s.value\">{{s.name}}</option>\r\n    </select>\r\n  </div>\r\n  <button class=\"common-button blue-fill\" style=\"background-color: #007bff;\" (click)=\"addProduct()\">\r\n \r\n    Add Product\r\n    <span class=\"fa fa-plus\"></span>\r\n  </button>\r\n  <button class=\"common-button blue-fill\" style=\"background-color: #007bff;\" (click)=\"addProductBulk()\">\r\n \r\n    Add Products (Bulk)\r\n    <!-- <span class=\"fa fa-plus\"></span> -->\r\n  </button>\r\n</div> \r\n</div>\r\n\r\n<div class=\"_container\">\r\n  <div *ngIf=\"swaper\" class=\"grid-view\">\r\n    <div class=\"form-group uploaded\" *ngIf=\"loading\">\r\n      <div class=\"loading\">\r\n          <div class=\"sk-fading-circle\">\r\n              <div class=\"sk-circle1 sk-circle\"></div>\r\n              <div class=\"sk-circle2 sk-circle\"></div>\r\n              <div class=\"sk-circle3 sk-circle\"></div>\r\n              <div class=\"sk-circle4 sk-circle\"></div>\r\n              <div class=\"sk-circle5 sk-circle\"></div>\r\n              <div class=\"sk-circle6 sk-circle\"></div>\r\n              <div class=\"sk-circle7 sk-circle\"></div>\r\n              <div class=\"sk-circle8 sk-circle\"></div>\r\n              <div class=\"sk-circle9 sk-circle\"></div>\r\n              <div class=\"sk-circle10 sk-circle\"></div>\r\n              <div class=\"sk-circle11 sk-circle\"></div>\r\n              <div class=\"sk-circle12 sk-circle\"></div>\r\n          </div>\r\n      </div>\r\n  </div>\r\n   <div *ngIf=\"!loading\">\r\n    <div class=\"col-md-2\" *ngFor=\"let c of Products\" (click)=\"openDetail(c._id)\">\r\n      <div class=\"card\">\r\n      <div class=\"img-wrapper\">\r\n        <div class=\"img\" [ngStyle]=\"{'background-image':'url('+(c.base_url+c.pic_medium_path)+')'}\">\r\n        </div>\r\n      </div>\r\n      <div class=\"info\">\r\n        <div class=\"text\">{{c.product_name}} </div>\r\n        <div class=\"price\">{{c.fixed_price | currency: 'Rp '}}</div>\r\n      </div>\r\n    </div>\r\n    </div></div>\r\n  </div>\r\n  <div *ngIf=\"swaper && Products\" class=\"grid-view-pagination\">\r\n    <ngb-pagination [collectionSize]=\"valueAll\" [(page)]=\"page\" [pageSize]=\"pageSize\" [maxSize]=\"5\" [rotate]=\"true\"\r\n      [ellipses]=\"true\" (pageChange)=\"loadPage($event)\"\r\n      (pageBoundsCorrection)=\"loadPage($event)\"\r\n>\r\n      <ng-template ngbPaginationFirst>&lt;&lt;</ng-template>\r\n      <ng-template ngbPaginationLast>&gt;&gt;</ng-template>\r\n      <ng-template ngbPaginationPrevious>&lt;</ng-template>\r\n      <ng-template ngbPaginationNext>&gt;</ng-template>\r\n    </ngb-pagination>\r\n  </div>\r\n  <div *ngIf='!swaper'>\r\n    <app-form-builder-table \r\n      [table_data]=\"Products\" \r\n      [searchCallback]=\"[service,'searchProductsReportLint',this]\"\r\n      [tableFormat]=\"tableFormat\"\r\n      [total_page]=\"totalPage\">\r\n    </app-form-builder-table>\r\n  </div>\r\n  \r\n</div>\r\n</div>\r\n</div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/statistics/statistics.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/statistics/statistics.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-order-history-summary [permissionType]=\"permissionType\"></app-order-history-summary>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/upgrade-genuine/upgrade-genuine.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/upgrade-genuine/upgrade-genuine.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input #inputFile4 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'npwp_image')\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n<input #inputFile3 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'id_image')\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n<input #inputFile1 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'siup_image')\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n<input #inputFile2 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'sampul_image')\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n<input #inputFile5 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'tdp_image')\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n<input #inputFile6 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'nib_image')\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n<input #inputFile7 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'domisili_image')\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n\r\n\r\n<!-- PAGE REGISTER MERCHANT UPGRADE -->\r\n<div *ngIf=\"!complete && !selectGenuine && !selectOfficial\" class=\"container-all\">\r\n    <div class=\"container-upgrade\">\r\n        <div class=\"upgrade-page-bg\">\r\n            <img src=\"/assets/images/upgrade-bg.png\">\r\n        </div>\r\n        <div class=\"upgrade-page-form\">\r\n            <div class=\"form-title\">\r\n                Do You Want To Upgrade Merchant?\r\n            </div>\r\n            <div class=\"upload-form\">\r\n                <div>\r\n                    <label style=\"text-align: center;color:darkblue;width: 100%;\">It's Super Easy! Just Click Upgrade in\r\n                        the below</label>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-6\">\r\n                        <div class=\"card cl-list\">\r\n                            <div class=\"card-header genuine\">\r\n                                Genuine Merchant\r\n                            </div>\r\n                            <div class=\"card-body body-area\">\r\n                                <div class=\"card-body-image\">\r\n                                    <img src=\"/assets/images/best-seller.png\" height=\"53px\">\r\n                                </div>\r\n                                <div class=\"center-text\"> Required Document</div>\r\n\r\n                                <ul>\r\n                                    <li>Fotocopy Kitas / KTP Pengurus</li>\r\n                                    <li>NPWP (Nomor Pokok Wajib Pajak)</li>\r\n                                </ul>\r\n                                <div class=\"button-container\">\r\n                                    <button class=\"btn btn-1 genuine\" (click)=\"genuineClick()\">\r\n                                        Upgrade\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-6\">\r\n                        <div class=\"card cl-list\">\r\n                            <div class=\"card-header official\">\r\n                                Official Merchant\r\n                            </div>\r\n                            <div class=\"card-body body-area\">\r\n                                <div class=\"card-body-image\">\r\n                                    <img src=\"/assets/images/official_merchant.png\" height=\"53px\">\r\n                                </div>\r\n                                <div class=\"center-text\"> Required Document</div>\r\n                                <ul>\r\n                                    <li>Fotokopy Kitas / KTP pengurus</li>\r\n                                    <li>SIUP (Surat Izin Usaha Perdagangan)</li>\r\n                                    <li>TDP (Tanda Daftar Perusahaan)</li>\r\n                                    <li>NIB (Nomor Induk Berusaha)</li>\r\n                                    <li>NPWP (Nomor Pokok Wajib Pajak)</li>\r\n                                    <li>Surat Keterangan Domisili Perusahaan</li>\r\n                                    <li>Sampul Buku Rekening Tabungan</li>\r\n                                </ul>\r\n                                <div class=\"button-container\">\r\n                                    <button class=\"btn btn-1 official\" (click)=\"officialClick()\">\r\n                                        Upgrade\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n<!-- END OF PAGE REGISTER UPGRADE MERCHANT-->\r\n\r\n\r\n\r\n<!-- PAGE REGISTER FOR GENUINE MERCHANT -->\r\n<div *ngIf=\"!complete && selectGenuine && !selectOfficial\" class=\"container-all\">\r\n    <div class=\"col-md-12\">\r\n        <div class=\"title\">\r\n            <img src=\"assets/images/best-seller.png\">\r\n            <span class=\"text\"> Genuine Merchant</span>\r\n        </div>\r\n        <div class=\"section\">\r\n\r\n            <div class=\"section-header\">\r\n                <span class=\"section-title\">Please upload your Legal Documents</span>\r\n            </div>\r\n            <div class=\"section-upload row\">\r\n                <div class=\"col-md-4\">\r\n                    Fotocopy Kitas / KTP Pengurus\r\n\r\n                    <div class=\"card cl-list\">\r\n                        <div class=\"card-content\" *ngIf=\"!form.id_image && !showLoading\">\r\n                            <div class=\"image\">\r\n                                <div class=\"card-content custom\">\r\n                                    <div>\r\n                                        <img *ngIf=\"!form.id_image\" class=\"logo-upload\" src=\"/assets/images/upload.svg\">\r\n                                    </div>\r\n                                    <p class=\"upload-desc\">Upload your image here <br>or</p>\r\n                                    <div class=\"browse-files\">\r\n                                        <div class=\"pb-framer\">\r\n                                            <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                            </div>\r\n                                            <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                            <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n                                        </div>\r\n\r\n                                        <button class=\"common-button blue-fill fileupload\"\r\n                                            (click)=\"inputFile3.click()\">Browse File</button>\r\n\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"showLoading\">\r\n                            <div class=\"loading\">\r\n                                <div class=\"sk-fading-circle\">\r\n                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"form.id_image && !showLoading\">\r\n                            <div class=\"uploaded-image\">\r\n                                <div class=\"img\">\r\n                                    <img class=\"resize\" src=\"{{form.id_image}}\">\r\n\r\n                                </div>\r\n\r\n                            </div>\r\n                            <a class=\"common-button blue-fill fileupload\" (click)=\"inputFile3.click()\"> Re-Upload</a>\r\n                        </div>\r\n\r\n\r\n\r\n                    </div>\r\n\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    NPWP ( Nomor Pokok Wajib Pajak)\r\n                    <div class=\"card cl-list\">\r\n\r\n                        <div class=\"card-content\" *ngIf=\"!form.npwp_image && !showLoading2\">\r\n                            <div class=\"image\">\r\n                                <div class=\"card-content custom\">\r\n                                    <div>\r\n\r\n                                        <img *ngIf=\"!form.npwp_image\" class=\"logo-upload\"\r\n                                            src=\"/assets/images/upload.svg\">\r\n                                    </div>\r\n                                    <p class=\"upload-desc\">Upload your image here <br> or</p>\r\n                                    <div class=\"browse-files\">\r\n                                        <div class=\"pb-framer\">\r\n                                            <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                            </div>\r\n                                            <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                            <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n                                        </div>\r\n\r\n                                        <button class=\"common-button blue-fill fileupload\"\r\n                                            (click)=\"inputFile4.click()\">Browse File</button>\r\n\r\n\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"showLoading2\">\r\n                            <div class=\"loading\">\r\n                                <div class=\"sk-fading-circle\">\r\n                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"form.npwp_image && !showLoading2\">\r\n                            <div class=\"uploaded-image\">\r\n                                <div class=\"img\">\r\n                                    <img class=\"resize\" src=\"{{form.npwp_image}}\">\r\n                                 \r\n                                </div>\r\n\r\n                            </div>\r\n                            <a class=\"common-button blue-fill fileupload\" (click)=\"inputFile4.click()\">Re-Upload</a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"save-container\">\r\n        <button class=\"common-button save\" (click)=\"saveProfile()\">Upgrade Now</button>\r\n    </div>\r\n</div>\r\n<!-- END OF PAGE REGISTER FOR GENUINE MERCHANT -->\r\n\r\n<!-- PAGE REGISTER FOR Official MERCHANT -->\r\n<div *ngIf=\"!complete && !selectGenuine && selectOfficial\" class=\"container-all\">\r\n    <div class=\"col-md-12\">\r\n        <div class=\"title\">\r\n            <img src=\"assets/images/official_merchant.png\">\r\n            <span class=\"text\"> Official Merchant</span>\r\n        </div>\r\n        <div class=\"section\">\r\n\r\n            <div class=\"section-header\">\r\n                <span class=\"section-title\">Please upload your Legal Documents</span>\r\n            </div>\r\n            <div class=\"section-upload row left-section-upload\">\r\n                <div class=\"col-md-3\">\r\n                    Fotocopy Kitas / KTP Pengurus\r\n\r\n                    <div class=\"card cl-list\">\r\n                        <div class=\"card-content\" *ngIf=\"!form.id_image && !showLoading\">\r\n                            <div class=\"image\">\r\n                                <div class=\"card-content custom\">\r\n                                    <div>\r\n\r\n                                        <img *ngIf=\"!form.id_image\" class=\"logo-upload\"\r\n                                            src=\"/assets/images/upload.svg\">\r\n                                    </div>\r\n                                    <p class=\"upload-desc\">Upload your image here <br> or</p>\r\n                                    <div class=\"browse-files\">\r\n                                        <div class=\"pb-framer\">\r\n                                            <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                            </div>\r\n                                            <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                            <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n                                        </div>\r\n\r\n                                        <button class=\"common-button blue-fill fileupload\"\r\n                                            (click)=\"inputFile3.click()\">Browse File</button>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"showLoading\">\r\n                            <div class=\"loading\">\r\n                                <div class=\"sk-fading-circle\">\r\n                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"form.id_image && !showLoading\">\r\n                            <div class=\"uploaded-image\">\r\n                                <div class=\"img\">\r\n                                    <img class=\"resize\" src=\"{{form.id_image}}\">\r\n                                    \r\n                                </div>\r\n\r\n                            </div>\r\n                            <a class=\"common-button blue-fill fileupload\" (click)=\"inputFile3.click()\">Re-Upload</a>\r\n                        </div>\r\n                    </div>\r\n\r\n                </div>\r\n                <div class=\"col-md-3\">\r\n                    NPWP ( Nomor Pokok Wajib Pajak)\r\n                    <div class=\"card cl-list\">\r\n                        <div class=\"card-content\" *ngIf=\"!form.npwp_image && !showLoading2\">\r\n                            <div class=\"image\">\r\n                                <div class=\"card-content custom\">\r\n                                    <div>\r\n\r\n                                        <img *ngIf=\"!form.npwp_image\" class=\"logo-upload\"\r\n                                            src=\"/assets/images/upload.svg\">\r\n                                    </div>\r\n                                    <p class=\"upload-desc\">Upload your image here <br> or</p>\r\n                                    <div class=\"browse-files\">\r\n                                        <div class=\"pb-framer\">\r\n                                            <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                            </div>\r\n                                            <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                            <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n                                        </div>\r\n\r\n                                        <button class=\"common-button blue-fill fileupload\"\r\n                                            (click)=\"inputFile4.click()\">Browse File</button>\r\n\r\n\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"showLoading2\">\r\n                            <div class=\"loading\">\r\n                                <div class=\"sk-fading-circle\">\r\n                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"form.npwp_image && !showLoading2\">\r\n                            <div class=\"uploaded-image\">\r\n                                <div class=\"img\">\r\n                                    <img class=\"resize\" src=\"{{form.npwp_image}}\">\r\n                                   \r\n                                </div>\r\n\r\n                            </div>\r\n                            <a class=\"common-button blue-fill fileupload\" (click)=\"inputFile4.click()\">Re-Upload</a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-3\">\r\n                    SIUP ( Surat Izin Usaha Perdagangan)\r\n                    <div class=\"card cl-list\">\r\n                        <div class=\"card-content\" *ngIf=\"!form.siup_image && !showLoading3\">\r\n                            <div class=\"image\">\r\n                                <div class=\"card-content custom\">\r\n                                    <div>\r\n                                        <img *ngIf=\"!form.siup_image\" class=\"logo-upload\"\r\n                                            src=\"/assets/images/upload.svg\">\r\n                                    </div>\r\n                                    <p class=\"upload-desc\">Upload your image here <br> or</p>\r\n                                    <div class=\"browse-files\">\r\n                                        <div class=\"pb-framer\">\r\n                                            <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                            </div>\r\n                                            <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                            <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                        </div>\r\n                                        <button class=\"common-button blue-fill fileupload\"\r\n                                            (click)=\"inputFile1.click()\">Browse File</button>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"showLoading3\">\r\n                            <div class=\"loading\">\r\n                                <div class=\"sk-fading-circle\">\r\n                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"form.siup_image && !showLoading3\">\r\n                            <div class=\"uploaded-image\">\r\n                                <div class=\"img\">\r\n                                    <img class=\"resize\" src=\"{{form.siup_image}}\">\r\n                                   \r\n                                </div>\r\n\r\n                            </div>\r\n                            <a class=\"common-button blue-fill fileupload\" (click)=\"inputFile1.click()\">Re-Upload</a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-3\">\r\n                    Sampul Buku Rekening Tabungan\r\n                    <div class=\"card cl-list\">\r\n                        <div class=\"card-content\" *ngIf=\"!form.sampul_image && !showLoading4\">\r\n                            <div class=\"image\">\r\n                                <div class=\"card-content custom\">\r\n                                    <div>\r\n                                        <img *ngIf=\"!form.sampul_image\" class=\"logo-upload\"\r\n                                            src=\"/assets/images/upload.svg\">\r\n                                    </div>\r\n                                    <p class=\"upload-desc\">Upload your image here <br> or</p>\r\n                                    <div class=\"browse-files\">\r\n                                        <div class=\"pb-framer\">\r\n                                            <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                            </div>\r\n                                            <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                            <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                        </div>\r\n                                        <button class=\"common-button blue-fill fileupload\"\r\n                                            (click)=\"inputFile2.click()\">Browse File</button>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"showLoading4\">\r\n                            <div class=\"loading\">\r\n                                <div class=\"sk-fading-circle\">\r\n                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"form.sampul_image && !showLoading4\">\r\n                            <div class=\"uploaded-image\">\r\n                                <div class=\"img\">\r\n                                    <img class=\"resize\" src=\"{{form.sampul_image}}\">\r\n                                   \r\n                                </div>\r\n\r\n                            </div>\r\n                            <a class=\"common-button blue-fill fileupload\" (click)=\"inputFile2.click()\">Re-Upload</a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-3\">\r\n                    TDP ( Tanda Daftar Perusahaan)\r\n                    <div class=\"card cl-list\">\r\n                        <div class=\"card-content\" *ngIf=\"!form.tdp_image && !showLoading5\">\r\n                            <div class=\"image\">\r\n                                <div class=\"card-content custom\">\r\n                                    <div>\r\n                                        <img *ngIf=\"!form.tdp_image\" class=\"logo-upload\"\r\n                                            src=\"/assets/images/upload.svg\">\r\n                                    </div>\r\n                                    <p class=\"upload-desc\">Upload your image here <br> or</p>\r\n                                    <div class=\"browse-files\">\r\n                                        <div class=\"pb-framer\">\r\n                                            <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                            </div>\r\n                                            <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                            <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                        </div>\r\n                                        <button class=\"common-button blue-fill fileupload\"\r\n                                            (click)=\"inputFile5.click()\">Browse File</button>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"showLoading5\">\r\n                            <div class=\"loading\">\r\n                                <div class=\"sk-fading-circle\">\r\n                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"form.tdp_image && !showLoading5\">\r\n                            <div class=\"uploaded-image\">\r\n                                <div class=\"img\">\r\n                                    <img class=\"resize\" src=\"{{form.tdp_image}}\">\r\n                                   \r\n                                </div>\r\n\r\n                            </div>\r\n                            <a class=\"common-button blue-fill fileupload\" (click)=\"inputFile5.click()\">Re-Upload</a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-3\">\r\n                    NIB (Nomor Induk Berusaha)\r\n                    <div class=\"card cl-list\">\r\n                        <div class=\"card-content\" *ngIf=\"!form.nib_image && !showLoading6\">\r\n                            <div class=\"image\">\r\n                                <div class=\"card-content custom\">\r\n                                    <div>\r\n                                        <img *ngIf=\"!form.nib_image\" class=\"logo-upload\"\r\n                                            src=\"/assets/images/upload.svg\">\r\n                                    </div>\r\n                                    <p class=\"upload-desc\">Upload your image here <br> or</p>\r\n                                    <div class=\"browse-files\">\r\n                                        <div class=\"pb-framer\">\r\n                                            <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                            </div>\r\n                                            <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                            <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                        </div>\r\n                                        <button class=\"common-button blue-fill fileupload\"\r\n                                            (click)=\"inputFile6.click()\">Browse File</button>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"showLoading6\">\r\n                            <div class=\"loading\">\r\n                                <div class=\"sk-fading-circle\">\r\n                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"form.nib_image && !showLoading6\">\r\n                            <div class=\"uploaded-image\">\r\n                                <div class=\"img\">\r\n                                    <img class=\"resize\" src=\"{{form.nib_image}}\">\r\n                                   \r\n                                </div>\r\n\r\n                            </div>\r\n                            <a class=\"common-button blue-fill fileupload\" (click)=\"inputFile6.click()\">Re-Upload</a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-3\">\r\n                    Surat Keterangan Domisili Perusahaan\r\n                    <div class=\"card cl-list\">\r\n                        <div class=\"card-content\" *ngIf=\"!form.domisili_image && !showLoading7\">\r\n                            <div class=\"image\">\r\n                                <div class=\"card-content custom\">\r\n                                    <div>\r\n                                        <img *ngIf=\"!form.domisili_image\" class=\"logo-upload\"\r\n                                            src=\"/assets/images/upload.svg\">\r\n                                    </div>\r\n                                    <p class=\"upload-desc\">Upload your image here <br> or</p>\r\n                                    <div class=\"browse-files\">\r\n                                        <div class=\"pb-framer\">\r\n                                            <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                                [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                            </div>\r\n                                            <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                            <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                        </div>\r\n                                        <button class=\"common-button blue-fill fileupload\"\r\n                                            (click)=\"inputFile7.click()\">Browse File</button>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"showLoading7\">\r\n                            <div class=\"loading\">\r\n                                <div class=\"sk-fading-circle\">\r\n                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group uploaded\" *ngIf=\"form.domisili_image && !showLoading7\">\r\n                            <div class=\"uploaded-image\">\r\n                                <div class=\"img\">\r\n                                    <img class=\"resize\" src=\"{{form.domisili_image}}\">\r\n                                   \r\n                                </div>\r\n\r\n                            </div>\r\n                            <a class=\"common-button blue-fill fileupload\" (click)=\"inputFile7.click()\">Re-Upload</a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"save-container\">\r\n        <button class=\"common-button save\" (click)=\"saveProfile()\">Upgrade Now</button>\r\n    </div>\r\n</div>\r\n<!-- END OF PAGE REGISTER FOR Official MERCHANT -->\r\n\r\n<!-- PAGE SUCCESS AFTER REGISTERING -->\r\n<div *ngIf=\"complete\" class=\"container-all\">\r\n    <div class=\"container-upgrade\">\r\n        <div class=\"upgrade-page-bg\">\r\n            <img src=\"/assets/images/upgrade_success.png\">\r\n        </div>\r\n        <div class=\"upgrade-page-success\">\r\n            <img *ngIf=\"selectGenuine\" class=\"image-success\" src=\"/assets/images/doneupgrade.png\">\r\n            <img *ngIf=\"selectOfficial\" class=\"image-success\" src=\"/assets/images/official_merchant.png\">\r\n            <div class=\"header-message\">\r\n                <strong>Thank You For Registering !</strong>\r\n            </div>\r\n            <div class=\"body-message\">\r\n                Before you can officially become a <span *ngIf=\"selectGenuine\">genuine</span> <span\r\n                    *ngIf=\"selectOfficial\">official</span> merchant,\r\n                we must review your account first. After that, we will inform you again by email\r\n            </div>\r\n\r\n\r\n        </div>\r\n    </div>\r\n\r\n\r\n</div>\r\n<!-- END OF PAGE SUCCESS AFTER REGISTERING -->\r\n\r\n\r\n<div *ngIf=\"complete\" class=\"container-all\">\r\n\r\n    <div class=\"\">\r\n\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/router-master/router-master.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/router-master/router-master.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/component-libs/photo-profile/photo-profile.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/component-libs/photo-profile/photo-profile.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".photo-profile {\n  width: 100%;\n  height: 100%;\n  display: fl;\n  border-radius: 50%;\n  overflow: hidden;\n  color: #555;\n  position: relative;\n  background-color: #DFDFDF;\n}\n.photo-profile .img, .photo-profile .no-img {\n  position: relative;\n  width: 100%;\n  height: 100%;\n  background-size: cover;\n  justify-content: center;\n  align-items: center;\n  padding: 25px;\n  display: flex;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50LWxpYnMvcGhvdG8tcHJvZmlsZS9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxjb21wb25lbnQtbGlic1xccGhvdG8tcHJvZmlsZVxccGhvdG8tcHJvZmlsZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tcG9uZW50LWxpYnMvcGhvdG8tcHJvZmlsZS9waG90by1wcm9maWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FDQ0o7QURBSTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0FDRVIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnQtbGlicy9waG90by1wcm9maWxlL3Bob3RvLXByb2ZpbGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGhvdG8tcHJvZmlsZXtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmw7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgY29sb3I6ICM1NTU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjREZERkRGO1xyXG4gICAgLmltZywgLm5vLWltZ3tcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiAyNXB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxufSIsIi5waG90by1wcm9maWxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgZGlzcGxheTogZmw7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgY29sb3I6ICM1NTU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0RGREZERjtcbn1cbi5waG90by1wcm9maWxlIC5pbWcsIC5waG90by1wcm9maWxlIC5uby1pbWcge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nOiAyNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59Il19 */"

/***/ }),

/***/ "./src/app/component-libs/photo-profile/photo-profile.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/component-libs/photo-profile/photo-profile.component.ts ***!
  \*************************************************************************/
/*! exports provided: PhotoProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotoProfileComponent", function() { return PhotoProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PhotoProfileComponent = /** @class */ (function () {
    function PhotoProfileComponent() {
    }
    PhotoProfileComponent.prototype.ngOnInit = function () {
        if (this.bgUrl == undefined || this.bgUrl == null) {
            this.bgUrl;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PhotoProfileComponent.prototype, "bgUrl", void 0);
    PhotoProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-photo-profile-component',
            template: __webpack_require__(/*! raw-loader!./photo-profile.component.html */ "./node_modules/raw-loader/index.js!./src/app/component-libs/photo-profile/photo-profile.component.html"),
            styles: [__webpack_require__(/*! ./photo-profile.component.scss */ "./src/app/component-libs/photo-profile/photo-profile.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PhotoProfileComponent);
    return PhotoProfileComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/animation.ts":
/*!*****************************************************!*\
  !*** ./src/app/layout/merchant-portal/animation.ts ***!
  \*****************************************************/
/*! exports provided: slideInAnimation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideInAnimation", function() { return slideInAnimation; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

var slideInAnimation = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routeAnimations', [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('* <=> *', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ position: 'relative' }),
        // First State
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ position: 'absolute', 'z-index': '1', opacity: '0' })
        ], { optional: true }),
        // End State when it leave
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ position: 'absolute', 'z-index': '1', opacity: '1' })], { optional: true }),
        //animating is here
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('350ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ 'z-index': '1', opacity: '0' }))
            ], { optional: true }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('350ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ 'z-index': '1', opacity: '1' }))
            ], { optional: true })
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animateChild"])(), { optional: true }),
    ]),
]);


/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-homepage/merchant-homepage.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-homepage/merchant-homepage.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  position: absolute;\n  width: 100%;\n  left: 0px;\n  top: 30px;\n  overflow: hidden;\n  display: flex;\n}\n:host .card {\n  margin-bottom: 25px;\n}\ndiv,\nspan:not(.fa) {\n  font-family: \"Poppins\";\n}\n._container {\n  padding: 25px 0vw;\n}\n._container .row.cl-row-list {\n  margin: 0px;\n}\n._container .card.cl-list {\n  border-radius: 10px;\n}\n._container .card.cl-list .profile {\n  background: #542cb3;\n  background: linear-gradient(50deg, #542cb3 0%, #da6c87 88%);\n}\n._container .card.cl-list .sales-order {\n  background: #892a6a;\n  background: linear-gradient(50deg, #892a6a 0%, #e86563 88%);\n}\n._container .card.cl-list .product {\n  background: #3e3c89;\n  background: linear-gradient(50deg, #3e3c89 0%, #348ef1 88%);\n}\n._container .card.cl-list .order-history {\n  background: #235b93;\n  background: linear-gradient(50deg, #235b93 0%, #35bf9c 88%);\n}\n._container .card.cl-list .statistics {\n  background: #d61e58;\n  background: linear-gradient(50deg, #d61e58 0%, #f28e41 88%);\n}\n._container .card.cl-list .card-header {\n  border-top-left-radius: 10px;\n  border-top-right-radius: 10px;\n  border-bottom-width: 0px;\n  display: flex;\n  text-align: center;\n  align-items: flex-start;\n  flex-direction: column;\n  height: 27vh;\n  color: white;\n  justify-content: flex-start;\n}\n._container .card.cl-list .card-header .card-header-title {\n  font-size: 17px;\n}\n._container .card.cl-list .card-header hr {\n  display: block;\n  height: 1px;\n  border: 0;\n  width: 100%;\n  border-top: 1px solid #ccc;\n  margin: 1vh 0;\n  padding: 0;\n}\n._container .card.cl-list .card-header .card-header-body {\n  align-self: stretch;\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n}\n._container .card.cl-list .card-header .card-header-body img {\n  height: 15vh;\n  width: auto;\n  min-width: 100px;\n}\n._container .card.cl-list .card-header .card-header-body .title-sub {\n  font-size: 15px;\n  font-weight: bold;\n}\n._container .card.cl-list .card-header .card-header-body .body-sub {\n  font-size: 50px;\n}\n._container .card.cl-list .card-header .fa {\n  font-size: 21px;\n  font-weight: bold;\n  width: 55px;\n  display: inline-flex;\n  margin: 0px 15px 15px 0px;\n  height: 55px;\n  background: #3498db;\n  justify-content: center;\n  padding: 5px;\n  color: white;\n  align-items: center;\n  border-radius: 5px;\n}\n._container .card.cl-list .card-header .profile {\n  background: #9b59b6;\n}\n._container .card.cl-list .card-header .transactions {\n  background: #2ecc71;\n}\n._container .card.cl-list .card-header .statistics {\n  background: #e67e22;\n}\n._container .card.cl-list .card-header .store {\n  background: #1abc9c;\n}\n._container .card.cl-list .card-body {\n  display: block;\n  position: relative;\n  font-weight: bold;\n  height: 140px;\n}\n._container .card.cl-list .card-body .center {\n  text-align: center;\n}\n._container .card.cl-list .card-body .button-container button.btn.btn-2 {\n  width: calc(100% - 40px);\n  position: absolute;\n  bottom: 80px;\n  left: 20px;\n}\n._container .card.cl-list .card-body .button-container button.btn.btn-1 {\n  width: calc(100% - 40px);\n  position: absolute;\n  bottom: 20px;\n  left: 20px;\n}\n._container .card.cl-list .card-body .button-container .upgrade {\n  background: white;\n  border: 1px solid #ffb24d;\n  color: #ffb24d;\n  font-size: 14px;\n  font-weight: bold;\n  padding: 5px;\n}\n._container .card.cl-list .card-body .button-container button > img {\n  width: auto;\n  height: 30px;\n  padding-right: 5px;\n  padding-bottom: 5px;\n}\n._container .card.cl-list .with-upgrade {\n  height: 201px;\n}\n@media (max-width: 1176px) {\n  ._container .cl-row-list .col-lg-3 {\n    display: flex;\n    justify-content: center;\n    align-items: flex-start;\n    width: calc((100vw - 202px) / 2 - 5px);\n    float: left;\n    max-width: unset;\n    flex: unset;\n  }\n}\n@media (max-width: 768px) {\n  ._container .card.cl-list .card-header {\n    font-size: 20px;\n  }\n  ._container .cl-row-list .col-lg-3 {\n    display: flex;\n    justify-content: center;\n    align-items: flex-start;\n    width: calc((100vw - 202px) / 2 - 5px);\n    float: left;\n  }\n\n  div,\nspan:not(.fa) {\n    font-size: 13px;\n  }\n}\n@media (max-width: 620px) {\n  ._container .cl-row-list .col-lg-3 {\n    width: calc((100vw - 61px) / 2 - 5px);\n  }\n}\n@media (max-width: 460px) {\n  ._container .cl-row-list .col-lg-3 {\n    display: flex;\n    justify-content: center;\n    align-items: flex-start;\n    width: 80vw;\n    float: left;\n    max-width: unset;\n    flex: unset;\n  }\n}\n@media only screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) {\n  ._container .card.cl-list .card-header {\n    font-size: 15px;\n  }\n\n  div,\nspan:not(.fa) {\n    font-size: 7px;\n  }\n\n  :host .card {\n    height: 250px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9tZXJjaGFudC1ob21lcGFnZS9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1lcmNoYW50LXBvcnRhbFxcbWVyY2hhbnQtaG9tZXBhZ2VcXG1lcmNoYW50LWhvbWVwYWdlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LWhvbWVwYWdlL21lcmNoYW50LWhvbWVwYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7QUNDRjtBRENFO0VBQ0UsbUJBQUE7QUNDSjtBREdBOztFQUVFLHNCQUFBO0FDQUY7QURHQTtFQUNFLGlCQUFBO0FDQUY7QURFRTtFQUNFLFdBQUE7QUNBSjtBREdFO0VBQ0UsbUJBQUE7QUNESjtBREVJO0VBQ0UsbUJBQUE7RUFDQSwyREFBQTtBQ0FOO0FERUk7RUFDRSxtQkFBQTtFQUNBLDJEQUFBO0FDQU47QURFSTtFQUNFLG1CQUFBO0VBQ0EsMkRBQUE7QUNBTjtBREVJO0VBQ0UsbUJBQUE7RUFDQSwyREFBQTtBQ0FOO0FERUk7RUFDRSxtQkFBQTtFQUNBLDJEQUFBO0FDQU47QURFSTtFQUNFLDRCQUFBO0VBQ0EsNkJBQUE7RUFDQSx3QkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLDJCQUFBO0FDQU47QURDTTtFQUNFLGVBQUE7QUNDUjtBRENNO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7QUNDUjtBRENNO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtBQ0NSO0FEQVE7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDRVY7QURBUTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQ0VWO0FEQVE7RUFDRSxlQUFBO0FDRVY7QURDTTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNDUjtBREVNO0VBQ0UsbUJBQUE7QUNBUjtBREdNO0VBQ0UsbUJBQUE7QUNEUjtBRElNO0VBQ0UsbUJBQUE7QUNGUjtBREtNO0VBQ0UsbUJBQUE7QUNIUjtBRFFJO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFJQSxhQUFBO0FDVE47QURXTTtFQUNFLGtCQUFBO0FDVFI7QURjUTtFQUNFLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtBQ1pWO0FEY1E7RUFDRSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUNaVjtBRGNRO0VBQ0UsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FDWlY7QURlUTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ2JWO0FEaUJJO0VBQ0UsYUFBQTtBQ2ZOO0FEb0JBO0VBRUk7SUFDRSxhQUFBO0lBQ0EsdUJBQUE7SUFDQSx1QkFBQTtJQUNBLHNDQUFBO0lBQ0EsV0FBQTtJQUNBLGdCQUFBO0lBQ0EsV0FBQTtFQ2xCSjtBQUNGO0FEc0JBO0VBR007SUFDRSxlQUFBO0VDdEJOO0VEeUJFO0lBQ0UsYUFBQTtJQUNBLHVCQUFBO0lBQ0EsdUJBQUE7SUFDQSxzQ0FBQTtJQUNBLFdBQUE7RUN2Qko7O0VEMEJBOztJQUVFLGVBQUE7RUN2QkY7QUFDRjtBRHlCQTtFQUVJO0lBQ0UscUNBQUE7RUN4Qko7QUFDRjtBRDRCQTtFQUVJO0lBQ0UsYUFBQTtJQUNBLHVCQUFBO0lBQ0EsdUJBQUE7SUFDQSxXQUFBO0lBQ0EsV0FBQTtJQUNBLGdCQUFBO0lBQ0EsV0FBQTtFQzNCSjtBQUNGO0FEZ0NBO0VBR007SUFDRSxlQUFBO0VDaENOOztFRG9DQTs7SUFFRSxjQUFBO0VDakNGOztFRG9DRTtJQUNFLGFBQUE7RUNqQ0o7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvbWVyY2hhbnQtaG9tZXBhZ2UvbWVyY2hhbnQtaG9tZXBhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGxlZnQ6IDBweDtcclxuICB0b3A6IDMwcHg7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG5cclxuICAuY2FyZCB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG4gIH1cclxufVxyXG5cclxuZGl2LFxyXG5zcGFuOm5vdCguZmEpIHtcclxuICBmb250LWZhbWlseTogXCJQb3BwaW5zXCI7XHJcbn1cclxuXHJcbi5fY29udGFpbmVyIHtcclxuICBwYWRkaW5nOiAyNXB4IDB2dztcclxuXHJcbiAgLnJvdy5jbC1yb3ctbGlzdCB7XHJcbiAgICBtYXJnaW46IDBweDtcclxuICB9XHJcblxyXG4gIC5jYXJkLmNsLWxpc3Qge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIC5wcm9maWxlIHtcclxuICAgICAgYmFja2dyb3VuZDogcmdiKDg0LCA0NCwgMTc5KTtcclxuICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDUwZGVnLCByZ2JhKDg0LCA0NCwgMTc5LCAxKSAwJSwgcmdiYSgyMTgsIDEwOCwgMTM1LCAxKSA4OCUpO1xyXG4gICAgfVxyXG4gICAgLnNhbGVzLW9yZGVyIHtcclxuICAgICAgYmFja2dyb3VuZDogcmdiKDEzNywgNDIsIDEwNik7XHJcbiAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg1MGRlZywgcmdiYSgxMzcsIDQyLCAxMDYsIDEpIDAlLCByZ2JhKDIzMiwgMTAxLCA5OSwgMSkgODglKTtcclxuICAgIH1cclxuICAgIC5wcm9kdWN0IHtcclxuICAgICAgYmFja2dyb3VuZDogcmdiKDYyLCA2MCwgMTM3KTtcclxuICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDUwZGVnLCByZ2JhKDYyLCA2MCwgMTM3LCAxKSAwJSwgcmdiYSg1MiwgMTQyLCAyNDEsIDEpIDg4JSk7XHJcbiAgICB9XHJcbiAgICAub3JkZXItaGlzdG9yeSB7XHJcbiAgICAgIGJhY2tncm91bmQ6IHJnYigzNSwgOTEsIDE0Nyk7XHJcbiAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg1MGRlZywgcmdiYSgzNSwgOTEsIDE0NywgMSkgMCUsIHJnYmEoNTMsIDE5MSwgMTU2LCAxKSA4OCUpO1xyXG4gICAgfVxyXG4gICAgLnN0YXRpc3RpY3Mge1xyXG4gICAgICBiYWNrZ3JvdW5kOiByZ2IoMjE0LCAzMCwgODgpO1xyXG4gICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNTBkZWcsIHJnYmEoMjE0LCAzMCwgODgsIDEpIDAlLCByZ2JhKDI0MiwgMTQyLCA2NSwgMSkgODglKTtcclxuICAgIH1cclxuICAgIC5jYXJkLWhlYWRlciB7XHJcbiAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gICAgICBib3JkZXItYm90dG9tLXdpZHRoOiAwcHg7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgIGhlaWdodDogMjd2aDtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICAgIC5jYXJkLWhlYWRlci10aXRsZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgICB9XHJcbiAgICAgIGhyIHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBoZWlnaHQ6IDFweDtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgbWFyZ2luOiAxdmggMDtcclxuICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICB9XHJcbiAgICAgIC5jYXJkLWhlYWRlci1ib2R5IHtcclxuICAgICAgICBhbGlnbi1zZWxmOiBzdHJldGNoO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgaGVpZ2h0OiAxNXZoO1xyXG4gICAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgICBtaW4td2lkdGg6IDEwMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAudGl0bGUtc3ViIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYm9keS1zdWIge1xyXG4gICAgICAgICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAuZmEge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICB3aWR0aDogNTVweDtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICAgICAgICBtYXJnaW46IDBweCAxNXB4IDE1cHggMHB4O1xyXG4gICAgICAgIGhlaWdodDogNTVweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5wcm9maWxlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjOWI1OWI2O1xyXG4gICAgICB9XHJcblxyXG4gICAgICAudHJhbnNhY3Rpb25zIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMmVjYzcxO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAuc3RhdGlzdGljcyB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2U2N2UyMjtcclxuICAgICAgfVxyXG5cclxuICAgICAgLnN0b3JlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDI2LCAxODgsIDE1NiwgMSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiBcclxuICAgIC5jYXJkLWJvZHkge1xyXG4gICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgLy8ganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIC8vIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgIC8vIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGhlaWdodDogMTQwcHg7XHJcblxyXG4gICAgICAuY2VudGVyIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5idXR0b24tY29udGFpbmVyIHtcclxuICAgICAgIFxyXG4gICAgICAgIGJ1dHRvbi5idG4uYnRuLTIge1xyXG4gICAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xyXG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgYm90dG9tOiA4MHB4O1xyXG4gICAgICAgICAgbGVmdDogMjBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgYnV0dG9uLmJ0bi5idG4tMSB7XHJcbiAgICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICBib3R0b206IDIwcHg7XHJcbiAgICAgICAgICBsZWZ0OiAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAudXBncmFkZSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNmZmIyNGQ7XHJcbiAgICAgICAgICBjb2xvcjogI2ZmYjI0ZDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGJ1dHRvbiA+IGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogYXV0bztcclxuICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAud2l0aC11cGdyYWRlIHsgIFxyXG4gICAgICBoZWlnaHQ6IDIwMXB4IDtcclxuICAgIH1cclxuICAgIFxyXG4gIH1cclxufVxyXG5AbWVkaWEgKG1heC13aWR0aDogMTE3NnB4KSB7XHJcbiAgLl9jb250YWluZXIge1xyXG4gICAgLmNsLXJvdy1saXN0IC5jb2wtbGctMyB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgd2lkdGg6IGNhbGMoKDEwMHZ3IC0gMjAycHgpIC8gMiAtIDVweCk7XHJcbiAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICBtYXgtd2lkdGg6IHVuc2V0O1xyXG4gICAgICBmbGV4OiB1bnNldDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xyXG4gIC5fY29udGFpbmVyIHtcclxuICAgIC5jYXJkLmNsLWxpc3Qge1xyXG4gICAgICAuY2FyZC1oZWFkZXIge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNsLXJvdy1saXN0IC5jb2wtbGctMyB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgd2lkdGg6IGNhbGMoKDEwMHZ3IC0gMjAycHgpIC8gMiAtIDVweCk7XHJcbiAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgfVxyXG4gIH1cclxuICBkaXYsXHJcbiAgc3Bhbjpub3QoLmZhKSB7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgfVxyXG59XHJcbkBtZWRpYSAobWF4LXdpZHRoOiA2MjBweCkge1xyXG4gIC5fY29udGFpbmVyIHtcclxuICAgIC5jbC1yb3ctbGlzdCAuY29sLWxnLTMge1xyXG4gICAgICB3aWR0aDogY2FsYygoMTAwdncgLSA2MXB4KSAvIDIgLSA1cHgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDQ2MHB4KSB7XHJcbiAgLl9jb250YWluZXIge1xyXG4gICAgLmNsLXJvdy1saXN0IC5jb2wtbGctMyB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgd2lkdGg6IDgwdnc7XHJcbiAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICBtYXgtd2lkdGg6IHVuc2V0O1xyXG4gICAgICBmbGV4OiB1bnNldDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi8vIGlQaG9uZSBYXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKGRldmljZS13aWR0aDogMzc1cHgpIGFuZCAoZGV2aWNlLWhlaWdodDogODEycHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDMpIHtcclxuICAuX2NvbnRhaW5lciB7XHJcbiAgICAuY2FyZC5jbC1saXN0IHtcclxuICAgICAgLmNhcmQtaGVhZGVyIHtcclxuICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgZGl2LFxyXG4gIHNwYW46bm90KC5mYSkge1xyXG4gICAgZm9udC1zaXplOiA3cHg7XHJcbiAgfVxyXG4gIDpob3N0IHtcclxuICAgIC5jYXJkIHtcclxuICAgICAgaGVpZ2h0OiAyNTBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiOmhvc3Qge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAxMDAlO1xuICBsZWZ0OiAwcHg7XG4gIHRvcDogMzBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgZGlzcGxheTogZmxleDtcbn1cbjpob3N0IC5jYXJkIHtcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcbn1cblxuZGl2LFxuc3Bhbjpub3QoLmZhKSB7XG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcbn1cblxuLl9jb250YWluZXIge1xuICBwYWRkaW5nOiAyNXB4IDB2dztcbn1cbi5fY29udGFpbmVyIC5yb3cuY2wtcm93LWxpc3Qge1xuICBtYXJnaW46IDBweDtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3Qge1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAucHJvZmlsZSB7XG4gIGJhY2tncm91bmQ6ICM1NDJjYjM7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg1MGRlZywgIzU0MmNiMyAwJSwgI2RhNmM4NyA4OCUpO1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuc2FsZXMtb3JkZXIge1xuICBiYWNrZ3JvdW5kOiAjODkyYTZhO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNTBkZWcsICM4OTJhNmEgMCUsICNlODY1NjMgODglKTtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLnByb2R1Y3Qge1xuICBiYWNrZ3JvdW5kOiAjM2UzYzg5O1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNTBkZWcsICMzZTNjODkgMCUsICMzNDhlZjEgODglKTtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLm9yZGVyLWhpc3Rvcnkge1xuICBiYWNrZ3JvdW5kOiAjMjM1YjkzO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNTBkZWcsICMyMzViOTMgMCUsICMzNWJmOWMgODglKTtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLnN0YXRpc3RpY3Mge1xuICBiYWNrZ3JvdW5kOiAjZDYxZTU4O1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNTBkZWcsICNkNjFlNTggMCUsICNmMjhlNDEgODglKTtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIHtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlci1ib3R0b20td2lkdGg6IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgaGVpZ2h0OiAyN3ZoO1xuICBjb2xvcjogd2hpdGU7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIC5jYXJkLWhlYWRlci10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTdweDtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIGhyIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGhlaWdodDogMXB4O1xuICBib3JkZXI6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luOiAxdmggMDtcbiAgcGFkZGluZzogMDtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIC5jYXJkLWhlYWRlci1ib2R5IHtcbiAgYWxpZ24tc2VsZjogc3RyZXRjaDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciAuY2FyZC1oZWFkZXItYm9keSBpbWcge1xuICBoZWlnaHQ6IDE1dmg7XG4gIHdpZHRoOiBhdXRvO1xuICBtaW4td2lkdGg6IDEwMHB4O1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuY2FyZC1oZWFkZXIgLmNhcmQtaGVhZGVyLWJvZHkgLnRpdGxlLXN1YiB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciAuY2FyZC1oZWFkZXItYm9keSAuYm9keS1zdWIge1xuICBmb250LXNpemU6IDUwcHg7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciAuZmEge1xuICBmb250LXNpemU6IDIxcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB3aWR0aDogNTVweDtcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gIG1hcmdpbjogMHB4IDE1cHggMTVweCAwcHg7XG4gIGhlaWdodDogNTVweDtcbiAgYmFja2dyb3VuZDogIzM0OThkYjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IHdoaXRlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciAucHJvZmlsZSB7XG4gIGJhY2tncm91bmQ6ICM5YjU5YjY7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciAudHJhbnNhY3Rpb25zIHtcbiAgYmFja2dyb3VuZDogIzJlY2M3MTtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIC5zdGF0aXN0aWNzIHtcbiAgYmFja2dyb3VuZDogI2U2N2UyMjtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIC5zdG9yZSB7XG4gIGJhY2tncm91bmQ6ICMxYWJjOWM7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWJvZHkge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgaGVpZ2h0OiAxNDBweDtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtYm9keSAuY2VudGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuY2FyZC1ib2R5IC5idXR0b24tY29udGFpbmVyIGJ1dHRvbi5idG4uYnRuLTIge1xuICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiA4MHB4O1xuICBsZWZ0OiAyMHB4O1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuY2FyZC1ib2R5IC5idXR0b24tY29udGFpbmVyIGJ1dHRvbi5idG4uYnRuLTEge1xuICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAyMHB4O1xuICBsZWZ0OiAyMHB4O1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuY2FyZC1ib2R5IC5idXR0b24tY29udGFpbmVyIC51cGdyYWRlIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmIyNGQ7XG4gIGNvbG9yOiAjZmZiMjRkO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nOiA1cHg7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWJvZHkgLmJ1dHRvbi1jb250YWluZXIgYnV0dG9uID4gaW1nIHtcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAud2l0aC11cGdyYWRlIHtcbiAgaGVpZ2h0OiAyMDFweDtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDExNzZweCkge1xuICAuX2NvbnRhaW5lciAuY2wtcm93LWxpc3QgLmNvbC1sZy0zIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgIHdpZHRoOiBjYWxjKCgxMDB2dyAtIDIwMnB4KSAvIDIgLSA1cHgpO1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG1heC13aWR0aDogdW5zZXQ7XG4gICAgZmxleDogdW5zZXQ7XG4gIH1cbn1cbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xuICAuX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICB9XG4gIC5fY29udGFpbmVyIC5jbC1yb3ctbGlzdCAuY29sLWxnLTMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgd2lkdGg6IGNhbGMoKDEwMHZ3IC0gMjAycHgpIC8gMiAtIDVweCk7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gIH1cblxuICBkaXYsXG5zcGFuOm5vdCguZmEpIHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gIH1cbn1cbkBtZWRpYSAobWF4LXdpZHRoOiA2MjBweCkge1xuICAuX2NvbnRhaW5lciAuY2wtcm93LWxpc3QgLmNvbC1sZy0zIHtcbiAgICB3aWR0aDogY2FsYygoMTAwdncgLSA2MXB4KSAvIDIgLSA1cHgpO1xuICB9XG59XG5AbWVkaWEgKG1heC13aWR0aDogNDYwcHgpIHtcbiAgLl9jb250YWluZXIgLmNsLXJvdy1saXN0IC5jb2wtbGctMyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICB3aWR0aDogODB2dztcbiAgICBmbG9hdDogbGVmdDtcbiAgICBtYXgtd2lkdGg6IHVuc2V0O1xuICAgIGZsZXg6IHVuc2V0O1xuICB9XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChkZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKGRldmljZS1oZWlnaHQ6IDgxMnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAzKSB7XG4gIC5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIHtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gIH1cblxuICBkaXYsXG5zcGFuOm5vdCguZmEpIHtcbiAgICBmb250LXNpemU6IDdweDtcbiAgfVxuXG4gIDpob3N0IC5jYXJkIHtcbiAgICBoZWlnaHQ6IDI1MHB4O1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-homepage/merchant-homepage.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-homepage/merchant-homepage.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: MerchantHomepageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantHomepageComponent", function() { return MerchantHomepageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



// import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
var MerchantHomepageComponent = /** @class */ (function () {
    function MerchantHomepageComponent(productService, router
    // ,private orderHistory: OrderhistoryService
    ) {
        this.productService = productService;
        this.router = router;
    }
    MerchantHomepageComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MerchantHomepageComponent.prototype.goGenuine = function () {
        this.router.navigate(['/merchant-portal/homepage/upgrade-genuine']);
    };
    MerchantHomepageComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.productService.getProductsReport()];
                    case 1:
                        result = _a.sent();
                        // let result2 = await this.orderHistory.getOrderHistoryMerchant();
                        if (result.result) {
                            this.myProducts = result.result;
                            // this.myOrderHistory = result2.result
                            // console.log("ini",this.myOrderHistory)
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantHomepageComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    MerchantHomepageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-homepage',
            template: __webpack_require__(/*! raw-loader!./merchant-homepage.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-homepage/merchant-homepage.component.html"),
            styles: [__webpack_require__(/*! ./merchant-homepage.component.scss */ "./src/app/layout/merchant-portal/merchant-homepage/merchant-homepage.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
            // ,private orderHistory: OrderhistoryService
        ])
    ], MerchantHomepageComponent);
    return MerchantHomepageComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component.scss":
/*!************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component.scss ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n  margin-top: 40px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -5px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content .status-button-container {\n  display: flex;\n  justify-content: space-between;\n}\n.card-detail .card-content .status-button-container .option {\n  background-color: white;\n  width: 45%;\n  color: grey;\n  border: grey 1px solid;\n}\n.card-detail .card-content .status-button-container .paid {\n  background-color: blue;\n  border-color: blue;\n  color: white;\n}\n.card-detail .card-content .status-button-container .cancel {\n  background-color: #dc3545;\n  border-color: #dc3545;\n  color: white;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .orderhistoryallhistory-detail label {\n  margin-top: 10px;\n}\n.card-detail .orderhistoryallhistory-detail .head-one {\n  line-height: 33px;\n  border-bottom: 1px solid #333;\n}\n.card-detail .orderhistoryallhistory-detail .head-one label.order-id {\n  font-size: 18px;\n  overflow: hidden;\n  width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n}\n.card-detail .orderhistoryallhistory-detail .head-one label.buyer-name {\n  font-size: 24px;\n  overflow: hidden;\n  width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  color: #0EA8DB;\n}\n.card-detail .orderhistoryallhistory-detail .product-group {\n  padding-bottom: 25px;\n}\n.card-detail .orderhistoryallhistory-detail .product-group .money-container {\n  padding: 20px;\n  display: flex;\n}\n.card-detail .orderhistoryallhistory-detail .product-group .money-container .icon-container > i {\n  border-radius: 50%;\n  padding: 3px;\n  font-size: 45px;\n}\n.card-detail .orderhistoryallhistory-detail .product-group .money-container .number-container {\n  margin-left: 20px;\n  padding-top: 2px;\n}\n.card-detail .orderhistoryallhistory-detail .product-group .money-container .number-container label {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: bold;\n}\n.card-detail .orderhistoryallhistory-detail .card-header {\n  background-color: white;\n  border-left: 3px solid blue;\n}\n.card-detail .orderhistoryallhistory-detail .card-header h2 {\n  color: #555;\n  font-size: 20px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list {\n  float: left;\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  overflow: hidden;\n  align-items: end;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list .awb {\n  font-size: 11px;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list .input-form {\n  width: 100%;\n}\n.button-modal {\n  font-size: 15px;\n  border-radius: 5px;\n  display: inline-flex;\n  min-width: 250px;\n  align-items: center;\n  justify-content: center;\n  cursor: pointer;\n  height: 50px;\n  padding: 3px 7px;\n  text-decoration: none !important;\n}\n.paid {\n  background-color: blue;\n  border-color: blue;\n  color: white;\n}\n.cancel {\n  background-color: #dc3545;\n  border-color: #dc3545;\n  color: white;\n}\n.red-fill {\n  background-color: red;\n}\n.printLabel {\n  text-align: center;\n}\n#line {\n  border-right: 1px solid black;\n  height: 20px;\n}\n.spacing {\n  font-size: 14px;\n}\ntextarea {\n  width: 100%;\n  height: 100px;\n  border-radius: 5px;\n}\n#cancel-note {\n  margin-top: 20px;\n}\n.payer {\n  text-align: left;\n  font-weight: bold;\n  padding: initial;\n}\n.payer table td {\n  color: #0EA8DB;\n}\n#header {\n  margin-top: 0px;\n}\n.save {\n  background-color: cornflowerblue;\n  border: none;\n  color: white;\n  padding: 8px 6px;\n  text-align: center;\n  text-decoration: none;\n  font-size: 14px;\n  margin: -34px -7px;\n  float: right;\n  border-radius: 5px;\n}\n.change {\n  background-color: blue !important;\n}\n.edit {\n  background-color: #4CAF50;\n  border: none;\n  color: white;\n  padding: 8px 18px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  font-size: 14px;\n  margin: -31px 3px;\n  float: right;\n  border-radius: 5px;\n}\n.done {\n  background-color: blue;\n  border: none;\n  color: white;\n  padding: 8px 18px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  font-size: 14px;\n  margin: -31px 3px;\n  float: right;\n  border-radius: 5px;\n}\n.delete-container {\n  margin: 0px auto;\n  background: white;\n  width: 40%;\n  border-radius: 20px;\n  padding: 10px;\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  color: black;\n}\n.delete-container .dialog {\n  padding: 17px;\n  text-align: center;\n}\n.delete-container .dialog .reject {\n  text-align: left;\n  font-weight: bold;\n  font-size: 20px;\n  margin-bottom: 4%;\n}\n.delete-container .dialog .release {\n  text-align: center;\n  font-weight: bold;\n  font-size: 25px;\n  margin-bottom: 4%;\n}\n.delete-container .dialog .blue-outline {\n  border: blue solid;\n  background: white !important;\n  color: blue;\n}\n.delete-container .dialog .red-outline {\n  border: red solid;\n  background: white !important;\n  color: red;\n}\n.delete-container .dialog .button-container {\n  width: 100%;\n  display: flex;\n  justify-content: space-between;\n}\n.modal-top {\n  background-color: white;\n}\n.modal-top .close {\n  color: black;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n.modal.false {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n.modal.true {\n  display: block;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n/* Modal Header */\n.modal-header {\n  padding: 2px 16px;\n  color: white;\n}\n/* Modal Body */\n.modal-body {\n  padding: 2px 16px;\n}\n/* Modal Footer */\n.modal-footer {\n  padding: 2px 16px;\n  color: white;\n}\n/* Modal Content */\n.modal-content {\n  margin-left: 256px;\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  padding: 0;\n  border: 1px solid #888;\n  width: 50%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n/* The Close Button */\n.close {\n  color: white;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n.close:hover,\n.close:focus {\n  color: #000;\n  text-decoration: none;\n  cursor: pointer;\n}\n/* Add Animation */\n@-webkit-keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n@keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\ntextarea {\n  padding: 10px;\n  line-height: 1.5;\n  border-radius: 5px;\n  border: 1px solid #ccc;\n  box-shadow: 1px 1px 1px #999;\n}\ntable {\n  width: 100%;\n  font-size: 14px;\n  box-shadow: 5px 5px 10px #999;\n  border-radius: 5px;\n  text-transform: capitalize;\n  overflow: hidden;\n}\ntable td {\n  border-width: 0px 1px 0px 0px;\n  padding: 15px;\n  vertical-align: top;\n}\ntable td em {\n  display: block;\n  margin-left: 5px;\n  font-size: 10px;\n  font-style: italic;\n}\ntable th {\n  background-color: #f2f2f2;\n  color: #8d8d8d;\n  padding: 15px;\n  font-weight: bold;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9tZXJjaGFudC1teS1lc2Nyb3cvbWVyY2hhbnQtZXNjcm93LWRldGFpbC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1lcmNoYW50LXBvcnRhbFxcbWVyY2hhbnQtbXktZXNjcm93XFxtZXJjaGFudC1lc2Nyb3ctZGV0YWlsXFxtZXJjaGFudC1lc2Nyb3ctZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LW15LWVzY3Jvdy9tZXJjaGFudC1lc2Nyb3ctZGV0YWlsL21lcmNoYW50LWVzY3Jvdy1kZXRhaWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNBUjtBREVRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RaO0FER1k7RUFDSSxpQkFBQTtBQ0RoQjtBRFFJO0VBQ0ksYUFBQTtBQ05SO0FEUVE7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7QUNOWjtBRFFZO0VBQ0ksdUJBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0FDTmhCO0FEU1k7RUFDSSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ1BoQjtBRFVZO0VBQ0kseUJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7QUNSaEI7QURZUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNWWjtBRGNJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNaUjtBRGlCUTtFQUNJLGdCQUFBO0FDZlo7QURrQlE7RUFDSSxpQkFBQTtFQUNBLDZCQUFBO0FDaEJaO0FEa0JZO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNoQmhCO0FEbUJZO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FDakJoQjtBRHVCUTtFQUNJLG9CQUFBO0FDckJaO0FEdUJZO0VBQ0ksYUFBQTtFQVdBLGFBQUE7QUMvQmhCO0FEc0JnQjtFQUNJLGtCQUFBO0VBRUEsWUFBQTtFQUNBLGVBQUE7QUNyQnBCO0FENEJnQjtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7QUMxQnBCO0FENEJvQjtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUMxQnhCO0FEbUNRO0VBQ0ksdUJBQUE7RUFDQSwyQkFBQTtBQ2pDWjtBRG1DWTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDakNoQjtBRHNDUTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ3BDWjtBRHNDWTtFQUNJLGVBQUE7QUNwQ2hCO0FEeUNZO0VBQ0ksV0FBQTtBQ3ZDaEI7QUQ4Q0E7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQ0FBQTtBQzNDSjtBRDhDQTtFQUNJLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDM0NKO0FEOENBO0VBQ0kseUJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7QUMzQ0o7QUQrQ0E7RUFDSSxxQkFBQTtBQzVDSjtBRCtDQTtFQUNJLGtCQUFBO0FDNUNKO0FEK0NBO0VBQ0ksNkJBQUE7RUFDQSxZQUFBO0FDNUNKO0FEdURBO0VBQ0ksZUFBQTtBQ3BESjtBRHdEQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUNyREo7QUR5REE7RUFDSSxnQkFBQTtBQ3RESjtBRHlEQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQ3RESjtBRHlEUTtFQUNJLGNBQUE7QUN2RFo7QUQ0REE7RUFDSSxlQUFBO0FDekRKO0FENERBO0VBQ0ksZ0NBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUVBLGVBQUE7RUFDQSxrQkFBQTtFQUVBLFlBQUE7RUFDQSxrQkFBQTtBQzNESjtBRDhEQTtFQUNJLGlDQUFBO0FDM0RKO0FEOERBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBRUEsWUFBQTtFQUNBLGtCQUFBO0FDNURKO0FEK0RBO0VBQ0ksc0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBRUEsWUFBQTtFQUNBLGtCQUFBO0FDN0RKO0FEZ0VBO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxrQ0FBQTtVQUFBLDBCQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLFlBQUE7QUM3REo7QUQrREk7RUFDSSxhQUFBO0VBQ0Esa0JBQUE7QUM3RFI7QUQrRFE7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDN0RaO0FEZ0VRO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQzlEWjtBRGtFUTtFQUNJLGtCQUFBO0VBQ0EsNEJBQUE7RUFDQSxXQUFBO0FDaEVaO0FEbUVRO0VBQ0ksaUJBQUE7RUFDQSw0QkFBQTtFQUNBLFVBQUE7QUNqRVo7QURvRVE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0FDbEVaO0FEeUVBO0VBQ0ksdUJBQUE7QUN0RUo7QUR3RUk7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ3RFUjtBRDBFQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUNBLE9BQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsNEJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0NBQUE7RUFDQSxxQkFBQTtBQ3ZFSjtBRDBFQTtFQUNJLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUVBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSw0QkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQ0FBQTtFQUNBLHFCQUFBO0FDeEVKO0FENEVBLGlCQUFBO0FBQ0E7RUFDSSxpQkFBQTtFQUVBLFlBQUE7QUMxRUo7QUQ2RUEsZUFBQTtBQUNBO0VBQ0ksaUJBQUE7QUMxRUo7QUQ2RUEsaUJBQUE7QUFDQTtFQUNJLGlCQUFBO0VBRUEsWUFBQTtBQzNFSjtBRDhFQSxrQkFBQTtBQUNBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxzQkFBQTtFQUNBLFVBQUE7RUFDQSw0RUFBQTtFQUNBLGtDQUFBO1VBQUEsMEJBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0Esa0JBQUE7QUMzRUo7QUQ4RUEscUJBQUE7QUFDQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDM0VKO0FEOEVBOztFQUVJLFdBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7QUMzRUo7QUQrRUEsa0JBQUE7QUFDQTtFQUNJO0lBQ0ksV0FBQTtJQUNBLFVBQUE7RUM1RU47RUQrRUU7SUFDSSxNQUFBO0lBQ0EsVUFBQTtFQzdFTjtBQUNGO0FEb0VBO0VBQ0k7SUFDSSxXQUFBO0lBQ0EsVUFBQTtFQzVFTjtFRCtFRTtJQUNJLE1BQUE7SUFDQSxVQUFBO0VDN0VOO0FBQ0Y7QURnRkE7RUFDSSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUM5RUo7QURpRkE7RUFDQyxXQUFBO0VBR0EsZUFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtFQUNBLGdCQUFBO0FDaEZEO0FEa0ZDO0VBR0MsNkJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUNsRkY7QURvRkU7RUFDQyxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNsRkg7QURxRkk7RUFDRix5QkFBQTtFQUNBLGNBQUE7RUFFQSxhQUFBO0VBRUEsaUJBQUE7RUFDQSwwQkFBQTtBQ3JGRiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvbWVyY2hhbnQtbXktZXNjcm93L21lcmNoYW50LWVzY3Jvdy1kZXRhaWwvbWVyY2hhbnQtZXNjcm93LWRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWRldGFpbCB7XHJcbiAgICA+LmNhcmQtaGVhZGVyIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDhweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA0MHB4O1xyXG5cclxuICAgICAgICAuYmFja19idXR0b24ge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtNXB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG5cclxuICAgICAgICAgICAgaSB7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG5cclxuICAgIC5jYXJkLWNvbnRlbnQge1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcblxyXG4gICAgICAgIC5zdGF0dXMtYnV0dG9uLWNvbnRhaW5lciB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICAgICAgICAgIC5vcHRpb24ge1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogNDUlO1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IGdyZXk7XHJcbiAgICAgICAgICAgICAgICBib3JkZXI6IGdyZXkgMXB4IHNvbGlkO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAucGFpZCB7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiBibHVlO1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAuY2FuY2VsIHtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkYzM1NDU7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItY29sb3I6ICNkYzM1NDU7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgID4uY29sLW1kLTEyIHtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaDEge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCB7XHJcblxyXG4gICAgICAgIGxhYmVsIHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5oZWFkLW9uZSB7XHJcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAzM3B4O1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzMzMztcclxuXHJcbiAgICAgICAgICAgIGxhYmVsLm9yZGVyLWlkIHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGFiZWwuYnV5ZXItbmFtZSB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDI0cHg7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzBFQThEQjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAucHJvZHVjdC1ncm91cCB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAyNXB4O1xyXG5cclxuICAgICAgICAgICAgLm1vbmV5LWNvbnRhaW5lciB7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG5cclxuICAgICAgICAgICAgICAgIC5pY29uLWNvbnRhaW5lcj5pIHtcclxuICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDNweDtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDQ1cHg7XHJcblxyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxuICAgICAgICAgICAgICAgIC5udW1iZXItY29udGFpbmVyIHtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLXRvcDogMnB4O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBsYWJlbCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgICAgIC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuY2FyZC1oZWFkZXIge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCBibHVlO1xyXG5cclxuICAgICAgICAgICAgaDIge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5hd2ItbGlzdCB7XHJcbiAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGVuZDtcclxuXHJcbiAgICAgICAgICAgIC5hd2Ige1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICAgICAgICAgICAgLy8gbWFyZ2luLXRvcDogMTBweDtcclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC5pbnB1dC1mb3JtIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufVxyXG5cclxuLmJ1dHRvbi1tb2RhbCB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICAgIG1pbi13aWR0aDogMjUwcHg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBwYWRkaW5nOiAzcHggN3B4O1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5wYWlkIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XHJcbiAgICBib3JkZXItY29sb3I6IGJsdWU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5jYW5jZWwge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2RjMzU0NTtcclxuICAgIGJvcmRlci1jb2xvcjogI2RjMzU0NTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuXHJcbi5yZWQtZmlsbCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbn1cclxuXHJcbi5wcmludExhYmVsIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuI2xpbmUge1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgYmxhY2s7XHJcbiAgICBoZWlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbi8vICAuYnRuLXByaW50e1xyXG4vLyAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyYWFkMmM7XHJcbi8vICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuLy8gICAgICBjb2xvcjp3aGl0ZTtcclxuLy8gICAgICB3aWR0aDogNTAlO1xyXG4vLyAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuLy8gIH1cclxuXHJcbi5zcGFjaW5nIHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIC8vIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG59XHJcblxyXG50ZXh0YXJlYSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAvLyBib3JkZXItdG9wOiAxcHggc29saWQ7XHJcbn1cclxuXHJcbiNjYW5jZWwtbm90ZSB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcblxyXG4ucGF5ZXIge1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgcGFkZGluZzogaW5pdGlhbDtcclxuXHJcbiAgICB0YWJsZSB7XHJcbiAgICAgICAgdGQge1xyXG4gICAgICAgICAgICBjb2xvcjogIzBFQThEQjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbiNoZWFkZXIge1xyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG59XHJcblxyXG4uc2F2ZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBjb3JuZmxvd2VyYmx1ZTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDhweCA2cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAvLyBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBtYXJnaW46IC0zNHB4IC03cHg7XHJcbiAgICAvLyBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuXHJcbi5jaGFuZ2Uge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uZWRpdCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNENBRjUwO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZzogOHB4IDE4cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBtYXJnaW46IC0zMXB4IDNweDtcclxuICAgIC8vIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5cclxuLmRvbmUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDhweCAxOHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgbWFyZ2luOiAtMzFweCAzcHg7XHJcbiAgICAvLyBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuXHJcbi5kZWxldGUtY29udGFpbmVyIHtcclxuICAgIG1hcmdpbjogMHB4IGF1dG87XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIHdpZHRoOiA0MCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGFuaW1hdGlvbi1uYW1lOiBhbmltYXRldG9wO1xyXG4gICAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjRzO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG5cclxuICAgIC5kaWFsb2cge1xyXG4gICAgICAgIHBhZGRpbmc6IDE3cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgICAgICAucmVqZWN0IHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogNCU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAucmVsZWFzZSB7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogNCU7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmJsdWUtb3V0bGluZSB7XHJcbiAgICAgICAgICAgIGJvcmRlcjogYmx1ZSBzb2xpZDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgY29sb3I6IGJsdWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAucmVkLW91dGxpbmUge1xyXG4gICAgICAgICAgICBib3JkZXI6IHJlZCBzb2xpZDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgY29sb3I6IHJlZDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5idXR0b24tY29udGFpbmVyIHtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuLm1vZGFsLXRvcCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuXHJcbiAgICAuY2xvc2Uge1xyXG4gICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAyOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG59XHJcblxyXG4ubW9kYWwuZmFsc2Uge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICAgIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAvKiBTdGF5IGluIHBsYWNlICovXHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgLyogU2l0IG9uIHRvcCAqL1xyXG4gICAgcGFkZGluZy10b3A6IDEwMHB4O1xyXG4gICAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xyXG4gICAgbGVmdDogMDtcclxuICAgIHRvcDogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLyogRnVsbCB3aWR0aCAqL1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLyogRnVsbCBoZWlnaHQgKi9cclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgLyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigwLCAwLCAwKTtcclxuICAgIC8qIEZhbGxiYWNrIGNvbG9yICovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XHJcbiAgICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXHJcbn1cclxuXHJcbi5tb2RhbC50cnVlIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIC8qIFN0YXkgaW4gcGxhY2UgKi9cclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICAvKiBTaXQgb24gdG9wICovXHJcbiAgICBwYWRkaW5nLXRvcDogMTAwcHg7XHJcbiAgICAvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXHJcblxyXG4gICAgdG9wOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICAvKiBGdWxsIHdpZHRoICovXHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAvKiBGdWxsIGhlaWdodCAqL1xyXG4gICAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDAsIDAsIDApO1xyXG4gICAgLyogRmFsbGJhY2sgY29sb3IgKi9cclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcclxuICAgIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cclxufVxyXG5cclxuXHJcbi8qIE1vZGFsIEhlYWRlciAqL1xyXG4ubW9kYWwtaGVhZGVyIHtcclxuICAgIHBhZGRpbmc6IDJweCAxNnB4O1xyXG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzVjYjg1YztcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLyogTW9kYWwgQm9keSAqL1xyXG4ubW9kYWwtYm9keSB7XHJcbiAgICBwYWRkaW5nOiAycHggMTZweDtcclxufVxyXG5cclxuLyogTW9kYWwgRm9vdGVyICovXHJcbi5tb2RhbC1mb290ZXIge1xyXG4gICAgcGFkZGluZzogMnB4IDE2cHg7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjNWNiODVjO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4vKiBNb2RhbCBDb250ZW50ICovXHJcbi5tb2RhbC1jb250ZW50IHtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNTZweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzg4ODtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDAuNHM7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi8qIFRoZSBDbG9zZSBCdXR0b24gKi9cclxuLmNsb3NlIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGZvbnQtc2l6ZTogMjhweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4uY2xvc2U6aG92ZXIsXHJcbi5jbG9zZTpmb2N1cyB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuXHJcbi8qIEFkZCBBbmltYXRpb24gKi9cclxuQGtleWZyYW1lcyBhbmltYXRldG9wIHtcclxuICAgIGZyb20ge1xyXG4gICAgICAgIHRvcDogLTMwMHB4O1xyXG4gICAgICAgIG9wYWNpdHk6IDBcclxuICAgIH1cclxuXHJcbiAgICB0byB7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIG9wYWNpdHk6IDFcclxuICAgIH1cclxufVxyXG5cclxudGV4dGFyZWEge1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCAxcHggIzk5OTtcclxufVxyXG5cclxudGFibGUge1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdC8vIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcblx0Ly8gYm9yZGVyOiAxcHggc29saWQgIzMzMztcclxuXHRmb250LXNpemU6IDE0cHg7XHJcblx0Ym94LXNoYWRvdzogNXB4IDVweCAxMHB4ICM5OTk7XHJcblx0Ym9yZGVyLXJhZGl1czogNXB4O1xyXG5cdHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG5cdG92ZXJmbG93OiBoaWRkZW47XHJcblxyXG5cdHRkIHtcclxuXHRcdC8vICBib3JkZXItc3R5bGU6c29saWQ7XHJcblx0XHQvLyAgYm9yZGVyLWNvbG9yOiAjMzMzO1xyXG5cdFx0Ym9yZGVyLXdpZHRoOiAwcHggMXB4IDBweCAwcHg7XHJcblx0XHRwYWRkaW5nOiAxNXB4O1xyXG5cdFx0dmVydGljYWwtYWxpZ246IHRvcDtcclxuXHJcblx0XHRlbSB7XHJcblx0XHRcdGRpc3BsYXk6IGJsb2NrO1xyXG5cdFx0XHRtYXJnaW4tbGVmdDogNXB4O1xyXG5cdFx0XHRmb250LXNpemU6IDEwcHg7XHJcblx0XHRcdGZvbnQtc3R5bGU6IGl0YWxpYztcclxuXHRcdH1cclxuICAgIH1cclxuICAgIHRoIHtcclxuXHRcdGJhY2tncm91bmQtY29sb3I6ICNmMmYyZjI7XHJcblx0XHRjb2xvcjogIzhkOGQ4ZDtcclxuXHRcdC8vIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XHJcblx0XHRwYWRkaW5nOiAxNXB4O1xyXG5cdFx0Ly8gdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0XHR0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuXHRcdC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcblx0fVxyXG59IiwiLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiA0OHB4O1xuICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC01cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgLnN0YXR1cy1idXR0b24tY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgLnN0YXR1cy1idXR0b24tY29udGFpbmVyIC5vcHRpb24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDQ1JTtcbiAgY29sb3I6IGdyZXk7XG4gIGJvcmRlcjogZ3JleSAxcHggc29saWQ7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCAuc3RhdHVzLWJ1dHRvbi1jb250YWluZXIgLnBhaWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xuICBib3JkZXItY29sb3I6IGJsdWU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IC5zdGF0dXMtYnV0dG9uLWNvbnRhaW5lciAuY2FuY2VsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2RjMzU0NTtcbiAgYm9yZGVyLWNvbG9yOiAjZGMzNTQ1O1xuICBjb2xvcjogd2hpdGU7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5oZWFkLW9uZSB7XG4gIGxpbmUtaGVpZ2h0OiAzM3B4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzMzMztcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmhlYWQtb25lIGxhYmVsLm9yZGVyLWlkIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5oZWFkLW9uZSBsYWJlbC5idXllci1uYW1lIHtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIGNvbG9yOiAjMEVBOERCO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAucHJvZHVjdC1ncm91cCB7XG4gIHBhZGRpbmctYm90dG9tOiAyNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAucHJvZHVjdC1ncm91cCAubW9uZXktY29udGFpbmVyIHtcbiAgcGFkZGluZzogMjBweDtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLnByb2R1Y3QtZ3JvdXAgLm1vbmV5LWNvbnRhaW5lciAuaWNvbi1jb250YWluZXIgPiBpIHtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwYWRkaW5nOiAzcHg7XG4gIGZvbnQtc2l6ZTogNDVweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLnByb2R1Y3QtZ3JvdXAgLm1vbmV5LWNvbnRhaW5lciAubnVtYmVyLWNvbnRhaW5lciB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICBwYWRkaW5nLXRvcDogMnB4O1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAucHJvZHVjdC1ncm91cCAubW9uZXktY29udGFpbmVyIC5udW1iZXItY29udGFpbmVyIGxhYmVsIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBib3JkZXItbGVmdDogM3B4IHNvbGlkIGJsdWU7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiAjNTU1O1xuICBmb250LXNpemU6IDIwcHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuYXdiLWxpc3Qge1xuICBmbG9hdDogbGVmdDtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGFsaWduLWl0ZW1zOiBlbmQ7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5hd2ItbGlzdCAuYXdiIHtcbiAgZm9udC1zaXplOiAxMXB4O1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuYXdiLWxpc3QgLmlucHV0LWZvcm0ge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmJ1dHRvbi1tb2RhbCB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgbWluLXdpZHRoOiAyNTBweDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgaGVpZ2h0OiA1MHB4O1xuICBwYWRkaW5nOiAzcHggN3B4O1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLnBhaWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xuICBib3JkZXItY29sb3I6IGJsdWU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmNhbmNlbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkYzM1NDU7XG4gIGJvcmRlci1jb2xvcjogI2RjMzU0NTtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4ucmVkLWZpbGwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG59XG5cbi5wcmludExhYmVsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4jbGluZSB7XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIGJsYWNrO1xuICBoZWlnaHQ6IDIwcHg7XG59XG5cbi5zcGFjaW5nIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG50ZXh0YXJlYSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbiNjYW5jZWwtbm90ZSB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbi5wYXllciB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nOiBpbml0aWFsO1xufVxuLnBheWVyIHRhYmxlIHRkIHtcbiAgY29sb3I6ICMwRUE4REI7XG59XG5cbiNoZWFkZXIge1xuICBtYXJnaW4tdG9wOiAwcHg7XG59XG5cbi5zYXZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogY29ybmZsb3dlcmJsdWU7XG4gIGJvcmRlcjogbm9uZTtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiA4cHggNnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW46IC0zNHB4IC03cHg7XG4gIGZsb2F0OiByaWdodDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4uY2hhbmdlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZSAhaW1wb3J0YW50O1xufVxuXG4uZWRpdCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0Q0FGNTA7XG4gIGJvcmRlcjogbm9uZTtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiA4cHggMThweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW46IC0zMXB4IDNweDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5kb25lIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcbiAgYm9yZGVyOiBub25lO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDhweCAxOHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbjogLTMxcHggM3B4O1xuICBmbG9hdDogcmlnaHQ7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLmRlbGV0ZS1jb250YWluZXIge1xuICBtYXJnaW46IDBweCBhdXRvO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgd2lkdGg6IDQwJTtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgcGFkZGluZzogMTBweDtcbiAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcbiAgY29sb3I6IGJsYWNrO1xufVxuLmRlbGV0ZS1jb250YWluZXIgLmRpYWxvZyB7XG4gIHBhZGRpbmc6IDE3cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5kZWxldGUtY29udGFpbmVyIC5kaWFsb2cgLnJlamVjdCB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IDQlO1xufVxuLmRlbGV0ZS1jb250YWluZXIgLmRpYWxvZyAucmVsZWFzZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogNCU7XG59XG4uZGVsZXRlLWNvbnRhaW5lciAuZGlhbG9nIC5ibHVlLW91dGxpbmUge1xuICBib3JkZXI6IGJsdWUgc29saWQ7XG4gIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiBibHVlO1xufVxuLmRlbGV0ZS1jb250YWluZXIgLmRpYWxvZyAucmVkLW91dGxpbmUge1xuICBib3JkZXI6IHJlZCBzb2xpZDtcbiAgYmFja2dyb3VuZDogd2hpdGUgIWltcG9ydGFudDtcbiAgY29sb3I6IHJlZDtcbn1cbi5kZWxldGUtY29udGFpbmVyIC5kaWFsb2cgLmJ1dHRvbi1jb250YWluZXIge1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuXG4ubW9kYWwtdG9wIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG4ubW9kYWwtdG9wIC5jbG9zZSB7XG4gIGNvbG9yOiBibGFjaztcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDI4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubW9kYWwuZmFsc2Uge1xuICBkaXNwbGF5OiBub25lO1xuICAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgei1pbmRleDogMTtcbiAgLyogU2l0IG9uIHRvcCAqL1xuICBwYWRkaW5nLXRvcDogMTAwcHg7XG4gIC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgLyogRnVsbCB3aWR0aCAqL1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIEZ1bGwgaGVpZ2h0ICovXG4gIG92ZXJmbG93OiBhdXRvO1xuICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG59XG5cbi5tb2RhbC50cnVlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xuICB6LWluZGV4OiAxO1xuICAvKiBTaXQgb24gdG9wICovXG4gIHBhZGRpbmctdG9wOiAxMDBweDtcbiAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICAvKiBGdWxsIHdpZHRoICovXG4gIGhlaWdodDogMTAwJTtcbiAgLyogRnVsbCBoZWlnaHQgKi9cbiAgb3ZlcmZsb3c6IGF1dG87XG4gIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICAvKiBGYWxsYmFjayBjb2xvciAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cbn1cblxuLyogTW9kYWwgSGVhZGVyICovXG4ubW9kYWwtaGVhZGVyIHtcbiAgcGFkZGluZzogMnB4IDE2cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLyogTW9kYWwgQm9keSAqL1xuLm1vZGFsLWJvZHkge1xuICBwYWRkaW5nOiAycHggMTZweDtcbn1cblxuLyogTW9kYWwgRm9vdGVyICovXG4ubW9kYWwtZm9vdGVyIHtcbiAgcGFkZGluZzogMnB4IDE2cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLyogTW9kYWwgQ29udGVudCAqL1xuLm1vZGFsLWNvbnRlbnQge1xuICBtYXJnaW4tbGVmdDogMjU2cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiAwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xuICB3aWR0aDogNTAlO1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICBhbmltYXRpb24tbmFtZTogYW5pbWF0ZXRvcDtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjRzO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi8qIFRoZSBDbG9zZSBCdXR0b24gKi9cbi5jbG9zZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDI4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uY2xvc2U6aG92ZXIsXG4uY2xvc2U6Zm9jdXMge1xuICBjb2xvcjogIzAwMDtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi8qIEFkZCBBbmltYXRpb24gKi9cbkBrZXlmcmFtZXMgYW5pbWF0ZXRvcCB7XG4gIGZyb20ge1xuICAgIHRvcDogLTMwMHB4O1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbiAgdG8ge1xuICAgIHRvcDogMDtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG50ZXh0YXJlYSB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgYm94LXNoYWRvdzogMXB4IDFweCAxcHggIzk5OTtcbn1cblxudGFibGUge1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3gtc2hhZG93OiA1cHggNXB4IDEwcHggIzk5OTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbnRhYmxlIHRkIHtcbiAgYm9yZGVyLXdpZHRoOiAwcHggMXB4IDBweCAwcHg7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XG59XG50YWJsZSB0ZCBlbSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tbGVmdDogNXB4O1xuICBmb250LXNpemU6IDEwcHg7XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbn1cbnRhYmxlIHRoIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjtcbiAgY29sb3I6ICM4ZDhkOGQ7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component.ts":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component.ts ***!
  \**********************************************************************************************************************/
/*! exports provided: MerchantEscrowDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantEscrowDetailComponent", function() { return MerchantEscrowDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_escrow_transaction_escrow_transaction_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/escrow-transaction/escrow-transaction.service */ "./src/app/services/escrow-transaction/escrow-transaction.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var MerchantEscrowDetailComponent = /** @class */ (function () {
    function MerchantEscrowDetailComponent(route, EscrowTransactionService) {
        this.route = route;
        this.EscrowTransactionService = EscrowTransactionService;
    }
    MerchantEscrowDetailComponent.prototype.ngOnInit = function () {
        this.firstload();
    };
    MerchantEscrowDetailComponent.prototype.firstload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                    var escrowID, result;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                escrowID = params.id;
                                this.service = this.EscrowTransactionService;
                                return [4 /*yield*/, this.EscrowTransactionService.detailEscrowtransaction(escrowID)];
                            case 1:
                                result = _a.sent();
                                this.data = result.result;
                                return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    MerchantEscrowDetailComponent.prototype.back = function () {
        window.history.back();
    };
    MerchantEscrowDetailComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _services_escrow_transaction_escrow_transaction_service__WEBPACK_IMPORTED_MODULE_2__["EscrowTransactionService"] }
    ]; };
    MerchantEscrowDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-escrow-detail',
            template: __webpack_require__(/*! raw-loader!./merchant-escrow-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component.html"),
            styles: [__webpack_require__(/*! ./merchant-escrow-detail.component.scss */ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_escrow_transaction_escrow_transaction_service__WEBPACK_IMPORTED_MODULE_2__["EscrowTransactionService"]])
    ], MerchantEscrowDetailComponent);
    return MerchantEscrowDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-my-escrow.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-my-escrow/merchant-my-escrow.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "._container {\n  padding: 20px;\n}\n\n::ng-deep td.withdrawal_status {\n  font-size: 20px;\n}\n\n.menu-bar {\n  width: 100%;\n  flex-direction: row;\n}\n\n.withdraw {\n  position: relative;\n  margin-left: 10px;\n  border-radius: 5px;\n  font-size: 14px;\n  width: 20%;\n}\n\ntable {\n  width: 100%;\n  font-size: 14px;\n  box-shadow: 5px 5px 10px #999;\n  border-radius: 5px;\n  text-transform: capitalize;\n  overflow: hidden;\n}\n\ntable td {\n  border-width: 0px 1px 0px 0px;\n  padding: 15px;\n  vertical-align: top;\n}\n\ntable td em {\n  display: block;\n  margin-left: 5px;\n  font-size: 10px;\n  font-style: italic;\n}\n\ntable tr:hover {\n  background-color: lightblue;\n}\n\ntable .pending {\n  border-radius: 30px;\n  background-color: #ffe6d5;\n  color: #ee680c;\n  width: 80px;\n  height: 35px;\n  font-size: 12px;\n  border: none;\n}\n\ntable .success {\n  background-color: #daf9f1;\n  color: #2ea56e;\n  border-radius: 30px;\n  width: 80px;\n  height: 35px;\n  font-size: 12px;\n  border: none;\n}\n\ntable .hold {\n  background-color: #f2e8ff;\n  color: #8b5acf;\n  border-radius: 30px;\n  width: 80px;\n  height: 35px;\n  font-size: 12px;\n  border: none;\n}\n\ntable .refunded {\n  border-radius: 30px;\n  background-color: #e4ecff;\n  color: #4271df;\n  width: 80px;\n  height: 35px;\n  font-size: 12px;\n  border: none;\n}\n\ntable .rejected {\n  border-radius: 30px;\n  background-color: #fcecf0;\n  color: #ec3b60;\n  width: 80px;\n  height: 35px;\n  font-size: 12px;\n  border: none;\n}\n\ntable td button {\n  pointer-events: none;\n  font-weight: bold;\n}\n\ntable th {\n  background-color: #f2f2f2;\n  color: #8d8d8d;\n  padding: 15px;\n  font-weight: bold;\n  text-transform: capitalize;\n}\n\n.logo {\n  width: 10%;\n}\n\n.div-transaction {\n  display: flex;\n  flex-direction: row;\n  font-size: 12px;\n  width: 100%;\n  padding: 10px;\n}\n\n.div-transaction label {\n  margin-bottom: 0px;\n}\n\n.div-transaction div.searchbar {\n  flex-wrap: wrap;\n  display: inherit;\n  width: calc(100% - 140px);\n  padding-left: 10px;\n  justify-content: flex-end;\n}\n\n.div-transaction div.searchbar div {\n  display: flex;\n  align-items: center;\n}\n\n.div-transaction div.searchbar div label {\n  margin-right: 5px;\n}\n\n#search {\n  margin-right: 20px;\n}\n\n#search-field {\n  margin-right: 10px;\n}\n\n#search-text {\n  padding-right: 23px;\n}\n\n#search-icon {\n  margin-left: -20px;\n  margin-right: 20px;\n}\n\n#card-transaction {\n  width: 100%;\n  align-items: baseline;\n}\n\n#transaction {\n  font-size: 15px;\n}\n\n.download {\n  margin-top: 11px;\n}\n\n.div-footer div {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: auto;\n  padding: 0px;\n  margin-bottom: 5px;\n  border-radius: 10px;\n}\n\n.div-footer div button {\n  background: white;\n  color: black;\n}\n\n.div-footer div button:hover {\n  background-color: #4271df;\n  color: white;\n  border-radius: 10px;\n}\n\n.modal.false {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n\n.modal.true {\n  display: block;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0px;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: white;\n  opacity: 1;\n  /* Fallback color */\n  /* Black w/ opacity */\n}\n\n.modal-content {\n  margin-left: 256px;\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  margin-top: 200px;\n  padding: 0;\n  width: 30%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n\n/* The Close Button */\n\n.close {\n  color: white;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n\n.close:hover,\n.close:focus {\n  color: #000;\n  text-decoration: none;\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9tZXJjaGFudC1teS1lc2Nyb3cvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtZXJjaGFudC1wb3J0YWxcXG1lcmNoYW50LW15LWVzY3Jvd1xcbWVyY2hhbnQtbXktZXNjcm93LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LW15LWVzY3Jvdy9tZXJjaGFudC1teS1lc2Nyb3cuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxhQUFBO0FDQ0Q7O0FERUE7RUFDQyxlQUFBO0FDQ0Q7O0FERUE7RUFDQyxXQUFBO0VBQ0EsbUJBQUE7QUNDRDs7QURFQTtFQUdDLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FDREQ7O0FESUE7RUFDQyxXQUFBO0VBR0EsZUFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtFQUNBLGdCQUFBO0FDSEQ7O0FES0M7RUFHQyw2QkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQ0xGOztBRE9FO0VBQ0MsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDTEg7O0FEU0M7RUFDQywyQkFBQTtBQ1BGOztBRFVDO0VBQ0MsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDUkY7O0FEV0M7RUFDQyx5QkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNURjs7QURZQztFQUNDLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ1ZGOztBRGFDO0VBQ0MsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDWEY7O0FEY0M7RUFDQyxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNaRjs7QURlQztFQUNDLG9CQUFBO0VBQ0EsaUJBQUE7QUNiRjs7QURnQkM7RUFDQyx5QkFBQTtFQUNBLGNBQUE7RUFFQSxhQUFBO0VBRUEsaUJBQUE7RUFDQSwwQkFBQTtBQ2hCRjs7QUQwQkE7RUFDQyxVQUFBO0FDdkJEOztBRDRCQTtFQUNDLGFBQUE7RUFFQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtBQzFCRDs7QUQyQkM7RUFDQyxrQkFBQTtBQ3pCRjs7QUQyQkM7RUFDQyxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7QUN6QkY7O0FEMEJFO0VBQ0MsYUFBQTtFQUNBLG1CQUFBO0FDeEJIOztBRHlCRztFQUNDLGlCQUFBO0FDdkJKOztBRDZCQTtFQUNDLGtCQUFBO0FDMUJEOztBRDZCQTtFQUNDLGtCQUFBO0FDMUJEOztBRDZCQTtFQUNDLG1CQUFBO0FDMUJEOztBRDZCQTtFQUNDLGtCQUFBO0VBQ0Esa0JBQUE7QUMxQkQ7O0FENkJBO0VBQ0MsV0FBQTtFQUNBLHFCQUFBO0FDMUJEOztBRDZCQTtFQUNDLGVBQUE7QUMxQkQ7O0FENkJBO0VBQ0MsZ0JBQUE7QUMxQkQ7O0FEOEJDO0VBQ0MsMEJBQUE7RUFBQSx1QkFBQTtFQUFBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUVBLG1CQUFBO0FDNUJGOztBRDhCRTtFQUNDLGlCQUFBO0VBRUEsWUFBQTtBQzdCSDs7QURnQ0U7RUFDQyx5QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQzlCSDs7QURzQ0E7RUFDQyxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxPQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLDRCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EscUJBQUE7QUNuQ0Q7O0FEc0NFO0VBQ0QsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsU0FBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSw0QkFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBRUEscUJBQUE7QUNwQ0Q7O0FEdUNFO0VBQ0Qsa0JBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtFQUVBLFVBQUE7RUFDQSw0RUFBQTtFQUNBLGtDQUFBO1VBQUEsMEJBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0Esa0JBQUE7QUNyQ0Q7O0FEd0NFLHFCQUFBOztBQUNBO0VBQ0QsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNyQ0Q7O0FEd0NFOztFQUVELFdBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7QUNyQ0QiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LW15LWVzY3Jvdy9tZXJjaGFudC1teS1lc2Nyb3cuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuX2NvbnRhaW5lciB7XHJcblx0cGFkZGluZzogMjBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIHRkLndpdGhkcmF3YWxfc3RhdHVzIHtcclxuXHRmb250LXNpemU6IDIwcHg7XHJcbn1cclxuXHJcbi5tZW51LWJhciB7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0ZmxleC1kaXJlY3Rpb246IHJvdztcclxufVxyXG5cclxuLndpdGhkcmF3IHtcclxuXHQvLyBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG5cdC8vIGZsb2F0OiByaWdodDtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0bWFyZ2luLWxlZnQ6IDEwcHg7XHJcblx0Ym9yZGVyLXJhZGl1czogNXB4O1xyXG5cdGZvbnQtc2l6ZTogMTRweDtcclxuXHR3aWR0aDogMjAlO1xyXG59XHJcblxyXG50YWJsZSB7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0Ly8gYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuXHQvLyBib3JkZXI6IDFweCBzb2xpZCAjMzMzO1xyXG5cdGZvbnQtc2l6ZTogMTRweDtcclxuXHRib3gtc2hhZG93OiA1cHggNXB4IDEwcHggIzk5OTtcclxuXHRib3JkZXItcmFkaXVzOiA1cHg7XHJcblx0dGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcblx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcblx0dGQge1xyXG5cdFx0Ly8gIGJvcmRlci1zdHlsZTpzb2xpZDtcclxuXHRcdC8vICBib3JkZXItY29sb3I6ICMzMzM7XHJcblx0XHRib3JkZXItd2lkdGg6IDBweCAxcHggMHB4IDBweDtcclxuXHRcdHBhZGRpbmc6IDE1cHg7XHJcblx0XHR2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG5cclxuXHRcdGVtIHtcclxuXHRcdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0XHRcdG1hcmdpbi1sZWZ0OiA1cHg7XHJcblx0XHRcdGZvbnQtc2l6ZTogMTBweDtcclxuXHRcdFx0Zm9udC1zdHlsZTogaXRhbGljO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0dHI6aG92ZXIge1xyXG5cdFx0YmFja2dyb3VuZC1jb2xvcjogbGlnaHRibHVlO1xyXG5cdH1cclxuXHJcblx0LnBlbmRpbmcge1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogMzBweDtcclxuXHRcdGJhY2tncm91bmQtY29sb3I6ICNmZmU2ZDU7XHJcblx0XHRjb2xvcjogI2VlNjgwYztcclxuXHRcdHdpZHRoOiA4MHB4O1xyXG5cdFx0aGVpZ2h0OiAzNXB4O1xyXG5cdFx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdFx0Ym9yZGVyOiBub25lO1xyXG5cdH1cclxuXHJcblx0LnN1Y2Nlc3Mge1xyXG5cdFx0YmFja2dyb3VuZC1jb2xvcjogI2RhZjlmMTtcclxuXHRcdGNvbG9yOiAjMmVhNTZlO1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogMzBweDtcclxuXHRcdHdpZHRoOiA4MHB4O1xyXG5cdFx0aGVpZ2h0OiAzNXB4O1xyXG5cdFx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdFx0Ym9yZGVyOiBub25lO1xyXG5cdH1cclxuXHJcblx0LmhvbGQge1xyXG5cdFx0YmFja2dyb3VuZC1jb2xvcjogI2YyZThmZjtcclxuXHRcdGNvbG9yOiAjOGI1YWNmO1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogMzBweDtcclxuXHRcdHdpZHRoOiA4MHB4O1xyXG5cdFx0aGVpZ2h0OiAzNXB4O1xyXG5cdFx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdFx0Ym9yZGVyOiBub25lO1xyXG5cdH1cclxuXHJcblx0LnJlZnVuZGVkIHtcclxuXHRcdGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcblx0XHRiYWNrZ3JvdW5kLWNvbG9yOiAjZTRlY2ZmO1xyXG5cdFx0Y29sb3I6ICM0MjcxZGY7XHJcblx0XHR3aWR0aDogODBweDtcclxuXHRcdGhlaWdodDogMzVweDtcclxuXHRcdGZvbnQtc2l6ZTogMTJweDtcclxuXHRcdGJvcmRlcjogbm9uZTtcclxuXHR9XHJcblxyXG5cdC5yZWplY3RlZCB7XHJcblx0XHRib3JkZXItcmFkaXVzOiAzMHB4O1xyXG5cdFx0YmFja2dyb3VuZC1jb2xvcjogI2ZjZWNmMDtcclxuXHRcdGNvbG9yOiAjZWMzYjYwO1xyXG5cdFx0d2lkdGg6IDgwcHg7XHJcblx0XHRoZWlnaHQ6IDM1cHg7XHJcblx0XHRmb250LXNpemU6IDEycHg7XHJcblx0XHRib3JkZXI6IG5vbmU7XHJcblx0fVxyXG5cclxuXHR0ZCBidXR0b24ge1xyXG5cdFx0cG9pbnRlci1ldmVudHM6IG5vbmU7XHJcblx0XHRmb250LXdlaWdodDogYm9sZDtcclxuXHR9XHJcblxyXG5cdHRoIHtcclxuXHRcdGJhY2tncm91bmQtY29sb3I6ICNmMmYyZjI7XHJcblx0XHRjb2xvcjogIzhkOGQ4ZDtcclxuXHRcdC8vIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XHJcblx0XHRwYWRkaW5nOiAxNXB4O1xyXG5cdFx0Ly8gdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0XHR0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuXHRcdC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcblx0fVxyXG5cclxuXHQvLyAucGFnaW5hdGlvbntcclxuXHQvLyAgICAgbWFyZ2luLXRvcDoxMHB4O1xyXG5cdC8vICAgICBmbG9hdDpyaWdodDtcclxuXHQvLyB9XHJcbn1cclxuXHJcbi5sb2dvIHtcclxuXHR3aWR0aDogMTAlO1xyXG5cdC8vIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHQvLyB0b3A6IDEzcHg7XHJcbn1cclxuXHJcbi5kaXYtdHJhbnNhY3Rpb24ge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0Ly8gZmxleC13cmFwOiB3cmFwO1xyXG5cdGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcblx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdHBhZGRpbmc6IDEwcHg7XHJcblx0bGFiZWwge1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogMHB4O1xyXG5cdH1cclxuXHRkaXYuc2VhcmNoYmFyIHtcclxuXHRcdGZsZXgtd3JhcDogd3JhcDtcclxuXHRcdGRpc3BsYXk6IGluaGVyaXQ7XHJcblx0XHR3aWR0aDogY2FsYygxMDAlIC0gMTQwcHgpO1xyXG5cdFx0cGFkZGluZy1sZWZ0OiAxMHB4O1xyXG5cdFx0anVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuXHRcdGRpdiB7XHJcblx0XHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0XHRcdGxhYmVsIHtcclxuXHRcdFx0XHRtYXJnaW4tcmlnaHQ6IDVweDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuI3NlYXJjaCB7XHJcblx0bWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG59XHJcblxyXG4jc2VhcmNoLWZpZWxkIHtcclxuXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbiNzZWFyY2gtdGV4dCB7XHJcblx0cGFkZGluZy1yaWdodDogMjNweDtcclxufVxyXG5cclxuI3NlYXJjaC1pY29uIHtcclxuXHRtYXJnaW4tbGVmdDogLTIwcHg7XHJcblx0bWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG59XHJcblxyXG4jY2FyZC10cmFuc2FjdGlvbiB7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0YWxpZ24taXRlbXM6IGJhc2VsaW5lO1xyXG59XHJcblxyXG4jdHJhbnNhY3Rpb24ge1xyXG5cdGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5cclxuLmRvd25sb2FkIHtcclxuXHRtYXJnaW4tdG9wOiAxMXB4O1xyXG59XHJcblxyXG4uZGl2LWZvb3RlciB7XHJcblx0ZGl2IHtcclxuXHRcdHdpZHRoOiBmaXQtY29udGVudDtcclxuXHRcdGhlaWdodDogYXV0bztcclxuXHRcdHBhZGRpbmc6IDBweDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDVweDtcclxuXHRcdC8vIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuXHRcdGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcblxyXG5cdFx0YnV0dG9uIHtcclxuXHRcdFx0YmFja2dyb3VuZDogd2hpdGU7XHJcblx0XHRcdC8vIGJvcmRlci1yYWRpdXM6MTBweDtcclxuXHRcdFx0Y29sb3I6IGJsYWNrO1xyXG5cdFx0fVxyXG5cclxuXHRcdGJ1dHRvbjpob3ZlciB7XHJcblx0XHRcdGJhY2tncm91bmQtY29sb3I6ICM0MjcxZGY7XHJcblx0XHRcdGNvbG9yOiB3aGl0ZTtcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czogMTBweDtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5tb2RhbC5mYWxzZSB7XHJcblx0ZGlzcGxheTogbm9uZTtcclxuXHQvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xyXG5cdHBvc2l0aW9uOiBmaXhlZDtcclxuXHQvKiBTdGF5IGluIHBsYWNlICovXHJcblx0ei1pbmRleDogMTtcclxuXHQvKiBTaXQgb24gdG9wICovXHJcblx0cGFkZGluZy10b3A6IDEwMHB4O1xyXG5cdC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cclxuXHRsZWZ0OiAwO1xyXG5cdHRvcDogMDtcclxuXHR3aWR0aDogMTAwJTtcclxuXHQvKiBGdWxsIHdpZHRoICovXHJcblx0aGVpZ2h0OiAxMDAlO1xyXG5cdC8qIEZ1bGwgaGVpZ2h0ICovXHJcblx0b3ZlcmZsb3c6IGF1dG87XHJcblx0LyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcblx0LyogRmFsbGJhY2sgY29sb3IgKi9cclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XHJcblx0LyogQmxhY2sgdy8gb3BhY2l0eSAqL1xyXG4gIH1cclxuICBcclxuICAubW9kYWwudHJ1ZSB7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcblx0LyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cclxuXHRwb3NpdGlvbjogZml4ZWQ7XHJcblx0LyogU3RheSBpbiBwbGFjZSAqL1xyXG5cdHotaW5kZXg6IDE7XHJcblx0LyogU2l0IG9uIHRvcCAqL1xyXG5cdHBhZGRpbmctdG9wOiAxMDBweDtcclxuXHQvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXHJcblx0bGVmdDogMHB4O1xyXG5cdHRvcDogMDtcclxuXHR3aWR0aDogMTAwJTtcclxuXHQvKiBGdWxsIHdpZHRoICovXHJcblx0aGVpZ2h0OiAxMDAlO1xyXG5cdC8qIEZ1bGwgaGVpZ2h0ICovXHJcblx0b3ZlcmZsb3c6IGF1dG87XHJcblx0LyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuXHRvcGFjaXR5OiAxO1xyXG5cdC8qIEZhbGxiYWNrIGNvbG9yICovXHJcblx0Ly8gYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcblx0LyogQmxhY2sgdy8gb3BhY2l0eSAqL1xyXG4gIH1cclxuICBcclxuICAubW9kYWwtY29udGVudCB7XHJcblx0bWFyZ2luLWxlZnQ6IDI1NnB4O1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZmVmZWZlO1xyXG5cdG1hcmdpbjogYXV0bztcclxuXHRtYXJnaW4tdG9wOjIwMHB4O1xyXG5cdHBhZGRpbmc6IDA7XHJcblx0Ly8gYm9yZGVyOiAxcHggc29saWQgIzg4ODtcclxuXHR3aWR0aDogMzAlO1xyXG5cdGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XHJcblx0YW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XHJcblx0YW5pbWF0aW9uLWR1cmF0aW9uOiAwLjRzO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgXHJcbiAgLyogVGhlIENsb3NlIEJ1dHRvbiAqL1xyXG4gIC5jbG9zZSB7XHJcblx0Y29sb3I6IHdoaXRlO1xyXG5cdGZsb2F0OiByaWdodDtcclxuXHRmb250LXNpemU6IDI4cHg7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgfVxyXG4gIFxyXG4gIC5jbG9zZTpob3ZlcixcclxuICAuY2xvc2U6Zm9jdXMge1xyXG5cdGNvbG9yOiAjMDAwO1xyXG5cdHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuXHRjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG4gIFxyXG5cclxuXHJcbi8vIC5jdXJyZW5jeSB7XHJcbi8vIFx0Zm9udC1zaXplOiAyMnB4O1xyXG4vLyBcdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuLy8gXHRsZWZ0OiA3MHB4O1xyXG4vLyBcdGJvdHRvbTogMjVweDtcclxuLy8gfVxyXG5cclxuLy8gI3NlYXJjaC10ZXh0e1xyXG5cclxuLy8gfVxyXG4iLCIuX2NvbnRhaW5lciB7XG4gIHBhZGRpbmc6IDIwcHg7XG59XG5cbjo6bmctZGVlcCB0ZC53aXRoZHJhd2FsX3N0YXR1cyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLm1lbnUtYmFyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG59XG5cbi53aXRoZHJhdyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB3aWR0aDogMjAlO1xufVxuXG50YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJveC1zaGFkb3c6IDVweCA1cHggMTBweCAjOTk5O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxudGFibGUgdGQge1xuICBib3JkZXItd2lkdGg6IDBweCAxcHggMHB4IDBweDtcbiAgcGFkZGluZzogMTVweDtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbn1cbnRhYmxlIHRkIGVtIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xufVxudGFibGUgdHI6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGJsdWU7XG59XG50YWJsZSAucGVuZGluZyB7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmU2ZDU7XG4gIGNvbG9yOiAjZWU2ODBjO1xuICB3aWR0aDogODBweDtcbiAgaGVpZ2h0OiAzNXB4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGJvcmRlcjogbm9uZTtcbn1cbnRhYmxlIC5zdWNjZXNzIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2RhZjlmMTtcbiAgY29sb3I6ICMyZWE1NmU7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIHdpZHRoOiA4MHB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgYm9yZGVyOiBub25lO1xufVxudGFibGUgLmhvbGQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJlOGZmO1xuICBjb2xvcjogIzhiNWFjZjtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgd2lkdGg6IDgwcHg7XG4gIGhlaWdodDogMzVweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBib3JkZXI6IG5vbmU7XG59XG50YWJsZSAucmVmdW5kZWQge1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTRlY2ZmO1xuICBjb2xvcjogIzQyNzFkZjtcbiAgd2lkdGg6IDgwcHg7XG4gIGhlaWdodDogMzVweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBib3JkZXI6IG5vbmU7XG59XG50YWJsZSAucmVqZWN0ZWQge1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmNlY2YwO1xuICBjb2xvcjogI2VjM2I2MDtcbiAgd2lkdGg6IDgwcHg7XG4gIGhlaWdodDogMzVweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBib3JkZXI6IG5vbmU7XG59XG50YWJsZSB0ZCBidXR0b24ge1xuICBwb2ludGVyLWV2ZW50czogbm9uZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG50YWJsZSB0aCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMmYyZjI7XG4gIGNvbG9yOiAjOGQ4ZDhkO1xuICBwYWRkaW5nOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5sb2dvIHtcbiAgd2lkdGg6IDEwJTtcbn1cblxuLmRpdi10cmFuc2FjdGlvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG4uZGl2LXRyYW5zYWN0aW9uIGxhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmRpdi10cmFuc2FjdGlvbiBkaXYuc2VhcmNoYmFyIHtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBkaXNwbGF5OiBpbmhlcml0O1xuICB3aWR0aDogY2FsYygxMDAlIC0gMTQwcHgpO1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG4uZGl2LXRyYW5zYWN0aW9uIGRpdi5zZWFyY2hiYXIgZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5kaXYtdHJhbnNhY3Rpb24gZGl2LnNlYXJjaGJhciBkaXYgbGFiZWwge1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuI3NlYXJjaCB7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbn1cblxuI3NlYXJjaC1maWVsZCB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuI3NlYXJjaC10ZXh0IHtcbiAgcGFkZGluZy1yaWdodDogMjNweDtcbn1cblxuI3NlYXJjaC1pY29uIHtcbiAgbWFyZ2luLWxlZnQ6IC0yMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG5cbiNjYXJkLXRyYW5zYWN0aW9uIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGFsaWduLWl0ZW1zOiBiYXNlbGluZTtcbn1cblxuI3RyYW5zYWN0aW9uIHtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG4uZG93bmxvYWQge1xuICBtYXJnaW4tdG9wOiAxMXB4O1xufVxuXG4uZGl2LWZvb3RlciBkaXYge1xuICB3aWR0aDogZml0LWNvbnRlbnQ7XG4gIGhlaWdodDogYXV0bztcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG4uZGl2LWZvb3RlciBkaXYgYnV0dG9uIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGNvbG9yOiBibGFjaztcbn1cbi5kaXYtZm9vdGVyIGRpdiBidXR0b246aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDI3MWRmO1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi5tb2RhbC5mYWxzZSB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xuICB6LWluZGV4OiAxO1xuICAvKiBTaXQgb24gdG9wICovXG4gIHBhZGRpbmctdG9wOiAxMDBweDtcbiAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xuICBsZWZ0OiAwO1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICAvKiBGdWxsIHdpZHRoICovXG4gIGhlaWdodDogMTAwJTtcbiAgLyogRnVsbCBoZWlnaHQgKi9cbiAgb3ZlcmZsb3c6IGF1dG87XG4gIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICAvKiBGYWxsYmFjayBjb2xvciAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cbn1cblxuLm1vZGFsLnRydWUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cbiAgcG9zaXRpb246IGZpeGVkO1xuICAvKiBTdGF5IGluIHBsYWNlICovXG4gIHotaW5kZXg6IDE7XG4gIC8qIFNpdCBvbiB0b3AgKi9cbiAgcGFkZGluZy10b3A6IDEwMHB4O1xuICAvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXG4gIGxlZnQ6IDBweDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgLyogRnVsbCB3aWR0aCAqL1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIEZ1bGwgaGVpZ2h0ICovXG4gIG92ZXJmbG93OiBhdXRvO1xuICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgb3BhY2l0eTogMTtcbiAgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgLyogQmxhY2sgdy8gb3BhY2l0eSAqL1xufVxuXG4ubW9kYWwtY29udGVudCB7XG4gIG1hcmdpbi1sZWZ0OiAyNTZweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmVmZWZlO1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi10b3A6IDIwMHB4O1xuICBwYWRkaW5nOiAwO1xuICB3aWR0aDogMzAlO1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICBhbmltYXRpb24tbmFtZTogYW5pbWF0ZXRvcDtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjRzO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi8qIFRoZSBDbG9zZSBCdXR0b24gKi9cbi5jbG9zZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDI4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uY2xvc2U6aG92ZXIsXG4uY2xvc2U6Zm9jdXMge1xuICBjb2xvcjogIzAwMDtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-my-escrow.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-my-escrow/merchant-my-escrow.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: MerchantMyEscrowComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantMyEscrowComponent", function() { return MerchantMyEscrowComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_escrow_transaction_escrow_transaction_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/escrow-transaction/escrow-transaction.service */ "./src/app/services/escrow-transaction/escrow-transaction.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/merchant/merchant.service */ "./src/app/services/merchant/merchant.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var MerchantMyEscrowComponent = /** @class */ (function () {
    function MerchantMyEscrowComponent(EscrowTransactionService, router, merchantService, memberService) {
        this.EscrowTransactionService = EscrowTransactionService;
        this.router = router;
        this.merchantService = merchantService;
        this.memberService = memberService;
        this.EscrowData = [];
        this.tableFormat = {
            title: 'My Escrow Transactions',
            label_headers: [
                // {label: 'Owner ID', visible: true, type: 'string', data_row_name: 'owner_id'},
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'Description', visible: true, type: 'string', data_row_name: 'description' },
                { label: 'Nominal', visible: true, type: 'number', data_row_name: 'how_much' },
                { label: 'No. Ref', visible: true, type: 'string', data_row_name: 'record_id' },
                { label: 'Debit', visible: true, type: 'escrow-d', data_row_name: 'how_much' },
                { label: 'Credit', visible: true, type: 'escrow-c', data_row_name: 'how_much' },
                { label: 'Type', visible: true, options: ['withdrawal', 'plus', 'minus', 'hold'], type: 'list-escrow', data_row_name: 'type' },
                { label: 'status', visible: true, options: ['PENDING', 'RELEASED', 'HOLD', 'SUCCEESS'], type: 'list-escrow-status', data_row_name: 'transaction_status' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'EscrowData',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.pageSize = 50;
        this.pageNumbering = [];
        this.currentPage = 1;
        this.setPin = false;
        this.number = [];
        this.search = {};
        this.pin = {
            password: '',
            new_pin: '',
            repeat_new_pin: ''
        };
        this.status = [
            {
                name: 'Pending', value: 'pending'
            },
            {
                name: 'Success', value: 'success'
            },
            {
                name: 'Refunded', value: 'refunded'
            },
            {
                name: 'Hold', value: 'hold'
            },
            {
                name: 'Rejected', value: 'rejected'
            }
        ];
        this.types = [
            { name: 'Debit', value: 'debit' },
            { name: 'Credit', value: 'credit' },
            { name: 'Withdrawal', value: 'withdrawal' },
            { name: 'Hold', value: 'hold' },
        ];
        this.filter = "order_id";
        this.filterResult = { order_date: -1 };
        this.pointstransactionDetail = false;
    }
    MerchantMyEscrowComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MerchantMyEscrowComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var member, resultDetail, result, resBalance, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        member = void 0;
                        return [4 /*yield*/, this.merchantService.getDetail()];
                    case 1:
                        resultDetail = _a.sent();
                        this.member = resultDetail.result;
                        this.service = this.EscrowTransactionService;
                        return [4 /*yield*/, this.EscrowTransactionService.getEscrowtransactionLint()];
                    case 2:
                        result = _a.sent();
                        console.log("result", result);
                        return [4 /*yield*/, this.EscrowTransactionService.getEscrowBalance()];
                    case 3:
                        resBalance = _a.sent();
                        console.log("resbalance", resBalance);
                        this.myBalance = resBalance.result.balance;
                        this.totalPage = result.result.total_page;
                        console.log(this.totalPage);
                        this.EscrowData = result.result.values;
                        // this.pagesTotals(result.result.total_page)
                        // this.pagesTotals(this.totalPage);
                        this.EscrowData.forEach(function (element, index) {
                            _this.number.push(index + 1);
                            // console.log("values", this.EscrowData.values)
                            // console.log("number", this.number)
                        });
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    MerchantMyEscrowComponent.prototype.pagesTotals = function (totalPage) {
        this.pageNumbering = [];
        this.total_page = totalPage;
        this.pageNumbering = this.pagination(this.currentPage, totalPage);
    };
    MerchantMyEscrowComponent.prototype.callDetail = function (escrowID) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                try {
                    result = void 0;
                    this.router.navigate(['merchant-portal/my-money/detail'], { queryParams: { id: escrowID } });
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    MerchantMyEscrowComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.pointstransactionDetail = false;
                return [2 /*return*/];
            });
        });
    };
    MerchantMyEscrowComponent.prototype.moneyDetail = function () {
        console.log(true);
    };
    MerchantMyEscrowComponent.prototype.checkAccount = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.memberService.getMemberDetail()];
                    case 1:
                        result = _a.sent();
                        this.memberDetail = result.result;
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.log((error_1.message));
                        return [3 /*break*/, 3];
                    case 3:
                        if (!this.member.withdrawal_bank_account) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Sorry", "You don't have bank account to access this page, fill <b>Payment Info</b> first ", "error");
                            this.router.navigate(['merchant-portal/profile']);
                            console.log(true);
                        }
                        if (this.memberDetail.access_pin != "enabled") {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Sorry", "You need to set PIN to withdraw", "error");
                            this.setPin = true;
                        }
                        if (this.memberDetail.access_pin == "enabled") {
                            this.router.navigate(['merchant-portal/my-money/withdrawal']);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantMyEscrowComponent.prototype.pagination = function (c, m) {
        var current = c, last = m, delta = 2, left = current - delta, right = current + delta + 1, range = [], rangeWithDots = [], l;
        for (var i = 1; i <= last; i++) {
            if (i == 1 || i == last || i >= left && i < right) {
                range.push(i);
            }
        }
        for (var _i = 0, range_1 = range; _i < range_1.length; _i++) {
            var i = range_1[_i];
            if (l) {
                if (i - l === 2) {
                    rangeWithDots.push(l + 1);
                }
                else if (i - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(i);
            l = i;
        }
        return rangeWithDots;
    };
    MerchantMyEscrowComponent.prototype.onChangePage = function (pageNumber) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (pageNumber <= 0) {
                    pageNumber = 1;
                }
                if (pageNumber >= this.total_page) {
                    pageNumber = this.total_page;
                }
                this.currentPage = pageNumber;
                console.log(pageNumber, this.currentPage, this.total_page);
                return [2 /*return*/];
            });
        });
    };
    MerchantMyEscrowComponent.prototype.checkPin = function () {
        console.log("test");
    };
    MerchantMyEscrowComponent.prototype.closeDialog = function () {
        this.setPin = false;
    };
    MerchantMyEscrowComponent.prototype.verify = function () {
        return __awaiter(this, void 0, void 0, function () {
            var settingPin, password, newPin, repeatNewPin, result, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        settingPin = this.pin;
                        password = settingPin.password.toString();
                        newPin = settingPin.new_pin.toString();
                        repeatNewPin = settingPin.repeat_new_pin.toString();
                        // settingPin.password = settingPin.password.toString();
                        // console.log(settingPin)
                        console.log(password);
                        console.log(newPin);
                        console.log(repeatNewPin);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.memberService.setNewPin(settingPin)];
                    case 2:
                        result = _a.sent();
                        if (result.result) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire("Congratulations", "Your PIN has been registered to LOCARD", "success");
                            // this.retypePin= false;
                            this.setPin = false;
                            // this.router.navigate(['merchant-portal/profile']);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        console.log((error_2.message));
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MerchantMyEscrowComponent.ctorParameters = function () { return [
        { type: _services_escrow_transaction_escrow_transaction_service__WEBPACK_IMPORTED_MODULE_1__["EscrowTransactionService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_4__["MerchantService"] },
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] }
    ]; };
    MerchantMyEscrowComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-my-escrow',
            template: __webpack_require__(/*! raw-loader!./merchant-my-escrow.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-my-escrow/merchant-my-escrow.component.html"),
            styles: [__webpack_require__(/*! ./merchant-my-escrow.component.scss */ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-my-escrow.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_escrow_transaction_escrow_transaction_service__WEBPACK_IMPORTED_MODULE_1__["EscrowTransactionService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_4__["MerchantService"], _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"]])
    ], MerchantMyEscrowComponent);
    return MerchantMyEscrowComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component.scss":
/*!******************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component.scss ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".-container {\n  padding-top: 20px;\n}\n\n.card {\n  padding: 10px;\n}\n\n.card .button-container {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.inputs {\n  display: flex;\n}\n\n.inputs .mat-form-field-appearance-fill .mat-form-field-flex {\n  padding: 0 !important;\n}\n\n#request {\n  font-size: 11px;\n  margin-bottom: 10px;\n  border-radius: 5px;\n  padding-left: 20px;\n  padding-right: 20px;\n}\n\n.amount {\n  position: relative;\n  left: 120px;\n  bottom: 80px;\n  padding-bottom: 30;\n  height: 10px;\n}\n\n.alert {\n  text-align: center;\n  width: 100%;\n  font-size: 20px;\n  font-weight: bold;\n  background-color: red;\n  color: white;\n}\n\n.modal.false {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n\n.modal.true {\n  display: block;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  /* Location of the box */\n  left: 0px;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: rgba(0, 0, 0, 0.5);\n  opacity: 1;\n  /* Fallback color */\n  /* Black w/ opacity */\n}\n\n.modal-content {\n  margin-left: 256px;\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  margin-top: 200px;\n  padding: 0;\n  width: 30%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n\n/* The Close Button */\n\n.close {\n  color: black;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n\n.forgot {\n  float: right;\n  /* text-align: right; */\n  margin-right: 35px;\n}\n\n.close:hover,\n.close:focus {\n  color: #000;\n  text-decoration: none;\n  cursor: pointer;\n}\n\n.modal-footer .btn-primary {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9tZXJjaGFudC1teS1lc2Nyb3cvbWVyY2hhbnQtd2l0aGRyYXdhbC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1lcmNoYW50LXBvcnRhbFxcbWVyY2hhbnQtbXktZXNjcm93XFxtZXJjaGFudC13aXRoZHJhd2FsXFxtZXJjaGFudC13aXRoZHJhd2FsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LW15LWVzY3Jvdy9tZXJjaGFudC13aXRoZHJhd2FsL21lcmNoYW50LXdpdGhkcmF3YWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBS0E7RUFDSSxpQkFBQTtBQ0pKOztBRE9BO0VBQ0ksYUFBQTtBQ0pKOztBREtJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNIUjs7QURPQTtFQUNJLGFBQUE7QUNKSjs7QURLSTtFQUNJLHFCQUFBO0FDSFI7O0FEUUE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNMSjs7QURRQTtFQUNHLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNMSDs7QURRQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtBQ0xKOztBRE9BO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSw0QkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQ0FBQTtFQUNBLHFCQUFBO0FDSko7O0FET0U7RUFDRSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUVBLHdCQUFBO0VBQ0EsU0FBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSw0QkFBQTtFQUNBLG9DQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBRUEscUJBQUE7QUNOSjs7QURTRTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFFQSxVQUFBO0VBQ0EsNEVBQUE7RUFDQSxrQ0FBQTtVQUFBLDBCQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLGtCQUFBO0FDUEo7O0FEU0UscUJBQUE7O0FBQ0Y7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ05KOztBRFFFO0VBQ0UsWUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUNMSjs7QURPRTs7RUFFRSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0FDSko7O0FET007RUFDSSxXQUFBO0FDSlYiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LW15LWVzY3Jvdy9tZXJjaGFudC13aXRoZHJhd2FsL21lcmNoYW50LXdpdGhkcmF3YWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAuX2NvbnRhaW5lcntcclxuLy8gICAgIHBhZGRpbmc6IDIwcHg7XHJcbi8vICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4vLyB9XHJcblxyXG4uLWNvbnRhaW5lcntcclxuICAgIHBhZGRpbmctdG9wOjIwcHg7XHJcbn1cclxuXHJcbi5jYXJke1xyXG4gICAgcGFkZGluZzoxMHB4O1xyXG4gICAgLmJ1dHRvbi1jb250YWluZXIge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIH1cclxufVxyXG5cclxuLmlucHV0c3tcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIC5tYXQtZm9ybS1maWVsZC1hcHBlYXJhbmNlLWZpbGwgLm1hdC1mb3JtLWZpZWxkLWZsZXgge1xyXG4gICAgICAgIHBhZGRpbmc6MCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuI3JlcXVlc3R7XHJcbiAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcclxufVxyXG5cclxuLmFtb3VudHtcclxuICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICBsZWZ0OiAxMjBweDtcclxuICAgYm90dG9tOiA4MHB4O1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMzA7XHJcbiAgIGhlaWdodDogMTBweDtcclxufVxyXG5cclxuLmFsZXJ0e1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LXNpemU6MjBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjpyZWQ7XHJcbiAgICBjb2xvcjp3aGl0ZTtcclxufVxyXG4ubW9kYWwuZmFsc2Uge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICAgIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAvKiBTdGF5IGluIHBsYWNlICovXHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgLyogU2l0IG9uIHRvcCAqL1xyXG4gICAgcGFkZGluZy10b3A6IDEwMHB4O1xyXG4gICAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xyXG4gICAgbGVmdDogMDtcclxuICAgIHRvcDogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLyogRnVsbCB3aWR0aCAqL1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLyogRnVsbCBoZWlnaHQgKi9cclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgLyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigwLCAwLCAwKTtcclxuICAgIC8qIEZhbGxiYWNrIGNvbG9yICovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XHJcbiAgICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXHJcbiAgfVxyXG4gIFxyXG4gIC5tb2RhbC50cnVlIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIC8qIFN0YXkgaW4gcGxhY2UgKi9cclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICAvKiBTaXQgb24gdG9wICovXHJcbiAgICBcclxuICAgIC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cclxuICAgIGxlZnQ6IDBweDtcclxuICAgIHRvcDogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLyogRnVsbCB3aWR0aCAqL1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLyogRnVsbCBoZWlnaHQgKi9cclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgLyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigwLCAwLCAwLCAwLjUpO1xyXG4gICAgb3BhY2l0eTogMTtcclxuICAgIC8qIEZhbGxiYWNrIGNvbG9yICovXHJcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cclxuICB9XHJcbiAgXHJcbiAgLm1vZGFsLWNvbnRlbnQge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDI1NnB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG1hcmdpbi10b3A6MjAwcHg7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgLy8gYm9yZGVyOiAxcHggc29saWQgIzg4ODtcclxuICAgIHdpZHRoOiAzMCU7XHJcbiAgICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDAuNHM7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC8qIFRoZSBDbG9zZSBCdXR0b24gKi9cclxuLmNsb3NlIHtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGZvbnQtc2l6ZTogMjhweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxuICAuZm9yZ290IHtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIC8qIHRleHQtYWxpZ246IHJpZ2h0OyAqL1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAzNXB4O1xyXG4gIH1cclxuICAuY2xvc2U6aG92ZXIsXHJcbiAgLmNsb3NlOmZvY3VzIHtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuICAubW9kYWwtZm9vdGVye1xyXG4gICAgICAuYnRuLXByaW1hcnl7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgfVxyXG4gIH1cclxuICAiLCIuLWNvbnRhaW5lciB7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xufVxuXG4uY2FyZCB7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG4uY2FyZCAuYnV0dG9uLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uaW5wdXRzIHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5pbnB1dHMgLm1hdC1mb3JtLWZpZWxkLWFwcGVhcmFuY2UtZmlsbCAubWF0LWZvcm0tZmllbGQtZmxleCB7XG4gIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbn1cblxuI3JlcXVlc3Qge1xuICBmb250LXNpemU6IDExcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuXG4uYW1vdW50IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAxMjBweDtcbiAgYm90dG9tOiA4MHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMzA7XG4gIGhlaWdodDogMTBweDtcbn1cblxuLmFsZXJ0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5tb2RhbC5mYWxzZSB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xuICB6LWluZGV4OiAxO1xuICAvKiBTaXQgb24gdG9wICovXG4gIHBhZGRpbmctdG9wOiAxMDBweDtcbiAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xuICBsZWZ0OiAwO1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICAvKiBGdWxsIHdpZHRoICovXG4gIGhlaWdodDogMTAwJTtcbiAgLyogRnVsbCBoZWlnaHQgKi9cbiAgb3ZlcmZsb3c6IGF1dG87XG4gIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICAvKiBGYWxsYmFjayBjb2xvciAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cbn1cblxuLm1vZGFsLnRydWUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cbiAgcG9zaXRpb246IGZpeGVkO1xuICAvKiBTdGF5IGluIHBsYWNlICovXG4gIHotaW5kZXg6IDE7XG4gIC8qIFNpdCBvbiB0b3AgKi9cbiAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xuICBsZWZ0OiAwcHg7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIC8qIEZ1bGwgd2lkdGggKi9cbiAgaGVpZ2h0OiAxMDAlO1xuICAvKiBGdWxsIGhlaWdodCAqL1xuICBvdmVyZmxvdzogYXV0bztcbiAgLyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjUpO1xuICBvcGFjaXR5OiAxO1xuICAvKiBGYWxsYmFjayBjb2xvciAqL1xuICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG59XG5cbi5tb2RhbC1jb250ZW50IHtcbiAgbWFyZ2luLWxlZnQ6IDI1NnB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLXRvcDogMjAwcHg7XG4gIHBhZGRpbmc6IDA7XG4gIHdpZHRoOiAzMCU7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG4gIGFuaW1hdGlvbi1uYW1lOiBhbmltYXRldG9wO1xuICBhbmltYXRpb24tZHVyYXRpb246IDAuNHM7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLyogVGhlIENsb3NlIEJ1dHRvbiAqL1xuLmNsb3NlIHtcbiAgY29sb3I6IGJsYWNrO1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5mb3Jnb3Qge1xuICBmbG9hdDogcmlnaHQ7XG4gIC8qIHRleHQtYWxpZ246IHJpZ2h0OyAqL1xuICBtYXJnaW4tcmlnaHQ6IDM1cHg7XG59XG5cbi5jbG9zZTpob3Zlcixcbi5jbG9zZTpmb2N1cyB7XG4gIGNvbG9yOiAjMDAwO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLm1vZGFsLWZvb3RlciAuYnRuLXByaW1hcnkge1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component.ts ***!
  \****************************************************************************************************************/
/*! exports provided: MerchantWithdrawalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantWithdrawalComponent", function() { return MerchantWithdrawalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_escrow_transaction_escrow_transaction_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/escrow-transaction/escrow-transaction.service */ "./src/app/services/escrow-transaction/escrow-transaction.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var MerchantWithdrawalComponent = /** @class */ (function () {
    function MerchantWithdrawalComponent(EscrowTransactionService, router, fb) {
        this.EscrowTransactionService = EscrowTransactionService;
        this.router = router;
        this.hideRequiredControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](false);
        this.floatLabelControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('never');
        this.loading = false;
        this.form = {};
        this.formEditable = {};
        this.othersForm = false;
        this.config = {
            allowNumbersOnly: true,
            length: 6,
            isPasswordInput: true,
            disableAutoFocus: false,
            placeholder: '',
            inputStyles: {
                'width': '50px',
                'height': '50px'
            }
        };
        this.options = fb.group({
            hideRequired: this.hideRequiredControl,
            floatLabel: this.floatLabelControl,
        });
    }
    MerchantWithdrawalComponent.prototype.ngOnInit = function () {
        this.firstload();
    };
    MerchantWithdrawalComponent.prototype.firstload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var resBalance, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.EscrowTransactionService.getEscrowBalance()];
                    case 1:
                        resBalance = _a.sent();
                        this.myBalance = resBalance.result.balance;
                        console.log("this.myBalance", this.myBalance);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log("E report", e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MerchantWithdrawalComponent.prototype.editableForm = function (how_much, how) {
    };
    MerchantWithdrawalComponent.prototype.stringToNumber = function (str) {
        if (typeof str == 'number') {
            str = str + '';
        }
        var s = str.toLowerCase().replace(/[^0-9]/g, '');
        return parseInt(s);
    };
    MerchantWithdrawalComponent.prototype.closeDialog = function () {
        this.othersForm = false;
    };
    MerchantWithdrawalComponent.prototype.forgotPin = function () {
        console.log('forgotpin');
    };
    MerchantWithdrawalComponent.prototype.sendRequest = function () {
        return __awaiter(this, void 0, void 0, function () {
            var balance, curHowMuch;
            return __generator(this, function (_a) {
                this.errorMessage = null;
                balance = this.myBalance;
                if (this.form.how_much) {
                    try {
                        curHowMuch = this.stringToNumber(this.form.how_much);
                        this.form.how_much = curHowMuch;
                        if (curHowMuch > this.myBalance) {
                            this.errorMessage = "Your balance is not enough";
                        }
                        else if (curHowMuch < balance && curHowMuch < 50000) {
                            this.errorMessage = "Your withdrawal amount in less than Rp. 50.000";
                        }
                    }
                    catch (e) {
                        this.errorMessage = "please fill with the correct number";
                    }
                    if (!this.form.description) {
                        this.errorMessage = "Please fill the Description";
                    }
                    if (!this.errorMessage) {
                        this.othersForm = true;
                    }
                }
                else {
                    this.errorMessage = "please fill with the correct number";
                }
                return [2 /*return*/];
            });
        });
    };
    MerchantWithdrawalComponent.prototype.onOtpChange = function (otp) {
        this.form.pin_code = otp;
        console.log(this.form.otp);
    };
    MerchantWithdrawalComponent.prototype.confirm = function () {
        return __awaiter(this, void 0, void 0, function () {
            var r, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.loading = true;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.EscrowTransactionService.escrowWithdrawalRequest(this.form)];
                    case 2:
                        r = _a.sent();
                        if (!r.error) {
                            alert("we will process your request soon");
                            this.othersForm = false;
                            this.router.navigate(['merchant-portal/my-money/']);
                        }
                        this.loading = false;
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorMessage = e_2.message;
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MerchantWithdrawalComponent.ctorParameters = function () { return [
        { type: _services_escrow_transaction_escrow_transaction_service__WEBPACK_IMPORTED_MODULE_1__["EscrowTransactionService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] }
    ]; };
    MerchantWithdrawalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-withdrawal',
            template: __webpack_require__(/*! raw-loader!./merchant-withdrawal.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component.html"),
            styles: [__webpack_require__(/*! ./merchant-withdrawal.component.scss */ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_escrow_transaction_escrow_transaction_service__WEBPACK_IMPORTED_MODULE_1__["EscrowTransactionService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], MerchantWithdrawalComponent);
    return MerchantWithdrawalComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-app-header/merchant-portal-app-header.component.scss":
/*!*************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-app-header/merchant-portal-app-header.component.scss ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host nav.navbar.navbar-expand-lg.fixed-top:after {\n  content: \"\";\n  position: absolute;\n  width: 0;\n  height: 0;\n  border-style: solid;\n  border-width: 0 0 62px 106px;\n  border-color: transparent transparent #2b3240 transparent;\n  left: -106px;\n}\n:host .navbar {\n  background-color: #2b3240;\n  box-shadow: 0 4px 1px 0 rgba(0, 0, 0, 0.01), 0 3px 5px 0 rgba(0, 0, 0, 0.19);\n  left: unset;\n  right: 0px;\n  width: 100%;\n  position: fixed;\n}\n:host .navbar .navbar-brand > img {\n  width: 55px;\n  padding-right: 15px;\n  padding-bottom: 5px;\n}\n:host .navbar .navbar-brand {\n  color: #fff;\n}\n:host .navbar .nav-item > a {\n  color: #999;\n}\n:host .navbar .nav-item > a:hover {\n  color: #56a4ff;\n}\n:host .navbar .nav-link > img {\n  width: auto;\n  height: 30px;\n  padding-right: 5px;\n  padding-bottom: 5px;\n}\n:host .navbar .nav-link > span {\n  color: #56a4ff;\n}\n:host .navbar .bg-circle {\n  background-color: white;\n  border-radius: 50%;\n  width: 40px;\n  height: 40px;\n}\n:host .navbar .bg-circle > img {\n  width: 28px;\n  margin: 6px 0 5px 6px;\n}\n:host .navbar .logout {\n  color: #627190;\n  margin-top: 10px;\n  text-decoration: none;\n}\n:host .navbar .logout:hover {\n  color: #56a4ff;\n}\n:host .navbar .time-custom,\n:host .navbar .date-custom {\n  margin: 10px 10px 0px;\n  text-decoration: none;\n  color: white;\n  border-right: #434c5c solid 1px;\n  padding-right: 20px;\n}\n:host .navbar .time-custom img,\n:host .navbar .date-custom img {\n  margin-right: 10px;\n}\n:host .navbar .admin-portal {\n  text-decoration: none;\n  color: white;\n  float: left;\n}\n:host .navbar .admin-portal img {\n  width: 50px;\n}\n:host .navbar .logout > img {\n  width: 20px;\n}\n:host .navbar #administrator > a {\n  padding-left: 20px;\n  padding-right: 20px;\n}\n:host .navbar #upgrade > a {\n  border-right: #434c5c solid 1px;\n  padding-right: 20px;\n}\n:host .navbar #upgrade > a span {\n  color: #ffb24d;\n}\n:host .navbar .dropdown-item {\n  margin: 8px auto;\n  font-size: 15px;\n}\n:host .messages {\n  width: 300px;\n}\n:host .messages .media {\n  border-bottom: 1px solid #ddd;\n  padding: 5px 10px;\n}\n:host .messages .media:last-child {\n  border-bottom: none;\n}\n:host .messages .media-body h5 {\n  font-size: 13px;\n  font-weight: 600;\n}\n:host .messages .media-body .small {\n  margin: 0;\n}\n:host .messages .media-body .last {\n  font-size: 12px;\n  margin: 0;\n}\n.resp {\n  display: none;\n}\n.switch {\n  position: relative;\n  display: inline-block;\n  width: 60px;\n  height: 34px;\n}\n.switch-capsule {\n  text-align: center;\n}\n/* Hide default HTML checkbox */\n.switch input {\n  opacity: 0;\n  width: 0;\n  height: 0;\n}\n/* The slider */\n.slider {\n  position: absolute;\n  cursor: pointer;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: #ccc;\n  transition: 0.4s;\n}\n.slider:before {\n  position: absolute;\n  content: \"\";\n  height: 26px;\n  width: 26px;\n  left: 4px;\n  bottom: 4px;\n  background-color: white;\n  transition: 0.4s;\n}\ninput:checked + .slider {\n  background-color: #2196f3;\n}\ninput:focus + .slider {\n  box-shadow: 0 0 1px #2196f3;\n}\ninput:checked + .slider:before {\n  -webkit-transform: translateX(26px);\n  transform: translateX(26px);\n}\n/* Rounded sliders */\n.slider.round {\n  border-radius: 34px;\n}\n.slider.round:before {\n  border-radius: 50%;\n}\n@media screen and (max-width: 600px) {\n  :host .navbar .navbar-brand {\n    margin-left: 50%;\n  }\n  :host .navbar #navbarright {\n    justify-content: center;\n    align-items: center;\n  }\n  :host .navbar #navbarright .logout {\n    font-size: 0pt;\n  }\n  :host .navbar #navbarright .logout img {\n    width: 25px;\n  }\n  :host .navbar #navbarright .time-custom,\n:host .navbar #navbarright .date-custom {\n    margin: 10px 10px 0px;\n    text-decoration: none;\n    color: white;\n    border-right: #434c5c solid 1px;\n    padding-right: 20px;\n  }\n  :host .navbar #navbarright .time-custom img,\n:host .navbar #navbarright .date-custom img {\n    display: none;\n    padding: 0;\n  }\n  :host .navbar .nav-link > span {\n    font-size: 15px;\n  }\n  :host .navbar #administrator > a {\n    margin-left: -14px;\n    margin-right: -20px;\n  }\n  :host .navbar .dropdown-menu {\n    top: 10vw !important;\n    left: 15vw !important;\n  }\n\n  .resp {\n    display: inline;\n  }\n\n  .navbar .navbar-expand-lg .fixed-top {\n    left: 0px !important;\n  }\n}\n@media screen and (max-width: 991px) {\n  :host #navbar-menu {\n    left: 0 !important;\n  }\n  :host #navbar-menu .navbar-brand {\n    margin-left: 50%;\n  }\n  :host #navbar-menu #administrator > a {\n    padding-right: 20px;\n    font-size: 1rem;\n  }\n  :host #navbar-menu .logout {\n    margin-top: 0px;\n    display: flex;\n    font-size: 1rem;\n    justify-content: center;\n    align-items: center;\n  }\n  :host #navbar-menu .logout img {\n    width: 25px;\n  }\n  :host #navbar-menu .admin-portal {\n    text-decoration: none;\n    color: white;\n    float: left;\n  }\n  :host #navbar-menu .admin-portal img {\n    width: 50px;\n  }\n  :host #navbar-menu .datetimecontainer {\n    display: flex;\n    flex-direction: column;\n  }\n  :host #navbar-menu .datetimecontainer .time-custom,\n:host #navbar-menu .datetimecontainer .date-custom {\n    margin: 3px 0px 0px;\n    text-decoration: none;\n    font-size: 12px;\n    color: white;\n    border-right: #434c5c solid 1px;\n  }\n  :host #navbar-menu .datetimecontainer .time-custom img,\n:host #navbar-menu .datetimecontainer .date-custom img {\n    margin-right: 10px;\n  }\n\n  .resp {\n    display: inline;\n  }\n\n  .navbar .navbar-expand-lg .fixed-top {\n    left: 0px !important;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9tZXJjaGFudC1wb3J0YWwtYXBwLWhlYWRlci9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1lcmNoYW50LXBvcnRhbFxcbWVyY2hhbnQtcG9ydGFsLWFwcC1oZWFkZXJcXG1lcmNoYW50LXBvcnRhbC1hcHAtaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LXBvcnRhbC1hcHAtaGVhZGVyL21lcmNoYW50LXBvcnRhbC1hcHAtaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdFO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxtQkFBQTtFQUNBLDRCQUFBO0VBQ0EseURBQUE7RUFDQSxZQUFBO0FDRko7QURLRTtFQUNFLHlCQWZzQjtFQWdCdEIsNEVBQUE7RUFFQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FDSko7QURNSTtFQUNFLFdBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDSk47QURPSTtFQUNFLFdBQUE7QUNMTjtBRFFJO0VBQ0UsV0FBQTtBQ05OO0FEUU07RUFDRSxjQUFBO0FDTlI7QURVSTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ1JOO0FEWUk7RUFDRSxjQUFBO0FDVk47QURhSTtFQUNFLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ1hOO0FEY0k7RUFDRSxXQUFBO0VBQ0EscUJBQUE7QUNaTjtBRGVJO0VBQ0UsY0FBQTtFQU1BLGdCQUFBO0VBQ0EscUJBQUE7QUNsQk47QURhTTtFQUNFLGNBQUE7QUNYUjtBRGtCSTs7RUFFRSxxQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0VBQ0EsbUJBQUE7QUNoQk47QURpQk07O0VBQ0Usa0JBQUE7QUNkUjtBRGtCSTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtFQUlBLFdBQUE7QUNuQk47QURnQk07RUFDRSxXQUFBO0FDZFI7QURtQkk7RUFDRSxXQUFBO0FDakJOO0FEb0JJO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtBQ2xCTjtBRHFCSTtFQUNFLCtCQUFBO0VBQ0EsbUJBQUE7QUNuQk47QURxQk07RUFDRSxjQUFBO0FDbkJSO0FEdUJJO0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0FDckJOO0FEeUJFO0VBQ0UsWUFBQTtBQ3ZCSjtBRHlCSTtFQUNFLDZCQUFBO0VBQ0EsaUJBQUE7QUN2Qk47QUR5Qk07RUFDRSxtQkFBQTtBQ3ZCUjtBRDRCTTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtBQzFCUjtBRDZCTTtFQUNFLFNBQUE7QUMzQlI7QUQ4Qk07RUFDRSxlQUFBO0VBQ0EsU0FBQTtBQzVCUjtBRGtDQTtFQUNFLGFBQUE7QUMvQkY7QURtQ0E7RUFDRSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNoQ0Y7QURtQ0E7RUFDRSxrQkFBQTtBQ2hDRjtBRG1DQSwrQkFBQTtBQUNBO0VBQ0UsVUFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FDaENGO0FEbUNBLGVBQUE7QUFDQTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxzQkFBQTtFQUVBLGdCQUFBO0FDaENGO0FEbUNBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBRUEsZ0JBQUE7QUNoQ0Y7QURtQ0E7RUFDRSx5QkFBQTtBQ2hDRjtBRG1DQTtFQUNFLDJCQUFBO0FDaENGO0FEbUNBO0VBQ0UsbUNBQUE7RUFFQSwyQkFBQTtBQ2hDRjtBRG1DQSxvQkFBQTtBQUNBO0VBQ0UsbUJBQUE7QUNoQ0Y7QURtQ0E7RUFDRSxrQkFBQTtBQ2hDRjtBRG1DQTtFQUdNO0lBQ0UsZ0JBQUE7RUNsQ047RURvQ0k7SUFDRSx1QkFBQTtJQUNBLG1CQUFBO0VDbENOO0VEbUNNO0lBQ0UsY0FBQTtFQ2pDUjtFRGtDUTtJQUNFLFdBQUE7RUNoQ1Y7RURtQ007O0lBRUUscUJBQUE7SUFDQSxxQkFBQTtJQUNBLFlBQUE7SUFDQSwrQkFBQTtJQUNBLG1CQUFBO0VDakNSO0VEa0NROztJQUNFLGFBQUE7SUFDQSxVQUFBO0VDL0JWO0VEbUNJO0lBQ0UsZUFBQTtFQ2pDTjtFRG9DSTtJQUNFLGtCQUFBO0lBQ0EsbUJBQUE7RUNsQ047RURxQ0k7SUFDRSxvQkFBQTtJQUNBLHFCQUFBO0VDbkNOOztFRHdDQTtJQUNFLGVBQUE7RUNyQ0Y7O0VEdUNBO0lBQ0Usb0JBQUE7RUNwQ0Y7QUFDRjtBRHVDQTtFQUVJO0lBQ0Usa0JBQUE7RUN0Q0o7RUR3Q0k7SUFDRSxnQkFBQTtFQ3RDTjtFRHlDSTtJQUNFLG1CQUFBO0lBRUEsZUFBQTtFQ3hDTjtFRDBDSTtJQUNFLGVBQUE7SUFDQSxhQUFBO0lBQ0EsZUFBQTtJQUNBLHVCQUFBO0lBQ0EsbUJBQUE7RUN4Q047RUR5Q007SUFDRSxXQUFBO0VDdkNSO0VEMENJO0lBQ0UscUJBQUE7SUFDQSxZQUFBO0lBSUEsV0FBQTtFQzNDTjtFRHdDTTtJQUNFLFdBQUE7RUN0Q1I7RUQwQ0k7SUFDRSxhQUFBO0lBQ0Esc0JBQUE7RUN4Q047RUQwQ007O0lBRUUsbUJBQUE7SUFFQSxxQkFBQTtJQUNBLGVBQUE7SUFDQSxZQUFBO0lBQ0EsK0JBQUE7RUN6Q1I7RUQyQ1E7O0lBQ0Usa0JBQUE7RUN4Q1Y7O0VEOENBO0lBQ0UsZUFBQTtFQzNDRjs7RUQ2Q0E7SUFDRSxvQkFBQTtFQzFDRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9tZXJjaGFudC1wb3J0YWwtYXBwLWhlYWRlci9tZXJjaGFudC1wb3J0YWwtYXBwLWhlYWRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjogIzJiMzI0MDtcclxuXHJcbjpob3N0IHtcclxuICBuYXYubmF2YmFyLm5hdmJhci1leHBhbmQtbGcuZml4ZWQtdG9wOmFmdGVyIHtcclxuICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDogMDtcclxuICAgIGhlaWdodDogMDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IDAgMCA2MnB4IDEwNnB4O1xyXG4gICAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCB0cmFuc3BhcmVudCAjMmIzMjQwIHRyYW5zcGFyZW50O1xyXG4gICAgbGVmdDogLTEwNnB4O1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XHJcbiAgICBib3gtc2hhZG93OiAwIDRweCAxcHggMCByZ2JhKDAsIDAsIDAsIDAuMDEpLCAwIDNweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xyXG4gICAgLy8gbWFyZ2luLXRvcDogMjNweDtcclxuICAgIGxlZnQ6IHVuc2V0O1xyXG4gICAgcmlnaHQ6IDBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG5cclxuICAgIC5uYXZiYXItYnJhbmQgPiBpbWcge1xyXG4gICAgICB3aWR0aDogNTVweDtcclxuICAgICAgcGFkZGluZy1yaWdodDogMTVweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAubmF2YmFyLWJyYW5kIHtcclxuICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICB9XHJcblxyXG4gICAgLm5hdi1pdGVtID4gYSB7XHJcbiAgICAgIGNvbG9yOiAjOTk5O1xyXG5cclxuICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgY29sb3I6ICM1NmE0ZmY7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmF2LWxpbmsgPiBpbWcge1xyXG4gICAgICB3aWR0aDogYXV0bztcclxuICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgXHJcbiAgICB9XHJcblxyXG4gICAgLm5hdi1saW5rID4gc3BhbiB7XHJcbiAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgfVxyXG5cclxuICAgIC5iZy1jaXJjbGUge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICB3aWR0aDogNDBweDtcclxuICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5iZy1jaXJjbGUgPiBpbWcge1xyXG4gICAgICB3aWR0aDogMjhweDtcclxuICAgICAgbWFyZ2luOiA2cHggMCA1cHggNnB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5sb2dvdXQge1xyXG4gICAgICBjb2xvcjogIzYyNzE5MDtcclxuXHJcbiAgICAgICY6aG92ZXIge1xyXG4gICAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICB9XHJcblxyXG4gICAgLnRpbWUtY3VzdG9tLFxyXG4gICAgLmRhdGUtY3VzdG9tIHtcclxuICAgICAgbWFyZ2luOiAxMHB4IDEwcHggMHB4O1xyXG4gICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgYm9yZGVyLXJpZ2h0OiAjNDM0YzVjIHNvbGlkIDFweDtcclxuICAgICAgcGFkZGluZy1yaWdodDogMjBweDtcclxuICAgICAgaW1nIHtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuYWRtaW4tcG9ydGFsIHtcclxuICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDUwcHg7XHJcbiAgICAgIH1cclxuICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLmxvZ291dCA+IGltZyB7XHJcbiAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgICNhZG1pbmlzdHJhdG9yID4gYSB7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMjBweDtcclxuICAgICAgcGFkZGluZy1yaWdodDogMjBweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgI3VwZ3JhZGUgPiBhIHtcclxuICAgICAgYm9yZGVyLXJpZ2h0OiAjNDM0YzVjIHNvbGlkIDFweDtcclxuICAgICAgcGFkZGluZy1yaWdodDogMjBweDtcclxuICAgICAgXHJcbiAgICAgIHNwYW4ge1xyXG4gICAgICAgIGNvbG9yIDogI2ZmYjI0ZDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5kcm9wZG93bi1pdGVtIHtcclxuICAgICAgbWFyZ2luOiA4cHggYXV0bztcclxuICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLm1lc3NhZ2VzIHtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuXHJcbiAgICAubWVkaWEge1xyXG4gICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZDtcclxuICAgICAgcGFkZGluZzogNXB4IDEwcHg7XHJcblxyXG4gICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubWVkaWEtYm9keSB7XHJcbiAgICAgIGg1IHtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLnNtYWxsIHtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5sYXN0IHtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4ucmVzcCB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxuICAvLyBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuXHJcbi5zd2l0Y2gge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgd2lkdGg6IDYwcHg7XHJcbiAgaGVpZ2h0OiAzNHB4O1xyXG59XHJcblxyXG4uc3dpdGNoLWNhcHN1bGUge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLyogSGlkZSBkZWZhdWx0IEhUTUwgY2hlY2tib3ggKi9cclxuLnN3aXRjaCBpbnB1dCB7XHJcbiAgb3BhY2l0eTogMDtcclxuICB3aWR0aDogMDtcclxuICBoZWlnaHQ6IDA7XHJcbn1cclxuXHJcbi8qIFRoZSBzbGlkZXIgKi9cclxuLnNsaWRlciB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB0b3A6IDA7XHJcbiAgbGVmdDogMDtcclxuICByaWdodDogMDtcclxuICBib3R0b206IDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2NjYztcclxuICAtd2Via2l0LXRyYW5zaXRpb246IDAuNHM7XHJcbiAgdHJhbnNpdGlvbjogMC40cztcclxufVxyXG5cclxuLnNsaWRlcjpiZWZvcmUge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBjb250ZW50OiBcIlwiO1xyXG4gIGhlaWdodDogMjZweDtcclxuICB3aWR0aDogMjZweDtcclxuICBsZWZ0OiA0cHg7XHJcbiAgYm90dG9tOiA0cHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjRzO1xyXG4gIHRyYW5zaXRpb246IDAuNHM7XHJcbn1cclxuXHJcbmlucHV0OmNoZWNrZWQgKyAuc2xpZGVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjE5NmYzO1xyXG59XHJcblxyXG5pbnB1dDpmb2N1cyArIC5zbGlkZXIge1xyXG4gIGJveC1zaGFkb3c6IDAgMCAxcHggIzIxOTZmMztcclxufVxyXG5cclxuaW5wdXQ6Y2hlY2tlZCArIC5zbGlkZXI6YmVmb3JlIHtcclxuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgyNnB4KTtcclxuICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDI2cHgpO1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgyNnB4KTtcclxufVxyXG5cclxuLyogUm91bmRlZCBzbGlkZXJzICovXHJcbi5zbGlkZXIucm91bmQge1xyXG4gIGJvcmRlci1yYWRpdXM6IDM0cHg7XHJcbn1cclxuXHJcbi5zbGlkZXIucm91bmQ6YmVmb3JlIHtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XHJcbiAgOmhvc3Qge1xyXG4gICAgLm5hdmJhciB7XHJcbiAgICAgIC5uYXZiYXItYnJhbmQge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1MCU7XHJcbiAgICAgIH1cclxuICAgICAgI25hdmJhcnJpZ2h0IHtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIC5sb2dvdXQge1xyXG4gICAgICAgICAgZm9udC1zaXplOiAwcHQ7XHJcbiAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICB3aWR0aDogMjVweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnRpbWUtY3VzdG9tLFxyXG4gICAgICAgIC5kYXRlLWN1c3RvbSB7XHJcbiAgICAgICAgICBtYXJnaW46IDEwcHggMTBweCAwcHg7XHJcbiAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICBib3JkZXItcmlnaHQ6ICM0MzRjNWMgc29saWQgMXB4O1xyXG4gICAgICAgICAgcGFkZGluZy1yaWdodDogMjBweDtcclxuICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5uYXYtbGluayA+IHNwYW4ge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgI2FkbWluaXN0cmF0b3IgPiBhIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogLTE0cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAtMjBweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmRyb3Bkb3duLW1lbnUge1xyXG4gICAgICAgIHRvcDogMTB2dyAhaW1wb3J0YW50O1xyXG4gICAgICAgIGxlZnQ6IDE1dncgIWltcG9ydGFudDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnJlc3Age1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gIH1cclxuICAubmF2YmFyIC5uYXZiYXItZXhwYW5kLWxnIC5maXhlZC10b3Age1xyXG4gICAgbGVmdDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xyXG4gIDpob3N0IHtcclxuICAgICNuYXZiYXItbWVudSB7XHJcbiAgICAgIGxlZnQ6IDAgIWltcG9ydGFudDtcclxuICAgICAgLy8gcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAubmF2YmFyLWJyYW5kIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNTAlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAjYWRtaW5pc3RyYXRvciA+IGEge1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XHJcbiAgICAgICAgLy8gbWFyZ2luLWxlZnQ6IDV2dztcclxuICAgICAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICAgIH1cclxuICAgICAgLmxvZ291dCB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAuYWRtaW4tcG9ydGFsIHtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogNTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgIH1cclxuICAgICAgLmRhdGV0aW1lY29udGFpbmVyIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblxyXG4gICAgICAgIC50aW1lLWN1c3RvbSxcclxuICAgICAgICAuZGF0ZS1jdXN0b20ge1xyXG4gICAgICAgICAgbWFyZ2luOiAzcHggMHB4IDBweDtcclxuXHJcbiAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICBib3JkZXItcmlnaHQ6ICM0MzRjNWMgc29saWQgMXB4O1xyXG5cclxuICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLnJlc3Age1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gIH1cclxuICAubmF2YmFyIC5uYXZiYXItZXhwYW5kLWxnIC5maXhlZC10b3Age1xyXG4gICAgbGVmdDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcbiIsIjpob3N0IG5hdi5uYXZiYXIubmF2YmFyLWV4cGFuZC1sZy5maXhlZC10b3A6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAwO1xuICBoZWlnaHQ6IDA7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogMCAwIDYycHggMTA2cHg7XG4gIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQgIzJiMzI0MCB0cmFuc3BhcmVudDtcbiAgbGVmdDogLTEwNnB4O1xufVxuOmhvc3QgLm5hdmJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyYjMyNDA7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDFweCAwIHJnYmEoMCwgMCwgMCwgMC4wMSksIDAgM3B4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG4gIGxlZnQ6IHVuc2V0O1xuICByaWdodDogMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IGZpeGVkO1xufVxuOmhvc3QgLm5hdmJhciAubmF2YmFyLWJyYW5kID4gaW1nIHtcbiAgd2lkdGg6IDU1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG46aG9zdCAubmF2YmFyIC5uYXZiYXItYnJhbmQge1xuICBjb2xvcjogI2ZmZjtcbn1cbjpob3N0IC5uYXZiYXIgLm5hdi1pdGVtID4gYSB7XG4gIGNvbG9yOiAjOTk5O1xufVxuOmhvc3QgLm5hdmJhciAubmF2LWl0ZW0gPiBhOmhvdmVyIHtcbiAgY29sb3I6ICM1NmE0ZmY7XG59XG46aG9zdCAubmF2YmFyIC5uYXYtbGluayA+IGltZyB7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cbjpob3N0IC5uYXZiYXIgLm5hdi1saW5rID4gc3BhbiB7XG4gIGNvbG9yOiAjNTZhNGZmO1xufVxuOmhvc3QgLm5hdmJhciAuYmctY2lyY2xlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgd2lkdGg6IDQwcHg7XG4gIGhlaWdodDogNDBweDtcbn1cbjpob3N0IC5uYXZiYXIgLmJnLWNpcmNsZSA+IGltZyB7XG4gIHdpZHRoOiAyOHB4O1xuICBtYXJnaW46IDZweCAwIDVweCA2cHg7XG59XG46aG9zdCAubmF2YmFyIC5sb2dvdXQge1xuICBjb2xvcjogIzYyNzE5MDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuOmhvc3QgLm5hdmJhciAubG9nb3V0OmhvdmVyIHtcbiAgY29sb3I6ICM1NmE0ZmY7XG59XG46aG9zdCAubmF2YmFyIC50aW1lLWN1c3RvbSxcbjpob3N0IC5uYXZiYXIgLmRhdGUtY3VzdG9tIHtcbiAgbWFyZ2luOiAxMHB4IDEwcHggMHB4O1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJpZ2h0OiAjNDM0YzVjIHNvbGlkIDFweDtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cbjpob3N0IC5uYXZiYXIgLnRpbWUtY3VzdG9tIGltZyxcbjpob3N0IC5uYXZiYXIgLmRhdGUtY3VzdG9tIGltZyB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbjpob3N0IC5uYXZiYXIgLmFkbWluLXBvcnRhbCB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY29sb3I6IHdoaXRlO1xuICBmbG9hdDogbGVmdDtcbn1cbjpob3N0IC5uYXZiYXIgLmFkbWluLXBvcnRhbCBpbWcge1xuICB3aWR0aDogNTBweDtcbn1cbjpob3N0IC5uYXZiYXIgLmxvZ291dCA+IGltZyB7XG4gIHdpZHRoOiAyMHB4O1xufVxuOmhvc3QgLm5hdmJhciAjYWRtaW5pc3RyYXRvciA+IGEge1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG46aG9zdCAubmF2YmFyICN1cGdyYWRlID4gYSB7XG4gIGJvcmRlci1yaWdodDogIzQzNGM1YyBzb2xpZCAxcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG46aG9zdCAubmF2YmFyICN1cGdyYWRlID4gYSBzcGFuIHtcbiAgY29sb3I6ICNmZmIyNGQ7XG59XG46aG9zdCAubmF2YmFyIC5kcm9wZG93bi1pdGVtIHtcbiAgbWFyZ2luOiA4cHggYXV0bztcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuOmhvc3QgLm1lc3NhZ2VzIHtcbiAgd2lkdGg6IDMwMHB4O1xufVxuOmhvc3QgLm1lc3NhZ2VzIC5tZWRpYSB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGRkO1xuICBwYWRkaW5nOiA1cHggMTBweDtcbn1cbjpob3N0IC5tZXNzYWdlcyAubWVkaWE6bGFzdC1jaGlsZCB7XG4gIGJvcmRlci1ib3R0b206IG5vbmU7XG59XG46aG9zdCAubWVzc2FnZXMgLm1lZGlhLWJvZHkgaDUge1xuICBmb250LXNpemU6IDEzcHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG46aG9zdCAubWVzc2FnZXMgLm1lZGlhLWJvZHkgLnNtYWxsIHtcbiAgbWFyZ2luOiAwO1xufVxuOmhvc3QgLm1lc3NhZ2VzIC5tZWRpYS1ib2R5IC5sYXN0IHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW46IDA7XG59XG5cbi5yZXNwIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLnN3aXRjaCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogNjBweDtcbiAgaGVpZ2h0OiAzNHB4O1xufVxuXG4uc3dpdGNoLWNhcHN1bGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi8qIEhpZGUgZGVmYXVsdCBIVE1MIGNoZWNrYm94ICovXG4uc3dpdGNoIGlucHV0IHtcbiAgb3BhY2l0eTogMDtcbiAgd2lkdGg6IDA7XG4gIGhlaWdodDogMDtcbn1cblxuLyogVGhlIHNsaWRlciAqL1xuLnNsaWRlciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICBib3R0b206IDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjY2M7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogMC40cztcbiAgdHJhbnNpdGlvbjogMC40cztcbn1cblxuLnNsaWRlcjpiZWZvcmUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGhlaWdodDogMjZweDtcbiAgd2lkdGg6IDI2cHg7XG4gIGxlZnQ6IDRweDtcbiAgYm90dG9tOiA0cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAtd2Via2l0LXRyYW5zaXRpb246IDAuNHM7XG4gIHRyYW5zaXRpb246IDAuNHM7XG59XG5cbmlucHV0OmNoZWNrZWQgKyAuc2xpZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzIxOTZmMztcbn1cblxuaW5wdXQ6Zm9jdXMgKyAuc2xpZGVyIHtcbiAgYm94LXNoYWRvdzogMCAwIDFweCAjMjE5NmYzO1xufVxuXG5pbnB1dDpjaGVja2VkICsgLnNsaWRlcjpiZWZvcmUge1xuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgyNnB4KTtcbiAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWCgyNnB4KTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDI2cHgpO1xufVxuXG4vKiBSb3VuZGVkIHNsaWRlcnMgKi9cbi5zbGlkZXIucm91bmQge1xuICBib3JkZXItcmFkaXVzOiAzNHB4O1xufVxuXG4uc2xpZGVyLnJvdW5kOmJlZm9yZSB7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcbiAgOmhvc3QgLm5hdmJhciAubmF2YmFyLWJyYW5kIHtcbiAgICBtYXJnaW4tbGVmdDogNTAlO1xuICB9XG4gIDpob3N0IC5uYXZiYXIgI25hdmJhcnJpZ2h0IHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIDpob3N0IC5uYXZiYXIgI25hdmJhcnJpZ2h0IC5sb2dvdXQge1xuICAgIGZvbnQtc2l6ZTogMHB0O1xuICB9XG4gIDpob3N0IC5uYXZiYXIgI25hdmJhcnJpZ2h0IC5sb2dvdXQgaW1nIHtcbiAgICB3aWR0aDogMjVweDtcbiAgfVxuICA6aG9zdCAubmF2YmFyICNuYXZiYXJyaWdodCAudGltZS1jdXN0b20sXG46aG9zdCAubmF2YmFyICNuYXZiYXJyaWdodCAuZGF0ZS1jdXN0b20ge1xuICAgIG1hcmdpbjogMTBweCAxMHB4IDBweDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1yaWdodDogIzQzNGM1YyBzb2xpZCAxcHg7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgfVxuICA6aG9zdCAubmF2YmFyICNuYXZiYXJyaWdodCAudGltZS1jdXN0b20gaW1nLFxuOmhvc3QgLm5hdmJhciAjbmF2YmFycmlnaHQgLmRhdGUtY3VzdG9tIGltZyB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG4gIDpob3N0IC5uYXZiYXIgLm5hdi1saW5rID4gc3BhbiB7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICB9XG4gIDpob3N0IC5uYXZiYXIgI2FkbWluaXN0cmF0b3IgPiBhIHtcbiAgICBtYXJnaW4tbGVmdDogLTE0cHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAtMjBweDtcbiAgfVxuICA6aG9zdCAubmF2YmFyIC5kcm9wZG93bi1tZW51IHtcbiAgICB0b3A6IDEwdncgIWltcG9ydGFudDtcbiAgICBsZWZ0OiAxNXZ3ICFpbXBvcnRhbnQ7XG4gIH1cblxuICAucmVzcCB7XG4gICAgZGlzcGxheTogaW5saW5lO1xuICB9XG5cbiAgLm5hdmJhciAubmF2YmFyLWV4cGFuZC1sZyAuZml4ZWQtdG9wIHtcbiAgICBsZWZ0OiAwcHggIWltcG9ydGFudDtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkxcHgpIHtcbiAgOmhvc3QgI25hdmJhci1tZW51IHtcbiAgICBsZWZ0OiAwICFpbXBvcnRhbnQ7XG4gIH1cbiAgOmhvc3QgI25hdmJhci1tZW51IC5uYXZiYXItYnJhbmQge1xuICAgIG1hcmdpbi1sZWZ0OiA1MCU7XG4gIH1cbiAgOmhvc3QgI25hdmJhci1tZW51ICNhZG1pbmlzdHJhdG9yID4gYSB7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgICBmb250LXNpemU6IDFyZW07XG4gIH1cbiAgOmhvc3QgI25hdmJhci1tZW51IC5sb2dvdXQge1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIDpob3N0ICNuYXZiYXItbWVudSAubG9nb3V0IGltZyB7XG4gICAgd2lkdGg6IDI1cHg7XG4gIH1cbiAgOmhvc3QgI25hdmJhci1tZW51IC5hZG1pbi1wb3J0YWwge1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gIH1cbiAgOmhvc3QgI25hdmJhci1tZW51IC5hZG1pbi1wb3J0YWwgaW1nIHtcbiAgICB3aWR0aDogNTBweDtcbiAgfVxuICA6aG9zdCAjbmF2YmFyLW1lbnUgLmRhdGV0aW1lY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIH1cbiAgOmhvc3QgI25hdmJhci1tZW51IC5kYXRldGltZWNvbnRhaW5lciAudGltZS1jdXN0b20sXG46aG9zdCAjbmF2YmFyLW1lbnUgLmRhdGV0aW1lY29udGFpbmVyIC5kYXRlLWN1c3RvbSB7XG4gICAgbWFyZ2luOiAzcHggMHB4IDBweDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItcmlnaHQ6ICM0MzRjNWMgc29saWQgMXB4O1xuICB9XG4gIDpob3N0ICNuYXZiYXItbWVudSAuZGF0ZXRpbWVjb250YWluZXIgLnRpbWUtY3VzdG9tIGltZyxcbjpob3N0ICNuYXZiYXItbWVudSAuZGF0ZXRpbWVjb250YWluZXIgLmRhdGUtY3VzdG9tIGltZyB7XG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICB9XG5cbiAgLnJlc3Age1xuICAgIGRpc3BsYXk6IGlubGluZTtcbiAgfVxuXG4gIC5uYXZiYXIgLm5hdmJhci1leHBhbmQtbGcgLmZpeGVkLXRvcCB7XG4gICAgbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-app-header/merchant-portal-app-header.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-app-header/merchant-portal-app-header.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: MerchantPortalAppHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantPortalAppHeaderComponent", function() { return MerchantPortalAppHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _routing_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../routing.path */ "./src/app/layout/routing.path.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MerchantPortalAppHeaderComponent = /** @class */ (function () {
    function MerchantPortalAppHeaderComponent(translate, router, modalService) {
        var _this = this;
        this.translate = translate;
        this.router = router;
        this.modalService = modalService;
        this.pushRightClass = 'push-right';
        this.dataSearch = '';
        this.developmentMode = 'production';
        // time: Date;
        this.time = new Date();
        this.routePath = _routing_path__WEBPACK_IMPORTED_MODULE_3__["routePath"];
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        var browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
        this.router.events.subscribe(function (val) {
            if (val instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"] &&
                window.innerWidth <= 992 &&
                _this.isToggled()) {
                _this.toggleSidebar();
            }
        });
    }
    MerchantPortalAppHeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dateTimeCurrent = Date();
        var clock = new Date();
        var arrayDate = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        var splittedDate = this.dateTimeCurrent.split(' ');
        // switch (splittedDate[0]) {
        //     case 'Mon':
        //         splittedDate[0] = arrayDate[0];
        //         break;
        //     case 'Tue':
        //         splittedDate[0] = arrayDate[1];
        //         break;
        //     case 'Wed':
        //         splittedDate[0] = arrayDate[2];
        //         break;    
        //     case 'Thu':
        //         splittedDate[0] = arrayDate[3];
        //         break; 
        //     case 'Fri':
        //         splittedDate[0] = arrayDate[4];
        //         break;
        //     case 'Sat':
        //         splittedDate[0] = arrayDate[5];
        //         break;
        //     case 'Sun':
        //         splittedDate[0] = arrayDate[6];
        //         break;      
        //     default:
        //         break;
        // }
        this.dateCurrent = splittedDate[0] + ', ' + splittedDate[2] + ' ' + splittedDate[1] + ' ' + splittedDate[3];
        var res = (splittedDate[4]).split(':');
        this.timeCurrent = setInterval(function () {
            _this.time = new Date();
        }, 1000);
        console.log(this.timeCurrent);
        // this.timeCurrent = setInterval(() => {
        //     clock = new Date(clock.setSeconds(clock.getSeconds()+ 1));
        //     this.time = clock;
        // })
        // console.log(this.timeCurrent)
    };
    // clock(){
    //     var today = new Date();
    //     var hours = today.getHours();
    //     var minutes = today.getMinutes();
    //     var second = today.getSeconds();
    //     console.log("result", today)
    // }
    MerchantPortalAppHeaderComponent.prototype.onSubmit = function () {
        console.log(this.password);
        if (this.password == "devmode1234") {
            localStorage.setItem('devmode', 'active');
            this.devModeLoggedout();
            window.location.href = "/login";
        }
        else {
            alert("wrong password");
        }
    };
    MerchantPortalAppHeaderComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    MerchantPortalAppHeaderComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    MerchantPortalAppHeaderComponent.prototype.rltAndLtr = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    };
    MerchantPortalAppHeaderComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
        localStorage.removeItem('devmode');
    };
    MerchantPortalAppHeaderComponent.prototype.devModeLoggedout = function () {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
    };
    MerchantPortalAppHeaderComponent.prototype.changeLang = function (language) {
        this.translate.use(language);
    };
    MerchantPortalAppHeaderComponent.prototype.devmodeon = function (content) {
        if (localStorage.getItem('devmode') == 'active') {
            localStorage.removeItem('devmode');
            this.onLoggedout();
            window.location.href = "/login";
        }
        else {
            // this.open(content);
            localStorage.setItem('devmode', 'active');
            this.devModeLoggedout();
            window.location.href = "/login";
        }
    };
    // devmodeoff(){
    //     localStorage.removeItem('devmode');
    //     this.onLoggedout();
    //     window.location.href = "/login";
    // }
    MerchantPortalAppHeaderComponent.prototype.onSearch = function () {
        // console.log('routepath', routePath);    
    };
    MerchantPortalAppHeaderComponent.prototype.onDataChanged = function () {
        console.log('changed', this.dataSearch);
        this.router.navigateByUrl(this.dataSearch);
        this.dataSearch = '';
    };
    MerchantPortalAppHeaderComponent.prototype.open = function (content) {
        var _this = this;
        if (localStorage.getItem('devmode') == 'active') {
            localStorage.removeItem('devmode');
            this.onLoggedout();
            window.location.href = "/login";
        }
        else {
            this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(function (result) {
                _this.closeResult = "Closed with: " + result;
            }, function (reason) {
                _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
            });
        }
    };
    MerchantPortalAppHeaderComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].ESC) {
            var element = document.getElementById("checkbox");
            element.checked = false;
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].BACKDROP_CLICK) {
            var element = document.getElementById("checkbox");
            element.checked = false;
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    MerchantPortalAppHeaderComponent.ctorParameters = function () { return [
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] }
    ]; };
    MerchantPortalAppHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'merchant-portal-app-header',
            template: __webpack_require__(/*! raw-loader!./merchant-portal-app-header.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-app-header/merchant-portal-app-header.component.html"),
            styles: [__webpack_require__(/*! ./merchant-portal-app-header.component.scss */ "./src/app/layout/merchant-portal/merchant-portal-app-header/merchant-portal-app-header.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"]])
    ], MerchantPortalAppHeaderComponent);
    return MerchantPortalAppHeaderComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-app-sidebar/merchant-portal-app-sidebar.component.scss":
/*!***************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-app-sidebar/merchant-portal-app-sidebar.component.scss ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-line {\n  display: block;\n  white-space: normal;\n  font-size: 12px;\n  font-family: \"Poppins\";\n  font-weight: bold;\n}\n\n::ng-deep .btn {\n  border-radius: 0px;\n}\n\n::ng-deep a {\n  color: white;\n}\n\n.sidebar {\n  font-size: small;\n  box-shadow: 0 4px 1px 0 rgba(0, 0, 0, 0.01), 0 3px 5px 0 rgba(0, 0, 0, 0.19);\n  border-radius: 0;\n  position: fixed;\n  z-index: 1000;\n  top: 64px;\n  left: 235px;\n  width: 100px;\n  height: 95%;\n  margin-left: -235px;\n  margin-bottom: 48px;\n  border: none;\n  border-radius: 0;\n  overflow-y: auto;\n  background-color: #2b3240;\n  padding-bottom: 40px;\n  white-space: nowrap;\n  transition: all 0.2s ease-in-out;\n  text-align: center;\n}\n\n.sidebar .item a.list-group-item {\n  background: #2b3240;\n  border: 0;\n  border-radius: 0;\n  color: #627190;\n  text-decoration: none;\n  padding-top: 15px;\n  padding-bottom: 15px;\n  padding-right: 5px;\n  padding-left: 5px;\n  text-align: center;\n  vertical-align: 10px;\n}\n\n.sidebar .item a.list-group-item > i {\n  display: block;\n  font-family: \"fontello\";\n  font-style: normal;\n}\n\n.sidebar .item a.list-group-item .fa {\n  margin-right: 10px;\n}\n\n.sidebar .item a.list-group-item > p {\n  margin-top: 5px;\n}\n\n.sidebar .item a.list-group-item > p:active {\n  color: #56a4ff;\n}\n\n.sidebar .item a:hover {\n  background: #bfd9e4;\n  color: #56a4ff;\n  width: 100%;\n  border-left: 2px #56a4ff solid;\n}\n\n.sidebar .item a.router-link-active {\n  background: #bfd9e4;\n  color: #56a4ff;\n  width: 100%;\n  border-left: 4px #56a4ff solid;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n}\n\n.sidebar .item .header-fields {\n  padding-top: 10px;\n}\n\n.sidebar .item .header-fields > .list-group-item:first-child {\n  border-top: 1px solid rgba(255, 255, 255, 0.2);\n}\n\n.sidebar .item img {\n  width: 35px;\n}\n\n.sidebar p {\n  margin-bottom: 0px;\n  margin-top: 0px;\n}\n\n.sidebar .sidebar-dropdown *:focus {\n  border-radius: none;\n  border: none;\n}\n\n.sidebar .sidebar-dropdown .panel-title {\n  font-size: 1rem;\n  height: 50px;\n  margin-bottom: 0;\n}\n\n.sidebar .sidebar-dropdown .panel-title a {\n  color: #999;\n  text-decoration: none;\n  font-weight: 400;\n  background: #2b3240;\n}\n\n.sidebar .sidebar-dropdown .panel-title a span {\n  position: relative;\n  display: block;\n  padding: 0.75rem 1.5rem;\n  padding-top: 1rem;\n}\n\n.sidebar .sidebar-dropdown .panel-title a:hover,\n.sidebar .sidebar-dropdown .panel-title a:focus {\n  color: #fff;\n  outline: none;\n  outline-offset: -2px;\n}\n\n.sidebar .sidebar-dropdown .panel-title:hover {\n  background: white;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse {\n  border-radius: 0;\n  border: none;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item {\n  border-radius: 0;\n  background-color: #2b3240;\n  border: 0 solid transparent;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a {\n  color: #999;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a:hover {\n  color: #fff;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item:hover {\n  background: white;\n}\n\n.sidebar .list-group > a.list-group-item .fa {\n  display: block;\n  clear: both;\n  font-size: 36px;\n  margin-right: 0px;\n}\n\n.sidebar .nested-menu .list-group-item {\n  cursor: pointer;\n}\n\n.sidebar .nested-menu i img {\n  width: 45px;\n  margin-bottom: 5px;\n}\n\n.sidebar .nested-menu > a.list-group-item .fa {\n  display: block;\n  clear: both;\n  font-size: 36px;\n  margin-right: 0px;\n}\n\n.sidebar .nested-menu .nested {\n  list-style-type: none;\n}\n\n.sidebar .nested-menu ul.submenu {\n  display: none;\n  height: 0;\n  margin: 0;\n  padding: 0;\n  background: #212631;\n}\n\n.sidebar .nested-menu .expand ul.submenu {\n  display: block;\n  height: auto;\n  background: #212631;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item {\n  background: #394458;\n  border-bottom: 1px solid #2b3240;\n  border-top: 1px solid rgba(255, 255, 255, 0.1);\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item .circle-custom {\n  width: 1px;\n  border-radius: 50%;\n  background-color: #247de5;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item span {\n  margin-bottom: 5px;\n  color: #9ea6b7;\n  padding: 0px;\n  list-style: circle;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item.router-link-active {\n  background: #247de5;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item.router-link-active span {\n  color: white;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item:hover p {\n  color: white;\n}\n\n.sidebar .nested-menu .expand ul.submenu li p {\n  color: #627190;\n  padding: 10px;\n  display: block;\n}\n\n@media (max-width: 992px) {\n  /* smartphones, iPhone, portrait 480x320 phones */\n  .sidebar {\n    top: 60px;\n    left: 0px;\n    padding-bottom: 3em;\n  }\n}\n\n@media (max-width: 600px) {\n  /* smartphones, iPhone, portrait 480x320 phones */\n  .sidebar {\n    margin-top: -3%;\n    left: 0px;\n    height: 97%;\n    width: 20%;\n    min-width: 100px;\n    padding-bottom: 3em;\n    position: fixed;\n  }\n  .sidebar .list-group > a.list-group-item .fa {\n    font-size: 40px;\n  }\n  .sidebar .list-group img {\n    width: 40px;\n  }\n  .sidebar .nested-menu > a.list-group-item .fa {\n    font-size: 40px;\n  }\n}\n\n@media print {\n  .sidebar {\n    display: none !important;\n  }\n}\n\n::-webkit-scrollbar {\n  width: 0px;\n}\n\n::-webkit-scrollbar-track {\n  -webkit-box-shadow: inset 0 0 0px white;\n  border-radius: 3px;\n}\n\n::-webkit-scrollbar-thumb {\n  border-radius: 3px;\n  -webkit-box-shadow: inset 0 0 3px white;\n}\n\n.toggle-button {\n  position: fixed;\n  width: 236px;\n  cursor: pointer;\n  padding: 12px;\n  bottom: 0;\n  color: #999;\n  background: #212529;\n  border-top: 1px solid #999;\n}\n\n.toggle-button i {\n  font-size: 23px;\n}\n\n.toggle-button:hover {\n  background: white;\n  color: #56a4ff;\n}\n\n.collapsed {\n  width: 50px;\n}\n\n.collapsed span {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9tZXJjaGFudC1wb3J0YWwtYXBwLXNpZGViYXIvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtZXJjaGFudC1wb3J0YWxcXG1lcmNoYW50LXBvcnRhbC1hcHAtc2lkZWJhclxcbWVyY2hhbnQtcG9ydGFsLWFwcC1zaWRlYmFyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LXBvcnRhbC1hcHAtc2lkZWJhci9tZXJjaGFudC1wb3J0YWwtYXBwLXNpZGViYXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR0U7RUFDRSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtBQ0ZKOztBREtFO0VBQ0Usa0JBQUE7QUNGSjs7QURNRTtFQUNFLFlBQUE7QUNISjs7QURNRTtFQUNFLGdCQUFBO0VBQ0EsNEVBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQWxDd0I7RUFxQ3hCLG9CQUFBO0VBRUEsbUJBQUE7RUFLQSxnQ0FBQTtFQUNBLGtCQUFBO0FDTko7O0FEZU07RUFDRSxtQkF2RG9CO0VBd0RwQixTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQWNBLGtCQUFBO0VBQ0Esb0JBQUE7QUMxQlI7O0FEYVE7RUFDRSxjQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQ1hWOztBRGVRO0VBQ0Usa0JBQUE7QUNiVjs7QURxQk07RUFDRSxlQUFBO0FDbkJSOztBRHNCTTtFQUNFLGNBQUE7QUNwQlI7O0FEdUJNO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0FDckJSOztBRHdCTTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtFQUNBLDRFQUFBO0FDdEJSOztBRHlCTTtFQUNFLGlCQUFBO0FDdkJSOztBRHlCUTtFQUNFLDhDQUFBO0FDdkJWOztBRDJCTTtFQUNFLFdBQUE7QUN6QlI7O0FENkJJO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0FDM0JOOztBRCtCTTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtBQzdCUjs7QURnQ007RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FDOUJSOztBRGdDUTtFQUNFLFdBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBeklrQjtBQzJHNUI7O0FEZ0NVO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtBQzlCWjs7QURrQ1E7O0VBRUUsV0FBQTtFQUNBLGFBQUE7RUFDQSxvQkFBQTtBQ2hDVjs7QURvQ007RUFDRSxpQkFBQTtBQ2xDUjs7QURxQ007RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUNuQ1I7O0FEc0NVO0VBQ0UsZ0JBQUE7RUFDQSx5QkF0S2dCO0VBdUtoQiwyQkFBQTtBQ3BDWjs7QURzQ1k7RUFDRSxXQUFBO0FDcENkOztBRHVDWTtFQUNFLFdBQUE7QUNyQ2Q7O0FEeUNVO0VBQ0UsaUJBQUE7QUN2Q1o7O0FEK0NRO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUM3Q1Y7O0FEbURNO0VBQ0UsZUFBQTtBQ2pEUjs7QURxRFE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7QUNuRFY7O0FEd0RRO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUN0RFY7O0FEMERNO0VBQ0UscUJBQUE7QUN4RFI7O0FEMkRNO0VBQ0UsYUFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0FDekRSOztBRDZEUTtFQUNFLGNBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUMzRFY7O0FENkRVO0VBQ0UsbUJBQUE7RUFDQSxnQ0FBQTtFQUNBLDhDQUFBO0FDM0RaOztBRDREWTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FDMURkOztBRDREWTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQzFEZDs7QUQ4RFU7RUFDRSxtQkFBQTtBQzVEWjs7QUQ2RFk7RUFDRSxZQUFBO0FDM0RkOztBRGdFWTtFQUNFLFlBQUE7QUM5RGQ7O0FEbUVZO0VBQ0UsY0FBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDakVkOztBRHdFRTtFQUNFLGlEQUFBO0VBQ0E7SUFDRSxTQUFBO0lBQ0EsU0FBQTtJQUNBLG1CQUFBO0VDckVKO0FBQ0Y7O0FEdUVFO0VBQ0UsaURBQUE7RUFDQTtJQUNFLGVBQUE7SUFFQSxTQUFBO0lBQ0EsV0FBQTtJQUNBLFVBQUE7SUFDQSxnQkFBQTtJQUNBLG1CQUFBO0lBQ0EsZUFBQTtFQ3RFSjtFRDBFUTtJQUNFLGVBQUE7RUN4RVY7RUQ0RU07SUFDRSxXQUFBO0VDMUVSO0VEZ0ZRO0lBQ0UsZUFBQTtFQzlFVjtBQUNGOztBRGdHRTtFQUNFO0lBQ0Usd0JBQUE7RUM5Rko7QUFDRjs7QURpR0U7RUFDRSxVQUFBO0FDL0ZKOztBRGtHRTtFQUNFLHVDQUFBO0VBQ0Esa0JBQUE7QUMvRko7O0FEa0dFO0VBQ0Usa0JBQUE7RUFDQSx1Q0FBQTtBQy9GSjs7QURrR0U7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQVdBLDBCQUFBO0FDekdKOztBRGdHSTtFQUNFLGVBQUE7QUM5Rk47O0FEaUdJO0VBQ0UsaUJBQUE7RUFDQSxjQUFBO0FDL0ZOOztBRDBHRTtFQUNFLFdBQUE7QUN2R0o7O0FEeUdJO0VBQ0UsYUFBQTtBQ3ZHTiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvbWVyY2hhbnQtcG9ydGFsLWFwcC1zaWRlYmFyL21lcmNoYW50LXBvcnRhbC1hcHAtc2lkZWJhci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4gICR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjogIzJiMzI0MDtcclxuICAvLyR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjogIzI1MkMzOTtcclxuICAubmV3LWxpbmUge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBcclxuICB9XHJcbiAgOjpuZy1kZWVwIC5idG4ge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gICBcclxuICAgIFxyXG59XHJcbiAgOjpuZy1kZWVwIGEge1xyXG4gICAgY29sb3I6d2hpdGU7XHJcbn1cclxuXHJcbiAgLnNpZGViYXIge1xyXG4gICAgZm9udC1zaXplOiBzbWFsbDtcclxuICAgIGJveC1zaGFkb3c6IDAgNHB4IDFweCAwIHJnYmEoMCwgMCwgMCwgMC4wMSksIDAgM3B4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgei1pbmRleDogMTAwMDtcclxuICAgIHRvcDogNjRweDtcclxuICAgIGxlZnQ6IDIzNXB4O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgaGVpZ2h0OiA5NSU7XHJcbiAgICBtYXJnaW4tbGVmdDogLTIzNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNDhweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xyXG4gICAgLy9ib3R0b206IDA7XHJcbiAgICAvLyBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogNDBweDtcclxuICAgIC8vIHBhZGRpbmctdG9wOiA3MHB4O1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gICAgLW1zLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gICAgLW8tdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIC8vYnVhdCBkZXZtb2RlXHJcbiAgICAvLyBtYXJnaW4tdG9wOiAyM3B4O1xyXG4gIFxyXG4gICAgLy8gYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjU1LDI1NSwyNTUsMC4zKTtcclxuICAgIC5pdGVtIHtcclxuICAgICAgXHJcbiAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgICBhLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgIGNvbG9yOiAjNjI3MTkwO1xyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgXHJcbiAgICAgICAgPiBpIHtcclxuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgZm9udC1mYW1pbHk6IFwiZm9udGVsbG9cIjtcclxuICAgICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgICB9XHJcbiAgXHJcbiAgICAgICAgLy9wYWRkaW5nLWxlZnQ6IDRweDtcclxuICAgICAgICAuZmEge1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogMTBweDtcclxuICAgICAgfVxyXG4gICAgXHJcbiAgICAgIGEubGlzdC1ncm91cC1pdGVtID4gcCB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIGEubGlzdC1ncm91cC1pdGVtID4gcDphY3RpdmUge1xyXG4gICAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIGE6aG92ZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNiZmQ5ZTQ7XHJcbiAgICAgICAgY29sb3I6ICM1NmE0ZmY7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYm9yZGVyLWxlZnQ6IDJweCAjNTZhNGZmIHNvbGlkO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIGEucm91dGVyLWxpbmstYWN0aXZlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjYmZkOWU0O1xyXG4gICAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGJvcmRlci1sZWZ0OiA0cHggIzU2YTRmZiBzb2xpZDtcclxuICAgICAgICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIC5oZWFkZXItZmllbGRzIHtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICBcclxuICAgICAgICA+IC5saXN0LWdyb3VwLWl0ZW06Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogMzVweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIFxyXG4gICAgcCB7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLnNpZGViYXItZHJvcGRvd24ge1xyXG4gICAgICAqOmZvY3VzIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiBub25lO1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgfVxyXG4gIFxyXG4gICAgICAucGFuZWwtdGl0bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICBcclxuICAgICAgICBhIHtcclxuICAgICAgICAgIGNvbG9yOiAjOTk5O1xyXG4gICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjtcclxuICBcclxuICAgICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwLjc1cmVtIDEuNXJlbTtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDFyZW07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIGE6aG92ZXIsXHJcbiAgICAgICAgYTpmb2N1cyB7XHJcbiAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgICBvdXRsaW5lLW9mZnNldDogLTJweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgLnBhbmVsLXRpdGxlOmhvdmVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgfVxyXG4gIFxyXG4gICAgICAucGFuZWwtY29sbGFwc2Uge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gIFxyXG4gICAgICAgIC5wYW5lbC1ib2R5IHtcclxuICAgICAgICAgIC5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICBcclxuICAgICAgICAgICAgYSB7XHJcbiAgICAgICAgICAgICAgY29sb3I6ICM5OTk7XHJcbiAgICAgICAgICAgIH1cclxuICBcclxuICAgICAgICAgICAgYTpob3ZlciB7XHJcbiAgICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICBcclxuICAgICAgICAgIC5saXN0LWdyb3VwLWl0ZW06aG92ZXIge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICBcclxuICAgIC5saXN0LWdyb3VwIHtcclxuICAgICAgPiBhLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgICAgLmZhIHtcclxuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgY2xlYXI6IGJvdGg7XHJcbiAgICAgICAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICBcclxuICAgIC5uZXN0ZWQtbWVudSB7XHJcbiAgICAgIC5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgfVxyXG4gIFxyXG4gICAgICBpIHtcclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgd2lkdGg6IDQ1cHg7XHJcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgID4gYS5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICAgIC5mYSB7XHJcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgIGNsZWFyOiBib3RoO1xyXG4gICAgICAgICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIC5uZXN0ZWQge1xyXG4gICAgICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcclxuICAgICAgfVxyXG4gIFxyXG4gICAgICB1bC5zdWJtZW51IHtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgIGhlaWdodDogMDtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yLCA1JSk7XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgJiAuZXhwYW5kIHtcclxuICAgICAgICB1bC5zdWJtZW51IHtcclxuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogZGFya2VuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgNSUpO1xyXG4gIFxyXG4gICAgICAgICAgYS5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjMzk0NDU4O1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xyXG4gICAgICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjEpO1xyXG4gICAgICAgICAgICAuY2lyY2xlLWN1c3RvbSB7XHJcbiAgICAgICAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzI0N2RlNTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBzcGFuIHtcclxuICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgICAgICAgICAgY29sb3I6ICM5ZWE2Yjc7XHJcbiAgICAgICAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgICAgICAgIGxpc3Qtc3R5bGU6IGNpcmNsZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgICAgYS5saXN0LWdyb3VwLWl0ZW0ucm91dGVyLWxpbmstYWN0aXZlIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogIzI0N2RlNTtcclxuICAgICAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgXHJcbiAgICAgICAgICBhLmxpc3QtZ3JvdXAtaXRlbTpob3ZlciB7XHJcbiAgICAgICAgICAgIHAge1xyXG4gICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgICAgbGkge1xyXG4gICAgICAgICAgICBwIHtcclxuICAgICAgICAgICAgICBjb2xvcjogIzYyNzE5MDtcclxuICAgICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA5OTJweCkge1xyXG4gICAgLyogc21hcnRwaG9uZXMsIGlQaG9uZSwgcG9ydHJhaXQgNDgweDMyMCBwaG9uZXMgKi9cclxuICAgIC5zaWRlYmFyIHtcclxuICAgICAgdG9wOiA2MHB4O1xyXG4gICAgICBsZWZ0OiAwcHg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAzZW07XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gICAgLyogc21hcnRwaG9uZXMsIGlQaG9uZSwgcG9ydHJhaXQgNDgweDMyMCBwaG9uZXMgKi9cclxuICAgIC5zaWRlYmFyIHtcclxuICAgICAgbWFyZ2luLXRvcDogLTMlO1xyXG4gICAgLy8gICB0b3A6IDhlbTtcclxuICAgICAgbGVmdDogMHB4O1xyXG4gICAgICBoZWlnaHQ6IDk3JTtcclxuICAgICAgd2lkdGg6IDIwJTtcclxuICAgICAgbWluLXdpZHRoOiAxMDBweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDNlbTtcclxuICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gIFxyXG4gICAgICAubGlzdC1ncm91cCB7XHJcbiAgICAgICAgPiBhLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgICAgICAuZmEge1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDQwcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogNDBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgLm5lc3RlZC1tZW51IHtcclxuICAgICAgICA+IGEubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgICAgIC5mYSB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgLy9AbWVkaWEgKG1pbi13aWR0aDogOTYxcHgpIHtcclxuICAvLyAgICAvKiB0YWJsZXQsIGxhbmRzY2FwZSBpUGFkLCBsby1yZXMgbGFwdG9wcyBhbmRzIGRlc2t0b3BzICovXHJcbiAgLy99XHJcbiAgLy9cclxuICAvL0BtZWRpYSAobWluLXdpZHRoOiAxMDI1cHgpIHtcclxuICAvLyAgICAvKiBiaWcgbGFuZHNjYXBlIHRhYmxldHMsIGxhcHRvcHMsIGFuZCBkZXNrdG9wcyAqL1xyXG4gIC8vfVxyXG4gIC8vXHJcbiAgLy9AbWVkaWEgKG1pbi13aWR0aDogMTI4MXB4KSB7XHJcbiAgLy8gICAgLyogaGktcmVzIGxhcHRvcHMgYW5kIGRlc2t0b3BzICovXHJcbiAgLy99XHJcbiAgXHJcbiAgQG1lZGlhIHByaW50IHtcclxuICAgIC5zaWRlYmFyIHtcclxuICAgICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICA6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICAgIHdpZHRoOiAwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIDo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgMHB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgfVxyXG4gIFxyXG4gIDo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgM3B4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMSk7XHJcbiAgfVxyXG4gIFxyXG4gIC50b2dnbGUtYnV0dG9uIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHdpZHRoOiAyMzZweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHBhZGRpbmc6IDEycHg7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBjb2xvcjogIzk5OTtcclxuICAgIGJhY2tncm91bmQ6ICMyMTI1Mjk7XHJcbiAgXHJcbiAgICBpIHtcclxuICAgICAgZm9udC1zaXplOiAyM3B4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgJjpob3ZlciB7XHJcbiAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICBjb2xvcjogIzU2YTRmZjtcclxuICAgIH1cclxuICBcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjOTk5O1xyXG4gICAgLy8gLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICAgIC8vIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgICAvLyAtbXMtdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgICAvLyAtby10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICAgIC8vIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIH1cclxuICBcclxuICAuY29sbGFwc2VkIHtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gIFxyXG4gICAgc3BhbiB7XHJcbiAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbiAgfVxyXG4gICIsIi5uZXctbGluZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbjo6bmctZGVlcCAuYnRuIHtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xufVxuXG46Om5nLWRlZXAgYSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnNpZGViYXIge1xuICBmb250LXNpemU6IHNtYWxsO1xuICBib3gtc2hhZG93OiAwIDRweCAxcHggMCByZ2JhKDAsIDAsIDAsIDAuMDEpLCAwIDNweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDEwMDA7XG4gIHRvcDogNjRweDtcbiAgbGVmdDogMjM1cHg7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiA5NSU7XG4gIG1hcmdpbi1sZWZ0OiAtMjM1cHg7XG4gIG1hcmdpbi1ib3R0b206IDQ4cHg7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJiMzI0MDtcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gIC1tcy10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uc2lkZWJhciAuaXRlbSBhLmxpc3QtZ3JvdXAtaXRlbSB7XG4gIGJhY2tncm91bmQ6ICMyYjMyNDA7XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgY29sb3I6ICM2MjcxOTA7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHZlcnRpY2FsLWFsaWduOiAxMHB4O1xufVxuLnNpZGViYXIgLml0ZW0gYS5saXN0LWdyb3VwLWl0ZW0gPiBpIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtZmFtaWx5OiBcImZvbnRlbGxvXCI7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbn1cbi5zaWRlYmFyIC5pdGVtIGEubGlzdC1ncm91cC1pdGVtIC5mYSB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbi5zaWRlYmFyIC5pdGVtIGEubGlzdC1ncm91cC1pdGVtID4gcCB7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5zaWRlYmFyIC5pdGVtIGEubGlzdC1ncm91cC1pdGVtID4gcDphY3RpdmUge1xuICBjb2xvcjogIzU2YTRmZjtcbn1cbi5zaWRlYmFyIC5pdGVtIGE6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjYmZkOWU0O1xuICBjb2xvcjogIzU2YTRmZjtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1sZWZ0OiAycHggIzU2YTRmZiBzb2xpZDtcbn1cbi5zaWRlYmFyIC5pdGVtIGEucm91dGVyLWxpbmstYWN0aXZlIHtcbiAgYmFja2dyb3VuZDogI2JmZDllNDtcbiAgY29sb3I6ICM1NmE0ZmY7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItbGVmdDogNHB4ICM1NmE0ZmYgc29saWQ7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG59XG4uc2lkZWJhciAuaXRlbSAuaGVhZGVyLWZpZWxkcyB7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLnNpZGViYXIgLml0ZW0gLmhlYWRlci1maWVsZHMgPiAubGlzdC1ncm91cC1pdGVtOmZpcnN0LWNoaWxkIHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcbn1cbi5zaWRlYmFyIC5pdGVtIGltZyB7XG4gIHdpZHRoOiAzNXB4O1xufVxuLnNpZGViYXIgcCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgbWFyZ2luLXRvcDogMHB4O1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gKjpmb2N1cyB7XG4gIGJvcmRlci1yYWRpdXM6IG5vbmU7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLXRpdGxlIGEge1xuICBjb2xvcjogIzk5OTtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBmb250LXdlaWdodDogNDAwO1xuICBiYWNrZ3JvdW5kOiAjMmIzMjQwO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLXRpdGxlIGEgc3BhbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmc6IDAuNzVyZW0gMS41cmVtO1xuICBwYWRkaW5nLXRvcDogMXJlbTtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC10aXRsZSBhOmhvdmVyLFxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLXRpdGxlIGE6Zm9jdXMge1xuICBjb2xvcjogI2ZmZjtcbiAgb3V0bGluZTogbm9uZTtcbiAgb3V0bGluZS1vZmZzZXQ6IC0ycHg7XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtdGl0bGU6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC1jb2xsYXBzZSB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC1jb2xsYXBzZSAucGFuZWwtYm9keSAubGlzdC1ncm91cC1pdGVtIHtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJiMzI0MDtcbiAgYm9yZGVyOiAwIHNvbGlkIHRyYW5zcGFyZW50O1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLWNvbGxhcHNlIC5wYW5lbC1ib2R5IC5saXN0LWdyb3VwLWl0ZW0gYSB7XG4gIGNvbG9yOiAjOTk5O1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLWNvbGxhcHNlIC5wYW5lbC1ib2R5IC5saXN0LWdyb3VwLWl0ZW0gYTpob3ZlciB7XG4gIGNvbG9yOiAjZmZmO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLWNvbGxhcHNlIC5wYW5lbC1ib2R5IC5saXN0LWdyb3VwLWl0ZW06aG92ZXIge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwID4gYS5saXN0LWdyb3VwLWl0ZW0gLmZhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNsZWFyOiBib3RoO1xuICBmb250LXNpemU6IDM2cHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5saXN0LWdyb3VwLWl0ZW0ge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgaSBpbWcge1xuICB3aWR0aDogNDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51ID4gYS5saXN0LWdyb3VwLWl0ZW0gLmZhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNsZWFyOiBib3RoO1xuICBmb250LXNpemU6IDM2cHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5uZXN0ZWQge1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgdWwuc3VibWVudSB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIGhlaWdodDogMDtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBiYWNrZ3JvdW5kOiAjMjEyNjMxO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBoZWlnaHQ6IGF1dG87XG4gIGJhY2tncm91bmQ6ICMyMTI2MzE7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmV4cGFuZCB1bC5zdWJtZW51IGEubGlzdC1ncm91cC1pdGVtIHtcbiAgYmFja2dyb3VuZDogIzM5NDQ1ODtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMyYjMyNDA7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMSk7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmV4cGFuZCB1bC5zdWJtZW51IGEubGlzdC1ncm91cC1pdGVtIC5jaXJjbGUtY3VzdG9tIHtcbiAgd2lkdGg6IDFweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjQ3ZGU1O1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBhLmxpc3QtZ3JvdXAtaXRlbSBzcGFuIHtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBjb2xvcjogIzllYTZiNztcbiAgcGFkZGluZzogMHB4O1xuICBsaXN0LXN0eWxlOiBjaXJjbGU7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmV4cGFuZCB1bC5zdWJtZW51IGEubGlzdC1ncm91cC1pdGVtLnJvdXRlci1saW5rLWFjdGl2ZSB7XG4gIGJhY2tncm91bmQ6ICMyNDdkZTU7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmV4cGFuZCB1bC5zdWJtZW51IGEubGlzdC1ncm91cC1pdGVtLnJvdXRlci1saW5rLWFjdGl2ZSBzcGFuIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBhLmxpc3QtZ3JvdXAtaXRlbTpob3ZlciBwIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBsaSBwIHtcbiAgY29sb3I6ICM2MjcxOTA7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLyogc21hcnRwaG9uZXMsIGlQaG9uZSwgcG9ydHJhaXQgNDgweDMyMCBwaG9uZXMgKi9cbiAgLnNpZGViYXIge1xuICAgIHRvcDogNjBweDtcbiAgICBsZWZ0OiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDNlbTtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC8qIHNtYXJ0cGhvbmVzLCBpUGhvbmUsIHBvcnRyYWl0IDQ4MHgzMjAgcGhvbmVzICovXG4gIC5zaWRlYmFyIHtcbiAgICBtYXJnaW4tdG9wOiAtMyU7XG4gICAgbGVmdDogMHB4O1xuICAgIGhlaWdodDogOTclO1xuICAgIHdpZHRoOiAyMCU7XG4gICAgbWluLXdpZHRoOiAxMDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogM2VtO1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgfVxuICAuc2lkZWJhciAubGlzdC1ncm91cCA+IGEubGlzdC1ncm91cC1pdGVtIC5mYSB7XG4gICAgZm9udC1zaXplOiA0MHB4O1xuICB9XG4gIC5zaWRlYmFyIC5saXN0LWdyb3VwIGltZyB7XG4gICAgd2lkdGg6IDQwcHg7XG4gIH1cbiAgLnNpZGViYXIgLm5lc3RlZC1tZW51ID4gYS5saXN0LWdyb3VwLWl0ZW0gLmZhIHtcbiAgICBmb250LXNpemU6IDQwcHg7XG4gIH1cbn1cbkBtZWRpYSBwcmludCB7XG4gIC5zaWRlYmFyIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG4gIH1cbn1cbjo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMHB4O1xufVxuXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRyYWNrIHtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgMHB4IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59XG5cbjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogaW5zZXQgMCAwIDNweCB3aGl0ZTtcbn1cblxuLnRvZ2dsZS1idXR0b24ge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAyMzZweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBwYWRkaW5nOiAxMnB4O1xuICBib3R0b206IDA7XG4gIGNvbG9yOiAjOTk5O1xuICBiYWNrZ3JvdW5kOiAjMjEyNTI5O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgIzk5OTtcbn1cbi50b2dnbGUtYnV0dG9uIGkge1xuICBmb250LXNpemU6IDIzcHg7XG59XG4udG9nZ2xlLWJ1dHRvbjpob3ZlciB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBjb2xvcjogIzU2YTRmZjtcbn1cblxuLmNvbGxhcHNlZCB7XG4gIHdpZHRoOiA1MHB4O1xufVxuLmNvbGxhcHNlZCBzcGFuIHtcbiAgZGlzcGxheTogbm9uZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-app-sidebar/merchant-portal-app-sidebar.component.ts":
/*!*************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-app-sidebar/merchant-portal-app-sidebar.component.ts ***!
  \*************************************************************************************************************/
/*! exports provided: MerchantPortalAppSidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantPortalAppSidebarComponent", function() { return MerchantPortalAppSidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _sidebarmenu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sidebarmenu */ "./src/app/layout/merchant-portal/merchant-portal-app-sidebar/sidebarmenu.ts");
/* harmony import */ var _object_interface_common_function__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../object-interface/common.function */ "./src/app/object-interface/common.function.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MerchantPortalAppSidebarComponent = /** @class */ (function () {
    function MerchantPortalAppSidebarComponent(router, matIconRegistry, domSanitizer) {
        this.router = router;
        this.matIconRegistry = matIconRegistry;
        this.domSanitizer = domSanitizer;
        this.sidebarMenu = _sidebarmenu__WEBPACK_IMPORTED_MODULE_1__["sidebarmenu"];
        this.matIconRegistry.addSvgIcon("icon-1", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-dashboard.svg"));
        this.matIconRegistry.addSvgIcon("icon-2", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-account-balance-wallet.svg"));
        this.matIconRegistry.addSvgIcon("icon-3", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-filter-list.svg"));
        this.matIconRegistry.addSvgIcon("icon-4", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-headset.svg"));
        this.matIconRegistry.addSvgIcon("icon-5", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-insert-photo.svg"));
        this.matIconRegistry.addSvgIcon("icon-6", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-local-offer.svg"));
        this.matIconRegistry.addSvgIcon("icon-7", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-local-shipping.svg"));
        this.matIconRegistry.addSvgIcon("icon-8", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-payment.svg"));
        this.matIconRegistry.addSvgIcon("icon-9", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-receipt.svg"));
        this.matIconRegistry.addSvgIcon("icon-10", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-shopping-cart.svg"));
        this.matIconRegistry.addSvgIcon("icon-11", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-view-carousel.svg"));
        this.matIconRegistry.addSvgIcon("icon-12", this.domSanitizer.bypassSecurityTrustResourceUrl("/assets/menuicons/Icon-material-email.svg"));
    }
    MerchantPortalAppSidebarComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MerchantPortalAppSidebarComponent.prototype.firstLoad = function () {
        // if (this.sideMenu.length <= 7) {
        //     let nextMenu = {
        //         icon: '<i class="fa fa-sign-out-alt"></i>',
        //         label: 'Logout',
        //         router_link: '/merchant-portal',
        //         func: this.logout
        //     };
        //     this.sideMenu.push(nextMenu);
        // }
    };
    MerchantPortalAppSidebarComponent.prototype.logout = function () {
        Object(_object_interface_common_function__WEBPACK_IMPORTED_MODULE_2__["logout"])();
        this.router.navigate(['/']);
    };
    MerchantPortalAppSidebarComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconRegistry"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] }
    ]; };
    MerchantPortalAppSidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'merchant-portal-app-sidebar',
            template: __webpack_require__(/*! raw-loader!./merchant-portal-app-sidebar.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-app-sidebar/merchant-portal-app-sidebar.component.html"),
            styles: [__webpack_require__(/*! ./merchant-portal-app-sidebar.component.scss */ "./src/app/layout/merchant-portal/merchant-portal-app-sidebar/merchant-portal-app-sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconRegistry"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"]])
    ], MerchantPortalAppSidebarComponent);
    return MerchantPortalAppSidebarComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-app-sidebar/sidebarmenu.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-app-sidebar/sidebarmenu.ts ***!
  \***********************************************************************************/
/*! exports provided: sidebarmenu */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sidebarmenu", function() { return sidebarmenu; });
var sidebarmenu = [
    // {
    //     icon: '<i class="icon-dashboard"></i>', label: 'Dashboard', router_link: '/merchant-portal', func: () => {
    //     }
    // },
    {
        icon: '<i class="icon-merchant"></i>', icons: 'icon-1', label: 'Front Explorer', router_link: '/merchant-portal/homepage', func: function () {
        }
    },
    {
        icon: ' <i class="icon-email-history"></i>', icons: 'icon-12', label: 'Inbox', router_link: '/merchant-portal/inbox', func: function () {
        }
    },
    {
        icon: '<i class="icon-order_history_merchant"></i>', icons: 'icon-9', label: 'Order Histories', router_link: '/merchant-portal/order-histories', func: function () {
        }
    },
    {
        icon: '<i class="icon-my_rupiah"></i>',
        icons: 'icon-2',
        label: 'My Money',
        router_link: '/merchant-portal/my-money',
        func: function () { }
    },
    {
        icon: '<i class="icon-shopping-cart"></i>', icons: 'icon-10', label: 'Sales Order', router_link: '/merchant-portal/sales-order', func: function () {
        }
    },
    // { icon: '<i class="fa fa-envelope"></i>', label: 'Inbox', router_link: '/merchant-portal' , func:()=>{}},
    {
        icon: '<i class="icon-product-list-merchant"></i>', icons: 'icon-4', label: 'Product List', router_link: '/merchant-portal/product-list/', func: function () {
        }
    },
    // {
    //     icon: '<i class="icon-product-list-merchant"></i>', label: 'Report Product', router_link: '/merchant-portal/report', func: () => {
    //     }
    // },
    // {
    //     icon: '<i class="fa fa-chart-bar"></i>',
    //     label: 'Evoucher Report',
    //     router_link: '/merchant-portal/evoucher-sales-report',
    //     func: () => {
    //     }
    // },
    {
        icon: '<i class="icon-member"></i>', icons: 'icon-7', label: 'Your Profile', router_link: '/merchant-portal/profile', func: function () {
        }
    },
];


/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-banner/merchant-portal-banner.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-banner/merchant-portal-banner.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvbWVyY2hhbnQtcG9ydGFsLWJhbm5lci9tZXJjaGFudC1wb3J0YWwtYmFubmVyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-banner/merchant-portal-banner.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-banner/merchant-portal-banner.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: MerchantPortalBannerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantPortalBannerComponent", function() { return MerchantPortalBannerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MerchantPortalBannerComponent = /** @class */ (function () {
    function MerchantPortalBannerComponent() {
    }
    MerchantPortalBannerComponent.prototype.ngOnInit = function () {
    };
    MerchantPortalBannerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-portal-banner',
            template: __webpack_require__(/*! raw-loader!./merchant-portal-banner.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-banner/merchant-portal-banner.component.html"),
            styles: [__webpack_require__(/*! ./merchant-portal-banner.component.scss */ "./src/app/layout/merchant-portal/merchant-portal-banner/merchant-portal-banner.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MerchantPortalBannerComponent);
    return MerchantPortalBannerComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-detail-inbox/merchant-portal-detail-inbox.component.scss":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-detail-inbox/merchant-portal-detail-inbox.component.scss ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvbWVyY2hhbnQtcG9ydGFsLWRldGFpbC1pbmJveC9tZXJjaGFudC1wb3J0YWwtZGV0YWlsLWluYm94LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-detail-inbox/merchant-portal-detail-inbox.component.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-detail-inbox/merchant-portal-detail-inbox.component.ts ***!
  \***************************************************************************************************************/
/*! exports provided: MerchantPortalDetailInboxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantPortalDetailInboxComponent", function() { return MerchantPortalDetailInboxComponent; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var MerchantPortalDetailInboxComponent = /** @class */ (function () {
    function MerchantPortalDetailInboxComponent(route, router, orderHistoryService) {
        this.route = route;
        this.router = router;
        this.orderHistoryService = orderHistoryService;
    }
    MerchantPortalDetailInboxComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MerchantPortalDetailInboxComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        console.log("params", params);
                        this.order_id = params.id;
                        return [2 /*return*/];
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    MerchantPortalDetailInboxComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"] },
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] }
    ]; };
    MerchantPortalDetailInboxComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-merchant-portal-detail-inbox',
            template: __webpack_require__(/*! raw-loader!./merchant-portal-detail-inbox.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-detail-inbox/merchant-portal-detail-inbox.component.html"),
            styles: [__webpack_require__(/*! ./merchant-portal-detail-inbox.component.scss */ "./src/app/layout/merchant-portal/merchant-portal-detail-inbox/merchant-portal-detail-inbox.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"], _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"]])
    ], MerchantPortalDetailInboxComponent);
    return MerchantPortalDetailInboxComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".background {\n  background-color: white;\n}\n\n.empty {\n  text-align: center;\n  padding-top: 70px;\n  padding-bottom: 100px;\n}\n\n#button {\n  text-align: center;\n  margin-top: 20px;\n}\n\n.mail-box {\n  border-collapse: collapse;\n  border-spacing: 0;\n  display: table;\n  table-layout: fixed;\n  width: 100%;\n  margin-top: 40px;\n}\n\n.mail-box aside {\n  display: table-cell;\n  float: none;\n  height: 100%;\n  padding: 0;\n  vertical-align: top;\n}\n\n.mail-box .sm-side {\n  background: none repeat scroll 0 0 #f2f2f2;\n  border-radius: 4px 0 0 4px;\n  width: 20%;\n}\n\n.mail-box .lg-side {\n  background: none repeat scroll 0 0 #fff;\n  border-radius: 0 4px 4px 0;\n  width: 100%;\n  overflow: hidden;\n  transition: ease 0.5s all 0s;\n}\n\n.mail-box .lg-side .table-inbox .tr {\n  display: block;\n  overflow: hidden;\n  height: 70px;\n  position: relative;\n  cursor: pointer;\n  border-bottom: 1px solid #ccc;\n}\n\n.mail-box .lg-side .table-inbox .tr .td {\n  float: left;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  padding: 12px !important;\n  height: 100%;\n}\n\n.mail-box .lg-side .table-inbox .tr .td div {\n  width: 100%;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n}\n\n.mail-box .lg-side .table-inbox .tr .td div h3 {\n  overflow: hidden !important;\n  text-overflow: ellipsis !important;\n}\n\n.mail-box .lg-side .table-inbox .tr .td:first-child {\n  width: 35px;\n}\n\n.mail-box .lg-side .table-inbox .tr .td:nth-child(3) {\n  width: calc(100% - 310px);\n}\n\n.mail-box .lg-side .table-inbox .tr .td:nth-child(4) {\n  width: 125px;\n}\n\n.mail-box .lg-side .table-inbox .tr:last-child {\n  border-bottom: unset;\n}\n\n.mail-box .email-content {\n  width: 0px;\n  overflow: hidden;\n  position: relative;\n  transition: ease 1s all 0s;\n  margin-left: 0.5s;\n  padding-left: 35px;\n  margin-top: 21px;\n  border-left: 2px solid blue;\n  background-color: #f7f8fa;\n  color: #373738;\n}\n\n/* when the box is opened */\n\n.mail-box.true .lg-side {\n  width: 35%;\n}\n\n.mail-box.true .lg-side .table-inbox .tr .td:first-child {\n  width: 35px;\n}\n\n.mail-box.true .lg-side .table-inbox .tr .td:nth-child(2) {\n  width: calc(100% - 115px);\n}\n\n.mail-box.true .lg-side .table-inbox .tr .td:nth-child(3) {\n  display: none;\n}\n\n.mail-box.true .lg-side .table-inbox .tr .td:nth-child(4) {\n  width: 80px;\n}\n\n.mail-box.true .email-content {\n  width: 90%;\n  overflow: hidden;\n  position: relative;\n}\n\n.mail-box .sm-side .user-head {\n  background: none repeat scroll 0 0 #00a8b3;\n  border-radius: 4px 0 0;\n  color: #fff;\n  min-height: 80px;\n  padding: 10px;\n  display: none;\n}\n\n.user-head .inbox-avatar {\n  float: left;\n  width: 65px;\n}\n\n.user-head .inbox-avatar img {\n  border-radius: 4px;\n}\n\n.user-head .user-name {\n  display: inline-block;\n  margin: 0 0 0 10px;\n}\n\n.user-head .user-name h5 {\n  font-size: 14px;\n  font-weight: 300;\n  margin-bottom: 0;\n  margin-top: 15px;\n}\n\n.user-head .user-name h5 a {\n  color: #fff;\n}\n\n.user-head .user-name span a {\n  color: #87e2e7;\n  font-size: 12px;\n}\n\na.mail-dropdown {\n  background: none repeat scroll 0 0 #80d3d9;\n  border-radius: 2px;\n  color: #01a7b3;\n  font-size: 10px;\n  margin-top: 20px;\n  padding: 3px 5px;\n}\n\n.inbox-body {\n  padding: 20px;\n  margin-bottom: 40px;\n}\n\n.btn-compose {\n  background: none repeat scroll 0 0 #ff6c60;\n  color: #fff;\n  padding: 12px 0;\n  text-align: center;\n  width: 100%;\n}\n\n.btn-compose:hover {\n  background: none repeat scroll 0 0 #f5675c;\n  color: #fff;\n}\n\nul.inbox-nav {\n  display: inline-block;\n  margin: 0;\n  padding: 0;\n  width: 100%;\n}\n\n.inbox-divider {\n  border-bottom: 1px solid #d5d8df;\n}\n\nul.inbox-nav li {\n  display: inline-block;\n  line-height: 45px;\n  width: 100%;\n}\n\nul.inbox-nav li a {\n  color: #6a6a6a;\n  display: inline-block;\n  line-height: 45px;\n  padding: 0 20px;\n  width: 100%;\n}\n\nul.inbox-nav li a:hover,\nul.inbox-nav li.active a,\nul.inbox-nav li a:focus {\n  background: none repeat scroll 0 0 white;\n  color: #6a6a6a;\n}\n\nul.inbox-nav li a i {\n  color: #6a6a6a;\n  font-size: 16px;\n  padding-right: 10px;\n}\n\nul.inbox-nav li a span.label {\n  margin-top: 13px;\n}\n\nul.labels-info li h4 {\n  color: #5c5c5e;\n  font-size: 13px;\n  padding-left: 15px;\n  padding-right: 15px;\n  padding-top: 5px;\n  text-transform: uppercase;\n}\n\nul.labels-info li {\n  margin: 0;\n}\n\nul.labels-info li a {\n  border-radius: 0;\n  color: #6a6a6a;\n}\n\nul.labels-info li a:hover,\nul.labels-info li a:focus {\n  background: none repeat scroll 0 0 #d5d7de;\n  color: #6a6a6a;\n}\n\nul.labels-info li a i {\n  padding-right: 10px;\n}\n\n.nav.nav-pills.nav-stacked.labels-info p {\n  color: #9d9f9e;\n  font-size: 11px;\n  margin-bottom: 0;\n  padding: 0 22px;\n}\n\n.inbox-head {\n  background: none repeat scroll 0 0 #41cac0;\n  border-radius: 0 4px 0 0;\n  color: #fff;\n  min-height: 80px;\n  padding: 20px;\n}\n\n.inbox-head h3 {\n  display: inline-block;\n  font-weight: 300;\n  margin: 0;\n  padding-top: 6px;\n}\n\n.inbox-head .sr-input {\n  border: medium none;\n  border-radius: 4px 0 0 4px;\n  box-shadow: none;\n  color: #8a8a8a;\n  float: left;\n  height: 40px;\n  padding: 0 10px;\n}\n\n.inbox-head .sr-btn {\n  background: none repeat scroll 0 0 #00a6b2;\n  border: medium none;\n  border-radius: 0 4px 4px 0;\n  color: #fff;\n  height: 40px;\n  padding: 0 20px;\n}\n\n.table-inbox {\n  border: 1px solid #d3d3d3;\n  margin-bottom: 0;\n  width: 60%;\n  font-size: 14px;\n  width: 100%;\n}\n\n.table-inbox .tr .td:hover {\n  cursor: pointer;\n}\n\n.table-inbox .tr .td .fa-star.inbox-started,\n.table-inbox .tr .td .fa-star:hover {\n  color: #f78a09;\n}\n\n.table-inbox .tr .td .fa-star {\n  color: #d5d5d5;\n}\n\n.table-inbox .tr.unread .td {\n  background: none repeat scroll 0 0 #eaeaea;\n  font-weight: 600;\n}\n\nul.inbox-pagination {\n  float: right;\n}\n\nul.inbox-pagination li {\n  float: left;\n}\n\n.mail-option {\n  display: inline-block;\n  margin-bottom: 10px;\n  width: 100%;\n}\n\n.mail-option .chk-all,\n.mail-option .btn-group {\n  margin-right: 5px;\n}\n\n.mail-option .chk-all,\n.mail-option .btn-group a.btn {\n  background: none repeat scroll 0 0 #fcfcfc;\n  border: 1px solid #e7e7e7;\n  border-radius: 3px !important;\n  color: #afafaf;\n  display: inline-block;\n  padding: 5px 10px;\n}\n\n.inbox-pagination a.np-btn {\n  background: none repeat scroll 0 0 #fcfcfc;\n  border: 1px solid #e7e7e7;\n  border-radius: 3px !important;\n  color: #afafaf;\n  display: inline-block;\n  padding: 5px 15px;\n}\n\n.mail-option .chk-all input[type=checkbox] {\n  margin-top: 0;\n}\n\n.mail-option .btn-group a.all {\n  border: medium none;\n  padding: 0;\n}\n\n.inbox-pagination a.np-btn {\n  margin-left: 5px;\n}\n\n.inbox-pagination li span {\n  display: inline-block;\n  margin-right: 5px;\n  margin-top: 7px;\n}\n\n.fileinput-button {\n  background: none repeat scroll 0 0 #eeeeee;\n  border: 1px solid #e6e6e6;\n}\n\n.inbox-body .modal .modal-body input,\n.inbox-body .modal .modal-body textarea {\n  border: 1px solid #e6e6e6;\n  box-shadow: none;\n}\n\n.btn-send,\n.btn-send:hover {\n  background: none repeat scroll 0 0 #00a8b3;\n  color: #fff;\n}\n\n.btn-send:hover {\n  background: none repeat scroll 0 0 #009da7;\n}\n\n.modal-header h4.modal-title {\n  font-family: \"Open Sans\", sans-serif;\n  font-weight: 300;\n}\n\n.modal-body label {\n  font-family: \"Open Sans\", sans-serif;\n  font-weight: 400;\n}\n\n.heading-inbox h4 {\n  border-bottom: 1px solid #ddd;\n  color: #444;\n  font-size: 18px;\n  margin-top: 20px;\n  padding-bottom: 10px;\n}\n\n.sender-info {\n  margin-bottom: 20px;\n}\n\n.sender-info img {\n  height: 30px;\n  width: 30px;\n}\n\n.sender-dropdown {\n  background: none repeat scroll 0 0 #eaeaea;\n  color: #777;\n  font-size: 10px;\n  padding: 0 3px;\n}\n\n.view-mail a {\n  color: #ff6c60;\n}\n\n.attachment-mail {\n  margin-top: 30px;\n}\n\n.attachment-mail ul {\n  display: inline-block;\n  margin-bottom: 30px;\n  width: 100%;\n}\n\n.attachment-mail ul li {\n  float: left;\n  margin-bottom: 10px;\n  margin-right: 10px;\n  width: 150px;\n}\n\n.attachment-mail ul li img {\n  width: 100%;\n}\n\n.attachment-mail ul li span {\n  float: right;\n}\n\n.attachment-mail .file-name {\n  float: left;\n}\n\n.attachment-mail .links {\n  display: inline-block;\n  width: 100%;\n}\n\n.fileinput-button {\n  float: left;\n  margin-right: 4px;\n  overflow: hidden;\n  position: relative;\n}\n\n.fileinput-button input {\n  cursor: pointer;\n  direction: ltr;\n  font-size: 23px;\n  margin: 0;\n  opacity: 0;\n  position: absolute;\n  right: 0;\n  top: 0;\n  -webkit-transform: translate(-300px, 0px) scale(4);\n          transform: translate(-300px, 0px) scale(4);\n}\n\n.fileupload-buttonbar .btn,\n.fileupload-buttonbar .toggle {\n  margin-bottom: 5px;\n}\n\n.files .progress {\n  width: 200px;\n}\n\n.fileupload-processing .fileupload-loading {\n  display: block;\n}\n\n* html .fileinput-button {\n  line-height: 24px;\n  margin: 1px -3px 0 0;\n}\n\n* + html .fileinput-button {\n  margin: 1px 0 0;\n  padding: 2px 15px;\n}\n\n@media (max-width: 767px) {\n  .files .btn span {\n    display: none;\n  }\n\n  .files .preview * {\n    width: 40px;\n  }\n\n  .files .name * {\n    display: inline-block;\n    width: 80px;\n    word-wrap: break-word;\n  }\n\n  .files .progress {\n    width: 20px;\n  }\n\n  .files .delete {\n    width: 60px;\n  }\n}\n\nul {\n  list-style-type: none;\n  padding: 0px;\n  margin: 0px;\n}\n\n.class {\n  outline: none;\n}\n\n.inbox {\n  background-color: white;\n}\n\n#loader {\n  transition: all 0.3s ease-in-out;\n  opacity: 1;\n  visibility: visible;\n  position: fixed;\n  height: 100vh;\n  width: 100%;\n  background: #fff;\n  z-index: 90000;\n}\n\n#loader.fadeOut {\n  opacity: 0;\n  visibility: hidden;\n}\n\n.spinner {\n  width: 40px;\n  height: 40px;\n  position: absolute;\n  top: calc(50% - 20px);\n  left: calc(50% - 20px);\n  background-color: #333;\n  border-radius: 100%;\n  -webkit-animation: sk-scaleout 1s infinite ease-in-out;\n  animation: sk-scaleout 1s infinite ease-in-out;\n}\n\n.email-app {\n  margin: 0;\n}\n\n.email-app .email-wrapper {\n  margin: 0;\n  padding: 0;\n  overflow: auto;\n  min-height: 100%;\n  transition: all 0.3s ease-in-out;\n}\n\n.description {\n  font-size: 16px;\n  font-weight: bold;\n}\n\n.description h3 {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\nh3 {\n  overflow: hidden !important;\n  text-overflow: ellipsis !important;\n}\n\n.message {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, Oxygen, Ubuntu, Cantarell, \"Open Sans\", \"Helvetica Neue\", sans-serif;\n}\n\nbutton {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9tZXJjaGFudC1wb3J0YWwtaW5ib3gvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtZXJjaGFudC1wb3J0YWxcXG1lcmNoYW50LXBvcnRhbC1pbmJveFxcbWVyY2hhbnQtcG9ydGFsLWluYm94LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LXBvcnRhbC1pbmJveC9tZXJjaGFudC1wb3J0YWwtaW5ib3guY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDRSx1QkFBQTtBQ0FGOztBREdBO0VBQ0Usa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0FDQUY7O0FER0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FDQUY7O0FESUE7RUFDRSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDREY7O0FESUE7RUFDRSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0FDREY7O0FESUE7RUFDRSwwQ0FBQTtFQUNBLDBCQUFBO0VBQ0EsVUFBQTtBQ0RGOztBREtFO0VBQ0UsdUNBQUE7RUFDQSwwQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLDRCQUFBO0FDRko7O0FES007RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsNkJBQUE7QUNIUjs7QURJUTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHdCQUFBO0VBQ0EsWUFBQTtBQ0ZWOztBREdVO0VBQ0UsV0FBQTtFQUNBLG1CQUFBO0VBRUEsdUJBQUE7QUNGWjs7QURJWTtFQUNJLDJCQUFBO0VBQ0Esa0NBQUE7QUNGaEI7O0FET1E7RUFDRSxXQUFBO0FDTFY7O0FEVVE7RUFDRSx5QkFBQTtBQ1JWOztBRFVRO0VBQ0UsWUFBQTtBQ1JWOztBRFdNO0VBQ0Usb0JBQUE7QUNUUjs7QURjRTtFQUNFLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsMEJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwyQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtBQ1pKOztBRGdCQSwyQkFBQTs7QUFFRTtFQUNFLFVBQUE7QUNkSjs7QURpQlE7RUFDRSxXQUFBO0FDZlY7O0FEaUJRO0VBQ0UseUJBQUE7QUNmVjs7QURpQlE7RUFDRSxhQUFBO0FDZlY7O0FEaUJRO0VBQ0UsV0FBQTtBQ2ZWOztBRG9CRTtFQUNFLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDbEJKOztBRHdCQTtFQUNFLDBDQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtBQ3JCRjs7QUR3QkE7RUFDRSxXQUFBO0VBQ0EsV0FBQTtBQ3JCRjs7QUR3QkE7RUFDRSxrQkFBQTtBQ3JCRjs7QUR3QkE7RUFDRSxxQkFBQTtFQUNBLGtCQUFBO0FDckJGOztBRHdCQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNyQkY7O0FEd0JBO0VBQ0UsV0FBQTtBQ3JCRjs7QUR3QkE7RUFDRSxjQUFBO0VBQ0EsZUFBQTtBQ3JCRjs7QUR3QkE7RUFDRSwwQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDckJGOztBRHdCQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQ3JCRjs7QUR3QkE7RUFDRSwwQ0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDckJGOztBRHdCQTtFQUNFLDBDQUFBO0VBQ0EsV0FBQTtBQ3JCRjs7QUR3QkE7RUFDRSxxQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQ3JCRjs7QUR3QkE7RUFDRSxnQ0FBQTtBQ3JCRjs7QUR3QkE7RUFDRSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtBQ3JCRjs7QUR3QkE7RUFDRSxjQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDckJGOztBRHdCQTs7O0VBR0Usd0NBQUE7RUFDQSxjQUFBO0FDckJGOztBRHdCQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNyQkY7O0FEd0JBO0VBQ0UsZ0JBQUE7QUNyQkY7O0FEd0JBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtBQ3JCRjs7QUR3QkE7RUFDRSxTQUFBO0FDckJGOztBRHdCQTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtBQ3JCRjs7QUR3QkE7O0VBRUUsMENBQUE7RUFDQSxjQUFBO0FDckJGOztBRHdCQTtFQUNFLG1CQUFBO0FDckJGOztBRHdCQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDckJGOztBRHdCQTtFQUNFLDBDQUFBO0VBQ0Esd0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0FDckJGOztBRHdCQTtFQUNFLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7QUNyQkY7O0FEd0JBO0VBQ0UsbUJBQUE7RUFDQSwwQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ3JCRjs7QUR3QkE7RUFDRSwwQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsMEJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNyQkY7O0FEd0JBO0VBQ0UseUJBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ3JCRjs7QUR3QkE7RUFDRSxlQUFBO0FDckJGOztBRHdCQTs7RUFFRSxjQUFBO0FDckJGOztBRHdCQTtFQUNFLGNBQUE7QUNyQkY7O0FEd0JBO0VBQ0UsMENBQUE7RUFDQSxnQkFBQTtBQ3JCRjs7QUR3QkE7RUFDRSxZQUFBO0FDckJGOztBRHdCQTtFQUNFLFdBQUE7QUNyQkY7O0FEd0JBO0VBQ0UscUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUNyQkY7O0FEd0JBOztFQUVFLGlCQUFBO0FDckJGOztBRHdCQTs7RUFFRSwwQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtBQ3JCRjs7QUR3QkE7RUFDRSwwQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtBQ3JCRjs7QUR3QkE7RUFDRSxhQUFBO0FDckJGOztBRHdCQTtFQUNFLG1CQUFBO0VBQ0EsVUFBQTtBQ3JCRjs7QUR3QkE7RUFDRSxnQkFBQTtBQ3JCRjs7QUR3QkE7RUFDRSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ3JCRjs7QUR3QkE7RUFDRSwwQ0FBQTtFQUNBLHlCQUFBO0FDckJGOztBRHdCQTs7RUFFRSx5QkFBQTtFQUNBLGdCQUFBO0FDckJGOztBRHdCQTs7RUFFRSwwQ0FBQTtFQUNBLFdBQUE7QUNyQkY7O0FEd0JBO0VBQ0UsMENBQUE7QUNyQkY7O0FEd0JBO0VBQ0Usb0NBQUE7RUFDQSxnQkFBQTtBQ3JCRjs7QUR3QkE7RUFDRSxvQ0FBQTtFQUNBLGdCQUFBO0FDckJGOztBRHdCQTtFQUNFLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0FDckJGOztBRHdCQTtFQUNFLG1CQUFBO0FDckJGOztBRHdCQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0FDckJGOztBRHdCQTtFQUNFLDBDQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDckJGOztBRHdCQTtFQUNFLGNBQUE7QUNyQkY7O0FEd0JBO0VBQ0UsZ0JBQUE7QUNyQkY7O0FEd0JBO0VBQ0UscUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUNyQkY7O0FEd0JBO0VBQ0UsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDckJGOztBRHdCQTtFQUNFLFdBQUE7QUNyQkY7O0FEd0JBO0VBQ0UsWUFBQTtBQ3JCRjs7QUR3QkE7RUFDRSxXQUFBO0FDckJGOztBRHdCQTtFQUNFLHFCQUFBO0VBQ0EsV0FBQTtBQ3JCRjs7QUR3QkE7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDckJGOztBRHdCQTtFQUNFLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsTUFBQTtFQUNBLGtEQUFBO1VBQUEsMENBQUE7QUNyQkY7O0FEd0JBOztFQUVFLGtCQUFBO0FDckJGOztBRHdCQTtFQUNFLFlBQUE7QUNyQkY7O0FEd0JBO0VBQ0UsY0FBQTtBQ3JCRjs7QUR3QkE7RUFDRSxpQkFBQTtFQUNBLG9CQUFBO0FDckJGOztBRHdCQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQ3JCRjs7QUR3QkE7RUFDRTtJQUNFLGFBQUE7RUNyQkY7O0VEd0JBO0lBQ0UsV0FBQTtFQ3JCRjs7RUR3QkE7SUFDRSxxQkFBQTtJQUNBLFdBQUE7SUFDQSxxQkFBQTtFQ3JCRjs7RUR3QkE7SUFDRSxXQUFBO0VDckJGOztFRHdCQTtJQUNFLFdBQUE7RUNyQkY7QUFDRjs7QUR3QkE7RUFDRSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDdEJGOztBRHlCQTtFQUNFLGFBQUE7QUN0QkY7O0FEeUJBO0VBQ0UsdUJBQUE7QUN0QkY7O0FEeUJBO0VBQ0UsZ0NBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUN0QkY7O0FEeUJBO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0FDdEJGOztBRHlCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHNEQUFBO0VBQ0EsOENBQUE7QUN0QkY7O0FEeUJBO0VBU0UsU0FBQTtBQzlCRjs7QURzQkU7RUFDRSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdDQUFBO0FDcEJKOztBRDBCQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQ3ZCRjs7QUR3QkU7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7QUN0Qk47O0FEMEJBO0VBQ0UsMkJBQUE7RUFDQSxrQ0FBQTtBQ3ZCRjs7QUQwQkE7RUFDRSx3SUFBQTtBQ3ZCRjs7QUQwQkE7RUFDRSxxQkFBQTtBQ3ZCRiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvbWVyY2hhbnQtcG9ydGFsLWluYm94L21lcmNoYW50LXBvcnRhbC1pbmJveC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgXHJcbi5iYWNrZ3JvdW5ke1xyXG4gIGJhY2tncm91bmQtY29sb3I6d2hpdGU7XHJcbn1cclxuXHJcbi5lbXB0eXtcclxuICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICBwYWRkaW5nLXRvcDo3MHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOjEwMHB4O1xyXG59XHJcblxyXG4jYnV0dG9ue1xyXG4gIHRleHQtYWxpZ246Y2VudGVyO1xyXG4gIG1hcmdpbi10b3A6MjBweDtcclxufVxyXG5cclxuXHJcbi5tYWlsLWJveCB7XHJcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICBib3JkZXItc3BhY2luZzogMDtcclxuICBkaXNwbGF5OiB0YWJsZTtcclxuICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG1hcmdpbi10b3A6IDQwcHg7XHJcbn1cclxuXHJcbi5tYWlsLWJveCBhc2lkZSB7XHJcbiAgZGlzcGxheTogdGFibGUtY2VsbDtcclxuICBmbG9hdDogbm9uZTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgcGFkZGluZzogMDtcclxuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG59XHJcblxyXG4ubWFpbC1ib3ggLnNtLXNpZGUge1xyXG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgI2YyZjJmMjtcclxuICBib3JkZXItcmFkaXVzOiA0cHggMCAwIDRweDtcclxuICB3aWR0aDogMjAlO1xyXG59XHJcblxyXG4ubWFpbC1ib3gge1xyXG4gIC5sZy1zaWRlIHtcclxuICAgIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgI2ZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgNHB4IDRweCAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdHJhbnNpdGlvbjogZWFzZSAwLjVzIGFsbCAwcztcclxuXHJcbiAgICAudGFibGUtaW5ib3gge1xyXG4gICAgICAudHIge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgLnRkIHtcclxuICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgIHBhZGRpbmc6IDEycHggIWltcG9ydGFudDtcclxuICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgIGRpdiB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgICAgICAvLyBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuXHJcbiAgICAgICAgICAgIGgzIHtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW4gIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICAudGQ6Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgICAgd2lkdGg6IDM1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZDpudGgtY2hpbGQoMikge1xyXG4gICAgICAgICAgLy8gd2lkdGg6IDE1MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAudGQ6bnRoLWNoaWxkKDMpIHtcclxuICAgICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAzMTBweCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZDpudGgtY2hpbGQoNCkge1xyXG4gICAgICAgICAgd2lkdGg6IDEyNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAudHI6bGFzdC1jaGlsZHtcclxuICAgICAgICBib3JkZXItYm90dG9tOiB1bnNldDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmVtYWlsLWNvbnRlbnQge1xyXG4gICAgd2lkdGg6IDBweDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0cmFuc2l0aW9uOiBlYXNlIDFzIGFsbCAwcztcclxuICAgIG1hcmdpbi1sZWZ0OiAwLjVzO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAzNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogMjFweDtcclxuICAgIGJvcmRlci1sZWZ0OiAycHggc29saWQgYmx1ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y4ZmE7XHJcbiAgICBjb2xvcjojMzczNzM4O1xyXG4gIH1cclxufVxyXG5cclxuLyogd2hlbiB0aGUgYm94IGlzIG9wZW5lZCAqL1xyXG4ubWFpbC1ib3gudHJ1ZSB7XHJcbiAgLmxnLXNpZGUge1xyXG4gICAgd2lkdGg6IDM1JTtcclxuICAgIC50YWJsZS1pbmJveCB7XHJcbiAgICAgIC50ciB7XHJcbiAgICAgICAgLnRkOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgIHdpZHRoOiAzNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAudGQ6bnRoLWNoaWxkKDIpIHtcclxuICAgICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAxMTVweCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZDpudGgtY2hpbGQoMykge1xyXG4gICAgICAgICAgZGlzcGxheTogbm9uZVxyXG4gICAgICAgIH1cclxuICAgICAgICAudGQ6bnRoLWNoaWxkKDQpIHtcclxuICAgICAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAuZW1haWwtY29udGVudCB7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIC8vIG1hcmdpbi10b3A6MTAwcHg7XHJcbiAgICAgIFxyXG4gIH1cclxufVxyXG5cclxuLm1haWwtYm94IC5zbS1zaWRlIC51c2VyLWhlYWQge1xyXG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgIzAwYThiMztcclxuICBib3JkZXItcmFkaXVzOiA0cHggMCAwO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIG1pbi1oZWlnaHQ6IDgwcHg7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4udXNlci1oZWFkIC5pbmJveC1hdmF0YXIge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHdpZHRoOiA2NXB4O1xyXG59XHJcblxyXG4udXNlci1oZWFkIC5pbmJveC1hdmF0YXIgaW1nIHtcclxuICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbn1cclxuXHJcbi51c2VyLWhlYWQgLnVzZXItbmFtZSB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIG1hcmdpbjogMCAwIDAgMTBweDtcclxufVxyXG5cclxuLnVzZXItaGVhZCAudXNlci1uYW1lIGg1IHtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDMwMDtcclxuICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbn1cclxuXHJcbi51c2VyLWhlYWQgLnVzZXItbmFtZSBoNSBhIHtcclxuICBjb2xvcjogI2ZmZjtcclxufVxyXG5cclxuLnVzZXItaGVhZCAudXNlci1uYW1lIHNwYW4gYSB7XHJcbiAgY29sb3I6ICM4N2UyZTc7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcblxyXG5hLm1haWwtZHJvcGRvd24ge1xyXG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgIzgwZDNkOTtcclxuICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgY29sb3I6ICMwMWE3YjM7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgcGFkZGluZzogM3B4IDVweDtcclxufVxyXG5cclxuLmluYm94LWJvZHkge1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcclxufVxyXG5cclxuLmJ0bi1jb21wb3NlIHtcclxuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICNmZjZjNjA7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgcGFkZGluZzogMTJweCAwO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmJ0bi1jb21wb3NlOmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICNmNTY3NWM7XHJcbiAgY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbnVsLmluYm94LW5hdiB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uaW5ib3gtZGl2aWRlciB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkNWQ4ZGY7XHJcbn1cclxuXHJcbnVsLmluYm94LW5hdiBsaSB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGxpbmUtaGVpZ2h0OiA0NXB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG51bC5pbmJveC1uYXYgbGkgYSB7XHJcbiAgY29sb3I6ICM2YTZhNmE7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGxpbmUtaGVpZ2h0OiA0NXB4O1xyXG4gIHBhZGRpbmc6IDAgMjBweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxudWwuaW5ib3gtbmF2IGxpIGE6aG92ZXIsXHJcbnVsLmluYm94LW5hdiBsaS5hY3RpdmUgYSxcclxudWwuaW5ib3gtbmF2IGxpIGE6Zm9jdXMge1xyXG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgd2hpdGU7XHJcbiAgY29sb3I6ICM2YTZhNmE7XHJcbn1cclxuXHJcbnVsLmluYm94LW5hdiBsaSBhIGkge1xyXG4gIGNvbG9yOiAjNmE2YTZhO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG51bC5pbmJveC1uYXYgbGkgYSBzcGFuLmxhYmVsIHtcclxuICBtYXJnaW4tdG9wOiAxM3B4O1xyXG59XHJcblxyXG51bC5sYWJlbHMtaW5mbyBsaSBoNCB7XHJcbiAgY29sb3I6ICM1YzVjNWU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIHBhZGRpbmctbGVmdDogMTVweDtcclxuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xyXG4gIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxudWwubGFiZWxzLWluZm8gbGkge1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG5cclxudWwubGFiZWxzLWluZm8gbGkgYSB7XHJcbiAgYm9yZGVyLXJhZGl1czogMDtcclxuICBjb2xvcjogIzZhNmE2YTtcclxufVxyXG5cclxudWwubGFiZWxzLWluZm8gbGkgYTpob3ZlcixcclxudWwubGFiZWxzLWluZm8gbGkgYTpmb2N1cyB7XHJcbiAgYmFja2dyb3VuZDogbm9uZSByZXBlYXQgc2Nyb2xsIDAgMCAjZDVkN2RlO1xyXG4gIGNvbG9yOiAjNmE2YTZhO1xyXG59XHJcblxyXG51bC5sYWJlbHMtaW5mbyBsaSBhIGkge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5uYXYubmF2LXBpbGxzLm5hdi1zdGFja2VkLmxhYmVscy1pbmZvIHAge1xyXG4gIGNvbG9yOiAjOWQ5ZjllO1xyXG4gIGZvbnQtc2l6ZTogMTFweDtcclxuICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gIHBhZGRpbmc6IDAgMjJweDtcclxufVxyXG5cclxuLmluYm94LWhlYWQge1xyXG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgIzQxY2FjMDtcclxuICBib3JkZXItcmFkaXVzOiAwIDRweCAwIDA7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgbWluLWhlaWdodDogODBweDtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG59XHJcblxyXG4uaW5ib3gtaGVhZCBoMyB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmctdG9wOiA2cHg7XHJcbn1cclxuXHJcbi5pbmJveC1oZWFkIC5zci1pbnB1dCB7XHJcbiAgYm9yZGVyOiBtZWRpdW0gbm9uZTtcclxuICBib3JkZXItcmFkaXVzOiA0cHggMCAwIDRweDtcclxuICBib3gtc2hhZG93OiBub25lO1xyXG4gIGNvbG9yOiAjOGE4YThhO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIGhlaWdodDogNDBweDtcclxuICBwYWRkaW5nOiAwIDEwcHg7XHJcbn1cclxuXHJcbi5pbmJveC1oZWFkIC5zci1idG4ge1xyXG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgIzAwYTZiMjtcclxuICBib3JkZXI6IG1lZGl1bSBub25lO1xyXG4gIGJvcmRlci1yYWRpdXM6IDAgNHB4IDRweCAwO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGhlaWdodDogNDBweDtcclxuICBwYWRkaW5nOiAwIDIwcHg7XHJcbn1cclxuXHJcbi50YWJsZS1pbmJveCB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2QzZDNkMztcclxuICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gIHdpZHRoOiA2MCU7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4udGFibGUtaW5ib3ggLnRyIC50ZDpob3ZlciB7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4udGFibGUtaW5ib3ggLnRyIC50ZCAuZmEtc3Rhci5pbmJveC1zdGFydGVkLFxyXG4udGFibGUtaW5ib3ggLnRyIC50ZCAuZmEtc3Rhcjpob3ZlciB7XHJcbiAgY29sb3I6ICNmNzhhMDk7XHJcbn1cclxuXHJcbi50YWJsZS1pbmJveCAudHIgLnRkIC5mYS1zdGFyIHtcclxuICBjb2xvcjogI2Q1ZDVkNTtcclxufVxyXG5cclxuLnRhYmxlLWluYm94IC50ci51bnJlYWQgLnRkIHtcclxuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICNlYWVhZWE7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG5cclxudWwuaW5ib3gtcGFnaW5hdGlvbiB7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG51bC5pbmJveC1wYWdpbmF0aW9uIGxpIHtcclxuICBmbG9hdDogbGVmdDtcclxufVxyXG5cclxuLm1haWwtb3B0aW9uIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1haWwtb3B0aW9uIC5jaGstYWxsLFxyXG4ubWFpbC1vcHRpb24gLmJ0bi1ncm91cCB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbi5tYWlsLW9wdGlvbiAuY2hrLWFsbCxcclxuLm1haWwtb3B0aW9uIC5idG4tZ3JvdXAgYS5idG4ge1xyXG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgI2ZjZmNmYztcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTdlN2U3O1xyXG4gIGJvcmRlci1yYWRpdXM6IDNweCAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiAjYWZhZmFmO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxufVxyXG5cclxuLmluYm94LXBhZ2luYXRpb24gYS5ucC1idG4ge1xyXG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgI2ZjZmNmYztcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTdlN2U3O1xyXG4gIGJvcmRlci1yYWRpdXM6IDNweCAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiAjYWZhZmFmO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBwYWRkaW5nOiA1cHggMTVweDtcclxufVxyXG5cclxuLm1haWwtb3B0aW9uIC5jaGstYWxsIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXSB7XHJcbiAgbWFyZ2luLXRvcDogMDtcclxufVxyXG5cclxuLm1haWwtb3B0aW9uIC5idG4tZ3JvdXAgYS5hbGwge1xyXG4gIGJvcmRlcjogbWVkaXVtIG5vbmU7XHJcbiAgcGFkZGluZzogMDtcclxufVxyXG5cclxuLmluYm94LXBhZ2luYXRpb24gYS5ucC1idG4ge1xyXG4gIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbn1cclxuXHJcbi5pbmJveC1wYWdpbmF0aW9uIGxpIHNwYW4ge1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICBtYXJnaW4tdG9wOiA3cHg7XHJcbn1cclxuXHJcbi5maWxlaW5wdXQtYnV0dG9uIHtcclxuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICNlZWVlZWU7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2U2ZTZlNjtcclxufVxyXG5cclxuLmluYm94LWJvZHkgLm1vZGFsIC5tb2RhbC1ib2R5IGlucHV0LFxyXG4uaW5ib3gtYm9keSAubW9kYWwgLm1vZGFsLWJvZHkgdGV4dGFyZWEge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNlNmU2ZTY7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxufVxyXG5cclxuLmJ0bi1zZW5kLFxyXG4uYnRuLXNlbmQ6aG92ZXIge1xyXG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgIzAwYThiMztcclxuICBjb2xvcjogI2ZmZjtcclxufVxyXG5cclxuLmJ0bi1zZW5kOmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICMwMDlkYTc7XHJcbn1cclxuXHJcbi5tb2RhbC1oZWFkZXIgaDQubW9kYWwtdGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbn1cclxuXHJcbi5tb2RhbC1ib2R5IGxhYmVsIHtcclxuICBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG4uaGVhZGluZy1pbmJveCBoNCB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZGQ7XHJcbiAgY29sb3I6ICM0NDQ7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5zZW5kZXItaW5mbyB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLnNlbmRlci1pbmZvIGltZyB7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIHdpZHRoOiAzMHB4O1xyXG59XHJcblxyXG4uc2VuZGVyLWRyb3Bkb3duIHtcclxuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICNlYWVhZWE7XHJcbiAgY29sb3I6ICM3Nzc7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG4gIHBhZGRpbmc6IDAgM3B4O1xyXG59XHJcblxyXG4udmlldy1tYWlsIGEge1xyXG4gIGNvbG9yOiAjZmY2YzYwO1xyXG59XHJcblxyXG4uYXR0YWNobWVudC1tYWlsIHtcclxuICBtYXJnaW4tdG9wOiAzMHB4O1xyXG59XHJcblxyXG4uYXR0YWNobWVudC1tYWlsIHVsIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmF0dGFjaG1lbnQtbWFpbCB1bCBsaSB7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgd2lkdGg6IDE1MHB4O1xyXG59XHJcblxyXG4uYXR0YWNobWVudC1tYWlsIHVsIGxpIGltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5hdHRhY2htZW50LW1haWwgdWwgbGkgc3BhbiB7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG4uYXR0YWNobWVudC1tYWlsIC5maWxlLW5hbWUge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG4uYXR0YWNobWVudC1tYWlsIC5saW5rcyB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZmlsZWlucHV0LWJ1dHRvbiB7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgbWFyZ2luLXJpZ2h0OiA0cHg7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5maWxlaW5wdXQtYnV0dG9uIGlucHV0IHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgZGlyZWN0aW9uOiBsdHI7XHJcbiAgZm9udC1zaXplOiAyM3B4O1xyXG4gIG1hcmdpbjogMDtcclxuICBvcGFjaXR5OiAwO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICByaWdodDogMDtcclxuICB0b3A6IDA7XHJcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTMwMHB4LCAwcHgpIHNjYWxlKDQpO1xyXG59XHJcblxyXG4uZmlsZXVwbG9hZC1idXR0b25iYXIgLmJ0bixcclxuLmZpbGV1cGxvYWQtYnV0dG9uYmFyIC50b2dnbGUge1xyXG4gIG1hcmdpbi1ib3R0b206IDVweDtcclxufVxyXG5cclxuLmZpbGVzIC5wcm9ncmVzcyB7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG59XHJcblxyXG4uZmlsZXVwbG9hZC1wcm9jZXNzaW5nIC5maWxldXBsb2FkLWxvYWRpbmcge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4qIGh0bWwgLmZpbGVpbnB1dC1idXR0b24ge1xyXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xyXG4gIG1hcmdpbjogMXB4IC0zcHggMCAwO1xyXG59XHJcblxyXG4qICsgaHRtbCAuZmlsZWlucHV0LWJ1dHRvbiB7XHJcbiAgbWFyZ2luOiAxcHggMCAwO1xyXG4gIHBhZGRpbmc6IDJweCAxNXB4O1xyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogNzY3cHgpIHtcclxuICAuZmlsZXMgLmJ0biBzcGFuIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAuZmlsZXMgLnByZXZpZXcgKiB7XHJcbiAgICB3aWR0aDogNDBweDtcclxuICB9XHJcblxyXG4gIC5maWxlcyAubmFtZSAqIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgd29yZC13cmFwOiBicmVhay13b3JkO1xyXG4gIH1cclxuXHJcbiAgLmZpbGVzIC5wcm9ncmVzcyB7XHJcbiAgICB3aWR0aDogMjBweDtcclxuICB9XHJcblxyXG4gIC5maWxlcyAuZGVsZXRlIHtcclxuICAgIHdpZHRoOiA2MHB4O1xyXG4gIH1cclxufVxyXG5cclxudWwge1xyXG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcclxuICBwYWRkaW5nOiAwcHg7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbn1cclxuXHJcbi5jbGFzcyB7XHJcbiAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLmluYm94IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuI2xvYWRlciB7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQ7XHJcbiAgb3BhY2l0eTogMTtcclxuICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICBoZWlnaHQ6IDEwMHZoO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgei1pbmRleDogOTAwMDA7XHJcbn1cclxuXHJcbiNsb2FkZXIuZmFkZU91dCB7XHJcbiAgb3BhY2l0eTogMDtcclxuICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbn1cclxuXHJcbi5zcGlubmVyIHtcclxuICB3aWR0aDogNDBweDtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogY2FsYyg1MCUgLSAyMHB4KTtcclxuICBsZWZ0OiBjYWxjKDUwJSAtIDIwcHgpO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMzMzM7XHJcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAtd2Via2l0LWFuaW1hdGlvbjogc2stc2NhbGVvdXQgMXMgaW5maW5pdGUgZWFzZS1pbi1vdXQ7XHJcbiAgYW5pbWF0aW9uOiBzay1zY2FsZW91dCAxcyBpbmZpbml0ZSBlYXNlLWluLW91dDtcclxufVxyXG5cclxuLmVtYWlsLWFwcCB7XHJcbiAgLmVtYWlsLXdyYXBwZXIge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgbWluLWhlaWdodDogMTAwJTtcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xyXG4gIH1cclxuXHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4uZGVzY3JpcHRpb24ge1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBmb250LXdlaWdodDogYm9sZDs7XHJcbiAgaDMge1xyXG4gICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICB9XHJcbn1cclxuXHJcbmgze1xyXG4gIG92ZXJmbG93OmhpZGRlbiAhaW1wb3J0YW50O1xyXG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tZXNzYWdle1xyXG4gIGZvbnQtZmFtaWx5OiAtYXBwbGUtc3lzdGVtLCBCbGlua01hY1N5c3RlbUZvbnQsICdTZWdvZSBVSScsIFJvYm90bywgT3h5Z2VuLCBVYnVudHUsIENhbnRhcmVsbCwgJ09wZW4gU2FucycsICdIZWx2ZXRpY2EgTmV1ZScsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbmJ1dHRvbntcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuXHJcbi8vIC5jbG9zZXtcclxuLy8gICBvcGFjaXR5OiAwO1xyXG4vLyB9XHJcbiIsIi5iYWNrZ3JvdW5kIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5lbXB0eSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDcwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMDBweDtcbn1cblxuI2J1dHRvbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cblxuLm1haWwtYm94IHtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgYm9yZGVyLXNwYWNpbmc6IDA7XG4gIGRpc3BsYXk6IHRhYmxlO1xuICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogNDBweDtcbn1cblxuLm1haWwtYm94IGFzaWRlIHtcbiAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgZmxvYXQ6IG5vbmU7XG4gIGhlaWdodDogMTAwJTtcbiAgcGFkZGluZzogMDtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbn1cblxuLm1haWwtYm94IC5zbS1zaWRlIHtcbiAgYmFja2dyb3VuZDogbm9uZSByZXBlYXQgc2Nyb2xsIDAgMCAjZjJmMmYyO1xuICBib3JkZXItcmFkaXVzOiA0cHggMCAwIDRweDtcbiAgd2lkdGg6IDIwJTtcbn1cblxuLm1haWwtYm94IC5sZy1zaWRlIHtcbiAgYmFja2dyb3VuZDogbm9uZSByZXBlYXQgc2Nyb2xsIDAgMCAjZmZmO1xuICBib3JkZXItcmFkaXVzOiAwIDRweCA0cHggMDtcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRyYW5zaXRpb246IGVhc2UgMC41cyBhbGwgMHM7XG59XG4ubWFpbC1ib3ggLmxnLXNpZGUgLnRhYmxlLWluYm94IC50ciB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBoZWlnaHQ6IDcwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NjYztcbn1cbi5tYWlsLWJveCAubGctc2lkZSAudGFibGUtaW5ib3ggLnRyIC50ZCB7XG4gIGZsb2F0OiBsZWZ0O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZzogMTJweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCU7XG59XG4ubWFpbC1ib3ggLmxnLXNpZGUgLnRhYmxlLWluYm94IC50ciAudGQgZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuLm1haWwtYm94IC5sZy1zaWRlIC50YWJsZS1pbmJveCAudHIgLnRkIGRpdiBoMyB7XG4gIG92ZXJmbG93OiBoaWRkZW4gIWltcG9ydGFudDtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXMgIWltcG9ydGFudDtcbn1cbi5tYWlsLWJveCAubGctc2lkZSAudGFibGUtaW5ib3ggLnRyIC50ZDpmaXJzdC1jaGlsZCB7XG4gIHdpZHRoOiAzNXB4O1xufVxuLm1haWwtYm94IC5sZy1zaWRlIC50YWJsZS1pbmJveCAudHIgLnRkOm50aC1jaGlsZCgzKSB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAzMTBweCk7XG59XG4ubWFpbC1ib3ggLmxnLXNpZGUgLnRhYmxlLWluYm94IC50ciAudGQ6bnRoLWNoaWxkKDQpIHtcbiAgd2lkdGg6IDEyNXB4O1xufVxuLm1haWwtYm94IC5sZy1zaWRlIC50YWJsZS1pbmJveCAudHI6bGFzdC1jaGlsZCB7XG4gIGJvcmRlci1ib3R0b206IHVuc2V0O1xufVxuLm1haWwtYm94IC5lbWFpbC1jb250ZW50IHtcbiAgd2lkdGg6IDBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0cmFuc2l0aW9uOiBlYXNlIDFzIGFsbCAwcztcbiAgbWFyZ2luLWxlZnQ6IDAuNXM7XG4gIHBhZGRpbmctbGVmdDogMzVweDtcbiAgbWFyZ2luLXRvcDogMjFweDtcbiAgYm9yZGVyLWxlZnQ6IDJweCBzb2xpZCBibHVlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmOGZhO1xuICBjb2xvcjogIzM3MzczODtcbn1cblxuLyogd2hlbiB0aGUgYm94IGlzIG9wZW5lZCAqL1xuLm1haWwtYm94LnRydWUgLmxnLXNpZGUge1xuICB3aWR0aDogMzUlO1xufVxuLm1haWwtYm94LnRydWUgLmxnLXNpZGUgLnRhYmxlLWluYm94IC50ciAudGQ6Zmlyc3QtY2hpbGQge1xuICB3aWR0aDogMzVweDtcbn1cbi5tYWlsLWJveC50cnVlIC5sZy1zaWRlIC50YWJsZS1pbmJveCAudHIgLnRkOm50aC1jaGlsZCgyKSB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAxMTVweCk7XG59XG4ubWFpbC1ib3gudHJ1ZSAubGctc2lkZSAudGFibGUtaW5ib3ggLnRyIC50ZDpudGgtY2hpbGQoMykge1xuICBkaXNwbGF5OiBub25lO1xufVxuLm1haWwtYm94LnRydWUgLmxnLXNpZGUgLnRhYmxlLWluYm94IC50ciAudGQ6bnRoLWNoaWxkKDQpIHtcbiAgd2lkdGg6IDgwcHg7XG59XG4ubWFpbC1ib3gudHJ1ZSAuZW1haWwtY29udGVudCB7XG4gIHdpZHRoOiA5MCU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLm1haWwtYm94IC5zbS1zaWRlIC51c2VyLWhlYWQge1xuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICMwMGE4YjM7XG4gIGJvcmRlci1yYWRpdXM6IDRweCAwIDA7XG4gIGNvbG9yOiAjZmZmO1xuICBtaW4taGVpZ2h0OiA4MHB4O1xuICBwYWRkaW5nOiAxMHB4O1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4udXNlci1oZWFkIC5pbmJveC1hdmF0YXIge1xuICBmbG9hdDogbGVmdDtcbiAgd2lkdGg6IDY1cHg7XG59XG5cbi51c2VyLWhlYWQgLmluYm94LWF2YXRhciBpbWcge1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG59XG5cbi51c2VyLWhlYWQgLnVzZXItbmFtZSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAwIDAgMCAxMHB4O1xufVxuXG4udXNlci1oZWFkIC51c2VyLW5hbWUgaDUge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi51c2VyLWhlYWQgLnVzZXItbmFtZSBoNSBhIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi51c2VyLWhlYWQgLnVzZXItbmFtZSBzcGFuIGEge1xuICBjb2xvcjogIzg3ZTJlNztcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG5hLm1haWwtZHJvcGRvd24ge1xuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICM4MGQzZDk7XG4gIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgY29sb3I6ICMwMWE3YjM7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgcGFkZGluZzogM3B4IDVweDtcbn1cblxuLmluYm94LWJvZHkge1xuICBwYWRkaW5nOiAyMHB4O1xuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xufVxuXG4uYnRuLWNvbXBvc2Uge1xuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICNmZjZjNjA7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiAxMnB4IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5idG4tY29tcG9zZTpob3ZlciB7XG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgI2Y1Njc1YztcbiAgY29sb3I6ICNmZmY7XG59XG5cbnVsLmluYm94LW5hdiB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmluYm94LWRpdmlkZXIge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q1ZDhkZjtcbn1cblxudWwuaW5ib3gtbmF2IGxpIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBsaW5lLWhlaWdodDogNDVweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbnVsLmluYm94LW5hdiBsaSBhIHtcbiAgY29sb3I6ICM2YTZhNmE7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbGluZS1oZWlnaHQ6IDQ1cHg7XG4gIHBhZGRpbmc6IDAgMjBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbnVsLmluYm94LW5hdiBsaSBhOmhvdmVyLFxudWwuaW5ib3gtbmF2IGxpLmFjdGl2ZSBhLFxudWwuaW5ib3gtbmF2IGxpIGE6Zm9jdXMge1xuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwIHdoaXRlO1xuICBjb2xvcjogIzZhNmE2YTtcbn1cblxudWwuaW5ib3gtbmF2IGxpIGEgaSB7XG4gIGNvbG9yOiAjNmE2YTZhO1xuICBmb250LXNpemU6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG59XG5cbnVsLmluYm94LW5hdiBsaSBhIHNwYW4ubGFiZWwge1xuICBtYXJnaW4tdG9wOiAxM3B4O1xufVxuXG51bC5sYWJlbHMtaW5mbyBsaSBoNCB7XG4gIGNvbG9yOiAjNWM1YzVlO1xuICBmb250LXNpemU6IDEzcHg7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxudWwubGFiZWxzLWluZm8gbGkge1xuICBtYXJnaW46IDA7XG59XG5cbnVsLmxhYmVscy1pbmZvIGxpIGEge1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBjb2xvcjogIzZhNmE2YTtcbn1cblxudWwubGFiZWxzLWluZm8gbGkgYTpob3ZlcixcbnVsLmxhYmVscy1pbmZvIGxpIGE6Zm9jdXMge1xuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICNkNWQ3ZGU7XG4gIGNvbG9yOiAjNmE2YTZhO1xufVxuXG51bC5sYWJlbHMtaW5mbyBsaSBhIGkge1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufVxuXG4ubmF2Lm5hdi1waWxscy5uYXYtc3RhY2tlZC5sYWJlbHMtaW5mbyBwIHtcbiAgY29sb3I6ICM5ZDlmOWU7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgcGFkZGluZzogMCAyMnB4O1xufVxuXG4uaW5ib3gtaGVhZCB7XG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgIzQxY2FjMDtcbiAgYm9yZGVyLXJhZGl1czogMCA0cHggMCAwO1xuICBjb2xvcjogI2ZmZjtcbiAgbWluLWhlaWdodDogODBweDtcbiAgcGFkZGluZzogMjBweDtcbn1cblxuLmluYm94LWhlYWQgaDMge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZy10b3A6IDZweDtcbn1cblxuLmluYm94LWhlYWQgLnNyLWlucHV0IHtcbiAgYm9yZGVyOiBtZWRpdW0gbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogNHB4IDAgMCA0cHg7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIGNvbG9yOiAjOGE4YThhO1xuICBmbG9hdDogbGVmdDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDEwcHg7XG59XG5cbi5pbmJveC1oZWFkIC5zci1idG4ge1xuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICMwMGE2YjI7XG4gIGJvcmRlcjogbWVkaXVtIG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDAgNHB4IDRweCAwO1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDIwcHg7XG59XG5cbi50YWJsZS1pbmJveCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkM2QzZDM7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHdpZHRoOiA2MCU7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi50YWJsZS1pbmJveCAudHIgLnRkOmhvdmVyIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4udGFibGUtaW5ib3ggLnRyIC50ZCAuZmEtc3Rhci5pbmJveC1zdGFydGVkLFxuLnRhYmxlLWluYm94IC50ciAudGQgLmZhLXN0YXI6aG92ZXIge1xuICBjb2xvcjogI2Y3OGEwOTtcbn1cblxuLnRhYmxlLWluYm94IC50ciAudGQgLmZhLXN0YXIge1xuICBjb2xvcjogI2Q1ZDVkNTtcbn1cblxuLnRhYmxlLWluYm94IC50ci51bnJlYWQgLnRkIHtcbiAgYmFja2dyb3VuZDogbm9uZSByZXBlYXQgc2Nyb2xsIDAgMCAjZWFlYWVhO1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG51bC5pbmJveC1wYWdpbmF0aW9uIHtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG51bC5pbmJveC1wYWdpbmF0aW9uIGxpIHtcbiAgZmxvYXQ6IGxlZnQ7XG59XG5cbi5tYWlsLW9wdGlvbiB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYWlsLW9wdGlvbiAuY2hrLWFsbCxcbi5tYWlsLW9wdGlvbiAuYnRuLWdyb3VwIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5tYWlsLW9wdGlvbiAuY2hrLWFsbCxcbi5tYWlsLW9wdGlvbiAuYnRuLWdyb3VwIGEuYnRuIHtcbiAgYmFja2dyb3VuZDogbm9uZSByZXBlYXQgc2Nyb2xsIDAgMCAjZmNmY2ZjO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTdlN2U3O1xuICBib3JkZXItcmFkaXVzOiAzcHggIWltcG9ydGFudDtcbiAgY29sb3I6ICNhZmFmYWY7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZzogNXB4IDEwcHg7XG59XG5cbi5pbmJveC1wYWdpbmF0aW9uIGEubnAtYnRuIHtcbiAgYmFja2dyb3VuZDogbm9uZSByZXBlYXQgc2Nyb2xsIDAgMCAjZmNmY2ZjO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTdlN2U3O1xuICBib3JkZXItcmFkaXVzOiAzcHggIWltcG9ydGFudDtcbiAgY29sb3I6ICNhZmFmYWY7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZzogNXB4IDE1cHg7XG59XG5cbi5tYWlsLW9wdGlvbiAuY2hrLWFsbCBpbnB1dFt0eXBlPWNoZWNrYm94XSB7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5cbi5tYWlsLW9wdGlvbiAuYnRuLWdyb3VwIGEuYWxsIHtcbiAgYm9yZGVyOiBtZWRpdW0gbm9uZTtcbiAgcGFkZGluZzogMDtcbn1cblxuLmluYm94LXBhZ2luYXRpb24gYS5ucC1idG4ge1xuICBtYXJnaW4tbGVmdDogNXB4O1xufVxuXG4uaW5ib3gtcGFnaW5hdGlvbiBsaSBzcGFuIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgbWFyZ2luLXRvcDogN3B4O1xufVxuXG4uZmlsZWlucHV0LWJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgI2VlZWVlZTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2U2ZTZlNjtcbn1cblxuLmluYm94LWJvZHkgLm1vZGFsIC5tb2RhbC1ib2R5IGlucHV0LFxuLmluYm94LWJvZHkgLm1vZGFsIC5tb2RhbC1ib2R5IHRleHRhcmVhIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2U2ZTZlNjtcbiAgYm94LXNoYWRvdzogbm9uZTtcbn1cblxuLmJ0bi1zZW5kLFxuLmJ0bi1zZW5kOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogbm9uZSByZXBlYXQgc2Nyb2xsIDAgMCAjMDBhOGIzO1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLmJ0bi1zZW5kOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogbm9uZSByZXBlYXQgc2Nyb2xsIDAgMCAjMDA5ZGE3O1xufVxuXG4ubW9kYWwtaGVhZGVyIGg0Lm1vZGFsLXRpdGxlIHtcbiAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG59XG5cbi5tb2RhbC1ib2R5IGxhYmVsIHtcbiAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG59XG5cbi5oZWFkaW5nLWluYm94IGg0IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZGQ7XG4gIGNvbG9yOiAjNDQ0O1xuICBmb250LXNpemU6IDE4cHg7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuXG4uc2VuZGVyLWluZm8ge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4uc2VuZGVyLWluZm8gaW1nIHtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMzBweDtcbn1cblxuLnNlbmRlci1kcm9wZG93biB7XG4gIGJhY2tncm91bmQ6IG5vbmUgcmVwZWF0IHNjcm9sbCAwIDAgI2VhZWFlYTtcbiAgY29sb3I6ICM3Nzc7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgcGFkZGluZzogMCAzcHg7XG59XG5cbi52aWV3LW1haWwgYSB7XG4gIGNvbG9yOiAjZmY2YzYwO1xufVxuXG4uYXR0YWNobWVudC1tYWlsIHtcbiAgbWFyZ2luLXRvcDogMzBweDtcbn1cblxuLmF0dGFjaG1lbnQtbWFpbCB1bCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5hdHRhY2htZW50LW1haWwgdWwgbGkge1xuICBmbG9hdDogbGVmdDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICB3aWR0aDogMTUwcHg7XG59XG5cbi5hdHRhY2htZW50LW1haWwgdWwgbGkgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5hdHRhY2htZW50LW1haWwgdWwgbGkgc3BhbiB7XG4gIGZsb2F0OiByaWdodDtcbn1cblxuLmF0dGFjaG1lbnQtbWFpbCAuZmlsZS1uYW1lIHtcbiAgZmxvYXQ6IGxlZnQ7XG59XG5cbi5hdHRhY2htZW50LW1haWwgLmxpbmtzIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmZpbGVpbnB1dC1idXR0b24ge1xuICBmbG9hdDogbGVmdDtcbiAgbWFyZ2luLXJpZ2h0OiA0cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmZpbGVpbnB1dC1idXR0b24gaW5wdXQge1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGRpcmVjdGlvbjogbHRyO1xuICBmb250LXNpemU6IDIzcHg7XG4gIG1hcmdpbjogMDtcbiAgb3BhY2l0eTogMDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMDtcbiAgdG9wOiAwO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtMzAwcHgsIDBweCkgc2NhbGUoNCk7XG59XG5cbi5maWxldXBsb2FkLWJ1dHRvbmJhciAuYnRuLFxuLmZpbGV1cGxvYWQtYnV0dG9uYmFyIC50b2dnbGUge1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbi5maWxlcyAucHJvZ3Jlc3Mge1xuICB3aWR0aDogMjAwcHg7XG59XG5cbi5maWxldXBsb2FkLXByb2Nlc3NpbmcgLmZpbGV1cGxvYWQtbG9hZGluZyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4qIGh0bWwgLmZpbGVpbnB1dC1idXR0b24ge1xuICBsaW5lLWhlaWdodDogMjRweDtcbiAgbWFyZ2luOiAxcHggLTNweCAwIDA7XG59XG5cbiogKyBodG1sIC5maWxlaW5wdXQtYnV0dG9uIHtcbiAgbWFyZ2luOiAxcHggMCAwO1xuICBwYWRkaW5nOiAycHggMTVweDtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gIC5maWxlcyAuYnRuIHNwYW4ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAuZmlsZXMgLnByZXZpZXcgKiB7XG4gICAgd2lkdGg6IDQwcHg7XG4gIH1cblxuICAuZmlsZXMgLm5hbWUgKiB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiA4MHB4O1xuICAgIHdvcmQtd3JhcDogYnJlYWstd29yZDtcbiAgfVxuXG4gIC5maWxlcyAucHJvZ3Jlc3Mge1xuICAgIHdpZHRoOiAyMHB4O1xuICB9XG5cbiAgLmZpbGVzIC5kZWxldGUge1xuICAgIHdpZHRoOiA2MHB4O1xuICB9XG59XG51bCB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW46IDBweDtcbn1cblxuLmNsYXNzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuLmluYm94IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbiNsb2FkZXIge1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcbiAgb3BhY2l0eTogMTtcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgei1pbmRleDogOTAwMDA7XG59XG5cbiNsb2FkZXIuZmFkZU91dCB7XG4gIG9wYWNpdHk6IDA7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbn1cblxuLnNwaW5uZXIge1xuICB3aWR0aDogNDBweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogY2FsYyg1MCUgLSAyMHB4KTtcbiAgbGVmdDogY2FsYyg1MCUgLSAyMHB4KTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzMzMztcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgLXdlYmtpdC1hbmltYXRpb246IHNrLXNjYWxlb3V0IDFzIGluZmluaXRlIGVhc2UtaW4tb3V0O1xuICBhbmltYXRpb246IHNrLXNjYWxlb3V0IDFzIGluZmluaXRlIGVhc2UtaW4tb3V0O1xufVxuXG4uZW1haWwtYXBwIHtcbiAgbWFyZ2luOiAwO1xufVxuLmVtYWlsLWFwcCAuZW1haWwtd3JhcHBlciB7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiAgb3ZlcmZsb3c6IGF1dG87XG4gIG1pbi1oZWlnaHQ6IDEwMCU7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xufVxuXG4uZGVzY3JpcHRpb24ge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmRlc2NyaXB0aW9uIGgzIHtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG5cbmgzIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbiAhaW1wb3J0YW50O1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcyAhaW1wb3J0YW50O1xufVxuXG4ubWVzc2FnZSB7XG4gIGZvbnQtZmFtaWx5OiAtYXBwbGUtc3lzdGVtLCBCbGlua01hY1N5c3RlbUZvbnQsIFwiU2Vnb2UgVUlcIiwgUm9ib3RvLCBPeHlnZW4sIFVidW50dSwgQ2FudGFyZWxsLCBcIk9wZW4gU2Fuc1wiLCBcIkhlbHZldGljYSBOZXVlXCIsIHNhbnMtc2VyaWY7XG59XG5cbmJ1dHRvbiB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: MerchantPortalInboxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantPortalInboxComponent", function() { return MerchantPortalInboxComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_inbox_inbox_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/inbox/inbox.service */ "./src/app/services/inbox/inbox.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var MerchantPortalInboxComponent = /** @class */ (function () {
    function MerchantPortalInboxComponent(inboxService, router, activatedRoute) {
        this.inboxService = inboxService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.inboxData = [
            {
            // title: 'test',
            // message: 'test',
            // short_description: 'New Order Coming!'
            }
        ];
    }
    MerchantPortalInboxComponent.prototype.ngOnInit = function () {
        // const list =  this.inboxService.getInbox();
        this.firstLoad();
        // this.secondLoad();
    };
    MerchantPortalInboxComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, dataSamples, data, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.inboxData = [];
                        return [4 /*yield*/, this.inboxService.getInbox()];
                    case 1:
                        result = _a.sent();
                        console.log('ISI INBOX', result);
                        dataSamples = [];
                        if (result.result) {
                            data = result.result;
                            data.forEach(function (value) {
                                // value.time = value.updated_date;
                                // console.log("result time", value.time)
                                // value.description = value.cta_value = 'New Order Coming!';
                                var html = value.title;
                                var div = document.createElement('div');
                                div.innerHTML = html;
                                //   console.log("result text", div.innerText)
                                value.title_message = div.innerText;
                                _this.inboxData.push(value);
                            });
                            console.log("testttt", this.inboxData.length);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log('Error', e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // async secondLoad() {
    // 	var html = '<p>Hello, <b>World</b>';
    // 	var div = document.createElement('div');
    // 	div.innerHTML = html;
    // 	alert(div.innerText); // Hello, World
    // }
    MerchantPortalInboxComponent.prototype.goToOrderDetail = function (order_id) {
        console.log('ini IDNya', order_id);
        this.router.navigate(['merchant-portal/order-detail/edit'], { queryParams: { id: order_id } });
    };
    MerchantPortalInboxComponent.prototype.delete = function (id) {
        console.log("button");
    };
    MerchantPortalInboxComponent.prototype.messageDetail = function (inboxData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.hideColumnWhenOpen = true;
                this.selectedData = inboxData;
                this.message = this.selectedData;
                console.log(this.selectedData.reference.order_id);
                // this.detail = await this.inboxService.detailInbox(this.getId);
                this.inboxService.readInbox(inboxData._id);
                this.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    MerchantPortalInboxComponent.prototype.closeData = function () {
        this.hideColumnWhenOpen = false;
    };
    MerchantPortalInboxComponent.ctorParameters = function () { return [
        { type: _services_inbox_inbox_service__WEBPACK_IMPORTED_MODULE_1__["InboxService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
    ]; };
    MerchantPortalInboxComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-portal-inbox',
            template: __webpack_require__(/*! raw-loader!./merchant-portal-inbox.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.component.html"),
            styles: [__webpack_require__(/*! ./merchant-portal-inbox.component.scss */ "./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_inbox_inbox_service__WEBPACK_IMPORTED_MODULE_1__["InboxService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], MerchantPortalInboxComponent);
    return MerchantPortalInboxComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: MerchantPortalInboxModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantPortalInboxModule", function() { return MerchantPortalInboxModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MerchantPortalInboxModule = /** @class */ (function () {
    function MerchantPortalInboxModule() {
    }
    MerchantPortalInboxModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            // declarations: [MerchantPortalInboxComponent],
            imports: [
            // CommonModule,
            // MerchantPortalInboxRoutingModule
            ],
        })
    ], MerchantPortalInboxModule);
    return MerchantPortalInboxModule;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component.scss":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component.scss ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvbWVyY2hhbnQtcG9ydGFsLW9yZGVyaGlzdG9yaWVzLXRhYi9tZXJjaGFudC1wb3J0YWwtb3JkZXJoaXN0b3JpZXMtdGFiLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component.ts":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component.ts ***!
  \***************************************************************************************************************************/
/*! exports provided: MerchantPortalOrderhistoriesTabComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantPortalOrderhistoriesTabComponent", function() { return MerchantPortalOrderhistoriesTabComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var MerchantPortalOrderhistoriesTabComponent = /** @class */ (function () {
    // topBar: topBarMenu[] = [
    //   { label: "All Order", routerLink: '/administrator/order-history-summary', active: true },
    //   { label: "Belum di proses", routerLink: '/administrator/order-amount-history-summary' },
    //   { label: "Packaging", routerLink: '/administrator/cart-abandonment-summary' },
    //   { label: "Proses Pengiriman", routerLink: '/administrator/cart-abandonment-summary' },
    //   { label: "Delivered", routerLink: '/administrator/cart-abandonment-summary' },
    //   { label: "Cancelled", routerLink: '/administrator/cart-abandonment-summary' },
    // ]
    function MerchantPortalOrderhistoriesTabComponent(orderHistoryService, route, router) {
        this.orderHistoryService = orderHistoryService;
        this.route = route;
        this.router = router;
        this.data = [];
    }
    MerchantPortalOrderhistoriesTabComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MerchantPortalOrderhistoriesTabComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    MerchantPortalOrderhistoriesTabComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    MerchantPortalOrderhistoriesTabComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-portal-orderhistories-tab',
            template: __webpack_require__(/*! raw-loader!./merchant-portal-orderhistories-tab.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component.html"),
            styles: [__webpack_require__(/*! ./merchant-portal-orderhistories-tab.component.scss */ "./src/app/layout/merchant-portal/merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], MerchantPortalOrderhistoriesTabComponent);
    return MerchantPortalOrderhistoriesTabComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: MerchantPortalRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantPortalRoutingModule", function() { return MerchantPortalRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _merchant_portal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./merchant-portal.component */ "./src/app/layout/merchant-portal/merchant-portal.component.ts");
/* harmony import */ var _merchant_route_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./merchant-route-path */ "./src/app/layout/merchant-portal/merchant-route-path.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        component: _merchant_portal_component__WEBPACK_IMPORTED_MODULE_2__["MerchantPortalComponent"],
        children: _merchant_route_path__WEBPACK_IMPORTED_MODULE_3__["path"].slice(),
    }
];
var MerchantPortalRoutingModule = /** @class */ (function () {
    function MerchantPortalRoutingModule() {
    }
    MerchantPortalRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes),
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], MerchantPortalRoutingModule);
    return MerchantPortalRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#merchant-portal {\n  position: absolute;\n  z-index: 1;\n  left: 0px;\n  top: 0px;\n  background-color: #F8F8F8;\n  width: 100%;\n  height: 100%;\n  overflow: hidden;\n}\n\n.dev-mode-merchant-portal {\n  margin-top: 23px;\n  height: calc(100% - 23px);\n}\n\n.dev-mode-merchant-portal #merchant-portal {\n  position: absolute;\n  z-index: 1;\n  width: 100%;\n  left: 0px;\n  top: 25px;\n}\n\n.main-container {\n  position: relative;\n  margin-left: 100px;\n  top: 70px;\n  height: calc(85vh + 2px) !important;\n  width: calc(100vw - 100px);\n  overflow: auto;\n  background: #fcfcfc;\n  overflow-x: hidden;\n}\n\n@media (max-width: 620px) {\n  .main-container {\n    position: relative;\n    width: calc(100vw - 61px);\n    margin-left: 50px;\n  }\n}\n\n@media print {\n  .main-container {\n    position: relative;\n    top: 0;\n    margin: 0px;\n    height: 100%;\n    width: 100%;\n    overflow: hidden;\n    background: #fcfcfc;\n    overflow-x: hidden;\n  }\n\n  .dev-mode-merchant-portal {\n    width: 100%;\n    height: 100%;\n    margin: 0px;\n  }\n\n  #merchant-portal {\n    position: relative;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1lcmNoYW50LXBvcnRhbFxcbWVyY2hhbnQtcG9ydGFsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LXBvcnRhbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FERUE7RUFDSSxnQkFBQTtFQUNBLHlCQUFBO0FDQ0o7O0FEQ0k7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBRUEsU0FBQTtFQUNBLFNBQUE7QUNBUjs7QURLQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsbUNBQUE7RUFDQSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDRko7O0FES0E7RUFDSTtJQUNJLGtCQUFBO0lBQ0EseUJBQUE7SUFDQSxpQkFBQTtFQ0ZOO0FBQ0Y7O0FESUE7RUFDSTtJQUNBLGtCQUFBO0lBQ0EsTUFBQTtJQUNBLFdBQUE7SUFDQSxZQUFBO0lBQ0EsV0FBQTtJQUNBLGdCQUFBO0lBQ0EsbUJBQUE7SUFDQSxrQkFBQTtFQ0ZGOztFREtFO0lBRUksV0FBQTtJQUNBLFlBQUE7SUFDQSxXQUFBO0VDSE47O0VETUU7SUFDSSxrQkFBQTtFQ0hOO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LXBvcnRhbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNtZXJjaGFudC1wb3J0YWwge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIHRvcDogMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0Y4RjhGODtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuLmRldi1tb2RlLW1lcmNoYW50LXBvcnRhbCB7XHJcbiAgICBtYXJnaW4tdG9wOiAyM3B4O1xyXG4gICAgaGVpZ2h0OiBjYWxjKDEwMCUgLSAyM3B4KTtcclxuXHJcbiAgICAjbWVyY2hhbnQtcG9ydGFsIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAvLyBoZWlnaHQ6IGNhbGMoMTAwdmggLSAyNXB4KTtcclxuICAgICAgICBsZWZ0OiAwcHg7XHJcbiAgICAgICAgdG9wOiAyNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4vLyBNYWluIENvbnRhaW5lclxyXG4ubWFpbi1jb250YWluZXIge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwMHB4O1xyXG4gICAgdG9wOiA3MHB4O1xyXG4gICAgaGVpZ2h0OiBjYWxjKDg1dmggKyAycHgpICFpbXBvcnRhbnQ7XHJcbiAgICB3aWR0aDogY2FsYygxMDB2dyAtIDEwMHB4KTtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgYmFja2dyb3VuZDogI2ZjZmNmYztcclxuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDYyMHB4KXtcclxuICAgIC5tYWluLWNvbnRhaW5lciB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHdpZHRoOiBjYWxjKDEwMHZ3IC0gNjFweCk7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIHByaW50IHtcclxuICAgIC5tYWluLWNvbnRhaW5lcntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMDtcclxuICAgIG1hcmdpbjogMHB4O1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYmFja2dyb3VuZDogI2ZjZmNmYztcclxuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuICAgIH1cclxuXHJcbiAgICAuZGV2LW1vZGUtbWVyY2hhbnQtcG9ydGFse1xyXG4gICAgICAgIC8vIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgfVxyXG5cclxuICAgICNtZXJjaGFudC1wb3J0YWwge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIH1cclxufSIsIiNtZXJjaGFudC1wb3J0YWwge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDE7XG4gIGxlZnQ6IDBweDtcbiAgdG9wOiAwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGOEY4Rjg7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5kZXYtbW9kZS1tZXJjaGFudC1wb3J0YWwge1xuICBtYXJnaW4tdG9wOiAyM3B4O1xuICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDIzcHgpO1xufVxuLmRldi1tb2RlLW1lcmNoYW50LXBvcnRhbCAjbWVyY2hhbnQtcG9ydGFsIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAxO1xuICB3aWR0aDogMTAwJTtcbiAgbGVmdDogMHB4O1xuICB0b3A6IDI1cHg7XG59XG5cbi5tYWluLWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLWxlZnQ6IDEwMHB4O1xuICB0b3A6IDcwcHg7XG4gIGhlaWdodDogY2FsYyg4NXZoICsgMnB4KSAhaW1wb3J0YW50O1xuICB3aWR0aDogY2FsYygxMDB2dyAtIDEwMHB4KTtcbiAgb3ZlcmZsb3c6IGF1dG87XG4gIGJhY2tncm91bmQ6ICNmY2ZjZmM7XG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDYyMHB4KSB7XG4gIC5tYWluLWNvbnRhaW5lciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiBjYWxjKDEwMHZ3IC0gNjFweCk7XG4gICAgbWFyZ2luLWxlZnQ6IDUwcHg7XG4gIH1cbn1cbkBtZWRpYSBwcmludCB7XG4gIC5tYWluLWNvbnRhaW5lciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogMDtcbiAgICBtYXJnaW46IDBweDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBiYWNrZ3JvdW5kOiAjZmNmY2ZjO1xuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgfVxuXG4gIC5kZXYtbW9kZS1tZXJjaGFudC1wb3J0YWwge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBtYXJnaW46IDBweDtcbiAgfVxuXG4gICNtZXJjaGFudC1wb3J0YWwge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal.component.ts ***!
  \*********************************************************************/
/*! exports provided: MerchantPortalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantPortalComponent", function() { return MerchantPortalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _animation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./animation */ "./src/app/layout/merchant-portal/animation.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MerchantPortalComponent = /** @class */ (function () {
    function MerchantPortalComponent() {
        this.devclass = 'dev-mode-merchant-portal';
        this.currentState = 'initial';
        this.items = [];
    }
    MerchantPortalComponent.prototype.addItem = function (val) {
        this.items.push(val);
    };
    MerchantPortalComponent.prototype.removeItem = function (n, items) {
        this.items.splice(n, 1);
    };
    MerchantPortalComponent.prototype.changeState = function () {
        this.currentState = this.currentState === 'initial' ? 'final' : 'initial';
    };
    MerchantPortalComponent.prototype.ngOnInit = function () {
    };
    MerchantPortalComponent.prototype.ngDoCheck = function () {
        // console.log("ITS CHANGED");
        if (localStorage.getItem('devmode') == 'active') {
            this.devclass = 'dev-mode-merchant-portal';
        }
        else {
            this.devclass = 'prod-mode-merchant-portal';
        }
    };
    MerchantPortalComponent.prototype.prepareRoute = function (outlet) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
    };
    MerchantPortalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-portal',
            template: __webpack_require__(/*! raw-loader!./merchant-portal.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-portal.component.html"),
            animations: [_animation__WEBPACK_IMPORTED_MODULE_1__["slideInAnimation"]],
            styles: [__webpack_require__(/*! ./merchant-portal.component.scss */ "./src/app/layout/merchant-portal/merchant-portal.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MerchantPortalComponent);
    return MerchantPortalComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-portal.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-portal.module.ts ***!
  \******************************************************************/
/*! exports provided: MerchantPortalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantPortalModule", function() { return MerchantPortalModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _merchant_portal_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./merchant-portal-routing.module */ "./src/app/layout/merchant-portal/merchant-portal-routing.module.ts");
/* harmony import */ var _merchant_homepage_merchant_homepage_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./merchant-homepage/merchant-homepage.component */ "./src/app/layout/merchant-portal/merchant-homepage/merchant-homepage.component.ts");
/* harmony import */ var _merchant_portal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./merchant-portal.component */ "./src/app/layout/merchant-portal/merchant-portal.component.ts");
/* harmony import */ var _merchant_portal_app_header_merchant_portal_app_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./merchant-portal-app-header/merchant-portal-app-header.component */ "./src/app/layout/merchant-portal/merchant-portal-app-header/merchant-portal-app-header.component.ts");
/* harmony import */ var _merchant_portal_app_sidebar_merchant_portal_app_sidebar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./merchant-portal-app-sidebar/merchant-portal-app-sidebar.component */ "./src/app/layout/merchant-portal/merchant-portal-app-sidebar/merchant-portal-app-sidebar.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../modules/bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _separated_modules_merchant_products_merchant_products_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./separated-modules/merchant-products/merchant-products.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/merchant-products.component.ts");
/* harmony import */ var _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ckeditor/ckeditor5-angular */ "./node_modules/@ckeditor/ckeditor5-angular/fesm5/ckeditor-ckeditor5-angular.js");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/layout/merchant-portal/profile/profile.component.ts");
/* harmony import */ var _order_histories_order_histories_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./order-histories/order-histories.component */ "./src/app/layout/merchant-portal/order-histories/order-histories.component.ts");
/* harmony import */ var _component_libs_photo_profile_photo_profile_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../component-libs/photo-profile/photo-profile.component */ "./src/app/component-libs/photo-profile/photo-profile.component.ts");
/* harmony import */ var _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./order-detail/order-detail.component */ "./src/app/layout/merchant-portal/order-detail/order-detail.component.ts");
/* harmony import */ var _statistics_statistics_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./statistics/statistics.component */ "./src/app/layout/merchant-portal/statistics/statistics.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _modules_admin_portal_admin_order_history_order_history_summary_orderhistorysummary_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.module */ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.module.ts");
/* harmony import */ var _modules_orderhistoryallhistory_orderhistoryallhistory_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../modules/orderhistoryallhistory/orderhistoryallhistory.module */ "./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _merchant_sales_order_merchant_sales_order_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./merchant-sales-order/merchant-sales-order.component */ "./src/app/layout/merchant-portal/merchant-sales-order/merchant-sales-order.component.ts");
/* harmony import */ var _merchant_portal_orderhistories_tab_merchant_portal_orderhistories_tab_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component */ "./src/app/layout/merchant-portal/merchant-portal-orderhistories-tab/merchant-portal-orderhistories-tab.component.ts");
/* harmony import */ var _modules_evouchersalesreport_evoucher_sales_report_evoucher_sales_report_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.module */ "./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.module.ts");
/* harmony import */ var _shared_guard_portal_guard__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../../shared/guard/portal.guard */ "./src/app/shared/guard/portal.guard.ts");
/* harmony import */ var _separated_modules_merchant_products_products_module__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./separated-modules/merchant-products/products.module */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products.module.ts");
/* harmony import */ var _modules_product_evoucher_evoucher_module__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../modules/product.evoucher/evoucher.module */ "./src/app/layout/modules/product.evoucher/evoucher.module.ts");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _merchant_portal_banner_merchant_portal_banner_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./merchant-portal-banner/merchant-portal-banner.component */ "./src/app/layout/merchant-portal/merchant-portal-banner/merchant-portal-banner.component.ts");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _order_histories_shipping_label_shipping_label_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./order-histories/shipping-label/shipping-label.component */ "./src/app/layout/merchant-portal/order-histories/shipping-label/shipping-label.component.ts");
/* harmony import */ var ngx_barcode__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ngx-barcode */ "./node_modules/ngx-barcode/index.js");
/* harmony import */ var _merchant_portal_inbox_merchant_portal_inbox_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./merchant-portal-inbox/merchant-portal-inbox.component */ "./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.component.ts");
/* harmony import */ var _merchant_portal_inbox_merchant_portal_inbox_module__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./merchant-portal-inbox/merchant-portal-inbox.module */ "./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.module.ts");
/* harmony import */ var _merchant_my_escrow_merchant_my_escrow_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./merchant-my-escrow/merchant-my-escrow.component */ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-my-escrow.component.ts");
/* harmony import */ var _merchant_my_escrow_merchant_withdrawal_merchant_withdrawal_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component */ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component.ts");
/* harmony import */ var _merchant_my_escrow_merchant_escrow_detail_merchant_escrow_detail_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component */ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ng_otp_input__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ng-otp-input */ "./node_modules/ng-otp-input/fesm5/ng-otp-input.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/fesm2015/agm-core.js");
/* harmony import */ var _upgrade_genuine_upgrade_genuine_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./upgrade-genuine/upgrade-genuine.component */ "./src/app/layout/merchant-portal/upgrade-genuine/upgrade-genuine.component.ts");
/* harmony import */ var _merchant_portal_detail_inbox_merchant_portal_detail_inbox_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./merchant-portal-detail-inbox/merchant-portal-detail-inbox.component */ "./src/app/layout/merchant-portal/merchant-portal-detail-inbox/merchant-portal-detail-inbox.component.ts");
/* harmony import */ var _modules_report_report_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ../modules/report/report.component */ "./src/app/layout/modules/report/report.component.ts");
/* harmony import */ var _modules_report_detail_report_detail_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ../modules/report/detail/report.detail.component */ "./src/app/layout/modules/report/detail/report.detail.component.ts");
/* harmony import */ var _modules_report_edit_report_edit_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ../modules/report/edit/report.edit.component */ "./src/app/layout/modules/report/edit/report.edit.component.ts");
/* harmony import */ var _modules_report_detail_report_excel_detail_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ../modules/report/detail/report.excel.detail.component */ "./src/app/layout/modules/report/detail/report.excel.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















































var MerchantPortalModule = /** @class */ (function () {
    function MerchantPortalModule() {
    }
    MerchantPortalModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _merchant_homepage_merchant_homepage_component__WEBPACK_IMPORTED_MODULE_3__["MerchantHomepageComponent"],
                _merchant_portal_component__WEBPACK_IMPORTED_MODULE_4__["MerchantPortalComponent"],
                _merchant_portal_app_header_merchant_portal_app_header_component__WEBPACK_IMPORTED_MODULE_5__["MerchantPortalAppHeaderComponent"],
                _merchant_portal_app_sidebar_merchant_portal_app_sidebar_component__WEBPACK_IMPORTED_MODULE_6__["MerchantPortalAppSidebarComponent"],
                _separated_modules_merchant_products_merchant_products_component__WEBPACK_IMPORTED_MODULE_10__["MerchantProductsComponent"],
                _profile_profile_component__WEBPACK_IMPORTED_MODULE_12__["ProfileComponent"],
                _order_histories_order_histories_component__WEBPACK_IMPORTED_MODULE_13__["OrderHistoriesComponent"],
                _order_histories_shipping_label_shipping_label_component__WEBPACK_IMPORTED_MODULE_30__["ShippingLabelComponent"],
                _component_libs_photo_profile_photo_profile_component__WEBPACK_IMPORTED_MODULE_14__["PhotoProfileComponent"],
                _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_15__["OrderDetailComponent"],
                _statistics_statistics_component__WEBPACK_IMPORTED_MODULE_16__["StatisticsComponent"],
                _merchant_sales_order_merchant_sales_order_component__WEBPACK_IMPORTED_MODULE_21__["MerchantSalesOrderComponent"],
                _merchant_portal_orderhistories_tab_merchant_portal_orderhistories_tab_component__WEBPACK_IMPORTED_MODULE_22__["MerchantPortalOrderhistoriesTabComponent"],
                _merchant_portal_banner_merchant_portal_banner_component__WEBPACK_IMPORTED_MODULE_28__["MerchantPortalBannerComponent"],
                _merchant_portal_inbox_merchant_portal_inbox_component__WEBPACK_IMPORTED_MODULE_32__["MerchantPortalInboxComponent"],
                _merchant_portal_detail_inbox_merchant_portal_detail_inbox_component__WEBPACK_IMPORTED_MODULE_41__["MerchantPortalDetailInboxComponent"],
                _merchant_my_escrow_merchant_my_escrow_component__WEBPACK_IMPORTED_MODULE_34__["MerchantMyEscrowComponent"],
                _merchant_my_escrow_merchant_withdrawal_merchant_withdrawal_component__WEBPACK_IMPORTED_MODULE_35__["MerchantWithdrawalComponent"],
                _merchant_my_escrow_merchant_escrow_detail_merchant_escrow_detail_component__WEBPACK_IMPORTED_MODULE_36__["MerchantEscrowDetailComponent"],
                _upgrade_genuine_upgrade_genuine_component__WEBPACK_IMPORTED_MODULE_40__["UpgradeGenuineComponent"],
                _modules_report_report_component__WEBPACK_IMPORTED_MODULE_42__["ReportComponent"],
                _modules_report_edit_report_edit_component__WEBPACK_IMPORTED_MODULE_44__["ReportEditComponent"],
                _modules_report_detail_report_detail_component__WEBPACK_IMPORTED_MODULE_43__["ReportDetailComponent"],
                _modules_report_detail_report_excel_detail_component__WEBPACK_IMPORTED_MODULE_45__["ReportExcelDetailComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _merchant_portal_routing_module__WEBPACK_IMPORTED_MODULE_2__["MerchantPortalRoutingModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_20__["NgbDatepickerModule"],
                _modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_9__["FormBuilderTableModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_11__["CKEditorModule"],
                _modules_admin_portal_admin_order_history_order_history_summary_orderhistorysummary_module__WEBPACK_IMPORTED_MODULE_18__["OrderHistorySummaryModule"],
                _separated_modules_merchant_products_products_module__WEBPACK_IMPORTED_MODULE_25__["ProductsModule"],
                _modules_orderhistoryallhistory_orderhistoryallhistory_module__WEBPACK_IMPORTED_MODULE_19__["OrderhistoryallhistoryModule"],
                _shared__WEBPACK_IMPORTED_MODULE_17__["PageHeaderModule"],
                _modules_evouchersalesreport_evoucher_sales_report_evoucher_sales_report_module__WEBPACK_IMPORTED_MODULE_23__["EvoucherSalesReportModule"],
                _modules_product_evoucher_evoucher_module__WEBPACK_IMPORTED_MODULE_26__["EvoucherModule"],
                _merchant_portal_inbox_merchant_portal_inbox_module__WEBPACK_IMPORTED_MODULE_33__["MerchantPortalInboxModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_20__["NgbModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_27__["MatCheckboxModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_29__["MatTabsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                ngx_barcode__WEBPACK_IMPORTED_MODULE_31__["NgxBarcodeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_37__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_37__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_37__["MatListModule"],
                ng_otp_input__WEBPACK_IMPORTED_MODULE_38__["NgOtpInputModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_39__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyChHtMJJ6RE3Bg-SFeTKQvOsks7MzGQ5VA',
                    libraries: ["places"]
                })
                // EvoucherSalesReportModule
            ],
            providers: [
                _shared_guard_portal_guard__WEBPACK_IMPORTED_MODULE_24__["PortalGuard"]
            ]
        })
    ], MerchantPortalModule);
    return MerchantPortalModule;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-route-path.ts":
/*!***************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-route-path.ts ***!
  \***************************************************************/
/*! exports provided: path */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "path", function() { return path; });
/* harmony import */ var _separated_modules_merchant_products_products_merchant_add_new_product_merchant_add_new_product_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component.ts");
/* harmony import */ var _merchant_homepage_merchant_homepage_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./merchant-homepage/merchant-homepage.component */ "./src/app/layout/merchant-portal/merchant-homepage/merchant-homepage.component.ts");
/* harmony import */ var _separated_modules_merchant_products_merchant_products_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./separated-modules/merchant-products/merchant-products.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/merchant-products.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/layout/merchant-portal/profile/profile.component.ts");
/* harmony import */ var _order_histories_order_histories_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./order-histories/order-histories.component */ "./src/app/layout/merchant-portal/order-histories/order-histories.component.ts");
/* harmony import */ var _separated_modules_merchant_products_products_merchant_edit_product_merchant_edit_product_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component.ts");
/* harmony import */ var _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-detail/order-detail.component */ "./src/app/layout/merchant-portal/order-detail/order-detail.component.ts");
/* harmony import */ var _statistics_statistics_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./statistics/statistics.component */ "./src/app/layout/merchant-portal/statistics/statistics.component.ts");
/* harmony import */ var _separated_modules_merchant_products_products_evoucher_list_evoucher_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./separated-modules/merchant-products/products/evoucher-list/evoucher-list.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-list.component.ts");
/* harmony import */ var _merchant_sales_order_merchant_sales_order_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./merchant-sales-order/merchant-sales-order.component */ "./src/app/layout/merchant-portal/merchant-sales-order/merchant-sales-order.component.ts");
/* harmony import */ var _separated_modules_merchant_products_products_evoucher_list_evoucher_detail_evoucher_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component.ts");
/* harmony import */ var _merchant_evoucher_sales_report_merchant_evoucher_sales_report_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./merchant-evoucher-sales-report/merchant-evoucher-sales-report.component */ "./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.ts");
/* harmony import */ var _order_histories_shipping_label_shipping_label_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./order-histories/shipping-label/shipping-label.component */ "./src/app/layout/merchant-portal/order-histories/shipping-label/shipping-label.component.ts");
/* harmony import */ var _merchant_portal_inbox_merchant_portal_inbox_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./merchant-portal-inbox/merchant-portal-inbox.component */ "./src/app/layout/merchant-portal/merchant-portal-inbox/merchant-portal-inbox.component.ts");
/* harmony import */ var _merchant_portal_detail_inbox_merchant_portal_detail_inbox_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./merchant-portal-detail-inbox/merchant-portal-detail-inbox.component */ "./src/app/layout/merchant-portal/merchant-portal-detail-inbox/merchant-portal-detail-inbox.component.ts");
/* harmony import */ var _merchant_my_escrow_merchant_my_escrow_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./merchant-my-escrow/merchant-my-escrow.component */ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-my-escrow.component.ts");
/* harmony import */ var _merchant_my_escrow_merchant_withdrawal_merchant_withdrawal_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component */ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component.ts");
/* harmony import */ var _merchant_my_escrow_merchant_escrow_detail_merchant_escrow_detail_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component */ "./src/app/layout/merchant-portal/merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component.ts");
/* harmony import */ var _separated_modules_merchant_products_products_merchant_detail_product_merchant_detail_product_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component.ts");
/* harmony import */ var _upgrade_genuine_upgrade_genuine_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./upgrade-genuine/upgrade-genuine.component */ "./src/app/layout/merchant-portal/upgrade-genuine/upgrade-genuine.component.ts");
/* harmony import */ var _modules_report_report_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../modules/report/report.component */ "./src/app/layout/modules/report/report.component.ts");
/* harmony import */ var _modules_report_detail_report_detail_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../modules/report/detail/report.detail.component */ "./src/app/layout/modules/report/detail/report.detail.component.ts");
/* harmony import */ var _modules_report_edit_report_edit_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../modules/report/edit/report.edit.component */ "./src/app/layout/modules/report/edit/report.edit.component.ts");
/* harmony import */ var _modules_report_detail_report_excel_detail_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../modules/report/detail/report.excel.detail.component */ "./src/app/layout/modules/report/detail/report.excel.detail.component.ts");



// import { ProductDetailComponent }    from './products/product-detail/product-detail.component';





















var path = [
    { path: 'homepage', component: _merchant_homepage_merchant_homepage_component__WEBPACK_IMPORTED_MODULE_1__["MerchantHomepageComponent"], data: { animation: 'homepage' } },
    { path: 'add', component: _separated_modules_merchant_products_products_merchant_add_new_product_merchant_add_new_product_component__WEBPACK_IMPORTED_MODULE_0__["MerchantAddNewProductComponent"], data: { animation: 'addNewProductPage' } },
    { path: 'my-money', component: _merchant_my_escrow_merchant_my_escrow_component__WEBPACK_IMPORTED_MODULE_15__["MerchantMyEscrowComponent"], data: { animation: 'merchantMyEscrow' } },
    { path: 'my-money/detail', component: _merchant_my_escrow_merchant_escrow_detail_merchant_escrow_detail_component__WEBPACK_IMPORTED_MODULE_17__["MerchantEscrowDetailComponent"], data: { animation: 'MerchantEscrowDetailComponent' } },
    { path: 'my-money/withdrawal', component: _merchant_my_escrow_merchant_withdrawal_merchant_withdrawal_component__WEBPACK_IMPORTED_MODULE_16__["MerchantWithdrawalComponent"], data: { animation: 'merchantMyEscrowWithdrawal' } },
    { path: 'product-list/detail', component: _separated_modules_merchant_products_products_merchant_detail_product_merchant_detail_product_component__WEBPACK_IMPORTED_MODULE_18__["MerchantDetailProductComponent"], data: { animation: 'MerchantDetailProductPage' } },
    { path: 'product-list/edit', component: _separated_modules_merchant_products_products_merchant_edit_product_merchant_edit_product_component__WEBPACK_IMPORTED_MODULE_5__["MerchantEditProductComponent"], data: { animation: 'MerchantEditProductPage' } },
    { path: 'product-list/generated-voucher/list', component: _separated_modules_merchant_products_products_evoucher_list_evoucher_list_component__WEBPACK_IMPORTED_MODULE_8__["EvoucherListComponent"], data: { animation: 'EvoucherListComponent' } },
    { path: 'product-list/generated-voucher/list/detail', component: _separated_modules_merchant_products_products_evoucher_list_evoucher_detail_evoucher_detail_component__WEBPACK_IMPORTED_MODULE_10__["EvoucherDetailComponent"], data: { animation: 'EvoucherDetailComponent' } },
    { path: 'product-list', component: _separated_modules_merchant_products_merchant_products_component__WEBPACK_IMPORTED_MODULE_2__["MerchantProductsComponent"], data: { animation: 'productListPage' } },
    { path: 'homepage/upgrade-genuine', component: _upgrade_genuine_upgrade_genuine_component__WEBPACK_IMPORTED_MODULE_19__["UpgradeGenuineComponent"], data: { animation: 'UpgradeGenuineComponent' } },
    // { path: 'product-list/detail', component: ProductDetailComponent,  data: {animation: 'producDetailPage'} },
    // {path: 'reportadmin', loadChildren: '../modules/report/report.module#ReportModule' , label: ''},
    { path: 'report', component: _modules_report_report_component__WEBPACK_IMPORTED_MODULE_20__["ReportComponent"], data: { animation: 'UpgradeGenuineComponent' } },
    { path: 'report/detail', component: _modules_report_detail_report_detail_component__WEBPACK_IMPORTED_MODULE_21__["ReportDetailComponent"], data: { animation: 'UpgradeGenuineComponent' } },
    { path: 'report/edit', component: _modules_report_edit_report_edit_component__WEBPACK_IMPORTED_MODULE_22__["ReportEditComponent"], data: { animation: 'UpgradeGenuineComponent' } },
    { path: 'report/exceldetail', component: _modules_report_detail_report_excel_detail_component__WEBPACK_IMPORTED_MODULE_23__["ReportExcelDetailComponent"], data: { animation: 'UpgradeGenuineComponent' } },
    { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_3__["ProfileComponent"], data: { animation: 'profilePage' } },
    { path: 'order-histories', component: _order_histories_order_histories_component__WEBPACK_IMPORTED_MODULE_4__["OrderHistoriesComponent"], data: { animation: 'transactionPage' } },
    { path: 'order-histories/shipping-label', component: _order_histories_shipping_label_shipping_label_component__WEBPACK_IMPORTED_MODULE_12__["ShippingLabelComponent"], data: { animation: 'transactionPage' } },
    { path: 'order-detail/edit', component: _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_6__["OrderDetailComponent"], data: { animation: 'transactionPage' } },
    { path: 'statistics', component: _statistics_statistics_component__WEBPACK_IMPORTED_MODULE_7__["StatisticsComponent"], data: { animation: 'transactionPage' } },
    { path: 'sales-order', component: _merchant_sales_order_merchant_sales_order_component__WEBPACK_IMPORTED_MODULE_9__["MerchantSalesOrderComponent"], data: { animation: 'transactionPage' } },
    { path: 'evoucher-sales-report', component: _merchant_evoucher_sales_report_merchant_evoucher_sales_report_component__WEBPACK_IMPORTED_MODULE_11__["MerchantEvoucherSalesReportComponent"], data: { animation: 'transactionPage' } },
    { path: 'inbox', component: _merchant_portal_inbox_merchant_portal_inbox_component__WEBPACK_IMPORTED_MODULE_13__["MerchantPortalInboxComponent"], data: { animation: 'MerchantPortalInboxComponent' } },
    { path: 'inbox-detail', component: _merchant_portal_detail_inbox_merchant_portal_detail_inbox_component__WEBPACK_IMPORTED_MODULE_14__["MerchantPortalDetailInboxComponent"], data: { animation: 'MerchantPortalInboxComponent' } },
];


/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-sales-order/merchant-sales-order.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-sales-order/merchant-sales-order.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".layout-card {\n  background-color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9tZXJjaGFudC1zYWxlcy1vcmRlci9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1lcmNoYW50LXBvcnRhbFxcbWVyY2hhbnQtc2FsZXMtb3JkZXJcXG1lcmNoYW50LXNhbGVzLW9yZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL21lcmNoYW50LXNhbGVzLW9yZGVyL21lcmNoYW50LXNhbGVzLW9yZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksdUJBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvbWVyY2hhbnQtc2FsZXMtb3JkZXIvbWVyY2hhbnQtc2FsZXMtb3JkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGF5b3V0LWNhcmQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBcclxuICBcclxuICB9XHJcbiAgIiwiLmxheW91dC1jYXJkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-sales-order/merchant-sales-order.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-sales-order/merchant-sales-order.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: MerchantSalesOrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantSalesOrderComponent", function() { return MerchantSalesOrderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var MerchantSalesOrderComponent = /** @class */ (function () {
    function MerchantSalesOrderComponent(router, route, orderhistoryService) {
        this.router = router;
        this.route = route;
        this.orderhistoryService = orderhistoryService;
        this.allData = [];
        this.tableFormat = {
            title: 'Sales Order Page',
            label_headers: [
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' }, { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
                { label: 'Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
                { label: 'Price', visible: true, type: 'string', data_row_name: 'total_product_price' },
                // { label: 'Discount Price', visible: false, type: 'string', data_row_name: 'discount_price' },
                // { label: 'Email', visible: false, type: 'string', data_row_name: 'user_email' },
                // { label: 'Order Date', visible: false, type: 'string', data_row_name: 'order_date' },
                // { label: 'Username', visible: false, type: 'string', data_row_name: 'username' },
                // { label: 'Payment Method', visible: false, type: 'string', data_row_name: 'payment_method' },
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Buyer Name', visible: true, type: 'string', data_row_name: 'buyer_name' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: 'order_id',
                this: this,
                result_var_name: 'allData',
                detail_function: [this, 'callDetail'],
            },
            show_checkbox_options: true
        };
        // this.service = this.orderhistoryService;
        // this.tableFormat = merchantSalesOrderTableFormat(this)
        // this.tableFormat.formOptions.addForm       = false;
        // delete this.tableFormat.formOptions.customButtons;
    }
    MerchantSalesOrderComponent.prototype.ngOnInit = function () {
        this.inLoadedProduct();
    };
    MerchantSalesOrderComponent.prototype.inLoadedProduct = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.orderhistoryService;
                        return [4 /*yield*/, this.orderhistoryService.getSalesOrdeReport()];
                    case 1:
                        result = _a.sent();
                        this.allData = result.result.values;
                        this.totalPage = result.result.total_page;
                        console.log("DISINI BRAY", result);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); // conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MerchantSalesOrderComponent.prototype.callDetail = function (result) {
        console.log(result);
    };
    MerchantSalesOrderComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] }
    ]; };
    MerchantSalesOrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-sales-order',
            template: __webpack_require__(/*! raw-loader!./merchant-sales-order.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-sales-order/merchant-sales-order.component.html"),
            styles: [__webpack_require__(/*! ./merchant-sales-order.component.scss */ "./src/app/layout/merchant-portal/merchant-sales-order/merchant-sales-order.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"]])
    ], MerchantSalesOrderComponent);
    return MerchantSalesOrderComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/order-detail/order-detail.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/order-detail/order-detail.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvb3JkZXItZGV0YWlsL29yZGVyLWRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/order-detail/order-detail.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/order-detail/order-detail.component.ts ***!
  \*******************************************************************************/
/*! exports provided: OrderDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailComponent", function() { return OrderDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var OrderDetailComponent = /** @class */ (function () {
    function OrderDetailComponent(orderhistoryService, route, router) {
        this.orderhistoryService = orderhistoryService;
        this.route = route;
        this.router = router;
    }
    OrderDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        console.log("params : ", params);
                        this.orderId = params.id;
                        return [2 /*return*/];
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    OrderDetailComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    OrderDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-detail',
            template: __webpack_require__(/*! raw-loader!./order-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/order-detail/order-detail.component.html"),
            styles: [__webpack_require__(/*! ./order-detail.component.scss */ "./src/app/layout/merchant-portal/order-detail/order-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], OrderDetailComponent);
    return OrderDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/order-histories/shipping-label/shipping-label.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/order-histories/shipping-label/shipping-label.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media print {\n  @page {\n    size: A4 landscape;\n    margin: 0.4mm 0.4mm 1.65mm 0.4mm;\n    display: block;\n  }\n  #header {\n    display: none;\n  }\n\n  h5 {\n    display: none;\n  }\n\n  app-page-header {\n    display: none;\n  }\n\n  button {\n    display: none;\n  }\n\n  * {\n    font-size: 13px;\n  }\n\n  div.table-order {\n    -webkit-transform: scale(0.7);\n            transform: scale(0.7);\n    padding-bottom: 80px;\n    width: 20cm;\n    height: 9cm;\n  }\n\n  div._container {\n    padding-top: 0px;\n    page-break-after: always;\n  }\n\n  .page-number {\n    display: none;\n  }\n}\nbody {\n  background-color: #333;\n  font-family: \"Poppins\";\n  color: #333;\n  text-align: left;\n  font-size: 11px;\n  margin: 0;\n}\n.mat-raised-button.mat-primary {\n  background-color: #2481fb;\n  float: right;\n  padding: 5px 20px;\n  font-family: \"Poppins\";\n  margin: 10px 40px;\n  border-radius: 5px;\n}\n.mat-raised-button.mat-primary .fa {\n  margin-left: 8px;\n}\n._container {\n  background-color: #fff;\n  margin: 0px 0px 30px 0px;\n  border-radius: 5px;\n  text-align: -webkit-center;\n  page-break-after: always;\n  padding-top: 8.7%;\n}\n._container .content .layanan {\n  text-align: center;\n  margin-bottom: 30px;\n}\n._container .content .page-number {\n  text-align: center;\n  padding-top: 25px;\n  padding-bottom: 25px;\n  font-size: 18px;\n}\n._container .content .table-order {\n  height: 100%;\n  width: 14cmcm;\n  font-size: 11px;\n}\n._container .content .table-order .order-id-header {\n  width: 70%;\n  table-layout: fixed;\n  border-top: 0;\n  width: 80%;\n}\n._container .content .table-order .header {\n  table-layout: auto;\n  border-bottom: 0;\n  width: 20cm;\n}\n._container .content .table-order .detail-barang-pengiriman {\n  text-align: left;\n  width: 20cm;\n}\n._container .content .table-order .detail-barang-pengiriman .row-eq-height {\n  display: flex;\n}\n._container .content .table-order .detail-barang-pengiriman .row {\n  margin-left: 0px;\n  height: 100%;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-barang {\n  border-right: solid black 1px;\n  width: 50%;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-barang .daftar-barang {\n  padding-left: 20px;\n  padding-top: 20px;\n  padding-bottom: 20px;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-barang .catatan {\n  padding-top: 20px;\n  padding-bottom: 20px;\n  font-weight: bold;\n  text-transform: uppercase;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman {\n  width: 50%;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman .jenis-pengiriman {\n  padding-top: 20px;\n  padding-bottom: 20px;\n  padding-left: 20px;\n  font-weight: bold;\n  text-align: left;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman .total-harga {\n  padding-top: 30px;\n  padding-bottom: 30px;\n  text-align: center;\n  font-weight: bold;\n  padding-left: 0px;\n}\n._container .content .table-order .detail-header {\n  width: 20cm;\n  text-align: left;\n  border-top: 0;\n  border-bottom: 0;\n}\n._container .content .table-order .detail-header .detail-person {\n  border-right: solid black 1px;\n  padding: 20px;\n  width: 60%;\n}\n._container .content .table-order .detail-header .detail-person .receiver {\n  border-bottom: black solid 1px;\n  padding-bottom: 10px;\n  margin-bottom: 15px;\n  margin-top: 15px;\n}\n._container .content .table-order .detail-header .detail-order {\n  padding: 20px;\n}\n._container .content .table-order .detail-header .detail-order .tlc {\n  font-size: 20px;\n}\n._container .content .table-order .detail-header .row {\n  margin-left: 0px;\n}\n._container .content .table-order .locard-logo {\n  width: 100px;\n  text-align: center;\n}\n._container .content .table-order .locard-logo .logo-avatar {\n  border-radius: 0%;\n  width: 60px;\n}\n._container .content .table-order .courier-logo {\n  text-align: center;\n}\n._container .content .table-order #courier-logo {\n  text-align: center;\n}\n._container .content .table-order #courier-logo .logo-avatar {\n  width: 120px;\n}\n._container .content .table-order .awb {\n  text-align: center;\n}\n._container .content .table-order .order-id {\n  text-align: center;\n}\n._container .content .table-order .order-id p {\n  font-size: 20px;\n  font-weight: bold;\n}\n.btn-print {\n  text-align: center;\n  margin: 15px 0px;\n}\ndiv._container:last-child {\n  page-break-after: always;\n}\ntable {\n  border: 2px solid #333;\n  border-collapse: collapse;\n}\ntd,\ntr,\nth {\n  padding: 12px;\n  border: 1px solid #333;\n  width: 185px;\n}\nth {\n  background-color: #f0f0f0;\n}\nh4,\np {\n  margin: 0px;\n}\n#garis {\n  height: 200px;\n  color: black;\n}\n#thanks {\n  font-size: 15px;\n  text-align: center;\n  color: #0ea8db;\n  font-weight: bold;\n}\n#header {\n  text-align: center;\n  margin-top: 10px;\n}\n#non {\n  border: 1px solid black;\n  font-size: 20px;\n  padding: 4px;\n}\n#shipping {\n  font-size: 20px;\n}\n#order {\n  position: relative;\n  bottom: 10px;\n}\n.send {\n  text-align: center;\n}\n.sticky {\n  position: fixed;\n  top: 0;\n  width: 100%;\n}\n.sticky + #card-transaction {\n  padding-top: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9vcmRlci1oaXN0b3JpZXMvc2hpcHBpbmctbGFiZWwvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtZXJjaGFudC1wb3J0YWxcXG9yZGVyLWhpc3Rvcmllc1xcc2hpcHBpbmctbGFiZWxcXHNoaXBwaW5nLWxhYmVsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL29yZGVyLWhpc3Rvcmllcy9zaGlwcGluZy1sYWJlbC9zaGlwcGluZy1sYWJlbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFxSEE7RUFDRTtJQUNFLGtCQUFBO0lBQ0EsZ0NBQUE7SUFDQSxjQUFBO0VDcEhGO0VEdUhBO0lBQ0UsYUFBQTtFQ3JIRjs7RUR3SEE7SUFDRSxhQUFBO0VDckhGOztFRHdIQTtJQUNFLGFBQUE7RUNySEY7O0VEd0hBO0lBQ0UsYUFBQTtFQ3JIRjs7RUR3SEE7SUFDRSxlQUFBO0VDckhGOztFRHdIQTtJQUNFLDZCQUFBO1lBQUEscUJBQUE7SUFDQSxvQkFBQTtJQUNBLFdBQUE7SUFDQSxXQUFBO0VDckhGOztFRHdIQTtJQUNFLGdCQUFBO0lBQ0Esd0JBQUE7RUNySEY7O0VEd0hBO0lBQ0UsYUFBQTtFQ3JIRjtBQUNGO0FEd0hBO0VBQ0Usc0JBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0FDdEhGO0FEd0hBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNySEY7QUR1SEU7RUFDRSxnQkFBQTtBQ3JISjtBRHlIQTtFQUNFLHNCQUFBO0VBQ0Esd0JBQUE7RUFDQSxrQkFBQTtFQUNBLDBCQUFBO0VBQ0Esd0JBQUE7RUFDQSxpQkFBQTtBQ3RIRjtBRHlISTtFQUNFLGtCQUFBO0VBQ0EsbUJBQUE7QUN2SE47QUQwSEk7RUFDRSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0FDeEhOO0FEMkhJO0VBQ0UsWUFBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0FDekhOO0FEMkhNO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7QUN6SFI7QUQ0SE07RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQzFIUjtBRDZITTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBQzNIUjtBRDZIUTtFQUlFLGFBQUE7QUMzSFY7QUQ4SFE7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUM1SFY7QUQrSFE7RUFDRSw2QkFBQTtFQUNBLFVBQUE7QUM3SFY7QUQ4SFU7RUFDRSxrQkFBQTtFQUVBLGlCQUFBO0VBQ0Esb0JBQUE7QUM3SFo7QURnSVU7RUFDRSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtBQzlIWjtBRGtJUTtFQUNFLFVBQUE7QUNoSVY7QURpSVU7RUFDRSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDL0haO0FEa0lVO0VBQ0UsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQ2hJWjtBRHFJTTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQ25JUjtBRHFJUTtFQUNFLDZCQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7QUNuSVY7QURvSVU7RUFDRSw4QkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ2xJWjtBRHNJUTtFQUNFLGFBQUE7QUNwSVY7QURxSVU7RUFDRSxlQUFBO0FDbklaO0FEdUlRO0VBQ0UsZ0JBQUE7QUNySVY7QUR5SU07RUFDRSxZQUFBO0VBQ0Esa0JBQUE7QUN2SVI7QUR3SVE7RUFFRSxpQkFBQTtFQUNBLFdBQUE7QUN0SVY7QUQwSU07RUFDRSxrQkFBQTtBQ3hJUjtBRDJJTTtFQUNFLGtCQUFBO0FDeklSO0FEMklRO0VBQ0UsWUFBQTtBQ3pJVjtBRDZJTTtFQUNFLGtCQUFBO0FDM0lSO0FEOElNO0VBQ0Usa0JBQUE7QUM1SVI7QUQ2SVE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUMzSVY7QURrSkE7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FDL0lGO0FEa0pBO0VBQ0Usd0JBQUE7QUMvSUY7QURpSkE7RUFDRSxzQkFBQTtFQUNBLHlCQUFBO0FDOUlGO0FEZ0pBOzs7RUFHRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0FDN0lGO0FEK0lBO0VBQ0UseUJBQUE7QUM1SUY7QUQ4SUE7O0VBRUUsV0FBQTtBQzNJRjtBRDhJQTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FDM0lGO0FEOElBO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FDM0lGO0FEOElBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBQzNJRjtBRDhJQTtFQUNFLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUMzSUY7QUQ4SUE7RUFDRSxlQUFBO0FDM0lGO0FEOElBO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0FDM0lGO0FEOElBO0VBQ0Usa0JBQUE7QUMzSUY7QUQ4SUE7RUFDRSxlQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7QUMzSUY7QUQ4SUE7RUFDRSxpQkFBQTtBQzNJRiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvb3JkZXItaGlzdG9yaWVzL3NoaXBwaW5nLWxhYmVsL3NoaXBwaW5nLWxhYmVsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQG1lZGlhIHByaW50e1xyXG4vLyAgIEBwYWdlIHtcclxuLy8gICAgIHNpemU6IEE0IGxhbmRzY2FwZTtcclxuLy8gICB9XHJcblxyXG4vLyAgICNoZWFkZXJ7XHJcbi8vICAgICBkaXNwbGF5OiBub25lO1xyXG4vLyAgIH1cclxuXHJcbi8vICAgaDV7XHJcbi8vICAgICBkaXNwbGF5OiBub25lO1xyXG4vLyAgIH1cclxuXHJcbi8vICAgYXBwLXBhZ2UtaGVhZGVye1xyXG4vLyAgICAgZGlzcGxheTogbm9uZTtcclxuLy8gICB9XHJcblxyXG4vLyAgIGJ1dHRvbntcclxuLy8gICAgIGRpc3BsYXk6IG5vbmU7XHJcbi8vICAgfVxyXG5cclxuLy8gICBkaXYuX2NvbnRhaW5lcntcclxuLy8gICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGFsd2F5cztcclxuLy8gICB9XHJcbi8vIH1cclxuXHJcbi8vIGxhYmVse1xyXG4vLyAgIGZvbnQtc2l6ZTogMTBweDtcclxuLy8gfVxyXG5cclxuLy8gLl9jb250YWluZXJ7XHJcbi8vICAgYm9yZGVyOiBzb2xpZCBibGFjayAxcHg7XHJcbi8vICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XHJcbi8vICAgLnRhYmxlLWxhYmVse1xyXG4vLyAgICAgd2lkdGg6IDE0Y207XHJcbi8vICAgICBoZWlnaHQ6IDljbTtcclxuLy8gICAgIC5zaGlwcGluZy1pbmZve1xyXG4vLyAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgICAgICBwYWRkaW5nOiA1cHg7XHJcbi8vICAgICAgIC5sb2dve1xyXG4vLyAgICAgICAgIC5sb2dvLWF2YXRhcntcclxuLy8gICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4vLyAgICAgICAgIH1cclxuLy8gICAgICAgfVxyXG4vLyAgICAgICAuYXdie1xyXG4vLyAgICAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4vLyAgICAgICB9XHJcbi8vICAgICAgIC5vcmRlci1pZHtcclxuLy8gICAgICAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuLy8gICAgICAgfVxyXG4vLyAgICAgfVxyXG4vLyAgICAgLnNoaXBwaW5nLWRldGFpbHtcclxuLy8gICAgICAgZGlzcGxheTogZmxleDtcclxuLy8gICAgICAgcGFkZGluZzogNXB4O1xyXG4vLyAgICAgICAuZGV0YWlse1xyXG4vLyAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbi8vICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuLy8gICAgICAgICAuc2VuZGVyLXJlY2VpdmVye1xyXG4vLyAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbi8vICAgICAgICAgICAuc2VuZGVye1xyXG4vLyAgICAgICAgICAgICBsYWJlbHtcclxuLy8gICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgICAgICAgICAgfVxyXG4vLyAgICAgICAgIH1cclxuLy8gICAgICAgICAgIC5yZWNlaXZlcntcclxuLy8gICAgICAgICAgICAgbGFiZWx7XHJcbi8vICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgICAgICAgICAgIH1cclxuLy8gICAgICAgICAgIH1cclxuLy8gICAgICAgICB9XHJcbi8vICAgICAgICAgLmRhZnRhci1iYXJhbmd7XHJcblxyXG4vLyAgICAgICAgIH1cclxuLy8gICAgICAgfVxyXG4vLyAgICAgICAuZGVzdGluYXRpb257XHJcbi8vICAgICAgICAgLmRlc3RpbmF0aW9uLWRldGFpbHtcclxuLy8gICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbi8vICAgICAgICAgICAudGFuZ2dhbHtcclxuXHJcbi8vICAgICAgICAgICB9XHJcbi8vICAgICAgICAgICAua290YS1hc2Fse1xyXG5cclxuLy8gICAgICAgICAgIH1cclxuLy8gICAgICAgICAgIC5rb3RhLXR1anVhbntcclxuXHJcbi8vICAgICAgICAgICB9XHJcbi8vICAgICAgICAgICAuanVtbGFoe1xyXG5cclxuLy8gICAgICAgICAgIH1cclxuLy8gICAgICAgICAgIC5iZXJhdC10b3RhbHtcclxuXHJcbi8vICAgICAgICAgICB9XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgICB9XHJcbi8vICAgICB9XHJcbi8vICAgICAuc2hpcHBpbmctbm90ZXN7XHJcbi8vICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbi8vICAgICAgIHBhZGRpbmc6IDVweDtcclxuLy8gICAgICAgLnRoYW5reW91LW5vdGV7XHJcblxyXG4vLyAgICAgICB9XHJcbi8vICAgICAgIC5jYXRhdGFue1xyXG5cclxuLy8gICAgICAgfVxyXG4vLyAgICAgICAuZGVsaXZlcnktc2VydmljZXtcclxuXHJcbi8vICAgICAgIH1cclxuLy8gICAgIH1cclxuLy8gICB9XHJcbi8vIH1cclxuXHJcbi8vICNhd2J7XHJcbi8vICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4vLyB9XHJcbi8vICNvcmRlci1pZHtcclxuLy8gICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbi8vIH1cclxuQG1lZGlhIHByaW50IHtcclxuICBAcGFnZSB7XHJcbiAgICBzaXplOiBBNCBsYW5kc2NhcGU7XHJcbiAgICBtYXJnaW46IDAuNG1tIDAuNG1tIDEuNjVtbSAwLjRtbTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gIH1cclxuXHJcbiAgI2hlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgaDUge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcblxyXG4gIGFwcC1wYWdlLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgYnV0dG9uIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAqIHtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICB9XHJcblxyXG4gIGRpdi50YWJsZS1vcmRlciB7XHJcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogODBweDtcclxuICAgIHdpZHRoOiAyMGNtO1xyXG4gICAgaGVpZ2h0OiA5Y207XHJcbiAgfVxyXG5cclxuICBkaXYuX2NvbnRhaW5lciB7XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xyXG4gIH1cclxuXHJcbiAgLnBhZ2UtbnVtYmVyIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG59XHJcblxyXG5ib2R5IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzMzO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcclxuICBjb2xvcjogIzMzMztcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGZvbnQtc2l6ZTogMTFweDtcclxuICBtYXJnaW46IDA7XHJcbn1cclxuLm1hdC1yYWlzZWQtYnV0dG9uLm1hdC1wcmltYXJ5IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjQ4MWZiO1xyXG4gIGZsb2F0OiByaWdodDtcclxuICBwYWRkaW5nOiA1cHggMjBweDtcclxuICBmb250LWZhbWlseTogXCJQb3BwaW5zXCI7XHJcbiAgbWFyZ2luOiAxMHB4IDQwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG5cclxuICAuZmEge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDhweDtcclxuICB9XHJcbn1cclxuXHJcbi5fY29udGFpbmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gIG1hcmdpbjogMHB4IDBweCAzMHB4IDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XHJcbiAgcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xyXG4gIHBhZGRpbmctdG9wOiA4LjclO1xyXG5cclxuICAuY29udGVudCB7XHJcbiAgICAubGF5YW5hbiB7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAgIH1cclxuXHJcbiAgICAucGFnZS1udW1iZXIge1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAyNXB4O1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMjVweDtcclxuICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC50YWJsZS1vcmRlciB7XHJcbiAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgd2lkdGg6IDE0Y21jbTtcclxuICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG5cclxuICAgICAgLm9yZGVyLWlkLWhlYWRlciB7XHJcbiAgICAgICAgd2lkdGg6IDcwJTtcclxuICAgICAgICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xyXG4gICAgICAgIGJvcmRlci10b3A6IDA7XHJcbiAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmhlYWRlciB7XHJcbiAgICAgICAgdGFibGUtbGF5b3V0OiBhdXRvO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDA7XHJcbiAgICAgICAgd2lkdGg6IDIwY207XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4ge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgd2lkdGg6IDIwY207XHJcblxyXG4gICAgICAgIC5yb3ctZXEtaGVpZ2h0IHtcclxuICAgICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gICAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLnJvdyB7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmRldGFpbC1iYXJhbmcge1xyXG4gICAgICAgICAgYm9yZGVyLXJpZ2h0OiBzb2xpZCBibGFjayAxcHg7XHJcbiAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICAgLmRhZnRhci1iYXJhbmcge1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbiAgICAgICAgICAgIC8vIGJvcmRlci1yaWdodDogc29saWQgYmxhY2sgMXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgLmNhdGF0YW4ge1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmRldGFpbC1wZW5naXJpbWFuIHtcclxuICAgICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgICAuamVuaXMtcGVuZ2lyaW1hbiB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAudG90YWwtaGFyZ2Ege1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMzBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDMwcHg7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLmRldGFpbC1oZWFkZXIge1xyXG4gICAgICAgIHdpZHRoOiAyMGNtO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgYm9yZGVyLXRvcDogMDtcclxuICAgICAgICBib3JkZXItYm90dG9tOiAwO1xyXG5cclxuICAgICAgICAuZGV0YWlsLXBlcnNvbiB7XHJcbiAgICAgICAgICBib3JkZXItcmlnaHQ6IHNvbGlkIGJsYWNrIDFweDtcclxuICAgICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgICB3aWR0aDogNjAlO1xyXG4gICAgICAgICAgLnJlY2VpdmVyIHtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogYmxhY2sgc29saWQgMXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5kZXRhaWwtb3JkZXIge1xyXG4gICAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICAgIC50bGMge1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAucm93IHtcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAubG9jYXJkLWxvZ28ge1xyXG4gICAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgLmxvZ28tYXZhdGFyIHtcclxuICAgICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogMCU7XHJcbiAgICAgICAgICB3aWR0aDogNjBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5jb3VyaWVyLWxvZ28ge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG5cclxuICAgICAgI2NvdXJpZXItbG9nbyB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgICAgICAubG9nby1hdmF0YXIge1xyXG4gICAgICAgICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLmF3YiB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAub3JkZXItaWQge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmJ0bi1wcmludCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbjogMTVweCAwcHg7XHJcbn1cclxuXHJcbmRpdi5fY29udGFpbmVyOmxhc3QtY2hpbGQge1xyXG4gIHBhZ2UtYnJlYWstYWZ0ZXI6IGFsd2F5cztcclxufVxyXG50YWJsZSB7XHJcbiAgYm9yZGVyOiAycHggc29saWQgIzMzMztcclxuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG59XHJcbnRkLFxyXG50cixcclxudGgge1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgIzMzMztcclxuICB3aWR0aDogMTg1cHg7XHJcbn1cclxudGgge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbn1cclxuaDQsXHJcbnAge1xyXG4gIG1hcmdpbjogMHB4O1xyXG59XHJcblxyXG4jZ2FyaXMge1xyXG4gIGhlaWdodDogMjAwcHg7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4jdGhhbmtzIHtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGNvbG9yOiAjMGVhOGRiO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4jaGVhZGVyIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuI25vbiB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIHBhZGRpbmc6IDRweDtcclxufVxyXG5cclxuI3NoaXBwaW5nIHtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuXHJcbiNvcmRlciB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGJvdHRvbTogMTBweDtcclxufVxyXG5cclxuLnNlbmQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnN0aWNreSB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHRvcDogMDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnN0aWNreSArICNjYXJkLXRyYW5zYWN0aW9uIHtcclxuICBwYWRkaW5nLXRvcDogNTBweDtcclxufVxyXG4iLCJAbWVkaWEgcHJpbnQge1xuICBAcGFnZSB7XG4gICAgc2l6ZTogQTQgbGFuZHNjYXBlO1xuICAgIG1hcmdpbjogMC40bW0gMC40bW0gMS42NW1tIDAuNG1tO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG4gICNoZWFkZXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICBoNSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIGFwcC1wYWdlLWhlYWRlciB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIGJ1dHRvbiB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gICoge1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgfVxuXG4gIGRpdi50YWJsZS1vcmRlciB7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwLjcpO1xuICAgIHBhZGRpbmctYm90dG9tOiA4MHB4O1xuICAgIHdpZHRoOiAyMGNtO1xuICAgIGhlaWdodDogOWNtO1xuICB9XG5cbiAgZGl2Ll9jb250YWluZXIge1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xuICB9XG5cbiAgLnBhZ2UtbnVtYmVyIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG59XG5ib2R5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzMzMztcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xuICBjb2xvcjogIzMzMztcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBtYXJnaW46IDA7XG59XG5cbi5tYXQtcmFpc2VkLWJ1dHRvbi5tYXQtcHJpbWFyeSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNDgxZmI7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZzogNXB4IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcbiAgbWFyZ2luOiAxMHB4IDQwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cbi5tYXQtcmFpc2VkLWJ1dHRvbi5tYXQtcHJpbWFyeSAuZmEge1xuICBtYXJnaW4tbGVmdDogOHB4O1xufVxuXG4uX2NvbnRhaW5lciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gIG1hcmdpbjogMHB4IDBweCAzMHB4IDBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xuICBwYWRkaW5nLXRvcDogOC43JTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5sYXlhbmFuIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnBhZ2UtbnVtYmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogMjVweDtcbiAgcGFkZGluZy1ib3R0b206IDI1cHg7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDE0Y21jbTtcbiAgZm9udC1zaXplOiAxMXB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5vcmRlci1pZC1oZWFkZXIge1xuICB3aWR0aDogNzAlO1xuICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xuICBib3JkZXItdG9wOiAwO1xuICB3aWR0aDogODAlO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5oZWFkZXIge1xuICB0YWJsZS1sYXlvdXQ6IGF1dG87XG4gIGJvcmRlci1ib3R0b206IDA7XG4gIHdpZHRoOiAyMGNtO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4ge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICB3aWR0aDogMjBjbTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5yb3ctZXEtaGVpZ2h0IHtcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAucm93IHtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLmRldGFpbC1iYXJhbmcge1xuICBib3JkZXItcmlnaHQ6IHNvbGlkIGJsYWNrIDFweDtcbiAgd2lkdGg6IDUwJTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5kZXRhaWwtYmFyYW5nIC5kYWZ0YXItYmFyYW5nIHtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLWJhcmFuZyAuY2F0YXRhbiB7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLXBlbmdpcmltYW4ge1xuICB3aWR0aDogNTAlO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLmRldGFpbC1wZW5naXJpbWFuIC5qZW5pcy1wZW5naXJpbWFuIHtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLmRldGFpbC1wZW5naXJpbWFuIC50b3RhbC1oYXJnYSB7XG4gIHBhZGRpbmctdG9wOiAzMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMzBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1oZWFkZXIge1xuICB3aWR0aDogMjBjbTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgYm9yZGVyLXRvcDogMDtcbiAgYm9yZGVyLWJvdHRvbTogMDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWhlYWRlciAuZGV0YWlsLXBlcnNvbiB7XG4gIGJvcmRlci1yaWdodDogc29saWQgYmxhY2sgMXB4O1xuICBwYWRkaW5nOiAyMHB4O1xuICB3aWR0aDogNjAlO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIC5kZXRhaWwtcGVyc29uIC5yZWNlaXZlciB7XG4gIGJvcmRlci1ib3R0b206IGJsYWNrIHNvbGlkIDFweDtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1oZWFkZXIgLmRldGFpbC1vcmRlciB7XG4gIHBhZGRpbmc6IDIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1oZWFkZXIgLmRldGFpbC1vcmRlciAudGxjIHtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIC5yb3cge1xuICBtYXJnaW4tbGVmdDogMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5sb2NhcmQtbG9nbyB7XG4gIHdpZHRoOiAxMDBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5sb2NhcmQtbG9nbyAubG9nby1hdmF0YXIge1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYm9yZGVyLXJhZGl1czogMCU7XG4gIHdpZHRoOiA2MHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5jb3VyaWVyLWxvZ28ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgI2NvdXJpZXItbG9nbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAjY291cmllci1sb2dvIC5sb2dvLWF2YXRhciB7XG4gIHdpZHRoOiAxMjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuYXdiIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5vcmRlci1pZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAub3JkZXItaWQgcCB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5idG4tcHJpbnQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMTVweCAwcHg7XG59XG5cbmRpdi5fY29udGFpbmVyOmxhc3QtY2hpbGQge1xuICBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXM7XG59XG5cbnRhYmxlIHtcbiAgYm9yZGVyOiAycHggc29saWQgIzMzMztcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbn1cblxudGQsXG50cixcbnRoIHtcbiAgcGFkZGluZzogMTJweDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzMzMztcbiAgd2lkdGg6IDE4NXB4O1xufVxuXG50aCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG5cbmg0LFxucCB7XG4gIG1hcmdpbjogMHB4O1xufVxuXG4jZ2FyaXMge1xuICBoZWlnaHQ6IDIwMHB4O1xuICBjb2xvcjogYmxhY2s7XG59XG5cbiN0aGFua3Mge1xuICBmb250LXNpemU6IDE1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICMwZWE4ZGI7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4jaGVhZGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4jbm9uIHtcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgcGFkZGluZzogNHB4O1xufVxuXG4jc2hpcHBpbmcge1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbiNvcmRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYm90dG9tOiAxMHB4O1xufVxuXG4uc2VuZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnN0aWNreSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLnN0aWNreSArICNjYXJkLXRyYW5zYWN0aW9uIHtcbiAgcGFkZGluZy10b3A6IDUwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/order-histories/shipping-label/shipping-label.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/order-histories/shipping-label/shipping-label.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: ShippingLabelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingLabelComponent", function() { return ShippingLabelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ShippingLabelComponent = /** @class */ (function () {
    function ShippingLabelComponent(OrderhistoryService, router, route, memberService) {
        this.OrderhistoryService = OrderhistoryService;
        this.router = router;
        this.route = route;
        this.memberService = memberService;
        this.getData = [];
        this.totalResult = [];
        this.totalWeight = [];
        this.perWeight = [];
        this.endTotalResult = [];
        this.loading = true;
        this.same = false;
        this.merchant_data = {};
        this.courier_image_array = [];
        // this.getData = route.snapshot.params['invoiceIds']
        //   .split(',');
    }
    ShippingLabelComponent.prototype.ngOnInit = function () {
        this.firstload();
        // this.print();
    };
    ShippingLabelComponent.prototype.firstload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var member, e_1, _i, _a, element, result, e_2;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.memberService.getMemberDetail()];
                    case 1:
                        member = _b.sent();
                        this.merchant_data = member.result;
                        console.log('data', this.merchant_data);
                        if (member.result) {
                            this.memberCity = member.result.mcity;
                            console.log("member", member.result);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _b.sent();
                        this.errorMessage = (e_1.message);
                        return [3 /*break*/, 3];
                    case 3:
                        this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                this.getData = params.data;
                                this.service = this.OrderhistoryService;
                                console.log(this.getData);
                                return [2 /*return*/];
                            });
                        }); });
                        _i = 0, _a = this.getData;
                        _b.label = 4;
                    case 4:
                        if (!(_i < _a.length)) return [3 /*break*/, 9];
                        element = _a[_i];
                        _b.label = 5;
                    case 5:
                        _b.trys.push([5, 7, , 8]);
                        return [4 /*yield*/, this.OrderhistoryService.getDetailOrderhistory(element)];
                    case 6:
                        result = _b.sent();
                        console.log(result);
                        this.totalResult.push(result.result[0]);
                        return [3 /*break*/, 8];
                    case 7:
                        e_2 = _b.sent();
                        this.errorMessage = (e_2.message);
                        this.loading = false;
                        return [3 /*break*/, 8];
                    case 8:
                        _i++;
                        return [3 /*break*/, 4];
                    case 9:
                        console.log(this.totalResult);
                        this.totalResult.forEach(function (element, index) {
                            console.log("asd", element);
                            _this.current_courier = element.courier.courier;
                            _this.courier_list = element.courier_list.supported;
                            _this.courier_list.forEach(function (entry) {
                                // console.log(entry)
                                console.log(entry.courier);
                                if (entry.courier == _this.current_courier) {
                                    var format = {
                                        'image': entry.image_url, 'courier': _this.current_courier, 'index': index, 'same': true
                                    };
                                    _this.courier_image_array.push(format);
                                    _this.courier_image = entry.image_url;
                                    console.log("image", _this.courier_image);
                                    _this.same = true;
                                }
                            });
                            if (_this.same == false) {
                                var format = {
                                    'image': '', 'courier': _this.current_courier, 'index': index, 'same': false
                                };
                                _this.courier_image_array.push(format);
                            }
                            element.products.forEach(function (el, i) {
                                _this.perWeight = el.weight;
                            });
                            _this.same = false;
                            console.log('courier image', _this.courier_image_array);
                            _this.totalWeight[index] = _this.perWeight;
                        });
                        // window.print();
                        this.loading = false;
                        return [2 /*return*/];
                }
            });
        });
    };
    ShippingLabelComponent.prototype.prints = function () {
        window.print();
    };
    ShippingLabelComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] }
    ]; };
    ShippingLabelComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-shipping-label',
            template: __webpack_require__(/*! raw-loader!./shipping-label.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/order-histories/shipping-label/shipping-label.component.html"),
            styles: [__webpack_require__(/*! ./shipping-label.component.scss */ "./src/app/layout/merchant-portal/order-histories/shipping-label/shipping-label.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"]])
    ], ShippingLabelComponent);
    return ShippingLabelComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/profile/profile.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/merchant-portal/profile/profile.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "agm-map {\n  height: 300px;\n}\n\n.image-upload-group {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  overflow: hidden;\n  margin-bottom: 25px;\n}\n\n.image-upload-group .img-upload {\n  width: calc((100vw - 300px) / 2);\n  position: relative;\n  height: calc((100vw - 300px) / 2);\n}\n\n.image-upload-group .img-upload > .card {\n  height: 100%;\n  display: block;\n}\n\n.image-upload-group .img-upload > .card > .card-title h2 {\n  font-size: 24px;\n}\n\n.image-upload-group .img-upload > .card > .card-content {\n  height: calc(100% - 82px);\n  position: relative;\n}\n\n.image-upload-group .img-upload > .card > .card-content .image {\n  display: block;\n  float: left;\n  height: calc(100% - 20px);\n  width: 100%;\n}\n\n.image-upload-group .img-upload > .card > .card-content .image .custom {\n  height: calc(100% - 20px);\n  margin: 20px;\n  background-color: rgba(0, 0, 0, 0.03);\n  border: 2px solid #ebebeb;\n  border-radius: 10px;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n  padding: 40px;\n}\n\n.image-upload-group .img-upload > .card > .card-content .image .custom .set-opacity {\n  opacity: 0.1;\n  height: 60px;\n  width: auto;\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: calc(100% - 82px);\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded .uploaded-image {\n  height: 100%;\n  padding: 20px;\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded .uploaded-image .img {\n  height: 100%;\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded .uploaded-image .editor-tool {\n  height: 100%;\n}\n\n.image-upload-group .img-upload:last-child {\n  margin-left: 16px;\n}\n\n#button-verify {\n  width: 100%;\n  border-radius: 5px;\n  margin-top: 10px;\n  border: none;\n  color: white;\n  height: 50px;\n  font-size: 20px;\n}\n\n.check-button {\n  margin-left: 5px;\n  border-radius: 5px;\n  text-decoration: none;\n}\n\n.display-none {\n  display: none;\n}\n\n.modal-header {\n  border: none;\n}\n\n.check-button true {\n  background-color: lightblue;\n  color: white;\n}\n\n#line {\n  border-bottom: 2px solid black;\n  width: 1175px;\n}\n\n.shield {\n  width: 7%;\n  margin-top: 20px;\n  margin-left: 10px;\n}\n\n.security-content {\n  margin-top: 20px;\n  margin-left: 20px;\n}\n\n.check-button false {\n  background-color: lightblue;\n  color: white;\n}\n\n.form-body .or-group {\n  margin-left: 0px;\n  margin-right: 0px;\n}\n\n.form-body .or-group .col-md-6 {\n  padding-left: 0px;\n}\n\n.form-body .or-group .col-md-6:last-child {\n  padding-right: 0px;\n}\n\n.blue-fill {\n  color: white !important;\n  border: 1px solid #0069d9;\n  background: #2480fb;\n}\n\n.blue-fill:hover {\n  background-color: #0069d9;\n}\n\n.courier-service .mat-checkbox:not(:first-child) {\n  margin-left: 15px;\n}\n\n.btn.save {\n  margin: 15px 15px 0px 0px;\n}\n\n.form {\n  padding: 25px;\n  overflow: hidden;\n  position: relative;\n}\n\n.form .title-diff {\n  margin-top: 25px;\n  margin-bottom: 15px;\n  border-bottom: 1px solid #333;\n}\n\n.form .browse-files .fileupload {\n  margin-top: 25px;\n}\n\n.form .error-message {\n  float: left;\n}\n\n.form .card.error {\n  text-align: center;\n  font-size: 20px;\n  background-color: red;\n  color: white;\n  width: 50%;\n  margin-left: 24%;\n  margin-top: 20px;\n}\n\n.form .head-form {\n  overflow: hidden;\n  height: 11vw;\n}\n\n.form .head-form .photo-profile {\n  display: block;\n  width: 10vw;\n  height: 10vw;\n  float: left;\n}\n\n.form .head-form .detail {\n  display: flex;\n  flex-direction: column;\n  float: left;\n  justify-content: center;\n  align-items: flex-start;\n  font-size: 15px;\n  height: 65px;\n  line-height: 17px;\n  padding: 5px;\n  margin-top: 3vw;\n  margin-left: 2vw;\n}\n\n.form .head-form .detail .name {\n  font-size: 27px;\n  font-weight: bold;\n}\n\n.form .editor-group {\n  margin-top: 25px;\n}\n\n.form .form-group {\n  position: relative;\n}\n\n.form .form-group label {\n  text-transform: capitalize;\n}\n\n.form .form-group .form-control {\n  font-size: 12px;\n  background-color: transparent;\n  font-weight: bold;\n}\n\n.form .form-group .form-control.true {\n  background-color: white;\n  font-weight: normal;\n}\n\n.form .form-group .form-control ~ .iconic {\n  position: absolute;\n  right: 10px;\n  top: 37px;\n  -webkit-transform: scale(0.7);\n          transform: scale(0.7);\n}\n\n.form .form-group .description {\n  height: 110px;\n}\n\n.form .vl {\n  border-left: 1px solid #eaeaea;\n}\n\n.form img.resize {\n  max-width: 100%;\n  max-height: 100%;\n}\n\n.card-title {\n  margin: 20px;\n  border-bottom: 1px solid #333;\n  border-left: 5px solid #8fd6ff;\n}\n\n.card-title h2 {\n  margin: 0px 0px 5px 5px;\n  font-size: 23px;\n  padding: 5px;\n}\n\n.check {\n  font-size: 12px;\n  padding-left: 12px;\n}\n\n.uploaded-image {\n  margin: 0px auto;\n  width: auto;\n  padding: 50px;\n  height: auto;\n  cursor: pointer;\n}\n\n.uploaded-image .text-fileupload {\n  display: block;\n  text-align: center;\n  cursor: pointer;\n  color: #2480fb;\n  margin-top: 10px;\n}\n\n.uploaded-image .img {\n  width: 100%;\n  background-size: cover;\n  position: relative;\n  background-position: center center;\n  background-repeat: no-repeat;\n}\n\n.uploaded-image .img .editor-tool {\n  background: rgba(0, 0, 0, 0.2);\n  width: 100%;\n  height: 100%;\n  position: relative;\n  opacity: 0;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.uploaded-image .img:hover .editor-tool {\n  opacity: 0.9;\n}\n\ninput[type=file] {\n  display: none;\n}\n\n.photo {\n  position: absolute;\n  display: flex;\n  justify-content: center;\n  vertical-align: middle;\n  align-items: center;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\n\n.image-part {\n  text-align: center;\n}\n\n.background {\n  position: relative;\n  top: 0px;\n  left: 0px;\n}\n\n.content {\n  padding-top: 10px;\n  font-size: 18px;\n  line-height: 18px;\n}\n\n.modal.false {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n\n.modal.true {\n  display: block;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0px;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: white;\n  opacity: 1;\n  /* Fallback color */\n  /* Black w/ opacity */\n}\n\n.modal-content {\n  margin-left: 256px;\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  margin-top: 200px;\n  padding: 0;\n  width: 30%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n\n/* The Close Button */\n\n.close {\n  color: white;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n\n.close:hover,\n.close:focus {\n  color: #000;\n  text-decoration: none;\n  cursor: pointer;\n}\n\n@media (max-width: 850px) {\n  .image-upload-group .img-upload > .card > .card-content .image .custom img.set-opacity {\n    height: 25px;\n  }\n  .image-upload-group .img-upload > .card > .card-content .image .custom .browse-files .pb-framer .common-button {\n    margin-top: 8px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9wcm9maWxlL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbWVyY2hhbnQtcG9ydGFsXFxwcm9maWxlXFxwcm9maWxlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLGFBQUE7QUNERjs7QURHQTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ0FGOztBRENFO0VBQ0UsZ0NBQUE7RUFDQSxrQkFBQTtFQUNBLGlDQUFBO0FDQ0o7O0FEQUk7RUFDRSxZQUFBO0VBQ0EsY0FBQTtBQ0VOOztBREFRO0VBQ0UsZUFBQTtBQ0VWOztBRENNO0VBQ0UseUJBQUE7RUFDQSxrQkFBQTtBQ0NSOztBRENRO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7QUNDVjs7QURBVTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLHFDQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0FDRVo7O0FERFk7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNHZDs7QURFTTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtBQ0FSOztBRENRO0VBQ0UsWUFBQTtFQUNBLGFBQUE7QUNDVjs7QURBVTtFQUNFLFlBQUE7QUNFWjs7QURDVTtFQUNFLFlBQUE7QUNDWjs7QURLRTtFQUNFLGlCQUFBO0FDSEo7O0FET0E7RUFFRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNMRjs7QURRQTtFQUNFLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBQ0xGOztBRFFBO0VBQ0UsYUFBQTtBQ0xGOztBRFFBO0VBQ0UsWUFBQTtBQ0xGOztBRFFBO0VBQ0UsMkJBQUE7RUFDQSxZQUFBO0FDTEY7O0FEUUE7RUFDRSw4QkFBQTtFQUNBLGFBQUE7QUNMRjs7QURRQTtFQUNFLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDTEY7O0FEUUE7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0FDTEY7O0FEUUE7RUFDRSwyQkFBQTtFQUNBLFlBQUE7QUNMRjs7QURTRTtFQUNFLGdCQUFBO0VBQ0EsaUJBQUE7QUNOSjs7QURPSTtFQUNFLGlCQUFBO0FDTE47O0FET0k7RUFDRSxrQkFBQTtBQ0xOOztBRFNBO0VBQ0UsdUJBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FDTkY7O0FEUUE7RUFDRSx5QkFBQTtBQ0xGOztBRFFFO0VBQ0UsaUJBQUE7QUNMSjs7QURRQTtFQUNFLHlCQUFBO0FDTEY7O0FET0E7RUFPRSxhQUFBO0VBQ0EsZ0JBQUE7RUFFQSxrQkFBQTtBQ1hGOztBREVFO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0FDQUo7O0FEU0k7RUFDRSxnQkFBQTtBQ1BOOztBRFVFO0VBQ0UsV0FBQTtBQ1JKOztBRFdFO0VBQ0Msa0JBQUE7RUFDQyxlQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNUSjs7QURhRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtBQ1hKOztBRGFJO0VBQ0UsY0FBQTtFQUNBLFdBL0xDO0VBZ01ELFlBaE1DO0VBaU1ELFdBQUE7QUNYTjs7QURjSTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNaTjs7QURjTTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQ1pSOztBRGlCRTtFQUNFLGdCQUFBO0FDZko7O0FEa0JFO0VBQ0Usa0JBQUE7QUNoQko7O0FEa0JJO0VBQ0UsMEJBQUE7QUNoQk47O0FEbUJJO0VBQ0UsZUFBQTtFQUNBLDZCQUFBO0VBQ0EsaUJBQUE7QUNqQk47O0FEb0JJO0VBQ0UsdUJBQUE7RUFDQSxtQkFBQTtBQ2xCTjs7QURxQkk7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsNkJBQUE7VUFBQSxxQkFBQTtBQ25CTjs7QURzQkk7RUFDRSxhQUFBO0FDcEJOOztBRHdCRTtFQUNFLDhCQUFBO0FDdEJKOztBRHlCRTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtBQ3ZCSjs7QUQyQkE7RUFDRSxZQUFBO0VBQ0EsNkJBQUE7RUFDQSw4QkFBQTtBQ3hCRjs7QUR5QkU7RUFDRSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDdkJKOztBRDJCQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBQ3hCRjs7QUQyQkE7RUFDRSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUN4QkY7O0FEMEJFO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQ3hCSjs7QUQyQkU7RUFDRSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7QUN6Qko7O0FEMkJJO0VBQ0UsOEJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDekJOOztBRDZCRTtFQUNFLFlBQUE7QUMzQko7O0FEK0JBO0VBQ0UsYUFBQTtBQzVCRjs7QUQrQkE7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FDNUJKOztBRGdDQTtFQUNFLGtCQUFBO0FDN0JGOztBRGdDQTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7QUM3Qko7O0FEZ0NBO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUM3QkY7O0FEaUNBO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSw0QkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQ0FBQTtFQUNBLHFCQUFBO0FDOUJGOztBRGlDQTtFQUNFLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUNBLFNBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsNEJBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUVBLHFCQUFBO0FDL0JGOztBRGtDQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFFQSxVQUFBO0VBQ0EsNEVBQUE7RUFDQSxrQ0FBQTtVQUFBLDBCQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLGtCQUFBO0FDaENGOztBRG1DQSxxQkFBQTs7QUFDQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDaENGOztBRG1DQTs7RUFFRSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0FDaENGOztBRHFDQTtFQU9jO0lBQ0UsWUFBQTtFQ3hDZDtFRDRDZ0I7SUFDRSxlQUFBO0VDMUNsQjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9wcm9maWxlL3Byb2ZpbGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkc2l6ZTogMTB2dztcclxuXHJcbmFnbS1tYXAge1xyXG4gIGhlaWdodDogMzAwcHg7XHJcbn1cclxuLmltYWdlLXVwbG9hZC1ncm91cCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcclxuICAuaW1nLXVwbG9hZCB7XHJcbiAgICB3aWR0aDogY2FsYygoMTAwdncgLSAzMDBweCkgLyAyKTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGhlaWdodDogY2FsYygoMTAwdncgLSAzMDBweCkgLyAyKTtcclxuICAgID4gLmNhcmQge1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICA+IC5jYXJkLXRpdGxle1xyXG4gICAgICAgIGgye1xyXG4gICAgICAgICAgZm9udC1zaXplOiAyNHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICA+IC5jYXJkLWNvbnRlbnQge1xyXG4gICAgICAgIGhlaWdodDogY2FsYygxMDAlIC0gODJweCk7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgICAgICAuaW1hZ2Uge1xyXG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgIGhlaWdodDogY2FsYygxMDAlIC0gMjBweCk7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIC5jdXN0b20ge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDIwcHgpO1xyXG4gICAgICAgICAgICBtYXJnaW46IDIwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC4wMyk7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMnB4IHNvbGlkICNlYmViZWI7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICAgICAgICAuc2V0LW9wYWNpdHkge1xyXG4gICAgICAgICAgICAgIG9wYWNpdHk6IDAuMTtcclxuICAgICAgICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgPiAuZm9ybS1ncm91cC51cGxvYWRlZHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIGhlaWdodDogY2FsYygxMDAlIC0gODJweCk7XHJcbiAgICAgICAgLnVwbG9hZGVkLWltYWdle1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICAgIC5pbWd7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAuZWRpdG9yLXRvb2x7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLmltZy11cGxvYWQ6bGFzdC1jaGlsZCB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTZweDtcclxuICB9XHJcbn1cclxuXHJcbiNidXR0b24tdmVyaWZ5e1xyXG4gIC8vIGJhY2tncm91bmQtY29sb3I6IzAwYmZmZjtcclxuICB3aWR0aDogMTAwJTtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGhlaWdodDogNTBweDtcclxuICBmb250LXNpemU6MjBweDtcclxufVxyXG5cclxuLmNoZWNrLWJ1dHRvbntcclxuICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcblxyXG59XHJcbi5kaXNwbGF5LW5vbmV7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLm1vZGFsLWhlYWRlcntcclxuICBib3JkZXI6bm9uZTtcclxufVxyXG5cclxuLmNoZWNrLWJ1dHRvbiB0cnVle1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Ymx1ZTtcclxuICBjb2xvcjp3aGl0ZTtcclxufVxyXG5cclxuI2xpbmV7XHJcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkIGJsYWNrO1xyXG4gIHdpZHRoOjExNzVweDtcclxufVxyXG5cclxuLnNoaWVsZHtcclxuICB3aWR0aDo3JTtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OjEwcHg7XHJcbn1cclxuXHJcbi5zZWN1cml0eS1jb250ZW50e1xyXG4gIG1hcmdpbi10b3A6MjBweDtcclxuICBtYXJnaW4tbGVmdDoyMHB4O1xyXG59XHJcblxyXG4uY2hlY2stYnV0dG9uIGZhbHNle1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Ymx1ZTtcclxuICBjb2xvcjp3aGl0ZTtcclxufVxyXG5cclxuLmZvcm0tYm9keSB7XHJcbiAgLm9yLWdyb3VwIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIC5jb2wtbWQtNiB7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgfVxyXG4gICAgLmNvbC1tZC02Omxhc3QtY2hpbGR7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLmJsdWUtZmlsbCB7XHJcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwNjlkOTtcclxuICBiYWNrZ3JvdW5kOiAjMjQ4MGZiO1xyXG59XHJcbi5ibHVlLWZpbGw6aG92ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDY5ZDk7XHJcbn1cclxuLmNvdXJpZXItc2VydmljZSB7XHJcbiAgLm1hdC1jaGVja2JveDpub3QoOmZpcnN0LWNoaWxkKSB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICB9XHJcbn1cclxuLmJ0bi5zYXZlIHtcclxuICBtYXJnaW46IDE1cHggMTVweCAwcHggMHB4O1xyXG59XHJcbi5mb3JtIHtcclxuICAudGl0bGUtZGlmZiB7XHJcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMzMzO1xyXG4gIH1cclxuXHJcbiAgcGFkZGluZzogMjVweDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIC8vIHRvcDogNXZ3O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgLmJyb3dzZS1maWxlcyB7XHJcbiAgICAuZmlsZXVwbG9hZCB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDI1cHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5lcnJvci1tZXNzYWdle1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgfVxyXG5cclxuICAuY2FyZC5lcnJvcntcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNCU7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG5cclxuICB9XHJcblxyXG4gIC5oZWFkLWZvcm0ge1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGhlaWdodDogMTF2dztcclxuXHJcbiAgICAucGhvdG8tcHJvZmlsZSB7XHJcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICB3aWR0aDogJHNpemU7XHJcbiAgICAgIGhlaWdodDogJHNpemU7XHJcbiAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgfVxyXG5cclxuICAgIC5kZXRhaWwge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICBmbG9hdDogbGVmdDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgIGhlaWdodDogNjVweDtcclxuICAgICAgbGluZS1oZWlnaHQ6IDE3cHg7XHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgbWFyZ2luLXRvcDogM3Z3O1xyXG4gICAgICBtYXJnaW4tbGVmdDogMnZ3O1xyXG5cclxuICAgICAgLm5hbWUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjdweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmVkaXRvci1ncm91cCB7XHJcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xyXG4gIH1cclxuXHJcbiAgLmZvcm0tZ3JvdXAge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgIGxhYmVsIHtcclxuICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICB9XHJcblxyXG4gICAgLmZvcm0tY29udHJvbCB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtLWNvbnRyb2wudHJ1ZSB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtLWNvbnRyb2wgfiAuaWNvbmljIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICByaWdodDogMTBweDtcclxuICAgICAgdG9wOiAzN3B4O1xyXG4gICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgICB9XHJcblxyXG4gICAgLmRlc2NyaXB0aW9uIHtcclxuICAgICAgaGVpZ2h0OiAxMTBweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC52bCB7XHJcbiAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNlYWVhZWE7XHJcbiAgfVxyXG5cclxuICBpbWcucmVzaXplIHtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIG1heC1oZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG59XHJcblxyXG4uY2FyZC10aXRsZSB7XHJcbiAgbWFyZ2luOiAyMHB4O1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMzMzO1xyXG4gIGJvcmRlci1sZWZ0OiA1cHggc29saWQgIzhmZDZmZjtcclxuICBoMiB7XHJcbiAgICBtYXJnaW46IDBweCAwcHggNXB4IDVweDtcclxuICAgIGZvbnQtc2l6ZTogMjNweDtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICB9XHJcbn1cclxuXHJcbi5jaGVjayB7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIHBhZGRpbmctbGVmdDogMTJweDtcclxufVxyXG5cclxuLnVwbG9hZGVkLWltYWdlIHtcclxuICBtYXJnaW46IDBweCBhdXRvO1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIHBhZGRpbmc6IDUwcHg7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuXHJcbiAgLnRleHQtZmlsZXVwbG9hZCB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGNvbG9yOiAjMjQ4MGZiO1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICB9XHJcblxyXG4gIC5pbWcge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcblxyXG4gICAgLmVkaXRvci10b29sIHtcclxuICAgICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpO1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIG9wYWNpdHk6IDA7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmltZzpob3ZlciAuZWRpdG9yLXRvb2wge1xyXG4gICAgb3BhY2l0eTogMC45O1xyXG4gIH1cclxufVxyXG5cclxuaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi5waG90byB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIGJvdHRvbTogMDtcclxufVxyXG5cclxuXHJcbi5pbWFnZS1wYXJ0e1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLmJhY2tncm91bmQge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAwcHg7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbn1cclxuXHJcbi5jb250ZW50e1xyXG4gIHBhZGRpbmctdG9wOjEwcHg7XHJcbiAgZm9udC1zaXplOjE4cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbn1cclxuXHJcblxyXG4ubW9kYWwuZmFsc2Uge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbiAgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xyXG4gIHotaW5kZXg6IDE7XHJcbiAgLyogU2l0IG9uIHRvcCAqL1xyXG4gIHBhZGRpbmctdG9wOiAxMDBweDtcclxuICAvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXHJcbiAgbGVmdDogMDtcclxuICB0b3A6IDA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgLyogRnVsbCB3aWR0aCAqL1xyXG4gIGhlaWdodDogMTAwJTtcclxuICAvKiBGdWxsIGhlaWdodCAqL1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG4gIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDAsIDAsIDApO1xyXG4gIC8qIEZhbGxiYWNrIGNvbG9yICovXHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xyXG4gIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cclxufVxyXG5cclxuLm1vZGFsLnRydWUge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIC8qIFN0YXkgaW4gcGxhY2UgKi9cclxuICB6LWluZGV4OiAxO1xyXG4gIC8qIFNpdCBvbiB0b3AgKi9cclxuICBwYWRkaW5nLXRvcDogMTAwcHg7XHJcbiAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xyXG4gIGxlZnQ6IDBweDtcclxuICB0b3A6IDA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgLyogRnVsbCB3aWR0aCAqL1xyXG4gIGhlaWdodDogMTAwJTtcclxuICAvKiBGdWxsIGhlaWdodCAqL1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG4gIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgb3BhY2l0eTogMTtcclxuICAvKiBGYWxsYmFjayBjb2xvciAqL1xyXG4gIC8vIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cclxufVxyXG5cclxuLm1vZGFsLWNvbnRlbnQge1xyXG4gIG1hcmdpbi1sZWZ0OiAyNTZweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcclxuICBtYXJnaW46IGF1dG87XHJcbiAgbWFyZ2luLXRvcDoyMDBweDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIC8vIGJvcmRlcjogMXB4IHNvbGlkICM4ODg7XHJcbiAgd2lkdGg6IDMwJTtcclxuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xyXG4gIGFuaW1hdGlvbi1uYW1lOiBhbmltYXRldG9wO1xyXG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi8qIFRoZSBDbG9zZSBCdXR0b24gKi9cclxuLmNsb3NlIHtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG4gIGZvbnQtc2l6ZTogMjhweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuLmNsb3NlOmhvdmVyLFxyXG4uY2xvc2U6Zm9jdXMge1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcblxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDg1MHB4KSB7XHJcbiAgLmltYWdlLXVwbG9hZC1ncm91cCB7XHJcbiAgICAuaW1nLXVwbG9hZCB7XHJcbiAgICAgID4gLmNhcmQge1xyXG4gICAgICAgID4gLmNhcmQtY29udGVudCB7XHJcbiAgICAgICAgICAuaW1hZ2Uge1xyXG4gICAgICAgICAgICAuY3VzdG9tIHtcclxuICAgICAgICAgICAgICBpbWcuc2V0LW9wYWNpdHkge1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAuYnJvd3NlLWZpbGVze1xyXG4gICAgICAgICAgICAgICAgLnBiLWZyYW1lcntcclxuICAgICAgICAgICAgICAgICAgLmNvbW1vbi1idXR0b257XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogOHB4O1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiYWdtLW1hcCB7XG4gIGhlaWdodDogMzAwcHg7XG59XG5cbi5pbWFnZS11cGxvYWQtZ3JvdXAge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcbn1cbi5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQge1xuICB3aWR0aDogY2FsYygoMTAwdncgLSAzMDBweCkgLyAyKTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IGNhbGMoKDEwMHZ3IC0gMzAwcHgpIC8gMik7XG59XG4uaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkID4gLmNhcmQge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAuaW1nLXVwbG9hZCA+IC5jYXJkID4gLmNhcmQtdGl0bGUgaDIge1xuICBmb250LXNpemU6IDI0cHg7XG59XG4uaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkID4gLmNhcmQgPiAuY2FyZC1jb250ZW50IHtcbiAgaGVpZ2h0OiBjYWxjKDEwMCUgLSA4MnB4KTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAuaW1nLXVwbG9hZCA+IC5jYXJkID4gLmNhcmQtY29udGVudCAuaW1hZ2Uge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZmxvYXQ6IGxlZnQ7XG4gIGhlaWdodDogY2FsYygxMDAlIC0gMjBweCk7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAuaW1nLXVwbG9hZCA+IC5jYXJkID4gLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSB7XG4gIGhlaWdodDogY2FsYygxMDAlIC0gMjBweCk7XG4gIG1hcmdpbjogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjAzKTtcbiAgYm9yZGVyOiAycHggc29saWQgI2ViZWJlYjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIHBhZGRpbmc6IDQwcHg7XG59XG4uaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkID4gLmNhcmQgPiAuY2FyZC1jb250ZW50IC5pbWFnZSAuY3VzdG9tIC5zZXQtb3BhY2l0eSB7XG4gIG9wYWNpdHk6IDAuMTtcbiAgaGVpZ2h0OiA2MHB4O1xuICB3aWR0aDogYXV0bztcbn1cbi5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQgPiAuY2FyZCA+IC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogY2FsYygxMDAlIC0gODJweCk7XG59XG4uaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkID4gLmNhcmQgPiAuZm9ybS1ncm91cC51cGxvYWRlZCAudXBsb2FkZWQtaW1hZ2Uge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBhZGRpbmc6IDIwcHg7XG59XG4uaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkID4gLmNhcmQgPiAuZm9ybS1ncm91cC51cGxvYWRlZCAudXBsb2FkZWQtaW1hZ2UgLmltZyB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQgPiAuY2FyZCA+IC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZSAuZWRpdG9yLXRvb2wge1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkOmxhc3QtY2hpbGQge1xuICBtYXJnaW4tbGVmdDogMTZweDtcbn1cblxuI2J1dHRvbi12ZXJpZnkge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBib3JkZXI6IG5vbmU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi5jaGVjay1idXR0b24ge1xuICBtYXJnaW4tbGVmdDogNXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuLmRpc3BsYXktbm9uZSB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5tb2RhbC1oZWFkZXIge1xuICBib3JkZXI6IG5vbmU7XG59XG5cbi5jaGVjay1idXR0b24gdHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Ymx1ZTtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4jbGluZSB7XG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCBibGFjaztcbiAgd2lkdGg6IDExNzVweDtcbn1cblxuLnNoaWVsZCB7XG4gIHdpZHRoOiA3JTtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi5zZWN1cml0eS1jb250ZW50IHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5cbi5jaGVjay1idXR0b24gZmFsc2Uge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGJsdWU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmZvcm0tYm9keSAub3ItZ3JvdXAge1xuICBtYXJnaW4tbGVmdDogMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbn1cbi5mb3JtLWJvZHkgLm9yLWdyb3VwIC5jb2wtbWQtNiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xufVxuLmZvcm0tYm9keSAub3ItZ3JvdXAgLmNvbC1tZC02Omxhc3QtY2hpbGQge1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG5cbi5ibHVlLWZpbGwge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwNjlkOTtcbiAgYmFja2dyb3VuZDogIzI0ODBmYjtcbn1cblxuLmJsdWUtZmlsbDpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDY5ZDk7XG59XG5cbi5jb3VyaWVyLXNlcnZpY2UgLm1hdC1jaGVja2JveDpub3QoOmZpcnN0LWNoaWxkKSB7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xufVxuXG4uYnRuLnNhdmUge1xuICBtYXJnaW46IDE1cHggMTVweCAwcHggMHB4O1xufVxuXG4uZm9ybSB7XG4gIHBhZGRpbmc6IDI1cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5mb3JtIC50aXRsZS1kaWZmIHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMzMzM7XG59XG4uZm9ybSAuYnJvd3NlLWZpbGVzIC5maWxldXBsb2FkIHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbn1cbi5mb3JtIC5lcnJvci1tZXNzYWdlIHtcbiAgZmxvYXQ6IGxlZnQ7XG59XG4uZm9ybSAuY2FyZC5lcnJvciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDUwJTtcbiAgbWFyZ2luLWxlZnQ6IDI0JTtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5mb3JtIC5oZWFkLWZvcm0ge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBoZWlnaHQ6IDExdnc7XG59XG4uZm9ybSAuaGVhZC1mb3JtIC5waG90by1wcm9maWxlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMHZ3O1xuICBoZWlnaHQ6IDEwdnc7XG4gIGZsb2F0OiBsZWZ0O1xufVxuLmZvcm0gLmhlYWQtZm9ybSAuZGV0YWlsIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgZmxvYXQ6IGxlZnQ7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBoZWlnaHQ6IDY1cHg7XG4gIGxpbmUtaGVpZ2h0OiAxN3B4O1xuICBwYWRkaW5nOiA1cHg7XG4gIG1hcmdpbi10b3A6IDN2dztcbiAgbWFyZ2luLWxlZnQ6IDJ2dztcbn1cbi5mb3JtIC5oZWFkLWZvcm0gLmRldGFpbCAubmFtZSB7XG4gIGZvbnQtc2l6ZTogMjdweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uZm9ybSAuZWRpdG9yLWdyb3VwIHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbn1cbi5mb3JtIC5mb3JtLWdyb3VwIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmZvcm0gLmZvcm0tZ3JvdXAgbGFiZWwge1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cbi5mb3JtIC5mb3JtLWdyb3VwIC5mb3JtLWNvbnRyb2wge1xuICBmb250LXNpemU6IDEycHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mb3JtIC5mb3JtLWdyb3VwIC5mb3JtLWNvbnRyb2wudHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuLmZvcm0gLmZvcm0tZ3JvdXAgLmZvcm0tY29udHJvbCB+IC5pY29uaWMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxMHB4O1xuICB0b3A6IDM3cHg7XG4gIHRyYW5zZm9ybTogc2NhbGUoMC43KTtcbn1cbi5mb3JtIC5mb3JtLWdyb3VwIC5kZXNjcmlwdGlvbiB7XG4gIGhlaWdodDogMTEwcHg7XG59XG4uZm9ybSAudmwge1xuICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNlYWVhZWE7XG59XG4uZm9ybSBpbWcucmVzaXplIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBtYXgtaGVpZ2h0OiAxMDAlO1xufVxuXG4uY2FyZC10aXRsZSB7XG4gIG1hcmdpbjogMjBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMzMzM7XG4gIGJvcmRlci1sZWZ0OiA1cHggc29saWQgIzhmZDZmZjtcbn1cbi5jYXJkLXRpdGxlIGgyIHtcbiAgbWFyZ2luOiAwcHggMHB4IDVweCA1cHg7XG4gIGZvbnQtc2l6ZTogMjNweDtcbiAgcGFkZGluZzogNXB4O1xufVxuXG4uY2hlY2sge1xuICBmb250LXNpemU6IDEycHg7XG4gIHBhZGRpbmctbGVmdDogMTJweDtcbn1cblxuLnVwbG9hZGVkLWltYWdlIHtcbiAgbWFyZ2luOiAwcHggYXV0bztcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmc6IDUwcHg7XG4gIGhlaWdodDogYXV0bztcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLnVwbG9hZGVkLWltYWdlIC50ZXh0LWZpbGV1cGxvYWQge1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGNvbG9yOiAjMjQ4MGZiO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLnVwbG9hZGVkLWltYWdlIC5pbWcge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xufVxuLnVwbG9hZGVkLWltYWdlIC5pbWcgLmVkaXRvci10b29sIHtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG9wYWNpdHk6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnVwbG9hZGVkLWltYWdlIC5pbWc6aG92ZXIgLmVkaXRvci10b29sIHtcbiAgb3BhY2l0eTogMC45O1xufVxuXG5pbnB1dFt0eXBlPWZpbGVdIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLnBob3RvIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgYm90dG9tOiAwO1xufVxuXG4uaW1hZ2UtcGFydCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmJhY2tncm91bmQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMHB4O1xuICBsZWZ0OiAwcHg7XG59XG5cbi5jb250ZW50IHtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDE4cHg7XG59XG5cbi5tb2RhbC5mYWxzZSB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xuICB6LWluZGV4OiAxO1xuICAvKiBTaXQgb24gdG9wICovXG4gIHBhZGRpbmctdG9wOiAxMDBweDtcbiAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xuICBsZWZ0OiAwO1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICAvKiBGdWxsIHdpZHRoICovXG4gIGhlaWdodDogMTAwJTtcbiAgLyogRnVsbCBoZWlnaHQgKi9cbiAgb3ZlcmZsb3c6IGF1dG87XG4gIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICAvKiBGYWxsYmFjayBjb2xvciAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cbn1cblxuLm1vZGFsLnRydWUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cbiAgcG9zaXRpb246IGZpeGVkO1xuICAvKiBTdGF5IGluIHBsYWNlICovXG4gIHotaW5kZXg6IDE7XG4gIC8qIFNpdCBvbiB0b3AgKi9cbiAgcGFkZGluZy10b3A6IDEwMHB4O1xuICAvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXG4gIGxlZnQ6IDBweDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgLyogRnVsbCB3aWR0aCAqL1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIEZ1bGwgaGVpZ2h0ICovXG4gIG92ZXJmbG93OiBhdXRvO1xuICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgb3BhY2l0eTogMTtcbiAgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgLyogQmxhY2sgdy8gb3BhY2l0eSAqL1xufVxuXG4ubW9kYWwtY29udGVudCB7XG4gIG1hcmdpbi1sZWZ0OiAyNTZweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmVmZWZlO1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi10b3A6IDIwMHB4O1xuICBwYWRkaW5nOiAwO1xuICB3aWR0aDogMzAlO1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICBhbmltYXRpb24tbmFtZTogYW5pbWF0ZXRvcDtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjRzO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi8qIFRoZSBDbG9zZSBCdXR0b24gKi9cbi5jbG9zZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDI4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uY2xvc2U6aG92ZXIsXG4uY2xvc2U6Zm9jdXMge1xuICBjb2xvcjogIzAwMDtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA4NTBweCkge1xuICAuaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkID4gLmNhcmQgPiAuY2FyZC1jb250ZW50IC5pbWFnZSAuY3VzdG9tIGltZy5zZXQtb3BhY2l0eSB7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICB9XG4gIC5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQgPiAuY2FyZCA+IC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20gLmJyb3dzZS1maWxlcyAucGItZnJhbWVyIC5jb21tb24tYnV0dG9uIHtcbiAgICBtYXJnaW4tdG9wOiA4cHg7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/profile/profile.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/merchant-portal/profile/profile.component.ts ***!
  \*********************************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../services/merchant/merchant.service */ "./src/app/services/merchant/merchant.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_courier_courier_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/courier/courier.service */ "./src/app/services/courier/courier.service.ts");
/* harmony import */ var _services_country_country_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/country/country.service */ "./src/app/services/country/country.service.ts");
/* harmony import */ var _services_paymentgateway_paymentgateway_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/paymentgateway/paymentgateway.service */ "./src/app/services/paymentgateway/paymentgateway.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/fesm2015/agm-core.js");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(memberService, countryService, courierService, paymentGatewayService, mapsAPILoader, ngZone, router, route, merchantService) {
        this.memberService = memberService;
        this.countryService = countryService;
        this.courierService = courierService;
        this.paymentGatewayService = paymentGatewayService;
        this.mapsAPILoader = mapsAPILoader;
        this.ngZone = ngZone;
        this.router = router;
        this.route = route;
        this.merchantService = merchantService;
        this.latlongs = {};
        this.latlong = {};
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4___default.a;
        this.config = {
            toolbar: ['heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote'],
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
                ]
            }
        };
        this.errorLabel = false;
        this.autoFill = true;
        this.complete = false;
        this.edit = false;
        this.swaper = [{ name: 'Profile Information', val: true }, { name: 'Store Information', val: false }];
        this.merchant_group = [];
        this.available_courier = [];
        this.bgUrl = '';
        this.countryList = [];
        this.pin = {
            password: '',
            new_pin: '',
            repeat_new_pin: ''
        };
        this.showMap = false;
        this.map_active = false;
        this.form = {
            cell_phone: '',
            dob: '',
            email: '',
            full_name: '',
            gender: 'male',
            merchant_name: '',
            merchant_username: '',
            description: '',
            image_owner_url: '',
            background_image: '',
            mcountry: 'Indonesia',
            mcity: '',
            mstate: '',
            mprovince: '',
            mdistrict: '',
            mpostal_code: '',
            region_code: '',
            mprovince_code: '',
            mcity_code: '',
            msubdistrict_code: '',
            mvillage_code: '',
            mvillage: '',
            msubdistrict: '',
            address: '',
            latitude: '',
            longitude: '',
            google_address: '',
            maddress: '',
            contact_person: '',
            email_address: '',
            work_phone: '',
            handphone: '',
            fax_number: '',
            active: 0,
            _id: '',
            slogan: '',
            available_courier: []
        };
        this.formEditable = {};
        this.courierServices = [];
        this.couriers = [];
        this.showModal = false;
        this.setPin = false;
        this.successPin = false;
        this.retypePin = false;
        this.passwordField = false;
        this.othersForm = false;
        this.regionList = [];
        this.provinceList = [];
        this.cityList = [];
        this.bankList = [];
        this.subDistrictList = [];
        this.villageList = [];
        this.merchantGroup = [];
        this.openModal = false;
        this.hide = true;
        this.selectedFile = null;
        this.selectedFile2 = null;
        this.cancel = false;
        this.hasToko = false;
        this.bukaTokoBtn = true;
        this.upload = false;
        this.addMerchantDetail = false;
        this.storeInfoBtn = true;
        this.storeProfile = false;
        this.open = false;
        this.openStore = false;
        this.keyname = [];
        this.activeCheckbox = false;
        this.checkBox = [];
    }
    ProfileComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ProfileComponent.prototype.getCountryList = function () {
        var n = this.countryService.getCountryList();
        var r = [];
        n.forEach(function (element) {
            r.push({ value: element, name: element });
        });
        this.countryList = r;
        this.form.mcountry = 'Indonesia';
    };
    ProfileComponent.prototype.swapClick = function (index) {
        this.swaper.forEach(function (e) {
            e.val = false;
        });
        this.swaper[index].val = true;
    };
    ProfileComponent.prototype.editableForm = function (label, val) {
        this.formEditable[label] = val;
    };
    ProfileComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var member, data, member_1, data, i;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.memberService.getMemberDetail()];
                    case 1:
                        member = _a.sent();
                        console.log("member", member);
                        if (member.result) {
                            data = member.result;
                            this.form = __assign({}, this.form, data);
                        }
                        if (!(member.result.permission == 'merchant')) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.merchantService.getDetail()];
                    case 2:
                        member_1 = _a.sent();
                        data = member_1.result;
                        console.log("data", data);
                        this.form = __assign({}, this.form, data);
                        _a.label = 3;
                    case 3:
                        for (i = 0; i < this.form.available_courier.length; i++) {
                            this.courier += this.form.available_courier[i];
                        }
                        // console.log("text", this.courier, 'available cour', this.form.available_courier)
                        this.zoom = 4;
                        if (this.form.latitude != '' && this.form.longitude != '') {
                            this.map_active = true;
                            this.showMap = true;
                            this.latitude = this.form.latitude;
                            this.longitude = this.form.longitude;
                        }
                        //create search FormControl
                        this.searchControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormControl"]();
                        this.geoCoder = new google.maps.Geocoder;
                        //set current position
                        // this.setCurrentLocation();
                        //load Places Autocomplete
                        this.mapsAPILoader.load().then(function () {
                            var autocomplete = new google.maps.places.Autocomplete(_this.searchElementRef.nativeElement, {
                                types: [],
                                componentRestrictions: { 'country': 'ID' }
                            });
                            autocomplete.addListener("place_changed", function () {
                                _this.ngZone.run(function () {
                                    //get the place result
                                    var place = autocomplete.getPlace();
                                    //verify result
                                    if (place.geometry === undefined || place.geometry === null) {
                                        return;
                                    }
                                    //set latitude, longitude and zoom
                                    _this.latitude = place.geometry.location.lat();
                                    _this.longitude = place.geometry.location.lng();
                                    _this.zoom = 12;
                                    _this.map_active = true;
                                    _this.showMap = false;
                                    _this.getAddress(_this.latitude, _this.longitude);
                                });
                            });
                        });
                        this.getAddress(this.latitude, this.longitude);
                        if (this.form.withdrawal_bank_name.account_name) {
                            this.form.withdrawal_account_name = this.form.withdrawal_bank_name.account_name;
                            delete this.form.withdrawal_bank_name.account_name;
                            console.log(this.form);
                        }
                        return [4 /*yield*/, this.getCountryList()];
                    case 4:
                        _a.sent();
                        // await this.getProvinceList();
                        // await this.getKeyname();
                        console.log(this.form);
                        // this.getCourier();
                        return [4 /*yield*/, this.checkCourier(true)];
                    case 5:
                        // this.getCourier();
                        _a.sent();
                        return [4 /*yield*/, this.getBankList()];
                    case 6:
                        _a.sent();
                        // console.log(this.getBankList);
                        if (this.form.available_courier && this.form.available_courier.length) {
                            this.form.available_courier.forEach(function (value) {
                                console.log('value', value);
                                _this.setCourierService(value);
                            });
                        }
                        console.log('coureiers', this.couriers);
                        return [2 /*return*/];
                }
            });
        });
    };
    // Get Current Location Coordinates
    ProfileComponent.prototype.setCurrentLocation = function () {
        var _this = this;
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                _this.latitude = position.coords.latitude;
                _this.longitude = position.coords.longitude;
                _this.zoom = 8;
                _this.getAddress(_this.latitude, _this.longitude);
            });
        }
    };
    ProfileComponent.prototype.markerDragEnd = function ($event) {
        // console.log(' event',$event.latLng.lat());
        this.latitude = $event.latLng.lat();
        this.longitude = $event.latLng.lng();
        this.getAddress(this.latitude, this.longitude);
    };
    ProfileComponent.prototype.getAddress = function (latitude, longitude) {
        var _this = this;
        this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, function (results, status) {
            //   console.log(results);
            //   console.log(status);
            if (status === 'OK') {
                if (results[0]) {
                    _this.zoom = 12;
                    _this.address = results[0].formatted_address;
                }
                else {
                    window.alert('No results found');
                }
            }
            else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    };
    ProfileComponent.prototype.ngAfterViewInit = function () {
        // this.getCurrentPosition();
        // this.findAdress();
    };
    ProfileComponent.prototype.findAdress = function () {
        var _this = this;
        this.mapsAPILoader.load().then(function () {
            var autocomplete = new google.maps.places.Autocomplete(_this.searchElementRef.nativeElement);
            autocomplete.addListener("place_changed", function () {
                _this.ngZone.run(function () {
                    // some details
                    _this.map_active = true;
                    var place = autocomplete.getPlace();
                    //set latitude, longitude and zoom
                    _this.latitude2 = place.geometry.location.lat();
                    _this.longitude2 = place.geometry.location.lng();
                    _this.zoom = 12;
                });
            });
        });
    };
    // async getCourier() {
    // 	let required = {
    // 		province_code: this.form.mprovince_code,
    // 		city_code: this.form.mcity_code,
    // 		subdistrict_code: this.form.mdistrict_code,
    // 		village_code: this.form.mvillage_code
    // 	}
    // 	console.log("required", required)
    // 	let result = await this.courierService.getCourier(required);
    // 	this.courierServices = result.result;
    // 	this.courierServices.forEach((el, i) => {
    // 		Object.assign(el.courier, { value: false });
    // 	});
    // }
    ProfileComponent.prototype.getBankList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('bank');
                        this.form.withdrawal_bank_name = JSON.stringify(this.form.withdrawal_bank_name);
                        console.log(this.form.withdrawal_bank_name, " HEREEEEEE");
                        this.bankList = [];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.paymentGatewayService.getWithdrawalBankList()];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        if (result.result) {
                            this.bankList.push({ name: 'choose your bank', value: 'none' });
                            result.result.forEach(function (element) {
                                _this.bankList.push({ name: element.bank_name, value: JSON.stringify(element) });
                            });
                        }
                        if (!this.form.withdrawal_bank_name || this.form.withdrawal_bank_name == '') {
                            this.form.withdrawal_bank_name = this.bankList[0].value;
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        console.log((e_1.message));
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ProfileComponent.prototype.setCourierService = function (value) {
        for (var n in this.couriers.result) {
            if (value == this.couriers.result[n].courier_code) {
                this.couriers.result[n].value = true;
            }
        }
    };
    ProfileComponent.prototype.datePickerOnDateSelection = function (date) {
        console.log(date, date.day);
    };
    ProfileComponent.prototype.maxLength = function ($event) {
        var value = this.form.mpostal_code;
        if (value.length > 5) {
            console.log(value.length);
            this.form.mpostal_code = parseInt(value.toString().substring(0, 2));
            // this.form.mpostal_code = parseInt(value.toString().substring(0,2));
        }
        // console.log("test", value);
    };
    ProfileComponent.prototype.getValueCheckbox = function () {
        var getData = [];
        this.couriers.result.forEach(function (el, i) {
            if (el.value == true) {
                getData.push(el);
            }
        });
        console.log("courier", getData);
        return getData;
    };
    ProfileComponent.prototype.checkCourier = function (load) {
        return __awaiter(this, void 0, void 0, function () {
            var props, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        props = {
                            province_code: this.form.mprovince_code,
                            city_code: this.form.mcity_code,
                            subdistrict_code: this.form.msubdistrict_code,
                            village_code: this.form.mvillage_code
                        };
                        _a = this;
                        return [4 /*yield*/, this.courierService.getCourier(props)];
                    case 1:
                        _a.couriers = _b.sent();
                        if (load == false) {
                            if (!this.couriers.result) {
                                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire("Warning", "Your area is not supported by courier services", "warning");
                            }
                            else {
                                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire("Success", "Please choose available couriers", "success");
                            }
                        }
                        console.log("courier", this.couriers);
                        return [2 /*return*/];
                }
            });
        });
    };
    // onChecked(){
    //     let getData = [];
    //     this.courierServices.forEach((el) =>{
    //         var data = this.courierServices;
    //         console.log("result", data)
    //     })
    // }
    // async getKeyname() {
    // 	this.merchantGroup = await this.merchantService.configuration();
    // 	this.merchantGroup.result.merchant_group.forEach((element) => {
    // 		this.merchant_group.push({ label: element.name, value: element.name });
    // 	});
    // 	// this.form.merchant_group = this.merchant_group[0].value
    // }
    ProfileComponent.prototype.saveThis = function () {
        var data = this.formValidation();
        if (data != false) {
            this.proceedToSave();
        }
    };
    ProfileComponent.prototype.showMaps = function () {
        this.showMap = true;
        this.form.google_address = this.address,
            this.form.latitude = this.latitude,
            this.form.longitude = this.longitude,
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire("Success", "Your Pickup Address Has Been Pointed on Google Map", "success");
    };
    ProfileComponent.prototype.closeMaps = function () {
        this.showMap = false;
        this.form.google_address = this.address = '',
            this.form.latitude = '',
            this.form.longitude = '',
            this.latitude = this.longitude = 0,
            this.map_active = false,
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire("Warning", "Your Pickup Address Has Been Deleted", "warning");
    };
    ProfileComponent.prototype.saveMerchant = function () {
        return __awaiter(this, void 0, void 0, function () {
            var courier, _i, _a, n, data, save, result, valid, resultMerchant, result_1, e_2;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.errorMessage = '';
                        courier = [];
                        console.log(" DOB ", this.form.dob);
                        for (_i = 0, _a = this.couriers.result; _i < _a.length; _i++) {
                            n = _a[_i];
                            if (n.value == true) {
                                courier.push(n.courier_code);
                            }
                        }
                        this.form.available_courier = courier;
                        if (this.showMap == false) {
                            delete this.form.latitude;
                            delete this.form.longitude;
                            delete this.form.google_address;
                        }
                        data = JSON.parse(JSON.stringify(this.form));
                        save = {
                            gender: this.form.gender
                        };
                        data.mpostal_code = data.mpostal_code.toString();
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 6, , 7]);
                        result = void 0;
                        if (this.form.withdrawal_bank_name && this.form.withdrawal_bank_name != "none") {
                            data.withdrawal_bank_name = JSON.parse(this.form.withdrawal_bank_name);
                            if (data.withdrawal_account_name) {
                                data.withdrawal_bank_name.account_name = data.withdrawal_account_name;
                                delete data.withdrawal_account_name;
                            }
                        }
                        else {
                            data.withdrawal_bank_name = '';
                        }
                        data.available_courier = this.getValueCheckbox();
                        if (!(this.form.permission == 'member')) return [3 /*break*/, 3];
                        valid = this.formValidation();
                        return [4 /*yield*/, this.merchantService.addMerchant(data)];
                    case 2:
                        resultMerchant = _b.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        if (!(this.form.permission == 'merchant')) return [3 /*break*/, 5];
                        console.log(typeof this.form.mpostal_code);
                        console.log(data);
                        return [4 /*yield*/, this.merchantService.updateMerchant(data)];
                    case 4:
                        result_1 = _b.sent();
                        _b.label = 5;
                    case 5:
                        sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire("Success", "Your profile has been updated", "success");
                        return [3 /*break*/, 7];
                    case 6:
                        e_2 = _b.sent();
                        setTimeout(function () {
                            _this.errorMessage = (e_2.message);
                        }, 500);
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    ProfileComponent.prototype.savePayment = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, save, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = JSON.parse(JSON.stringify(this.form));
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.merchantService.updateMerchant(data)];
                    case 2:
                        save = _a.sent();
                        if (save.result) {
                            alert("your profile has been saved");
                        }
                        console.log("result", save);
                        return [3 /*break*/, 4];
                    case 3:
                        e_3 = _a.sent();
                        this.errorMessage = (e_3.message);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ProfileComponent.prototype.proceedToSave = function () {
        return __awaiter(this, void 0, void 0, function () {
            var profile, res, e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.errorMessage = '';
                        profile = {
                            address: this.form.google_address,
                            cell_phone: this.form.cell_phone,
                            dob: this.form.dob,
                            full_name: this.form.full_name,
                            gender: this.form.gender,
                            email: this.form.email
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.memberService.updateProfile(profile)];
                    case 2:
                        res = _a.sent();
                        if (res.result) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire("Success", "Your profile has been updated", "success");
                        }
                        console.log("result", res);
                        return [3 /*break*/, 4];
                    case 3:
                        e_4 = _a.sent();
                        this.errorMessage = (e_4.message);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ProfileComponent.prototype.nameInput = function ($event) {
        // if (this.autoFill == true) {
        // 	let fill = this.form.merchant_name.toLowerCase().replace(/[\s\?\*]/g, '-');
        // 	this.form.merchant_username = fill;
        // }
    };
    ProfileComponent.prototype.formValidation = function () {
        console.log(this.form);
        return true;
    };
    ProfileComponent.prototype.openUploadLegalDocument = function () {
        this.upload = true;
        this.bukaTokoBtn = false;
    };
    ProfileComponent.prototype.openAddMerchantDetail = function () {
        this.upload = false;
        this.addMerchantDetail = true;
    };
    ProfileComponent.prototype.openStoreInfo = function () {
        this.openStore = true;
        this.storeInfoBtn = false;
    };
    ProfileComponent.prototype.openStoreProfile = function () {
        this.openStore = false;
        this.storeProfile = true;
    };
    ProfileComponent.prototype.getSubDistrictList = function ($event) {
        var _this = this;
        var result = $event.target.value;
        console.log('ini biang kerok', $event);
        if (result.length > 2) {
            this.memberService.getSubDistrict(result, result, 'origin').then(function (result) {
                _this.subDistrictList = result.result;
                console.log('INI PROVINCE', _this.subDistrictList);
            });
        }
    };
    ProfileComponent.prototype.getProvinceList = function ($event) {
        var _this = this;
        var result = $event.target.value;
        if (result.length > 2) {
            this.memberService.getProvince(result, 'origin').then(function (result) {
                _this.provinceList = result.result;
                console.log('hasil reg', _this.provinceList);
                _this.onProvinceChange();
            });
        }
    };
    ProfileComponent.prototype.onProvinceChange = function () {
        var _this = this;
        console.log("province", this.form.mprovince, this.provinceList);
        this.form.mcity = this.form.mcity_code = this.form.msubdistrict = this.form.msubdistrict_code = this.form.mvillage = this.form.mvillage_code = '';
        this.provinceList.forEach(function (element) {
            if (_this.form.mprovince.toLowerCase() == element.province.toLowerCase()) {
                _this.form.mprovince_code = element.province_code;
            }
            console.log("code", _this.form.mprovince_code);
        });
    };
    // getCityList($event) {
    // 	let result = $event.target.value;
    // 	const payload = {
    // 		province_code: this.form.mprovince_code,
    // 		city: result
    // 	}
    // 	if (result.length > 2) {
    // 		this.memberService.getCity(result, origin, this.form.mprovince_code).then((result) => {
    // 			console.log(payload)
    // 			this.cityList = result.result;
    // 			console.log('hasil city', this.cityList)
    // 			this.onCityChange();
    // 		})
    // 	}
    // }
    // onCityChange() {
    // 	this.cityList.forEach((element) => {
    // 		if (this.form.mcity.toLowerCase() == element.city.toLowerCase()) {
    // 			this.form.mcity_code = element.city_code;
    // 		}
    // 	})
    // }
    ProfileComponent.prototype.getCityList = function ($event) {
        var _this = this;
        console.log('sekarang di city list');
        var province_code = this.form.mprovince_code;
        var city = $event.target.value;
        console.log();
        if (city.length > 2) {
            this.memberService.getCity(city, 'origin', province_code).then(function (result) {
                _this.cityList = result.result;
                console.log('INI City', _this.cityList);
                _this.onCityChange();
            });
        }
    };
    ProfileComponent.prototype.onCityChange = function () {
        var _this = this;
        this.form.msubdistrict = this.form.msubdistrict_code = this.form.mvillage = this.form.mvillage_code = '';
        console.log('ON City Changed', this.form.mcity, this.cityList);
        this.cityList.forEach(function (element) {
            if (_this.form.mcity.toLowerCase() == element.city.toLowerCase()) {
                _this.form.city_code = _this.form.mcity_code = element.city_code;
            }
        });
        console.log("kode kota", this.form.city_code);
    };
    // getSubDistList($event) {
    // 	let result = $event.target.value
    // 	if (result.length > 2) {
    // 		this.memberService.getSubDistrict(result, origin, this.form.mcity_code).then((result) => {
    // 			this.subDistrictList = result.result
    // 			this.onSubChange();
    // 		})
    // 		console.log("district", this.subDistrictList)
    // 		console.log("dist", this.formEditable)
    // 	}
    // }
    // onSubChange() {
    // 	this.subDistrictList.forEach((element) => {
    // 		if (this.form.msubdistrict.toLowerCase() == element.subdistrict.toLowerCase()) {
    // 			this.form.msubdistrict_code = element.subdistrict_code
    // 		}
    // 	})
    // }
    ProfileComponent.prototype.getSubDistList = function ($event) {
        var _this = this;
        var city_code = this.form.mcity_code;
        var result = $event.target.value;
        console.log('ini biang kerok', $event);
        if (result.length > 2) {
            this.memberService.getSubDistrict(result, 'origin', city_code).then(function (result) {
                _this.subDistrictList = result.result;
                console.log('INI Subdistrict', _this.subDistrictList);
                _this.onSubDistrictChange();
            });
        }
    };
    ProfileComponent.prototype.onSubDistrictChange = function () {
        var _this = this;
        this.form.mvillage = this.form.mvillage_code = '';
        console.log('ON Subdistrict Changed', this.form.msubdistrict, this.subDistrictList);
        this.subDistrictList.forEach(function (element) {
            if (_this.form.msubdistrict.toLowerCase() == element.subdistrict.toLowerCase()) {
                _this.form.msubdistrict_code = _this.form.msubdistrict_code = element.subdistrict_code;
            }
        });
        console.log("kode subdistrict", this.form.mcity_code);
    };
    ProfileComponent.prototype.getVillageList = function ($event) {
        var _this = this;
        var result = $event.target.value;
        if (result.length > 2) {
            this.memberService.getVillage(result, 'origin', this.form.msubdistrict_code).then(function (result) {
                _this.villageList = result.result;
            });
        }
        console.log("village", this.villageList);
    };
    ProfileComponent.prototype.onVillageChange = function () {
        var _this = this;
        this.villageList.forEach(function (element) {
            if (_this.form.mvillage.toLowerCase() == element.village.toLowerCase()) {
                _this.form.mvillage_code = element.village_code;
                _this.checkForm();
            }
        });
    };
    ProfileComponent.prototype.getRegionList = function ($event) {
        var _this = this;
        var result = $event.target.value;
        console.log("ini contoh ", result);
        if (result.length > 2)
            this.memberService.getRegion(result, 'origin').then(function (result) {
                _this.regionList = result.result;
                console.log('hasil reg', _this.regionList);
                _this.onRegionChange();
            });
    };
    ProfileComponent.prototype.onRegionChange = function () {
        var _this = this;
        console.log('ON Region Changed', this.form.region_name, this.regionList);
        this.regionList.forEach(function (element) {
            if (_this.form.region_name.toLowerCase() == element.region_name.toLowerCase()) {
                _this.form.region_code = element.region_code;
            }
        });
        console.log(this.form.region_code);
    };
    ProfileComponent.prototype.dateformater = function (params) {
        var res = params.substring(0, 10);
        console.log('this is form.dob', params, res);
        return res;
    };
    ProfileComponent.prototype.onFileSelected = function (event, img) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_1, logo, e_5;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_1 = 3000000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_1) {
                                _this.errorFile = 'Maximum File Upload is 3MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this.selFile = event.target.result;
                        };
                        reader.readAsDataURL(event.target.files[0]);
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.merchantService.upload(this.selectedFile, this, 'image', function (result) {
                                console.log("hasil", result);
                                _this.callImage(result, img);
                            })];
                    case 2:
                        logo = _a.sent();
                        _a.label = 3;
                    case 3:
                        console.log("test lah", this.callImage);
                        return [3 /*break*/, 5];
                    case 4:
                        e_5 = _a.sent();
                        this.errorLabel = e_5.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    ProfileComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    ProfileComponent.prototype.callImage = function (result, img) {
        console.log('result upload logo ', result);
        this.form[img] = result.base_url + result.pic_big_path;
        console.log('This detail', this.form);
        // this.detail = {...this.detail, ...result}
    };
    // maxlength(){
    // }
    ProfileComponent.prototype.verifyPin = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log("tes");
                return [2 /*return*/];
            });
        });
    };
    // regex(event){
    // 	this.pin.new_pin = this.pin.new_pin.replace(/d/g);
    // 	console.log("regex jalan")
    // }
    ProfileComponent.prototype.numeric = function (event) {
        var ch = String.fromCharCode(event.which);
        if (!(/[0-9]/).test(ch)) {
            event.preventDefault();
        }
    };
    // nextStep(){
    // 	this.retypePin = true;
    // }
    ProfileComponent.prototype.verify = function () {
        return __awaiter(this, void 0, void 0, function () {
            var settingPin, password, newPin, repeatNewPin, result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        settingPin = this.pin;
                        password = settingPin.password.toString();
                        newPin = settingPin.new_pin.toString();
                        repeatNewPin = settingPin.repeat_new_pin.toString();
                        // settingPin.password = settingPin.password.toString();
                        // console.log(settingPin)
                        console.log(password);
                        console.log(newPin);
                        console.log(repeatNewPin);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        if (!newPin) {
                            alert("Input Your Pin");
                        }
                        else if (newPin) {
                            this.retypePin = true;
                        }
                        else if (!repeatNewPin) {
                            alert("Retype your pin");
                        }
                        else if (newPin != repeatNewPin) {
                            alert("Your pin not matched");
                        }
                        else if (repeatNewPin == newPin) {
                            this.passwordField = true;
                        }
                        return [4 /*yield*/, this.memberService.setNewPin(settingPin)];
                    case 2:
                        result = _a.sent();
                        if (result.result) {
                            if (this.form.access_pin == 'enabled') {
                                // Swal.fire("Congratulations", "Your new PIN has been registered to LOCARD", "success");
                                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
                                    title: 'Congratulation',
                                    html: 'Your new PIN has been registered to LOCARD',
                                    imageUrl: 'assets/images/check.png',
                                    imageWidth: 120,
                                    imageHeight: 120
                                });
                            }
                            else {
                                // Swal.fire("Congratulations", "Your PIN has been registered to LOCARD", "success");
                                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
                                    title: 'Congratulation',
                                    html: 'Your PIN has been registered to LOCARD',
                                    imageUrl: 'assets/images/check.png',
                                    imageWidth: 120,
                                    imageHeight: 120
                                });
                            }
                            this.retypePin = false;
                            this.setPin = false;
                            // this.router.navigate(['merchant-portal/profile']);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        console.log((error_1.message));
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ProfileComponent.prototype.checkForm = function () {
        if (this.form.mprovince_code && this.form.mcity_code && this.form.msubdistrict_code && this.form.mvillage_code) {
            this.complete = true;
            this.checkCourier(false);
        }
        else {
            this.complete = false;
        }
    };
    ProfileComponent.prototype.openDialog = function () {
        // this.showModal = true;
        if (this.setPin == false) {
            console.log("berhasil");
            this.setPin = true;
        }
        else {
            console.log("error");
        }
    };
    ProfileComponent.prototype.closeDialog = function () {
        this.setPin = false;
        this.retypePin = false;
        this.passwordField = false;
        this.successPin = false;
    };
    ProfileComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_country_country_service__WEBPACK_IMPORTED_MODULE_6__["CountryService"] },
        { type: _services_courier_courier_service__WEBPACK_IMPORTED_MODULE_5__["CourierServiceService"] },
        { type: _services_paymentgateway_paymentgateway_service__WEBPACK_IMPORTED_MODULE_7__["PaymentGatewayService"] },
        { type: _agm_core__WEBPACK_IMPORTED_MODULE_10__["MapsAPILoader"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_0__["MerchantService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('search', { static: false }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ProfileComponent.prototype, "searchElementRef", void 0);
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! raw-loader!./profile.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.scss */ "./src/app/layout/merchant-portal/profile/profile.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"],
            _services_country_country_service__WEBPACK_IMPORTED_MODULE_6__["CountryService"],
            _services_courier_courier_service__WEBPACK_IMPORTED_MODULE_5__["CourierServiceService"],
            _services_paymentgateway_paymentgateway_service__WEBPACK_IMPORTED_MODULE_7__["PaymentGatewayService"],
            _agm_core__WEBPACK_IMPORTED_MODULE_10__["MapsAPILoader"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_0__["MerchantService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/merchant-products.component.scss":
/*!*************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/merchant-products.component.scss ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  position: absolute;\n  width: 100%;\n  left: 0px;\n  font-family: \"Poppins\";\n}\n\n*:focus {\n  box-shadow: unset;\n}\n\n.col-md-12 {\n  padding: 3vh 3vw;\n}\n\n.col-md-12 > .card {\n  padding: 3vh 3vw;\n}\n\n.option-container {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  flex-direction: row;\n}\n\ndiv,\nspan:not(.fa) {\n  font-family: \"Poppins\";\n}\n\n._container {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.nav.toolbar {\n  background: white;\n  color: black;\n  height: 35px;\n  padding: 0px 8px;\n  vertical-align: middle;\n  margin-bottom: 30px;\n  justify-content: space-between;\n}\n\n.nav.toolbar .search {\n  margin-right: 20px;\n}\n\n.nav.toolbar .search input ::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: red;\n  opacity: 1;\n  /* Firefox */\n}\n\n.nav.toolbar .search input ::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: red;\n  opacity: 1;\n  /* Firefox */\n}\n\n.nav.toolbar .search input :-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: red;\n  opacity: 1;\n  /* Firefox */\n}\n\n.nav.toolbar .search input ::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: red;\n  opacity: 1;\n  /* Firefox */\n}\n\n.nav.toolbar .search input ::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: red;\n  opacity: 1;\n  /* Firefox */\n}\n\n.nav.toolbar .search input :-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: red;\n}\n\n.nav.toolbar .search input ::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: red;\n}\n\n.nav.toolbar .container-swapper-option {\n  display: flex;\n  justify-content: center;\n  flex-direction: row;\n  align-items: center;\n}\n\n.nav.toolbar .container-swapper-option .button-container {\n  -webkit-animation: all 0.25s cubic-bezier(0.31, -0.105, 0.43, 1.4);\n          animation: all 0.25s cubic-bezier(0.31, -0.105, 0.43, 1.4);\n  border: 2px solid #ddd;\n  border-radius: 8px;\n}\n\n.nav.toolbar .container-swapper-option .button-container button {\n  color: #ddd;\n  background: white;\n  font-size: 14px;\n  border-radius: 8px;\n}\n\n.nav.toolbar .container-swapper-option .button-container button.true {\n  background: #2480fb;\n  border-radius: 8px;\n  color: #ddd;\n}\n\n.grid-view {\n  position: relative;\n  width: 100%;\n  height: 100%;\n}\n\n.grid-view .col-md-2 {\n  cursor: pointer;\n  width: 100%;\n  display: inline-block;\n  position: relative;\n  padding: 12px 8px;\n}\n\n.grid-view .col-md-2 .card {\n  height: 300px;\n  padding: 29px 3px;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n}\n\n.grid-view .col-md-2 .card .img-wrapper {\n  display: flex;\n  position: relative;\n  width: 100%;\n  height: calc(100% - 50px);\n  overflow: hidden;\n}\n\n.grid-view .col-md-2 .card .img-wrapper .img {\n  display: flex;\n  background-position: center center;\n  background-size: cover;\n  width: 100%;\n  height: 100%;\n  justify-content: center;\n  align-content: center;\n  align-items: center;\n  -webkit-transform: scale(1);\n          transform: scale(1);\n  transition: ease 0.5s all 0s;\n}\n\n.grid-view .col-md-2 .card .info {\n  padding: 5px 10px;\n}\n\n.grid-view .col-md-2 .card .info .price {\n  text-overflow: ellipsis;\n  overflow: hidden;\n  font-weight: bold;\n  font-size: 14px;\n  text-align: left;\n  color: #2480fb;\n  white-space: nowrap;\n}\n\n.grid-view .col-md-2 .card .info .text {\n  text-overflow: ellipsis;\n  overflow: hidden;\n  font-size: 14px;\n  text-align: left;\n  font-weight: bold;\n  white-space: nowrap;\n}\n\n.grid-view .grid-view-item:hover .img-wrapper .img {\n  -webkit-transform: scale(1.2);\n          transform: scale(1.2);\n  transition: ease 1s all 0s;\n}\n\n.grid-view-pagination {\n  position: relative;\n  margin-top: 50px;\n  align-self: center;\n  display: inline-block;\n}\n\n.grid-view-pagination select {\n  margin-right: 10px;\n}\n\n@media (min-width: 769px) and (max-width: 1074px) {\n  .grid-view {\n    position: relative;\n    width: 100%;\n    height: 100%;\n  }\n  .grid-view .grid-view-item {\n    width: calc(25vw - 56px);\n    height: calc(25vw - 56px + 50px);\n  }\n}\n\n@media (min-width: 460px) and (max-width: 769px) {\n  .grid-view {\n    position: relative;\n    width: 100%;\n    height: 100%;\n  }\n  .grid-view .grid-view-item {\n    width: calc(33.333vw - 73px);\n    height: calc(33.333vw - 73px + 50px);\n  }\n}\n\n@media (min-width: 320px) and (max-width: 460px) {\n  .grid-view {\n    position: relative;\n    width: 100%;\n    height: 100%;\n  }\n  .grid-view .grid-view-item {\n    width: calc(50vw - 107px);\n    height: calc(50vw - 107px + 50px);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9zZXBhcmF0ZWQtbW9kdWxlcy9tZXJjaGFudC1wcm9kdWN0cy9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1lcmNoYW50LXBvcnRhbFxcc2VwYXJhdGVkLW1vZHVsZXNcXG1lcmNoYW50LXByb2R1Y3RzXFxtZXJjaGFudC1wcm9kdWN0cy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9zZXBhcmF0ZWQtbW9kdWxlcy9tZXJjaGFudC1wcm9kdWN0cy9tZXJjaGFudC1wcm9kdWN0cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxzQkFBQTtBQ0NGOztBREtBO0VBQ0UsaUJBQUE7QUNGRjs7QURLQTtFQUNFLGdCQUFBO0FDRkY7O0FER0U7RUFDRSxnQkFBQTtBQ0RKOztBREtBO0VBQ0UsYUFBQTtFQUNFLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0ZKOztBRElBOztFQUVFLHNCQUFBO0FDREY7O0FER0E7RUFDRSxhQUFBO0VBQ0Usc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDQUo7O0FER0E7RUFDRSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUNBRjs7QURDRTtFQUNFLGtCQUFBO0FDQ0o7O0FEQ1E7RUFBZ0IseUNBQUE7RUFDWixVQUFBO0VBQ0EsVUFBQTtFQUFZLFlBQUE7QUNHeEI7O0FETFE7RUFBZ0IseUNBQUE7RUFDWixVQUFBO0VBQ0EsVUFBQTtFQUFZLFlBQUE7QUNHeEI7O0FETFE7RUFBZ0IseUNBQUE7RUFDWixVQUFBO0VBQ0EsVUFBQTtFQUFZLFlBQUE7QUNHeEI7O0FETFE7RUFBZ0IseUNBQUE7RUFDWixVQUFBO0VBQ0EsVUFBQTtFQUFZLFlBQUE7QUNHeEI7O0FETFE7RUFBZ0IseUNBQUE7RUFDWixVQUFBO0VBQ0EsVUFBQTtFQUFZLFlBQUE7QUNHeEI7O0FEQVU7RUFBeUIsNEJBQUE7RUFDdkIsVUFBQTtBQ0daOztBREFVO0VBQTBCLG1CQUFBO0VBQ3hCLFVBQUE7QUNHWjs7QURDRTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNDSjs7QURBSTtFQUNJLGtFQTVESztVQTRETCwwREE1REs7RUE2RFAsc0JBQUE7RUFDQSxrQkFBQTtBQ0VOOztBRERNO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDR1I7O0FERE07RUFFRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0VSOztBRElBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0RGOztBREVFO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNBSjs7QURDSTtFQUNFLGFBQUE7RUFDQSxpQkFBQTtFQUdGLDRFQUFBO0FDQ0o7O0FEQUk7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtBQ0VOOztBREFNO0VBQ0UsYUFBQTtFQUNBLGtDQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO1VBQUEsbUJBQUE7RUFDQSw0QkFBQTtBQ0VSOztBREVJO0VBQ0UsaUJBQUE7QUNBTjs7QURHTTtFQUNFLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQ0RSOztBREdNO0VBQ0UsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNEUjs7QURRTTtFQUNFLDZCQUFBO1VBQUEscUJBQUE7RUFDQSwwQkFBQTtBQ05SOztBRFlBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0YscUJBQUE7QUNUQTs7QURXRTtFQUNFLGtCQUFBO0FDVEo7O0FEYUE7RUFDRTtJQUNFLGtCQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7RUNWRjtFRFdFO0lBQ0Usd0JBQUE7SUFDQSxnQ0FBQTtFQ1RKO0FBQ0Y7O0FEYUE7RUFDRTtJQUNFLGtCQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7RUNYRjtFRFlFO0lBQ0UsNEJBQUE7SUFDQSxvQ0FBQTtFQ1ZKO0FBQ0Y7O0FEY0E7RUFDRTtJQUNFLGtCQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7RUNaRjtFRGFFO0lBQ0UseUJBQUE7SUFDQSxpQ0FBQTtFQ1hKO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3NlcGFyYXRlZC1tb2R1bGVzL21lcmNoYW50LXByb2R1Y3RzL21lcmNoYW50LXByb2R1Y3RzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBsZWZ0OiAwcHg7XHJcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xyXG4gIC8vIHRvcDogNXZ3O1xyXG59XHJcblxyXG4kc3BlZWQ6IFwiMC4yNXNcIjtcclxuJHRyYW5zaXRpb246IGFsbCAjeyRzcGVlZH0gY3ViaWMtYmV6aWVyKDAuMzEwLCAtMC4xMDUsIDAuNDMwLCAxLjQwMCk7XHJcbio6Zm9jdXMge1xyXG4gIGJveC1zaGFkb3c6IHVuc2V0O1xyXG4gIFxyXG59XHJcbi5jb2wtbWQtMTIge1xyXG4gIHBhZGRpbmc6IDN2aCAzdnc7XHJcbiAgPiAuY2FyZCB7XHJcbiAgICBwYWRkaW5nOiAzdmggM3Z3O1xyXG4gICAgXHJcbiAgfVxyXG59XHJcbi5vcHRpb24tY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbn1cclxuZGl2LFxyXG5zcGFuOm5vdCguZmEpIHtcclxuICBmb250LWZhbWlseTogXCJQb3BwaW5zXCI7XHJcbn1cclxuLl9jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4ubmF2LnRvb2xiYXIge1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBoZWlnaHQ6IDM1cHg7XHJcbiAgcGFkZGluZzogMHB4IDhweDtcclxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIC5zZWFyY2gge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gICAgICBpbnB1dHtcclxuICAgICAgICA6OnBsYWNlaG9sZGVyIHsgLyogQ2hyb21lLCBGaXJlZm94LCBPcGVyYSwgU2FmYXJpIDEwLjErICovXHJcbiAgICAgICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAgICAgIG9wYWNpdHk6IDE7IC8qIEZpcmVmb3ggKi9cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgICAgOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIEludGVybmV0IEV4cGxvcmVyIDEwLTExICovXHJcbiAgICAgICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBcclxuICAgICAgICAgIDo6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHsgLyogTWljcm9zb2Z0IEVkZ2UgKi9cclxuICAgICAgICAgICAgY29sb3I6IHJlZDtcclxuICAgICAgICAgIH1cclxuICAgICAgfVxyXG4gIH1cclxuICAuY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgLmJ1dHRvbi1jb250YWluZXIge1xyXG4gICAgICAgIGFuaW1hdGlvbjogJHRyYW5zaXRpb247XHJcbiAgICAgIGJvcmRlcjogMnB4IHNvbGlkICNkZGQ7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6OHB4O1xyXG4gICAgICBidXR0b24ge1xyXG4gICAgICAgIGNvbG9yOiAjZGRkO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgIH1cclxuICAgICAgYnV0dG9uLnRydWUge1xyXG4gICAgICAgXHJcbiAgICAgICAgYmFja2dyb3VuZDogIzI0ODBmYjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgICAgY29sb3I6ICNkZGQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5ncmlkLXZpZXcge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgLmNvbC1tZC0yIHtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgcGFkZGluZyA6IDEycHggOHB4O1xyXG4gICAgLmNhcmQge1xyXG4gICAgICBoZWlnaHQ6IDMwMHB4O1xyXG4gICAgICBwYWRkaW5nOiAyOXB4IDNweDtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcclxuICAgIC1tb3otYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcclxuICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XHJcbiAgICAuaW1nLXdyYXBwZXIge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDUwcHgpO1xyXG4gICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG5cclxuICAgICAgLmltZyB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMSk7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogZWFzZSAwLjVzIGFsbCAwcztcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5pbmZvIHtcclxuICAgICAgcGFkZGluZzogNXB4IDEwcHg7XHJcblxyXG4gICAgICBcclxuICAgICAgLnByaWNlIHtcclxuICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuOyBcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICBjb2xvciA6ICMyNDgwZmI7XHJcbiAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgfVxyXG4gICAgICAudGV4dCB7XHJcbiAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIH1cclxuICAuZ3JpZC12aWV3LWl0ZW06aG92ZXIge1xyXG4gICAgLmltZy13cmFwcGVyIHtcclxuICAgICAgLmltZyB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxLjIpO1xyXG4gICAgICAgIHRyYW5zaXRpb246IGVhc2UgMXMgYWxsIDBzO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4uZ3JpZC12aWV3LXBhZ2luYXRpb24ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBtYXJnaW4tdG9wIDogNTBweDtcclxuICBhbGlnbi1zZWxmOiBjZW50ZXI7XHJcbmRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHJcbiAgc2VsZWN0IHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA3NjlweCkgYW5kIChtYXgtd2lkdGg6IDEwNzRweCkge1xyXG4gIC5ncmlkLXZpZXcge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAuZ3JpZC12aWV3LWl0ZW0ge1xyXG4gICAgICB3aWR0aDogY2FsYygyNXZ3IC0gNTZweCk7XHJcbiAgICAgIGhlaWdodDogY2FsYygyNXZ3IC0gNTZweCArIDUwcHgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDQ2MHB4KSBhbmQgKG1heC13aWR0aDogNzY5cHgpIHtcclxuICAuZ3JpZC12aWV3IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLmdyaWQtdmlldy1pdGVtIHtcclxuICAgICAgd2lkdGg6IGNhbGMoMzMuMzMzdncgLSA3M3B4KTtcclxuICAgICAgaGVpZ2h0OiBjYWxjKDMzLjMzM3Z3IC0gNzNweCArIDUwcHgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDMyMHB4KSBhbmQgKG1heC13aWR0aDogNDYwcHgpIHtcclxuICAuZ3JpZC12aWV3IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLmdyaWQtdmlldy1pdGVtIHtcclxuICAgICAgd2lkdGg6IGNhbGMoNTB2dyAtIDEwN3B4KTtcclxuICAgICAgaGVpZ2h0OiBjYWxjKDUwdncgLSAxMDdweCArIDUwcHgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCI6aG9zdCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGxlZnQ6IDBweDtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xufVxuXG4qOmZvY3VzIHtcbiAgYm94LXNoYWRvdzogdW5zZXQ7XG59XG5cbi5jb2wtbWQtMTIge1xuICBwYWRkaW5nOiAzdmggM3Z3O1xufVxuLmNvbC1tZC0xMiA+IC5jYXJkIHtcbiAgcGFkZGluZzogM3ZoIDN2dztcbn1cblxuLm9wdGlvbi1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG59XG5cbmRpdixcbnNwYW46bm90KC5mYSkge1xuICBmb250LWZhbWlseTogXCJQb3BwaW5zXCI7XG59XG5cbi5fY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5uYXYudG9vbGJhciB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBjb2xvcjogYmxhY2s7XG4gIGhlaWdodDogMzVweDtcbiAgcGFkZGluZzogMHB4IDhweDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLm5hdi50b29sYmFyIC5zZWFyY2gge1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG4ubmF2LnRvb2xiYXIgLnNlYXJjaCBpbnB1dCA6OnBsYWNlaG9sZGVyIHtcbiAgLyogQ2hyb21lLCBGaXJlZm94LCBPcGVyYSwgU2FmYXJpIDEwLjErICovXG4gIGNvbG9yOiByZWQ7XG4gIG9wYWNpdHk6IDE7XG4gIC8qIEZpcmVmb3ggKi9cbn1cbi5uYXYudG9vbGJhciAuc2VhcmNoIGlucHV0IDotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xuICAvKiBJbnRlcm5ldCBFeHBsb3JlciAxMC0xMSAqL1xuICBjb2xvcjogcmVkO1xufVxuLm5hdi50b29sYmFyIC5zZWFyY2ggaW5wdXQgOjotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xuICAvKiBNaWNyb3NvZnQgRWRnZSAqL1xuICBjb2xvcjogcmVkO1xufVxuLm5hdi50b29sYmFyIC5jb250YWluZXItc3dhcHBlci1vcHRpb24ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5uYXYudG9vbGJhciAuY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIHtcbiAgYW5pbWF0aW9uOiBhbGwgMC4yNXMgY3ViaWMtYmV6aWVyKDAuMzEsIC0wLjEwNSwgMC40MywgMS40KTtcbiAgYm9yZGVyOiAycHggc29saWQgI2RkZDtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xufVxuLm5hdi50b29sYmFyIC5jb250YWluZXItc3dhcHBlci1vcHRpb24gLmJ1dHRvbi1jb250YWluZXIgYnV0dG9uIHtcbiAgY29sb3I6ICNkZGQ7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbn1cbi5uYXYudG9vbGJhciAuY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIGJ1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZDogIzI0ODBmYjtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBjb2xvcjogI2RkZDtcbn1cblxuLmdyaWQtdmlldyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5ncmlkLXZpZXcgLmNvbC1tZC0yIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHBhZGRpbmc6IDEycHggOHB4O1xufVxuLmdyaWQtdmlldyAuY29sLW1kLTIgLmNhcmQge1xuICBoZWlnaHQ6IDMwMHB4O1xuICBwYWRkaW5nOiAyOXB4IDNweDtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICAtbW96LWJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG59XG4uZ3JpZC12aWV3IC5jb2wtbWQtMiAuY2FyZCAuaW1nLXdyYXBwZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDUwcHgpO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmdyaWQtdmlldyAuY29sLW1kLTIgLmNhcmQgLmltZy13cmFwcGVyIC5pbWcge1xuICBkaXNwbGF5OiBmbGV4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xuICB0cmFuc2l0aW9uOiBlYXNlIDAuNXMgYWxsIDBzO1xufVxuLmdyaWQtdmlldyAuY29sLW1kLTIgLmNhcmQgLmluZm8ge1xuICBwYWRkaW5nOiA1cHggMTBweDtcbn1cbi5ncmlkLXZpZXcgLmNvbC1tZC0yIC5jYXJkIC5pbmZvIC5wcmljZSB7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogIzI0ODBmYjtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbn1cbi5ncmlkLXZpZXcgLmNvbC1tZC0yIC5jYXJkIC5pbmZvIC50ZXh0IHtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG4uZ3JpZC12aWV3IC5ncmlkLXZpZXctaXRlbTpob3ZlciAuaW1nLXdyYXBwZXIgLmltZyB7XG4gIHRyYW5zZm9ybTogc2NhbGUoMS4yKTtcbiAgdHJhbnNpdGlvbjogZWFzZSAxcyBhbGwgMHM7XG59XG5cbi5ncmlkLXZpZXctcGFnaW5hdGlvbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLXRvcDogNTBweDtcbiAgYWxpZ24tc2VsZjogY2VudGVyO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG4uZ3JpZC12aWV3LXBhZ2luYXRpb24gc2VsZWN0IHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY5cHgpIGFuZCAobWF4LXdpZHRoOiAxMDc0cHgpIHtcbiAgLmdyaWQtdmlldyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgfVxuICAuZ3JpZC12aWV3IC5ncmlkLXZpZXctaXRlbSB7XG4gICAgd2lkdGg6IGNhbGMoMjV2dyAtIDU2cHgpO1xuICAgIGhlaWdodDogY2FsYygyNXZ3IC0gNTZweCArIDUwcHgpO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNDYwcHgpIGFuZCAobWF4LXdpZHRoOiA3NjlweCkge1xuICAuZ3JpZC12aWV3IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICB9XG4gIC5ncmlkLXZpZXcgLmdyaWQtdmlldy1pdGVtIHtcbiAgICB3aWR0aDogY2FsYygzMy4zMzN2dyAtIDczcHgpO1xuICAgIGhlaWdodDogY2FsYygzMy4zMzN2dyAtIDczcHggKyA1MHB4KTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDMyMHB4KSBhbmQgKG1heC13aWR0aDogNDYwcHgpIHtcbiAgLmdyaWQtdmlldyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgfVxuICAuZ3JpZC12aWV3IC5ncmlkLXZpZXctaXRlbSB7XG4gICAgd2lkdGg6IGNhbGMoNTB2dyAtIDEwN3B4KTtcbiAgICBoZWlnaHQ6IGNhbGMoNTB2dyAtIDEwN3B4ICsgNTBweCk7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/merchant-products.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/merchant-products.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: MerchantProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantProductsComponent", function() { return MerchantProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _object_interface_common_object__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../object-interface/common.object */ "./src/app/object-interface/common.object.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
// Dear Future Developer, please consider leaving notes on your code, good habits will reduce development time ~ H
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var MerchantProductsComponent = /** @class */ (function () {
    function MerchantProductsComponent(productService, router) {
        this.productService = productService;
        this.router = router;
        this.Products = [];
        this.form_input = {};
        this.errorLabel = false;
        this.orderBy = "newest";
        this.filteredBy = "order_id";
        this.orderByResult = { order_date: -1 };
        this.allMemberDetail = false;
        this.swaper = true;
        this.loading = false;
        this.filterBy = [
            { name: 'Order ID', value: 'order_id' },
            { name: 'Buyer Name', value: 'buyer_name' },
            { name: 'Product Name', value: 'product_name' },
            { name: 'No. Resi', value: 'no_resi' }
        ];
        this.sortBy = [
            { name: 'Newest', value: 'newest' },
            { name: 'Highest Transactions', value: 'highest transactions' },
            { name: 'Lowest Transactions', value: 'lowest transactions' }
        ];
        this.page = 1;
        this.totalPage = 0;
        this.pageSize = 50;
        this.searchProduct = {};
        this.service = this.productService;
        this.tableFormat = Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_1__["productsTableFormat"])(this);
        console.log(this.tableFormat);
        this.tableFormat.label_headers.splice(0, 1); // Remove the Table Format Label for Merchant Username
        this.tableFormat.formOptions.addForm = false;
        delete this.tableFormat.formOptions.customButtons;
    }
    MerchantProductsComponent.prototype.callDetail = function (product_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.openDetail(product_id);
                return [2 /*return*/];
            });
        });
    };
    MerchantProductsComponent.prototype.openDetail = function (product_id) {
        this.router.navigate(['merchant-portal/product-list/detail'], { queryParams: { id: product_id } });
    };
    MerchantProductsComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
    };
    MerchantProductsComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MerchantProductsComponent.prototype.addProduct = function () {
        // console.log('add product clicked');
        this.router.navigate(['merchant-portal/add']);
    };
    MerchantProductsComponent.prototype.addProductBulk = function () {
        // console.log('add product clicked');
        this.router.navigate(['merchant-portal/report']);
    };
    MerchantProductsComponent.prototype.loadPage = function (page) {
        return __awaiter(this, void 0, void 0, function () {
            var search, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.loading = true;
                        if (!(page !== this.previousPage)) return [3 /*break*/, 4];
                        console.log('prev page', this.previousPage, page);
                        this.previousPage = page;
                        search = {
                            search: { type: 'product' },
                            order_by: null,
                            limit_per_page: 50,
                            current_page: page,
                            download: false
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.productService.searchProductsLint((search))];
                    case 2:
                        result = _a.sent();
                        if (result) {
                            this.Products = result.result.values;
                            console.log('product berubah ', this.Products);
                            this.loading = false;
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MerchantProductsComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var rQuery, request, result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        rQuery = {
                            column_request: "product_code,product_name,price,fixed_price,qty,active,type,approved,pic_small_path,pic_medium_path,pic_big_path,base_url",
                        };
                        request = JSON.stringify(rQuery);
                        result = void 0;
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.getProductsReport()];
                    case 1:
                        result = _a.sent();
                        console.log("isi produk", result);
                        this.valueAll = result.result.total_all_values;
                        this.Products = result.result.values;
                        this.totalPage = result.result.total_page;
                        console.log("result", this.Products);
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MerchantProductsComponent.prototype.search = function ($event) {
        return __awaiter(this, void 0, void 0, function () {
            var search, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        search = $event.target.value;
                        console.log("test", search);
                        this.searchProduct = {
                            current_page: 1,
                            download: false,
                            limit_per_page: 50,
                            order_by: {},
                            search: { product_name: search }
                        };
                        console.log("test");
                        return [4 /*yield*/, this.productService.searchProductsLint(this.searchProduct)];
                    case 1:
                        result = _a.sent();
                        this.Products = result.result.values;
                        console.log("tst", this.searchProduct);
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantProductsComponent.prototype.searchSortFilter = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var order_by, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('awdawd', params);
                        order_by = params;
                        this.searchProduct = {
                            current_page: 1,
                            download: false,
                            limit_per_page: 50,
                            order_by: order_by,
                            search: {}
                        };
                        console.log("test");
                        return [4 /*yield*/, this.productService.searchProductsLint(this.searchProduct)];
                    case 1:
                        result = _a.sent();
                        this.Products = result.result.values;
                        console.log("tst", this.searchProduct);
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantProductsComponent.prototype.orderBySelected = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                if (this.orderBy == "highest transactions") {
                    this.orderByResult = {
                        total_price: -1
                    };
                    this.searchSortFilter(this.orderByResult);
                    // this.valuechange({}, false, false)
                }
                else if (this.orderBy == "lowest transactions") {
                    this.orderByResult = {
                        total_price: 1
                    };
                    this.searchSortFilter(this.orderByResult);
                    // this.valuechange({}, false, false)
                }
                else if (this.orderBy == "newest") {
                    this.orderByResult = {
                        order_date: -1
                    };
                    this.searchSortFilter(this.orderByResult);
                    // this.valuechange({}, false, false)
                }
                return [2 /*return*/];
            });
        });
    };
    MerchantProductsComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    MerchantProductsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-products',
            template: __webpack_require__(/*! raw-loader!./merchant-products.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/merchant-products.component.html"),
            styles: [__webpack_require__(/*! ./merchant-products.component.scss */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/merchant-products.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], MerchantProductsComponent);
    return MerchantProductsComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/statistics/statistics.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/statistics/statistics.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  padding: 20px;\n  display: block;\n  position: relative;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9zdGF0aXN0aWNzL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbWVyY2hhbnQtcG9ydGFsXFxzdGF0aXN0aWNzXFxzdGF0aXN0aWNzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3N0YXRpc3RpY3Mvc3RhdGlzdGljcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSxjQUFBO0VBRUEsa0JBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvc3RhdGlzdGljcy9zdGF0aXN0aWNzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3R7XHJcbiAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAvLyB0b3A6IDV2dztcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufSIsIjpob3N0IHtcbiAgcGFkZGluZzogMjBweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/statistics/statistics.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/statistics/statistics.component.ts ***!
  \***************************************************************************/
/*! exports provided: StatisticsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatisticsComponent", function() { return StatisticsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StatisticsComponent = /** @class */ (function () {
    function StatisticsComponent() {
        this.permissionType = "merchant";
    }
    StatisticsComponent.prototype.ngOnInit = function () {
    };
    StatisticsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-statistics',
            template: __webpack_require__(/*! raw-loader!./statistics.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/statistics/statistics.component.html"),
            styles: [__webpack_require__(/*! ./statistics.component.scss */ "./src/app/layout/merchant-portal/statistics/statistics.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StatisticsComponent);
    return StatisticsComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/upgrade-genuine/upgrade-genuine.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/upgrade-genuine/upgrade-genuine.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  transition: margin-left 0.2s ease-in-out;\n  font-family: \"Poppins\", sans-serif;\n}\n\n.pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n\n.pb-framer .clr-white {\n  color: white;\n}\n\n.center-text {\n  margin-top: 10px;\n  margin-bottom: 10px;\n  text-align: center;\n}\n\n.genuine {\n  background-color: #fb9224;\n  color: white;\n  text-align: center;\n}\n\n.official {\n  background-color: #f45905;\n  color: white;\n  text-align: center;\n}\n\n.body-area {\n  background-color: #e7f1f9;\n  font-size: 10px;\n  color: black;\n  font-weight: bold;\n  height: 350px;\n}\n\n.body-area ul {\n  padding: 0;\n  margin: 0;\n  line-height: 2;\n}\n\n.body-area ul li {\n  background-image: url(\"/src/assets/images/icon_list.svg\") no-repeat left top;\n}\n\n.body-area .card-body-image {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.body-area .button-container button.btn.btn-2 {\n  width: calc(100% - 40px);\n  position: absolute;\n  bottom: 80px;\n  left: 20px;\n}\n\n.body-area .button-container button.btn.btn-1 {\n  width: calc(100% - 40px);\n  position: absolute;\n  bottom: 20px;\n  left: 20px;\n  border-radius: 5px;\n}\n\n.body-area .button-container button > img {\n  width: auto;\n  height: 30px;\n  padding-right: 5px;\n  padding-bottom: 5px;\n}\n\n.container-all {\n  background: #f8f8f8;\n  height: 100%;\n  width: 100%;\n  padding: 5vh 3vw;\n}\n\n.container-all .save-container {\n  margin: 1% 0%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.container-all .save-container .save {\n  width: 35%;\n  outline: none;\n  height: 60px;\n  color: white;\n  background-color: #ff7301;\n}\n\n.container-all .col-md-12 {\n  display: flex;\n  justify-content: flex-start;\n  flex-direction: column;\n  box-shadow: 0px 0px 16px -6px rgba(0, 0, 0, 0.5);\n  vertical-align: middle;\n  padding-bottom: 7vh;\n  background: white;\n  width: 100%;\n}\n\n.container-all .col-md-12 .title {\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n  margin-top: 2vh;\n  margin-left: 2vw;\n}\n\n.container-all .col-md-12 .title img {\n  width: 40px;\n}\n\n.container-all .col-md-12 .title .text {\n  font-size: 30px;\n  font-weight: bold;\n  margin-left: 10px;\n}\n\n.container-all .col-md-12 .section {\n  width: 100%;\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .section-header {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.container-all .col-md-12 .section .section-header .section-title {\n  width: 100%;\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n}\n\n.container-all .col-md-12 .section .left-section-upload {\n  justify-content: flex-start !important;\n}\n\n.container-all .col-md-12 .section .section-upload {\n  margin-top: 5vh;\n  width: 100%;\n  align-items: center;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 {\n  text-align: center;\n  font-size: 14px;\n  margin-top: 4vh;\n  margin-bottom: 4vh;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card {\n  margin-top: 3vh;\n  height: 300px;\n  border: none;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .card-content {\n  height: 100%;\n  position: relative;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .card-content .upload-desc {\n  color: #bbb !important;\n  text-align: center;\n  font-size: 14px;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .card-content .logo-upload {\n  width: 100px;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .card-content .image {\n  display: block;\n  float: left;\n  height: auto;\n  width: 100%;\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .card-content .image .custom {\n  height: 100%;\n  background-color: #eee;\n  border-radius: 10px;\n  border: 3px solid #ebebeb;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n  padding: 40px;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .card-content .image .custom .drag-drop {\n  font-size: 10px;\n  text-align: center;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .card-content .image .custom .set-opacity {\n  opacity: 0.1;\n  height: 60px;\n  width: auto;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .form-group.uploaded .loading {\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .form-group.uploaded .uploaded-image {\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .form-group.uploaded .uploaded-image .img {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .form-group.uploaded .uploaded-image .img > img {\n  width: 100%;\n  max-height: 320px;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-3 .card .form-group.uploaded .uploaded-image .editor-tool {\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 {\n  text-align: center;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card {\n  margin-top: 3vh;\n  height: 300px;\n  border: none;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .card-content {\n  height: 100%;\n  position: relative;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .card-content .upload-desc {\n  color: #bbb !important;\n  text-align: center;\n  font-size: 14px;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .card-content .logo-upload {\n  width: 100px;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .card-content .image {\n  display: block;\n  float: left;\n  height: auto;\n  width: 100%;\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .card-content .image .custom {\n  height: 100%;\n  background-color: #eee;\n  border-radius: 10px;\n  justify-content: center;\n  border: 3px solid #ebebeb;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n  padding: 40px;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .card-content .image .custom .drag-drop {\n  font-size: 10px;\n  text-align: center;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .card-content .image .custom .set-opacity {\n  opacity: 0.1;\n  height: 60px;\n  width: auto;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .form-group.uploaded .loading {\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .form-group.uploaded .uploaded-image {\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .form-group.uploaded .uploaded-image .img {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .form-group.uploaded .uploaded-image .img > img {\n  width: 100%;\n  max-height: 320px;\n}\n\n.container-all .col-md-12 .section .section-upload .col-md-4 .card .form-group.uploaded .uploaded-image .editor-tool {\n  height: 100%;\n}\n\n.container-all .col-md-12 .section .row {\n  justify-content: center;\n}\n\n.container-all .container-upgrade {\n  display: flex;\n  justify-content: center;\n  vertical-align: middle;\n  height: 100%;\n  padding: 5%;\n  background: white;\n  box-shadow: 0px 0px 16px -6px rgba(0, 0, 0, 0.5);\n}\n\n.container-all .container-upgrade .upgrade-page-bg {\n  width: 50%;\n}\n\n.container-all .container-upgrade .upgrade-page-bg > img {\n  width: 100%;\n  height: auto;\n}\n\n.container-all .container-upgrade .upgrade-page-success {\n  width: 40%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.container-all .container-upgrade .upgrade-page-success .image-success {\n  width: 40px;\n  margin-bottom: 20px;\n  height: auto;\n  align-self: center;\n}\n\n.container-all .container-upgrade .upgrade-page-success .header-message {\n  font-size: 1.5rem;\n  font-weight: bold;\n  text-align: center;\n  margin-bottom: 30px;\n}\n\n.container-all .container-upgrade .upgrade-page-success .body-message {\n  text-align: center;\n}\n\n.container-all .container-upgrade .upgrade-page-form {\n  width: 45%;\n  display: flex;\n  flex-direction: column;\n  justify-content: flex-start;\n}\n\n.container-all .container-upgrade .upgrade-page-form .form-title {\n  font-size: 1.5rem;\n  font-weight: bold;\n  text-align: center;\n  margin-bottom: 30px;\n}\n\n.container-all .container-upgrade .upgrade-page-form .upload-form {\n  border: 1px solid #ddd;\n  height: 100%;\n  border-radius: 20px;\n  padding: 20px;\n}\n\n.container-all .container-upgrade .upgrade-page-form .upload-form .col-md-6 .card {\n  border: none;\n}\n\n.container-all .container-upgrade .upgrade-page-form .upload-form .col-md-6 .card .card-header {\n  font-size: 13px;\n  border-top-left-radius: 10px;\n  border-top-right-radius: 10px;\n}\n\n.container-all .container-upgrade .upgrade-page-form .upload-form .col-md-6 .card .card-body {\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n}\n\n.container-all .container-upgrade .upgrade-page-form .upload-form button {\n  border: none;\n}\n\n.container-all .container-upgrade .upgrade-page-form .upload-form .upgrade-button {\n  width: 100%;\n  background: white;\n  color: blue;\n  font-weight: bold;\n  border: 3px solid blue;\n  border-radius: 40px;\n  height: 60px;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n}\n\n.container-all .container-upgrade .upgrade-page-form .upload-form .upgrade-button:hover {\n  width: 100%;\n  background: blue;\n  color: white;\n  cursor: pointer;\n  font-weight: bold;\n  border: 3px solid blue;\n  border-radius: 40px;\n  height: 60px;\n}\n\n@media (max-width: 1000px) {\n  .container-all .container-upload .title {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n  }\n  .container-all .container-upgrade .upgrade-page-bg {\n    display: none;\n  }\n  .container-all .container-upgrade .upgrade-page-form {\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC91cGdyYWRlLWdlbnVpbmUvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtZXJjaGFudC1wb3J0YWxcXHVwZ3JhZGUtZ2VudWluZVxcdXBncmFkZS1nZW51aW5lLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3VwZ3JhZGUtZ2VudWluZS91cGdyYWRlLWdlbnVpbmUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFLRSx3Q0FBQTtFQUNBLGtDQUFBO0FDQ0Y7O0FERUE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUNDRjs7QURBRTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDRUo7O0FEQUU7RUFDRSxxQkFBQTtBQ0VKOztBREFFO0VBQ0UsWUFBQTtBQ0VKOztBRENBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDRUY7O0FEQUE7RUFDRSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ0dGOztBRERBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNJRjs7QURGQTtFQUNFLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7QUNLRjs7QURKRTtFQUNFLFVBQUE7RUFDQSxTQUFBO0VBQ0EsY0FBQTtBQ01KOztBRExJO0VBQ0UsNEVBQUE7QUNPTjs7QURKRTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDTUo7O0FERkk7RUFDRSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUNJTjs7QURGSTtFQUNFLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0FDSU47O0FEREk7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNHTjs7QURDQTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ0VGOztBREFFO0VBQ0UsYUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDRUo7O0FEREk7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7QUNHTjs7QURBRTtFQUNFLGFBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBR0EsZ0RBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FDRUo7O0FEREk7RUFDRSxhQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0dOOztBREZNO0VBQ0UsV0FBQTtBQ0lSOztBREZNO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUNJUjs7QURESTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDR047O0FERk07RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ0lSOztBREhRO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDS1Y7O0FERk07RUFDRSxzQ0FBQTtBQ0lSOztBREZNO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQ0lSOztBREhRO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDS1Y7O0FESlU7RUFDRSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUNNWjs7QURMWTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtBQ09kOztBRE5jO0VBQ0Usc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUNRaEI7O0FETmM7RUFDRSxZQUFBO0FDUWhCOztBRE5jO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNRaEI7O0FEUGdCO0VBQ0UsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0FDU2xCOztBRFJrQjtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBQ1VwQjs7QURSa0I7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNVcEI7O0FETFk7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ09kOztBRE5jO0VBQ0UsWUFBQTtBQ1FoQjs7QURQZ0I7RUFDRSxRQUFBO0FDU2xCOztBRE5jO0VBQ0UsWUFBQTtBQ1FoQjs7QUROZ0I7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDUWxCOztBRE5nQjtFQUNFLFdBQUE7RUFDQSxpQkFBQTtBQ1FsQjs7QUROZ0I7RUFDRSxZQUFBO0FDUWxCOztBRERRO0VBQ0Usa0JBQUE7QUNHVjs7QUREVTtFQUNFLGVBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ0daOztBREZZO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0FDSWQ7O0FESGM7RUFDRSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ0toQjs7QURIYztFQUNFLFlBQUE7QUNLaEI7O0FESGM7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0toQjs7QURKZ0I7RUFDRSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLGFBQUE7QUNNbEI7O0FETGtCO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0FDT3BCOztBRExrQjtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ09wQjs7QURGWTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDSWQ7O0FESGM7RUFDRSxZQUFBO0FDS2hCOztBREpnQjtFQUNFLFFBQUE7QUNNbEI7O0FESGM7RUFDRSxZQUFBO0FDS2hCOztBREhnQjtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNLbEI7O0FESGdCO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0FDS2xCOztBREhnQjtFQUNFLFlBQUE7QUNLbEI7O0FERU07RUFDRSx1QkFBQTtBQ0FSOztBRElFO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBR0EsZ0RBQUE7QUNGSjs7QURJSTtFQUNFLFVBQUE7QUNGTjs7QURJSTtFQUNFLFdBQUE7RUFFQSxZQUFBO0FDSE47O0FES0k7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7QUNITjs7QURJTTtFQUNFLFdBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ0ZSOztBRElNO0VBQ0UsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNGUjs7QURJTTtFQUNFLGtCQUFBO0FDRlI7O0FES0k7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsMkJBQUE7QUNITjs7QURJTTtFQUNFLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDRlI7O0FES007RUFDRSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7QUNIUjs7QURLVTtFQUNFLFlBQUE7QUNIWjs7QURJWTtFQUNFLGVBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FDRmQ7O0FESVk7RUFDRSwrQkFBQTtFQUNBLGdDQUFBO0FDRmQ7O0FETVE7RUFDRSxZQUFBO0FDSlY7O0FETVE7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLDBDQUFBO0FDSlY7O0FETVE7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNKVjs7QURZQTtFQUdNO0lBQ0UsYUFBQTtJQUNBLHVCQUFBO0lBQ0EsbUJBQUE7RUNYTjtFRGVJO0lBQ0UsYUFBQTtFQ2JOO0VEZUk7SUFDRSxXQUFBO0VDYk47QUFDRiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvdXBncmFkZS1nZW51aW5lL3VwZ3JhZGUtZ2VudWluZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIioge1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogbWFyZ2luLWxlZnQgMC4ycyBlYXNlLWluLW91dDtcclxuICAtbW96LXRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgLW1zLXRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgLW8tdHJhbnNpdGlvbjogbWFyZ2luLWxlZnQgMC4ycyBlYXNlLWluLW91dDtcclxuICB0cmFuc2l0aW9uOiBtYXJnaW4tbGVmdCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIiwgc2Fucy1zZXJpZjtcclxuICAvLyBmb250LXdlaWdodDogMjAwO1xyXG59XHJcbi5wYi1mcmFtZXIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG4gIGNvbG9yOiAjZTY3ZTIyO1xyXG4gIC5wcm9ncmVzc2JhciB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYmFja2dyb3VuZDogIzM0OThkYjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGhlaWdodDogMjVweDtcclxuICAgIHotaW5kZXg6IC0xO1xyXG4gIH1cclxuICAudGVybWluYXRlZC5wcm9ncmVzc2JhciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbiAgfVxyXG4gIC5jbHItd2hpdGUge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxufVxyXG4uY2VudGVyLXRleHQge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmdlbnVpbmUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmYjkyMjQ7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4ub2ZmaWNpYWwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmNDU5MDU7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uYm9keS1hcmVhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTdmMWY5O1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgaGVpZ2h0OiAzNTBweDtcclxuICB1bCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgbGluZS1oZWlnaHQ6IDI7XHJcbiAgICBsaSB7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9zcmMvYXNzZXRzL2ltYWdlcy9pY29uX2xpc3Quc3ZnXCIpIG5vLXJlcGVhdCBsZWZ0IHRvcDtcclxuICAgIH1cclxuICB9XHJcbiAgLmNhcmQtYm9keS1pbWFnZSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgLmJ1dHRvbi1jb250YWluZXIge1xyXG4gICAgYnV0dG9uLmJ0bi5idG4tMiB7XHJcbiAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBib3R0b206IDgwcHg7XHJcbiAgICAgIGxlZnQ6IDIwcHg7XHJcbiAgICB9XHJcbiAgICBidXR0b24uYnRuLmJ0bi0xIHtcclxuICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGJvdHRvbTogMjBweDtcclxuICAgICAgbGVmdDogMjBweDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbiA+IGltZyB7XHJcbiAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLmNvbnRhaW5lci1hbGwge1xyXG4gIGJhY2tncm91bmQ6ICNmOGY4Zjg7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHBhZGRpbmc6IDV2aCAzdnc7XHJcblxyXG4gIC5zYXZlLWNvbnRhaW5lciB7XHJcbiAgICBtYXJnaW46IDElIDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC5zYXZlIHtcclxuICAgICAgd2lkdGg6IDM1JTtcclxuICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZjczMDE7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb2wtbWQtMTIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAwcHggMTZweCAtNnB4IHJnYmEoMCwgMCwgMCwgMC41KTtcclxuICAgIC1tb3otYm94LXNoYWRvdzogMHB4IDBweCAxNnB4IC02cHggcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDBweCAxNnB4IC02cHggcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiA3dmg7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLnRpdGxlIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICBtYXJnaW4tdG9wOiAydmg7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAydnc7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDQwcHg7XHJcbiAgICAgIH1cclxuICAgICAgLnRleHQge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLnNlY3Rpb24ge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAuc2VjdGlvbi1oZWFkZXIge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAuc2VjdGlvbi10aXRsZSB7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAubGVmdC1zZWN0aW9uLXVwbG9hZCB7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0ICFpbXBvcnRhbnQ7XHJcbiAgICAgIH1cclxuICAgICAgLnNlY3Rpb24tdXBsb2FkIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1dmg7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAuY29sLW1kLTMge1xyXG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgbWFyZ2luLXRvcCA6IDR2aDtcclxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDR2aDtcclxuICAgICAgICAgIC5jYXJkIHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogM3ZoO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgIC5jYXJkLWNvbnRlbnQge1xyXG4gICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgICAgLnVwbG9hZC1kZXNjIHtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjYmJiICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIC5sb2dvLXVwbG9hZCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIC5pbWFnZSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAuY3VzdG9tIHtcclxuICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiNlZWU7XHJcbiAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAgIGJvcmRlcjogM3B4IHNvbGlkICNlYmViZWI7XHJcbiAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgICAgICAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICAgICAgICAgICAgICAuZHJhZy1kcm9wIHtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIC5zZXQtb3BhY2l0eSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMC4xO1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogYXV0bztcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuZm9ybS1ncm91cC51cGxvYWRlZCB7XHJcbiAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgLmxvYWRpbmcge1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgLnNrLWZhZGluZy1jaXJjbGUge1xyXG4gICAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgLnVwbG9hZGVkLWltYWdlIHtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgICAgICAuaW1nIHtcclxuICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAuaW1nID4gaW1nIHtcclxuICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDMyMHB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLmVkaXRvci10b29sIHtcclxuICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNvbC1tZC00IHtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgLmNhcmQge1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAzdmg7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzAwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgICAgLmNhcmQtY29udGVudCB7XHJcbiAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgICAudXBsb2FkLWRlc2Mge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICNiYmIgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgLmxvZ28tdXBsb2FkIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgLmltYWdlIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgIC5jdXN0b20ge1xyXG4gICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlZWU7XHJcbiAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICBib3JkZXI6IDNweCBzb2xpZCAjZWJlYmViO1xyXG4gICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgICAgICAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICAgICAgICAgICAgICAuZHJhZy1kcm9wIHtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIC5zZXQtb3BhY2l0eSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMC4xO1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogYXV0bztcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuZm9ybS1ncm91cC51cGxvYWRlZCB7XHJcbiAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgLmxvYWRpbmcge1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgLnNrLWZhZGluZy1jaXJjbGUge1xyXG4gICAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgLnVwbG9hZGVkLWltYWdlIHtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgICAgICAuaW1nIHtcclxuICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAuaW1nID4gaW1nIHtcclxuICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDMyMHB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLmVkaXRvci10b29sIHtcclxuICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAucm93IHtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAuY29udGFpbmVyLXVwZ3JhZGUge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHBhZGRpbmc6IDUlO1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAwcHggMTZweCAtNnB4IHJnYmEoMCwgMCwgMCwgMC41KTtcclxuICAgIC1tb3otYm94LXNoYWRvdzogMHB4IDBweCAxNnB4IC02cHggcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDBweCAxNnB4IC02cHggcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG5cclxuICAgIC51cGdyYWRlLXBhZ2UtYmcge1xyXG4gICAgICB3aWR0aDogNTAlO1xyXG4gICAgfVxyXG4gICAgLnVwZ3JhZGUtcGFnZS1iZyA+IGltZyB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gICAgLnVwZ3JhZGUtcGFnZS1zdWNjZXNzIHtcclxuICAgICAgd2lkdGg6IDQwJTtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIC5pbWFnZS1zdWNjZXNzIHtcclxuICAgICAgICB3aWR0aDogNDBweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XHJcbiAgICAgIH1cclxuICAgICAgLmhlYWRlci1tZXNzYWdlIHtcclxuICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAgICAgfVxyXG4gICAgICAuYm9keS1tZXNzYWdlIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC51cGdyYWRlLXBhZ2UtZm9ybSB7XHJcbiAgICAgIHdpZHRoOiA0NSU7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgICAgLmZvcm0tdGl0bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICAudXBsb2FkLWZvcm0ge1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICAuY29sLW1kLTYge1xyXG4gICAgICAgICAgLmNhcmQge1xyXG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgIC5jYXJkLWhlYWRlciB7XHJcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgICAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtYm9keSB7XHJcbiAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICAgICAgICAgICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBidXR0b24ge1xyXG4gICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIH1cclxuICAgICAgICAudXBncmFkZS1idXR0b24ge1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICAgIGNvbG9yOiBibHVlO1xyXG4gICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICBib3JkZXI6IDNweCBzb2xpZCBibHVlO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNDBweDtcclxuICAgICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnVwZ3JhZGUtYnV0dG9uOmhvdmVyIHtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogYmx1ZTtcclxuICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgYm9yZGVyOiAzcHggc29saWQgYmx1ZTtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XHJcbiAgICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogMTAwMHB4KSB7XHJcbiAgLmNvbnRhaW5lci1hbGwge1xyXG4gICAgLmNvbnRhaW5lci11cGxvYWQge1xyXG4gICAgICAudGl0bGUge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNvbnRhaW5lci11cGdyYWRlIHtcclxuICAgICAgLnVwZ3JhZGUtcGFnZS1iZyB7XHJcbiAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgfVxyXG4gICAgICAudXBncmFkZS1wYWdlLWZvcm0ge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiIsIioge1xuICAtd2Via2l0LXRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XG4gIC1tb3otdHJhbnNpdGlvbjogbWFyZ2luLWxlZnQgMC4ycyBlYXNlLWluLW91dDtcbiAgLW1zLXRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XG4gIC1vLXRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XG4gIHRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIiwgc2Fucy1zZXJpZjtcbn1cblxuLnBiLWZyYW1lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDI1cHg7XG4gIGNvbG9yOiAjZTY3ZTIyO1xufVxuLnBiLWZyYW1lciAucHJvZ3Jlc3NiYXIge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjVweDtcbiAgei1pbmRleDogLTE7XG59XG4ucGItZnJhbWVyIC50ZXJtaW5hdGVkLnByb2dyZXNzYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xufVxuLnBiLWZyYW1lciAuY2xyLXdoaXRlIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uY2VudGVyLXRleHQge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5nZW51aW5lIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZiOTIyNDtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5vZmZpY2lhbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNDU5MDU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYm9keS1hcmVhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U3ZjFmOTtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBoZWlnaHQ6IDM1MHB4O1xufVxuLmJvZHktYXJlYSB1bCB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG59XG4uYm9keS1hcmVhIHVsIGxpIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL3NyYy9hc3NldHMvaW1hZ2VzL2ljb25fbGlzdC5zdmdcIikgbm8tcmVwZWF0IGxlZnQgdG9wO1xufVxuLmJvZHktYXJlYSAuY2FyZC1ib2R5LWltYWdlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uYm9keS1hcmVhIC5idXR0b24tY29udGFpbmVyIGJ1dHRvbi5idG4uYnRuLTIge1xuICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiA4MHB4O1xuICBsZWZ0OiAyMHB4O1xufVxuLmJvZHktYXJlYSAuYnV0dG9uLWNvbnRhaW5lciBidXR0b24uYnRuLmJ0bi0xIHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMjBweDtcbiAgbGVmdDogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLmJvZHktYXJlYSAuYnV0dG9uLWNvbnRhaW5lciBidXR0b24gPiBpbWcge1xuICB3aWR0aDogYXV0bztcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG5cbi5jb250YWluZXItYWxsIHtcbiAgYmFja2dyb3VuZDogI2Y4ZjhmODtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogNXZoIDN2dztcbn1cbi5jb250YWluZXItYWxsIC5zYXZlLWNvbnRhaW5lciB7XG4gIG1hcmdpbjogMSUgMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmNvbnRhaW5lci1hbGwgLnNhdmUtY29udGFpbmVyIC5zYXZlIHtcbiAgd2lkdGg6IDM1JTtcbiAgb3V0bGluZTogbm9uZTtcbiAgaGVpZ2h0OiA2MHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZjczMDE7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAwcHggMTZweCAtNnB4IHJnYmEoMCwgMCwgMCwgMC41KTtcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMHB4IDE2cHggLTZweCByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggMTZweCAtNnB4IHJnYmEoMCwgMCwgMCwgMC41KTtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgcGFkZGluZy1ib3R0b206IDd2aDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAudGl0bGUge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDJ2aDtcbiAgbWFyZ2luLWxlZnQ6IDJ2dztcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnRpdGxlIGltZyB7XG4gIHdpZHRoOiA0MHB4O1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAudGl0bGUgLnRleHQge1xuICBmb250LXNpemU6IDMwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24ge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi1oZWFkZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24gLnNlY3Rpb24taGVhZGVyIC5zZWN0aW9uLXRpdGxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24gLmxlZnQtc2VjdGlvbi11cGxvYWQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQgIWltcG9ydGFudDtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24gLnNlY3Rpb24tdXBsb2FkIHtcbiAgbWFyZ2luLXRvcDogNXZoO1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tdG9wOiA0dmg7XG4gIG1hcmdpbi1ib3R0b206IDR2aDtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMyAuY2FyZCB7XG4gIG1hcmdpbi10b3A6IDN2aDtcbiAgaGVpZ2h0OiAzMDBweDtcbiAgYm9yZGVyOiBub25lO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0zIC5jYXJkIC5jYXJkLWNvbnRlbnQge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMyAuY2FyZCAuY2FyZC1jb250ZW50IC51cGxvYWQtZGVzYyB7XG4gIGNvbG9yOiAjYmJiICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0zIC5jYXJkIC5jYXJkLWNvbnRlbnQgLmxvZ28tdXBsb2FkIHtcbiAgd2lkdGg6IDEwMHB4O1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0zIC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZsb2F0OiBsZWZ0O1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTMgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSB7XG4gIGhlaWdodDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYm9yZGVyOiAzcHggc29saWQgI2ViZWJlYjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIHBhZGRpbmc6IDQwcHg7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTMgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSAuZHJhZy1kcm9wIHtcbiAgZm9udC1zaXplOiAxMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTMgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSAuc2V0LW9wYWNpdHkge1xuICBvcGFjaXR5OiAwLjE7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IGF1dG87XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTMgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0zIC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0zIC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIC5zay1mYWRpbmctY2lyY2xlIHtcbiAgdG9wOiA1MCU7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTMgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0zIC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZSAuaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTMgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlIC5pbWcgPiBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LWhlaWdodDogMzIwcHg7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTMgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlIC5lZGl0b3ItdG9vbCB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtNCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtNCAuY2FyZCB7XG4gIG1hcmdpbi10b3A6IDN2aDtcbiAgaGVpZ2h0OiAzMDBweDtcbiAgYm9yZGVyOiBub25lO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC00IC5jYXJkIC5jYXJkLWNvbnRlbnQge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtNCAuY2FyZCAuY2FyZC1jb250ZW50IC51cGxvYWQtZGVzYyB7XG4gIGNvbG9yOiAjYmJiICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC00IC5jYXJkIC5jYXJkLWNvbnRlbnQgLmxvZ28tdXBsb2FkIHtcbiAgd2lkdGg6IDEwMHB4O1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC00IC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZsb2F0OiBsZWZ0O1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTQgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSB7XG4gIGhlaWdodDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGJvcmRlcjogM3B4IHNvbGlkICNlYmViZWI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIHBhZGRpbmc6IDQwcHg7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTQgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSAuZHJhZy1kcm9wIHtcbiAgZm9udC1zaXplOiAxMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTQgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSAuc2V0LW9wYWNpdHkge1xuICBvcGFjaXR5OiAwLjE7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IGF1dG87XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC00IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC00IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIC5zay1mYWRpbmctY2lyY2xlIHtcbiAgdG9wOiA1MCU7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbC1tZC0xMiAuc2VjdGlvbiAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC00IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZSAuaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlIC5pbWcgPiBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LWhlaWdodDogMzIwcHg7XG59XG4uY29udGFpbmVyLWFsbCAuY29sLW1kLTEyIC5zZWN0aW9uIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlIC5lZGl0b3ItdG9vbCB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5jb250YWluZXItYWxsIC5jb2wtbWQtMTIgLnNlY3Rpb24gLnJvdyB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbnRhaW5lci11cGdyYWRlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIGhlaWdodDogMTAwJTtcbiAgcGFkZGluZzogNSU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAwcHggMTZweCAtNnB4IHJnYmEoMCwgMCwgMCwgMC41KTtcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMHB4IDE2cHggLTZweCByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggMTZweCAtNnB4IHJnYmEoMCwgMCwgMCwgMC41KTtcbn1cbi5jb250YWluZXItYWxsIC5jb250YWluZXItdXBncmFkZSAudXBncmFkZS1wYWdlLWJnIHtcbiAgd2lkdGg6IDUwJTtcbn1cbi5jb250YWluZXItYWxsIC5jb250YWluZXItdXBncmFkZSAudXBncmFkZS1wYWdlLWJnID4gaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbn1cbi5jb250YWluZXItYWxsIC5jb250YWluZXItdXBncmFkZSAudXBncmFkZS1wYWdlLXN1Y2Nlc3Mge1xuICB3aWR0aDogNDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5jb250YWluZXItYWxsIC5jb250YWluZXItdXBncmFkZSAudXBncmFkZS1wYWdlLXN1Y2Nlc3MgLmltYWdlLXN1Y2Nlc3Mge1xuICB3aWR0aDogNDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBhbGlnbi1zZWxmOiBjZW50ZXI7XG59XG4uY29udGFpbmVyLWFsbCAuY29udGFpbmVyLXVwZ3JhZGUgLnVwZ3JhZGUtcGFnZS1zdWNjZXNzIC5oZWFkZXItbWVzc2FnZSB7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbnRhaW5lci11cGdyYWRlIC51cGdyYWRlLXBhZ2Utc3VjY2VzcyAuYm9keS1tZXNzYWdlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbnRhaW5lci11cGdyYWRlIC51cGdyYWRlLXBhZ2UtZm9ybSB7XG4gIHdpZHRoOiA0NSU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbn1cbi5jb250YWluZXItYWxsIC5jb250YWluZXItdXBncmFkZSAudXBncmFkZS1wYWdlLWZvcm0gLmZvcm0tdGl0bGUge1xuICBmb250LXNpemU6IDEuNXJlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cbi5jb250YWluZXItYWxsIC5jb250YWluZXItdXBncmFkZSAudXBncmFkZS1wYWdlLWZvcm0gLnVwbG9hZC1mb3JtIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2RkZDtcbiAgaGVpZ2h0OiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBwYWRkaW5nOiAyMHB4O1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbnRhaW5lci11cGdyYWRlIC51cGdyYWRlLXBhZ2UtZm9ybSAudXBsb2FkLWZvcm0gLmNvbC1tZC02IC5jYXJkIHtcbiAgYm9yZGVyOiBub25lO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbnRhaW5lci11cGdyYWRlIC51cGdyYWRlLXBhZ2UtZm9ybSAudXBsb2FkLWZvcm0gLmNvbC1tZC02IC5jYXJkIC5jYXJkLWhlYWRlciB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDEwcHg7XG59XG4uY29udGFpbmVyLWFsbCAuY29udGFpbmVyLXVwZ3JhZGUgLnVwZ3JhZGUtcGFnZS1mb3JtIC51cGxvYWQtZm9ybSAuY29sLW1kLTYgLmNhcmQgLmNhcmQtYm9keSB7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbnRhaW5lci11cGdyYWRlIC51cGdyYWRlLXBhZ2UtZm9ybSAudXBsb2FkLWZvcm0gYnV0dG9uIHtcbiAgYm9yZGVyOiBub25lO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbnRhaW5lci11cGdyYWRlIC51cGdyYWRlLXBhZ2UtZm9ybSAudXBsb2FkLWZvcm0gLnVwZ3JhZGUtYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBjb2xvcjogYmx1ZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJvcmRlcjogM3B4IHNvbGlkIGJsdWU7XG4gIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gIGhlaWdodDogNjBweDtcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLmNvbnRhaW5lci1hbGwgLmNvbnRhaW5lci11cGdyYWRlIC51cGdyYWRlLXBhZ2UtZm9ybSAudXBsb2FkLWZvcm0gLnVwZ3JhZGUtYnV0dG9uOmhvdmVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IGJsdWU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYm9yZGVyOiAzcHggc29saWQgYmx1ZTtcbiAgYm9yZGVyLXJhZGl1czogNDBweDtcbiAgaGVpZ2h0OiA2MHB4O1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogMTAwMHB4KSB7XG4gIC5jb250YWluZXItYWxsIC5jb250YWluZXItdXBsb2FkIC50aXRsZSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC5jb250YWluZXItYWxsIC5jb250YWluZXItdXBncmFkZSAudXBncmFkZS1wYWdlLWJnIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG4gIC5jb250YWluZXItYWxsIC5jb250YWluZXItdXBncmFkZSAudXBncmFkZS1wYWdlLWZvcm0ge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/upgrade-genuine/upgrade-genuine.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/upgrade-genuine/upgrade-genuine.component.ts ***!
  \*************************************************************************************/
/*! exports provided: UpgradeGenuineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpgradeGenuineComponent", function() { return UpgradeGenuineComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/merchant/merchant.service */ "./src/app/services/merchant/merchant.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var UpgradeGenuineComponent = /** @class */ (function () {
    function UpgradeGenuineComponent(merchantService) {
        this.merchantService = merchantService;
        this.progressBar = 0;
        this.showLoading = false;
        this.showLoading2 = false;
        this.showLoading3 = false;
        this.showLoading4 = false;
        this.showLoading5 = false;
        this.showLoading6 = false;
        this.showLoading7 = false;
        this.errorLabel = false;
        this.selectGenuine = false;
        this.selectOfficial = false;
        this.selectedFile = null;
        this.selectedFile2 = null;
        this.form = {
            id_image: '',
            npwp_image: '',
            siup_image: '',
            sampul_image: '',
            tdp_image: '',
            nip_image: '',
            domisili_image: ''
        };
        this.complete = false;
    }
    UpgradeGenuineComponent.prototype.ngOnInit = function () {
    };
    UpgradeGenuineComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    UpgradeGenuineComponent.prototype.onFileSelected = function (event, img) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_1, logo, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_1 = 3000000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_1) {
                                _this.errorFile = 'Maximum File Upload is 3MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        switch (img) {
                            case 'id_image':
                                this.showLoading = true;
                                console.log("ini1", img);
                                break;
                            case 'npwp_image':
                                this.showLoading2 = true;
                                console.log("ini2", img);
                                break;
                            case 'siup_image':
                                this.showLoading3 = true;
                                console.log("ini3", img);
                                break;
                            case 'sampul_image':
                                this.showLoading4 = true;
                                console.log("ini4", img);
                                break;
                            case 'tdp_image':
                                this.showLoading5 = true;
                                console.log("ini5", img);
                                break;
                            case 'nib_image':
                                this.showLoading6 = true;
                                console.log("ini6", img);
                                break;
                            case 'domisili_image':
                                this.showLoading7 = true;
                                console.log("ini7", img);
                                break;
                        }
                        return [4 /*yield*/, this.merchantService.upload(this.selectedFile, this, 'image', function (result) {
                                console.log("hasil", result);
                                _this.callImage(result, img);
                            })];
                    case 2:
                        logo = _a.sent();
                        _a.label = 3;
                    case 3:
                        console.log("test lah", this.callImage);
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        this.errorLabel = e_1.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    UpgradeGenuineComponent.prototype.allfalse = function () {
        this.showLoading = this.showLoading2 = this.showLoading3
            = this.showLoading4 = this.showLoading5 = this.showLoading6
                = this.showLoading7 = false;
    };
    UpgradeGenuineComponent.prototype.callImage = function (result, img) {
        console.log('result upload logo ', result);
        this.form[img] = result.base_url + result.pic_big_path;
        this.allfalse();
        console.log('This detail', this.form);
    };
    UpgradeGenuineComponent.prototype.genuineClick = function () {
        this.selectGenuine = true;
        this.selectOfficial = false;
    };
    UpgradeGenuineComponent.prototype.officialClick = function () {
        this.selectOfficial = true;
        this.selectGenuine = false;
    };
    UpgradeGenuineComponent.prototype.saveProfile = function () {
        this.complete = true;
    };
    UpgradeGenuineComponent.ctorParameters = function () { return [
        { type: _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_1__["MerchantService"] }
    ]; };
    UpgradeGenuineComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-upgrade-genuine',
            template: __webpack_require__(/*! raw-loader!./upgrade-genuine.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/upgrade-genuine/upgrade-genuine.component.html"),
            styles: [__webpack_require__(/*! ./upgrade-genuine.component.scss */ "./src/app/layout/merchant-portal/upgrade-genuine/upgrade-genuine.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_1__["MerchantService"]])
    ], UpgradeGenuineComponent);
    return UpgradeGenuineComponent;
}());



/***/ }),

/***/ "./src/app/layout/router-master/router-master.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/router-master/router-master.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9yb3V0ZXItbWFzdGVyL3JvdXRlci1tYXN0ZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/router-master/router-master.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/router-master/router-master.component.ts ***!
  \*****************************************************************/
/*! exports provided: RouterMasterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouterMasterComponent", function() { return RouterMasterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/observerable/permission-observer */ "./src/app/services/observerable/permission-observer.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RouterMasterComponent = /** @class */ (function () {
    function RouterMasterComponent(router, castPermission) {
        this.router = router;
        this.castPermission = castPermission;
    }
    RouterMasterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.castPermission.currentPermission.subscribe(function (permissionVal) {
            // if(permissionVal == 'merchant'){
            //     this.router.navigate(['/merchant-portal/homepage']);
            //     return true;
            // }
            if (permissionVal == 'admin') {
                // this.router.navigate(['/app-login']);
                // this.router.navigate(['/administrator/order-history-summary']);
            }
            else {
                _this.router.navigate(['/login/out']);
            }
        });
        var isLoggedin = localStorage.getItem('isLoggedin');
        if (isLoggedin == 'true') {
            this.router.navigateByUrl('/app-login');
        }
    };
    RouterMasterComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__["PermissionObserver"] }
    ]; };
    RouterMasterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-router-master',
            template: __webpack_require__(/*! raw-loader!./router-master.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/router-master/router-master.component.html"),
            styles: [__webpack_require__(/*! ./router-master.component.scss */ "./src/app/layout/router-master/router-master.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__["PermissionObserver"]])
    ], RouterMasterComponent);
    return RouterMasterComponent;
}());



/***/ }),

/***/ "./src/app/layout/routing.path.ts":
/*!****************************************!*\
  !*** ./src/app/layout/routing.path.ts ***!
  \****************************************/
/*! exports provided: routePath */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routePath", function() { return routePath; });
/* harmony import */ var _router_master_router_master_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./router-master/router-master.component */ "./src/app/layout/router-master/router-master.component.ts");

var routePath = [
    { path: '', component: _router_master_router_master_component__WEBPACK_IMPORTED_MODULE_0__["RouterMasterComponent"], label: "router master component" },
    { path: 'administrator', loadChildren: './administrator/administrator.module#AdministratorModule', label: 'Administrator' },
    { path: 'merchant-portal', loadChildren: './merchant-portal/merchant-portal.module#MerchantPortalModule', label: "Merchant portal" },
    { path: 'admin-management', loadChildren: './admin-management/admin-management.module#AdminManagementModule', label: 'Admin Management' },
];


/***/ }),

/***/ "./src/app/services/inbox/inbox.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/inbox/inbox.service.ts ***!
  \*************************************************/
/*! exports provided: InboxService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InboxService", function() { return InboxService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _master_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../master.service */ "./src/app/services/master.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var InboxService = /** @class */ (function () {
    function InboxService(http, myService) {
        this.http = http;
        this.myService = myService;
        this.api_url = '';
        this.httpReq = false;
        this.gerro = "test";
        this.api_url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])();
    }
    InboxService.prototype.getInbox = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications-portal';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4: 
                    // console.log("test", result)
                    return [2 /*return*/, result];
                }
            });
        });
    };
    InboxService.prototype.detailInbox = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/' + _id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: 
                    // console.log("test", result)
                    return [2 /*return*/, result];
                }
            });
        });
    };
    InboxService.prototype.readInbox = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/read/' + _id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4:
                        console.log("test", result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    InboxService.prototype.deleteInbox = function (params, _id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/delete' + _id;
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    InboxService.prototype.multipleDelete = function (params, _id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/delete' + _id;
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    InboxService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
        { type: _master_service__WEBPACK_IMPORTED_MODULE_1__["MasterService"] }
    ]; };
    InboxService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _master_service__WEBPACK_IMPORTED_MODULE_1__["MasterService"]])
    ], InboxService);
    return InboxService;
}());



/***/ })

}]);
//# sourceMappingURL=merchant-portal-merchant-portal-module.js.map