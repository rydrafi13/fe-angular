(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin-report-member-demography-report-member-demography-report-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.component.html":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.component.html ***!
  \****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n    <app-page-header [heading]=\"'Member Demography'\" [icon]=\"'fa-table'\" >\r\n    </app-page-header>\r\n    <!--- Demography Age -->\r\n    <div class=\"summary-report row\">\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"demographyData.age.barChartLabels.length\">\r\n                    <div class=\"card-header\">\r\n                        Age graph\r\n                    </div>\r\n                    <div class=\"card-body\"> \r\n                        <canvas baseChart [datasets]=\"demographyData.age.barChartData\"\r\n                            [labels]=\"demographyData.age.barChartLabels\" [options]=\"barChartOptions\"\r\n                            [legend]=\"barChartLegend\" [chartType]=\"'bar'\" (chartHover)=\"chartHovered($event)\"\r\n                            (chartClick)=\"chartClicked($event)\">\r\n                        </canvas>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(demographyData.age.daily)\">Refresh</button>\r\n                        </div> -->\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"demographyData.age.barChartLabels.length\">\r\n                    <div class=\"card-header\">\r\n                        Age table\r\n                    </div>\r\n                    <div class=\"card-body emirate-daily\">\r\n                        <table>\r\n                            <tr>\r\n                                <th>Age</th>\r\n                                <th>Total</th>\r\n                            </tr>\r\n                            <tr *ngFor=\"let dt of demographyData.age.barChartLabels;let i=index;\">\r\n                                <td><strong> {{dt}}</strong></td>\r\n                                <td>{{data.age[dt]}}</td>\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                                        <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n                                    </div> -->\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n    <!--- END Demography -->\r\n    \r\n    <!--- Demography Gender -->\r\n    <div class=\"summary-report row\">\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"demographyData.gender.barChartLabels.length\">\r\n                    <div class=\"card-header\">\r\n                         Gender graph\r\n                    </div>\r\n                    <div class=\"card-body\"> \r\n                        <canvas baseChart [datasets]=\"demographyData.gender.barChartData\"\r\n                            [labels]=\"demographyData.gender.barChartLabels\" [options]=\"barChartOptions\"\r\n                            [legend]=\"barChartLegend\" [chartType]=\"'pie'\" (chartHover)=\"chartHovered($event)\"\r\n                            (chartClick)=\"chartClicked($event)\">\r\n                        </canvas>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(demographyData.age.daily)\">Refresh</button>\r\n                        </div> -->\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"demographyData.gender.barChartLabels.length\">\r\n                    <div class=\"card-header\">\r\n                        Gender table\r\n                    </div>\r\n                    <div class=\"card-body emirate-daily\">\r\n                        <table>\r\n                            <tr>\r\n                                <th>Gender</th>\r\n                                <th>Total</th>\r\n                            </tr>\r\n                            <tr *ngFor=\"let dt of demographyData.gender.barChartLabels;let i=index;\">\r\n                                <td><strong> {{dt}}</strong></td>\r\n                                <td>{{data.gender[dt]}}</td>\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                   \r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n    <!--- END Gender Demography -->\r\n</div>"

/***/ }),

/***/ "./src/app/layout/modules/admin-report/member-demography-report/member-demography-report-routing.module.ts":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/member-demography-report/member-demography-report-routing.module.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: MemberDemographyReportRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberDemographyReportRoutingModule", function() { return MemberDemographyReportRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _member_demography_report_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./member-demography-report.component */ "./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _member_demography_report_component__WEBPACK_IMPORTED_MODULE_2__["MemberDemographyReportComponent"],
    },
];
var MemberDemographyReportRoutingModule = /** @class */ (function () {
    function MemberDemographyReportRoutingModule() {
    }
    MemberDemographyReportRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], MemberDemographyReportRoutingModule);
    return MemberDemographyReportRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.component.scss":
/*!**************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.component.scss ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".summary-report .dark-header {\n  background: #34495e;\n  font-weight: bold;\n  color: white;\n}\n.summary-report .card-body.emirate-daily {\n  max-height: 340px;\n  overflow: auto;\n}\n.summary-report .row-per-card {\n  margin-bottom: 20px;\n}\n.summary-report table {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n  text-align: center;\n}\n.summary-report table th {\n  font-size: 12px;\n  font-weight: normal;\n}\n.summary-report table tr:nth-child(odd) td {\n  background-color: #f0f0f0;\n}\n.summary-report table td, .summary-report table th {\n  border: 1px solid #ccc;\n  padding: 4px 15px;\n}\n.summary-report table td {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tcmVwb3J0L21lbWJlci1kZW1vZ3JhcGh5LXJlcG9ydC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGFkbWluLXJlcG9ydFxcbWVtYmVyLWRlbW9ncmFwaHktcmVwb3J0XFxtZW1iZXItZGVtb2dyYXBoeS1yZXBvcnQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLXJlcG9ydC9tZW1iZXItZGVtb2dyYXBoeS1yZXBvcnQvbWVtYmVyLWRlbW9ncmFwaHktcmVwb3J0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNBUjtBREdJO0VBQ0ksaUJBQUE7RUFDQSxjQUFBO0FDRFI7QURHSTtFQUNJLG1CQUFBO0FDRFI7QURHSTtFQUNJLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNEUjtBREVRO0VBR0ksZUFBQTtFQUNBLG1CQUFBO0FDRlo7QURJUTtFQUNJLHlCQUFBO0FDRlo7QURJUTtFQUNJLHNCQUFBO0VBQ0EsaUJBQUE7QUNGWjtBRElRO0VBQ0ksZUFBQTtBQ0ZaIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tcmVwb3J0L21lbWJlci1kZW1vZ3JhcGh5LXJlcG9ydC9tZW1iZXItZGVtb2dyYXBoeS1yZXBvcnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3VtbWFyeS1yZXBvcnR7XHJcbiAgICAuZGFyay1oZWFkZXJ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzM0NDk1ZTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5jYXJkLWJvZHkuZW1pcmF0ZS1kYWlseXtcclxuICAgICAgICBtYXgtaGVpZ2h0OiAzNDBweDtcclxuICAgICAgICBvdmVyZmxvdzogYXV0bztcclxuICAgIH1cclxuICAgIC5yb3ctcGVyLWNhcmR7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgIH1cclxuICAgIHRhYmxle1xyXG4gICAgICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgdGh7XHJcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQ6ICMzMzM7XHJcbiAgICAgICAgICAgIC8vIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0cjpudGgtY2hpbGQob2RkKSB0ZHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGQsIHRoe1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA0cHggMTVweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGR7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufSIsIi5zdW1tYXJ5LXJlcG9ydCAuZGFyay1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjMzQ0OTVlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IHdoaXRlO1xufVxuLnN1bW1hcnktcmVwb3J0IC5jYXJkLWJvZHkuZW1pcmF0ZS1kYWlseSB7XG4gIG1heC1oZWlnaHQ6IDM0MHB4O1xuICBvdmVyZmxvdzogYXV0bztcbn1cbi5zdW1tYXJ5LXJlcG9ydCAucm93LXBlci1jYXJkIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdGgge1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdHI6bnRoLWNoaWxkKG9kZCkgdGQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xufVxuLnN1bW1hcnktcmVwb3J0IHRhYmxlIHRkLCAuc3VtbWFyeS1yZXBvcnQgdGFibGUgdGgge1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBwYWRkaW5nOiA0cHggMTVweDtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0ZCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.component.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.component.ts ***!
  \************************************************************************************************************/
/*! exports provided: MemberDemographyReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberDemographyReportComponent", function() { return MemberDemographyReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/activity/activity.service */ "./src/app/services/activity/activity.service.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var MemberDemographyReportComponent = /** @class */ (function () {
    function MemberDemographyReportComponent(memberReportService, sanitizer, activityService) {
        this.memberReportService = memberReportService;
        this.sanitizer = sanitizer;
        this.activityService = activityService;
        this.data = [];
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
        };
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.optO = { fill: false, borderWidth: 1, };
        this.demographyData = {
            age: {
                barChartData: [__assign({ data: [], label: 'Age Demography' }, this.optO)],
                barChartLabels: []
            },
            gender: {
                barChartData: [__assign({ data: [], label: 'Gender Demography' }, this.optO)],
                barChartLabels: []
            }
        };
        this.service = memberReportService;
        this.activityService.addLog({ page: this.title });
    }
    MemberDemographyReportComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate;
            return __generator(this, function (_a) {
                currentDate = new Date();
                this.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    MemberDemographyReportComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var resultAge, resultGender, cloneAge, cloneGender, convertedData, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 6, , 7]);
                        this.service = this.memberReportService;
                        return [4 /*yield*/, this.memberReportService.getMemberDemographyReport('age')];
                    case 1:
                        resultAge = _a.sent();
                        return [4 /*yield*/, this.memberReportService.getMemberDemographyReport('gender')];
                    case 2:
                        resultGender = _a.sent();
                        this.data = {
                            age: resultAge.result,
                            gender: resultGender.result
                        };
                        if (!this.data) return [3 /*break*/, 5];
                        return [4 /*yield*/, JSON.parse(JSON.stringify(this.demographyData.age.barChartData))];
                    case 3:
                        cloneAge = _a.sent();
                        return [4 /*yield*/, JSON.parse(JSON.stringify(this.demographyData.gender.barChartData))];
                    case 4:
                        cloneGender = _a.sent();
                        convertedData = {
                            age: this.convertToReport(this.data.age),
                            gender: this.convertToReport(this.data.gender)
                        };
                        AGE: {
                            cloneAge[0].data = convertedData.age.data;
                            this.demographyData.age.barChartLabels = convertedData.age.label;
                            this.demographyData.age.barChartData = cloneAge;
                        }
                        GENDER: {
                            cloneGender[0].data = convertedData.gender.data;
                            this.demographyData.gender.barChartLabels = convertedData.gender.label;
                            this.demographyData.gender.barChartData = cloneGender;
                        }
                        console.log("CLONE convertedData here ", this.demographyData);
                        _a.label = 5;
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    MemberDemographyReportComponent.prototype.convertToReport = function (data) {
        var dt = [];
        var lbl = [];
        for (var obj in data) {
            dt.push(data[obj]);
            lbl.push(obj);
        }
        return { data: dt, label: lbl };
    };
    MemberDemographyReportComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"] },
        { type: _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_2__["ActivityService"] }
    ]; };
    MemberDemographyReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-member-demography-report',
            template: __webpack_require__(/*! raw-loader!./member-demography-report.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.component.html"),
            styles: [__webpack_require__(/*! ./member-demography-report.component.scss */ "./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"],
            _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_2__["ActivityService"]])
    ], MemberDemographyReportComponent);
    return MemberDemographyReportComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.module.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.module.ts ***!
  \*********************************************************************************************************/
/*! exports provided: MemberDemographyReportModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberDemographyReportModule", function() { return MemberDemographyReportModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _member_demography_report_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./member-demography-report.component */ "./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _member_demography_report_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./member-demography-report-routing.module */ "./src/app/layout/modules/admin-report/member-demography-report/member-demography-report-routing.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var MemberDemographyReportModule = /** @class */ (function () {
    function MemberDemographyReportModule() {
    }
    MemberDemographyReportModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_member_demography_report_component__WEBPACK_IMPORTED_MODULE_2__["MemberDemographyReportComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _member_demography_report_routing_module__WEBPACK_IMPORTED_MODULE_9__["MemberDemographyReportRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_7__["BsComponentModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_8__["ChartsModule"]
            ]
        })
    ], MemberDemographyReportModule);
    return MemberDemographyReportModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin-report-member-demography-report-member-demography-report-module.js.map