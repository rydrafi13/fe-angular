(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-points-transaction-report-points-transaction-report-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/points-transaction-report/add/points-transaction-report.add.component.html":
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/points-transaction-report/add/points-transaction-report.add.component.html ***!
  \*************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n    <app-page-header [heading]=\"''\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n    <div *ngIf=\"!showPasswordPage\">\r\n        <div class=\"card card-detail\">\r\n            <div class=\"card-header\">\r\n                <button class=\"btn back_button\" (click)=\"backTo()\"><i class=\"fa fa-fw fa-angle-left\"></i>Back</button>\r\n            </div>\r\n\r\n            <div class=\"card-content\">\r\n                <div class=\"member-detail\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <input #addBulk type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event)\"\r\n                                />\r\n\r\n                                <div class=\"card-header\">\r\n                                    <h2>Upload Points</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n                                        <label>Upload Type</label>\r\n                                        <form-select [(ngModel)]=\"type\" name=\"type\" [data]=\"pointType\"></form-select>\r\n                                        <label>Upload Your File Here</label>\r\n                                        <div class=\"member-bulk-update-container\">\r\n                                            <span *ngIf=\"selectedFile && prodOnUpload == false\">{{selectedFile.name}}</span>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group uploaded\" *ngIf=\"prodOnUpload == false && startUploading == true && cancel == false\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <div class=\"container-two-button\">\r\n                                            <div class=\"img-upload\" *ngIf=\"!selectedFile; else upload_image\">\r\n                                                <button class=\"btn btn-upload\" (click)=\"addBulk.click()\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span>upload</span>\r\n                                                </button>\r\n                                            </div>\r\n    \r\n                                            <div class=\"img-upload\" *ngIf=\"selectedFile\">\r\n                                                <button class=\"btn btn-submit\" (click)=\"updateDataBulk()\">\r\n                                                    <i class=\"fa fa-fw fa-save\"></i>\r\n                                                    <span>submit</span>\r\n                                                </button>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <ng-template #upload_image>\r\n                                        <button class=\"btn btn-upload\" (click)=\"addBulk.click()\">\r\n                                            <i class=\"fa fa-fw fa-upload\"></i>\r\n                                            <span>re-upload</span>\r\n                                        </button>\r\n                                    </ng-template>\r\n\r\n                                    <div class=\"col-md-12 col-sm-12 error-msg-upload\">\r\n                                        <div *ngIf=\"errorFile\"><mark>Error: {{errorFile}}</mark></div>                       \r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/points-transaction-report/detail/points-transaction-report.detail.component.html":
/*!*******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/points-transaction-report/detail/points-transaction-report.detail.component.html ***!
  \*******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"pointStockDetail\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToHere()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n    </div>\r\n\r\n    <ng-template #empty_image><span>\r\n        empty\r\n    </span></ng-template>\r\n  \r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Stock Detail</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>Process Number</label>\r\n                                <div name=\"member_id\">{{pointStockDetail.process_number}}</div>\r\n\r\n                                <!-- <label>Process Type</label>\r\n                                <div name=\"member_id\">{{pointStockDetail.process_type}}</div> -->\r\n\r\n                                <!-- <label>Created Date</label>\r\n                                <div name=\"member_id\">{{pointStockDetail.created_date}}</div> -->\r\n\r\n                                <!-- <label>Updated Date</label>\r\n                                <div name=\"member_id\">{{pointStockDetail.updated_date}}</div> -->\r\n\r\n                                <label>Total Transaction</label>\r\n                                <div name=\"member_id\">{{pointStockDetail.total_transaction}}</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Approval Status</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>Status</label>\r\n                                <div name=\"member_id\">{{pointStockDetail.status}}</div>\r\n                                \r\n                                <div class=\"need-approval-text\" *ngIf=\"pointStockDetail.status == 'REQUESTED' && !processNow && !declineNow\">\r\n                                    <span>This request needs approval</span>\r\n                                </div>\r\n\r\n                                <div class=\"need-approval-text\" *ngIf=\"pointStockDetail.status == 'REQUESTED' && processNow && !declineNow\">\r\n                                    <span>Are you sure to process this request?</span>\r\n                                </div>\r\n\r\n                                <div class=\"need-approval-text\" *ngIf=\"pointStockDetail.status == 'REQUESTED' && !processNow && declineNow\">\r\n                                    <span>Are you sure to decline this request?</span>\r\n                                </div>\r\n\r\n                                <div style=\"display: flex;\">\r\n                                    <div class=\"btn-action-stock\" *ngIf=\"pointStockDetail.status == 'REQUESTED' && !processNow && !declineNow\">\r\n                                        <button class=\"btn btn-approve\" (click)=\"isProcessNow()\">Process</button>\r\n                                    </div>\r\n    \r\n                                    <div class=\"btn-action-stock\" *ngIf=\"pointStockDetail.status == 'REQUESTED' && !processNow && !declineNow\">\r\n                                        <button class=\"btn btn-decline\" (click)=\"isDeclineNow()\">Decline</button>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"btn-action-stock\" *ngIf=\"pointStockDetail.status == 'REQUESTED' && processNow && !declineNow\">\r\n                                    <button class=\"btn btn-approve\" (click)=\"processPoint(pointStockDetail.process_number, 'approve')\">Process Now</button>\r\n                                    <button class=\"btn btn-cancel\" (click)=\"isProcessNow()\">Cancel</button>\r\n                                </div>\r\n\r\n                                <div class=\"btn-action-stock\" *ngIf=\"pointStockDetail.status == 'REQUESTED' && !processNow && declineNow\">\r\n                                    <button class=\"btn btn-decline\" (click)=\"processPoint(pointStockDetail.process_number, 'cancel')\">Decline Now</button>\r\n                                    <button class=\"btn btn-cancel\" (click)=\"isDeclineNow()\">Cancel</button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n               \r\n            </div>\r\n         </div>\r\n    </div>\r\n    \r\n</div>\r\n\r\n<div *ngIf=\"points&&pointDetail==false &&!errorMessage\">\r\n    <app-form-builder-table \r\n    [tableFormat] = \"tableFormat\" \r\n    [table_data]  = \"points\" \r\n    [searchCallback]= \"[service, 'searchPointstransactionReportProcessLint',this]\" \r\n    [total_page]=\"totalPage\"\r\n    >\r\n    </app-form-builder-table>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/points-transaction-report/points-transaction-report.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/points-transaction-report/points-transaction-report.component.html ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'Point'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n  <div class=\"container-swapper-option\" *ngIf=\"pointDetail==false&&!errorMessage\">\r\n    <span style=\"margin-right: 10px;\">View</span> \r\n    <div class=\"button-container\">\r\n    <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n        <span class=\"fa fa-table\"> Requested</span>\r\n    </button>\r\n    <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n        <span class=\"fa fa-table\"> Processed</span>\r\n    </button>\r\n    </div>\r\n  </div>\r\n\r\n  <div *ngIf=\"points&&pointDetail==false && swaper &&!errorMessage\">\r\n        <app-form-builder-table \r\n        [tableFormat] = \"tableFormat\" \r\n        [table_data]  = \"points\" \r\n        [searchCallback]= \"[service, 'searchPointstransactionReportProcessLint',this]\" \r\n        [total_page]=\"totalPage\"\r\n        >\r\n        </app-form-builder-table>\r\n  </div>\r\n\r\n  <div *ngIf=\"points&&pointDetail==false && !swaper &&!errorMessage\">\r\n    <app-form-builder-table \r\n    [tableFormat] = \"tableFormat\" \r\n    [table_data]  = \"points\" \r\n    [searchCallback]= \"[service, 'searchPointstransactionReportProcessLint2',this]\" \r\n    [total_page]=\"totalPage\"\r\n    >\r\n        \r\n    </app-form-builder-table>\r\n  </div>\r\n  <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n    <div class=\"text-message\">\r\n      <i class=\"fas fa-ban\"></i>\r\n      {{errorMessage}}\r\n    </div>\r\n  </div>\r\n  <div *ngIf=\"pointDetail\">\r\n    <app-points-transaction-report-detail [back]=\"[this,backToHere]\" [detail]=\"pointDetail\"></app-points-transaction-report-detail>\r\n</div>\r\n \r\n</div>\r\n\r\n<!-- <div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  <div class=\"container-swapper-option\" *ngIf=\"pointDetail==false&&!errorMessage\">\r\n      <span style=\"margin-right: 10px;\">View</span> \r\n      <div class=\"button-container\">\r\n      <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n          <span class=\"fa fa-table\"> Requested</span>\r\n      </button>\r\n      <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n          <span class=\"fa fa-table\"> Approved</span>\r\n      </button>\r\n      </div>\r\n  </div>\r\n\r\n  <div *ngIf=\"Evouchers && pointDetail==false && swaper &&!errorMessage\">\r\n        <app-form-builder-table \r\n        [tableFormat] = \"tableFormat\" \r\n        [table_data]  = \"Evouchers\" \r\n        [searchCallback]= \"[service, 'searchEvoucherStockReport',this]\" \r\n        [total_page]=\"totalPage\"\r\n        >\r\n            \r\n        </app-form-builder-table>\r\n  </div>\r\n\r\n  <div *ngIf=\"Evouchers && evoucherStockDetail==false && !swaper &&!errorMessage\">\r\n      <app-form-builder-table \r\n      [tableFormat] = \"tableFormat\" \r\n      [table_data]  = \"Evouchers\" \r\n      [searchCallback]= \"[service, 'searchEvoucherStockReport2',this]\" \r\n      [total_page]=\"totalPage\"\r\n      >\r\n          \r\n      </app-form-builder-table>\r\n  </div>\r\n\r\n  <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n      <div class=\"text-message\">\r\n          <i class=\"fas fa-ban\"></i>\r\n          {{errorMessage}}\r\n      </div>\r\n  </div>\r\n\r\n  <div *ngIf=\"evoucherStockDetail\">\r\n      <app-evoucher-stock-report-detail [back]=\"[this,backToHere]\" [detail]=\"evoucherStockDetail\"></app-evoucher-stock-report-detail>\r\n  </div>\r\n\r\n \r\n</div> -->\r\n\r\n \r\n\r\n "

/***/ }),

/***/ "./src/app/layout/modules/points-transaction-report/add/points-transaction-report.add.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-report/add/points-transaction-report.add.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.ng-pristine {\n  color: white !important;\n}\n\n.ng-pristine {\n  color: black !important;\n  width: 100%;\n  border-radius: 5px;\n  padding: 6px 0px;\n}\n\n.member-bulk-update-container {\n  padding: 10px !important;\n}\n\n.container-two-button {\n  display: flex;\n}\n\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n\n.file-selected {\n  color: red;\n}\n\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n\n.btn-upload, .btn-submit {\n  margin-right: 10px;\n}\n\n.btn-upload {\n  border-color: black;\n}\n\n.btn-submit {\n  background-color: #3498db;\n  color: white;\n}\n\n.star-required {\n  color: red;\n}\n\n.text-required {\n  color: red;\n  font-size: 11px;\n  margin-top: -15px;\n}\n\nlabel {\n  color: black;\n}\n\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 10px;\n  background-color: #3498db;\n  color: white;\n  height: 30px;\n  align-items: center;\n  display: flex;\n  right: 10px;\n  padding-top: 0px;\n  padding-right: 10px;\n  padding-bottom: 0;\n  padding-left: 10px;\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 0px;\n  border: 1px solid #f8f9fa;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n\n.save-button {\n  background-color: #3498db;\n  color: white;\n  margin-top: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcG9pbnRzLXRyYW5zYWN0aW9uLXJlcG9ydC9hZGQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxwb2ludHMtdHJhbnNhY3Rpb24tcmVwb3J0XFxhZGRcXHBvaW50cy10cmFuc2FjdGlvbi1yZXBvcnQuYWRkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wb2ludHMtdHJhbnNhY3Rpb24tcmVwb3J0L2FkZC9wb2ludHMtdHJhbnNhY3Rpb24tcmVwb3J0LmFkZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0EsYUFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7QUNDSjs7QURFQTtFQUNJLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURFQTtFQUNJLHdCQUFBO0FDQ0o7O0FERUE7RUFDSSxhQUFBO0FDQ0o7O0FERUE7RUFDSSxnQkFBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7QUNDSjs7QURFQTtFQUNJLG1CQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtBQ0NKOztBREVBO0VBQ0ksbUJBQUE7QUNDSjs7QURFQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVJO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQ0NSOztBREFRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0NaOztBREFZO0VBQ0ksaUJBQUE7QUNFaEI7O0FEQ1E7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNBWjs7QURHSTtFQUNJLGFBQUE7QUNEUjs7QURFUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNBWjs7QURJSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDRlI7O0FES1E7RUFDSSxrQkFBQTtBQ0haOztBREtRO0VBQ0ksZ0JBQUE7QUNIWjs7QURLUTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNIWjs7QURPUTtFQUNJLHNCQUFBO0FDTFo7O0FETVk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ0poQjs7QURVQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDUEo7O0FEWUE7RUFDSSxnQkFBQTtFQUVBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUNUSjs7QURXQTtFQUNRLG1CQUFBO0VBRUEsYUFBQTtBQ1RSOztBRG9CQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FDakJKIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcG9pbnRzLXRyYW5zYWN0aW9uLXJlcG9ydC9hZGQvcG9pbnRzLXRyYW5zYWN0aW9uLXJlcG9ydC5hZGQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmctZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5uZy1wcmlzdGluZXtcclxuICAgIGNvbG9yIDogd2hpdGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm5nLXByaXN0aW5le1xyXG4gICAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIHBhZGRpbmc6IDZweCAwcHg7XHJcbn1cclxuXHJcbi5tZW1iZXItYnVsay11cGRhdGUtY29udGFpbmVyIHtcclxuICAgIHBhZGRpbmc6IDEwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNvbnRhaW5lci10d28tYnV0dG9uIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5lcnJvci1tc2ctdXBsb2FkIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5maWxlLXNlbGVjdGVkIHtcclxuICAgIGNvbG9yOiByZWQ7XHJcbn1cclxuXHJcbi5idG4tYWRkLWJ1bGsge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLmJ0bi11cGxvYWQsIC5idG4tc3VibWl0IHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuLmJ0bi11cGxvYWQge1xyXG4gICAgYm9yZGVyLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLmJ0bi1zdWJtaXQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLnN0YXItcmVxdWlyZWQge1xyXG4gICAgY29sb3I6IHJlZDtcclxufVxyXG5cclxuLnRleHQtcmVxdWlyZWQge1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIGZvbnQtc2l6ZTogMTFweDtcclxuICAgIG1hcmdpbi10b3A6IC0xNXB4O1xyXG59XHJcblxyXG5sYWJlbHtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG4uY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtaW4taGVpZ2h0OiA0OHB4O1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiAxMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgIC8vIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5tZW1iZXItZGV0YWlse1xyXG4gICAgICAgIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWFsO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNmOGY5ZmE7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBcclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbi5yb3VuZGVkLWJ0biB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgYmFja2dyb3VuZDogIzU2YTRmZjtcclxuICAgIC8vIG1hcmdpbi1sZWZ0OiA0MCU7XHJcbiAgICAvLyBtYXJnaW4tcmlnaHQ6IDUwJTtcclxuICAgIH1cclxuXHJcbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBsaW5lLWhlaWdodDogNDBweDtcclxuICAgIHBhZGRpbmc6IDAgNDBweDtcclxuICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICB9XHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogZGFya2VuKCM1NkE0RkYsIDE1JSk7XHJcbiAgICAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgfVxyXG4vLyAuZGF0ZXBpY2tlci1pbnB1dHtcclxuLy8gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsgICAgXHJcbi8vICAgICB9XHJcbi8vIC5mb3JtLWNvbnRyb2x7XHJcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vICAgICB0b3A6IDEwcHg7XHJcbi8vICAgICBsZWZ0OiAwcHg7XHJcbi8vIH1cclxuXHJcbi5zYXZlLWJ1dHRvbiB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxufVxyXG4iLCIuYmctZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLm5nLXByaXN0aW5lIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG59XG5cbi5uZy1wcmlzdGluZSB7XG4gIGNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiA2cHggMHB4O1xufVxuXG4ubWVtYmVyLWJ1bGstdXBkYXRlLWNvbnRhaW5lciB7XG4gIHBhZGRpbmc6IDEwcHggIWltcG9ydGFudDtcbn1cblxuLmNvbnRhaW5lci10d28tYnV0dG9uIHtcbiAgZGlzcGxheTogZmxleDtcbn1cblxuLmVycm9yLW1zZy11cGxvYWQge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcbn1cblxuLmZpbGUtc2VsZWN0ZWQge1xuICBjb2xvcjogcmVkO1xufVxuXG4uYnRuLWFkZC1idWxrIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cblxuLmJ0bi11cGxvYWQsIC5idG4tc3VibWl0IHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG4uYnRuLXVwbG9hZCB7XG4gIGJvcmRlci1jb2xvcjogYmxhY2s7XG59XG5cbi5idG4tc3VibWl0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uc3Rhci1yZXF1aXJlZCB7XG4gIGNvbG9yOiByZWQ7XG59XG5cbi50ZXh0LXJlcXVpcmVkIHtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBtYXJnaW4tdG9wOiAtMTVweDtcbn1cblxubGFiZWwge1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWluLWhlaWdodDogNDhweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICByaWdodDogMTBweDtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDA7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBwYWRkaW5nOiAwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmOGY5ZmE7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5yb3VuZGVkLWJ0biB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG59XG5cbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBjb2xvcjogIzU1NTtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi5yb3VuZGVkLWJ0bjpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMwYTdiZmY7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbi5zYXZlLWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/points-transaction-report/add/points-transaction-report.add.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-report/add/points-transaction-report.add.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: PointsTransationReportAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsTransationReportAddComponent", function() { return PointsTransationReportAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
/* harmony import */ var _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/points-transaction/points-transaction.service */ "./src/app/services/points-transaction/points-transaction.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var PointsTransationReportAddComponent = /** @class */ (function () {
    function PointsTransationReportAddComponent(evoucherService, pointsTransactionService) {
        this.evoucherService = evoucherService;
        this.pointsTransactionService = pointsTransactionService;
        this.name = "";
        this.Evouchers = [];
        this.errorLabel = false;
        this.isBulkUpdate = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.prodOnUpload = false;
        this.startUploading = false;
        this.showUploadButton = false;
        this.successUpload = false;
        this.urlDownload = "";
        this.warnIDPelanggan = false;
        this.warnNamaPemilik = false;
        this.warnTelpPemilik = false;
        this.warnWAPemilik = false;
        this.showPasswordPage = false;
        this.pointType = [
            { label: 'Add', value: 'add', selected: 1 },
            { label: 'Adjust In', value: 'adj_in' },
            { label: 'Adjust Out', value: 'adj_out' }
        ];
        this.type = "add";
    }
    PointsTransationReportAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    PointsTransationReportAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.selectedFile = null;
                this.startUploading = false;
                return [2 /*return*/];
            });
        });
    };
    PointsTransationReportAddComponent.prototype.updateDataBulk = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.startUploading = true;
                        this.cancel = false;
                        payload = {
                            type: 'application/form-data',
                        };
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.pointsTransactionService.uploadPointsCodeBulk(this.selectedFile, this, payload, this.type)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            this.firstLoad();
                        }
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.startUploading = false;
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    // async formSubmitAddMember(){
    //     let form_add = this.form;
    //     try {
    //       this.service    = this.evoucherService;
    //       const result: any  = await this.evoucherService.uploadEvoucherCodeBulk(form_add);
    //       console.warn("result add member", result)
    //       if (result && result.data) {
    //         this.getPassword = result.data.password;
    //       }
    //       this.data.username = this.form.id_pel;
    //       this.data.password = this.getPassword;
    //       Swal.fire({
    //         title: 'Success',
    //         text: 'Pelanggan berhasil ditambahkan',
    //         icon: 'success',
    //         confirmButtonText: 'Ok',
    //       }).then((result) => {
    //         if(result.isConfirmed){
    //           this.showThisPassword();
    //         }
    //       });
    //     } catch (e) {
    //       console.warn("error", e.message)
    //       Swal.fire({
    //         title: 'Failed',
    //         text: e.message,
    //         icon: 'warning',
    //         confirmButtonText: 'Ok',
    //       }).then((result) => {
    //         if(result.isConfirmed){
    //           this.firstLoad();
    //         }
    //       });
    //       this.errorLabel = ((<Error>e).message);//conversion to Error type
    //     }
    // }
    PointsTransationReportAddComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        // var reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]); //
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
        this.addBulk.nativeElement.value = '';
    };
    PointsTransationReportAddComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    PointsTransationReportAddComponent.prototype.actionShowUploadButton = function () {
        this.showUploadButton = !this.showUploadButton;
    };
    PointsTransationReportAddComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    PointsTransationReportAddComponent.prototype.backTo = function () {
        window.history.back();
    };
    PointsTransationReportAddComponent.ctorParameters = function () { return [
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__["EVoucherService"] },
        { type: _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_3__["PointsTransactionService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PointsTransationReportAddComponent.prototype, "back", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PointsTransationReportAddComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('addBulk', { static: false }),
        __metadata("design:type", Object)
    ], PointsTransationReportAddComponent.prototype, "addBulk", void 0);
    PointsTransationReportAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-points-transaction-report-add',
            template: __webpack_require__(/*! raw-loader!./points-transaction-report.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/points-transaction-report/add/points-transaction-report.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./points-transaction-report.add.component.scss */ "./src/app/layout/modules/points-transaction-report/add/points-transaction-report.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__["EVoucherService"], _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_3__["PointsTransactionService"]])
    ], PointsTransationReportAddComponent);
    return PointsTransationReportAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/points-transaction-report/detail/points-transaction-report.detail.component.scss":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-report/detail/points-transaction-report.detail.component.scss ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.need-approval-text {\n  color: red;\n  font-size: 14px;\n  margin-bottom: 15px;\n}\n\n.btn-action-stock {\n  display: flex;\n}\n\n.btn-action-stock .btn-approve {\n  background-color: #3498db;\n  color: white;\n  margin-right: 10px;\n  font-size: 14px;\n}\n\n.btn-action-stock .btn-decline {\n  font-size: 14px;\n  margin-right: 10px;\n  background-color: red;\n  color: white;\n}\n\n.btn-action-stock .btn-cancel {\n  font-size: 14px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcG9pbnRzLXRyYW5zYWN0aW9uLXJlcG9ydC9kZXRhaWwvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxwb2ludHMtdHJhbnNhY3Rpb24tcmVwb3J0XFxkZXRhaWxcXHBvaW50cy10cmFuc2FjdGlvbi1yZXBvcnQuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wb2ludHMtdHJhbnNhY3Rpb24tcmVwb3J0L2RldGFpbC9wb2ludHMtdHJhbnNhY3Rpb24tcmVwb3J0LmRldGFpbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0EsYUFBQTtBQ0NKOztBREdJO0VBQ0ksa0JBQUE7QUNBUjs7QURDUTtFQUVJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDRFo7O0FERVk7RUFDSSxpQkFBQTtBQ0FoQjs7QURHUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUVBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUlBLFdBQUE7QUNMWjs7QURFWTtFQUNJLGlCQUFBO0FDQWhCOztBRElRO0VBQ0ksc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ0ZaOztBREtJO0VBQ0ksYUFBQTtBQ0hSOztBRElRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0ZaOztBRGlCSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDZlI7O0FEa0JRO0VBQ0ksa0JBQUE7QUNoQlo7O0FEa0JRO0VBQ0ksZ0JBQUE7QUNoQlo7O0FEa0JRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2hCWjs7QURtQlE7RUFDSSxzQkFBQTtBQ2pCWjs7QURrQlk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ2hCaEI7O0FEdUJBO0VBQ0ksVUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ3BCSjs7QUR1QkE7RUFDSSxhQUFBO0FDcEJKOztBRHFCSTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ25CUjs7QURxQkk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7QUNuQlI7O0FEcUJJO0VBQ0ksZUFBQTtBQ25CUiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3BvaW50cy10cmFuc2FjdGlvbi1yZXBvcnQvZGV0YWlsL3BvaW50cy10cmFuc2FjdGlvbi1yZXBvcnQuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWRlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4uY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmVkaXRfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmJ0bi1yaWdodCB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIC5pbWFnZXtcclxuICAgIC8vICAgICBoM3tcclxuICAgIC8vICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgLy8gICAgIH1cclxuICAgIC8vICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgLy8gICAgICAgICBpbWd7XHJcbiAgICAvLyAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAvLyAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAvLyAgICAgICAgIH1cclxuICAgIC8vICAgICB9XHJcbiAgICAvLyB9XHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5tZW1iZXItZGV0YWlse1xyXG4gICAgICAgIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5uZWVkLWFwcHJvdmFsLXRleHQge1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbn1cclxuXHJcbi5idG4tYWN0aW9uLXN0b2NrIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAuYnRuLWFwcHJvdmUge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB9XHJcbiAgICAuYnRuLWRlY2xpbmUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuICAgIC5idG4tY2FuY2VsIHtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB9XHJcbn0iLCIuYmctZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5idG4tcmlnaHQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ubmVlZC1hcHByb3ZhbC10ZXh0IHtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uYnRuLWFjdGlvbi1zdG9jayB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uYnRuLWFjdGlvbi1zdG9jayAuYnRuLWFwcHJvdmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLmJ0bi1hY3Rpb24tc3RvY2sgLmJ0bi1kZWNsaW5lIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmJ0bi1hY3Rpb24tc3RvY2sgLmJ0bi1jYW5jZWwge1xuICBmb250LXNpemU6IDE0cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/points-transaction-report/detail/points-transaction-report.detail.component.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-report/detail/points-transaction-report.detail.component.ts ***!
  \***************************************************************************************************************/
/*! exports provided: PointsTransactionReportDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsTransactionReportDetailComponent", function() { return PointsTransactionReportDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
/* harmony import */ var _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/points-transaction/points-transaction.service */ "./src/app/services/points-transaction/points-transaction.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var PointsTransactionReportDetailComponent = /** @class */ (function () {
    function PointsTransactionReportDetailComponent(evoucherService, route, router, pointsTransactionService) {
        this.evoucherService = evoucherService;
        this.route = route;
        this.router = router;
        this.pointsTransactionService = pointsTransactionService;
        this.processNow = false;
        this.declineNow = false;
        this.edit = false;
        this.errorLabel = false;
        this.points = [];
        this.row_id = "process_number";
        this.errorMessage = false;
        this.pointDetail = false;
        this.tableFormat = {
            title: 'Point Transaction Report',
            label_headers: [
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'Process Number', visible: true, type: 'string', data_row_name: 'process_number' },
                { label: 'Process ID', visible: true, type: 'string', data_row_name: 'process_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
                { label: 'Nama Pelanggan', visible: true, type: 'string', data_row_name: 'full_name' },
                { label: 'Points', visible: true, type: 'string', data_row_name: 'points' },
                { label: 'Description', visible: true, type: 'string', data_row_name: 'description' },
                { label: 'Remarks', visible: true, type: 'string', data_row_name: 'remarks' },
                { label: 'Process Type', visible: true, type: 'string', data_row_name: 'process_type' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Transaction Month', visible: true, type: 'string', data_row_name: 'transaction_month' },
            ],
            row_primary_key: '_id',
            formOptions: {
                // addForm   : true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'points',
                detail_function: [this, 'callDetail']
            }
        };
    }
    PointsTransactionReportDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    PointsTransactionReportDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.pointStockDetail = this.detail;
                        this.processNumber = this.detail.process_number;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        this.service = this.pointsTransactionService;
                        return [4 /*yield*/, this.pointsTransactionService.searchPointstransactionProcessReport(this.processNumber)];
                    case 2:
                        result = _a.sent();
                        console.warn("result??", result);
                        this.totalPage = result.total_page;
                        this.points = result.values;
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    PointsTransactionReportDetailComponent.prototype.backToHere = function () {
        // this.evoucherStockDetail = false;
        this.back[1](this.back[0]);
        // this.firstLoad();
    };
    PointsTransactionReportDetailComponent.prototype.isProcessNow = function () {
        this.processNow = !this.processNow;
    };
    PointsTransactionReportDetailComponent.prototype.isDeclineNow = function () {
        this.declineNow = !this.declineNow;
    };
    PointsTransactionReportDetailComponent.prototype.processPoint = function (process_number, status) {
        return __awaiter(this, void 0, void 0, function () {
            var dataProcess, result, e_2, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        dataProcess = {
                            "process_number": process_number,
                            "status": status
                        };
                        return [4 /*yield*/, this.pointsTransactionService.processPoint(dataProcess)];
                    case 1:
                        result = _a.sent();
                        console.warn("result process", result);
                        if (result) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                                title: 'Success',
                                text: 'Point is Processed',
                                icon: 'success',
                                confirmButtonText: 'Ok',
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    _this.backToHere();
                                }
                            });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        console.warn("error appove", e_2);
                        // alert("Error");
                        this.errorLabel = (e_2.message);
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PointsTransactionReportDetailComponent.ctorParameters = function () { return [
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_4__["EVoucherService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_5__["PointsTransactionService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PointsTransactionReportDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PointsTransactionReportDetailComponent.prototype, "back", void 0);
    PointsTransactionReportDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-points-transaction-report-detail',
            template: __webpack_require__(/*! raw-loader!./points-transaction-report.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/points-transaction-report/detail/points-transaction-report.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_3__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./points-transaction-report.detail.component.scss */ "./src/app/layout/modules/points-transaction-report/detail/points-transaction-report.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_4__["EVoucherService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_5__["PointsTransactionService"]])
    ], PointsTransactionReportDetailComponent);
    return PointsTransactionReportDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/points-transaction-report/points-transaction-report.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-report/points-transaction-report.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "iframe {\n  width: 0px;\n  height: 0px;\n  border: 0px medium none;\n  visibility: hidden;\n}\n\n.container-swapper-option {\n  display: flex;\n  justify-content: center;\n  flex-direction: row;\n  align-items: center;\n}\n\n.container-swapper-option .button-container {\n  border: 2px solid #ddd;\n  border-radius: 8px;\n}\n\n.container-swapper-option .button-container button {\n  color: #ddd;\n  background: white;\n  font-size: 14px;\n  border-radius: 8px;\n}\n\n.container-swapper-option .button-container button.true {\n  background: #2480fb;\n  border-radius: 8px;\n  color: #ddd;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcG9pbnRzLXRyYW5zYWN0aW9uLXJlcG9ydC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXHBvaW50cy10cmFuc2FjdGlvbi1yZXBvcnRcXHBvaW50cy10cmFuc2FjdGlvbi1yZXBvcnQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3BvaW50cy10cmFuc2FjdGlvbi1yZXBvcnQvcG9pbnRzLXRyYW5zYWN0aW9uLXJlcG9ydC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBREVBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0NKOztBREFJO0VBRUUsc0JBQUE7RUFDQSxrQkFBQTtBQ0NOOztBREFNO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDRVI7O0FEQU07RUFFRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0NSOztBRElBO0VBQ0ksaUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNESjs7QURHSTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUNEUiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3BvaW50cy10cmFuc2FjdGlvbi1yZXBvcnQvcG9pbnRzLXRyYW5zYWN0aW9uLXJlcG9ydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlmcmFtZXtcclxuICAgIHdpZHRoOjBweDtcclxuICAgIGhlaWdodDogMHB4O1xyXG4gICAgYm9yZGVyOjBweCBtZWRpdW0gbm9uZTtcclxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxufVxyXG5cclxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC5idXR0b24tY29udGFpbmVyIHtcclxuXHJcbiAgICAgIGJvcmRlcjogMnB4IHNvbGlkICNkZGQ7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6OHB4O1xyXG4gICAgICBidXR0b24ge1xyXG4gICAgICAgIGNvbG9yOiAjZGRkO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgIH1cclxuICAgICAgYnV0dG9uLnRydWUge1xyXG4gICAgICAgXHJcbiAgICAgICAgYmFja2dyb3VuZDogIzI0ODBmYjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgICAgY29sb3I6ICNkZGQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4uZXJyb3ItbWVzc2FnZSB7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGhlaWdodDoxMDB2aDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZToyNnB4O1xyXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XHJcblxyXG4gICAgLnRleHQtbWVzc2FnZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0U3NEMzQztcclxuICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgIG1hcmdpbjogMCAxMHB4O1xyXG4gICAgfVxyXG59IiwiaWZyYW1lIHtcbiAgd2lkdGg6IDBweDtcbiAgaGVpZ2h0OiAwcHg7XG4gIGJvcmRlcjogMHB4IG1lZGl1bSBub25lO1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG59XG5cbi5jb250YWluZXItc3dhcHBlci1vcHRpb24ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5jb250YWluZXItc3dhcHBlci1vcHRpb24gLmJ1dHRvbi1jb250YWluZXIge1xuICBib3JkZXI6IDJweCBzb2xpZCAjZGRkO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG59XG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIGJ1dHRvbiB7XG4gIGNvbG9yOiAjZGRkO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG59XG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIGJ1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZDogIzI0ODBmYjtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBjb2xvcjogI2RkZDtcbn1cblxuLmVycm9yLW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDI2cHg7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLmVycm9yLW1lc3NhZ2UgLnRleHQtbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6ICNFNzRDM0M7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIG1hcmdpbjogMCAxMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/points-transaction-report/points-transaction-report.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-report/points-transaction-report.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: PointsTransationReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsTransationReportComponent", function() { return PointsTransationReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/points-transaction/points-transaction.service */ "./src/app/services/points-transaction/points-transaction.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;
var PointsTransationReportComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function PointsTransationReportComponent(sanitizer, PointsTransactionService) {
        this.sanitizer = sanitizer;
        this.PointsTransactionService = PointsTransactionService;
        this.points = [];
        this.row_id = "process_number";
        this.swaper = true;
        this.tableFormat = {
            title: 'Point Transaction Approval',
            label_headers: [
                { label: 'Process Number', visible: true, type: 'string', data_row_name: 'process_number' },
                { label: 'Process Type', visible: true, type: 'string', data_row_name: 'process_type' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Total Transaction', visible: true, type: 'string', data_row_name: 'total_transaction' },
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date' },
            ],
            row_primary_key: '_id',
            formOptions: {
                addForm: true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'points',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.errorMessage = false;
        this.pointDetail = false;
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    PointsTransationReportComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    PointsTransationReportComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        // let params = {
                        //   'type':'member'
                        // }
                        this.service = this.PointsTransactionService;
                        result = void 0;
                        if (!(this.swaper == true)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.PointsTransactionService.getPointstransactionReportRequestedLint('REQUESTED')];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.PointsTransactionService.getPointstransactionReportProcessLint('PROCESSED', 'CANCELED', 'CANCELLED')];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        // const result: any  = await this.PointsTransactionService.getPointstransactionReportProcessLint();
                        console.warn("form member", result);
                        this.totalPage = result.total_page;
                        this.points = result.values;
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    PointsTransationReportComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    // console.warn("id search",rowData.id_pel);
                    // console.warn("members", this.Members);
                    // this.service    = this.memberService;
                    // let result: any = await this.memberService.detailMember(_id);
                    // this.memberDetail = result.result[0];
                    this.pointDetail = this.points.find(function (point) { return point.process_number == rowData.process_number; });
                    // console.log("pointDetail", this.pointDetail);
                    // console.warn("Member detail",this.memberDetail);
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    PointsTransationReportComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.pointDetail = false;
                obj.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    PointsTransationReportComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
        this.firstLoad();
    };
    PointsTransationReportComponent.ctorParameters = function () { return [
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] },
        { type: _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_3__["PointsTransactionService"] }
    ]; };
    PointsTransationReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-form-member',
            template: __webpack_require__(/*! raw-loader!./points-transaction-report.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/points-transaction-report/points-transaction-report.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./points-transaction-report.component.scss */ "./src/app/layout/modules/points-transaction-report/points-transaction-report.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"], _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_3__["PointsTransactionService"]])
    ], PointsTransationReportComponent);
    return PointsTransationReportComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/points-transaction-report/points-transaction-report.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-report/points-transaction-report.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: PointsTransactionReportModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsTransactionReportModule", function() { return PointsTransactionReportModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _points_transaction_report_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./points-transaction-report.component */ "./src/app/layout/modules/points-transaction-report/points-transaction-report.component.ts");
/* harmony import */ var _add_points_transaction_report_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/points-transaction-report.add.component */ "./src/app/layout/modules/points-transaction-report/add/points-transaction-report.add.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _points_transaction_report_routing__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./points-transaction-report.routing */ "./src/app/layout/modules/points-transaction-report/points-transaction-report.routing.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _detail_points_transaction_report_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./detail/points-transaction-report.detail.component */ "./src/app/layout/modules/points-transaction-report/detail/points-transaction-report.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var PointsTransactionReportModule = /** @class */ (function () {
    function PointsTransactionReportModule() {
    }
    PointsTransactionReportModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _points_transaction_report_routing__WEBPACK_IMPORTED_MODULE_8__["PointsTransationReportRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_9__["BsComponentModule"]
            ],
            declarations: [
                _points_transaction_report_component__WEBPACK_IMPORTED_MODULE_2__["PointsTransationReportComponent"],
                _add_points_transaction_report_add_component__WEBPACK_IMPORTED_MODULE_3__["PointsTransationReportAddComponent"],
                _detail_points_transaction_report_detail_component__WEBPACK_IMPORTED_MODULE_10__["PointsTransactionReportDetailComponent"],
            ],
        })
    ], PointsTransactionReportModule);
    return PointsTransactionReportModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/points-transaction-report/points-transaction-report.routing.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-report/points-transaction-report.routing.ts ***!
  \***********************************************************************************************/
/*! exports provided: PointsTransationReportRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsTransationReportRoutingModule", function() { return PointsTransationReportRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _points_transaction_report_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./points-transaction-report.component */ "./src/app/layout/modules/points-transaction-report/points-transaction-report.component.ts");
/* harmony import */ var _add_points_transaction_report_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/points-transaction-report.add.component */ "./src/app/layout/modules/points-transaction-report/add/points-transaction-report.add.component.ts");
/* harmony import */ var _detail_points_transaction_report_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detail/points-transaction-report.detail.component */ "./src/app/layout/modules/points-transaction-report/detail/points-transaction-report.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '', component: _points_transaction_report_component__WEBPACK_IMPORTED_MODULE_2__["PointsTransationReportComponent"],
    },
    {
        path: 'add', component: _add_points_transaction_report_add_component__WEBPACK_IMPORTED_MODULE_3__["PointsTransationReportAddComponent"]
    },
    {
        path: 'detail', component: _detail_points_transaction_report_detail_component__WEBPACK_IMPORTED_MODULE_4__["PointsTransactionReportDetailComponent"]
    },
];
var PointsTransationReportRoutingModule = /** @class */ (function () {
    function PointsTransationReportRoutingModule() {
    }
    PointsTransationReportRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], PointsTransationReportRoutingModule);
    return PointsTransationReportRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-points-transaction-report-points-transaction-report-module.js.map