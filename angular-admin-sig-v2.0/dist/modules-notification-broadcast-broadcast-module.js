(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-notification-broadcast-broadcast-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notification.broadcast/add/broadcast.add.component.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/notification.broadcast/add/broadcast.add.component.html ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n  <app-page-header [heading]=\"'Add New Notification Setting'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  <div class=\"error\" *ngIf=\"errorLabel\">{{errorLabel}}</div>\r\n  <div style=\"display: flex; margin-bottom: 20px;\">\r\n    <div class=\"col-md-5\">\r\n      <div class=\"card card-detail\">\r\n        <div class=\"card-header\">\r\n          <label class=\"uploading\">NOTIFICATION TYPE</label>\r\n        </div>\r\n        <div class=\"card-content\">\r\n          <div class=\"member-detail row\">\r\n            <div class=\"col-md-12\">\r\n              <label>Notification Type</label>\r\n          <div name=\"setting_id\">\r\n            <form-select name=\"setting_id\" [(ngModel)]=\"form.notification_type\" [data]=\"type_notif\" required>\r\n            </form-select>\r\n          </div>\r\n          <div *ngIf=\"form.notification_type == 'TOKEN'\">\r\n          <label>Member List</label>\r\n          <!-- <form-input name=\"org_id\" [placeholder]=\"'Logo'\" [type]=\"'text'\" [(ngModel)]=\"form.member_list\" required> -->\r\n            <div class=\"hashhash\">\r\n            <span class=\"hashtag-item\" *ngFor=\"let ht of form.member_list\">{{ht}}</span>\r\n              <div class=\"hashtagskey-input\" contenteditable=\"true\" [textContent]=\"hashtagsKey\"\r\n                (keyup)=\"hashtagsKeyDown($event)\"></div>\r\n              </div>\r\n            </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>   \r\n    <div class=\"col-md-7\">\r\n      <div class=\"card card-detail\">\r\n        <div class=\"card-header\">\r\n          <label class=\"uploading\">CTA TYPE</label>\r\n        </div>\r\n        <div class=\"card-content\">\r\n          <div class=\"member-detail row\">\r\n            <div class=\"col-md-12\">\r\n              <label>CTA type</label>\r\n              <div name=\"setting_id\">\r\n                <form-select name=\"setting_id\" [(ngModel)]=\"form.cta_type\" [data]=\"type_cta\" required></form-select>\r\n              </div>\r\n              <ng-container [ngSwitch]=\"form.cta_type\">\r\n                  <ng-container *ngSwitchCase=\"1 || 11 || 13\">\r\n                    <label>Order ID</label>\r\n                    <form-input name=\"org_id\" [placeholder]=\"'order ID'\" [type]=\"'text'\"\r\n                      [(ngModel)]=\"form.reference.order_id\" required></form-input>\r\n                  </ng-container>\r\n                  <ng-container *ngSwitchCase=\"2 || 3\">\r\n                    <label>Voucher ID</label>\r\n                    <form-input name=\"org_id\" [placeholder]=\"'voucher ID'\" [type]=\"'text'\"\r\n                      [(ngModel)]=\"form.reference.voucher_id\" required></form-input>  \r\n                  </ng-container>\r\n                  <ng-container *ngSwitchCase=\"14\">\r\n                    <label>Product ID</label>\r\n                    <form-input name=\"org_id\" [placeholder]=\"'product ID'\" [type]=\"'text'\"\r\n                      [(ngModel)]=\"form.reference.product_id\" required></form-input>\r\n                  </ng-container>\r\n                   <ng-container *ngSwitchCase=\"15\">\r\n                    <label>Search Value</label>\r\n                    <form-input name=\"org_id\" [placeholder]=\"'product ID'\" [type]=\"'text'\"\r\n                      [(ngModel)]=\"form.reference.category\" required></form-input>\r\n                  </ng-container>\r\n                  <ng-container *ngSwitchDefault></ng-container>\r\n              </ng-container>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>  \r\n  </div>\r\n  <div style=\"display: flex;\">\r\n  <div class=\"col-md-5\">\r\n  <div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n      <label class=\"uploading\">DETAIL NOTIFICATION</label>\r\n    </div>\r\n    <div class=\"card-content\">\r\n      <div class=\"member-detail row\">\r\n\r\n        <input #inputFile1 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'banner')\"\r\n          accept=\"image/x-png,image/gif,image/jpeg\">\r\n        <input #inputFile2 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'inbox')\"\r\n          accept=\"image/x-png,image/gif,image/jpeg\">\r\n        <input #inputFile3 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'notif')\"\r\n          accept=\"image/x-png,image/gif,image/jpeg\">\r\n        <input #inputFile4 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'logo')\"\r\n          accept=\"image/x-png,image/gif,image/jpeg\">\r\n          <input #inputFile5 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'icon')\"\r\n          accept=\"image/x-png,image/gif,image/jpeg\">\r\n        <div class=\"col-md-12\">\r\n          <label>Key</label>\r\n          <form-input name=\"org_id\" [placeholder]=\"'Key'\" [type]=\"'text'\" [(ngModel)]=\"form.key\" required></form-input>\r\n          <label>Inbox Title</label>\r\n          <form-input name=\"org_id\" [placeholder]=\"'Inbox Title'\" [type]=\"'text'\" [(ngModel)]=\"form.inbox_title\"\r\n            required></form-input>\r\n          <label>Inbox Message</label>\r\n          <form-input name=\"org_id\" [placeholder]=\"'Inbox Message'\" [type]=\"'text'\" [(ngModel)]=\"form.inbox_message\"\r\n            required></form-input>\r\n          <label>Notification Title</label>\r\n          <form-input name=\"org_id\" [placeholder]=\"'Notification Title'\" [type]=\"'text'\"\r\n            [(ngModel)]=\"form.notification_title\" required></form-input>\r\n          <label>Notification Message</label>\r\n          <form-input name=\"org_id\" [placeholder]=\"'Notification Message'\" [type]=\"'text'\"\r\n            [(ngModel)]=\"form.notification_message\" required></form-input>\r\n          <label>From</label>\r\n          <form-input name=\"org_id\" [placeholder]=\"'From'\" [type]=\"'text'\" [(ngModel)]=\"form.from\" required>\r\n          </form-input>\r\n\r\n          <label>Expiry Day</label>\r\n          <form-input name=\"org_id\" [placeholder]=\"'Expiry Day'\" [type]=\"'number'\" [(ngModel)]=\"form.expiry_day\"\r\n            required></form-input>\r\n         \r\n         \r\n\r\n          \r\n          <!-- </form-input> -->\r\n          <label>Option</label>\r\n          <div name=\"setting_id\">\r\n            <form-select name=\"setting_id\" [(ngModel)]=\"form.option\" [data]=\"type_option\" required></form-select>\r\n          </div>\r\n        </div>\r\n     \r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"col-md-7\">\r\n  <div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n      <label class=\"uploading\">IMAGES UPLOAD</label>\r\n    </div>\r\n    <div class=\"card-content\">\r\n      <div class=\"member-detail row\">\r\n        <div class=\"col-md-12\">\r\n\r\n          <label>Banner</label>\r\n          <div class=\"section-upload left-section-upload\">\r\n            <div class=\"col-md-12 imagess col-half-offset\">\r\n              <div class=\"card cl-list\">\r\n                <div class=\"card-content\" *ngIf=\"!uploads.banner && !showLoading1\">\r\n                  <div class=\"image\">\r\n                    <div class=\"card-content custom\">\r\n                      <div class=\"image-canvas\">\r\n                        <img *ngIf=\"!uploads.banner\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                      </div>\r\n                      <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                      <div class=\"browse-files\">\r\n                        <div class=\"pb-framer\">\r\n                          <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                          </div>\r\n                          <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                            {{cancel?'terminated':progressBar+'%'}} </span>\r\n                          <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                            {{cancel?'terminated':progressBar+'%'}} </span>\r\n                        </div>\r\n                        <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile1.click()\">+ Add Image</a>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <!-- <div *ngIf=\"errorLabel!==false\">\r\n                  {{errorLabel}}\r\n                </div> -->\r\n                </div>\r\n                <div class=\"form-group uploaded\" *ngIf=\"showLoading1\">\r\n                  <div class=\"loading\">\r\n                    <div class=\"sk-fading-circle\">\r\n                      <div class=\"sk-circle1 sk-circle\"></div>\r\n                      <div class=\"sk-circle2 sk-circle\"></div>\r\n                      <div class=\"sk-circle3 sk-circle\"></div>\r\n                      <div class=\"sk-circle4 sk-circle\"></div>\r\n                      <div class=\"sk-circle5 sk-circle\"></div>\r\n                      <div class=\"sk-circle6 sk-circle\"></div>\r\n                      <div class=\"sk-circle7 sk-circle\"></div>\r\n                      <div class=\"sk-circle8 sk-circle\"></div>\r\n                      <div class=\"sk-circle9 sk-circle\"></div>\r\n                      <div class=\"sk-circle10 sk-circle\"></div>\r\n                      <div class=\"sk-circle11 sk-circle\"></div>\r\n                      <div class=\"sk-circle12 sk-circle\"></div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"form-group uploaded\" *ngIf=\"uploads.banner && !showLoading1\">\r\n                  <!-- <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image3')\"></i> -->\r\n                  <div class=\"uploaded-image2\">\r\n                    <div class=\"img\">\r\n                      <img class=\"resize\" src=\"{{uploads.banner}}\">\r\n                      <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile1.click()\">Re-Upload</a>\r\n                    </div>\r\n\r\n                  </div>\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div style=\"display: flex\">\r\n            <div class=\"col-md-6\">\r\n              <div style=\"display: flex; justify-content: flex-start;align-items: center;\">\r\n              <div class=\"col-md-6\">  <label>Inbox Image</label> \r\n              </div>\r\n              <div  class=\"col-md-6\">\r\n                <mat-checkbox (change)=\"checkbox1()\" [(ngModel)]=\"activeCheckbox1\" type=\"checkbox\"\r\n                name=\"cbox\"><label>same as banner</label></mat-checkbox>\r\n              </div>\r\n            </div>\r\n            \r\n              <div class=\"section-upload row left-section-upload\">\r\n\r\n                <div class=\"col-md-12 imagess col-half-offset\">\r\n                  <div class=\"card cl-list\">\r\n                    <div class=\"card-content\" *ngIf=\"!uploads.inbox && !showLoading2\">\r\n                      <div class=\"image\">\r\n                        <div class=\"card-content custom\">\r\n                          <div class=\"image-canvas\">\r\n                            <img *ngIf=\"!uploads.inbox\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                          </div>\r\n                          <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                          <div class=\"browse-files\">\r\n                            <div class=\"pb-framer\">\r\n                              <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                              </div>\r\n                              <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                              <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                            </div>\r\n                            <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile2.click()\">+ Add Image</a>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <!-- <div *ngIf=\"errorLabel!==false\">\r\n                    {{errorLabel}}\r\n                  </div> -->\r\n                    </div>\r\n                    <div class=\"form-group uploaded\" *ngIf=\"showLoading2\">\r\n                      <div class=\"loading\">\r\n                        <div class=\"sk-fading-circle\">\r\n                          <div class=\"sk-circle1 sk-circle\"></div>\r\n                          <div class=\"sk-circle2 sk-circle\"></div>\r\n                          <div class=\"sk-circle3 sk-circle\"></div>\r\n                          <div class=\"sk-circle4 sk-circle\"></div>\r\n                          <div class=\"sk-circle5 sk-circle\"></div>\r\n                          <div class=\"sk-circle6 sk-circle\"></div>\r\n                          <div class=\"sk-circle7 sk-circle\"></div>\r\n                          <div class=\"sk-circle8 sk-circle\"></div>\r\n                          <div class=\"sk-circle9 sk-circle\"></div>\r\n                          <div class=\"sk-circle10 sk-circle\"></div>\r\n                          <div class=\"sk-circle11 sk-circle\"></div>\r\n                          <div class=\"sk-circle12 sk-circle\"></div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"form-group uploaded\" *ngIf=\"uploads.inbox && !showLoading2\">\r\n                      <!-- <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image3')\"></i> -->\r\n                      <div class=\"uploaded-image2\">\r\n                        <div class=\"img\">\r\n                          <img class=\"resize\" src=\"{{uploads.inbox}}\">\r\n                          <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile2.click()\">Re-Upload</a>\r\n                        </div>\r\n\r\n                      </div>\r\n\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <div style=\"display: flex; justify-content: flex-start;align-items: center;\">\r\n                <div class=\"col-md-6\">  <label>Notification Image</label> \r\n                </div>\r\n                <div  class=\"col-md-6\">\r\n                  <mat-checkbox (change)=\"checkbox2()\" [(ngModel)]=\"activeCheckbox2\" type=\"checkbox\"\r\n                  name=\"cbox\"><label>same as banner</label></mat-checkbox>\r\n                </div>\r\n              </div>\r\n              <div class=\"section-upload row left-section-upload\">\r\n                <div class=\"col-md-12 imagess col-half-offset\">\r\n                  <div class=\"card cl-list\">\r\n                    <div class=\"card-content\" *ngIf=\"!uploads.notif && !showLoading3\">\r\n                      <div class=\"image\">\r\n                        <div class=\"card-content custom\">\r\n                          <div class=\"image-canvas\">\r\n                            <img *ngIf=\"!uploads.notif\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                          </div>\r\n                          <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                          <div class=\"browse-files\">\r\n                            <div class=\"pb-framer\">\r\n                              <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                              </div>\r\n                              <span *ngIf=\"(progressBar < 58) && (progressBar>0)\">\r\n                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                              <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                            </div>\r\n                            <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile3.click()\">+ Add Image</a>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <!-- <div *ngIf=\"errorLabel!==false\">\r\n                    {{errorLabel}}\r\n                  </div> -->\r\n                    </div>\r\n                    <div class=\"form-group uploaded\" *ngIf=\"showLoading3\">\r\n                      <div class=\"loading\">\r\n                        <div class=\"sk-fading-circle\">\r\n                          <div class=\"sk-circle1 sk-circle\"></div>\r\n                          <div class=\"sk-circle2 sk-circle\"></div>\r\n                          <div class=\"sk-circle3 sk-circle\"></div>\r\n                          <div class=\"sk-circle4 sk-circle\"></div>\r\n                          <div class=\"sk-circle5 sk-circle\"></div>\r\n                          <div class=\"sk-circle6 sk-circle\"></div>\r\n                          <div class=\"sk-circle7 sk-circle\"></div>\r\n                          <div class=\"sk-circle8 sk-circle\"></div>\r\n                          <div class=\"sk-circle9 sk-circle\"></div>\r\n                          <div class=\"sk-circle10 sk-circle\"></div>\r\n                          <div class=\"sk-circle11 sk-circle\"></div>\r\n                          <div class=\"sk-circle12 sk-circle\"></div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"form-group uploaded\" *ngIf=\"uploads.notif && !showLoading3\">\r\n                      <!-- <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image3')\"></i> -->\r\n                      <div class=\"uploaded-image2\">\r\n                        <div class=\"img\">\r\n                          <img class=\"resize\" src=\"{{uploads.notif}}\">\r\n                          <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile3.click()\">Re-Upload</a>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div style=\"display: flex\">\r\n            <div class=\"col-md-6\">\r\n              <label>Logo</label>\r\n              <div class=\"section-upload row left-section-upload\">\r\n\r\n                <div class=\"col-md-12 imagess col-half-offset\">\r\n                  <div class=\"card cl-list\">\r\n                    <div class=\"card-content\" *ngIf=\"!uploads.logo && !showLoading4\">\r\n                      <div class=\"image\">\r\n                        <div class=\"card-content custom\">\r\n                          <div class=\"image-canvas\">\r\n                            <img *ngIf=\"!uploads.logo\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                          </div>\r\n                          <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                          <div class=\"browse-files\">\r\n                            <div class=\"pb-framer\">\r\n                              <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                              </div>\r\n                              <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                              <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                            </div>\r\n                            <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile4.click()\">+ Add Image</a>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <!-- <div *ngIf=\"errorLabel!==false\">\r\n                    {{errorLabel}}\r\n                  </div> -->\r\n                    </div>\r\n                    <div class=\"form-group uploaded\" *ngIf=\"showLoading4\">\r\n                      <div class=\"loading\">\r\n                        <div class=\"sk-fading-circle\">\r\n                          <div class=\"sk-circle1 sk-circle\"></div>\r\n                          <div class=\"sk-circle2 sk-circle\"></div>\r\n                          <div class=\"sk-circle3 sk-circle\"></div>\r\n                          <div class=\"sk-circle4 sk-circle\"></div>\r\n                          <div class=\"sk-circle5 sk-circle\"></div>\r\n                          <div class=\"sk-circle6 sk-circle\"></div>\r\n                          <div class=\"sk-circle7 sk-circle\"></div>\r\n                          <div class=\"sk-circle8 sk-circle\"></div>\r\n                          <div class=\"sk-circle9 sk-circle\"></div>\r\n                          <div class=\"sk-circle10 sk-circle\"></div>\r\n                          <div class=\"sk-circle11 sk-circle\"></div>\r\n                          <div class=\"sk-circle12 sk-circle\"></div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"form-group uploaded\" *ngIf=\"uploads.logo && !showLoading4\">\r\n                      <!-- <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image3')\"></i> -->\r\n                      <div class=\"uploaded-image2\">\r\n                        <div class=\"img\">\r\n                          <img class=\"resize\" src=\"{{uploads.logo}}\">\r\n                          <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile4.click()\">Re-Upload</a>\r\n                        </div>\r\n\r\n                      </div>\r\n\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <label>Icon</label>\r\n              <div class=\"section-upload row left-section-upload\">\r\n                <div class=\"col-md-12 imagess col-half-offset\">\r\n                  <div class=\"card cl-list\">\r\n                    <div class=\"card-content\" *ngIf=\"!uploads.icon && !showLoading5\">\r\n                      <div class=\"image\">\r\n                        <div class=\"card-content custom\">\r\n                          <div class=\"image-canvas\">\r\n                            <img *ngIf=\"!uploads.icon\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                          </div>\r\n                          <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                          <div class=\"browse-files\">\r\n                            <div class=\"pb-framer\">\r\n                              <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                              </div>\r\n                              <span *ngIf=\"(progressBar < 58) && (progressBar>0)\">\r\n                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                              <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                {{cancel?'terminated':progressBar+'%'}} </span>\r\n                            </div>\r\n                            <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile5.click()\">+ Add Image</a>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <!-- <div *ngIf=\"errorLabel!==false\">\r\n                    {{errorLabel}}\r\n                  </div> -->\r\n                    </div>\r\n                    <div class=\"form-group uploaded\" *ngIf=\"showLoading5\">\r\n                      <div class=\"loading\">\r\n                        <div class=\"sk-fading-circle\">\r\n                          <div class=\"sk-circle1 sk-circle\"></div>\r\n                          <div class=\"sk-circle2 sk-circle\"></div>\r\n                          <div class=\"sk-circle3 sk-circle\"></div>\r\n                          <div class=\"sk-circle4 sk-circle\"></div>\r\n                          <div class=\"sk-circle5 sk-circle\"></div>\r\n                          <div class=\"sk-circle6 sk-circle\"></div>\r\n                          <div class=\"sk-circle7 sk-circle\"></div>\r\n                          <div class=\"sk-circle8 sk-circle\"></div>\r\n                          <div class=\"sk-circle9 sk-circle\"></div>\r\n                          <div class=\"sk-circle10 sk-circle\"></div>\r\n                          <div class=\"sk-circle11 sk-circle\"></div>\r\n                          <div class=\"sk-circle12 sk-circle\"></div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"form-group uploaded\" *ngIf=\"uploads.icon && !showLoading5\">\r\n                      <!-- <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image3')\"></i> -->\r\n                      <div class=\"uploaded-image2\">\r\n                        <div class=\"img\">\r\n                          <img class=\"resize\" src=\"{{uploads.icon}}\">\r\n                          <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile5.click()\">Re-Upload</a>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n</div></div></div></div>\r\n\r\n</div>\r\n<div class=\"row end-footer\">\r\n  <div class=\"footer-save-cancel\">\r\n    <!-- <button class=\"btn cancel\">Cancel</button> -->\r\n    <button class=\"btn save btn-primary\"  (click)=\"formSubmitAddNotificationBroadcast()\">Save</button>\r\n  </div>\r\n</div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notification.broadcast/broadcast.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/notification.broadcast/broadcast.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'Broadcast'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  \r\n  <div *ngIf=\"Broadcast&&broadcastDetail==false\">\r\n        <app-form-builder-table\r\n        [table_data]  = \"Broadcast\" \r\n        [searchCallback]= \"[service, 'searchBroadcastLint',this]\"\r\n        [tableFormat]=\"tableFormat\"\r\n        >\r\n            \r\n        </app-form-builder-table>\r\n  </div>\r\n  <div *ngIf=\"broadcastDetail\">\r\n    <app-broadcast-detail [back]=\"[this,backToHere]\" [detail]=\"broadcastDetail\"></app-broadcast-detail>\r\n</div>\r\n \r\n</div>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notification.broadcast/detail/broadcast.detail.component.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/notification.broadcast/detail/broadcast.detail.component.html ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.message_from}}</h1>\r\n        <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"broadcast-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> Broadcast Details </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                            <label>Organization ID</label>\r\n                            <div name=\"org_id\" >{{detail.org_id}}</div>\r\n                            <label>Setting Name</label>\r\n                            <div name=\"message_from\" >{{detail.message_from}}</div>\r\n                            <label>Group Name</label>\r\n                            <div name=\"group_name\" >{{detail.group_name}}</div>\r\n                            <label>Template Name</label>\r\n                            <div name=\"title\" >{{detail.title}}</div>\r\n                            <label>Message</label>\r\n                            <div name=\"message\" >{{detail.message}}</div>\r\n                            <label>Response</label>\r\n                            <div name=\"firebase_response\" >{{detail.firebase_response}}</div>\r\n\r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                </div>\r\n            </div>\r\n            \r\n            <div class=\"row\">\r\n                <div class=\"col-md-12 bg-delete\"><button class=\"btn delete-button float-right btn-danger\" (click)=\"deleteThis()\">delete this</button></div>\r\n            </div>\r\n            \r\n    </div>\r\n</div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-broadcast-edit [back]=\"[this, 'editThis']\" [detail]=\"detail\"></app-broadcast-edit>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notification.broadcast/edit/broadcast.edit.component.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/notification.broadcast/edit/broadcast.edit.component.html ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail._id}}</h1>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save and Send</span> <span *ngIf=\"loading\">loading</span></button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"broadcast-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> Edit Notification Details </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             <label>Organization ID</label>\r\n                             <form-input  name=\"org_id\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Organization ID'\"  [(ngModel)]=\"detail.org_id\" autofocus required></form-input>\r\n             \r\n                             <label>Setting</label>\r\n                             <div name=\"setting_id\">\r\n                                 <form-select name=\"setting_id\" [(ngModel)]=\"detail.setting_id\" [data]=\"settingBroadcast\" required></form-select> \r\n                             </div> \r\n             \r\n                             <label>Group</label>\r\n                             <div name=\"group_id\">\r\n                                 <form-select name=\"group_id\" [(ngModel)]=\"detail.group_id\" [data]=\"groupBroadcast\" required></form-select> \r\n                             </div> \r\n\r\n                             <label>Template</label>\r\n                             <div name=\"template_id\">\r\n                                 <form-select name=\"template_id\" [(ngModel)]=\"detail.template_id\" [data]=\"templateBroadcast\" required></form-select> \r\n                             </div> \r\n                            \r\n                             <label>Status</label>\r\n                             <div name=\"status\"> {{status}}\r\n                                 <form-select name=\"status\" [(ngModel)]=\"detail.status\" [data]=\"broadcastStatus\" required></form-select> \r\n                             </div> \r\n\r\n                            </div>\r\n                         <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                        </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                </div>\r\n                <!-- <div class=\" col-md-4\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Email </label>\r\n                            <form-input name=\"email\" [type]=\"'text'\" [placeholder]=\"'Email'\"  [(ngModel)]=\"detail.email\" autofocus required></form-input>\r\n\r\n                            <label>Cell Phone </label>\r\n                            <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Cell Phone'\"  [(ngModel)]=\"detail.cell_phone\" autofocus required></form-input>\r\n                            \r\n                            <label>Registration Date </label>\r\n                            <form-input  name=\"registration_date\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Registration Date'\"  [(ngModel)]=\"detail.registration_date\" autofocus required></form-input>\r\n\r\n                            <label>Point Balance </label>\r\n                            <form-input  name=\"point_balance\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Point Balance'\"  [(ngModel)]=\"detail.point_balance\" autofocus required></form-input>\r\n\r\n                            <label>Activation Code </label>\r\n                            <form-input  name=\"activation_code\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Activation Code'\"  [(ngModel)]=\"detail.activation_code\" autofocus required></form-input>\r\n\r\n                            <label>Activation Status </label>\r\n                            <div name=\"activation_status\"> {{activation_status}}\r\n                                <form-select name=\"activation_status\" [(ngModel)]=\"detail.activation_status\" [data]=\"activationStatus\" required></form-select> \r\n                             </div> \r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div> \r\n                 \r\n                </div>-->\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    \r\n</div>"

/***/ }),

/***/ "./src/app/layout/modules/notification.broadcast/add/broadcast.add.component.scss":
/*!****************************************************************************************!*\
  !*** ./src/app/layout/modules/notification.broadcast/add/broadcast.add.component.scss ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div.error {\n  background: #e74c3c;\n  color: white;\n  padding: 25px;\n  margin-bottom: 25px;\n}\n\n.hashhash .hashtag-item {\n  display: inline-block;\n  margin-right: 5px;\n  background: #eee;\n  padding: 2px 5px;\n  margin-bottom: 5px;\n  border-radius: 5px;\n}\n\n.hashhash .hashtag-item::after {\n  content: \",\";\n  display: inline;\n}\n\n.hashhash .hashtagskey-input {\n  display: inline-block;\n  min-width: 75px;\n  margin-bottom: 5px;\n  font-size: 1rem;\n  background: white;\n  border-radius: 5px;\n  outline: none;\n}\n\n.hashhash .hashtagskey-input::after {\n  content: \"\";\n  display: inline;\n}\n\n.pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n\n.pb-framer .clr-white {\n  color: white;\n}\n\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 10px;\n  background-color: #3498db;\n  color: white;\n  height: 30px;\n  align-items: center;\n  display: flex;\n  right: 10px;\n  padding-top: 0px;\n  padding-right: 10px;\n  padding-bottom: 0;\n  padding-left: 10px;\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 25px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 0px;\n  border: 1px solid #f8f9fa;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.rounded-btn {\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n  margin-left: 40%;\n  margin-right: 50%;\n}\n\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n\n.section-upload {\n  width: 100%;\n  align-items: center;\n}\n\n.section-upload .col-2dot4,\n.section-upload .col-sm-2dot4,\n.section-upload .col-md-2dot4,\n.section-upload .col-lg-2dot4,\n.section-upload .col-xl-2dot4 {\n  position: relative;\n  width: 100%;\n  min-height: 1px;\n  padding-right: 0px;\n  padding-left: 0px;\n}\n\n.section-upload .col-2dot4 {\n  flex: 0 0 20%;\n  max-width: 20%;\n}\n\n@media (min-width: 540px) {\n  .section-upload .col-sm-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n@media (min-width: 720px) {\n  .section-upload .col-md-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n@media (min-width: 960px) {\n  .section-upload .col-lg-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n@media (min-width: 1140px) {\n  .section-upload .col-xl-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n.section-upload .imagess {\n  text-align: center;\n  font-size: 14px;\n  padding: 0px;\n}\n\n.section-upload .imagess .blue-upload {\n  color: #56a4ff;\n  font-size: 16px;\n  font-weight: bold;\n}\n\n.section-upload .imagess .card {\n  padding: 20px;\n  height: 190px;\n  border: none;\n}\n\n.section-upload .imagess .card .card-content {\n  height: 100%;\n  position: relative;\n}\n\n.section-upload .imagess .card .card-content .upload-desc {\n  color: #bbb !important;\n  text-align: center;\n  font-size: 14px;\n}\n\n.section-upload .imagess .card .card-content .logo-upload {\n  width: 100%;\n  background: white;\n}\n\n.section-upload .imagess .card .card-content .image {\n  display: block;\n  float: left;\n  height: auto;\n  width: 100%;\n  height: 100%;\n}\n\n.section-upload .imagess .card .card-content .image .custom {\n  height: 100%;\n  border-radius: 10px;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n}\n\n.section-upload .imagess .card .card-content .image .custom .drag-drop {\n  font-size: 10px;\n  text-align: center;\n}\n\n.section-upload .imagess .card .card-content .image .custom .set-opacity {\n  opacity: 0.1;\n  height: 60px;\n  width: auto;\n}\n\n.section-upload .imagess .card .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n\n.section-upload .imagess .card .form-group.uploaded .loading {\n  height: 100%;\n}\n\n.section-upload .imagess .card .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\n\n.section-upload .imagess .card .form-group.uploaded .uploaded-image2 {\n  height: 100%;\n}\n\n.section-upload .imagess .card .form-group.uploaded .uploaded-image2 .img {\n  padding: 15px;\n  height: 100%;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n}\n\n.section-upload .imagess .card .form-group.uploaded .uploaded-image2 .img > img {\n  height: 100%;\n  max-height: 320px;\n}\n\n.section-upload .imagess .card .form-group.uploaded .uploaded-image2 .editor-tool {\n  height: 100%;\n}\n\n.section-upload .row-flex {\n  display: flex;\n}\n\n.section-upload .imagess {\n  text-align: center;\n}\n\n.section-upload .imagess .card {\n  height: 300px;\n  border: none;\n  border-radius: 8px;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.05);\n  background-color: #ffffff;\n}\n\n.section-upload .imagess .card .card-content {\n  display: flex;\n  flex-direction: column;\n  height: 100%;\n  position: relative;\n}\n\n.section-upload .imagess .card .card-content .image-canvas {\n  height: 200px;\n}\n\n.section-upload .imagess .card .card-content .upload-desc {\n  color: #bbb !important;\n  text-align: center;\n  font-size: 14px;\n}\n\n.section-upload .imagess .card .card-content .logo-upload {\n  width: auto;\n  height: 100%;\n  background: white;\n}\n\n.section-upload .imagess .card .card-content .image {\n  display: block;\n  float: left;\n  width: auto;\n  height: 100%;\n}\n\n.section-upload .imagess .card .card-content .image .custom {\n  height: 100%;\n  border-radius: 10px;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n}\n\n.section-upload .imagess .card .card-content .image .custom .drag-drop {\n  font-size: 10px;\n  text-align: center;\n}\n\n.section-upload .imagess .card .card-content .image .custom .set-opacity {\n  opacity: 0.1;\n  height: 60px;\n  width: auto;\n}\n\n.section-upload .imagess .card .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n\n.section-upload .imagess .card .form-group.uploaded .remove-icon {\n  position: absolute;\n  right: 0;\n  top: 0;\n  color: red;\n  font-size: 22px;\n}\n\n.section-upload .imagess .card .form-group.uploaded .loading {\n  height: 100%;\n}\n\n.section-upload .imagess .card .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\n\n.section-upload .imagess .card .form-group.uploaded .uploaded-image2 {\n  height: 100%;\n}\n\n.section-upload .imagess .card .form-group.uploaded .uploaded-image2 .img {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.section-upload .imagess .card .form-group.uploaded .uploaded-image2 .img > img {\n  height: 100%;\n  max-height: 320px;\n}\n\n.section-upload .imagess .card .form-group.uploaded .uploaded-image2 .editor-tool {\n  height: 100%;\n}\n\n.end-footer {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.end-footer .footer-save-cancel {\n  margin: 50px;\n}\n\n.end-footer .footer-save-cancel .cancel {\n  border-color: #2480fb;\n  margin-right: 20px;\n  color: #2480fb;\n  background-color: white;\n  font-size: 20px;\n  padding: 15px;\n  width: 25rem;\n  border-radius: 10px;\n}\n\n.end-footer .footer-save-cancel .save {\n  font-size: 20px;\n  padding: 15px;\n  width: 25rem;\n  border-radius: 10px;\n  background-color: #2480fb;\n}\n\n.end-footer .delete {\n  position: relative;\n  float: right;\n  background-color: #e74c3c;\n  opacity: 0.5;\n  border-color: #c0392b;\n}\n\n.end-footer .delete .delete-modal {\n  position: absolute;\n  display: none;\n  background: white;\n  border: 1px solid #ccc;\n  border-radius: 10px;\n  padding: 4px 10px;\n  top: 0px;\n  right: 0px;\n  color: #e74c3c;\n  -webkit-transform: translateY(-45px);\n          transform: translateY(-45px);\n}\n\n.end-footer .delete .delete-modal .btn-group {\n  border: 1px solid #e74c3c;\n  border-radius: 5px;\n  overflow: hidden;\n}\n\n.end-footer .delete .delete-modal .btn-group .no {\n  opacity: 0.5;\n  border-width: 0px;\n}\n\n.end-footer .delete .delete-modal .btn-group .yes {\n  background: #e74c3c;\n  border-width: 0px;\n}\n\n.end-footer .delete .delete-modal .btn-group .no:hover {\n  opacity: 1;\n}\n\n.end-footer .delete:hover,\n.end-footer .delete:focus {\n  opacity: 1;\n}\n\n.end-footer .delete:focus .delete-modal {\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbm90aWZpY2F0aW9uLmJyb2FkY2FzdC9hZGQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxub3RpZmljYXRpb24uYnJvYWRjYXN0XFxhZGRcXGJyb2FkY2FzdC5hZGQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL25vdGlmaWNhdGlvbi5icm9hZGNhc3QvYWRkL2Jyb2FkY2FzdC5hZGQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUU7RUFDRSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUNESjs7QURJSTtFQUNJLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0RSOztBRElJO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUNGUjs7QURLSTtFQUNJLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0FDSFI7O0FETUk7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQ0pSOztBRE9BO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDSko7O0FETUk7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ0pSOztBRE9JO0VBQ0kscUJBQUE7QUNMUjs7QURRSTtFQUNJLFlBQUE7QUNOUjs7QURXSTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7QUNSUjs7QURVUTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNUWjs7QURXWTtFQUNJLGlCQUFBO0FDVGhCOztBRGFRO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFFQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDWlo7O0FEZ0JJO0VBQ0ksYUFBQTtBQ2RSOztBRGdCUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNkWjs7QURrQkk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ2hCUjs7QURvQlE7RUFDSSxrQkFBQTtBQ2xCWjs7QURxQlE7RUFDSSxnQkFBQTtBQ25CWjs7QURzQlE7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDcEJaOztBRHdCUTtFQUNJLHNCQUFBO0FDdEJaOztBRHdCWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDdEJoQjs7QUQ2QkE7RUFFSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDMUJKOztBRDZCQTtFQUNJLG1CQUFBO0VBRUEsYUFBQTtBQzNCSjs7QUQ4QkE7RUFFSSxXQUFBO0VBRUEsbUJBQUE7QUM3Qko7O0FEK0JJOzs7OztFQUtJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDN0JSOztBRGdDSTtFQUdJLGFBQUE7RUFDQSxjQUFBO0FDOUJSOztBRGlDSTtFQUNJO0lBR0ksYUFBQTtJQUNBLGNBQUE7RUMvQlY7QUFDRjs7QURrQ0k7RUFDSTtJQUdJLGFBQUE7SUFDQSxjQUFBO0VDaENWO0FBQ0Y7O0FEbUNJO0VBQ0k7SUFHSSxhQUFBO0lBQ0EsY0FBQTtFQ2pDVjtBQUNGOztBRG9DSTtFQUNJO0lBR0ksYUFBQTtJQUNBLGNBQUE7RUNsQ1Y7QUFDRjs7QURxQ0k7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDbkNSOztBRHFDUTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNuQ1o7O0FEc0NRO0VBQ0ksYUFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FDcENaOztBRHNDWTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtBQ3BDaEI7O0FEc0NnQjtFQUNJLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDcENwQjs7QUR1Q2dCO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0FDckNwQjs7QUR3Q2dCO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUN0Q3BCOztBRHdDb0I7RUFDSSxZQUFBO0VBRUEsbUJBQUE7RUFFQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0FDeEN4Qjs7QUQyQ3dCO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0FDekM1Qjs7QUQ0Q3dCO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDMUM1Qjs7QURnRFk7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQzlDaEI7O0FEZ0RnQjtFQUNJLFlBQUE7QUM5Q3BCOztBRGdEb0I7RUFDSSxRQUFBO0FDOUN4Qjs7QURrRGdCO0VBQ0ksWUFBQTtBQ2hEcEI7O0FEa0RvQjtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNoRHhCOztBRG1Eb0I7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7QUNqRHhCOztBRG9Eb0I7RUFDSSxZQUFBO0FDbER4Qjs7QUR3REk7RUFDSSxhQUFBO0FDdERSOztBRHlESTtFQUNJLGtCQUFBO0FDdkRSOztBRHdEUTtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQ0FBQTtFQUNBLHlCQUFBO0FDdERaOztBRHlEWTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ3ZEaEI7O0FEeURnQjtFQUNJLGFBQUE7QUN2RHBCOztBRDBEZ0I7RUFDSSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ3hEcEI7O0FEMkRnQjtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUN6RHBCOztBRDREZ0I7RUFDSSxjQUFBO0VBQ0EsV0FBQTtFQUVBLFdBQUE7RUFDQSxZQUFBO0FDM0RwQjs7QUQ2RG9CO0VBQ0ksWUFBQTtFQUVBLG1CQUFBO0VBQ0EsdUJBQUE7RUFFQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQzdEeEI7O0FEZ0V3QjtFQUNJLGVBQUE7RUFDQSxrQkFBQTtBQzlENUI7O0FEaUV3QjtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQy9ENUI7O0FEcUVZO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNuRWhCOztBRHFFZ0I7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxNQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUNuRXBCOztBRHNFZ0I7RUFDSSxZQUFBO0FDcEVwQjs7QURzRW9CO0VBQ0ksUUFBQTtBQ3BFeEI7O0FEd0VnQjtFQUNJLFlBQUE7QUN0RXBCOztBRHdFb0I7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDdEV4Qjs7QUR5RW9CO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0FDdkV4Qjs7QUQwRW9CO0VBQ0ksWUFBQTtBQ3hFeEI7O0FEK0VBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQzVFSjs7QUQ4RUk7RUFDSSxZQUFBO0FDNUVSOztBRDhFUTtFQUNJLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQzVFWjs7QUQrRVE7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FDN0VaOztBRGlGSTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0FDL0VSOztBRGlGUTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0Esb0NBQUE7VUFBQSw0QkFBQTtBQy9FWjs7QURpRlk7RUFDSSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUMvRWhCOztBRGlGZ0I7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7QUMvRXBCOztBRGtGZ0I7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0FDaEZwQjs7QURtRmdCO0VBQ0ksVUFBQTtBQ2pGcEI7O0FEdUZJOztFQUVJLFVBQUE7QUNyRlI7O0FEeUZRO0VBQ0ksY0FBQTtBQ3ZGWiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL25vdGlmaWNhdGlvbi5icm9hZGNhc3QvYWRkL2Jyb2FkY2FzdC5hZGQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuICBcclxuICBkaXYuZXJyb3Ige1xyXG4gICAgYmFja2dyb3VuZDogI2U3NGMzYztcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDI1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG59XHJcbiAgLmhhc2hoYXNoIHtcclxuICAgIC5oYXNodGFnLWl0ZW0ge1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZWVlO1xyXG4gICAgICAgIHBhZGRpbmc6IDJweCA1cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAuaGFzaHRhZy1pdGVtOjphZnRlciB7XHJcbiAgICAgICAgY29udGVudDogXCIsXCI7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lO1xyXG4gICAgfVxyXG5cclxuICAgIC5oYXNodGFnc2tleS1pbnB1dCB7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIG1pbi13aWR0aDogNzVweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIC5oYXNodGFnc2tleS1pbnB1dDo6YWZ0ZXIge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lO1xyXG4gICAgfVxyXG59XHJcbi5wYi1mcmFtZXIge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgY29sb3I6ICNlNjdlMjI7XHJcblxyXG4gICAgLnByb2dyZXNzYmFyIHtcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMzNDk4ZGI7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGhlaWdodDogMjVweDtcclxuICAgICAgICB6LWluZGV4OiAtMTtcclxuICAgIH1cclxuXHJcbiAgICAudGVybWluYXRlZC5wcm9ncmVzc2JhciB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gICAgfVxyXG5cclxuICAgIC5jbHItd2hpdGUge1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxufVxyXG5cclxuLmNhcmQtZGV0YWlsIHtcclxuICAgID4uY2FyZC1oZWFkZXIge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtaW4taGVpZ2h0OiA0OHB4O1xyXG5cclxuICAgICAgICAuYmFja19idXR0b24ge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcblxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuXHJcbiAgICAgICAgICAgIGkge1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5zYXZlX2J1dHRvbiB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiAxMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgIC8vIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5jYXJkLWNvbnRlbnQge1xyXG4gICAgICAgIHBhZGRpbmc6IDI1cHg7XHJcblxyXG4gICAgICAgID4uY29sLW1kLTEyIHtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaDEge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5tZW1iZXItZGV0YWlsIHtcclxuICAgICAgICBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsYWJlbCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsYWJlbCtkaXYge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNmOGY5ZmE7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuXHJcbiAgICAgICAgICAgIGgyIHtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnJvdW5kZWQtYnRuIHtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBsaW5lLWhlaWdodDogNDBweDtcclxuICAgIHBhZGRpbmc6IDAgNDBweDtcclxuICAgIGJhY2tncm91bmQ6ICM1NmE0ZmY7XHJcbiAgICBtYXJnaW4tbGVmdDogNDAlO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1MCU7XHJcbn1cclxuXHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2QTRGRiwgMTUlKTtcclxuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4uc2VjdGlvbi11cGxvYWQge1xyXG4gICAgLy8gbWFyZ2luLXRvcDogNXZoO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICAuY29sLTJkb3Q0LFxyXG4gICAgLmNvbC1zbS0yZG90NCxcclxuICAgIC5jb2wtbWQtMmRvdDQsXHJcbiAgICAuY29sLWxnLTJkb3Q0LFxyXG4gICAgLmNvbC14bC0yZG90NCB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDFweDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmNvbC0yZG90NCB7XHJcbiAgICAgICAgLXdlYmtpdC1ib3gtZmxleDogMDtcclxuICAgICAgICAtbXMtZmxleDogMCAwIDIwJTtcclxuICAgICAgICBmbGV4OiAwIDAgMjAlO1xyXG4gICAgICAgIG1heC13aWR0aDogMjAlO1xyXG4gICAgfVxyXG5cclxuICAgIEBtZWRpYSAobWluLXdpZHRoOiA1NDBweCkge1xyXG4gICAgICAgIC5jb2wtc20tMmRvdDQge1xyXG4gICAgICAgICAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xyXG4gICAgICAgICAgICAtbXMtZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgbWF4LXdpZHRoOiAyMCU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIEBtZWRpYSAobWluLXdpZHRoOiA3MjBweCkge1xyXG4gICAgICAgIC5jb2wtbWQtMmRvdDQge1xyXG4gICAgICAgICAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xyXG4gICAgICAgICAgICAtbXMtZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgbWF4LXdpZHRoOiAyMCU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIEBtZWRpYSAobWluLXdpZHRoOiA5NjBweCkge1xyXG4gICAgICAgIC5jb2wtbGctMmRvdDQge1xyXG4gICAgICAgICAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xyXG4gICAgICAgICAgICAtbXMtZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgbWF4LXdpZHRoOiAyMCU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIEBtZWRpYSAobWluLXdpZHRoOiAxMTQwcHgpIHtcclxuICAgICAgICAuY29sLXhsLTJkb3Q0IHtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtZmxleDogMDtcclxuICAgICAgICAgICAgLW1zLWZsZXg6IDAgMCAyMCU7XHJcbiAgICAgICAgICAgIGZsZXg6IDAgMCAyMCU7XHJcbiAgICAgICAgICAgIG1heC13aWR0aDogMjAlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuaW1hZ2VzcyB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBwYWRkaW5nOiAwcHg7XHJcblxyXG4gICAgICAgIC5ibHVlLXVwbG9hZCB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNhcmQge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDE5MHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcblxyXG4gICAgICAgICAgICAuY2FyZC1jb250ZW50IHtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAudXBsb2FkLWRlc2Mge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjYmJiICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAubG9nby11cGxvYWQge1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHdoaXRlXHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLmltYWdlIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAuY3VzdG9tIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiNlZWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGJvcmRlcjogM3B4IHNvbGlkICNlYmViZWI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gcGFkZGluZzogNDBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmRyYWctZHJvcCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zZXQtb3BhY2l0eSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLjE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogYXV0bztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG5cclxuICAgICAgICAgICAgICAgIC5sb2FkaW5nIHtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5zay1mYWRpbmctY2lyY2xlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC51cGxvYWRlZC1pbWFnZTIge1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmltZyB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmltZz5pbWcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDMyMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmVkaXRvci10b29sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5yb3ctZmxleHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgfVxyXG5cclxuICAgIC5pbWFnZXNzIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgLmNhcmQge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICAgICAgYm94LXNoYWRvdzogMCAzcHggNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA1KTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuXHJcblxyXG4gICAgICAgICAgICAuY2FyZC1jb250ZW50IHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgICAgICAgICAgICAgIC5pbWFnZS1jYW52YXMge1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLnVwbG9hZC1kZXNjIHtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogI2JiYiAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLmxvZ28tdXBsb2FkIHtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogYXV0bztcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLmltYWdlIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmN1c3RvbSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGJvcmRlcjogM3B4IHNvbGlkICNlYmViZWI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuZHJhZy1kcm9wIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLnNldC1vcGFjaXR5IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDAuMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAuZm9ybS1ncm91cC51cGxvYWRlZCB7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICAgICAgLnJlbW92ZS1pY29uIHtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC5sb2FkaW5nIHtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5zay1mYWRpbmctY2lyY2xlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC51cGxvYWRlZC1pbWFnZTIge1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmltZyB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmltZz5pbWcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDMyMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmVkaXRvci10b29sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4uZW5kLWZvb3RlciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICAuZm9vdGVyLXNhdmUtY2FuY2VsIHtcclxuICAgICAgICBtYXJnaW46IDUwcHg7XHJcblxyXG4gICAgICAgIC5jYW5jZWwge1xyXG4gICAgICAgICAgICBib3JkZXItY29sb3I6ICMyNDgwZmI7XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgICAgICAgICAgY29sb3I6ICMyNDgwZmI7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiAyNXJlbTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5zYXZlIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgICAgICB3aWR0aDogMjVyZW07XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyNDgwZmI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5kZWxldGUge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U3NGMzYztcclxuICAgICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjYzAzOTJiO1xyXG5cclxuICAgICAgICAuZGVsZXRlLW1vZGFsIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICAgICAgcGFkZGluZzogNHB4IDEwcHg7XHJcbiAgICAgICAgICAgIHRvcDogMHB4O1xyXG4gICAgICAgICAgICByaWdodDogMHB4O1xyXG4gICAgICAgICAgICBjb2xvcjogI2U3NGMzYztcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC00NXB4KTtcclxuXHJcbiAgICAgICAgICAgIC5idG4tZ3JvdXAge1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2U3NGMzYztcclxuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcblxyXG4gICAgICAgICAgICAgICAgLm5vIHtcclxuICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXdpZHRoOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLnllcyB7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2U3NGMzYztcclxuICAgICAgICAgICAgICAgICAgICBib3JkZXItd2lkdGg6IDBweDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAubm86aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmRlbGV0ZTpob3ZlcixcclxuICAgIC5kZWxldGU6Zm9jdXMge1xyXG4gICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICB9XHJcblxyXG4gICAgLmRlbGV0ZTpmb2N1cyB7XHJcbiAgICAgICAgLmRlbGV0ZS1tb2RhbCB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iLCJkaXYuZXJyb3Ige1xuICBiYWNrZ3JvdW5kOiAjZTc0YzNjO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDI1cHg7XG4gIG1hcmdpbi1ib3R0b206IDI1cHg7XG59XG5cbi5oYXNoaGFzaCAuaGFzaHRhZy1pdGVtIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgYmFja2dyb3VuZDogI2VlZTtcbiAgcGFkZGluZzogMnB4IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uaGFzaGhhc2ggLmhhc2h0YWctaXRlbTo6YWZ0ZXIge1xuICBjb250ZW50OiBcIixcIjtcbiAgZGlzcGxheTogaW5saW5lO1xufVxuLmhhc2hoYXNoIC5oYXNodGFnc2tleS1pbnB1dCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWluLXdpZHRoOiA3NXB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgb3V0bGluZTogbm9uZTtcbn1cbi5oYXNoaGFzaCAuaGFzaHRhZ3NrZXktaW5wdXQ6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogaW5saW5lO1xufVxuXG4ucGItZnJhbWVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGhlaWdodDogMjVweDtcbiAgY29sb3I6ICNlNjdlMjI7XG59XG4ucGItZnJhbWVyIC5wcm9ncmVzc2JhciB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJhY2tncm91bmQ6ICMzNDk4ZGI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAyNXB4O1xuICB6LWluZGV4OiAtMTtcbn1cbi5wYi1mcmFtZXIgLnRlcm1pbmF0ZWQucHJvZ3Jlc3NiYXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG59XG4ucGItZnJhbWVyIC5jbHItd2hpdGUge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWluLWhlaWdodDogNDhweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICByaWdodDogMTBweDtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDA7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDI1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBwYWRkaW5nOiAwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmOGY5ZmE7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5yb3VuZGVkLWJ0biB7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG4gIG1hcmdpbi1sZWZ0OiA0MCU7XG4gIG1hcmdpbi1yaWdodDogNTAlO1xufVxuXG4ucm91bmRlZC1idG46aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMGE3YmZmO1xuICBvdXRsaW5lOiBub25lO1xufVxuXG4uc2VjdGlvbi11cGxvYWQge1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuY29sLTJkb3Q0LFxuLnNlY3Rpb24tdXBsb2FkIC5jb2wtc20tMmRvdDQsXG4uc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCxcbi5zZWN0aW9uLXVwbG9hZCAuY29sLWxnLTJkb3Q0LFxuLnNlY3Rpb24tdXBsb2FkIC5jb2wteGwtMmRvdDQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHdpZHRoOiAxMDAlO1xuICBtaW4taGVpZ2h0OiAxcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG59XG4uc2VjdGlvbi11cGxvYWQgLmNvbC0yZG90NCB7XG4gIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gIC1tcy1mbGV4OiAwIDAgMjAlO1xuICBmbGV4OiAwIDAgMjAlO1xuICBtYXgtd2lkdGg6IDIwJTtcbn1cbkBtZWRpYSAobWluLXdpZHRoOiA1NDBweCkge1xuICAuc2VjdGlvbi11cGxvYWQgLmNvbC1zbS0yZG90NCB7XG4gICAgLXdlYmtpdC1ib3gtZmxleDogMDtcbiAgICAtbXMtZmxleDogMCAwIDIwJTtcbiAgICBmbGV4OiAwIDAgMjAlO1xuICAgIG1heC13aWR0aDogMjAlO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNzIwcHgpIHtcbiAgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQge1xuICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gICAgLW1zLWZsZXg6IDAgMCAyMCU7XG4gICAgZmxleDogMCAwIDIwJTtcbiAgICBtYXgtd2lkdGg6IDIwJTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDk2MHB4KSB7XG4gIC5zZWN0aW9uLXVwbG9hZCAuY29sLWxnLTJkb3Q0IHtcbiAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xuICAgIC1tcy1mbGV4OiAwIDAgMjAlO1xuICAgIGZsZXg6IDAgMCAyMCU7XG4gICAgbWF4LXdpZHRoOiAyMCU7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiAxMTQwcHgpIHtcbiAgLnNlY3Rpb24tdXBsb2FkIC5jb2wteGwtMmRvdDQge1xuICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gICAgLW1zLWZsZXg6IDAgMCAyMCU7XG4gICAgZmxleDogMCAwIDIwJTtcbiAgICBtYXgtd2lkdGg6IDIwJTtcbiAgfVxufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE0cHg7XG4gIHBhZGRpbmc6IDBweDtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuYmx1ZS11cGxvYWQge1xuICBjb2xvcjogIzU2YTRmZjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCB7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGhlaWdodDogMTkwcHg7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuY2FyZC1jb250ZW50IHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uc2VjdGlvbi11cGxvYWQgLmltYWdlc3MgLmNhcmQgLmNhcmQtY29udGVudCAudXBsb2FkLWRlc2Mge1xuICBjb2xvcjogI2JiYiAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuY2FyZC1jb250ZW50IC5sb2dvLXVwbG9hZCB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuY2FyZC1jb250ZW50IC5pbWFnZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmbG9hdDogbGVmdDtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20ge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20gLmRyYWctZHJvcCB7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20gLnNldC1vcGFjaXR5IHtcbiAgb3BhY2l0eTogMC4xO1xuICBoZWlnaHQ6IDYwcHg7XG4gIHdpZHRoOiBhdXRvO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyAuc2stZmFkaW5nLWNpcmNsZSB7XG4gIHRvcDogNTAlO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZTIge1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uc2VjdGlvbi11cGxvYWQgLmltYWdlc3MgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlMiAuaW1nIHtcbiAgcGFkZGluZzogMTVweDtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG4uc2VjdGlvbi11cGxvYWQgLmltYWdlc3MgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlMiAuaW1nID4gaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtYXgtaGVpZ2h0OiAzMjBweDtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAudXBsb2FkZWQtaW1hZ2UyIC5lZGl0b3ItdG9vbCB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5zZWN0aW9uLXVwbG9hZCAucm93LWZsZXgge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIHtcbiAgaGVpZ2h0OiAzMDBweDtcbiAgYm9yZGVyOiBub25lO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGJveC1zaGFkb3c6IDAgM3B4IDZweCAwIHJnYmEoMCwgMCwgMCwgMC4wNSk7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59XG4uc2VjdGlvbi11cGxvYWQgLmltYWdlc3MgLmNhcmQgLmNhcmQtY29udGVudCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlLWNhbnZhcyB7XG4gIGhlaWdodDogMjAwcHg7XG59XG4uc2VjdGlvbi11cGxvYWQgLmltYWdlc3MgLmNhcmQgLmNhcmQtY29udGVudCAudXBsb2FkLWRlc2Mge1xuICBjb2xvcjogI2JiYiAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuY2FyZC1jb250ZW50IC5sb2dvLXVwbG9hZCB7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZsb2F0OiBsZWZ0O1xuICB3aWR0aDogYXV0bztcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20ge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20gLmRyYWctZHJvcCB7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20gLnNldC1vcGFjaXR5IHtcbiAgb3BhY2l0eTogMC4xO1xuICBoZWlnaHQ6IDYwcHg7XG4gIHdpZHRoOiBhdXRvO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAucmVtb3ZlLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDA7XG4gIGNvbG9yOiByZWQ7XG4gIGZvbnQtc2l6ZTogMjJweDtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyAuc2stZmFkaW5nLWNpcmNsZSB7XG4gIHRvcDogNTAlO1xufVxuLnNlY3Rpb24tdXBsb2FkIC5pbWFnZXNzIC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZTIge1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uc2VjdGlvbi11cGxvYWQgLmltYWdlc3MgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlMiAuaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uc2VjdGlvbi11cGxvYWQgLmltYWdlc3MgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlMiAuaW1nID4gaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtYXgtaGVpZ2h0OiAzMjBweDtcbn1cbi5zZWN0aW9uLXVwbG9hZCAuaW1hZ2VzcyAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAudXBsb2FkZWQtaW1hZ2UyIC5lZGl0b3ItdG9vbCB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLmVuZC1mb290ZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5lbmQtZm9vdGVyIC5mb290ZXItc2F2ZS1jYW5jZWwge1xuICBtYXJnaW46IDUwcHg7XG59XG4uZW5kLWZvb3RlciAuZm9vdGVyLXNhdmUtY2FuY2VsIC5jYW5jZWwge1xuICBib3JkZXItY29sb3I6ICMyNDgwZmI7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbiAgY29sb3I6ICMyNDgwZmI7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIHdpZHRoOiAyNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cbi5lbmQtZm9vdGVyIC5mb290ZXItc2F2ZS1jYW5jZWwgLnNhdmUge1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIHdpZHRoOiAyNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI0ODBmYjtcbn1cbi5lbmQtZm9vdGVyIC5kZWxldGUge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZsb2F0OiByaWdodDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U3NGMzYztcbiAgb3BhY2l0eTogMC41O1xuICBib3JkZXItY29sb3I6ICNjMDM5MmI7XG59XG4uZW5kLWZvb3RlciAuZGVsZXRlIC5kZWxldGUtbW9kYWwge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwYWRkaW5nOiA0cHggMTBweDtcbiAgdG9wOiAwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIGNvbG9yOiAjZTc0YzNjO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTQ1cHgpO1xufVxuLmVuZC1mb290ZXIgLmRlbGV0ZSAuZGVsZXRlLW1vZGFsIC5idG4tZ3JvdXAge1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTc0YzNjO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uZW5kLWZvb3RlciAuZGVsZXRlIC5kZWxldGUtbW9kYWwgLmJ0bi1ncm91cCAubm8ge1xuICBvcGFjaXR5OiAwLjU7XG4gIGJvcmRlci13aWR0aDogMHB4O1xufVxuLmVuZC1mb290ZXIgLmRlbGV0ZSAuZGVsZXRlLW1vZGFsIC5idG4tZ3JvdXAgLnllcyB7XG4gIGJhY2tncm91bmQ6ICNlNzRjM2M7XG4gIGJvcmRlci13aWR0aDogMHB4O1xufVxuLmVuZC1mb290ZXIgLmRlbGV0ZSAuZGVsZXRlLW1vZGFsIC5idG4tZ3JvdXAgLm5vOmhvdmVyIHtcbiAgb3BhY2l0eTogMTtcbn1cbi5lbmQtZm9vdGVyIC5kZWxldGU6aG92ZXIsXG4uZW5kLWZvb3RlciAuZGVsZXRlOmZvY3VzIHtcbiAgb3BhY2l0eTogMTtcbn1cbi5lbmQtZm9vdGVyIC5kZWxldGU6Zm9jdXMgLmRlbGV0ZS1tb2RhbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/notification.broadcast/add/broadcast.add.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/layout/modules/notification.broadcast/add/broadcast.add.component.ts ***!
  \**************************************************************************************/
/*! exports provided: BroadcastAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastAddComponent", function() { return BroadcastAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/notification.broadcast/broadcast.service */ "./src/app/services/notification.broadcast/broadcast.service.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var BroadcastAddComponent = /** @class */ (function () {
    function BroadcastAddComponent(broadcastService, productService) {
        this.broadcastService = broadcastService;
        this.productService = productService;
        this.name = "";
        this.Broadcast = [];
        this.settingBroadcast = [];
        this.groupBroadcast = [];
        this.templateBroadcast = [];
        this.broadcastStatus = [
            { label: "ACTIVE", value: "ACTIVE" },
            { label: "INACTIVE", value: "INACTIVE" }
        ];
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
        this.errorLabel = false;
        this.setting = false;
        this.group = false;
        this.template = false;
        this.activeCheckbox1 = false;
        this.activeCheckbox2 = false;
        this.form = {
            key: '',
            inbox_title: '',
            inbox_message: '',
            banner: '',
            notification_title: '',
            notification_message: '',
            inbox_image: [],
            notification_image: '',
            from: 'LOCARD',
            icon: 'https://res.cloudinary.com/dqcj36zfd/image/upload/v1604047373/admin/image/icon.5f9bd20b43219.png',
            expiry_day: 0,
            cta_type: 1,
            reference: {
                product_id: '',
                order_id: '',
                voucher_id: '',
                booking_id: '',
            },
            logo: 'https://res.cloudinary.com/dqcj36zfd/image/upload/v1604047373/admin/image/icon.5f9bd20b43219.png',
            notification_type: 'TOKEN',
            member_list: [],
            option: 2,
        };
        this.type_cta = [
            { label: 'Go to Invoice Detail', value: 1, selected: 1 },
            { label: 'See Voucher Detail', value: 2 },
            { label: 'See Redeem Detail', value: 3 },
            { label: 'Go to My Voucher', value: 4 },
            { label: 'Go to Shopping Cart', value: 5 },
            { label: 'Go to Discovery Deals', value: 6 },
            { label: 'Go to Discovery product', value: 7 },
            { label: 'Go to Discovery', value: 8 },
            { label: 'Go to Wishlist', value: 9 },
            { label: 'Go to Home', value: 10 },
            { label: 'See Vouchers', value: 11 },
            { label: 'Go to Transaction History Detail', value: 12 },
            { label: 'Go to Payment confirmation on invoice detail', value: 13 },
            { label: 'Go to Product Detail', value: 14 },
            { label: 'Go to Search Result', value: 15 },
        ];
        this.type_option = [
            { label: 'OPT 1', value: 1 },
            { label: 'OPT 2', value: 2, selected: 1 },
            { label: 'OPT 3', value: 3 },
            { label: 'OPT 4', value: 4 },
            { label: 'OPT 5', value: 5 },
        ];
        this.type_notif = [
            { label: 'Broadcast All', value: 'TOPIC' },
            { label: 'Broadcast to specific Users', value: 'TOKEN', selected: 1 },
        ];
        this.showLoading1 = false;
        this.showLoading2 = false;
        this.showLoading3 = false;
        this.showLoading4 = false;
        this.showLoading5 = false;
        this.uploads = {
            banner: '',
            inbox: '',
            notif: '',
            logo: 'https://res.cloudinary.com/dqcj36zfd/image/upload/v1604047373/admin/image/icon.5f9bd20b43219.png',
            icon: 'https://res.cloudinary.com/dqcj36zfd/image/upload/v1604047373/admin/image/icon.5f9bd20b43219.png',
        };
    }
    BroadcastAddComponent.prototype.ngOnInit = function () {
        this.getCollection();
        this.firstLoad();
    };
    BroadcastAddComponent.prototype.getCollection = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.broadcastService.getCTAList()];
                    case 1:
                        result = _a.sent();
                        if (result.result.cta_list) {
                            console.log('result', result.result.cta_list);
                            this.type_cta = [];
                            result.result.cta_list.forEach(function (element) {
                                if (element.type == 1) {
                                    _this.type_cta.push({ label: element.text, value: element.type, selected: 1 });
                                }
                                else {
                                    _this.type_cta.push({ label: element.text, value: element.type });
                                }
                            });
                        }
                        console.log("type", result);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    BroadcastAddComponent.prototype.firstLoad = function () {
        // this.service = this.memberService;
        // let result: any = await this.memberService.getMembersLint();
        // this.Members = result.result;
    };
    BroadcastAddComponent.prototype.addHashtags = function (newWord) {
        newWord = newWord.replace(/\s/g, '');
        this.form.member_list.push(newWord);
    };
    BroadcastAddComponent.prototype.hashtagsKeyDown = function (event) {
        var n = event.target.textContent;
        var key = event.key.toLowerCase();
        // console.log("key", key)
        if (key == ' ' || key == 'enter') {
            if (n.trim() !== '') {
                this.addHashtags(n);
            }
            event.target.textContent = '';
            event.preventDefault();
        }
        if (key == 'backspace' && n == '' && this.form.member_list.length > 0) {
            this.form.member_list.pop();
        }
    };
    BroadcastAddComponent.prototype.stringToNumber = function (str) {
        if (typeof str == 'number') {
            str = str + '';
        }
        var s = str.toLowerCase().replace(/[^0-9]/g, '');
        return parseInt(s);
    };
    BroadcastAddComponent.prototype.formSubmitAddNotificationBroadcast = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.errorLabel = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                    title: 'Do you want to broadcast the message?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Send It'
                }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                    var result_1, e_2;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!result.isConfirmed) return [3 /*break*/, 4];
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 3, , 4]);
                                this.form.inbox_image = [];
                                console.log('result : ', this.form);
                                this.form.banner = this.uploads.banner;
                                this.form.logo = this.uploads.logo;
                                this.form.icon = this.uploads.icon;
                                this.form.inbox_image.push(this.uploads.inbox);
                                this.form.notification_image = this.uploads.notif;
                                this.form.cta_type = this.stringToNumber(this.form.cta_type);
                                this.form.expiry_day = this.stringToNumber(this.form.expiry_day);
                                this.form.option = this.stringToNumber(this.form.option);
                                return [4 /*yield*/, this.broadcastService.addNewBroadcastNotif(this.form)];
                            case 2:
                                result_1 = _a.sent();
                                if (result_1.result) {
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('SUCCESS!', 'Your notification has been sent', 'success');
                                }
                                if (result_1.error) {
                                    this.errorLabel = result_1.error;
                                }
                                return [3 /*break*/, 4];
                            case 3:
                                e_2 = _a.sent();
                                this.errorLabel = (e_2.message); //conversion to Error type
                                return [3 /*break*/, 4];
                            case 4: return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    BroadcastAddComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    BroadcastAddComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    BroadcastAddComponent.prototype.onFileSelected2 = function (event, img) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_1, logo, e_3;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_1 = 3000000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_1) {
                                _this.errorFile = 'Maximum File Upload is 3MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        switch (img) {
                            case 'banner':
                                this.showLoading1 = true;
                                console.log("ini1", img);
                                break;
                            case 'inbox':
                                this.showLoading2 = true;
                                console.log("ini2", img);
                                break;
                            case 'notif':
                                this.showLoading3 = true;
                                console.log("ini3", img);
                                break;
                            case 'logo':
                                this.showLoading4 = true;
                                console.log("ini4", img);
                                break;
                            case 'icon':
                                this.showLoading5 = true;
                                console.log("ini5", img);
                                break;
                        }
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) {
                                console.log("hasil", result);
                                _this.callImage(result, img);
                            })];
                    case 2:
                        logo = _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_3 = _a.sent();
                        this.errorLabel = e_3.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    BroadcastAddComponent.prototype.checkbox1 = function () {
        console.log(this.activeCheckbox1);
        if (this.activeCheckbox1 = true) {
            this.uploads.inbox = this.uploads.banner;
        }
    };
    BroadcastAddComponent.prototype.checkbox2 = function () {
        if (this.activeCheckbox2 = true) {
            this.uploads.notif = this.uploads.banner;
        }
    };
    BroadcastAddComponent.prototype.callImage = function (result, img) {
        if (img == 'banner') {
            this.uploads.banner = result.base_url + result.pic_big_path;
            if (this.activeCheckbox1 = true) {
                this.uploads.inbox = this.uploads.banner;
            }
            if (this.activeCheckbox2 = true) {
                this.uploads.notif = this.uploads.banner;
            }
        }
        else {
            this.uploads[img] = result.base_url + result.pic_big_path;
        }
        this.allfalse();
    };
    BroadcastAddComponent.prototype.allfalse = function () {
        this.showLoading1 = this.showLoading2 = this.showLoading3
            = this.showLoading4 = this.showLoading5 = false;
    };
    BroadcastAddComponent.ctorParameters = function () { return [
        { type: _services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__["BroadcastService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_3__["ProductService"] }
    ]; };
    BroadcastAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-broadcast-add',
            template: __webpack_require__(/*! raw-loader!./broadcast.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notification.broadcast/add/broadcast.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./broadcast.add.component.scss */ "./src/app/layout/modules/notification.broadcast/add/broadcast.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__["BroadcastService"],
            _services_product_product_service__WEBPACK_IMPORTED_MODULE_3__["ProductService"]])
    ], BroadcastAddComponent);
    return BroadcastAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/notification.broadcast/broadcast-routing.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/modules/notification.broadcast/broadcast-routing.module.ts ***!
  \***********************************************************************************/
/*! exports provided: BroadcastRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastRoutingModule", function() { return BroadcastRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _broadcast_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./broadcast.component */ "./src/app/layout/modules/notification.broadcast/broadcast.component.ts");
/* harmony import */ var _add_broadcast_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/broadcast.add.component */ "./src/app/layout/modules/notification.broadcast/add/broadcast.add.component.ts");
/* harmony import */ var _detail_broadcast_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detail/broadcast.detail.component */ "./src/app/layout/modules/notification.broadcast/detail/broadcast.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '', component: _broadcast_component__WEBPACK_IMPORTED_MODULE_2__["BroadcastComponent"],
    },
    {
        path: 'add', component: _add_broadcast_add_component__WEBPACK_IMPORTED_MODULE_3__["BroadcastAddComponent"]
    },
    {
        path: 'detail', component: _detail_broadcast_detail_component__WEBPACK_IMPORTED_MODULE_4__["BroadcastDetailComponent"]
    }
];
var BroadcastRoutingModule = /** @class */ (function () {
    function BroadcastRoutingModule() {
    }
    BroadcastRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], BroadcastRoutingModule);
    return BroadcastRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/notification.broadcast/broadcast.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/layout/modules/notification.broadcast/broadcast.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL25vdGlmaWNhdGlvbi5icm9hZGNhc3QvYnJvYWRjYXN0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/modules/notification.broadcast/broadcast.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/layout/modules/notification.broadcast/broadcast.component.ts ***!
  \******************************************************************************/
/*! exports provided: BroadcastComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastComponent", function() { return BroadcastComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/notification.broadcast/broadcast.service */ "./src/app/services/notification.broadcast/broadcast.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var BroadcastComponent = /** @class */ (function () {
    function BroadcastComponent(broadcastService) {
        this.broadcastService = broadcastService;
        this.Broadcast = [];
        this.tableFormat = {
            title: 'Notification Broadcast Detail',
            label_headers: [
                { label: 'Organization ID', visible: true, type: 'string', data_row_name: 'org_id' },
                { label: 'From', visible: true, type: 'string', data_row_name: 'message_from' },
                // {label: 'Setting ID', visible: false, type: 'string', data_row_name: 'setting_id'},
                { label: 'Group ID', visible: true, type: 'string', data_row_name: 'group_id' },
                // {label: 'Template ID', visible: false, type: 'string', data_row_name: 'template_id'},
                { label: 'Status',
                    options: [
                        'active',
                        'inactive',
                    ],
                    visible: true, type: 'list', data_row_name: 'status' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                addForm: true,
                this: this,
                result_var_name: 'Broadcast',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.broadcastDetail = false;
    }
    BroadcastComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    BroadcastComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.broadcastService;
                        return [4 /*yield*/, this.broadcastService.getBroadcastLint()];
                    case 1:
                        result = _a.sent();
                        this.Broadcast = result.result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    BroadcastComponent.prototype.callDetail = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.broadcastService;
                        return [4 /*yield*/, this.broadcastService.detailBroadcast(_id)];
                    case 1:
                        result = _a.sent();
                        this.broadcastDetail = result.result;
                        console.log('detail', this.broadcastDetail);
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    BroadcastComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.broadcastDetail = false;
                return [2 /*return*/];
            });
        });
    };
    BroadcastComponent.ctorParameters = function () { return [
        { type: _services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__["BroadcastService"] }
    ]; };
    BroadcastComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notification-setting',
            template: __webpack_require__(/*! raw-loader!./broadcast.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notification.broadcast/broadcast.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./broadcast.component.scss */ "./src/app/layout/modules/notification.broadcast/broadcast.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__["BroadcastService"]])
    ], BroadcastComponent);
    return BroadcastComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/notification.broadcast/broadcast.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/layout/modules/notification.broadcast/broadcast.module.ts ***!
  \***************************************************************************/
/*! exports provided: BroadcastModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastModule", function() { return BroadcastModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _broadcast_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./broadcast.component */ "./src/app/layout/modules/notification.broadcast/broadcast.component.ts");
/* harmony import */ var _add_broadcast_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/broadcast.add.component */ "./src/app/layout/modules/notification.broadcast/add/broadcast.add.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _detail_broadcast_detail_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./detail/broadcast.detail.component */ "./src/app/layout/modules/notification.broadcast/detail/broadcast.detail.component.ts");
/* harmony import */ var _broadcast_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./broadcast-routing.module */ "./src/app/layout/modules/notification.broadcast/broadcast-routing.module.ts");
/* harmony import */ var _edit_broadcast_edit_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./edit/broadcast.edit.component */ "./src/app/layout/modules/notification.broadcast/edit/broadcast.edit.component.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var BroadcastModule = /** @class */ (function () {
    function BroadcastModule() {
    }
    BroadcastModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _broadcast_routing_module__WEBPACK_IMPORTED_MODULE_9__["BroadcastRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_11__["BsComponentModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_12__["MatCheckboxModule"],
            ],
            declarations: [
                _broadcast_component__WEBPACK_IMPORTED_MODULE_2__["BroadcastComponent"], _add_broadcast_add_component__WEBPACK_IMPORTED_MODULE_3__["BroadcastAddComponent"], _detail_broadcast_detail_component__WEBPACK_IMPORTED_MODULE_8__["BroadcastDetailComponent"], _edit_broadcast_edit_component__WEBPACK_IMPORTED_MODULE_10__["BroadcastEditComponent"]
            ]
        })
    ], BroadcastModule);
    return BroadcastModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/notification.broadcast/detail/broadcast.detail.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/layout/modules/notification.broadcast/detail/broadcast.detail.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .broadcast-detail label {\n  margin-top: 10px;\n}\n.card-detail .broadcast-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .broadcast-detail .card-header {\n  background-color: #555;\n}\n.card-detail .broadcast-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbm90aWZpY2F0aW9uLmJyb2FkY2FzdC9kZXRhaWwvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxub3RpZmljYXRpb24uYnJvYWRjYXN0XFxkZXRhaWxcXGJyb2FkY2FzdC5kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL25vdGlmaWNhdGlvbi5icm9hZGNhc3QvZGV0YWlsL2Jyb2FkY2FzdC5kZXRhaWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtBQ0FSO0FEQ1E7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDQVo7QURDWTtFQUNJLGlCQUFBO0FDQ2hCO0FERVE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUtBLFdBQUE7QUNMWjtBREVZO0VBQ0ksaUJBQUE7QUNBaEI7QURLSTtFQUNJLGFBQUE7QUNIUjtBRElRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0ZaO0FEaUJJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNmUjtBRGtCUTtFQUNJLGdCQUFBO0FDaEJaO0FEa0JRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2hCWjtBRG1CUTtFQUNJLHNCQUFBO0FDakJaO0FEa0JZO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7QUNoQmhCIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbm90aWZpY2F0aW9uLmJyb2FkY2FzdC9kZXRhaWwvYnJvYWRjYXN0LmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuZWRpdF9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gLmltYWdle1xyXG4gICAgLy8gICAgIGgze1xyXG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAvLyAgICAgICAgIGltZ3tcclxuICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLmJyb2FkY2FzdC1kZXRhaWx7XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLmJyb2FkY2FzdC1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5icm9hZGNhc3QtZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5icm9hZGNhc3QtZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLmJyb2FkY2FzdC1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/notification.broadcast/detail/broadcast.detail.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/layout/modules/notification.broadcast/detail/broadcast.detail.component.ts ***!
  \********************************************************************************************/
/*! exports provided: BroadcastDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastDetailComponent", function() { return BroadcastDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/notification.broadcast/broadcast.service */ "./src/app/services/notification.broadcast/broadcast.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var BroadcastDetailComponent = /** @class */ (function () {
    function BroadcastDetailComponent(broadcastService) {
        this.broadcastService = broadcastService;
        this.errorLabel = false;
        this.edit = false;
    }
    BroadcastDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    BroadcastDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    BroadcastDetailComponent.prototype.editThis = function () {
        //console.log(this.edit );
        this.edit = !this.edit;
        //console.log(this.edit );
    };
    BroadcastDetailComponent.prototype.backToTable = function () {
        console.log(this.back);
        this.back[1](this.back[0]);
    };
    BroadcastDetailComponent.prototype.deleteThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var delResult, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.broadcastService.deleteNotificationSetting(this.detail)];
                    case 1:
                        delResult = _a.sent();
                        //console.log(delResult);
                        if (delResult.error == false) {
                            console.log(this.back[0]);
                            this.back[0].broadcastDetail = false;
                            this.back[0].firstLoad();
                            // delete this.back[0].notificationDetail;
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    BroadcastDetailComponent.ctorParameters = function () { return [
        { type: _services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__["BroadcastService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BroadcastDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BroadcastDetailComponent.prototype, "back", void 0);
    BroadcastDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-broadcast-detail',
            template: __webpack_require__(/*! raw-loader!./broadcast.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notification.broadcast/detail/broadcast.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./broadcast.detail.component.scss */ "./src/app/layout/modules/notification.broadcast/detail/broadcast.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__["BroadcastService"]])
    ], BroadcastDetailComponent);
    return BroadcastDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/notification.broadcast/edit/broadcast.edit.component.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/layout/modules/notification.broadcast/edit/broadcast.edit.component.scss ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .broadcast-detail label {\n  margin-top: 10px;\n}\n.card-detail .broadcast-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .broadcast-detail .card-header {\n  background-color: #555;\n}\n.card-detail .broadcast-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbm90aWZpY2F0aW9uLmJyb2FkY2FzdC9lZGl0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcbm90aWZpY2F0aW9uLmJyb2FkY2FzdFxcZWRpdFxcYnJvYWRjYXN0LmVkaXQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL25vdGlmaWNhdGlvbi5icm9hZGNhc3QvZWRpdC9icm9hZGNhc3QuZWRpdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGtCQUFBO0FDQVI7QURDUTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNBWjtBRENZO0VBQ0ksaUJBQUE7QUNDaEI7QURFUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBS0EsV0FBQTtBQ0xaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBRElRO0VBQ0kseUJBQUE7QUNGWjtBREtJO0VBQ0ksYUFBQTtBQ0hSO0FES1E7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDSFo7QURPSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDTFI7QURTUTtFQUNJLGdCQUFBO0FDUFo7QURTUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNQWjtBRG9DUTtFQUNJLHNCQUFBO0FDbENaO0FEbUNZO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7QUNqQ2hCIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbm90aWZpY2F0aW9uLmJyb2FkY2FzdC9lZGl0L2Jyb2FkY2FzdC5lZGl0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b24udHJ1ZXtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICBcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLmJyb2FkY2FzdC1kZXRhaWx7XHJcbiAgICAgICBcclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gLmltYWdle1xyXG4gICAgICAgIC8vICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIC8vICAgICA+ZGl2e1xyXG4gICAgICAgIC8vICAgICAgICAgd2lkdGg6IDMzLjMzMzMlO1xyXG4gICAgICAgIC8vICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgIC8vICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgICAgLmNhcmQtaGVhZGVye1xyXG4gICAgICAgIC8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vICAgICBoM3tcclxuICAgICAgICAvLyAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAvLyAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAvLyAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgLy8gICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIC8vICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgLy8gICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgLy8gICAgICAgICBpbWd7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICAgICAgLy8gICAgICAgICB9XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5icm9hZGNhc3QtZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAuYnJvYWRjYXN0LWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAuYnJvYWRjYXN0LWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5icm9hZGNhc3QtZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/notification.broadcast/edit/broadcast.edit.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/layout/modules/notification.broadcast/edit/broadcast.edit.component.ts ***!
  \****************************************************************************************/
/*! exports provided: BroadcastEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BroadcastEditComponent", function() { return BroadcastEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/notification.broadcast/broadcast.service */ "./src/app/services/notification.broadcast/broadcast.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var BroadcastEditComponent = /** @class */ (function () {
    function BroadcastEditComponent(broadcastService) {
        this.broadcastService = broadcastService;
        this.loading = false;
        this.settingBroadcast = [];
        this.groupBroadcast = [];
        this.templateBroadcast = [];
        this.broadcastStatus = [
            { label: "ACTIVE", value: "ACTIVE" },
            { label: "INACTIVE", value: "INACTIVE" }
        ];
        this.errorLabel = false;
        this.setting = false;
        this.group = false;
        this.template = false;
    }
    BroadcastEditComponent.prototype.ngOnInit = function () {
        this.getCollection();
        this.firstLoad();
    };
    BroadcastEditComponent.prototype.getCollection = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, e_1;
            var _this = this;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        _d.trys.push([0, 4, , 5]);
                        _a = this;
                        return [4 /*yield*/, this.broadcastService.getSettingLint()];
                    case 1:
                        _a.setting = _d.sent();
                        this.setting.result.forEach(function (element) {
                            _this.settingBroadcast.push({ label: element.name, value: element.id });
                        });
                        _b = this;
                        return [4 /*yield*/, this.broadcastService.getGroupLint()];
                    case 2:
                        _b.group = _d.sent();
                        this.group.result.forEach(function (element) {
                            _this.groupBroadcast.push({ label: element.name, value: element.id });
                        });
                        _c = this;
                        return [4 /*yield*/, this.broadcastService.getTemplateLint()];
                    case 3:
                        _c.template = _d.sent();
                        this.template.result.forEach(function (element) {
                            _this.templateBroadcast.push({ label: element.title, value: element.id });
                        });
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _d.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    BroadcastEditComponent.prototype.firstLoad = function () {
        var _this = this;
        this.detail.previous_status = this.detail.status;
        this.broadcastStatus.forEach(function (element, index) {
            if (element.value == _this.detail.status) {
                _this.broadcastStatus[index].selected = 1;
            }
        });
        console.log('template : ', this.broadcastStatus);
        this.detail.previous_setting_id = this.detail.setting_id;
        this.settingBroadcast.forEach(function (element, index) {
            if (element.value == _this.detail.setting_id) {
                _this.settingBroadcast[index].selected = 1;
            }
        });
        this.detail.previous_group_id = this.detail.group_id;
        this.groupBroadcast.forEach(function (element, index) {
            if (element.value == _this.detail.group_id) {
                _this.groupBroadcast[index].selected = 1;
            }
        });
        this.detail.previous_template_id = this.detail.template_id;
        this.templateBroadcast.forEach(function (element, index) {
            if (element.value == _this.detail.template_id) {
                _this.templateBroadcast[index].selected = 1;
            }
        });
    };
    BroadcastEditComponent.prototype.backToDetail = function () {
        //console.log(this.back);
        this.back[0][this.back[1]]();
    };
    BroadcastEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        console.log(this.detail);
                        this.loading = !this.loading;
                        return [4 /*yield*/, this.broadcastService.updateBroadcastID(this.detail)];
                    case 1:
                        _a.sent();
                        this.loading = !this.loading;
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    BroadcastEditComponent.ctorParameters = function () { return [
        { type: _services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__["BroadcastService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BroadcastEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BroadcastEditComponent.prototype, "back", void 0);
    BroadcastEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-broadcast-edit',
            template: __webpack_require__(/*! raw-loader!./broadcast.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notification.broadcast/edit/broadcast.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./broadcast.edit.component.scss */ "./src/app/layout/modules/notification.broadcast/edit/broadcast.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_notification_broadcast_broadcast_service__WEBPACK_IMPORTED_MODULE_2__["BroadcastService"]])
    ], BroadcastEditComponent);
    return BroadcastEditComponent;
}());



/***/ })

}]);
//# sourceMappingURL=modules-notification-broadcast-broadcast-module.js.map