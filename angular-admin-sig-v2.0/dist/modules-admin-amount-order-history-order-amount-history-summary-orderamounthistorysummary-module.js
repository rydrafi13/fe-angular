(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin-amount-order-history-order-amount-history-summary-orderamounthistorysummary-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/OrderAmountHistorysummary.component.html":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/OrderAmountHistorysummary.component.html ***!
  \***********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n    <app-page-header [heading]=\"'Amount of Sales'\" [icon]=\"'fa-table'\" [topBarMenu]=\"topBarMenu\"></app-page-header>\r\n    <div class=\"summary-report row\">\r\n        <div class=\"col-md-8\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"analitycsData.daily.barChartLabels.length\">\r\n                    <div class=\"card-header\">\r\n                        Order History Per Day\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <canvas baseChart [datasets]=\"analitycsData.daily.barChartData\"\r\n                            [labels]=\"analitycsData.daily.barChartLabels\" [options]=\"barChartOptions\"\r\n                            [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                            (chartClick)=\"chartClicked($event)\">\r\n                        </canvas>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n                        </div> -->\r\n                </div>\r\n            </div>\r\n            <div class=\"row\" style=\"margin-bottom: 20px; text-align: center\">\r\n                <div class=\"col-md-6\">\r\n\r\n                    <div class=\"card \" *ngIf=\"analitycsData.monthly.barChartLabels.length\">\r\n                        <div class=\"card-header\">\r\n                            monthly analytics\r\n                        </div>\r\n                        <div class=\"card-body\">\r\n                            <canvas baseChart [datasets]=\"analitycsData.monthly.barChartData\"\r\n                                [labels]=\"analitycsData.monthly.barChartLabels\" [options]=\"barChartOptions\"\r\n                                [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                                (chartClick)=\"chartClicked($event)\">\r\n                            </canvas>\r\n                        </div>\r\n                        <!-- <div class=\"card-footer\">\r\n                                  <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.monthly)\">Refresh</button>\r\n                              </div> -->\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"card \" *ngIf=\"analitycsData.yearly.barChartLabels\">\r\n                        <div class=\"card-header\">\r\n                            Yearly analytics\r\n                        </div>\r\n                        <div class=\"card-body\">\r\n                            <canvas baseChart [datasets]=\"analitycsData.yearly.barChartData\"\r\n                                [labels]=\"analitycsData.yearly.barChartLabels\" [options]=\"barChartOptions\"\r\n                                [legend]=\"barChartLegend\" [chartType]=\"'bar'\" (chartHover)=\"chartHovered($event)\"\r\n                                (chartClick)=\"chartClicked($event)\">\r\n                            </canvas>\r\n                        </div>\r\n                        <!-- <div class=\"card-footer\">\r\n                                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.hourly)\">Refresh</button>\r\n                                        </div> -->\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"row\" style=\"margin-bottom: 20px; text-align: center\">\r\n                <div class=\"col-md-12\">\r\n                    <div class=\"row-per-card\">\r\n                        <div class=\"card \" *ngIf=\"analitycsData.hourly.barChartLabels\">\r\n                            <div class=\"card-header\">\r\n                                Hourly analytics\r\n                            </div>\r\n                            <div class=\"card-body\">\r\n                                <canvas baseChart [datasets]=\"analitycsData.hourly.barChartData\"\r\n                                    [labels]=\"analitycsData.hourly.barChartLabels\" [options]=\"barChartOptions\"\r\n                                    [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                                    (chartClick)=\"chartClicked($event)\">\r\n                                </canvas>\r\n                            </div>\r\n                            <!-- <div class=\"card-footer\">\r\n                                                <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.hourly)\">Refresh</button>\r\n                                            </div> -->\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-4\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\" style=\"margin-bottom: 20px; text-align: center\">\r\n                    <div class=\"card sum\" *ngIf=\"data.current_order_history\">\r\n                        <div class=\"card-header\"> Today Total Order</div>\r\n                        <div class=\"card-body\">\r\n                                <table style=\"text-align: left\">\r\n                                        <tr>\r\n                                            <td>All</td>\r\n                                            <td>{{data.current_order_history.total | currency:'Rp '}} </td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Pending</td>\r\n                                            <td>{{data.current_order_history_pending.total | currency:'Rp '}} </td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Paid</td>\r\n                                            <td>{{data.current_order_history_paid.total | currency:'Rp '}} </td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Checkout</td>\r\n                                            <td>{{data.current_order_history_checkout.total | currency:'Rp '}} </td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Cancel</td>\r\n                                            <td>{{data.current_order_history_cancel.total | currency:'Rp '}} </td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Waiting</td>\r\n                                            <td>{{data.current_order_history_waiting.total | currency:'Rp '}} </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    </div>\r\n                    </div>\r\n                </div>\r\n                \r\n                <div class=\"col-md-12\" style=\"margin-bottom: 20px; text-align: center\">\r\n                    <div class=\"card sum\" *ngIf=\"data.current_order_history\">\r\n                        <div class=\"card-header\"> Avg Hourly</div>\r\n                        <div class=\"card-body\">\r\n                            <table style=\"text-align: left\">\r\n                                <tr>\r\n                                    <td>All</td>\r\n                                    <td>{{avgData.hourly.all | currency:'Rp '}} </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Pending</td>\r\n                                    <td>{{avgData.hourly.pending | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Paid</td>\r\n                                    <td>{{avgData.hourly.paid  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Checkout</td>\r\n                                    <td>{{avgData.hourly.checkout  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Cancel</td>\r\n                                    <td>{{avgData.hourly.cancel  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Waiting</td>\r\n                                    <td>{{avgData.hourly.waiting | currency:'Rp '}}</td>\r\n                                </tr>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-12\" style=\"margin-bottom: 20px; text-align: center\">\r\n                    <div class=\"card sum\" *ngIf=\"data.current_order_history\">\r\n                        <div class=\"card-header\"> Avg Daily</div>\r\n                        <div class=\"card-body\">\r\n                            <table style=\"text-align: left\">\r\n                                <tr>\r\n                                    <td>All</td>\r\n                                    <td>{{avgData.daily.all | currency:'Rp '}} </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Pending</td>\r\n                                    <td>{{avgData.daily.pending | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Paid</td>\r\n                                    <td>{{avgData.daily.paid  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Checkout</td>\r\n                                    <td>{{avgData.daily.checkout  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Cancel</td>\r\n                                    <td>{{avgData.daily.cancel  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Waiting</td>\r\n                                    <td>{{avgData.daily.waiting | currency:'Rp '}}</td>\r\n                                </tr>\r\n\r\n                            </table>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-12\" style=\"margin-bottom: 20px; text-align: center\">\r\n                    <div class=\"card sum\" *ngIf=\"data.current_order_history\">\r\n                        <div class=\"card-header\"> Avg Monthly</div>\r\n                        <div class=\"card-body\">\r\n                            <table style=\"text-align: left\">\r\n                                <tr>\r\n                                    <td>All</td>\r\n                                    <td>{{avgData.monthly.all | currency:'Rp '}} </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Pending</td>\r\n                                    <td>{{avgData.monthly.pending | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Paid</td>\r\n                                    <td>{{avgData.monthly.paid  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Checkout</td>\r\n                                    <td>{{avgData.monthly.checkout  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Cancel</td>\r\n                                    <td>{{avgData.monthly.cancel  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Waiting</td>\r\n                                    <td>{{avgData.monthly.waiting | currency:'Rp '}}</td>\r\n                                </tr>\r\n\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-12\" style=\"margin-bottom: 20px; text-align: center\">\r\n                    <div class=\"card sum\" *ngIf=\"data.current_order_history\">\r\n                        <div class=\"card-header\"> Avg Yearly</div>\r\n                        <div class=\"card-body\">\r\n                            <table style=\"text-align: left\">\r\n                                <tr>\r\n                                    <td>All</td>\r\n                                    <td>{{avgData.yearly.all | currency:'Rp '}} </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Pending</td>\r\n                                    <td>{{avgData.yearly.pending | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Paid</td>\r\n                                    <td>{{avgData.yearly.paid  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Checkout</td>\r\n                                    <td>{{avgData.yearly.checkout  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Cancel</td>\r\n                                    <td>{{avgData.yearly.cancel  | currency:'Rp '}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Waiting</td>\r\n                                    <td>{{avgData.yearly.waiting | currency:'Rp '}}</td>\r\n                                </tr>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-12\" style=\"float:right\">\r\n                    <div class=\"row-per-card\">\r\n                        <div class=\"card \" *ngIf=\"analitycsData.yearly.barChartLabels\">\r\n                            <div class=\"card-header\">\r\n                                Top 20 Buyers (<em> by Amount of Transactions</em>)\r\n                            </div>\r\n                            <div class=\"card-body\">\r\n                                <table>\r\n                                    <tr>\r\n                                        <th>No.</th>\r\n                                        <th>Name</th>\r\n                                        <th>Number of Transaction</th>\r\n                                    </tr>\r\n                                    <tr *ngFor=\"let m of topMembers;let i = index;\">\r\n                                        <td>{{i+1}}</td>\r\n                                        <td>{{m.user_name}}</td>\r\n                                        <td>{{m.count | currency:'Rp '}}</td>\r\n                                    </tr>\r\n                                </table>\r\n                            </div>\r\n                            <!-- <div class=\"card-footer\">\r\n                                        <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.hourly)\">Refresh</button>\r\n                                    </div> -->\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n\r\n\r\n\r\n    </div>\r\n\r\n\r\n\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/OrderAmountHistorysummary.component.scss":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/OrderAmountHistorysummary.component.scss ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".summary-report .row-per-card {\n  margin-bottom: 20px;\n}\n.summary-report table {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n  text-align: center;\n}\n.summary-report table th {\n  font-weight: bold;\n}\n.summary-report table tr:nth-child(odd) td {\n  background-color: #f0f0f0;\n}\n.summary-report table td, .summary-report table th {\n  border: 1px solid #ccc;\n  padding: 4px 15px;\n}\n.summary-report table td {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tYW1vdW50LW9yZGVyLWhpc3Rvcnkvb3JkZXItYW1vdW50LWhpc3Rvcnktc3VtbWFyeS9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGFkbWluLWFtb3VudC1vcmRlci1oaXN0b3J5XFxvcmRlci1hbW91bnQtaGlzdG9yeS1zdW1tYXJ5XFxPcmRlckFtb3VudEhpc3RvcnlzdW1tYXJ5LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbi1hbW91bnQtb3JkZXItaGlzdG9yeS9vcmRlci1hbW91bnQtaGlzdG9yeS1zdW1tYXJ5L09yZGVyQW1vdW50SGlzdG9yeXN1bW1hcnkuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBS0k7RUFDSSxtQkFBQTtBQ0pSO0FETUk7RUFDSSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDSlI7QURLUTtFQUdJLGlCQUFBO0FDTFo7QURPUTtFQUNJLHlCQUFBO0FDTFo7QURPUTtFQUNJLHNCQUFBO0VBQ0EsaUJBQUE7QUNMWjtBRE9RO0VBQ0ksZUFBQTtBQ0xaIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tYW1vdW50LW9yZGVyLWhpc3Rvcnkvb3JkZXItYW1vdW50LWhpc3Rvcnktc3VtbWFyeS9PcmRlckFtb3VudEhpc3RvcnlzdW1tYXJ5LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN1bW1hcnktcmVwb3J0e1xyXG4gICAgLmNhcmQuc3Vte1xyXG4gICAgICAgIC8vIHdpZHRoOiBjYWxjKDEwMCUgLyAzKTtcclxuXHJcbiAgICB9XHJcbiAgICAucm93LXBlci1jYXJke1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICB9XHJcbiAgICB0YWJsZXtcclxuICAgICAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIHRoe1xyXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kOiAjMzMzO1xyXG4gICAgICAgICAgICAvLyBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0cjpudGgtY2hpbGQob2RkKSB0ZHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGQsIHRoe1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA0cHggMTVweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGR7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufSIsIi5zdW1tYXJ5LXJlcG9ydCAucm93LXBlci1jYXJkIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdGgge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0cjpudGgtY2hpbGQob2RkKSB0ZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdGQsIC5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0aCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIHBhZGRpbmc6IDRweCAxNXB4O1xufVxuLnN1bW1hcnktcmVwb3J0IHRhYmxlIHRkIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary-routing.module.ts":
/*!************************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary-routing.module.ts ***!
  \************************************************************************************************************************************/
/*! exports provided: OrderAmountHistorySummaryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderAmountHistorySummaryRoutingModule", function() { return OrderAmountHistorySummaryRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _orderamounthistorysummary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orderamounthistorysummary.component */ "./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _orderamounthistorysummary_component__WEBPACK_IMPORTED_MODULE_2__["OrderAmountHistorySummaryComponent"],
    },
];
var OrderAmountHistorySummaryRoutingModule = /** @class */ (function () {
    function OrderAmountHistorySummaryRoutingModule() {
    }
    OrderAmountHistorySummaryRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OrderAmountHistorySummaryRoutingModule);
    return OrderAmountHistorySummaryRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary.component.ts":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary.component.ts ***!
  \*******************************************************************************************************************************/
/*! exports provided: OrderAmountHistorySummaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderAmountHistorySummaryComponent", function() { return OrderAmountHistorySummaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var OrderAmountHistorySummaryComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function OrderAmountHistorySummaryComponent(OrderAmountHistoryService, sanitizer) {
        this.OrderAmountHistoryService = OrderAmountHistoryService;
        this.sanitizer = sanitizer;
        this.data = [];
        this.topBarMenu = [
            { label: "Number of Sales", routerLink: '/administrator/order-history-summary' },
            { label: "Amount of Sales", routerLink: '/administrator/order-amount-history-summary', active: true },
            { label: "Abondonment Rate", routerLink: '/administrator/cart-abandonment-summary' }
        ];
        this.avgData = {
            monthly: {
                all: 0,
                pending: 0,
                paid: 0,
                checkout: 0,
                waiting: 0,
                cancel: 0
            },
            daily: {
                all: 0,
                pending: 0,
                paid: 0,
                checkout: 0,
                waiting: 0,
                cancel: 0
            },
            yearly: {
                all: 0,
                pending: 0,
                paid: 0,
                checkout: 0,
                waiting: 0,
                cancel: 0
            },
            hourly: {
                all: 0,
                pending: 0,
                paid: 0,
                checkout: 0,
                waiting: 0,
                cancel: 0
            }
        };
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
            scales: {
                yAxes: [{
                        ticks: {
                            callback: this.currencyFormatter,
                        }
                    }]
            },
        };
        this.barChartLabels = [];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartData = [
            { data: [], label: 'All Order' },
        ];
        this.optO = { fill: false, borderWidth: 1, };
        this.analitycsData = {
            monthly: {
                barChartData: [
                    __assign({ data: [], label: 'All Order' }, this.optO),
                ],
                barChartLabels: []
            },
            daily: {
                barChartData: [
                    __assign({ data: [], label: 'All Order' }, this.optO),
                ],
                barChartLabels: []
            },
            hourly: {
                barChartData: [
                    __assign({ data: [], label: 'All Order' }, this.optO),
                ],
                barChartLabels: []
            },
            yearly: {
                barChartData: [
                    __assign({ data: [], label: 'All Order' }, this.optO),
                ],
                barChartLabels: []
            }
        };
        // this.firstLoad();
        // this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    OrderAmountHistorySummaryComponent.prototype.currencyFormatter = function (value, index, values) {
        // add comma as thousand separator
        var val = 0;
        var kbm = '';
        if (value > (1000000000 - 1)) {
            val = value / 1000000000;
            kbm = "B";
        }
        else if (value > (1000000 - 1)) {
            val = value / 1000000;
            kbm = "M";
        }
        else if (value > (1000 - 1)) {
            val = value / 1000;
            kbm = "K";
        }
        // console.log("VAL ", val, value);
        return (val).toLocaleString('id-ID', {
            style: 'currency',
            currency: 'IDR',
            minimumFractionDigits: 0
        }) + kbm;
    };
    OrderAmountHistorySummaryComponent.prototype.onUpdateCart = function (data) {
        var clonedData = JSON.parse(JSON.stringify(data.barChartData));
        var clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = [];
        data.barChartLabels.forEach(function () {
            clone[0].data.push(Math.round(Math.random() * 100));
        });
        data.barChartData = clone;
        setTimeout(function () {
            // this.firstLoad();
            data.barChartData = clonedData;
        }, 500);
    };
    OrderAmountHistorySummaryComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate;
            return __generator(this, function (_a) {
                currentDate = new Date();
                console.log(this.barChartLabels);
                // this.onUpdateCart();
                this.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    OrderAmountHistorySummaryComponent.prototype.generateChartData = function (dataValues) {
        var barChartLabels = [];
        var newData = [];
        var allData;
        allData = dataValues;
        for (var data in allData) {
            // console.log('allData data', allData[data])
            barChartLabels.push(data);
            newData.push(allData[data]);
        }
        allData = null; //clearing memory
        // console.log("new Data", newData)
        return [barChartLabels, newData];
    };
    OrderAmountHistorySummaryComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, convertedDataMonthly, convertedDataMonthlyPending, convertedDataMonthlyPaid, convertedDataMonthlyCancel, convertedDataMonthlyWaiting, convertedDataMonthlyCheckout, clone, convertedDataDaily, convertedDataDailyPending, convertedDataDailyPaid, convertedDataDailyCancel, convertedDataDailyWaiting, convertedDataDailyCheckout, clone, convertedDataHourly, convertedDataHourlyPending, convertedDataHourlyPaid, convertedDataHourlyCancel, convertedDataHourlyCheckout, convertedDataHourlyWaiting, clone, convertedDataYearly, convertedDataYearlyPending, convertedDataYearlyPaid, convertedDataYearlyCancel, convertedDataYearlyCheckout, convertedDataYearlyWaiting, clone, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.OrderAmountHistoryService;
                        return [4 /*yield*/, this.OrderAmountHistoryService.getAmountOrderHistorySummaryByDate('today')];
                    case 1:
                        result = _a.sent();
                        this.data = result.result;
                        if (this.data.order_history_by_month) {
                            convertedDataMonthly = this.generateChartData(this.data.order_history_by_month.value);
                            convertedDataMonthlyPending = this.generateChartData(this.data.order_history_by_month_pending.value);
                            convertedDataMonthlyPaid = this.generateChartData(this.data.order_history_by_month_paid.value);
                            convertedDataMonthlyCancel = this.generateChartData(this.data.order_history_by_month_cancel.value);
                            convertedDataMonthlyWaiting = this.generateChartData(this.data.order_history_by_month_waiting.value);
                            convertedDataMonthlyCheckout = this.generateChartData(this.data.order_history_by_month_checkout.value);
                            this.avgData.monthly.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month.value));
                            this.avgData.monthly.pending = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_pending.value));
                            this.avgData.monthly.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_paid.value));
                            this.avgData.monthly.cancel = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_cancel.value));
                            this.avgData.monthly.waiting = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_waiting.value));
                            this.avgData.monthly.checkout = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_checkout.value));
                            clone = JSON.parse(JSON.stringify(this.analitycsData.monthly.barChartData));
                            clone[0].data = convertedDataMonthly[1];
                            // console.log('convertedDataMonthlyPending', convertedDataMonthlyPending);
                            clone.push({
                                data: convertedDataMonthlyPending[1],
                                label: "pending",
                                fill: false
                            });
                            clone.push({
                                data: convertedDataMonthlyPaid[1],
                                label: "paid",
                                fill: false
                            });
                            clone.push({
                                data: convertedDataMonthlyCancel[1],
                                label: "Cancel",
                                fill: false
                            });
                            clone.push({
                                data: convertedDataMonthlyWaiting[1],
                                label: "Waiting",
                                fill: false
                            });
                            clone.push({
                                data: convertedDataMonthlyCheckout[1],
                                label: "Checkout",
                                fill: false
                            });
                            console.log("convertedDataMonthly clone", clone);
                            this.barChartLabels = convertedDataMonthly[0];
                            this.analitycsData.monthly.barChartLabels = convertedDataMonthly[0];
                            // this.barChartLabels = convertedDataMonthly[0]
                            // this.analitycsData.monthly.barChartLabels = convertedDataMonthly[0]
                            // this.barChartData   = clone;
                            this.analitycsData.monthly.barChartData = clone;
                        }
                        if (this.data.order_history_by_the_day) {
                            convertedDataDaily = this.generateChartData(this.data.order_history_by_the_day.value);
                            convertedDataDailyPending = this.generateChartData(this.data.order_history_by_the_day_pending.value);
                            convertedDataDailyPaid = this.generateChartData(this.data.order_history_by_the_day_paid.value);
                            convertedDataDailyCancel = this.generateChartData(this.data.order_history_by_the_day_cancel.value);
                            convertedDataDailyWaiting = this.generateChartData(this.data.order_history_by_the_day_waiting.value);
                            convertedDataDailyCheckout = this.generateChartData(this.data.order_history_by_the_day_checkout.value);
                            this.avgData.daily.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day.value));
                            this.avgData.daily.pending = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_pending.value));
                            this.avgData.daily.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_paid.value));
                            this.avgData.daily.cancel = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_cancel.value));
                            this.avgData.daily.waiting = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_waiting.value));
                            this.avgData.daily.checkout = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_checkout.value));
                            clone = JSON.parse(JSON.stringify(this.analitycsData.daily.barChartData));
                            console.log("convertedData daily", clone);
                            clone[0].data = convertedDataDaily[1];
                            clone.push({
                                data: convertedDataDailyPending[1],
                                label: "pending",
                                fill: false
                            });
                            clone.push({
                                data: convertedDataDailyPaid[1],
                                label: "paid",
                                fill: false
                            });
                            clone.push({
                                data: convertedDataDailyCancel[1],
                                label: "Cancel",
                                fill: false
                            });
                            clone.push({
                                data: convertedDataDailyWaiting[1],
                                label: "Waiting",
                                fill: false
                            });
                            clone.push({
                                data: convertedDataDailyCheckout[1],
                                label: "Checkout",
                                fill: false
                            });
                            this.analitycsData.daily.barChartLabels = convertedDataDaily[0];
                            this.barChartLabels = convertedDataDaily[0];
                            // this.barChartData   = clone;
                            this.analitycsData.daily.barChartData = clone;
                            console.log("his.analitycsData", this.analitycsData.daily);
                        }
                        if (this.data.order_history_hourly) {
                            convertedDataHourly = this.generateChartData(this.data.order_history_hourly.value);
                            convertedDataHourlyPending = this.generateChartData(this.data.order_history_hourly_pending.value);
                            convertedDataHourlyPaid = this.generateChartData(this.data.order_history_hourly_paid.value);
                            convertedDataHourlyCancel = this.generateChartData(this.data.order_history_hourly_cancel.value);
                            convertedDataHourlyCheckout = this.generateChartData(this.data.order_history_hourly_checkout.value);
                            convertedDataHourlyWaiting = this.generateChartData(this.data.order_history_hourly_waiting.value);
                            this.avgData.hourly.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly.value));
                            this.avgData.hourly.pending = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_pending.value));
                            this.avgData.hourly.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_paid.value));
                            this.avgData.hourly.cancel = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_cancel.value));
                            this.avgData.hourly.waiting = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_waiting.value));
                            this.avgData.hourly.checkout = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_checkout.value));
                            clone = this.analitycsData.hourly.barChartData;
                            SETUP_DATA_FOR_HOURLY: {
                                clone[0].data = convertedDataHourly[1];
                                clone.push({
                                    data: convertedDataHourlyPending[1],
                                    label: "pending",
                                    fill: false
                                });
                                clone.push({
                                    data: convertedDataHourlyPaid[1],
                                    label: "paid",
                                    fill: false
                                });
                                clone.push({
                                    data: convertedDataHourlyCancel[1],
                                    label: "Cancel",
                                    fill: false
                                });
                                clone.push({
                                    data: convertedDataHourlyWaiting[1],
                                    label: "Waiting",
                                    fill: false
                                });
                                clone.push({
                                    data: convertedDataHourlyCheckout[1],
                                    label: "Checkout",
                                    fill: false
                                });
                            }
                            this.analitycsData.hourly.barChartLabels = convertedDataHourly[0];
                            this.barChartLabels = convertedDataHourly[0];
                            // this.barChartData   = clone;
                            this.analitycsData.hourly.barChartData = clone;
                            console.log("his.analitycsData hourly", this.analitycsData.hourly);
                        }
                        if (this.data.order_history_yearly) {
                            convertedDataYearly = this.generateChartData(this.data.order_history_yearly.value);
                            convertedDataYearlyPending = this.generateChartData(this.data.order_history_yearly_pending.value);
                            convertedDataYearlyPaid = this.generateChartData(this.data.order_history_yearly_paid.value);
                            convertedDataYearlyCancel = this.generateChartData(this.data.order_history_yearly_cancel.value);
                            convertedDataYearlyCheckout = this.generateChartData(this.data.order_history_yearly_checkout.value);
                            convertedDataYearlyWaiting = this.generateChartData(this.data.order_history_yearly_waiting.value);
                            this.avgData.yearly.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly.value));
                            this.avgData.yearly.pending = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_pending.value));
                            this.avgData.yearly.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_paid.value));
                            this.avgData.yearly.cancel = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_cancel.value));
                            this.avgData.yearly.waiting = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_waiting.value));
                            this.avgData.yearly.checkout = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_checkout.value));
                            clone = this.analitycsData.yearly.barChartData;
                            SETUP_DATA_FOR_HOURLY: {
                                clone[0].data = convertedDataYearly[1];
                                clone.push({
                                    data: convertedDataYearlyPending[1],
                                    label: "pending",
                                    fill: false
                                });
                                clone.push({
                                    data: convertedDataYearlyPaid[1],
                                    label: "paid",
                                    fill: false
                                });
                                clone.push({
                                    data: convertedDataYearlyCancel[1],
                                    label: "Cancel",
                                    fill: false
                                });
                                clone.push({
                                    data: convertedDataYearlyWaiting[1],
                                    label: "Waiting",
                                    fill: false
                                });
                                clone.push({
                                    data: convertedDataYearlyCheckout[1],
                                    label: "Checkout",
                                    fill: false
                                });
                            }
                            this.analitycsData.yearly.barChartLabels = convertedDataYearly[0];
                            this.barChartLabels = convertedDataYearly[0];
                            // this.barChartData   = clone;
                            this.analitycsData.yearly.barChartData = clone;
                            console.log("his.analitycsData yearly", this.analitycsData.yearly);
                            TOP_MEMBERS_SETUP: {
                                this.topMembers = this.data.top_users.value;
                            }
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log("this e result", e_1);
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderAmountHistorySummaryComponent.prototype.calculateTheAverageValue = function (_value) {
        var howMany = Object.keys(_value).length;
        var sum = 0;
        Reflect.ownKeys(_value).forEach(function (key) {
            sum = sum + _value[key];
        });
        var avg = sum / howMany;
        return avg;
    };
    OrderAmountHistorySummaryComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] }
    ]; };
    OrderAmountHistorySummaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-OrderAmountHistorysummary-OrderAmountHistory',
            template: __webpack_require__(/*! raw-loader!./OrderAmountHistorysummary.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/OrderAmountHistorysummary.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./OrderAmountHistorysummary.component.scss */ "./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/OrderAmountHistorysummary.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], OrderAmountHistorySummaryComponent);
    return OrderAmountHistorySummaryComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary.module.ts":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary.module.ts ***!
  \****************************************************************************************************************************/
/*! exports provided: OrderAmountHistorySummaryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderAmountHistorySummaryModule", function() { return OrderAmountHistorySummaryModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _orderamounthistorysummary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orderamounthistorysummary.component */ "./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _orderamounthistorysummary_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./orderamounthistorysummary-routing.module */ "./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var OrderAmountHistorySummaryModule = /** @class */ (function () {
    function OrderAmountHistorySummaryModule() {
    }
    OrderAmountHistorySummaryModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _orderamounthistorysummary_routing_module__WEBPACK_IMPORTED_MODULE_7__["OrderAmountHistorySummaryRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_9__["ChartsModule"]
            ],
            declarations: [
                _orderamounthistorysummary_component__WEBPACK_IMPORTED_MODULE_2__["OrderAmountHistorySummaryComponent"]
            ]
        })
    ], OrderAmountHistorySummaryModule);
    return OrderAmountHistorySummaryModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin-amount-order-history-order-amount-history-summary-orderamounthistorysummary-module.js.map