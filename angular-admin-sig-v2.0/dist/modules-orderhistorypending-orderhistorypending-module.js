(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-orderhistorypending-orderhistorypending-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistorypending/detail/orderhistorypending.detail.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/orderhistorypending/detail/orderhistorypending.detail.component.html ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <app-page-header [heading]=\"'Order detail for' + orderhistoryDetail.order_id\" [icon]=\"'fa-table'\"></app-page-header>\r\n    <div *ngIf=\"orderhistoryDetail\">\r\n        <app-orderhistorypending-edit [back]=\"[this, 'editThis']\" [detail]=\"orderHistoryDetail\">\r\n        </app-orderhistorypending-edit>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n<!-- <div *ngIf=\"!edit\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail._id}}</h1>\r\n        <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button>\r\n    </div> \r\n    <div class=\"card-content\">\r\n        <div class=\"orderhistorypending-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> Order History Pending Details </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                            <label>Order ID</label>\r\n                            <div name=\"order_id\" >{{detail.order_id}}</div>\r\n                            <label>Order date</label>\r\n                            <div name=\"created_date\" >{{detail.created_date}}</div>\r\n                            <label>Expired date</label>\r\n                            <div name=\"payment_expire_date\" >{{detail.payment_expire_date.in_string}}</div>\r\n                            <label>User Id</label>\r\n                            <div name=\"user_id\" >{{detail._id}}</div>\r\n                            <label>User Name</label>\r\n                            <div name=\"buyer_detail.name\" >{{detail.buyer_detail.name}}</div>\r\n                            <label>User Email</label>\r\n                            <div name=\"buyer_detail.email\" >{{detail.buyer_detail.email}}</div>\r\n                            <label>Payment Service</label>\r\n                            <div name=\"service\" >{{detail.service}}</div>\r\n                            <label>Status</label>\r\n                            <div name=\"transaction_status\" >{{detail.status}}</div>\r\n                            <label>Total Price</label>\r\n                            <div name=\"billings.sum_total\" >{{detail.billings.sum_total}}</div>\r\n                            \r\n\r\n\r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Shipping Info</h2> </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\" *ngIf=\"!detail.shipping_info\">\r\n                                no shipping info\r\n                            </div>\r\n                            <div class=\"col-md-12\" *ngIf=\"detail.shipping_info\">\r\n                                <ul>\r\n                                    <li *ngFor=\"let si of detail.shipping_info\">\r\n                                            <label>{{si.label}} - {{si.created_date}}</label>\r\n                                    </li>\r\n                                    <label>Awb Receipt</label>\r\n                                    <div name=\"detail.awb_receipt\" >{{detail.awb_receipt}}</div>\r\n                                    <label>Shipping Services</label>\r\n                                    <div name=\"detail.shipping_services\" >{{detail.shipping_services}}</div> \r\n                                </ul>\r\n                            </div>\r\n                        \r\n                        </div> \r\n                    </div>\r\n                 \r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                           \r\n                             \r\n                         </div>\r\n                       \r\n                      </div> \r\n                 </div>\r\n                 \r\n                </div>\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"orderhistorypending-detail\">\r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> Order Summary </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                            <h3> Product List</h3>\r\n                            <table>\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th>Product Name</th>\r\n                                        <th>Qty</th>\r\n                                        <th>Price</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr *ngFor=\"let dp of detail.products\">\r\n                                            <td>{{dp.product_name}} @ {{dp.fixed_price}}</td>\r\n                                            <td>{{dp.quantity}}</td>\r\n                                            <td>{{dp.total_product_price}}</td>\r\n                                    </tr>\r\n                                    <tr *ngFor=\"let bd of detail.additionalBillings\">\r\n                                            <td>{{bd.product_name}}</td>\r\n                                            <td></td>\r\n                                            <td>{{bd.value}}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td colspan=\"2\">Total Price</td>\r\n                                        <td><div name=\"sum_total\" >{{detail.billings.sum_total}}</div></td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                         </div>\r\n                      </div>\r\n                 </div>\r\n                </div>\r\n            </div>\r\n         </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-orderhistorypending-edit [back]=\"[this,'editThis']\" [detail]=\"detail\"></app-orderhistorypending-edit>\r\n</div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistorypending/edit/orderhistorypending.edit.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/orderhistorypending/edit/orderhistorypending.edit.component.html ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i>\r\n            <span *ngIf=\"!loading\">Save</span>\r\n            <span *ngIf=\"loading\">Loading</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"orderhistoryallhistory-detail\">\r\n\r\n            <div class=\"row\">\r\n                <div *ngIf=\"errorLabel!==false\">\r\n                    {{errorLabel}}\r\n                </div>\r\n                <div class=\" col-md-8\">\r\n                    <div class=\"card mb-3\">\r\n\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <div class=\"form-group head-one\">\r\n                                    <label class=\"order-id\"><strong> {{detail.order_id}}</strong> <br />\r\n                                        <span class=\"buyer-name\"><strong>\r\n                                                {{detail.buyer_detail.name}}</strong></span></label>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"form-group col-md-6\">\r\n                                        <label>Order at : <strong>{{detail.created_date}}</strong></label>\r\n                                    </div>\r\n\r\n                                    <div class=\"form-group col-md-6\">\r\n                                        <label>User ID : {{detail._id}} </label>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"product-group\">\r\n                                    <table>\r\n                                        <tr>\r\n                                            <th>Product Name</th>\r\n                                            <th>qty</th>\r\n                                            <th>@price</th>\r\n                                            <th>price</th>\r\n                                        </tr>\r\n                                        <tr *ngFor=\"let product of detail.products\">\r\n                                            <td>{{product.product_name}} <br />\r\n                                                <em><strong>note</strong>: {{product.note_to_merchant}}</em></td>\r\n                                            <td class=\"qty\">{{product.quantity}}x</td>\r\n                                            <td class=\"price\">{{product.price | currency: 'Rp '}}</td>\r\n                                            <td class=\"price\">{{product.fixed_price | currency: 'Rp '}}</td>\r\n                                        </tr>\r\n                                        <tr *ngFor=\"let bd of detail.additionalBillings\">\r\n                                            <td>{{bd.product_name}}</td>\r\n                                            <td></td>\r\n                                            <td></td>\r\n                                            <td class=\"price\">{{bd.value  | currency: 'Rp '}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td colspan=\"3\">Total Price</td>\r\n                                            <td class=\"price\">{{detail.billings.sum_total| currency: 'Rp '}}</td>\r\n                                        </tr>\r\n                                    </table>\r\n\r\n                                </div>\r\n                                <div class=\"buyer-detail\">\r\n                                    <div class=\"form-group\">\r\n                                        <label id=\"buyer-detail\">Buyer Detail</label>\r\n                                        <div class=\"buyer-name form-control-plaintext\">{{detail.buyer_detail.name}}\r\n                                        </div>\r\n                                        <div name=\"address form-control-plaintext\">{{detail.buyer_detail.address}}</div>\r\n                                        <div name=\"address form-control-plaintext\">{{detail.buyer_detail.cell_phone}}\r\n                                        </div>\r\n                                        <div name=\"address form-control-plaintext\">{{detail.buyer_detail.email}}</div>\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Payment Info</h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <label>Transaction Status</label>\r\n                            <div *ngIf=\"!merchantMode\" name=\"transaction_status\">\r\n                                <select name=\"transaction_status\" [(ngModel)]=\"detail.status\">\r\n                                    <option *ngFor=\"let t of transactionStatus\" [ngValue]=\"t.value\">{{t.label}}\r\n                                    </option>\r\n                                </select>\r\n                            </div>\r\n                            <div *ngIf=\"merchantMode\" name=\"transaction_status\">\r\n                                <div class=\"\">\r\n                                    {{detail.status}}\r\n                                </div>\r\n                            </div>\r\n                            <br />\r\n                            <table>\r\n                                <tr>\r\n                                    <th>Service</th>\r\n                                    <th>Via</th>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>{{detail.payment_detail.service}}</td>\r\n                                    <td>{{detail.payment_detail.payment_via}}</td>\r\n                                </tr>\r\n                            </table>\r\n\r\n                            <div *ngIf=\"detail.status.toLowerCase() != 'paid'\" class=\"form-group\">\r\n                                <label>Expired Payment date</label>\r\n                                <div name=\"payment_expire_date\">{{detail.payment_expire_date.in_string}}</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Setup Shipping Information </h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\" *ngIf=\"!detail.shipping_info\">\r\n                                no shipping info\r\n                            </div>\r\n                            <div class=\"col-md-12\" *ngIf=\"detail.shipping_info\">\r\n                                <div class=\"form-group\">\r\n                                    <label>delivery services</label>\r\n                                    <select class=\"form-control\" [(ngModel)]=\"detail.shipping_services\">\r\n\r\n                                        <option *ngFor=\"let ss of shippingServices\" [ngValue]=\"ss.value\">\r\n                                            {{ss.label}}</option>\r\n                                    </select>\r\n                                </div>\r\n\r\n\r\n                                <div class=\"form-group\" *ngIf=\"detail.shipping_services!='no-services'\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <!-- <input type=\"checkbox\" [(ngModel)]=\"sSL.checked\" id=\"{{sSL.id}}\" /> -->\r\n                                        <div class=\"radio-button checked-{{sSL.checked}} hovered-{{sSL.hovered}}\"\r\n                                            (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                        </div>\r\n                                        <label (click)=\"changeshippingStatusList(i, true)\"\r\n                                            (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                            {{sSL.label}}<br />\r\n                                            <span class=\"shipping-date\"\r\n                                                *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                            <div *ngIf=\"sSL.value=='on delivery'\" class=\"awb-list\">\r\n                                                <input\r\n                                                    *ngIf=\"detail.shipping_services!='no-services' && shippingStatusList[2].checked == true\"\r\n                                                    name=\"awb_receipt\" class=\"form-control awb\" [type]=\"'text'\"\r\n                                                    [placeholder]=\"'Put AWB / receipt Number'\"\r\n                                                    [(ngModel)]=\"detail.awb_receipt\" autofocus required />\r\n                                                <input\r\n                                                    *ngIf=\"detail.shipping_services=='no-services' || shippingStatusList[2].checked == false \"\r\n                                                    class=\"form-control awb\" disabled name=\"unnamed\"\r\n                                                    [placeholder]=\"'Put AWB / receipt Number'\"\r\n                                                    title=\"You can add AWB after choosing the services\" />\r\n                                            </div>\r\n                                        </label>\r\n\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group\" *ngIf=\"detail.shipping_services=='no-services'\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <!-- <input type=\"checkbox\" [(ngModel)]=\"sSL.checked\" id=\"{{sSL.id}}\" /> -->\r\n                                        <div class=\"radio-button checked-false hovered-false\">\r\n                                        </div>\r\n                                        <label>\r\n                                            {{sSL.label}}<br />\r\n                                            <span class=\"shipping-date\"\r\n                                                *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                        </label>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <!-- <div class=\"card mb-3\">\r\n                     \r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Email </label>\r\n                            <form-input name=\"email\" [type]=\"'text'\" [placeholder]=\"'Email'\"  [(ngModel)]=\"detail.email\" autofocus required></form-input>\r\n\r\n                            <label>Cell Phone </label>\r\n                            <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Cell Phone'\"  [(ngModel)]=\"detail.cell_phone\" autofocus required></form-input>\r\n                            \r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>  -->\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistorypending/orderhistorypending.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/orderhistorypending/orderhistorypending.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n  <app-page-header [heading]=\"'Order History Pending'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  <div *ngIf=\"Orderhistorypending&&orderhistorypendingDetail==false\">\r\n        <app-form-builder-table \r\n        [searchCallback]=\"[service, 'searchOrderhistorypendingLint',this]\" \r\n        [tableFormat]=\"tableFormat\"\r\n        [table_data]=\"Orderhistorypending\"\r\n        [total_page]=\"totalPage\"\r\n      >\r\n            \r\n        </app-form-builder-table>\r\n  </div>\r\n      <!-- <div *ngIf=\"orderhistorypendingDetail\">\r\n            <app-orderhistorypending-detail [back]=\"[this,backToHere]\" [detail]=\"orderhistorypendingDetail\"></app-orderhistorypending-detail>\r\n      </div> -->\r\n\r\n  </div>\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/modules/orderhistorypending/detail/orderhistorypending.detail.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistorypending/detail/orderhistorypending.detail.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -5px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 16px;\n}\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .orderhistorypending-detail label {\n  margin-top: 10px;\n}\n.card-detail .orderhistorypending-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .orderhistorypending-detail .card-header {\n  background-color: #555;\n}\n.card-detail .orderhistorypending-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\ntable {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n}\ntable thead {\n  background-color: #555;\n}\ntable thead th {\n  color: white;\n  padding: 10px;\n  border: 1px solid #ccc;\n}\ntable tbody td {\n  padding: 10px;\n  border: 1px solid #ccc;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvb3JkZXJoaXN0b3J5cGVuZGluZy9kZXRhaWwvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxvcmRlcmhpc3RvcnlwZW5kaW5nXFxkZXRhaWxcXG9yZGVyaGlzdG9yeXBlbmRpbmcuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9vcmRlcmhpc3RvcnlwZW5kaW5nL2RldGFpbC9vcmRlcmhpc3RvcnlwZW5kaW5nLmRldGFpbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFSTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7QUNEUjtBREVRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTlo7QURHWTtFQUNJLGlCQUFBO0FDRGhCO0FETUk7RUFDSSxhQUFBO0FDSlI7QURLUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNIWjtBRGtCSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDaEJSO0FEbUJRO0VBQ0ksZ0JBQUE7QUNqQlo7QURtQlE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDakJaO0FEb0JRO0VBQ0ksc0JBQUE7QUNsQlo7QURtQlk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ2pCaEI7QUR1QkE7RUFDSSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtBQ3BCSjtBRHFCSTtFQUNJLHNCQXBHZ0I7QUNpRnhCO0FEb0JRO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQ2xCWjtBRHdCUTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtBQ3RCWiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL29yZGVyaGlzdG9yeXBlbmRpbmcvZGV0YWlsL29yZGVyaGlzdG9yeXBlbmRpbmcuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGJhY2tncm91bmRDb2xvckhlYWRlcjogIzU1NTtcclxuLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDhweDtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC01cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuZWRpdF9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTZweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gLmltYWdle1xyXG4gICAgLy8gICAgIGgze1xyXG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAvLyAgICAgICAgIGltZ3tcclxuICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm9yZGVyaGlzdG9yeXBlbmRpbmctZGV0YWlse1xyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbnRhYmxle1xyXG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgdGhlYWR7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGJhY2tncm91bmRDb2xvckhlYWRlcjtcclxuICAgICAgICB0aHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBwYWRkaW5nOjEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcblxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0Ym9keXtcclxuICAgICAgICB0ZHtcclxuICAgICAgICAgICAgcGFkZGluZzoxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59IiwiLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiA0OHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC01cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDE2cHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeXBlbmRpbmctZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5cGVuZGluZy1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeXBlbmRpbmctZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeXBlbmRpbmctZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxudGFibGUge1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICB3aWR0aDogMTAwJTtcbn1cbnRhYmxlIHRoZWFkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbnRhYmxlIHRoZWFkIHRoIHtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxudGFibGUgdGJvZHkgdGQge1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/orderhistorypending/detail/orderhistorypending.detail.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistorypending/detail/orderhistorypending.detail.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: OrderhistorypendingDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistorypendingDetailComponent", function() { return OrderhistorypendingDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var OrderhistorypendingDetailComponent = /** @class */ (function () {
    function OrderhistorypendingDetailComponent(orderhistoryService, route, router) {
        this.orderhistoryService = orderhistoryService;
        this.route = route;
        this.router = router;
    }
    OrderhistorypendingDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderhistorypendingDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                    var historyID;
                    return __generator(this, function (_a) {
                        historyID = params.id;
                        this.loadHistoryDetail(historyID);
                        return [2 /*return*/];
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    OrderhistorypendingDetailComponent.prototype.loadHistoryDetail = function (historyID) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.orderhistoryService;
                        return [4 /*yield*/, this.orderhistoryService.detailOrderhistory(historyID)];
                    case 1:
                        result = _a.sent();
                        // console.log("result", result);
                        this.orderhistoryDetail = result.result[0];
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderhistorypendingDetailComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    OrderhistorypendingDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-orderhistorypending-detail',
            template: __webpack_require__(/*! raw-loader!./orderhistorypending.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistorypending/detail/orderhistorypending.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./orderhistorypending.detail.component.scss */ "./src/app/layout/modules/orderhistorypending/detail/orderhistorypending.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], OrderhistorypendingDetailComponent);
    return OrderhistorypendingDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistorypending/edit/orderhistorypending.edit.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistorypending/edit/orderhistorypending.edit.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -5px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 16px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail table {\n  width: 100%;\n  border-collapse: collapse;\n  border: 1px solid #333;\n}\n.card-detail table td {\n  border-style: solid;\n  border-color: #333;\n  border-width: 0px 1px 0px 0px;\n  padding: 5px;\n  vertical-align: top;\n}\n.card-detail table td em {\n  display: block;\n  margin-left: 5px;\n  font-size: 10px;\n  font-style: italic;\n}\n.card-detail table td.qty {\n  text-align: center;\n}\n.card-detail table td.price {\n  text-align: right;\n}\n.card-detail table th {\n  background-color: #999;\n  color: white;\n  border: 1px solid #333;\n  padding: 5px;\n  text-align: left;\n  font-weight: bold;\n  text-transform: capitalize;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .orderhistoryallhistory-detail label {\n  margin-top: 10px;\n}\n.card-detail .orderhistoryallhistory-detail .head-one {\n  line-height: 33px;\n  border-bottom: 2px solid #333;\n}\n.card-detail .orderhistoryallhistory-detail .head-one label.order-id {\n  font-size: 26px;\n  overflow: hidden;\n  width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n}\n.card-detail .orderhistoryallhistory-detail .head-one label.buyer-name {\n  font-size: 24px;\n  overflow: hidden;\n  width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n}\n.card-detail .orderhistoryallhistory-detail .buyer-detail {\n  border-top: 2px solid #333;\n}\n.card-detail .orderhistoryallhistory-detail .buyer-detail label#buyer-detail {\n  font-size: 12px;\n  font-weight: bold;\n}\n.card-detail .orderhistoryallhistory-detail .buyer-detail label#buyer-detail .buyer-name {\n  font-weight: bold;\n  font-size: 14px;\n}\n.card-detail .orderhistoryallhistory-detail .product-group {\n  padding-bottom: 25px;\n}\n.card-detail .orderhistoryallhistory-detail label + div {\n  font-weight: normal;\n}\n.card-detail .orderhistoryallhistory-detail .card-header {\n  background-color: #555;\n}\n.card-detail .orderhistoryallhistory-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list {\n  float: left;\n  width: 100%;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list .awb {\n  font-size: 11px;\n  margin-top: 10px;\n}\n.shipping-date {\n  color: #999;\n  font-size: 10px;\n}\n.shipping-status {\n  float: left;\n  width: 100%;\n  border-left: 2px solid black;\n  position: relative;\n  text-transform: capitalize;\n}\n.shipping-status .radio-button {\n  display: flex;\n  width: 16px;\n  height: 16px;\n  border: 2px solid black;\n  background: white;\n  border-radius: 16px;\n  float: left;\n  -webkit-transform: translate(-9px, -7px);\n          transform: translate(-9px, -7px);\n  position: absolute;\n  cursor: pointer;\n  justify-content: center;\n  align-items: center;\n}\n.shipping-status .radio-button + label {\n  display: block;\n  float: left;\n  width: calc(100% - 27px);\n  padding: 0px 5px 10px 10px;\n  -webkit-transform: translate(0px, -22px);\n          transform: translate(0px, -22px);\n  cursor: pointer;\n}\n.shipping-status .radio-button.checked-true {\n  border-color: #2ecc71;\n}\n.shipping-status .radio-button.checked-true:before {\n  content: \"\";\n  background-color: #2ecc71;\n  display: block;\n  border-radius: 9px;\n  width: 8px !important;\n  height: 8px !important;\n}\n.shipping-status .radio-button.hovered-true:before {\n  background-color: #2ecc71;\n  width: 6px;\n  height: 6px;\n  content: \"\";\n  border-radius: 8px;\n  background-color: #2ecc71;\n  display: block;\n}\n.shipping-status:last-child {\n  border-left-color: transparent;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvb3JkZXJoaXN0b3J5cGVuZGluZy9lZGl0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcb3JkZXJoaXN0b3J5cGVuZGluZ1xcZWRpdFxcb3JkZXJoaXN0b3J5cGVuZGluZy5lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9vcmRlcmhpc3RvcnlwZW5kaW5nL2VkaXQvb3JkZXJoaXN0b3J5cGVuZGluZy5lZGl0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQ0FSO0FERVE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDRFo7QURHWTtFQUNJLGlCQUFBO0FDRGhCO0FES1E7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQU1BLFdBQUE7QUNUWjtBREtZO0VBQ0ksaUJBQUE7QUNIaEI7QURTUTtFQUNJLHlCQUFBO0FDUFo7QURXSTtFQUNJLFdBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0FDVFI7QURXUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ1RaO0FEV1k7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNUaEI7QURlUTtFQUNJLGtCQUFBO0FDYlo7QURnQlE7RUFDSSxpQkFBQTtBQ2RaO0FEaUJRO0VBQ0ksc0JBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDBCQUFBO0FDZlo7QURtQkk7RUFDSSxhQUFBO0FDakJSO0FEbUJRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ2pCWjtBRHFCSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDbkJSO0FEd0JRO0VBQ0ksZ0JBQUE7QUN0Qlo7QUR5QlE7RUFDSSxpQkFBQTtFQUNBLDZCQUFBO0FDdkJaO0FEeUJZO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUN2QmhCO0FEMEJZO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUN4QmhCO0FENEJRO0VBQ0ksMEJBQUE7QUMxQlo7QUQ0Qlk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7QUMxQmhCO0FENEJnQjtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQzFCcEI7QUQrQlE7RUFDSSxvQkFBQTtBQzdCWjtBRGtDUTtFQUVJLG1CQUFBO0FDakNaO0FEcUVRO0VBQ0ksc0JBQUE7QUNuRVo7QURxRVk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ25FaEI7QUR3RVE7RUFDSSxXQUFBO0VBQ0EsV0FBQTtBQ3RFWjtBRHdFWTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtBQ3RFaEI7QUQ0RUE7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQ3pFSjtBRDRFQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtFQUNBLDBCQUFBO0FDekVKO0FEMkVJO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLHdDQUFBO1VBQUEsZ0NBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDekVSO0FENkVJO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFDQSx3QkFBQTtFQUNBLDBCQUFBO0VBQ0Esd0NBQUE7VUFBQSxnQ0FBQTtFQUNBLGVBQUE7QUMzRVI7QUQrRUk7RUFDSSxxQkFBQTtBQzdFUjtBRGdGSTtFQUNJLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7QUM5RVI7QURpRkk7RUFDSSx5QkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0FDL0VSO0FEbUZBO0VBQ0ksOEJBQUE7QUNoRkoiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9vcmRlcmhpc3RvcnlwZW5kaW5nL2VkaXQvb3JkZXJoaXN0b3J5cGVuZGluZy5lZGl0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtZGV0YWlsIHtcclxuICAgID4uY2FyZC1oZWFkZXIge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtaW4taGVpZ2h0OiA0OHB4O1xyXG5cclxuICAgICAgICAuYmFja19idXR0b24ge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtNXB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG5cclxuICAgICAgICAgICAgaSB7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLnNhdmVfYnV0dG9uIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICAgICAgICAgIGkge1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJpZ2h0OiAxNnB4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLnNhdmVfYnV0dG9uLnRydWUge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0YWJsZSB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMzMzO1xyXG5cclxuICAgICAgICB0ZCB7XHJcbiAgICAgICAgICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICAgICAgICAgIGJvcmRlci1jb2xvcjogIzMzMztcclxuICAgICAgICAgICAgYm9yZGVyLXdpZHRoOiAwcHggMXB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuXHJcbiAgICAgICAgICAgIGVtIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgICAgICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0ZC5xdHkge1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0ZC5wcmljZSB7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGgge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOTk5O1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuY2FyZC1jb250ZW50IHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG5cclxuICAgICAgICA+LmNvbC1tZC0xMiB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGgxIHtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuXHJcbiAgICAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwge1xyXG5cclxuICAgICAgICBsYWJlbCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuaGVhZC1vbmUge1xyXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMzNweDtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICMzMzM7XHJcblxyXG4gICAgICAgICAgICBsYWJlbC5vcmRlci1pZCB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDI2cHg7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxhYmVsLmJ1eWVyLW5hbWUge1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyNHB4O1xyXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuYnV5ZXItZGV0YWlsIHtcclxuICAgICAgICAgICAgYm9yZGVyLXRvcDogMnB4IHNvbGlkICMzMzM7XHJcblxyXG4gICAgICAgICAgICBsYWJlbCNidXllci1kZXRhaWwge1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcblxyXG4gICAgICAgICAgICAgICAgLmJ1eWVyLW5hbWUge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLnByb2R1Y3QtZ3JvdXAge1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMjVweDtcclxuXHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGFiZWwrZGl2IHtcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICAgICAgLy8gcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgLy8gYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgLy8gbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgLy8gbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgLy8gZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICAvLyBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIC5pbWFnZXtcclxuICAgICAgICAvLyAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAvLyAgICAgPmRpdntcclxuICAgICAgICAvLyAgICAgICAgIHdpZHRoOiAzMy4zMzMzJTtcclxuICAgICAgICAvLyAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAvLyAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gICAgIC5jYXJkLWhlYWRlcntcclxuICAgICAgICAvLyAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgICAgaDN7XHJcbiAgICAgICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgLy8gICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgLy8gICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIC8vICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAvLyAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIC8vICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgIC8vICAgICAgICAgaW1ne1xyXG4gICAgICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgICAgICAvLyAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgICAgLy8gICAgICAgICB9XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIC5jYXJkLWhlYWRlciB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcblxyXG4gICAgICAgICAgICBoMiB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5hd2ItbGlzdCB7XHJcbiAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgIC5hd2Ige1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnNoaXBwaW5nLWRhdGUge1xyXG4gICAgY29sb3I6ICM5OTk7XHJcbiAgICBmb250LXNpemU6IDEwcHg7XHJcbn1cclxuXHJcbi5zaGlwcGluZy1zdGF0dXMge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1sZWZ0OiAycHggc29saWQgYmxhY2s7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuXHJcbiAgICAucmFkaW8tYnV0dG9uIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgICAgIGhlaWdodDogMTZweDtcclxuICAgICAgICBib3JkZXI6IDJweCBzb2xpZCBibGFjaztcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxNnB4O1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC05cHgsIC03cHgpO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLnJhZGlvLWJ1dHRvbitsYWJlbCB7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDI3cHgpO1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCA1cHggMTBweCAxMHB4O1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgLTIycHgpO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLnJhZGlvLWJ1dHRvbi5jaGVja2VkLXRydWUge1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogIzJlY2M3MTtcclxuICAgIH1cclxuXHJcbiAgICAucmFkaW8tYnV0dG9uLmNoZWNrZWQtdHJ1ZTpiZWZvcmUge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA5cHg7XHJcbiAgICAgICAgd2lkdGg6IDhweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIGhlaWdodDogOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLnJhZGlvLWJ1dHRvbi5ob3ZlcmVkLXRydWU6YmVmb3JlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmVjYzcxO1xyXG4gICAgICAgIHdpZHRoOiA2cHg7XHJcbiAgICAgICAgaGVpZ2h0OiA2cHg7XHJcbiAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIH1cclxufVxyXG5cclxuLnNoaXBwaW5nLXN0YXR1czpsYXN0LWNoaWxkIHtcclxuICAgIGJvcmRlci1sZWZ0LWNvbG9yOiB0cmFuc3BhcmVudDtcclxufVxyXG4iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1pbi1oZWlnaHQ6IDQ4cHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTZweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcbn1cbi5jYXJkLWRldGFpbCB0YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjMzMzO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRkIHtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLWNvbG9yOiAjMzMzO1xuICBib3JkZXItd2lkdGg6IDBweCAxcHggMHB4IDBweDtcbiAgcGFkZGluZzogNXB4O1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRkIGVtIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRkLnF0eSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jYXJkLWRldGFpbCB0YWJsZSB0ZC5wcmljZSB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRoIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzk5OTtcbiAgY29sb3I6IHdoaXRlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjMzMzO1xuICBwYWRkaW5nOiA1cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmhlYWQtb25lIHtcbiAgbGluZS1oZWlnaHQ6IDMzcHg7XG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjMzMzO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuaGVhZC1vbmUgbGFiZWwub3JkZXItaWQge1xuICBmb250LXNpemU6IDI2cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmhlYWQtb25lIGxhYmVsLmJ1eWVyLW5hbWUge1xuICBmb250LXNpemU6IDI0cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmJ1eWVyLWRldGFpbCB7XG4gIGJvcmRlci10b3A6IDJweCBzb2xpZCAjMzMzO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuYnV5ZXItZGV0YWlsIGxhYmVsI2J1eWVyLWRldGFpbCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5idXllci1kZXRhaWwgbGFiZWwjYnV5ZXItZGV0YWlsIC5idXllci1uYW1lIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLnByb2R1Y3QtZ3JvdXAge1xuICBwYWRkaW5nLWJvdHRvbTogMjVweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5hd2ItbGlzdCB7XG4gIGZsb2F0OiBsZWZ0O1xuICB3aWR0aDogMTAwJTtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmF3Yi1saXN0IC5hd2Ige1xuICBmb250LXNpemU6IDExcHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbi5zaGlwcGluZy1kYXRlIHtcbiAgY29sb3I6ICM5OTk7XG4gIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuLnNoaXBwaW5nLXN0YXR1cyB7XG4gIGZsb2F0OiBsZWZ0O1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLWxlZnQ6IDJweCBzb2xpZCBibGFjaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cbi5zaGlwcGluZy1zdGF0dXMgLnJhZGlvLWJ1dHRvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxNnB4O1xuICBoZWlnaHQ6IDE2cHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkIGJsYWNrO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTZweDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC05cHgsIC03cHgpO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uc2hpcHBpbmctc3RhdHVzIC5yYWRpby1idXR0b24gKyBsYWJlbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmbG9hdDogbGVmdDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDI3cHgpO1xuICBwYWRkaW5nOiAwcHggNXB4IDEwcHggMTBweDtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAtMjJweCk7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5zaGlwcGluZy1zdGF0dXMgLnJhZGlvLWJ1dHRvbi5jaGVja2VkLXRydWUge1xuICBib3JkZXItY29sb3I6ICMyZWNjNzE7XG59XG4uc2hpcHBpbmctc3RhdHVzIC5yYWRpby1idXR0b24uY2hlY2tlZC10cnVlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyZWNjNzE7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBib3JkZXItcmFkaXVzOiA5cHg7XG4gIHdpZHRoOiA4cHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiA4cHggIWltcG9ydGFudDtcbn1cbi5zaGlwcGluZy1zdGF0dXMgLnJhZGlvLWJ1dHRvbi5ob3ZlcmVkLXRydWU6YmVmb3JlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcbiAgd2lkdGg6IDZweDtcbiAgaGVpZ2h0OiA2cHg7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5zaGlwcGluZy1zdGF0dXM6bGFzdC1jaGlsZCB7XG4gIGJvcmRlci1sZWZ0LWNvbG9yOiB0cmFuc3BhcmVudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/orderhistorypending/edit/orderhistorypending.edit.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistorypending/edit/orderhistorypending.edit.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: OrderhistorypendingEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistorypendingEditComponent", function() { return OrderhistorypendingEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _object_interface_common_function__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../object-interface/common.function */ "./src/app/object-interface/common.function.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var OrderhistorypendingEditComponent = /** @class */ (function () {
    function OrderhistorypendingEditComponent(orderhistoryService) {
        this.orderhistoryService = orderhistoryService;
        this.loading = false;
        this.done = false;
        this.shippingStatusList = [
            { label: 'on checking and processing', value: 'on process', id: 'on-process', checked: true, hovered: false },
            { label: 'Warehouse Packaging', value: 'on packaging', id: 'warehouse-packaging', hovered: false },
            { label: 'On Delivery process', value: 'on delivery', id: 'on-delivery', hovered: false },
            { label: 'Delivered', value: 'delivered', id: 'delivered', hovered: false }
        ];
        this.shippingServices = [
            { label: 'no services', value: 'no-services', id: 'noshipping' },
            // {label: 'JNE', value: 'JNE', id:'jne'},
            { label: 'SAP', value: 'SAP', id: 'sap' },
            { label: 'Si Cepat', value: 'Si Cepat', id: 'sicepat' },
        ];
        this.transactionStatus = [
            { label: "PENDING", value: "PENDING" },
            { label: "CANCEL", value: "CANCEL" },
            { label: "ORDER", value: "ORDER" },
            { label: "CHECKOUT", value: "CHECKOUT" },
            { label: "PAID", value: "PAID" },
            { label: "ERROR", value: "ERROR" },
            { label: "WAITING", value: "WAITING" }
        ];
        this.errorLabel = false;
        this.merchantMode = false;
    }
    OrderhistorypendingEditComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderhistorypendingEditComponent.prototype.isShippingValueExists = function (value) {
        for (var _i = 0, _a = this.detail.shipping_info; _i < _a.length; _i++) {
            var element = _a[_i];
            console.log("value", value, element.label);
            if (element.label.trim().toLowerCase() == value.trim().toLowerCase()) {
                return [true, element.created_date];
            }
        }
        return false;
    };
    OrderhistorypendingEditComponent.prototype.ngOnChange = function () {
        console.log("HELLO");
    };
    OrderhistorypendingEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var billings, additionalBillings, prop, _a, e_1;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        if (this.detail.billings) {
                            billings = this.detail.billings;
                            additionalBillings = [];
                            if (billings.discount) {
                                additionalBillings.push({
                                    product_name: billings.discount.product_name,
                                    value: billings.discount.value
                                });
                            }
                            if (this.detail.payment_detail.payment_via != 'manual banking') {
                                if (billings.unique_amount) {
                                    delete billings.unique_amount;
                                }
                            }
                            for (prop in billings) {
                                console.log("PROP", prop);
                                if (prop == 'shipping_fee' || prop == 'unique_amount') {
                                    additionalBillings.push({
                                        product_name: prop,
                                        value: billings[prop]
                                    });
                                }
                            }
                            console.log(this.detail.billings);
                            this.detail.additionalBillings = additionalBillings;
                        }
                        if (this.detail.merchant) {
                            this.merchantMode = true;
                        }
                        if (!this.detail.shipping_services) {
                            this.detail.shipping_services = 'no-services';
                            // console.log("Result", shippingServices)
                        }
                        if (this.detail.status) {
                            this.detail.status = this.detail.status.trim().toUpperCase();
                        }
                        if (this.detail.shipping_info) {
                            console.log('this.detail.shipping_info', this.detail.shipping_info);
                            this.shippingStatusList.forEach(function (element, index) {
                                var check = _this.isShippingValueExists(element.value);
                                if (check) {
                                    _this.shippingStatusList[index].checked = check[0];
                                    _this.shippingStatusList[index].created_date = check[1];
                                }
                                else {
                                    _this.shippingStatusList[index].checked = false;
                                }
                            });
                        }
                        _a = this.detail;
                        return [4 /*yield*/, this.detail.status];
                    case 1:
                        _a.previous_status = _b.sent();
                        this.transactionStatus.forEach(function (element, index) {
                            if (element.value == _this.detail.transaction_status) {
                                _this.transactionStatus[index].selected = 1;
                            }
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _b.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderhistorypendingEditComponent.prototype.backToDetail = function () {
        if (this.back[0][this.back[1]] && !Object(_object_interface_common_function__WEBPACK_IMPORTED_MODULE_3__["isFunction"])(this.back[0])) {
            this.back[0][this.back[1]];
        }
        else if (Object(_object_interface_common_function__WEBPACK_IMPORTED_MODULE_3__["isFunction"])(this.back[0])) {
            this.back[0]();
        }
        else {
            window.history.back();
        }
    };
    OrderhistorypendingEditComponent.prototype.changeshippingStatusList = function (index) {
        for (var n = 0; n <= index; n++) {
            //  console.log("HOVERED",  this.shippingStatusList[n].hovered)
            this.shippingStatusList[n].checked = true;
        }
        for (var n = index + 1; n < this.shippingStatusList.length; n++) {
            this.shippingStatusList[n].checked = false;
        }
    };
    OrderhistorypendingEditComponent.prototype.hoverShippingStatusList = function (index, objectHover) {
        // console.log("HOVERED", index)
        for (var n = 0; n <= index; n++) {
            //  console.log("HOVERED",  this.shippingStatusList[n].hovered)
            this.shippingStatusList[n].hovered = true;
        }
        for (var n = index + 1; n < this.shippingStatusList.length; n++) {
            this.shippingStatusList[n].hovered = false;
        }
    };
    OrderhistorypendingEditComponent.prototype.convertShippingStatusToShippingInfo = function () {
        var newData = [];
        this.shippingStatusList.forEach(function (element, index) {
            if (element.checked) {
                newData.push({ value: element.value });
            }
        });
        return newData;
    };
    OrderhistorypendingEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var frm, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.loading = !this.loading;
                        frm = JSON.parse(JSON.stringify(this.detail));
                        frm.shipping_info = this.convertShippingStatusToShippingInfo();
                        // this.detail.shipping_status = this.valu
                        delete frm.buyer_detail;
                        delete frm.payment_expire_date;
                        delete frm.products;
                        return [4 /*yield*/, this.orderhistoryService.updateOrderHistoryAllHistory(frm)];
                    case 1:
                        _a.sent();
                        this.loading = !this.loading;
                        this.done = true;
                        alert("Edit submitted");
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderhistorypendingEditComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderhistorypendingEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderhistorypendingEditComponent.prototype, "back", void 0);
    OrderhistorypendingEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-orderhistorypending-edit',
            template: __webpack_require__(/*! raw-loader!./orderhistorypending.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistorypending/edit/orderhistorypending.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./orderhistorypending.edit.component.scss */ "./src/app/layout/modules/orderhistorypending/edit/orderhistorypending.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"]])
    ], OrderhistorypendingEditComponent);
    return OrderhistorypendingEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistorypending/orderhistorypending-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistorypending/orderhistorypending-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: OrderhistorypendingRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistorypendingRoutingModule", function() { return OrderhistorypendingRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _orderhistorypending_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orderhistorypending.component */ "./src/app/layout/modules/orderhistorypending/orderhistorypending.component.ts");
/* harmony import */ var _detail_orderhistorypending_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detail/orderhistorypending.detail.component */ "./src/app/layout/modules/orderhistorypending/detail/orderhistorypending.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';
var routes = [
    {
        path: '', component: _orderhistorypending_component__WEBPACK_IMPORTED_MODULE_2__["OrderhistorypendingComponent"]
    },
    // {
    //   path:'detail', component: OrderhistorysuccessDetailComponent
    // },
    // {
    //   path:'edit', component: OrderhistorysummaryEditComponent
    // }
    {
        path: 'edit', component: _detail_orderhistorypending_detail_component__WEBPACK_IMPORTED_MODULE_3__["OrderhistorypendingDetailComponent"]
    }
];
var OrderhistorypendingRoutingModule = /** @class */ (function () {
    function OrderhistorypendingRoutingModule() {
    }
    OrderhistorypendingRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OrderhistorypendingRoutingModule);
    return OrderhistorypendingRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistorypending/orderhistorypending.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistorypending/orderhistorypending.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL29yZGVyaGlzdG9yeXBlbmRpbmcvb3JkZXJoaXN0b3J5cGVuZGluZy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/modules/orderhistorypending/orderhistorypending.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistorypending/orderhistorypending.component.ts ***!
  \*************************************************************************************/
/*! exports provided: OrderhistorypendingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistorypendingComponent", function() { return OrderhistorypendingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var OrderhistorypendingComponent = /** @class */ (function () {
    function OrderhistorypendingComponent(orderhistoryService, router) {
        this.orderhistoryService = orderhistoryService;
        this.router = router;
        this.Orderhistorypending = [];
        this.tableFormat = {
            title: 'Order History Pending Detail Page',
            label_headers: [
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'Order Date', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'Expired Date', visible: true, type: 'string', data_row_name: 'payment_expire_date' },
                { label: 'Merchant Name', visible: true, type: 'string', data_row_name: 'merchant_name' },
                { label: 'User Id', visible: true, type: 'string', data_row_name: 'user_id' },
                // unfinished
                { label: 'User Name', visible: true, type: 'string', data_row_name: 'user_name' },
                // unfinished
                { label: 'User Email', visible: true, type: 'string', data_row_name: 'email' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                // unfinished
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' }
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'Orderhistorypending',
                detail_function: [this, 'callDetail']
            },
            show_checkbox_options: true
        };
        this.form_input = {};
        this.errorLabel = false;
        this.orderhistorypendingDetail = false;
    }
    // ngOnInit() {
    //   this.orderhistoryService.getOrderhistory().subscribe(data=>{
    //     let result: any = data;
    //     console.log(result.result);
    //     this.Orderhistory = result.result;
    //     //display and convert shoppingcart_id in foreach because type data is ObjectID
    //     this.Orderhistory.forEach((data, index)=>{this.Orderhistory[index].shoppingcart_id = this.Orderhistory[index].shoppingcart_id.$oid})
    //   });
    // }
    OrderhistorypendingComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderhistorypendingComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.orderhistoryService;
                        return [4 /*yield*/, this.orderhistoryService.getOrderhistorypendingLint()];
                    case 1:
                        result = _a.sent();
                        this.totalPage = result.result.total_page;
                        console.log(result);
                        this.Orderhistorypending = result.result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderhistorypendingComponent.prototype.callDetail = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log(orderhistory_id);
                this.router.navigate(['administrator/orderhistoryallhistoryadmin/edit'], { queryParams: { id: orderhistory_id } });
                return [2 /*return*/];
            });
        });
    };
    OrderhistorypendingComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.orderhistorypendingDetail = false;
                return [2 /*return*/];
            });
        });
    };
    OrderhistorypendingComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    OrderhistorypendingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-orderhistorypending',
            template: __webpack_require__(/*! raw-loader!./orderhistorypending.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistorypending/orderhistorypending.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./orderhistorypending.component.scss */ "./src/app/layout/modules/orderhistorypending/orderhistorypending.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], OrderhistorypendingComponent);
    return OrderhistorypendingComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistorypending/orderhistorypending.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistorypending/orderhistorypending.module.ts ***!
  \**********************************************************************************/
/*! exports provided: OrderhistorypendingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistorypendingModule", function() { return OrderhistorypendingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _orderhistorypending_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orderhistorypending-routing.module */ "./src/app/layout/modules/orderhistorypending/orderhistorypending-routing.module.ts");
/* harmony import */ var _orderhistorypending_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./orderhistorypending.component */ "./src/app/layout/modules/orderhistorypending/orderhistorypending.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _detail_orderhistorypending_detail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./detail/orderhistorypending.detail.component */ "./src/app/layout/modules/orderhistorypending/detail/orderhistorypending.detail.component.ts");
/* harmony import */ var _edit_orderhistorypending_edit_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./edit/orderhistorypending.edit.component */ "./src/app/layout/modules/orderhistorypending/edit/orderhistorypending.edit.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var OrderhistorypendingModule = /** @class */ (function () {
    function OrderhistorypendingModule() {
    }
    OrderhistorypendingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _orderhistorypending_routing_module__WEBPACK_IMPORTED_MODULE_2__["OrderhistorypendingRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"]
            ],
            declarations: [_orderhistorypending_component__WEBPACK_IMPORTED_MODULE_3__["OrderhistorypendingComponent"], _detail_orderhistorypending_detail_component__WEBPACK_IMPORTED_MODULE_9__["OrderhistorypendingDetailComponent"], _edit_orderhistorypending_edit_component__WEBPACK_IMPORTED_MODULE_10__["OrderhistorypendingEditComponent"]]
        })
    ], OrderhistorypendingModule);
    return OrderhistorypendingModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-orderhistorypending-orderhistorypending-module.js.map