(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6"],{

/***/ "./src/app/services/product/product.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/product/product.service.ts ***!
  \*****************************************************/
/*! exports provided: ProductService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductService", function() { return ProductService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _master_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../master.service */ "./src/app/services/master.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ProductService = /** @class */ (function () {
    function ProductService(http, myService) {
        this.http = http;
        this.myService = myService;
        this.api_url = '';
        this.httpReq = false;
        this.serviceUrl = 'https://jsonplaceholder.typicode.com/users';
        this.api_url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])();
    }
    ProductService.prototype.detailProducts = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'product/detail/' + _id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        console.log("product detail error", error_1);
                        return [2 /*return*/, error_1];
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.getProductsReport = function (_params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, params, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (_params) {
                            params = _params;
                        }
                        else {
                            params = {
                                search: {
                                    type: {
                                        in: ["product", "voucher", "gold", "top-up"]
                                    }
                                    // active : '1'
                                },
                                limit_per_page: 50,
                            };
                        }
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'product/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log("API", params, url);
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.getEvoucherReport = function (_params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, params, getDetail, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (_params) {
                            params = _params;
                        }
                        else {
                            params = {
                                search: {
                                    type: 'e-voucher'
                                },
                                limit_per_page: 50,
                            };
                        }
                        getDetail = {
                            get_detail: true,
                        };
                        Object.assign(params.search, getDetail);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'product/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.getCategoryLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'category/list';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.searchProductsLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, getDetail, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("wadidwa", params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/report';
                        getDetail = {
                            get_detail: true,
                        };
                        Object.assign(params.search, getDetail);
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        // console.log('params', params)
                        result = _a.sent();
                        console.log("llll", result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.searchProductsReportLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'product/all/report';
                        // params.type = 'evoucher';
                        // let getDetail = {
                        //   get_detail: true,
                        // }
                        Object.assign(params.search);
                        params.search.type = {
                            in: ["product", "voucher", "gold", "top-up"]
                        };
                        console.log('params', params);
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.searchVouchersReportLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, getDetail, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'product/all/report';
                        getDetail = {
                            get_detail: true,
                        };
                        Object.assign(params.search, getDetail);
                        params.search.type = 'e-voucher';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        // console.log('params', params)
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // public async searchEvouchersLint(params){
    //   let result;
    //   const customHeaders = getTokenHeader();
    //   try {
    //     const url   = 'products/all';
    //     console.log("isi dr evoucher code", url)
    //     params.type = 'e_voucher';
    //     result  = await this.myService.searchResult(params, url, customHeaders);
    //   } catch (error) {
    //     throw new TypeError(error);
    //   }
    //     return  result;
    // }
    ProductService.prototype.addProductsLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/add';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        console.log("ERROR , error", error_8.message);
                        throw new TypeError(error_8.message);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.addProducts = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("params", params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'product/add';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_9 = _a.sent();
                        console.log("ERROR , error", error_9.message);
                        throw new TypeError(error_9.message);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.generateProductsLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'generate';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_10 = _a.sent();
                        throw new TypeError(error_10);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.updateProductData = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'product/update';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_11 = _a.sent();
                        throw new TypeError(error_11);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.updateProduct = function (params, _id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/' + _id;
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_12 = _a.sent();
                        throw new TypeError(error_12);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.configGroup = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'configs/keyname/merchant-group-configuration';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // con  ole.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_13 = _a.sent();
                        throw new TypeError(error_13);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // public async redeemGroup() {
    //   let result;
    //   const customHeaders = getTokenHeader();
    //   try {
    //     let url = 'configs/keyname/products-configuration';
    //     result = await this.myService.get(null, url, customHeaders);
    //   } catch (error) {
    //     throw new TypeError(error);
    //   }
    //   return result;
    // }
    ProductService.prototype.updateEvoucher = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/' + params.previous_product_code;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_14 = _a.sent();
                        throw new TypeError(error_14);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.deleteProduct = function (product_code) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'product/delete/' + product_code;
                        return [4 /*yield*/, this.myService.delete(null, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_15 = _a.sent();
                        throw new TypeError(error_15);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.getDropdown = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_16;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/all';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_16 = _a.sent();
                        throw new TypeError(error_16);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.configuration = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_17;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'configs/keyname/products-configuration';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_17 = _a.sent();
                        throw new TypeError(error_17);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.getProductActivityReport = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_18;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/report/product-activity-summary';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_18 = _a.sent();
                        throw new TypeError(error_18);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.getMerchantList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_19;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant_list';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_19 = _a.sent();
                        throw new TypeError(error_19);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.approvedProducts = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_20;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/approve';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_20 = _a.sent();
                        throw new TypeError(error_20);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.statusEdit = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_21;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/set_status';
                        return [4 /*yield*/, this.myService.post(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_21 = _a.sent();
                        throw new TypeError(error_21);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.packageBundle = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_22;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/create_bundle';
                        return [4 /*yield*/, this.myService.post(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_22 = _a.sent();
                        throw new TypeError(error_22);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    ProductService.prototype.uploadFile = function (file, obj, fileType, funcOnFinish) {
        return __awaiter(this, void 0, void 0, function () {
            var fd, url, myToken;
            var _this = this;
            return __generator(this, function (_a) {
                fd = new FormData();
                fd.append('image', file, file.name);
                if (fileType == 'product') {
                    fd.append('type', 'product');
                }
                else if (fileType == 'image') {
                    fd.append('type', 'image');
                }
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'evoucher/upload';
                myToken = localStorage.getItem('tokenlogin');
                this.httpReq = this.http.post(url, fd, {
                    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                        'Authorization': myToken,
                    }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) {
                    // console.log(event);
                    if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                        var prgval = Math.round(event.loaded / event.total * 100);
                        // obj.updateProgressBar(prgval);
                        // console.log(obj.cancel);
                        if (obj.cancel == true) {
                            _this.httpReq.unsubscribe();
                        }
                        console.log('Upload Progress: ' + prgval + "%", event);
                    }
                    if (event["body"] != undefined) {
                        console.log("EVENT STATUS body", event['body']);
                        funcOnFinish(event['body'].result);
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Success!', 'Upload Success', 'success');
                    }
                }, function (result) {
                    console.log("ERROR result", result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Error!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    ProductService.prototype.upload = function (file, obj, fileType, funcOnFinish, newFileName) {
        if (newFileName === void 0) { newFileName = false; }
        return __awaiter(this, void 0, void 0, function () {
            var fd, endPoint, url, myToken;
            var _this = this;
            return __generator(this, function (_a) {
                fd = new FormData();
                if (newFileName) {
                    if (_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].production == false)
                        newFileName = 'DEV-' + newFileName;
                    if (fileType == 'image') {
                        fd.append('image', file, newFileName);
                    }
                    else {
                        fd.append('document', file, newFileName);
                    }
                }
                else {
                    if (fileType == 'image') {
                        fd.append('image', file, newFileName);
                    }
                    else {
                        fd.append('document', file, newFileName);
                    }
                }
                endPoint = 'media/upload/image';
                // if(fileType == 'product'){
                //   fd.append('type', 'product');
                // } else if(fileType == 'image') {
                //   fd.append('type', 'image');
                // } else if(fileType == 'document'){
                //   fd.append('type', 'document');
                // }
                fd.append('type', fileType);
                if (fileType == 'document')
                    endPoint = 'media/upload/document';
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + endPoint;
                myToken = localStorage.getItem('tokenlogin');
                this.httpReq = this.http.post(url, fd, {
                    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                        'Authorization': myToken,
                        'app-label': 'general_dt',
                    }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) {
                    // console.log(event);
                    if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                        var prgval = Math.round(event.loaded / event.total * 100);
                        // obj.updateProgressBar(prgval);
                        // console.log(obj.cancel);
                        if (obj.cancel == true) {
                            _this.httpReq.unsubscribe();
                        }
                        console.log('Upload Progress: ' + prgval + "%", event);
                    }
                    if (event["body"] != undefined) {
                        console.log("EVENT STATUS body", event['body']);
                        funcOnFinish(event['body']);
                    }
                }, function (result) {
                    console.log("ERROR result", result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Error!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    ProductService.prototype.addProductBulk = function (file, obj, payload) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'product/upload/bulk';
                console.log('now', fd);
                this.httpReq = this.http.post(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                    // headers : new HttpHeaders({
                    //   'Authorization': myToken,
                    //   'app-label': 'retailer_prg'
                    // }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        console.log("event", event);
                        console.log(event);
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            console.log('this is body');
                            console.warn(" obj", obj);
                            // obj.firstLoad();
                            obj.Report = c.body;
                            //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
                            obj.process_id = c.body;
                            // console.log("process_id",obj.Report);
                            // obj.prodOnUpload = true;
                            // console.log(c.body.result.failed);
                            // obj.failed = c.body.result.failed.length;
                            if (c.status == 200) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Success!', 'Your file has been uploaded.', 'success');
                            }
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                                // Swal.fire(
                                //   'Success!',
                                //   'Your file has been uploaded.',
                                //   'success'
                                // )
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    //console.log(result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    ProductService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
        { type: _master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"] }
    ]; };
    ProductService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"]])
    ], ProductService);
    return ProductService;
}());



/***/ })

}]);
//# sourceMappingURL=default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6.js.map