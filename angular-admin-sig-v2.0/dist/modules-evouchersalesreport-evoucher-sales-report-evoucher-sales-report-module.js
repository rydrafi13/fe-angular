(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-evouchersalesreport-evoucher-sales-report-evoucher-sales-report-module"],{

/***/ "./src/app/services/observerable/permission-observer.ts":
/*!**************************************************************!*\
  !*** ./src/app/services/observerable/permission-observer.ts ***!
  \**************************************************************/
/*! exports provided: PermissionObserver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionObserver", function() { return PermissionObserver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PermissionObserver = /** @class */ (function () {
    function PermissionObserver() {
        this.currentPermission = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]({});
        this.castCurrentPermission = this.currentPermission.asObservable();
        this.api_url = '';
    }
    PermissionObserver.prototype.updateCurrentPermission = function (data) {
        this.currentPermission.next(data);
    };
    PermissionObserver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [])
    ], PermissionObserver);
    return PermissionObserver;
}());



/***/ })

}]);
//# sourceMappingURL=modules-evouchersalesreport-evoucher-sales-report-evoucher-sales-report-module.js.map