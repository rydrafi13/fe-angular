(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-delivery-process-delivery-process-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/delivery-process/delivery-process.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/delivery-process/delivery-process.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div [@routerTransition]> -->\r\n<ng-container *ngIf=\"!orderDetail && !shippingLabel && !errorMessage\">\r\n<div class=\"board\">\r\n  <div class=\"nav toolbar\">\r\n    <h3 id=\"h3\"><strong>Transaction List</strong></h3><br>\r\n    <div class=\"card\">\r\n      <div class=\"card-content\">\r\n        <label id=\"label-header\"><strong>Order Status </strong></label>\r\n        <hr>\r\n        <div class=\"order-status\">\r\n          <button *ngFor=\"let sw of swaper; let i = index;\" class=\"btn toolbar {{sw.val}}\" id=\"button-swapper\"\r\n            (click)=\"swapClick(i)\">\r\n            <span class=\"{{sw.icon}}\"></span>\r\n            <span class=\"status-name\"><span class=\"badge\"\r\n                *ngIf=\"sw.name == 'Incoming' && totalValuePending\">{{totalValuePending}}</span>{{sw.name}}</span>\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"card\">\r\n      <div class=\"card sticky-top\" id=\"card-transaction\">\r\n        <div class=\"div-transaction\">\r\n          <div id=\"transaction\">\r\n            <label id=\"label-header\"><strong>Transaction Filters</strong></label>\r\n          </div>\r\n          <div class=\"searchbar\">\r\n            <div id=\"search\">\r\n              <!-- <label>Filter by</label> -->\r\n              <select class=\"form-control\" [(ngModel)]=\"filter\">\r\n                <option *ngFor=\"let f of filterBy\" [ngValue]=\"f.value\">{{f.name}}</option>\r\n              </select>\r\n            </div>\r\n            <div id=\"search-field\">\r\n              <input id=\"search-text\" type=\"text\" class=\"form-control\" [(ngModel)]=\"searchTextModel\" (keyup)=\"searchText($event)\"\r\n                placeholder=\"search\">\r\n              <i class=\"fa fa-search\" id=\"search-icon\"></i>\r\n            </div>\r\n            <div class=\"date-picker-wrapper\">\r\n              <div class=\"date-picker-overlay\" *ngIf=\"toggler && toggler['approve_date']\">\r\n                <div class=\"dpicker\">\r\n                  <ngb-datepicker #d (select)=\"datePickerOnDateSelection($event, 'approve_date')\" [displayMonths]=\"1\"\r\n                    [dayTemplate]=\"t\" outsideDays=\"hidden\"></ngb-datepicker>\r\n                  <div style=\"float: right;justify-self: right;\">\r\n                    <a class=\"btn ok\" (click)=\"dateToggler('approve_date')\">OK</a>\r\n                    <a class=\"btn clear\" (click)=\"dateClear($event, 'approve_date')\"> <span class=\"fa fa-trash\"></span>\r\n                      clear</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <ng-template #t let-date=\"date\" let-focused=\"focused\">\r\n                <span class=\"custom-day\" [class.focused]=\"focused\"\r\n                  [class.range]=\"datePickerOnDateIsRange(date, 'approve_date')\">\r\n                  {{ date.day }}\r\n                </span>\r\n              </ng-template>\r\n              <em\r\n                *ngIf=\"typeof(form_input['approve_date']) != 'object' || (typeof(form_input['approve_date']) == 'object' && !form_input['approve_date'].from) || toggler['approve_date']\">\r\n                <input class=\"form-control\" style=\"padding-right: 22px;\" type=\"text\" [(ngModel)]=\"form_input['date']\"\r\n                  placeholder=\"search date here\" (click)=\"dateToggler('approve_date')\">\r\n                <i class=\"fa fa-calendar-alt\" id=\"calendar-icon\"></i>\r\n              </em>\r\n              <a class=\"btn form-control\" style=\"border: 1px solid #ced4da\"\r\n                *ngIf=\"typeof(form_input['approve_date']) == 'object' && form_input['approve_date'].from && !toggler['approve_date'] \"\r\n                (click)=\"dateToggler('approve_date')\">\r\n                <span *ngIf=\"form_input['approve_date'].from\"> {{form_input['approve_date'].from}} </span>\r\n                <span *ngIf=\"form_input['approve_date'].to\"> - {{form_input['approve_date'].to}} </span>\r\n                <i class=\"fa fa-calendar-alt\"></i>\r\n\r\n              </a>\r\n            </div>\r\n            <div id=\"search\">\r\n              <!-- <label>Sort by</label> -->\r\n              <select class=\"form-control\" [(ngModel)]=\"orderBy\" (change)=\"orderBySelected()\">\r\n                <option *ngFor=\"let s of sortBy\" [ngValue]=\"s.value\">{{s.name}}</option>\r\n              </select>\r\n            </div>\r\n            <button (click)=\"clickToDownloadReport()\" class=\"btn save btn-primary\" id=\"btn-download\">Download\r\n              Report</button>\r\n            <button (click)=\"addToPackingList()\" class=\"btn save btn-success\" id=\"btn-print\" *ngIf=\"swaper[3].val == true\">Add to Packing List</button>\r\n            <button (click)=\"printLabel()\" class=\"btn save btn-success\" id=\"btn-print\" *ngIf=\"swaper[2].val == true || swaper[3].val == true\">Print Label</button>\r\n            <button (click)=\"deletePrintLabel()\" class=\"btn save btn-success\" id=\"btn-print\" *ngIf=\"swaper[2].val == true || swaper[3].val == true\">Delete Print Marked</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"select-all\" *ngIf=\"allData && allData.length != 0\">\r\n        <label class=\"header-text\" style=\"float: right;margin-right: 35px;\">\r\n          <strong>Select All\r\n            <mat-checkbox (change)=\"checkedAll()\" [(ngModel)]=\"activeCheckbox\" type=\"checkbox\"\r\n              style=\"vertical-align: -webkit-baseline-middle;margin-left: 10px;\">\r\n            </mat-checkbox>\r\n          </strong>\r\n        </label>\r\n      </div>\r\n      <div class=\"loading\" *ngIf='showLoading'>\r\n        <div class=\"sk-fading-circle\">\r\n          <div class=\"sk-circle1 sk-circle\"></div>\r\n          <div class=\"sk-circle2 sk-circle\"></div>\r\n          <div class=\"sk-circle3 sk-circle\"></div>\r\n          <div class=\"sk-circle4 sk-circle\"></div>\r\n          <div class=\"sk-circle5 sk-circle\"></div>\r\n          <div class=\"sk-circle6 sk-circle\"></div>\r\n          <div class=\"sk-circle7 sk-circle\"></div>\r\n          <div class=\"sk-circle8 sk-circle\"></div>\r\n          <div class=\"sk-circle9 sk-circle\"></div>\r\n          <div class=\"sk-circle10 sk-circle\"></div>\r\n          <div class=\"sk-circle11 sk-circle\"></div>\r\n          <div class=\"sk-circle12 sk-circle\"></div>\r\n        </div>\r\n      </div>\r\n      <div class=\"div-content-row\" *ngFor=\"let data of allData; let i = index\">\r\n        <div class=\"div-content-cell div-product\">\r\n          <div *ngIf=\"allData[i].product_status == true; else equalToOne\">\r\n            <!-- <div *ngIf=\"allData[i].products.length > 1; else equalToOne\"> -->\r\n            <div class=\"more-product\">\r\n              <div class=\"loop-product\" *ngFor=\"let product of allData[i].product_list;\">\r\n                <!-- <div class=\"card\"> -->\r\n                  <div *ngIf=\"product && product.images_gallery && product.images_gallery[0]\" class=\"img-wrapper\" >\r\n                    <img src=\"{{product.images_gallery[0].base_url+product.images_gallery[0].pic_small_path}}\"\r\n                    class=\"image\">\r\n                  </div>\r\n                  <div *ngIf=\"!(product && product.images_gallery && product.images_gallery[0])\" class=\"img-wrapper\" >\r\n                    <img src=\"https://res.cloudinary.com/pt-cls-system/image/upload/v1632905858/admin/image/no%20image.jpg61542a81617a2.jpg\"\r\n                    class=\"image\">\r\n                  </div>\r\n                <!-- </div> -->\r\n                <div>\r\n                  <div class=\"form-label\">\r\n                    <label class=\"see-more-header-text order-id-text\">Reference No : <span>{{data.reference_no}}</span></label>\r\n                    <label class=\"see-more-normal-text date-text\">Tanggal proses : {{data.created_date}}</label>\r\n                    <label *ngIf=\"data.shipping_info && data.shipping_info.length > 0\" class=\"see-more-normal-text date-text shipping-info\">Last Shipping Info : {{data.shipping_info[data.shipping_info.length - 1].title}}</label>\r\n                    <label [ngClass]=\"data.status == 'CANCEL' ? 'red-return' : 'green-return'\">{{data.return_detail && data.return_detail.awb_number ? data.status == \"CANCEL\" ? \"REORDER / CANCEL\" : \"REDELIVER\" : \"\"}}</label>\r\n                  </div>\r\n                  <div class=\"see-more-form-label\">\r\n                    <label\r\n                      class=\"product-name\"><strong>{{product.product_name}}</strong></label>\r\n                  </div>\r\n                  <label class=\"price-see-more\"><strong>{{product.total_product_price | number}} Points</strong></label><br />\r\n                  <label class=\"see-more-header-text\" style=\"margin-bottom: 0.5rem;\">\r\n                    <strong>Qty : {{product.quantity}}</strong>\r\n                  </label>\r\n                  <!-- <label class=\"header-text\"><strong>Notes:</strong></label>\r\n                  <p class=\"normal-text\" style=\"word-break: break-all;\">\r\n                    {{product.note_to_merchant}}</p> -->\r\n                </div>\r\n              </div>\r\n              <div class=\"printed-stamp\" *ngIf=\"allData[i].additional_info && allData[i].additional_info.label_printed == 'ya'\">\r\n                <span>printed</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <ng-template #equalToOne>\r\n            <div class=\"div-default-product\">\r\n              <div class=\"default-product\">\r\n                <div *ngIf=\"allData[i].product_list && allData[i].product_list[0] && allData[i].product_list[0].images_gallery && allData[i].product_list[0].images_gallery[0]\" class=\"img-wrapper\"\r\n                  style=\"background-image: url({{allData[i].product_list[0].images_gallery[0].base_url+allData[i].product_list[0].images_gallery[0].pic_small_path}});\">\r\n                  <img src=\"{{allData[i].product_list[0].images_gallery[0].base_url+allData[i].product_list[0].images_gallery[0].pic_small_path}}\"\r\n                    class=\"image\">\r\n                </div>\r\n                <div *ngIf=\"!(allData[i].product_list && allData[i].product_list[0] && allData[i].product_list[0].images_gallery && allData[i].product_list[0].images_gallery[0])\" class=\"img-wrapper\"\r\n                  >\r\n                  <img src=\"https://res.cloudinary.com/pt-cls-system/image/upload/v1632905858/admin/image/no%20image.jpg61542a81617a2.jpg\"\r\n                    class=\"image\">\r\n                </div>\r\n                <div class=\"product-detail\">\r\n                  <div class=\"form-label\">\r\n                    <label class=\"header-text order-id-text\">Reference No : <span>{{data.reference_no}}</span></label>\r\n                    <label class=\"normal-text date-text\">Tanggal proses : {{data.created_date}}</label>\r\n                    <label *ngIf=\"data.shipping_info && data.shipping_info.length > 0\" class=\"see-more-normal-text date-text shipping-info\">Last Shipping Info : {{data.shipping_info[data.shipping_info.length - 1].title}}</label>\r\n                    <label [ngClass]=\"data.status == 'CANCEL' ? 'red-return' : 'green-return'\">{{data.return_detail && data.return_detail.awb_number ? data.status == \"CANCEL\" ? \"REORDER / CANCEL\" : \"REDELIVER\" : \"\"}}</label>\r\n                  </div>\r\n                  <div class=\"form-label\">\r\n                    <label class=\"header-text\"><strong>{{allData[i].product_list[0].product_name}}</strong></label>\r\n                  </div>\r\n                  <!-- <div class=\"form-label\">\r\n                    <label\r\n                      class=\"price\"><strong>{{allData[i].products[0].total_product_price | number}} Points</strong></label>\r\n                  </div> -->\r\n                  <!-- <label class=\"header-text\" style=\"margin-bottom: 0.5rem;\"><strong>Quantity :\r\n                      {{allData[i].products[0].quantity}}</strong></label><br />\r\n                  <div *ngIf=\"allData[i].products[0].note_to_merchant\">\r\n                    <label class=\"header-text\"><strong>Notes:</strong></label>\r\n                    <p class=\"normal-text\" style=\"word-break: break-all;\">\r\n                      {{allData[i].products[0].note_to_merchant}}</p>\r\n                  </div> -->\r\n\r\n                </div>\r\n                <div class=\"printed-stamp\" *ngIf=\"allData[i].additional_info && allData[i].additional_info.label_printed == 'ya' && swaper[4].val != true\">\r\n                  <span>printed</span>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </ng-template>\r\n          <div *ngIf=\"allData[i].product_list.length > 1 && allData[i].product_status != true;\" style=\"text-align: right;\" class=\"see-more-container\">\r\n            <label class=\"see-more\" (click)=\"setSeeMore(i)\" >See {{allData[i].product_list.length-1}}\r\n              more...</label>\r\n          </div>\r\n          <div *ngIf=\"allData[i].product_list.length > 1 && allData[i].product_status == true;\" style=\"text-align: right;\" class=\"see-more-container\">\r\n            <label class=\"see-more\" (click)=\"setSeeLess(i)\" >See less</label>\r\n          </div>\r\n        </div>\r\n        <div class=\"div-content-cell \">\r\n          <div class=\"form-label\">\r\n            <label class=\"header-text\">Shipping Address</label>\r\n            <label class=\"normal-text\">{{data.destination.address}}</label>\r\n          </div>\r\n          <!-- <div class=\"form-label\" *ngIf=\"data.delivery_detail && data.delivery_detail.awb_number\">\r\n            <label class=\"header-text\">AWB Number</label>\r\n            <label class=\"normal-text\">{{data.delivery_detail.awb_number}}</label>\r\n          </div> -->\r\n          <div class=\"form-label\" *ngIf=\"data.delivery_detail && data.delivery_detail.in_packing_list == true && data.delivery_detail.packing_no\">\r\n            <label class=\"header-text\">In Packing List : </label>\r\n            <label class=\"normal-text\">{{data.delivery_detail.packing_no}}</label>\r\n          </div>\r\n          <div class=\"form-label\" *ngIf=\"data.type\">\r\n            <label class=\"header-text\">Delivery Service</label>\r\n            <label class=\"normal-text\" *ngIf=\"data.type == 'product'\">{{data.buyer_detail.delivery_request.service}} /\r\n              {{data.buyer_detail.delivery_request.description}}</label>\r\n            <label *ngIf=\"type(i) == 'evoucher'\">no-services</label>\r\n          </div>\r\n\r\n          <div class=\"form-label\" *ngIf=\"data.delivery_courier\">\r\n            <label class=\"header-text\">Courier</label>\r\n            <label class=\"normal-text\">{{data.delivery_courier}}</label>\r\n          </div>\r\n        </div>\r\n        <div class=\"div-content-cell products-left\">\r\n          <div class=\"products-left-board\">\r\n            <div class=\"products-left-data\">\r\n              <label class=\"header-text\">Receiver</label>\r\n              <label class=\"normal-text\">{{data.destination.name}}</label>\r\n              <label class=\"header-text\">Phone Number</label>\r\n              <label class=\"normal-text\">{{data.destination.cell_phone}}</label>\r\n              <label class=\"header-text\">Total ({{data.total_item_qty}} Items)</label>\r\n              <label class=\"price-total\"><strong>{{data.total_item_value | number }} Points</strong></label>\r\n            </div>\r\n            <div class=\"products-left-checkbox\">\r\n              <mat-checkbox (change)='onChecked()' [(ngModel)]=\"checkBox[i]\" type=\"checkbox\"></mat-checkbox>\r\n            </div>\r\n          </div>\r\n          <div class=\"products-left-status\">\r\n            <span class=\"payment-status {{data.payment_status}}\" *ngIf=\"data.payment_status == 'PAID'\">Paid</span>\r\n            <span class=\"payment-status {{data.payment_status}}\" *ngIf=\"data.payment_status == 'CANCEL'\">Cancel</span>\r\n            <label class=\"see-more\" (click)=\"getDetail(data)\">More Details &raquo;</label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"div-footer\" *ngIf=\"allData && allData.length!=0\">\r\n        <div>\r\n          <button class=\"btn\" (click)=\"onChangePage(currentPage = 1)\">\r\n            &laquo; </button> <button class=\"btn\" (click)=\"onChangePage(currentPage - 1)\">\r\n            &lsaquo; </button> <button *ngFor=\"let tp of pageNumbering\" class=\"btn\" (click)=\"onChangePage(tp)\">{{tp}}\r\n          </button>\r\n          <button class=\"btn\" (click)=\"onChangePage(currentPage + 1)\">&rsaquo;</button>\r\n          <button class=\"btn\" (click)=\"onChangePage(currentPage = total_page)\">&raquo;</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n<table style=\"display: none;\" id=\"reportTable\" #reportTable>\r\n  <thead>\r\n    <tr>\r\n      <th>NO.</th>\r\n      <th>ID Pelanggan</th>\r\n      <th>ID Bisnis</th>\r\n      <th>Order ID</th>\r\n      <th>Reference No.</th>\r\n      <th>Nomor PO</th>\r\n      <!-- <th>ID Member</th> -->\r\n      <th>Nama Member</th>\r\n      <th>Order Date</th>\r\n      <th>Delivery Date</th>\r\n      <th>Product Name</th>\r\n      <th>Product Price</th>\r\n      <th>Quantity</th>\r\n      <!-- <th>Notes</th> -->\r\n      <th>Shipping Address</th>\r\n      <th>Support SAP</th>\r\n      <th>Shipping Status</th>\r\n      <th>No. Resi</th>\r\n      <th>Service</th>\r\n      <th>Courier</th>\r\n      <th *ngIf=\"mci_project == false\">Receiver</th>\r\n      <th>Phone Number</th>\r\n      <th>Total</th>\r\n      <!-- <th>Order Status</th> -->\r\n      <th>Tanggal Delivered</th>\r\n      <th>Nama Penerima</th>\r\n      <th>Status Penerima</th>\r\n      <th>Remarks</th>\r\n    </tr>\r\n  </thead>\r\n\r\n  <tbody>\r\n    <tr *ngFor=\"let data of allDataToDownload; let i = index\">\r\n      <td>{{i+1}}</td>\r\n      <td>{{data.id_pel}}</td>\r\n      <td *ngIf=\"data.business_id\">{{data.business_id}}</td>\r\n      <td *ngIf=\"!data.business_id\">-</td>\r\n      <td>{{data.order_id}}</td>\r\n      <td>{{data.reference_no}}</td>\r\n      <td *ngIf=\"data.po_no\">{{data.po_no.value}}</td>\r\n      <td *ngIf=\"!data.po_no\">-</td>\r\n      <!-- <td>{{data.username}}</td> -->\r\n      <td *ngIf=\"data.destination\">{{data.destination.name}}</td>\r\n      <td>{{data.created_date}}</td>\r\n      <td *ngIf=\"data.shipping_info && data.shipping_info[1]\">{{data.shipping_info[1].created_date}}</td>\r\n      <td *ngIf=\"!data.shipping_info || data.shipping_info && !data.shipping_info[1]\">-</td>\r\n      <td>\r\n        <div *ngFor=\"let product of allDataToDownload[i].product_list;\">\r\n          <span>{{product.product_name}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of allDataToDownload[i].product_list;\">\r\n          <span>Rp. {{product.total_product_price}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of allDataToDownload[i].product_list;\">\r\n          <span>{{product.quantity}}</span>\r\n        </div>\r\n      </td>\r\n      <td>{{data.destination.address}}, {{data.destination.village_name}}, {{data.destination.district_name}}, {{data.destination.city_name}}, {{data.destination.province_name}}, {{data.destination.postal_code}}</td>\r\n      <!-- <ng-container *ngIf=\"data.member_detail.alamat_rumah && data.member_detail.kelurahan_rumah && data.member_detail.kecamatan_rumah && data.member_detail.kota_rumah && data.member_detail.provinsi_rumah && data.member_detail.kode_pos_rumah &&\r\n                           data.member_detail.alamat_rumah != '' && data.m  ember_detail.kelurahan_rumah != '' && data.member_detail.kecamatan_rumah != '' && data.member_detail.kota_rumah != '' && data.member_detail.provinsi_rumah != '' && data.member_detail.kode_pos_rumah != '' &&\r\n                           data.member_detail.alamat_rumah != '-' && data.member_detail.kelurahan_rumah != '-' && data.member_detail.kecamatan_rumah != '-' && data.member_detail.kota_rumah != '-' && data.member_detail.provinsi_rumah != '-' && data.member_detail.kode_pos_rumah != '-'; else unsupportedcourier\">\r\n        <td>YA</td>\r\n      </ng-container> -->\r\n      <ng-container *ngIf=\"data && data.supported == 1 ; else unsupportedcourier\">\r\n        <td>YA</td>\r\n      </ng-container>\r\n      <td>{{data.last_shipping_info}}</td>\r\n      <td *ngIf=\"data.awb_number\">{{data.awb_number}}</td>\r\n      <td *ngIf=\"!data.awb_number\">-</td>\r\n      <!-- <td >Empty</td> -->\r\n      <td *ngIf=\"data.delivery_service\">{{data.delivery_service}}</td>\r\n      <td *ngIf=\"!data.delivery_service\">-</td>\r\n      <td *ngIf=\"data.courier\">{{data.courier}}</td>\r\n      <td *ngIf=\"!data.courier\">-</td>\r\n      <td *ngIf=\"mci_project == false\">{{data.destination.name}}</td>\r\n      <td>{{data.destination.cell_phone}}</td>\r\n      <td>{{data.total_item_value}}</td>\r\n      <!-- <td>{{data.status}}</td> -->\r\n      <td *ngIf=\"data.shipping_info && data.shipping_info[2]\">{{data.shipping_info[2].delivered_date}}</td>\r\n      <td *ngIf=\"!data.shipping_info || !data.shipping_info[2]\">-</td>\r\n      <td *ngIf=\"data.shipping_info && data.shipping_info[2]\">{{data.shipping_info[2].receiver_name}}</td>\r\n      <td *ngIf=\"!data.shipping_info || !data.shipping_info[2]\">-</td>\r\n      <td *ngIf=\"data.shipping_info && data.shipping_info[2]\">{{data.shipping_info[2].delivery_status}}</td>\r\n      <td *ngIf=\"!data.shipping_info || !data.shipping_info[2]\">-</td>\r\n      <td *ngIf=\"data.shipping_info && data.shipping_info[2]\">{{data.shipping_info[2].remarks}}</td>\r\n      <td *ngIf=\"!data.shipping_info || !data.shipping_info[2]\">-</td>\r\n    </tr>\r\n  </tbody>\r\n\r\n\r\n\r\n  <!-- All Order -->\r\n  <!-- <tbody *ngIf=\"swaper[0].val == true\">\r\n    <tr *ngFor=\"let data of allDataToDownload; let i = index\">\r\n      <td>{{i+1}}</td>\r\n      <td>{{data.order_id}}</td>\r\n      <td>{{data.approve_date}}</td>\r\n      <td *ngIf=\"data.shipping_info[1]\">{{data.shipping_info[1].created_date}}</td>\r\n      <td *ngIf=\"!data.shipping_info[1]\">-</td>\r\n      <td>\r\n        <div *ngFor=\"let product of allDataToDownload[i].products;\">\r\n          <span>{{product.product_name}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of allDataToDownload[i].products;\">\r\n          <span>Rp. {{product.total_product_price}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of allDataToDownload[i].products;\">\r\n          <span>{{product.quantity}}</span>\r\n        </div>\r\n      </td>\r\n      <td>{{data.member_detail.alamat_rumah}}</td>\r\n      <td>{{data.last_shipping_info}}</td>\r\n      <td *ngIf=\"data.delivery_detail.awb_number\">{{data.delivery_detail.awb_number}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.awb_number\">-</td>\r\n      <td *ngIf=\"data.delivery_detail.delivery_service\">{{data.delivery_detail.delivery_service}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.delivery_service\">-</td>\r\n      <td *ngIf=\"data.delivery_detail.courier\">{{data.delivery_detail.courier}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.courier\">-</td>\r\n      <td>{{data.member_detail.nama_penerima_gopay}}</td>\r\n      <td>{{data.member_detail.no_wa_penerima}}</td>\r\n      <td>{{data.sum_total}}</td>\r\n      <td>{{data.status}}</td>\r\n      <td *ngIf=\"data.shipping_info[2]\">{{data.shipping_info[2].created_date}}</td>\r\n      <td *ngIf=\"!data.shipping_info[2]\">-</td>\r\n      <td *ngIf=\"data.shipping_info[2]\">{{data.shipping_info[2].receiver_name}}</td>\r\n      <td *ngIf=\"!data.shipping_info[2]\">-</td>\r\n      <td *ngIf=\"data.shipping_info[2]\">{{data.shipping_info[2].delivery_status}}</td>\r\n      <td *ngIf=\"!data.shipping_info[2]\">-</td>\r\n      <td *ngIf=\"data.shipping_info[2]\">{{data.shipping_info[2].remarks}}</td>\r\n      <td *ngIf=\"!data.shipping_info[2]\">-</td>\r\n    </tr>\r\n  </tbody> -->\r\n  <!-- Incoming Order -->\r\n  <!-- <tbody *ngIf=\"swaper[1].val == true\">\r\n    <tr *ngFor=\"let data of incomingToDownload; let i = index\">\r\n      <td>{{i+1}}</td>\r\n      <td>{{data.order_id}}</td>\r\n      <td>{{data.approve_date}}</td>\r\n      <td>-</td>\r\n      <td>\r\n        <div *ngFor=\"let product of incomingToDownload[i].products;\">\r\n          <span>{{product.product_name}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of incomingToDownload[i].products;\">\r\n          <span>Rp. {{product.total_product_price}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of incomingToDownload[i].products;\">\r\n          <span>{{product.quantity}}</span>\r\n        </div>\r\n      </td>\r\n      <td>{{data.member_detail.alamat_rumah}}</td>\r\n      <td>{{data.last_shipping_info}}</td>\r\n      <td *ngIf=\"data.delivery_detail.awb_number\">{{data.delivery_detail.awb_number}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.awb_number\">-</td>\r\n      <td *ngIf=\"data.delivery_detail.delivery_service\">{{data.delivery_detail.delivery_service}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.delivery_service\">-</td>\r\n      <td *ngIf=\"data.delivery_detail.courier\">{{data.delivery_detail.courier}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.courier\">-</td>\r\n      <td>{{data.member_detail.nama_penerima_gopay}}</td>\r\n      <td>{{data.member_detail.no_wa_penerima}}</td>\r\n      <td>{{data.sum_total}}</td>\r\n      <td>{{data.status}}</td>\r\n      <td>-</td>\r\n      <td>-</td>\r\n      <td>-</td>\r\n      <td>-</td>\r\n    </tr>\r\n  </tbody> -->\r\n  <!-- On Delivery Order -->\r\n  <!-- <tbody *ngIf=\"swaper[2].val == true\">\r\n    <tr *ngFor=\"let data of deliveryToDownload; let i = index\">\r\n      <td>{{i+1}}</td>\r\n      <td>{{data.order_id}}</td>\r\n      <td>{{data.approve_date}}</td>\r\n      <td *ngIf=\"data.shipping_info[1]\">{{data.shipping_info[1].created_date}}</td>\r\n      <td>\r\n        <div *ngFor=\"let product of deliveryToDownload[i].products;\">\r\n          <span>{{product.product_name}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of deliveryToDownload[i].products;\">\r\n          <span>Rp. {{product.total_product_price}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of deliveryToDownload[i].products;\">\r\n          <span>{{product.quantity}}</span>\r\n        </div>\r\n      </td>\r\n      <td>{{data.member_detail.alamat_rumah}}</td>\r\n      <td>{{data.last_shipping_info}}</td>\r\n      <td *ngIf=\"data.delivery_detail.awb_number\">{{data.delivery_detail.awb_number}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.awb_number\">-</td>\r\n      <td *ngIf=\"data.delivery_detail.delivery_service\">{{data.delivery_detail.delivery_service}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.delivery_service\">-</td>\r\n      <td *ngIf=\"data.delivery_detail.courier\">{{data.delivery_detail.courier}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.courier\">-</td>\r\n      <td>{{data.member_detail.nama_penerima_gopay}}</td>\r\n      <td>{{data.member_detail.no_wa_penerima}}</td>\r\n      <td>{{data.sum_total}}</td>\r\n      <td>{{data.status}}</td>\r\n      <td>-</td>\r\n      <td>-</td>\r\n      <td>-</td>\r\n      <td>-</td>\r\n    </tr>\r\n  </tbody> -->\r\n  <!-- Complete Order -->\r\n  <!-- <tbody *ngIf=\"swaper[3].val == true\">\r\n    <tr *ngFor=\"let data of completeToDownload; let i = index\">\r\n      <td>{{i+1}}</td>\r\n      <td>{{data.order_id}}</td>\r\n      <td>{{data.approve_date}}</td>\r\n      <td *ngIf=\"data.shipping_info[1]\">{{data.shipping_info[1].created_date}}</td>\r\n      <td>\r\n        <div *ngFor=\"let product of completeToDownload[i].products;\">\r\n          <span>{{product.product_name}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of completeToDownload[i].products;\">\r\n          <span>Rp. {{product.total_product_price}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of completeToDownload[i].products;\">\r\n          <span>{{product.quantity}}</span>\r\n        </div>\r\n      </td>\r\n      <td>{{data.member_detail.alamat_rumah}}</td>\r\n      <td>{{data.last_shipping_info}}</td>\r\n      <td *ngIf=\"data.delivery_detail.awb_number\">{{data.delivery_detail.awb_number}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.awb_number\">-</td>\r\n      <td *ngIf=\"data.delivery_detail.delivery_service\">{{data.delivery_detail.delivery_service}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.delivery_service\">-</td>\r\n      <td *ngIf=\"data.delivery_detail.courier\">{{data.delivery_detail.courier}}</td>\r\n      <td *ngIf=\"!data.delivery_detail.courier\">-</td>\r\n      <td>{{data.member_detail.nama_penerima_gopay}}</td>\r\n      <td>{{data.member_detail.no_wa_penerima}}</td>\r\n      <td>{{data.sum_total}}</td>\r\n      <td>{{data.status}}</td>\r\n      <td *ngIf=\"data.shipping_info[2]\">{{data.shipping_info[2].created_date}}</td>\r\n      <td *ngIf=\"data.shipping_info[2]\">{{data.shipping_info[2].receiver_name}}</td>\r\n      <td *ngIf=\"data.shipping_info[2]\">{{data.shipping_info[2].delivery_status}}</td>\r\n      <td *ngIf=\"data.shipping_info[2]\">{{data.shipping_info[2].remarks}}</td>\r\n    </tr>\r\n  </tbody> -->\r\n</table>\r\n</ng-container>\r\n\r\n<div *ngIf=\"errorMessage\" class=\"error-message\">\r\n  <div class=\"text-message\">\r\n    <i class=\"fas fa-ban\"></i>\r\n    {{errorMessage}}\r\n  </div>\r\n</div>\r\n\r\n<ng-container *ngIf=\"orderDetail\">\r\n  <!-- <app-page-header [heading]=\"'Order History All History'\" [icon]=\"'fa-table'\"></app-page-header> -->\r\n  <div class=\"board\">\r\n    <div class=\"nav toolbar\">\r\n      <app-delivery-process-shipping [back]=\"[this,backToHere]\" [detail]=\"orderDetail\" [propsDetail]=\"propsDetail\" class=\"parent-container\"></app-delivery-process-shipping>\r\n    </div>\r\n</div>\r\n</ng-container>\r\n\r\n<ng-container *ngIf=\"shippingLabel\">\r\n  <!-- <app-page-header [heading]=\"'Order History All History'\" [icon]=\"'fa-table'\"></app-page-header> -->\r\n  <app-shipping-label [back]=\"[this,backToHere]\" [detail]=\"shippingLabel\"></app-shipping-label>\r\n</ng-container>\r\n<!-- </div> -->\r\n\r\n<ng-template #unsupportedcourier>\r\n  <td></td>\r\n</ng-template>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/delivery-process/edit/delivery-process.edit.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/delivery-process/edit/delivery-process.edit.component.html ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- OPENMOREDETAIL == UNTUK DI ADMIN PORTAL -->\r\n<!-- EDITTHIS = UNTUK DI MERCHANT PORTAL -->\r\n\r\n<!-- Note :\r\n\r\ncurrentPermission untuk ADMIN itu untuk si admin\r\ncurrentPermission untuk MERCHANT itu untuk bagian merchant\r\nkarena kita memakai 1 page yang sama -->\r\n\r\n\r\n\r\n\r\n\r\n<div class=\"card card-detail\" *ngIf=\"!shippingDetail\">\r\n    <!-- <div class=\"card-header\" id=\"header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n    </div> -->\r\n    <div class=\"card-content\" *ngIf=\"deliveryHistoryDetail && orderDetail\">\r\n        <div class=\"orderhistoryallhistory-detail\">\r\n\r\n            <div class=\"row\">\r\n                <!-- <div *ngIf=\"errorLabel!==false\">\r\n                    {{errorLabel}}\r\n                </div> -->\r\n                <div class=\" col-md-8\">\r\n                    <div>\r\n\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <!-- <button class=\"btn btn-outline-primary btn-sm\" *ngIf=\"openMoreDetail\"\r\n                                    (click)=\"backToList()\">Back</button> -->\r\n                                <div class=\"form-group head-one\">\r\n                                    <label class=\"order-id\"><strong> <span style=\"color:#0ea8db; font-weight: bold;\">Order Id : </span>{{deliveryHistoryDetail[0].order_id}} | </strong>\r\n                                        <span class=\"buyer-name\" style=\"color:#0EA8DB\"><strong>\r\n                                                {{orderDetail.member_detail.id_pel}}</strong></span>\r\n                                        </label>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"form-group col-md-12\" style=\"font-size: 20px;\">\r\n                                        <label>Tanggal proses : <strong>{{deliveryHistoryDetail[0].created_date}}</strong></label>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <!-- ADMIN PART -->\r\n                                <!-- <div *ngIf=\"currentPermission == 'admin' && !openMoreDetail\"> -->\r\n                                    <!-- <div *ngFor=\"let deliveryHistoryDetail of deliveryHistoryDetail.order_list let i = index\"\r\n                                        class=\"card card-body mb-5 shadow rounded\"> -->\r\n                                        <div class=\"product-group\" *ngFor=\"let _deliveryHistoryDetail of deliveryHistoryDetail\">\r\n                                            <table class=\"product\">\r\n                                                <tr>\r\n                                                    <th colspan=\"5\">{{_deliveryHistoryDetail.delivery_type}}</th>\r\n                                                    <!-- <th colspan=\"3\" class=\"shipping-detail\" (click)=\"goShippingDetail(_deliveryHistoryDetail)\">Lihat Pengiriman ></th> -->\r\n                                                </tr>\r\n                                                <tr>\r\n                                                    <td colspan=\"5\" class=\"shipping-id\">Ref No. : {{_deliveryHistoryDetail.reference_no}} </td>\r\n                                                </tr>\r\n                                                <tr *ngIf=\"_deliveryHistoryDetail.shipping_info && isArray(_deliveryHistoryDetail.shipping_info) && _deliveryHistoryDetail.shipping_info.length > 0\">\r\n                                                    <td colspan=\"5\" class=\"shipping-status-row\">Status Pengiriman : {{_deliveryHistoryDetail.shipping_info.at(-1).title}}</td>\r\n                                                </tr>\r\n                                                <tr *ngIf=\"!(_deliveryHistoryDetail.shipping_info && isArray(_deliveryHistoryDetail.shipping_info) && _deliveryHistoryDetail.shipping_info.length > 0)\">\r\n                                                    <td colspan=\"5\" class=\"shipping-status-row\" style=\"padding-top:0;\"></td>\r\n                                                </tr>\r\n                                                <tr class=\"product-list\">\r\n                                                    <td class=\"product\">Product Name</td>\r\n                                                    <td>\r\n                                                        Varian\r\n                                                    </td>\r\n                                                    <td class=\"qty\">qty</td>\r\n                                                    <td class=\"ut\">unit price</td>\r\n                                                    <td class=\"price\">price</td>\r\n                                                </tr>\r\n                                                <tr *ngFor=\"let product of _deliveryHistoryDetail.product_list; let i = index\">\r\n                                                    <td>{{product.product_name}}\r\n                                                    </td>\r\n                                                    <td>\r\n                                                        {{product.variant && ((product.variant | json) != '{}') && ((product.variant | json) != '[]') ? replaceVarian(product.variant) : '-'}}\r\n                                                    </td>\r\n                                                    <td class=\"qty\">{{product.quantity | number}}x</td>\r\n                                                    <td class=\"qty\">{{product.sku_value | number}}</td>\r\n                                                    <td class=\"qty\">{{product.total_product_price | number}}\r\n                                                    </td>\r\n                                                </tr>\r\n                                                <tr style=\"border-top: 1.5px solid #eaeaea; width:80%;\">\r\n                                                    <td colspan=\"4\" class=\"priceTot\">Total Price</td>\r\n                                                    <td class=\"priceTotal\">\r\n                                                        {{_deliveryHistoryDetail.total_item_value | number}}\r\n                                                    </td>\r\n                                                </tr>\r\n                                            </table>\r\n                                        </div>\r\n                                        <!-- SHIPPING HISTORY -->\r\n                                        <!-- <div class=\"product-group\" *ngIf=\"deliveryHistoryDetail.status && deliveryHistoryDetail.status != 'CANCEL' \">\r\n                                            <span style=\"color:#0EA8DB\">Shipping History</span>\r\n                                            <table>\r\n                                                <td>\r\n                                                    <tr>\r\n                                                        \r\n\r\n                                                        <td>Delivery Service :\r\n                                                                <span *ngIf=\"deliveryHistoryDetail.status == 'PROCESSED'\">\r\n                                                                    <span *ngIf=\"deliveryHistoryDetail.delivery_detail.length == undefined\"\r\n                                                                    style=\"color:#0EA8DB; font-weight: bold;\">{{deliveryHistoryDetail.delivery_detail.courier}}</span>\r\n                                                                    <span *ngIf=\"deliveryHistoryDetail.delivery_detail.length != undefined\"\r\n                                                                    style=\"color:#0EA8DB; font-weight: bold;\">No Service</span>\r\n                                                                </span>\r\n                                                             \r\n                                                        </td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"deliveryHistoryDetail.delivery_detail.length == undefined\">\r\n                                                        <td>Courier : <span>{{deliveryHistoryDetail.delivery_detail.courier}}</span> </td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"deliveryHistoryDetail.delivery_detail.length != undefinedl\">\r\n                                                        <td>Courier : <span>No Service</span> </td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"deliveryHistoryDetail.courier && deliveryHistoryDetail.courier.supported == 1\">\r\n                                                        <td style=\"color:#0EA8DB\"><b>Courier Kerjasama </b></td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"deliveryHistoryDetail.courier && deliveryHistoryDetail.courier.supported != 1\">\r\n                                                        <td style=\"color:green\">Courier Lain </td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n\r\n                                                </td>\r\n                                                <td style=\"vertical-align : middle;text-align:center;\">\r\n                                                    <tr rowspan=\"2\">\r\n                                                        Last Status :<br />\r\n                                                        <span\r\n                                                            style=\"font-size: 20px;\"><strong>{{deliveryHistoryDetail.shipping_info[deliveryHistoryDetail.shipping_info.length-1].title}}</strong></span>\r\n                                                        <br />\r\n                                                        {{deliveryHistoryDetail.shipping_info[deliveryHistoryDetail.shipping_info.length-1].updated_date ? deliveryHistoryDetail.shipping_info[deliveryHistoryDetail.shipping_info.length-1].updated_date : deliveryHistoryDetail.shipping_info[deliveryHistoryDetail.shipping_info.length-1].created_date}}\r\n\r\n                                                    </tr>\r\n                                                </td>\r\n\r\n\r\n                                            </table>\r\n                                        </div> -->\r\n\r\n                                        <!-- <div class=\"product-group\" *ngIf=\"deliveryHistoryDetail.status == 'CANCEL'\">\r\n                                            <span style=\"color:#0EA8DB\">Shipping History</span>\r\n                                            <table>\r\n                                                <tr>\r\n                                                    <td>\r\n                                                        <span\r\n                                                            style=\"color:white; font-weight: bold;background-color: red;\">This\r\n                                                            order has been cancelled</span>\r\n                                                    </td>\r\n                                                </tr>\r\n                                            </table>\r\n                                        </div> -->\r\n\r\n                                        <!-- END SHIPPING HISTORY -->\r\n\r\n                                    <!-- </div> -->\r\n                                    <div class=\"product-group\">\r\n                                        <span style=\"color:#0EA8DB; font-weight: bold; font-size:14px; \">Order\r\n                                            Summary</span>\r\n                                        <table>\r\n                                            <tr>\r\n                                                <th class=\"product\">Name</th>\r\n                                                <th></th>\r\n                                                <th></th>\r\n                                                <th class=\"price\">Price</th>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>Total Orders</td>\r\n                                                <td></td>\r\n                                                <td></td>\r\n                                                <td class=\"price\">{{orderDetail.sum_total | number}}\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr *ngIf=\"deliveryHistoryDetail.billings\">\r\n                                                <td><span *ngIf=\"deliveryHistoryDetail.billings.discount\"> {{deliveryHistoryDetail.billings.discount.product_name}}</span></td>\r\n                                                <td></td>\r\n                                                <td class=\"priceBil\"></td>\r\n                                                <td class=\"priceBil\" *ngIf=\"deliveryHistoryDetail.billings.discount\">\r\n                                                    {{deliveryHistoryDetail.billings.discount.value | number}}\r\n                                                </td>\r\n                                                <td class=\"priceBil\" *ngIf=\" !deliveryHistoryDetail.billings.discount\">\r\n                                                    {{0 | number}}</td>\r\n                                            </tr>\r\n                                            <tr\r\n                                                *ngIf=\"deliveryHistoryDetail.unique_amount && deliveryHistoryDetail.payment_method == 'manual_payment'\">\r\n                                                <td>Unique Amount</td>\r\n                                                <td></td>\r\n                                                <td></td>\r\n                                                <td class=\"price\">{{deliveryHistoryDetail.unique_amount | number}}\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr style=\"border-top: 1.5px solid #eaeaea; \">\r\n                                                <td>TOTAL PRICE</td>\r\n                                                <td></td>\r\n                                                <td></td>\r\n                                                <td class=\"price\">{{orderDetail.sum_total | number}}\r\n                                                </td>\r\n                                            </tr>\r\n                                        </table>\r\n                                    </div>\r\n                                <!-- </div> -->\r\n\r\n                                <!-- ADMIN EDIT PAGE -->\r\n\r\n                                <div *ngIf=\"currentPermission == 'admin' && openMoreDetail\">\r\n                                    <div>\r\n                                        <span style=\"color:#0ea8db; font-weight: bold;\">Order Id : </span>\r\n                                        {{detailPage.booking_id}}\r\n                                        <div *ngIf=\"detailPage.products\" class=\"product-group\">\r\n                                            <table class=\"product\">\r\n                                                <tr>\r\n                                                    <th class=\"product\">Product Name</th>\r\n                                                    <th class=\"qty\">qty</th>\r\n                                                    <th class=\"ut\">unit price</th>\r\n                                                    <th class=\"price\">price</th>\r\n                                                </tr>\r\n                                                <tr *ngFor=\"let p of detailPage.products\">\r\n                                                    <td>{{p.product_name}} <br />\r\n                                                        <em><strong>note</strong>: {{p.note_to_merchant}}</em>\r\n                                                    </td>\r\n                                                    <td class=\"qty\">{{p.quantity | number }}x</td>\r\n                                                    <td class=\"qty\">{{p.fixed_price | number}}</td>\r\n                                                    <td class=\"qty\">{{p.total_product_price | number}}\r\n                                                    </td>\r\n                                                    <!-- <td>{{deliveryHistoryDetail.unique_amount}}</td>  -->\r\n                                                </tr>\r\n                                                <tr>\r\n                                                    <td>Shipping Fee</td>\r\n                                                    <td></td>\r\n                                                    <td></td>\r\n                                                    <td class=\"fee\">{{detailPage.shipping_fee | number }}</td>\r\n                                                </tr>\r\n                                                <tr style=\"border-top: 1.5px solid #eaeaea; width:80%;\">\r\n                                                    <td colspan=\"3\" class=\"priceTot\">Total Price</td>\r\n                                                    <td *ngIf=\"deliveryHistoryDetail.billings != undefined\"\r\n                                                        class=\"priceTotal\">\r\n                                                        {{moreOrderDetail.billings.sum_total | number }}</td>\r\n                                                    <td *ngIf=\"deliveryHistoryDetail.billings == undefined\"\r\n                                                        class=\"priceTotal\">\r\n                                                        {{moreOrderDetail.billings.sum_total | number }}</td>\r\n                                                    <td *ngIf=\"deliveryHistoryDetail.billings == undefined\"\r\n                                                        class=\"priceTotal\">\r\n                                                        {{moreOrderDetail.billings.sum_total | number }}</td>\r\n                                                </tr>\r\n                                            </table>\r\n                                        </div>\r\n                                        <!-- SHIPPING HISTORY -->\r\n                                        <!-- <div class=\"product-group\"\r\n                                            *ngIf=\"detailPage.status && detailPage.status != 'CANCEL'\">\r\n                                            <span style=\"color:#0EA8DB\">Shipping History</span>\r\n                                            <table>\r\n\r\n                                                <td>\r\n                                                    <tr>\r\n                                                        <td>Delivery Service :\r\n                                                            <span *ngIf=\"detailPage.delivery_detail\"\r\n                                                                style=\"color:#0EA8DB; font-weight: bold;\">{{detailPage.delivery_detail.awb_number}}</span>\r\n                                                        </td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"detailPage.delivery_detail\">\r\n                                                        <td>Courier : <span>{{detailPage.delivery_detail.courier}}</span> </td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"!detailPage.delivery_detail\">\r\n                                                        <td>Courier : <span>No Service</span> </td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"detailPage.courier && detailPage.courier.supported == 1\">\r\n                                                        <td style=\"color:#0EA8DB\"><b>Courier Kerjasama </b></td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"detailPage.courier && detailPage.courier.supported != 1\">\r\n                                                        <td style=\"color:green\">Courier Lain </td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                </td>\r\n                                                <td style=\"vertical-align : middle;text-align:center;\">\r\n                                                    <tr rowspan=\"2\">\r\n                                                        Last Status : <br />\r\n                                                        <span\r\n                                                            style=\"font-size: 20px;\"><strong>{{infoDetail}}</strong></span>\r\n                                                        <br />\r\n                                                        {{deliveryHistoryDetail.detailPage}}\r\n\r\n                                                    </tr>\r\n                                                </td>\r\n\r\n                                            </table>\r\n                                        </div> -->\r\n\r\n                                        <div class=\"product-group\" *ngIf=\"detailPage.status == 'CANCEL'\">\r\n                                            <span style=\"color:#0EA8DB\">Shipping History</span>\r\n                                            <table>\r\n                                                <tr>\r\n                                                    <td>\r\n                                                        <span\r\n                                                            style=\"color:white; font-weight: bold;background-color: red;\">This\r\n                                                            order has been cancelled</span>\r\n                                                    </td>\r\n                                                </tr>\r\n                                            </table>\r\n                                        </div>\r\n\r\n                                        <!-- END SHIPPING HISTORY -->\r\n\r\n                                    </div>\r\n                                    <div class=\"product-group\" *ngIf=\"deliveryHistoryDetail.order_list\">\r\n                                        <span style=\"color:#0EA8DB; font-weight: bold; font-size:14px; \">Order\r\n                                            Summary</span>\r\n                                        <table>\r\n                                            <tr>\r\n                                                <th class=\"product\">Name</th>\r\n                                                <th></th>\r\n                                                <th></th>\r\n                                                <th class=\"price\">Price</th>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>Total Orders</td>\r\n                                                <td></td>\r\n                                                <td></td>\r\n                                                <td class=\"price\">{{deliveryHistoryDetail.billings.total_price | number}}\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr *ngIf=\"deliveryHistoryDetail.billings\">\r\n                                                <td><span *ngIf=\"deliveryHistoryDetail.billings.discount\"> {{deliveryHistoryDetail.billings.discount.product_name}}</span></td>\r\n                                                <td></td>\r\n                                                <td class=\"priceBil\"></td>\r\n                                                <td class=\"priceBil\" *ngIf=\"deliveryHistoryDetail.billings.discount\">\r\n                                                    {{deliveryHistoryDetail.billings.discount.value | number}}\r\n                                                </td>\r\n                                                <td class=\"priceBil\" *ngIf=\" !deliveryHistoryDetail.billings.discount\">\r\n                                                    {{0 | number}}</td>\r\n                                            </tr>\r\n                                            <tr\r\n                                            *ngIf=\"deliveryHistoryDetail.unique_amount && deliveryHistoryDetail.payment_method == 'manual_payment'\">\r\n                                            <td>Unique Amount</td>\r\n                                            <td></td>\r\n                                            <td></td>\r\n                                            <td class=\"price\">{{deliveryHistoryDetail.unique_amount | number}}\r\n                                            </td>\r\n                                        </tr>\r\n                                            <tr style=\"border-top: 1.5px solid #eaeaea; \">\r\n                                                <td>TOTAL PRICE</td>\r\n                                                <td></td>\r\n                                                <td></td>\r\n                                                <td class=\"price\">{{deliveryHistoryDetail.sum_total | number}}\r\n                                                </td>\r\n                                            </tr>\r\n                                        </table>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <!-- MERCHANT PART -->\r\n                                <div *ngIf=\"currentPermission == 'merchant'\">\r\n                                    <div class=\"product-group\" *ngIf=\"deliveryHistoryDetail.products\">\r\n                                        <table>\r\n                                            <tr>\r\n                                                <th class=\"product\">Product Name</th>\r\n                                                <th class=\"qty\">qty</th>\r\n                                                <th class=\"ut\">unit price</th>\r\n                                                <th class=\"price\">price</th>\r\n                                            </tr>\r\n                                            <tr *ngFor=\"let product of deliveryHistoryDetail.products; let i = index\">\r\n                                                <td>{{product.product_name}} <br />\r\n                                                    <em><strong>note</strong>: {{product.note_to_merchant}}</em></td>\r\n                                                <td class=\"qty\">{{product.quantity}}</td>\r\n                                                <td class=\"price\">{{product.fixed_price | number}}</td>\r\n                                                <td class=\"price\">{{product.total_product_price | number}}</td>\r\n                                                <!-- <td class=\"price\">{{deliveryHistoryDetail.shipping_fee}}</td> -->\r\n                                                <!-- <td>{{deliveryHistoryDetail.unique_amount}}</td>  -->\r\n                                            </tr>\r\n                                            <tr *ngFor=\"let bd of deliveryHistoryDetail.additionalBillings\">\r\n                                                <td>{{bd.product_name}}</td>\r\n                                                <td></td>\r\n                                                <td></td>\r\n                                                <td class=\"priceBil\" *ngIf=\"bd.discount == true\">\r\n                                                    {{bd.value  | number}}</td>\r\n                                                <td class=\"fee\" *ngIf=\"bd.discount == false\">\r\n                                                    {{bd.value  | number}}\r\n                                                </td>\r\n\r\n                                            <!-- </tr>\r\n                                            <td>Shipping Fee</td>\r\n                                            <td></td>\r\n                                            <td></td>\r\n                                            <td class=\"fee\">{{deliveryHistoryDetail.shipping_fee | currency: ' Rp '}}</td>\r\n                                            <tr>\r\n                                                <td></td>\r\n                                            </tr> -->\r\n\r\n                                            <tr style=\"border-top: 1.5px solid #eaeaea; width:80%;\">\r\n                                                <td colspan=\"3\" class=\"priceTot\">Total Price</td>\r\n                                                <td class=\"priceTotal\">\r\n                                                    {{deliveryHistoryDetail.sum_total| number}}</td>\r\n                                                <!-- <td *ngIf=\"deliveryHistoryDetail.billings == undefined\" class=\"priceTotal\">\r\n                                                    {{calculate| number}}</td> -->\r\n                                            </tr>\r\n                                        </table>\r\n\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"buyer-detail\" *ngIf=\"orderDetail && orderDetail.member_detail\">\r\n                                    <div class=\"form-group\">\r\n                                        <div class=\"spacing\">\r\n                                            <label id=\"buyer-detail\">Buyer Detail</label>\r\n                                            <table id=\"custom_table\">\r\n                                                <tr>\r\n                                                    <th>Name</th>\r\n                                                    <th>Address</th>\r\n                                                    <th>Cell Phone</th>\r\n                                                </tr>\r\n                                                <tr>\r\n                                                    <td>{{orderDetail.member_detail.nama_penerima_gopay}}</td>\r\n                                                    <td>{{orderDetail.member_detail.alamat_rumah}} , {{orderDetail.member_detail.kecamatan_rumah}} , {{orderDetail.member_detail.kota_rumah}} , {{orderDetail.member_detail.provinsi_rumah}} , <span *ngIf=\"orderDetail.member_detail.kode_pos_rumah\">{{orderDetail.member_detail.kode_pos_rumah}}</span></td>\r\n                                                    <td>{{orderDetail.member_detail.telp_penerima_gopay != '-' ? orderDetail.member_detail.telp_penerima_gopay : orderDetail.member_detail.no_wa_penerima}}</td>\r\n                                                    <!-- <td>{{deliveryHistoryDetail.member_detail.email}}</td> -->\r\n                                                </tr>\r\n                                            </table>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n                            <!-- <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div> -->\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    <div *ngIf=\"!merchantMode && currentPermission == 'admin'\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\">\r\n                                <h2>Order Info</h2>\r\n\r\n                            </div>\r\n                            <div class=\"card-content\">\r\n                                <label>Order Status :\r\n                                    <strong>\r\n                                        <span style=\"color:#0EA8DB\">{{orderDetail.status}}</span>\r\n                                    </strong>\r\n                                </label>\r\n                                <div class=\"img-upload\"  *ngIf=\"deliveryHistoryDetail == 'PENDING'\">\r\n                                    <button class=\"btn_upload\" (click)=\"processOrder()\">\r\n                                        <i class=\"fa fa-fw fa-check-circle\"></i>\r\n                                        <span>Process Order</span>\r\n                                    </button>\r\n                                    <button class=\"btn_delete\" (click)=\"cancelOrder()\">\r\n                                        <i class=\"fa fa-fw fa-times-circle\"></i>\r\n                                        <span>Cancel Order</span>\r\n                                    </button>\r\n                                </div>                    \r\n                            </div>\r\n\r\n                            <div class=\"complete-order-container\">\r\n                                <div class=\"completed-order-head\" *ngIf=\"orderDetail.status && orderDetail.status.toLowerCase() == 'processed'\">Silahkan klik button di bawah ini jika anda ingin mengubah status order menjadi completed. Pastikan masing-masing pengiriman sudah delivered terlebih dahulu.</div>\r\n                                <!-- <label for=\"weight\">Date and time</label>\r\n                                <div class=\"input-date-form\">\r\n                                    <form class=\"form-inline\">\r\n                                        <div class=\"form-group\">\r\n                                            <div class=\"input-group\">\r\n                                            <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                    name=\"dp\" [(ngModel)]=\"date\" ngbDatepicker [footerTemplate]=\"footerTemplate\" #d=\"ngbDatepicker\" [disabled] = \"isCompleteOrder == true\">\r\n                                            <div class=\"input-group-append\">\r\n                                                <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\">\r\n                                                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                </button>\r\n                                            </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </form>\r\n                                    <ngb-timepicker [(ngModel)]=\"time\" [seconds]=\"true\" [disabled] = \"isCompleteOrder == true\"></ngb-timepicker>\r\n                                </div>\r\n                            \r\n                                <label for=\"weight\">Reciever Name</label>\r\n                                <input\r\n                                    name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                    placeholder=\"Masukan nama penerima\"\r\n                                    [(ngModel)]=\"receiver_name\"\r\n                                    [disabled] = \"isCompleteOrder == true\"\r\n                                    required />\r\n                            \r\n                                <label for=\"weight\">Receiver Status</label>\r\n                                <input\r\n                                    name=\"delivery_status\" class=\"form-control awb\" type=\"text\"\r\n                                    placeholder=\"Masukan status penerima\"\r\n                                    [(ngModel)]=\"delivery_status\"\r\n                                    [disabled] = \"isCompleteOrder == true\"\r\n                                    required />\r\n                                \r\n                                <label for=\"weight\">Remark</label>\r\n                                <input\r\n                                    name=\"remark\" class=\"form-control awb\" type=\"text\"\r\n                                    placeholder=\"Masukan remark\"\r\n                                    [(ngModel)]=\"remark\"\r\n                                    [disabled] = \"isCompleteOrder == true\"\r\n                                    required /> -->\r\n\r\n                                <div class=\"button-complete\">\r\n                                    <div *ngIf=\"completeButtonConfirmation\" class=\"complete-comfirmation\">\r\n                                        <span class=\"warning-text\">Anda yakin akan mengubah menjadi complete?</span>\r\n                                        <div style=\"display:flex;width:100%;justify-content:space-around;align-items:center;cursor:auto;\">\r\n                                            <button\r\n                                                *ngIf=\"isChangeLoading == false\"\r\n                                                class=\" btn btn-success half-button\"\r\n                                                type=\"submit\"\r\n                                                value=\"Submit\"\r\n                                                (click)=\"updateComplete()\"\r\n                                            >\r\n                                                Ya\r\n                                            </button>\r\n                                            <button *ngIf=\"isChangeLoading == true\" class=\"btn rounded-btn open-login\" type=\"submit\" ><i class=\"fa fa-spinner fa-pulse\"></i></button>&nbsp;\r\n                                            <span>&nbsp;</span>\r\n                                            <button\r\n                                                class=\" btn btn-cancel half-button\"\r\n                                                type=\"submit\"\r\n                                                value=\"Submit\"\r\n                                                (click)=\"completeButtonConfirmation = false\"\r\n                                            >\r\n                                                Tidak\r\n                                            </button>\r\n                                        </div>\r\n                                    </div>\r\n                                    <button *ngIf=\"!completeButtonConfirmation && orderDetail.status && orderDetail.status.toLowerCase() == 'processed'\"\r\n                                        class=\" btn btn-success full-button\"\r\n                                        type=\"submit\"\r\n                                        value=\"Submit\"\r\n                                        (click)=\"completeButtonConfirmation = true\"\r\n                                        >\r\n                                        Completed this Order\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <!-- <div class=\"card mb-3\"> -->\r\n                            <!-- <div class=\"card-header\">\r\n                                <h2>Payment Info</h2>\r\n                                <button *ngIf=\"deliveryHistoryDetail.status == 'WAITING FOR CONFIRMATION'\" class=\"save\"\r\n                                    [disabled]=\"!isClicked && !isClicked2\" [class.save-active]=\"isClicked || isClicked2\"\r\n                                    (click)=\"saveThis()\">Save Change</button>\r\n                                <button *ngIf=\"deliveryHistoryDetail.status == 'WAITING FOR CONFIRMATION' && !change\"\r\n                                    class=\"save change\" (click)=\"change = true\">Change Transaction Status</button>\r\n\r\n                            </div> -->\r\n                            <!-- <div class=\"card-content\">\r\n                                <label>Payment Status :\r\n                                    <strong>\r\n                                        <span *ngIf=\"deliveryHistoryDetail.status == 'WAITING FOR CONFIRMATION'\"\r\n                                            style=\"color:rgb(2, 172, 24);\">{{deliveryHistoryDetail.status}}</span>\r\n                                        <span *ngIf=\"deliveryHistoryDetail.status != 'WAITING FOR CONFIRMATION'\"\r\n                                            style=\"color:#0EA8DB\">{{deliveryHistoryDetail.status}}</span>\r\n                                    </strong>\r\n                                </label>\r\n                                <br>\r\n                                <div *ngIf=\"change\">\r\n                                    <label>Transaction Status</label>\r\n                                    <div *ngIf=\"!merchantMode\" name=\"transaction_status\"\r\n                                        class=\"status-button-container\">\r\n                                        \r\n                                        <button class=\"common-button option\" [class.paid]=\"isClicked\"\r\n                                            (click)=\"onpaidclick()\" target=\"_blank\"> <span *ngIf=\"isClicked\"><i\r\n                                                    class=\"material-icons\">\r\n                                                    done\r\n                                                </i></span>Paid</button>\r\n                                        <button class=\"common-button option\" [class.cancel]=\"isClicked2\"\r\n                                            (click)=\"oncancelclick()\" target=\"_blank\"><span *ngIf=\"isClicked2\"><i\r\n                                                    class=\"material-icons\">\r\n                                                    done\r\n                                                </i></span> Cancel</button>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"border-top my-2\" style=\"border-bottom: solid 0.5px #eaeaea\"></div>\r\n                                <div *ngIf=\"merchantMode\" name=\"transaction_status\">\r\n                                    <div class=\"\">\r\n                                        {{deliveryHistoryDetail.status}}\r\n                                    </div>\r\n                                </div>\r\n                                <br *ngIf=\"deliveryHistoryDetail.billings != undefined\" class=\"services\">\r\n                                <table *ngIf=\"deliveryHistoryDetail.billings != undefined\" class=\"services\">\r\n                                    <tr>\r\n                                        <th>Service</th>\r\n                                        <th>Via</th>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td *ngIf=\"deliveryHistoryDetail.status!='CHECKOUT'\">\r\n                                            {{deliveryHistoryDetail.payment_detail.service}}</td>\r\n                                        <td *ngIf=\"deliveryHistoryDetail.status=='CHECKOUT'\"> -</td>\r\n\r\n                                        <td *ngIf=\"deliveryHistoryDetail.status!='CHECKOUT'\">\r\n                                            {{deliveryHistoryDetail.payment_detail.payment_via}}</td>\r\n                                        <td *ngIf=\"deliveryHistoryDetail.status=='CHECKOUT'\"> -</td>\r\n                                    </tr>\r\n                                </table>\r\n\r\n                                <div class=\"border-top my-4\" style=\"border-bottom: solid 0.5px #eaeaea\"></div>\r\n                                <div class=\"btn btn-danger\" style=\"width: 100%; padding:10px;font-size: 12px;\"\r\n                                    (click)=\"openNote(historyID)\" *ngIf=\"openMoreDetail\">\r\n                                    Cancel Order</div>\r\n                            </div> -->\r\n                        <!-- </div> -->\r\n                        <!-- <div class=\"card mb-3\"\r\n                            *ngIf=\"openMoreDetail && detailPage.status == 'PAID' && detailPage.transaction_status != 'HOLD' && (infoDetailLabel == 'on_delivery' || infoDetailLabel == 'delivered')\">\r\n                            <div class=\"card-header\">\r\n                                <h2>Escrow Option</h2>\r\n                            </div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"btn\"\r\n                                    style=\"width: 100%;padding:10px;background-color: #8854D0;color:white;font-size: 12px;\"\r\n                                    (click)=\"openNote2(historyID)\">\r\n                                    Hold Funds\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div> -->\r\n\r\n                        <!-- <div *ngIf=\"deliveryHistoryDetail.status == 'WAITING FOR CONFIRMATION'\">\r\n                            <div *ngIf=\"deliveryHistoryDetail.billings != undefined\">\r\n                                <div *ngIf=\"deliveryHistoryDetail.status != 'PAID'\" class=\"form-group\">\r\n                                    <label>Expired Payment date</label>\r\n                                    <div *ngIf=\"deliveryHistoryDetail.status!='CHECKOUT'\" name=\"payment_expire_date\">\r\n                                        {{deliveryHistoryDetail.payment_expire_date.in_string}}</div>\r\n                                    <div *ngIf=\"deliveryHistoryDetail.status=='CHECKOUT'\" name=\"payment_expire_date\">\r\n                                        {{deliveryHistoryDetail.payment_expire_date}}</div>\r\n                                </div>\r\n                            </div>\r\n                            <span class=\"card-content\" style=\"text-align: center;\">Payer Detail</span>\r\n                            <div class=\"card-content payer\">\r\n                                <table align=\"center\">\r\n                                    <th>Payer Name</th>\r\n                                    <th>Account Number</th>\r\n                                    <th>Payer Note</th>\r\n                                    <tr>\r\n                                        <td>{{deliveryHistoryDetail.payer_detail.payer_name}}</td>\r\n                                        <td>{{deliveryHistoryDetail.payer_detail.payer_acc_number}}</td>\r\n                                        <td>{{deliveryHistoryDetail.payer_detail.payment_notes}}</td>\r\n                                    </tr>\r\n\r\n\r\n                                </table>\r\n                            </div>\r\n                        </div> -->\r\n                        <!-- <div>\r\n                            <br>\r\n                            <div *ngIf=\"deliveryHistoryDetail.uploaded_receipt\">\r\n                                <div class=\"card-content\" style=\"text-align: center; padding:initial\">\r\n                                    Payment Slip:\r\n                                </div>\r\n                                <div class=\"card-content\" style=\"text-align: center\">\r\n                                    <img src=\"{{deliveryHistoryDetail.uploaded_receipt}}\" height=\"150\">\r\n                                </div>\r\n                                <div style=\"text-align: center; padding-bottom: 12px;\">\r\n                                    <button class=\"btn btn-success\" style=\"margin:auto;\" (click)=\"receipt()\"\r\n                                        target=\"_blank\"><i class=\"fas fa-print\"></i> Open Receipt</button>\r\n                                </div>\r\n\r\n                            </div>\r\n                        </div> -->\r\n\r\n                    </div>\r\n\r\n                    <!-- ADMIN SHIPPING INFO -->\r\n                    <!-- <div *ngIf=\"deliveryHistoryDetail.status=='PROCESSED'\" class=\"card mb-3\"\r\n                        >\r\n                        <div class=\"card-header\">\r\n                            <h2>Shipping Information </h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\" *ngIf=\"deliveryHistoryDetail.shipping_info.length <= 1\">\r\n                                <label>no shipping info</label>\r\n                            </div>\r\n                            <div class=\"col-md-12\" *ngIf=\"deliveryHistoryDetail.shipping_info.length > 0\">\r\n                                <label>Delivery Service :\r\n                                    <strong><span style=\"color:#0EA8DB\">{{deliveryHistoryDetail.delivery_detail.courier}}\r\n                                            ({{deliveryHistoryDetail.delivery_detail.delivery_service}})</span></strong></label>\r\n\r\n                                <div class=\"line\" style=\"width:100%; border-top: 1.5px solid #eaeaea; \"></div>\r\n                                <p style=\"margin-bottom:30px; margin-top:15px;\">Shipping Status</p>\r\n                                <p style=\"font-size:12px;\">Click each button to process your Shipping</p>\r\n\r\n                                <div class=\"form-group\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <div class=\"radio-button checked-{{deliveryHistoryDetail.shipping_info[i]?true:'' || sSL.checked}} hovered-{{sSL.hovered}}\"\r\n                                            (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                        </div>\r\n                                        <label (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                            {{sSL.label}}<br />\r\n                                            <div class=\"shipping-date\" *ngIf=\"sSL.created_date\">\r\n                                                <i class=\"fa fa-check\"></i>{{sSL.created_date}}</div>\r\n                                            <span *ngIf=\"deliveryHistoryDetail.shipping_info[i]\"><i class=\"fa fa-check\"></i>Done</span>\r\n\r\n                                            <div *ngIf=\"sSL.value=='on_delivery'\" class=\"awb-list\">\r\n                                                <div class=\"input-delivery-container\" *ngIf=\"shippingStatusList[1].checked == true && changeAWB\">\r\n\r\n                                                    \r\n                                                    <ng-container *ngIf=\"deliveryHistoryDetail.available_courier\">\r\n\r\n                                                        <select name=\"courier\" class=\"form-control awb\" [(ngModel)]=\"courier\">\r\n                                                            <option value=\"\">Jenis Kurir</option>\r\n                                                            <ng-container *ngFor=\"let typeCourier of deliveryHistoryDetail.available_courier\">\r\n                                                                <option [value]=\"typeCourier.courier_code\">{{typeCourier.courier_code.toUpperCase()}}</option>\r\n                                                            </ng-container>\r\n                                                        </select>\r\n                                                        \r\n                                                        <ng-container *ngIf=\"courier == 'REALS-SAP'\">\r\n                                                            <ng-container *ngFor=\"let typeCourier of deliveryHistoryDetail.available_courier\">\r\n                                                                <ng-container *ngIf=\"typeCourier.courier_code == 'REALS-SAP'\">\r\n                                                                    <select name=\"delivery_service\" class=\"form-control awb\" [(ngModel)]=\"delivery_service\">\r\n                                                                        <option value=\"\">Delivery Service</option>\r\n                                                                        <ng-container *ngFor=\"let typeDeliveryService of typeCourier.delivery_service\">\r\n                                                                            <option [value] = \"typeDeliveryService\">{{typeDeliveryService}}</option>\r\n                                                                        </ng-container>\r\n                                                                    </select>\r\n                                                                    <select name=\"delivery_method\" class=\"form-control awb\" [(ngModel)]=\"delivery_method\">\r\n                                                                        <option value=\"\">Delivery Method</option>\r\n                                                                        <ng-container *ngFor=\"let typeDeliveryMethod of typeCourier.delivery_method\">\r\n                                                                            <option [value] = \"typeDeliveryMethod\">{{typeDeliveryMethod}}</option>\r\n                                                                        </ng-container>\r\n                                                                    </select>\r\n                                                                </ng-container>\r\n                                                            </ng-container>\r\n\r\n                                                            <input    \r\n                                                                name=\"delivery_remarks\" class=\"form-control awb\" type=\"text\"\r\n                                                                placeholder=\"Masukan remarks\"\r\n                                                                [(ngModel)]=\"delivery_remarks\"\r\n                                                            />\r\n                                                            \r\n                                                        </ng-container>\r\n                                                   \r\n                                                \r\n\r\n                                                        <ng-container *ngIf=\"courier == 'others'\">\r\n                                                            <input\r\n                                                                \r\n                                                                name=\"courier\" class=\"form-control awb\" type=\"text\"\r\n                                                                placeholder=\"Masukan nama kurir\"\r\n                                                                [(ngModel)]=\"courier_name\"\r\n                                                                required />\r\n                                                    \r\n                                                            <input\r\n                                                                \r\n                                                                name=\"delivery_service\" class=\"form-control awb\" type=\"text\"\r\n                                                                placeholder=\"Masukan delivery service\"\r\n                                                                [(ngModel)]=\"delivery_service\"\r\n                                                                required />\r\n                                                    \r\n                                                            <input\r\n                                                                \r\n                                                                name=\"delivery_method\" class=\"form-control awb\" type=\"text\"\r\n                                                                placeholder=\"Masukan delivery method\"\r\n                                                                [(ngModel)]=\"delivery_method\"\r\n                                                                required />\r\n                                                        \r\n                                                            <input\r\n                                                                \r\n                                                                name=\"awb_number\" class=\"form-control awb\" type=\"text\"\r\n                                                                placeholder=\"Masukan nomor resi\"\r\n                                                                [(ngModel)]=\"awb_number\"\r\n                                                                required />\r\n                                                        \r\n                                                            <input\r\n                                                                \r\n                                                                name=\"delivery_remarks\" class=\"form-control awb\" type=\"text\"\r\n                                                                placeholder=\"Masukan remarks\"\r\n                                                                [(ngModel)]=\"delivery_remarks\"\r\n                                                                required />\r\n                                                        </ng-container>\r\n                                                        \r\n                                                    </ng-container>\r\n\r\n                                                    <ng-container *ngIf=\"!deliveryHistoryDetail.available_courier\">\r\n\r\n                                                        <input\r\n                                                        \r\n                                                        name=\"courier\" class=\"form-control awb\" type=\"text\"\r\n                                                        [(ngModel)] = \"courier\"\r\n                                                        disabled />\r\n                                                \r\n                                                        <input\r\n                                                            \r\n                                                            name=\"courier_name\" class=\"form-control awb\" type=\"text\"\r\n                                                            placeholder=\"Masukan nama kurir\"\r\n                                                            [(ngModel)]=\"courier_name\"\r\n                                                            required />\r\n                                                    \r\n                                                        <input\r\n                                                            \r\n                                                            name=\"delivery_service\" class=\"form-control awb\" type=\"text\"\r\n                                                            placeholder=\"Masukan delivery service\"\r\n                                                            [(ngModel)]=\"delivery_service\"\r\n                                                            required />\r\n                                                    \r\n                                                        <input\r\n                                                            \r\n                                                            name=\"delivery_method\" class=\"form-control awb\" type=\"text\"\r\n                                                            placeholder=\"Masukan delivery method\"\r\n                                                            [(ngModel)]=\"delivery_method\"\r\n                                                            required />\r\n                                                        \r\n                                                        <input\r\n                                                            \r\n                                                            name=\"awb_number\" class=\"form-control awb\" type=\"text\"\r\n                                                            placeholder=\"Masukan nomor resi\"\r\n                                                            [(ngModel)]=\"awb_number\"\r\n                                                            required />\r\n                                                        \r\n                                                        <input\r\n                                                            \r\n                                                            name=\"delivery_remarks\" class=\"form-control awb\" type=\"text\"\r\n                                                            placeholder=\"Masukan remarks\"\r\n                                                            [(ngModel)]=\"delivery_remarks\"\r\n                                                            required />\r\n\r\n                                                    </ng-container>\r\n                                                    \r\n                                                        <div style=\"display:flex;width:100%;justify-content:space-around;align-items:center;cursor:auto;\">\r\n\r\n                                                            <button\r\n                                                                class=\"btn btn-primary\"\r\n                                                                *ngIf=\"shippingStatusList[1].checked == true && isChangeAWBLoading != true\"\r\n                                                                type=\"submit\"\r\n                                                                (click)=\"updateAWB()\"\r\n                                                                value=\"submit\">\r\n                                                                Update\r\n                                                            </button>\r\n                                                            \r\n                                                            <button\r\n                                                                class=\"btn sbtn-primary\"\r\n                                                                *ngIf=\"shippingStatusList[1].checked == true && isChangeAWBLoading != true && deliveryHistoryDetail.delivery_detail.courier\"\r\n                                                                type=\"submit\"\r\n                                                                (click)=\"changeAWB = false\"\r\n                                                                value=\"submit\">\r\n                                                                Cancel\r\n                                                            </button>\r\n\r\n                                                            <button *ngIf=\"isChangeAWBLoading == true\" style=\"margin:auto;width:85px;\" class=\"btn sbtn-primary\"><i class=\"fa fa-spinner fa-pulse\"></i></button>\r\n                                                        </div>\r\n                                                </div>\r\n\r\n                                                <div class=\"input-delivery-container\" *ngIf=\"deliveryHistoryDetail.delivery_detail.courier && changeAWB == false && isCompleteOrder != true\">\r\n                                                        <input\r\n                                                            *ngIf=\"deliveryHistoryDetail.delivery_detail.courier == 'others'\"\r\n                                                            name=\"courier_name\" class=\"form-control awb\" type=\"text\"\r\n                                                            [(ngModel)]=\"deliveryHistoryDetail.delivery_detail.courier_name\"\r\n                                                            disabled />\r\n\r\n                                                            <input\r\n                                                            *ngIf=\"deliveryHistoryDetail.delivery_detail.courier != 'others'\"\r\n                                                            name=\"courier\" class=\"form-control awb\" type=\"text\"\r\n                                                            [(ngModel)]=\"deliveryHistoryDetail.delivery_detail.courier\"\r\n                                                            disabled />\r\n                                                    \r\n                                                        <input\r\n                                                            name=\"delivery_service\" class=\"form-control awb\" type=\"text\"\r\n                                                            placeholder=\"empty delivery service\"\r\n                                                            [(ngModel)]=\"deliveryHistoryDetail.delivery_detail.delivery_service\"\r\n                                                            disabled />\r\n                                                    \r\n                                                        <input\r\n                                                            name=\"delivery_method\" class=\"form-control awb\" type=\"text\"\r\n                                                            placeholder=\"empty delivery method\"\r\n                                                            [(ngModel)]=\"deliveryHistoryDetail.delivery_detail.delivery_method\"\r\n                                                            disabled />\r\n                                                        \r\n                                                        <input\r\n                                                            name=\"awb_number\" class=\"form-control awb\" type=\"text\"\r\n                                                            placeholder=\"empty AWB\"\r\n                                                            [(ngModel)]=\"deliveryHistoryDetail.delivery_detail.awb_number\"\r\n                                                            disabled />\r\n\r\n                                                        <input\r\n                                                            name=\"delivery_remarks\" class=\"form-control awb\" type=\"text\"\r\n                                                            placeholder=\"empty remarks\"\r\n                                                            [(ngModel)]=\"delivery_remarks\"\r\n                                                            disabled />\r\n\r\n                                                        <button\r\n                                                            style=\"margin:auto;\"\r\n                                                            class=\"btn btn-primary\"\r\n                                                            *ngIf=\"deliveryHistoryDetail.last_shipping_info == 'on_delivery' && deliveryHistoryDetail.delivery_detail.courier == 'others' \"\r\n                                                            type=\"submit\"\r\n                                                            (click)=\"changeAWB = true\"\r\n                                                            value=\"Submit\">\r\n                                                        Change AWB Number\r\n                                                        </button>\r\n                                                </div>\r\n                                            </div>\r\n\r\n                                            <div  *ngIf=\"sSL.value=='delivered' && shippingStatusList[2].checked == true && deliveryHistoryDetail.last_shipping_info == 'on_delivery'\" class=\"awb-list delivered-form\">\r\n                                                <div class=\"input-delivery-container\">\r\n                                                    <div class=\"input-date-form\">\r\n                                                        <form class=\"form-inline\">\r\n                                                            <div class=\"form-group\">\r\n                                                                <div class=\"input-group\">\r\n                                                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                        name=\"dp\" [(ngModel)]=\"date\" ngbDatepicker [footerTemplate]=\"footerTemplate\" #d=\"ngbDatepicker\" [disabled] = \"isCompleteOrder == true\">\r\n                                                                <div class=\"input-group-append\">\r\n                                                                    <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\">\r\n                                                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                    </button>\r\n                                                                </div>\r\n                                                                </div>\r\n                                                            </div>\r\n                                                        </form>\r\n                                                        <ngb-timepicker [(ngModel)]=\"time\" [seconds]=\"true\" [disabled] = \"isCompleteOrder == true\"></ngb-timepicker>\r\n                                                    </div>\r\n                                                \r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Masukan nama penerima\"\r\n                                                        [(ngModel)]=\"receiver_name\"\r\n                                                        [disabled] = \"isCompleteOrder == true\"\r\n                                                        required />\r\n                                                \r\n                                                    <input\r\n                                                        name=\"delivery_status\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Masukan status penerima\"\r\n                                                        [(ngModel)]=\"delivery_status\"\r\n                                                        [disabled] = \"isCompleteOrder == true\"\r\n                                                        required />\r\n                                                    \r\n                                                    <input\r\n                                                        name=\"remark\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Masukan remark\"\r\n                                                        [(ngModel)]=\"remark\"\r\n                                                        [disabled] = \"isCompleteOrder == true\"\r\n                                                        required />\r\n\r\n\r\n                                                    <div *ngIf=\"isCompleteOrder == false\" class=\"button-complete\">\r\n                                                        <div *ngIf=\"completeButtonConfirmation\">\r\n                                                            <span>Anda yakin akan mengubah menjadi complete?</span>\r\n                                                            <div style=\"display:flex;width:100%;justify-content:space-around;align-items:center;cursor:auto;\">\r\n                                                                <button\r\n                                                                    *ngIf=\"isChangeLoading == false\"\r\n                                                                    class=\" btn btn-success\"\r\n                                                                    type=\"submit\"\r\n                                                                    value=\"Submit\"\r\n                                                                    (click)=\"updateComplete()\"\r\n                                                                >\r\n                                                                    Ya\r\n                                                                </button>\r\n                                                                <button *ngIf=\"isChangeLoading == true\" class=\"btn rounded-btn open-login\" type=\"submit\" ><i class=\"fa fa-spinner fa-pulse\"></i></button>&nbsp;\r\n                                                                <span>&nbsp;</span>\r\n                                                                <button\r\n                                                                    class=\" btn btn-cancel\"\r\n                                                                    type=\"submit\"\r\n                                                                    value=\"Submit\"\r\n                                                                    (click)=\"completeButtonConfirmation = false\"\r\n                                                                >\r\n                                                                    Tidak\r\n                                                                </button>\r\n                                                            </div>\r\n                                                        </div>\r\n                                                        <button *ngIf=\"!completeButtonConfirmation\"\r\n                                                            class=\" btn btn-success\"\r\n                                                            type=\"submit\"\r\n                                                            value=\"Submit\"\r\n                                                            (click)=\"completeButtonConfirmation = true\"\r\n                                                            [disabled]=\"!receiver_name || !delivery_status || !remark || !date || !time\"\r\n                                                            >\r\n                                                            Completed this Order\r\n                                                        </button>\r\n                                                    </div>\r\n\r\n                                                    <div *ngIf=\"isCompleteOrder == true\" class=\"order-complete-text\">\r\n                                                        Order Completed\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n\r\n                                        </label>\r\n\r\n                                    </div>\r\n                                </div>\r\n\r\n\r\n                                <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services=='no-services'\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <div class=\"radio-button checked-false hovered-false\">\r\n                                        </div>\r\n                                        <label>\r\n                                            {{sSL.label}}<br />\r\n                                            <span class=\"shipping-date\"\r\n                                                *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                        </label>\r\n                                    </div>\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n\r\n                            <div class=\"printLabel\" *ngIf=\"deliveryHistoryDetail.billings != undefined\">\r\n                                <button class=\"btn btn-primary\" (click)=\"openLabel(historyID)\">Open Label</button>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    <!-- END OF ADMIN SHIPPING INFO -->\r\n\r\n                    <!-- MERCHANT SHIPPING INFO -->\r\n                    <!-- <div *ngIf=\"deliveryHistoryDetail.status=='PAID' && currentPermission == 'merchant' && !editThis || deliveryHistoryDetail.status=='PAID' && currentPermission == 'admin' && !editThis\"\r\n                        class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Shipping Information </h2>\r\n                            <button class=\"edit\" (click)=\"edit()\">Edit</button>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\" *ngIf=\" !deliveryHistoryDetail.shipping_info\">\r\n                                <label>no shipping info</label>\r\n                            </div>\r\n                            <div class=\"col-md-12\" *ngIf=\"deliveryHistoryDetail.shipping_info\">\r\n                                <label>Delivery Service :\r\n                                    <strong><span *ngIf=\"deliveryHistoryDetail.shipping\" style=\"color:#0EA8DB\">{{deliveryHistoryDetail.shipping.service}}\r\n                                            ({{deliveryHistoryDetail.delivery.delivery_detail.description}})</span></strong></label>\r\n\r\n                                <div class=\"line\" style=\"width:100%; border-top: 1.5px solid #eaeaea; \"></div>\r\n                                <p style=\"margin-bottom:30px; margin-top:15px;\">Shipping Status</p>\r\n\r\n\r\n                                <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services'\">\r\n                                    <div class=\"shipping-statuss\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <div class=\"radio-button checked-{{sSL.checked}} hovered-{{sSL.hovered}} disabled\"\r\n                                            (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusL//// ist(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                        </div>\r\n                                        <label (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                            {{sSL.label}}<br />\r\n                                            <span class=\"shipping-date\"\r\n                                                *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                            <div class=\"form-group\" *ngIf=\"sSL.value == 'on_packaging'\">\r\n                                                <div\r\n                                                    *ngIf=\"deliveryHistoryDetail.billings != undefined && deliveryHistoryDetail.courier == undefined\">\r\n                                                    <select *ngIf=\"!packaging\" disabled class=\"form-control\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.shipping_services\">\r\n                                                        <option *ngFor=\"let ss of shippingServices\"\r\n                                                            [ngValue]=\"ss.value\">\r\n                                                            {{ss.label}}</option>\r\n                                                        <button *ngIf=\"detail.courier\" class=\"btn btn-warning\"\r\n                                                            (click)=\"saveThis()\"> Process</button>\r\n                                                    </select>\r\n\r\n                                                    <select *ngIf=\"packaging\" class=\"form-control\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.shipping_services\">\r\n                                                        <option *ngFor=\"let ss of shippingServices\"\r\n                                                            [ngValue]=\"ss.value\">\r\n                                                            {{ss.label}}</option>\r\n                                                    </select>\r\n                                                </div>\r\n                                                <div *ngIf=\"deliveryHistoryDetail.billings == undefined && deliveryHistoryDetail.courier == undefined\"\r\n                                                    class=\"awb-list\">\r\n                                                </div>\r\n                                                <div\r\n                                                    *ngIf=\"deliveryHistoryDetail.billings == undefined && deliveryHistoryDetail.courier != undefined\">\r\n\r\n\r\n                                                </div>\r\n                                            </div>\r\n\r\n                                            <div *ngIf=\"sSL.value=='on_delivery'\" class=\"awb-list\">\r\n                                                <div *ngIf=\"deliveryHistoryDetail.courier == undefined\" class=\"input-form\">\r\n                                                </div>\r\n                                                <div *ngIf=\"deliveryHistoryDetail.courier != undefined\" class=\"input-form\">\r\n\r\n\r\n                                                </div>\r\n                                            </div>\r\n\r\n\r\n                                        </label>\r\n\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services=='no-services'\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <div class=\"radio-button checked-false hovered-false\">\r\n                                        </div>\r\n                                        <label>\r\n                                            {{sSL.label}}<br />\r\n                                            <span class=\"shipping-date\"\r\n                                                *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                        </label>\r\n                                    </div>\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n\r\n                            <div class=\"printLabel\" *ngIf=\"deliveryHistoryDetail.billings != undefined\">\r\n                                <button class=\"btn btn-primary\" (click)=\"openLabel(historyID)\">Open Label</button>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div> -->\r\n\r\n\r\n                    <!----------------------------------- Status Completed ------------------------>\r\n\r\n                    <!-- <div *ngIf=\"deliveryHistoryDetail.status=='COMPLETED' && !editThis || deliveryHistoryDetail.status=='COMPLETED' && !editThis\"\r\n                        class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Shipping Information </h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\" *ngIf=\" !deliveryHistoryDetail.delivery_detail\">\r\n                                <label>no shipping info</label>\r\n                            </div>\r\n                            <div class=\"col-md-12\" *ngIf=\"deliveryHistoryDetail.delivery_detail\">\r\n                                <label>Delivery Service :\r\n                                    <strong><span *ngIf=\"deliveryHistoryDetail.delivery_detail\" style=\"color:#0EA8DB\">{{deliveryHistoryDetail.delivery_detail.courier}}\r\n                                            ({{deliveryHistoryDetail.delivery_detail.delivery_service}})</span></strong></label>\r\n\r\n                                <div class=\"line\" style=\"width:100%; border-top: 1.5px solid #eaeaea; \"></div>\r\n\r\n                                <div class=\"line\" style=\"width:100%; border-top: 1.5px solid #eaeaea; \"></div>\r\n                                <p style=\"margin-bottom:30px; margin-top:15px;\">Shipping Status</p>\r\n\r\n\r\n                                <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services'\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <div class=\"radio-button checked-{{sSL.checked}} hovered-{{sSL.hovered}} disabled\">\r\n                                        </div>\r\n                                        <label>\r\n                                            {{sSL.label}}<br />\r\n                                            <div class=\"shipping-date\" *ngIf=\"sSL.created_date\">\r\n                                                <i class=\"fa fa-check\"></i>{{sSL.created_date}}</div>\r\n                                                <span *ngIf=\"deliveryHistoryDetail.shipping_info[i]\"><i class=\"fa fa-check\"></i>Done</span>\r\n                                            <div class=\"form-group\" *ngIf=\"sSL.value == 'on_packaging'\">\r\n                                            </div>\r\n\r\n                                            <div *ngIf=\"sSL.value=='on_delivery'\" class=\"awb-list\">\r\n                                                <div class=\"input-delivery-container\" >\r\n                                                    <input\r\n                                                        *ngIf=\"shippingStatusList[1].checked == true && deliveryHistoryDetail.delivery_detail.courier == 'others'\"\r\n                                                        name=\"courier_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.delivery_detail.courier_name\"\r\n                                                        disabled />\r\n\r\n                                                        <input\r\n                                                        *ngIf=\"shippingStatusList[1].checked == true && deliveryHistoryDetail.delivery_detail.courier != 'others'\"\r\n                                                        name=\"courier\" class=\"form-control awb\" type=\"text\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.delivery_detail.courier\"\r\n                                                        disabled />\r\n                                                \r\n                                                    <input\r\n                                                        *ngIf=\"shippingStatusList[1].checked == true\"\r\n                                                        name=\"delivery_service\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"empty delivery service\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.delivery_detail.delivery_service\"\r\n                                                        disabled />\r\n                                                \r\n                                                    <input\r\n                                                        *ngIf=\"shippingStatusList[1].checked == true\"\r\n                                                        name=\"delivery_method\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"empty delivery method\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.delivery_detail.delivery_method\"\r\n                                                        disabled />\r\n                                                    \r\n                                                    <input\r\n                                                        *ngIf=\"shippingStatusList[1].checked == true\"\r\n                                                        name=\"awb_number\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"empty\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.delivery_detail.awb_number\"\r\n                                                        disabled />\r\n\r\n                                                    <input\r\n                                                        *ngIf=\"shippingStatusList[1].checked == true\"\r\n                                                        name=\"delivery_remarks\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"empty remarks\"\r\n                                                        [(ngModel)]=\"delivery_remarks\"\r\n                                                        disabled />\r\n                                                </div>\r\n                                            </div>\r\n\r\n                                            <div>\r\n                                                <button *ngIf=\"sSL.value=='on_delivery' && deliveryHistoryDetail && deliveryHistoryDetail.delivery_detail && deliveryHistoryDetail.delivery_detail.track_history && isArray(deliveryHistoryDetail.delivery_detail.track_history) && deliveryHistoryDetail.delivery_detail.track_history.length > 0\" class=\"btn btn-outline-primary mb-2 mr-2\" (click)=\"openScrollableContent(longContent)\"><i class=\"fa fa-truck-pickup\"></i>Tracking Info Detail</button>\r\n                                            </div>\r\n\r\n                                            <div *ngIf=\"sSL.value=='delivered'\" class=\"awb-list\">\r\n                                                <div class=\"input-delivery-container\" >\r\n                                                    <label>\r\n                                                       <small> Nama Penerima</small>\r\n                                                    </label>\r\n                                                \r\n                                                    <input\r\n                                                        name=\"delivery_method\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"empty delivery method\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.shipping_info[i].receiver_name\"\r\n                                                        disabled />\r\n\r\n                                                    <label>\r\n                                                        <small>Status Penerima</small>\r\n                                                    </label>\r\n                                                    \r\n                                                    <input\r\n                                                        name=\"awb_number\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"empty\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.shipping_info[i].delivery_status\"\r\n                                                        disabled />\r\n\r\n                                                    <label>\r\n                                                        <small>Remarks</small>\r\n                                                    </label>\r\n\r\n                                                    <input\r\n                                                        name=\"delivery_remarks\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"empty remarks\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.shipping_info[i].remarks\"\r\n                                                        disabled />\r\n                                                </div>                             \r\n                                                \r\n                                            </div>\r\n\r\n                                        </label>\r\n\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services=='no-services'\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <div class=\"radio-button checked-false hovered-false\">\r\n                                        </div>\r\n                                        <label>\r\n                                            {{sSL.label}}<br />\r\n                                            <span class=\"shipping-date\"\r\n                                                *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                        </label>\r\n                                    </div>\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n\r\n                            <div class=\"printLabel\" *ngIf=\"deliveryHistoryDetail.billings != undefined\">\r\n                                <button class=\"btn btn-primary\" (click)=\"openLabel(historyID)\">Open Label</button>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    <!-- EDIT SHIPPING INFO -->\r\n                    <!-- <div *ngIf=\"deliveryHistoryDetail.status=='PAID' && currentPermission == 'merchant' && editThis || deliveryHistoryDetail.status=='PAID' && currentPermission == 'admin' && editThis\"\r\n                        class=\"card mb-3 \">\r\n                        <div class=\"card-header\">\r\n                            <h2>Shipping Information </h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\" *ngIf=\" !deliveryHistoryDetail.shipping_info\">\r\n                                <label>no shipping info</label>\r\n                            </div>\r\n                            <div class=\"col-md-12\" *ngIf=\"deliveryHistoryDetail.shipping_info\">\r\n                                <label>Delivery Service :\r\n                                    <strong><span *ngIf=\"deliveryHistoryDetail.shipping\" style=\"color:#0EA8DB\"><span *ngIf=\"deliveryHistoryDetail.shipping[1]\">{{deliveryHistoryDetail.shipping[1].service}}\r\n                                        ({{deliveryHistoryDetail.delivery.delivery_detail.description}})</span></span></strong></label>\r\n                                <div class=\"line\" style=\"width:100%; border-top: 1.5px solid #eaeaea; \"></div>\r\n                                <p style=\"margin-bottom:30px; margin-top:15px;\">Shipping Status</p>\r\n                                <p style=\"font-size: 12px; margin-bottom:20px; font-weight: bold;\">Click on each button\r\n                                    to process your shipping</p>\r\n\r\n\r\n                                <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services'\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <div class=\"radio-button checked-{{sSL.checked}} hovered-{{sSL.hovered}}\"\r\n                                            (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusL//// ist(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                        </div>\r\n                                        <label (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                            {{sSL.label}}<br />\r\n                                            <span class=\"shipping-date\"\r\n                                                *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                            <div class=\"form-group\" *ngIf=\"sSL.value == 'on_packaging'\">\r\n                                                <div\r\n                                                    *ngIf=\"deliveryHistoryDetail.billings != undefined && deliveryHistoryDetail.courier == undefined\">\r\n                                                    <select *ngIf=\"!packaging\" disabled class=\"form-control\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.shipping_services\">\r\n                                                        <option *ngFor=\"let ss of shippingServices\"\r\n                                                            [ngValue]=\"ss.value\">\r\n                                                            {{ss.label}} <i class=\"fas fa-pencil-alt\"></i></option>\r\n                                                    </select>\r\n\r\n                                                    <select *ngIf=\"packaging\" class=\"form-control\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.shipping_services\">\r\n                                                        <option *ngFor=\"let ss of shippingServices\"\r\n                                                            [ngValue]=\"ss.value\">\r\n                                                            {{ss.label}} <i class=\"fas fa-pencil-alt\"></i> </option>\r\n                                                    </select>\r\n                                                </div>\r\n                                                <div *ngIf=\"deliveryHistoryDetail.billings == undefined && deliveryHistoryDetail.courier == undefined\"\r\n                                                    class=\"awb-list\">\r\n                                                    <select *ngIf=\"!packaging\" disabled class=\"form-control\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.shipping_services\">\r\n                                                        <option *ngFor=\"let ss of courier_list\"\r\n                                                            [ngValue]=\"ss.courier_code\">\r\n                                                            {{ss.courier}} </option>\r\n                                                        <option [ngValue]=\"'other'\">\r\n                                                            Another Courier\r\n                                                        </option>\r\n                                                    </select>\r\n                                                    <select *ngIf=\"packaging\" class=\"form-control\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.shipping_services\"\r\n                                                        (change)=\"other_cour()\">\r\n                                                        <option *ngFor=\"let ss of courier_list\"\r\n                                                            [ngValue]=\"ss.courier_code\">\r\n                                                            {{ss.courier}}</option>\r\n                                                        <option [ngValue]=\"'other'\">\r\n                                                            Another Courier\r\n                                                        </option>\r\n                                                    </select>\r\n\r\n                                                    <button class=\"btn btn-primary btn-sm process\"\r\n                                                        *ngIf=\"packaging && choose_service != 'PICKUP' && supported\"\r\n                                                        (click)=\"saveThis()\">Process</button>\r\n                                                    <button class=\"btn btn-primary btn-sm process\"\r\n                                                        *ngIf=\"packaging && choose_service == 'PICKUP' && supported\"\r\n                                                        (click)=\"saveThis()\" style=\"width: fit-content;\"> Request Pickup\r\n                                                        Now </button>\r\n                                                    <button class=\"btn btn-primary btn-sm process\"\r\n                                                        *ngIf=\"packaging && !supported\"\r\n                                                        (click)=\"saveThis()\">Process</button>\r\n\r\n                                                </div>\r\n                                                <div *ngIf=\"deliveryHistoryDetail.billings == undefined && deliveryHistoryDetail.courier == undefined && othercour\"\r\n                                                    class=\"awb-list\">\r\n                                                    <input name=\"courier\" class=\"form-control awb\" [type]=\"'text'\"\r\n                                                        [placeholder]=\"'Input Courier Service'\"\r\n                                                        [(ngModel)]=\"otherCourValue\" required>\r\n                                                </div>\r\n\r\n                                                <div *ngIf=\"deliveryHistoryDetail.billings == undefined && deliveryHistoryDetail.courier\"\r\n                                                    class=\"awb-list\">\r\n                                                    <div *ngIf=\"deliveryHistoryDetail.billings == undefined && deliveryHistoryDetail.courier\"\r\n                                                    class=\"awb-list\">\r\n                                                    <input name=\"courier\" class=\"form-control awb\" [type]=\"'text'\"\r\n                                                        \r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.courier.courier\" disabled>\r\n                                                </div>\r\n\r\n\r\n                                                </div>\r\n                                            </div>\r\n                                            <div *ngIf=\"sSL.value=='on_delivery'\" class=\"awb-list\">\r\n                                                <div *ngIf=\"deliveryHistoryDetail.courier == undefined\" class=\"input-form\">\r\n                                                    <input\r\n                                                        *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services' && shippingStatusList[2].checked == true\"\r\n                                                        name=\"awb_receipt\" class=\"form-control awb\" [type]=\"'text'\"\r\n                                                        [placeholder]=\"'Put AWB / receipt Number'\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.awb_receipt\" autofocus\r\n                                                        required />\r\n                                                    <input\r\n                                                        *ngIf=\"deliveryHistoryDetail.shipping_services=='no-services' || shippingStatusList[2].checked == false \"\r\n                                                        class=\"form-control awb\" disabled name=\"unnamed\"\r\n                                                        [placeholder]=\"'Put AWB / receipt Number'\"\r\n                                                        title=\"You can add AWB after choosing the services\" />\r\n                                                </div>\r\n                                                <div *ngIf=\"deliveryHistoryDetail.courier != undefined\" class=\"input-form\">\r\n                                                    <ng-container *ngIf=\"deliveryHistoryDetail.delivery\">\r\n                                                    <div\r\n                                                    *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services' && shippingStatusList[2].checked == true && deliveryHistoryDetail.courier.supported != 1 && deliveryHistoryDetail.delivery.delivery_detail.service == 'INSTANT'\">\r\n                                                    <div><label>Please choose a courier service:</label></div>\r\n                                                </div>\r\n                                            </ng-container>\r\n                                                    <input\r\n                                                        *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services' && shippingStatusList[2].checked == true  && deliveryHistoryDetail.courier.supported != 1\"\r\n                                                        name=\"awb_receipt\" class=\"form-control awb\" [type]=\"'text'\"\r\n                                                        [placeholder]=\"'Put AWBsddsf / receipt Number'\" (change)=\"onAWBchange()\"\r\n                                                        [(ngModel)]=\"deliveryHistoryDetail.courier.awb_number\" autofocus\r\n                                                        required />\r\n                                                      \r\n                                                    <div\r\n                                                        *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services' && shippingStatusList[2].checked == true && deliveryHistoryDetail.courier.supported == 1\">\r\n                                                        <div><label>Please choose a courier service:</label></div>\r\n                                                    </div>\r\n                                                  \r\n                                                    \r\n                                                </div>\r\n                                                    <button class=\"btn btn-primary btn-sm process\"\r\n                                                    *ngIf=\"!deliveryHistoryDetail.delivery && shippingStatusList[2].checked == true \"\r\n                                                    (click)=\"saveThis()\">Process</button>\r\n                                                    <ng-container *ngIf=\"deliveryHistoryDetail.delivery\">\r\n                                                    <span *ngIf=\"deliveryHistoryDetail.delivery.delivery_detail.service != 'INSTANT'\">\r\n                                                    <button class=\"btn btn-primary btn-sm process\"\r\n                                                    *ngIf=\"choose_service != 'PICKUP' && shippingStatusList[2].checked == true \"\r\n                                                    (click)=\"saveThis()\">Process</button>\r\n                                                    \r\n                                                <button class=\"btn btn-primary btn-sm process\"\r\n                                                    *ngIf=\"choose_service == 'PICKUP' && shippingStatusList[2].checked == true \"\r\n                                                    (click)=\"saveThis()\" style=\"width: fit-content;\"> Request Pickup\r\n                                                    Now </button>\r\n                                                    </span>\r\n                                                <span *ngIf=\"deliveryHistoryDetail.delivery.delivery_detail.service == 'INSTANT'\">\r\n                                                    \r\n                                                <button class=\"btn btn-primary btn-sm process\"\r\n                                                    *ngIf=\"choose_service == 'PICKUP' && shippingStatusList[2].checked == true\"\r\n                                                    [class.btn-secondary]=\"!awb_check\"\r\n                                                    [disabled]=\"!awb_check\"\r\n                                                    (click)=\"saveThis()\" style=\"width: fit-content;\"> Request Pickup\r\n                                                    Now </button>\r\n                                                </span>\r\n                                            </ng-container>\r\n                                            </div>\r\n                                        </label>\r\n\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services=='no-services'\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <div class=\"radio-button checked-false hovered-false\">\r\n                                        </div>\r\n                                        <label>\r\n                                            {{sSL.label}}<br/>\r\n                                            <span class=\"shipping-date\"\r\n                                                *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                        </label>\r\n                                    </div>\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n\r\n                            <div class=\"printLabel\" *ngIf=\"deliveryHistoryDetail.billings != undefined\">\r\n                                <button class=\"btn btn-primary\" (click)=\"openLabel(historyID)\">Open Label</button>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    <!-- <div  *ngIf=\"isEverDelivered\"\r\n                        class=\"card mb-3 \">\r\n                        <div class=\"card-header\">\r\n                            <h2>Return Process </h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\" *ngIf=\"deliveryHistoryDetail.shipping_info\">\r\n                                <div *ngIf=\"isRedeliver == false && isProcessRedelivery == false && isRedeliveryCompleted == false && isReorder == false\">\r\n                                    <p style=\"font-size: 12px; margin-bottom:20px; font-weight: bold;\">Please select reorder or redeliver</p>\r\n                                    <select class=\"form-control\" [(ngModel)]=\"returnType\">\r\n                                        <option value=\"redeliver\">\r\n                                            REDELIVER\r\n                                        </option>\r\n                                        <option value=\"reorder\">\r\n                                            REORDER\r\n                                        </option>\r\n                                    </select>\r\n                                </div>\r\n\r\n\r\n                                <div *ngIf=\"isRedeliver == true || isProcessRedelivery == true || isRedeliveryCompleted == true\">\r\n                                    Redeliver Info\r\n                                </div>\r\n                                <div *ngIf=\"isReorder == true\">\r\n                                    Reorder Info\r\n                                </div>\r\n                                <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services' && returnType == 'redeliver'\" style=\"margin-top:30px;\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of redeliverList; let i = index\">\r\n                                        <div class=\"radio-button checked-{{sSL.checked}} hovered-{{sSL.hovered}}\"\r\n                                            (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusL//// ist(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                        </div>\r\n                                        <label (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\" [ngClass]=\"sSL.value=='redeliver'? 'redelivery-label' : ''\">\r\n                                            {{sSL.label}}<br />\r\n                                            <div *ngIf=\"sSL.value=='redeliver'\" class=\"awb-list return-form\">\r\n                                                <div class=\"information-text\">*proses pengembalian dari pelanggan ke admin, kemudian akan dilakukan pengiriman ulang dengan order yang sama (tanpa harus order ulang)</div>\r\n                                                <div class=\"input-form\">\r\n                                                    <div class=\"input-date-form awb\">\r\n                                                        <form class=\"form-inline\">\r\n                                                            <div class=\"form-group\">\r\n                                                                <div class=\"input-group\">\r\n                                                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                        name=\"dp\" [(ngModel)]=\"return_date\" ngbDatepicker #d=\"ngbDatepicker\" [disabled] = \"isRedeliver == true\">\r\n                                                                <div class=\"input-group-append\">\r\n                                                                    <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\" [disabled] = \"isRedeliver == true\">\r\n                                                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                    </button>\r\n                                                                </div>\r\n                                                                </div>\r\n                                                            </div>\r\n                                                        </form>\r\n                                                    </div>\r\n                                                \r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Kurir\"\r\n                                                        [(ngModel)]=\"return_courier\"\r\n                                                        [disabled] = \"isRedeliver == true\"\r\n                                                        required />\r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"AWB Number\"\r\n                                                        [(ngModel)]=\"return_awb_number\"\r\n                                                        [disabled] = \"isRedeliver == true\"\r\n                                                        required />\r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Remarks\"\r\n                                                        [(ngModel)]=\"return_remarks\"\r\n                                                        [disabled] = \"isRedeliver == true\"\r\n                                                        required />\r\n\r\n\r\n\r\n                                                    <div *ngIf=\"isRedeliver == false\" class=\"button-complete\">\r\n                                                        <div *ngIf=\"redeliverButtonConfirmation\">\r\n                                                            <span>Anda yakin akan memproses order redelivery?</span>\r\n                                                            <button\r\n                                                                *ngIf=\"isChangeRedeliverLoading == false\"\r\n                                                                class=\" btn btn-success\"\r\n                                                                type=\"submit\"\r\n                                                                value=\"Submit\"\r\n                                                                (click)=\"processRedeliver()\"\r\n                                                            >\r\n                                                                Ya\r\n                                                            </button>\r\n                                                            <button *ngIf=\"isChangeRedeliverLoading == true\" class=\"btn rounded-btn open-login\" type=\"submit\" ><i class=\"fa fa-spinner fa-pulse\"></i></button>&nbsp;\r\n                                                            <span>&nbsp;</span>\r\n                                                            <button\r\n                                                                class=\" btn btn-cancel\"\r\n                                                                type=\"submit\"\r\n                                                                value=\"Submit\"\r\n                                                                (click)=\"redeliverButtonConfirmation = false\"\r\n                                                            >\r\n                                                                Tidak\r\n                                                            </button>\r\n                                                        </div>\r\n                                                        <button *ngIf=\"!redeliverButtonConfirmation\"\r\n                                                            class=\" btn btn-success\"\r\n                                                            type=\"submit\"\r\n                                                            value=\"Submit\"\r\n                                                            (click)=\"redeliverButtonConfirmation = true\"\r\n                                                            [disabled]=\"!return_courier || !return_awb_number || !return_remarks || !return_date\"\r\n                                                            >\r\n                                                            Process Redeliver\r\n                                                        </button>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n\r\n                                            <div *ngIf=\"sSL.value=='redelivery' && (isRedeliver == true || isProcessRedelivery == true)\" class=\"awb-list return-form\">\r\n                                                <div class=\"information-text\">*proses pengiriman ulang dari admin ke pelanggan</div>\r\n                                                <div class=\"input-form\">\r\n                                                    <div class=\"input-date-form awb\">\r\n                                                        <form class=\"form-inline\">\r\n                                                            <div class=\"form-group\">\r\n                                                                <div class=\"input-group\">\r\n                                                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                        name=\"dp\" [(ngModel)]=\"redelivery_date\" ngbDatepicker #d=\"ngbDatepicker\" [disabled] = \"isProcessRedelivery == true\">\r\n                                                                <div class=\"input-group-append\">\r\n                                                                    <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\" [disabled] = \"isProcessRedelivery == true\">\r\n                                                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                    </button>\r\n                                                                </div>\r\n                                                                </div>\r\n                                                            </div>\r\n                                                        </form>\r\n                                                    </div>\r\n                                                \r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Kurir\"\r\n                                                        [(ngModel)]=\"redelivery_courier\"\r\n                                                        [disabled] = \"isProcessRedelivery == true\"\r\n                                                        required />\r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"AWB Number\"\r\n                                                        [(ngModel)]=\"redelivery_awb_number\"\r\n                                                        [disabled] = \"isProcessRedelivery == true\"\r\n                                                        required />\r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Delivery Service\"\r\n                                                        [(ngModel)]=\"redelivery_services\"\r\n                                                        [disabled] = \"isProcessRedelivery == true\"\r\n                                                        required />\r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Delivery Method\"\r\n                                                        [(ngModel)]=\"redelivery_method\"\r\n                                                        [disabled] = \"isProcessRedelivery == true\"\r\n                                                        required />\r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Remarks\"\r\n                                                        [(ngModel)]=\"redelivery_remarks\"\r\n                                                        [disabled] = \"isProcessRedelivery == true\"\r\n                                                        required />\r\n\r\n\r\n\r\n                                                    <div *ngIf=\"isProcessRedelivery == false\" class=\"button-complete\">\r\n                                                        <div *ngIf=\"processOrderRedeliveryButtonConfirmation\">\r\n                                                            <span>Anda yakin akan memproses order redelivery?</span>\r\n                                                            <button\r\n                                                                *ngIf=\"isChangeProcessRedeliveryLoading == false\"\r\n                                                                class=\" btn btn-success\"\r\n                                                                type=\"submit\"\r\n                                                                value=\"Submit\"\r\n                                                                (click)=\"processOrderRedelivery()\"\r\n                                                            >\r\n                                                                Ya\r\n                                                            </button>\r\n                                                            <button *ngIf=\"isChangeProcessRedeliveryLoading == true\" class=\"btn rounded-btn open-login\" type=\"submit\" ><i class=\"fa fa-spinner fa-pulse\"></i></button>&nbsp;\r\n                                                            <span>&nbsp;</span>\r\n                                                            <button\r\n                                                                class=\" btn btn-cancel\"\r\n                                                                type=\"submit\"\r\n                                                                value=\"Submit\"\r\n                                                                (click)=\"processOrderRedeliveryButtonConfirmation = false\"\r\n                                                            >\r\n                                                                Tidak\r\n                                                            </button>\r\n                                                        </div>\r\n                                                        <button *ngIf=\"!processOrderRedeliveryButtonConfirmation\"\r\n                                                            class=\" btn btn-success\"\r\n                                                            type=\"submit\"\r\n                                                            value=\"Submit\"\r\n                                                            (click)=\"processOrderRedeliveryButtonConfirmation = true\"\r\n                                                            [disabled]=\"!redelivery_courier || !redelivery_awb_number || !redelivery_services || !redelivery_method || !redelivery_remarks || !redelivery_date\"\r\n                                                            >\r\n                                                            Process Order Redelivery\r\n                                                        </button>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n\r\n                                            <div  *ngIf=\"sSL.value=='delivered' && (isProcessRedelivery == true || isRedeliveryCompleted == true)\" class=\"awb-list return-form\">\r\n                                                <div class=\"input-delivery-container\">\r\n                                                    <div class=\"input-date-form\">\r\n                                                        <form class=\"form-inline\">\r\n                                                            <div class=\"form-group\">\r\n                                                                <div class=\"input-group\">\r\n                                                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                        name=\"dp\" [(ngModel)]=\"date\" ngbDatepicker [footerTemplate]=\"footerTemplate\" #d=\"ngbDatepicker\" [disabled] = \"isRedeliveryCompleted == true\">\r\n                                                                <div class=\"input-group-append\">\r\n                                                                    <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\" [disabled] = \"isRedeliveryCompleted == true\">\r\n                                                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                    </button>\r\n                                                                </div>\r\n                                                                </div>\r\n                                                            </div>\r\n                                                        </form>\r\n                                                        <ngb-timepicker [(ngModel)]=\"time\" [seconds]=\"true\" [disabled] = \"isRedeliveryCompleted == true\"></ngb-timepicker>\r\n                                                    </div>\r\n                                                \r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Masukan nama penerima\"\r\n                                                        [(ngModel)]=\"receiver_name\"\r\n                                                        [disabled] = \"isRedeliveryCompleted == true\"\r\n                                                        required />\r\n                                                \r\n                                                    <input\r\n                                                        name=\"delivery_status\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Masukan status penerima\"\r\n                                                        [(ngModel)]=\"delivery_status\"\r\n                                                        [disabled] = \"isRedeliveryCompleted == true\"\r\n                                                        required />\r\n                                                    \r\n                                                    <input\r\n                                                        name=\"remark\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Masukan remark\"\r\n                                                        [(ngModel)]=\"remark\"\r\n                                                        [disabled] = \"isRedeliveryCompleted == true\"\r\n                                                        required />\r\n\r\n\r\n                                                    <div *ngIf=\"isRedeliveryCompleted == false\" class=\"button-complete\">\r\n                                                        <div *ngIf=\"completeButtonConfirmation\">\r\n                                                            <span>Anda yakin akan mengubah menjadi complete?</span>\r\n                                                            <button\r\n                                                                *ngIf=\"isChangeLoading == false\"\r\n                                                                class=\" btn btn-success\"\r\n                                                                type=\"submit\"\r\n                                                                value=\"Submit\"\r\n                                                                (click)=\"updateComplete()\"\r\n                                                            >\r\n                                                                Ya\r\n                                                            </button>\r\n                                                            <button *ngIf=\"isChangeLoading == true\" class=\"btn rounded-btn open-login\" type=\"submit\" ><i class=\"fa fa-spinner fa-pulse\"></i></button>&nbsp;\r\n                                                            <span>&nbsp;</span>\r\n                                                            <button\r\n                                                                class=\" btn btn-cancel\"\r\n                                                                type=\"submit\"\r\n                                                                value=\"Submit\"\r\n                                                                (click)=\"completeButtonConfirmation = false\"\r\n                                                            >\r\n                                                                Tidak\r\n                                                            </button>\r\n                                                        </div>\r\n                                                        <button *ngIf=\"!completeButtonConfirmation\"\r\n                                                            class=\" btn btn-success\"\r\n                                                            type=\"submit\"\r\n                                                            value=\"Submit\"\r\n                                                            (click)=\"completeButtonConfirmation = true\"\r\n                                                            [disabled]=\"!receiver_name || !delivery_status || !remark || !date || !time\"\r\n                                                            >\r\n                                                            Completed this Order\r\n                                                        </button>\r\n                                                    </div>\r\n\r\n                                                    <div *ngIf=\"isCompleteOrder == true\" class=\"order-complete-text\">\r\n                                                        Order Completed\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                        </label>\r\n\r\n                                    </div>\r\n                                </div>\r\n\r\n\r\n                                <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services' && returnType == 'reorder'\" style=\"margin-top:30px;\">\r\n                                    <div class=\"shipping-status\">\r\n                                        <div class=\"radio-button checked-true hovered-true\"\r\n                                            (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusL//// ist(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                        </div>\r\n                                        <label (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                            Reorder Process<br />\r\n                                            <div class=\"awb-list return-form\">\r\n                                                <div class=\"information-text\">*proses pengembalian dari pelanggan ke admin (status akan menjadi cancel), kemudian dilakukan order ulang</div>\r\n                                                <div class=\"input-form\">\r\n                                                    <div class=\"input-date-form awb\">\r\n                                                        <form class=\"form-inline\">\r\n                                                            <div class=\"form-group\">\r\n                                                                <div class=\"input-group\">\r\n                                                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                        name=\"dp\" [(ngModel)]=\"reorder_date\" ngbDatepicker #d=\"ngbDatepicker\" [disabled] = \"isReorder == true\">\r\n                                                                <div class=\"input-group-append\">\r\n                                                                    <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\" [disabled] = \"isReorder == true\">\r\n                                                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                    </button>\r\n                                                                </div>\r\n                                                                </div>\r\n                                                            </div>\r\n                                                        </form>\r\n                                                    </div>\r\n                                                \r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Kurir\"\r\n                                                        [(ngModel)]=\"reorder_courier\"\r\n                                                        [disabled] = \"isReorder == true\"\r\n                                                        required />\r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"AWB Number\"\r\n                                                        [(ngModel)]=\"reorder_awb_number\"\r\n                                                        [disabled] = \"isReorder == true\"\r\n                                                        required />\r\n                                                    <input\r\n                                                        name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                        placeholder=\"Remarks\"\r\n                                                        [(ngModel)]=\"reorder_remarks\"\r\n                                                        [disabled] = \"isReorder == true\"\r\n                                                        required />\r\n\r\n\r\n\r\n                                                    <div *ngIf=\"isReorder == false\" class=\"button-complete\">\r\n                                                        <div *ngIf=\"reorderButtonConfirmation\">\r\n                                                            <span>Anda yakin akan memproses reorder?</span>\r\n                                                            <button\r\n                                                                *ngIf=\"isChangeReorderLoading == false\"\r\n                                                                class=\" btn btn-success\"\r\n                                                                type=\"submit\"\r\n                                                                value=\"Submit\"\r\n                                                                (click)=\"processReorder()\"\r\n                                                            >\r\n                                                                Ya\r\n                                                            </button>\r\n                                                            <button *ngIf=\"isChangeReorderLoading == true\" class=\"btn rounded-btn open-login\" type=\"submit\" ><i class=\"fa fa-spinner fa-pulse\"></i></button>&nbsp;\r\n                                                            <span>&nbsp;</span>\r\n                                                            <button\r\n                                                                class=\" btn btn-cancel\"\r\n                                                                type=\"submit\"\r\n                                                                value=\"Submit\"\r\n                                                                (click)=\"reorderButtonConfirmation = false\"\r\n                                                            >\r\n                                                                Tidak\r\n                                                            </button>\r\n                                                        </div>\r\n                                                        <button *ngIf=\"!reorderButtonConfirmation\"\r\n                                                            class=\" btn btn-success\"\r\n                                                            type=\"submit\"\r\n                                                            value=\"Submit\"\r\n                                                            (click)=\"reorderButtonConfirmation = true\"\r\n                                                            [disabled]=\"!reorder_courier || !reorder_awb_number || !reorder_remarks || !reorder_date\"\r\n                                                            >\r\n                                                            Process Reorder\r\n                                                        </button>\r\n                                                    </div>\r\n\r\n\r\n\r\n\r\n                                                </div>\r\n                                            </div>\r\n                                        </label>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div> -->\r\n\r\n\r\n\r\n\r\n\r\n\r\n                    <!-- <div class=\"btn btn-danger\" style=\"width: 100%; padding:10px;\" (click)=\"openNote(historyID)\"\r\n                        *ngIf=\"(deliveryHistoryDetail.status == 'PAID' || deliveryHistoryDetail.status == 'WAITING FOR CONFIRMATION' || deliveryHistoryDetail.status == 'PENDING') && currentPermission == 'merchant' || currentPermission == 'admin'\">\r\n                        Cancel Order</div> -->\r\n\r\n\r\n\r\n\r\n\r\n\r\n                    <!-- <div id=\"myModal\" class=\"modal {{openNotes}}\">\r\n                        <div class=\"modal-content\">\r\n                            <div class=\"modal-header\">\r\n                                <span class=\"close\" (click)=\"noteClose()\">&times;</span>\r\n                            </div>\r\n                            <div class=\"modal-body\">\r\n                                <h3> Why cancel your order? </h3>\r\n                                <br />\r\n\r\n                                <div class=\"form-check\" *ngFor=\"let notes of notesCategory let i = index\">\r\n                                    <input class=\"form-check-input\" type=\"radio\" id=\"radio1\" name=\"radio\"\r\n                                        (click)=\"clickCategory(i)\">\r\n                                    <label class=\"form-check-label\" for=\"radio1\">{{notes.label}}</label>\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n                            <br />\r\n                            <div class=\"modal-body\">\r\n                                <textarea placeholder=\"Write your reason here...\" rows=\"4\" cols=\"50\"\r\n                                    [(ngModel)]=\"notes\"></textarea>\r\n\r\n                            </div>\r\n                            <div class=\"modal-body\">\r\n                                <button class=\"btn btn-primary\" (click)=\"cancelOrders()\">Submit</button>\r\n                            </div>\r\n                            <div class=\"modal-footer\">\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div id=\"myModal2\" class=\"modal {{openNotes2}}\">\r\n                        <div class=\"modal-content2 card-content\">\r\n                            <div class=\"modal-header\">\r\n                                <span class=\"close\" (click)=\"noteClose()\">&times;</span>\r\n                            </div>\r\n                            <div class=\"modal-body2\">\r\n                                <h4 style=\"text-align: left;\"><strong>Apakah Anda yakin untuk melakukan \"HOLD\"\r\n                                        dana?</strong> </h4>\r\n\r\n                                <br />\r\n                                <span style=\"float:left\"><span class=\"red\">*</span>Note wajib diisi</span>\r\n                                <br />\r\n                                <textarea class=\"form-control mt-2\" (click)=\"errorLabel= false\"\r\n                                    placeholder=\"Write your reason here...\" [(ngModel)]=\"notes\"></textarea>\r\n                            </div>\r\n                            <div class=\"modal-body2 mt-2\">\r\n                                <span style=\"float:left\"><strong><span class=\"red\">*</span>Nominal</strong> </span>\r\n                                <br />\r\n\r\n                                <div class=\"mt-2 form-group container-nominal \">\r\n                                    <label style=\"width: 5%;\">Rp.</label>\r\n                                    <input class=\"form-control\" type=\"number\" (click)=\"errorLabel= false\"\r\n                                        [(ngModel)]=\"holdvalue\" style=\"width: 95%\" />\r\n                                </div>\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel\">\r\n                                <span style=\"color: red;\">{{errorLabel}}</span>\r\n                            </div>\r\n                            <div class=\"modal-buttons mt-4\">\r\n                                <button class=\"common-button half-10 cancel-hold\" (click)=\"noteClose()\">Cancel</button>\r\n                                <button class=\"common-button half-10 confirm-hold\" (click)=\"hold()\">Hold</button>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div> -->\r\n\r\n\r\n\r\n                    <div *ngIf=\"deliveryHistoryDetail.status == 'CANCEL' && currentPermission == 'merchant' || deliveryHistoryDetail.status == 'CANCEL' && currentPermission == 'admin'\"\r\n                        class=\"cancel-order\">\r\n                        <div class=\"card mb-3\">\r\n                            <label class=\"cancel-status\">Status : <span style=\"color:red;\">Order Cancelled</span></label>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <!-- END OF MERCHANT SHIPPING INFO -->\r\n\r\n                    <!-- <div class=\"card-header\">\r\n                        <h2>Payment Slip</h2>\r\n                    </div>\r\n                    <div class=\"card-content\">\r\n                    <div *ngIf=\"deliveryHistoryDetail.status =='WAITING FOR CONFIRMATION'\">\r\n                            <button class= \"btn btn-default\" style=\"width: 100%;\" (click)=\"receipt()\" target=\"_blank\">Print Receipt</button>\r\n                    </div> -->\r\n                </div>\r\n                <!-- <div class=\"card mb-3\">\r\n                     \r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Email </label>\r\n                            <form-input name=\"email\" [type]=\"'text'\" [placeholder]=\"'Email'\"  [(ngModel)]=\"deliveryHistoryDetail.email\" autofocus required></form-input>\r\n\r\n                            <label>Cell Phone </label>\r\n                            <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Cell Phone'\"  [(ngModel)]=\"deliveryHistoryDetail.cell_phone\" autofocus required></form-input>\r\n                            \r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>  -->\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div *ngIf=\"!(deliveryHistoryDetail && orderDetail)\" class=\"default-loading\">\r\n        <i class=\"fa fa-spinner fa-pulse\"></i>\r\n    </div>\r\n\r\n</div>\r\n\r\n<ng-template #longContent let-modal>\r\n    <div class=\"outer\" *ngIf=\"deliveryHistoryDetail && deliveryHistoryDetail.delivery_detail && deliveryHistoryDetail.delivery_detail.track_history\">\r\n        <div class=\"progress\">\r\n            <div class=\"left\">\r\n                <div *ngFor=\"let track of deliveryHistoryDetail.delivery_detail.track_history; let i = index\">\r\n                    <div class=\"created-date\">{{track.create_date}}</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"right\">\r\n                <div *ngFor=\"let track of deliveryHistoryDetail.delivery_detail.track_history; let i = index\" [ngClass]=\"i == deliveryHistoryDetail.delivery_detail.track_history.length - 1? 'current' : ''\">\r\n                    <div class=\"rowstate-name\">{{track.rowstate_name}}</div>\r\n                    <!-- <div class=\"created-date\">{{track.create_date}}</div> -->\r\n                    <div class=\"description\" *ngIf=\"track.description\">{{track.description}}</div>\r\n                </div>\r\n            </div>\r\n        </div>  \r\n    </div>\r\n</ng-template>\r\n\r\n<!-- <div *ngIf=\"shippingDetail\">\r\n    <app-delivery-process-shipping [back]=\"[this,backHere]\" [detail]=\"shippingDetail\" [propsDetail]=\"propsDetail\" class=\"parent-container\"></app-delivery-process-shipping>\r\n</div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.html ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 id=\"header\" class=\"header-title\"><strong>Shipping Label</strong></h1>\r\n<div class=\"btn-print\">\r\n    <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n    <button mat-raised-button color=\"primary\" class=\"btn btn-primary\" (click)=\"prints()\">Print Label<i\r\n            class=\"fa fa-print\" aria-hidden=\"true\"></i></button>\r\n</div>\r\n<div class=\"notif error-message\">{{errorMessage}}</div>\r\n<div class=\"loading\" *ngIf='loading'>\r\n    <div class=\"sk-fading-circle\">\r\n      <div class=\"sk-circle1 sk-circle\"></div>\r\n      <div class=\"sk-circle2 sk-circle\"></div>\r\n      <div class=\"sk-circle3 sk-circle\"></div>\r\n      <div class=\"sk-circle4 sk-circle\"></div>\r\n      <div class=\"sk-circle5 sk-circle\"></div>\r\n      <div class=\"sk-circle6 sk-circle\"></div>\r\n      <div class=\"sk-circle7 sk-circle\"></div>\r\n      <div class=\"sk-circle8 sk-circle\"></div>\r\n      <div class=\"sk-circle9 sk-circle\"></div>\r\n      <div class=\"sk-circle10 sk-circle\"></div>\r\n      <div class=\"sk-circle11 sk-circle\"></div>\r\n      <div class=\"sk-circle12 sk-circle\"></div>\r\n    </div>\r\n  </div>\r\n<div class=\"page-break\" *ngIf=\"!loading\">\r\n    <div class=\"_container\" *ngFor=\"let data of totalResult; let i = index\">\r\n        <div class=\"content\">\r\n            <div class=\"table-order\">\r\n                <table class=\"header\">\r\n                    <tr>\r\n                        <td class=\"locard-logo\">\r\n                            <!-- <img src=\"assets/images/logo.png\" class=\"logo-avatar\" /><br> -->\r\n                            <p>\r\n                                <strong>PT. CLS System</strong><br>\r\n                                Jl. Kebon Jeruk VII No.2E, Maphar Taman Sari<br>\r\n                                Jakarta Barat 11160, Indonesia.<br>\r\n                                Tel:+62 21 2268 6277\r\n                            </p>\r\n                            \r\n                        </td>\r\n                        <td class=\"awb\" style=\"text-align: center;\">\r\n                            <!-- <p>AWB</p> -->\r\n                            <label style=\"font-size:14px;\"><strong>{{data.awb_number}}</strong></label>\r\n                            <ngx-barcode [bc-value]=\"data.awb_number ? data.awb_number : data.reference_no\" [bc-display-value]=\"false\" [bc-width]=\"1.2\"\r\n                                [bc-height]=\"60\" [bc-font-size]=\"10\">\r\n                            </ngx-barcode>\r\n                        </td>\r\n                        <!-- <td *ngIf=\"same\" class=\"locard-logo\">\r\n                            <img src=\"{{courier_image}}\" class=\"logo-avatar\" />\r\n                        </td>\r\n                        <td *ngIf=\"!same\" class=\"locard-logo\">\r\n                            <span style=\"font-size: 20px;\">{{data.courier.courier}}</span>\r\n                        </td> -->\r\n                        <!-- <td class=\"locard-logo\">\r\n                            <img *ngIf=\"courier_image_array[i].same == true\" src=\"{{courier_image_array[i].image}}\" class=\"logo-avatar\" />\r\n                            <span *ngIf=\"courier_image_array[i].same == false\" style=\"font-size: 20px;\">{{courier_image_array[i].courier}}</span>\r\n                        </td> -->\r\n                    </tr>\r\n                </table>\r\n                <table class=\"additional-info-header\">\r\n                    <tr class=\"row-additional-info\" *ngIf=\"data.additional_info && data.additional_info.origin_branch && data.additional_info.dest_branch\">\r\n                        <td>{{data.additional_info.origin_branch}}-{{data.additional_info.dest_branch}}</td>\r\n                        <td>NON COD</td>\r\n                        <td *ngIf=\"data.courier == 'REALS-SAP'\">AWB API</td>\r\n                    </tr>\r\n                </table>\r\n                <table align=\"center\" class=\"detail-header\">\r\n                    <div class=\"row row-detail-shipping\">\r\n                        <div class=\" detail-person\">\r\n                            <!-- <div id=\"order\">\r\n                            <strong>Order ID : {{data.order_id}} </strong> <br />\r\n                            </div> -->\r\n                            <div class=\"row sender\">\r\n                                <div style=\"padding: 0;width: 100%;\">\r\n                                    <p>\r\n                                        <tr style=\"border: none;\">\r\n                                            <td class=\"table-no-border td-fit\"><strong>PENGIRIM:</strong></td>\r\n                                            <td *ngIf=\"mci_project\" class=\"table-no-border\">PT. CLS SYSTEM</td>\r\n                                            <td *ngIf=\"!mci_project\" class=\"table-no-border\">CS {{programTitle}}</td>\r\n                                        </tr>\r\n                                        <!-- <tr style=\"border: none;\" *ngIf=\"!mci_project\">\r\n                                            <td class=\"table-no-border\"><strong>CLIENT:</strong></td>\r\n                                            <td class=\"table-no-border\"><span style=\"text-transform: uppercase;\">{{data.member_detail.cluster}}</span></td>\r\n                                        </tr> -->\r\n                                        <tr style=\"border: none;\">\r\n                                            <td class=\"table-no-border\"><strong>PENERIMA:</strong></td>\r\n                                            <td class=\"table-no-border\">{{data.destination.name}}</td>\r\n                                        </tr>\r\n                                        <!-- <tr style=\"border: none;\" *ngIf=\"!mci_project\">\r\n                                            <td class=\"table-no-border\"><strong>No.KTP:</strong></td>\r\n                                            <td class=\"table-no-border\">{{data.member_detail.ktp_penerima}}</td>\r\n                                        </tr> -->\r\n                                        <tr style=\"border: none;\">\r\n                                            <td class=\"table-no-border\"><strong>ALAMAT:</strong></td>\r\n                                            <!-- <td class=\"table-no-border\" *ngIf=\"!mci_project\">{{data.member_detail.alamat_rumah}}</td>\r\n                                            <td class=\"table-no-border\" *ngIf=\"mci_project\">{{data.member_detail.alamat_rumah}},{{data.member_detail.kota_rumah}},{{data.member_detail.kode_pos_rumah}}</td> -->\r\n                                            <td class=\"table-no-border\">{{data.destination.address}}, {{data.destination.province_name}}, {{data.destination.city_name}}, {{data.destination.district_name}}, {{data.destination.village_name}}, {{data.destination.postal_code}}</td>\r\n                                        </tr>\r\n                                        <tr style=\"border: none;\">\r\n                                            <td class=\"table-no-border\"><strong>HP/TEL:</strong></td>\r\n                                            <td class=\"table-no-border\">{{data.destination.cell_phone}}</td>\r\n                                        </tr>\r\n                                    </p>\r\n                                </div>\r\n                                <!-- <div style=\"padding: 0;width: 50%;\">\r\n                                    <p><strong>Penerima : </strong><br>\r\n                                        {{data.member_detail.nama_penerima}}<br>\r\n                                        {{data.member_detail.alamat_rumah}}<br>\r\n                                        {{data.member_detail.no_wa_penerima}}<br>\r\n                                    </p>\r\n                                </div> -->\r\n                            </div>\r\n                            <!-- <div class=\"receiver\"></div> -->\r\n                            <!-- <div class=\"send\">\r\n                            <label id=\"non\">NON-COD</label> <br />\r\n                            <label id=\"shipping\">{{data.shipping.service}}</label>\r\n                            </div> -->\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\" detail-order\">\r\n                                <p>\r\n                                    <strong>Tanggal: </strong>\r\n                                    {{data.created_date}}<br>\r\n                                    <strong>No. Reference : </strong>\r\n                                    {{data.reference_no}}<br>\r\n                                    <strong>Courier: </strong>\r\n                                    {{data.courier_name}}<br>\r\n                                    <strong>Service: </strong>\r\n                                    <span *ngIf=\"data.delivery_service && data.delivery_method\">\r\n                                        {{data.delivery_service + \" (\" + data.delivery_method + \")\"}}\r\n                                    </span>\r\n                                    <br>\r\n                                    <strong>Pieces: </strong>\r\n                                    {{data.total_item_qty}}<br>\r\n                                    <strong>Weight: </strong>\r\n                                    {{data.weight}}\r\n                                    <br>\r\n                                    <strong>Instruksi Khusus: </strong>\r\n                                    <br>\r\n                                    \r\n                                </p>\r\n                            </div>\r\n                            <div class=\" detail-order\">\r\n                                <p>\r\n                                    <strong>Nama Penerima: </strong>\r\n                                    <br>\r\n                                    <strong>Tgl. Terima: </strong>\r\n                                    <br>\r\n                                    <!-- <strong *ngIf=\"!mci_project\">No.KTP: </strong>\r\n                                    <br> -->\r\n                                    <strong>Tanda Tangan: </strong>\r\n                                </p>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </table>\r\n                <table align=\"center\" class=\"detail-barang-pengiriman\">\r\n                    <div class=\"row row-eq-height\">\r\n                        <div class=\"detail-barang\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-8 daftar-barang\">\r\n                                    <strong>ITEM: </strong>\r\n                                    <span class=\"data-products\"\r\n                                        *ngFor=\"let b of data.product_list\">{{b.product_name}}({{b.quantity}}),\r\n                                    </span><br>\r\n                                </div>\r\n                                <!-- <div class=\"col-md-6 catatan\">\r\n                                    <label>Terima Kasih sudah berbelanja di locard!</label>\r\n                                </div> -->\r\n                            </div>\r\n                        </div>\r\n                        <!-- <div class=\"detail-pengiriman\">\r\n                            <div class=\"row\" style=\"margin-right: 0px;\">\r\n                                <div class=\"col-md-12 jenis-pengiriman\">\r\n                                    <strong>Catatan: </strong><br>\r\n                                    <span  *ngFor=\"let b of data.products;let i = index\">\r\n                                        {{i+1}}. {{b.note_to_merchant}}\r\n                                    </span>\r\n                                  \r\n                                </div>\r\n                            </div>\r\n                        </div> -->\r\n                    </div>\r\n                </table>\r\n            </div>\r\n            <p class=\"page-number\">\r\n                Page {{i+1}} of {{totalResult.length}}\r\n            </p>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/delivery-process/shipping/delivery-process.shipping.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/delivery-process/shipping/delivery-process.shipping.component.html ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- OPENMOREDETAIL == UNTUK DI ADMIN PORTAL -->\r\n<!-- EDITTHIS = UNTUK DI MERCHANT PORTAL -->\r\n\r\n<!-- Note :\r\n\r\ncurrentPermission untuk ADMIN itu untuk si admin\r\ncurrentPermission untuk MERCHANT itu untuk bagian merchant\r\nkarena kita memakai 1 page yang sama -->\r\n\r\n\r\n\r\n\r\n\r\n<div class=\"card card-detail\">\r\n    <div class=\"card-header\" id=\"header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <!-- <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i>\r\n            <span *ngIf=\"!loading\">Save</span>\r\n            <span *ngIf=\"loading\">Please Wait</span>\r\n        </button> -->\r\n    </div>\r\n    <div class=\"card-content\" *ngIf=\"deliveryHistoryDetail\">\r\n        <div class=\"orderhistoryallhistory-detail\">\r\n\r\n            <div class=\"row\">\r\n                <!-- <div *ngIf=\"errorLabel!==false\">\r\n                    {{errorLabel}}\r\n                </div> -->\r\n                <div class=\" col-md-8\">\r\n                    <div>\r\n\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <!-- <button class=\"btn btn-outline-primary btn-sm\" *ngIf=\"openMoreDetail\"\r\n                                    (click)=\"backToList()\">Back</button> -->\r\n                                <div class=\"form-group head-one\">\r\n                                    <label class=\"order-id\"><strong><span style=\"color:#0ea8db; font-weight: bold;\">No. Referensi : </span> {{deliveryHistoryDetail.reference_no}}</strong> \r\n                                        <!-- | -->\r\n                                        <!-- <span class=\"buyer-name\" style=\"color:#0EA8DB\"><strong>\r\n                                            {{detail.member_detail.id_pel}}</strong></span> -->\r\n                                    </label>\r\n                                    <button class=\"btn_delete cancel-pickup\" (click)=\"cancelShipping()\" *ngIf=\"deliveryHistoryDetail.shipping_info && isArray(deliveryHistoryDetail.shipping_info) && deliveryHistoryDetail.shipping_info.length > 0 && deliveryHistoryDetail.shipping_info.at(-1).label == 'on_delivery'\">\r\n                                        <i class=\"fa fa-fw fa-times-circle\"></i>\r\n                                        <span>Cancel Pickup</span>\r\n                                    </button>\r\n                                </div>\r\n                                <div class=\"row process-date-head\">\r\n                                    <div class=\"form-group col-md-12 process-date\">\r\n                                        <label>Tanggal Proses : <strong>{{deliveryHistoryDetail.created_date}}</strong></label>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row delivery-status-head\" *ngIf=\"deliveryHistoryDetail.shipping_info && isArray(deliveryHistoryDetail.shipping_info) && deliveryHistoryDetail.shipping_info.length > 0\">\r\n                                    <div class=\"form-group col-md-12 delivery-status\">\r\n                                        <label>Status Pengiriman : {{deliveryHistoryDetail.shipping_info.at(-1).title}}</label>\r\n                                    </div>\r\n                                </div>\r\n                                \r\n\r\n                                <!-- ADMIN PART -->\r\n                                <!-- <div *ngIf=\"currentPermission == 'admin' && !openMoreDetail\"> -->\r\n                                    <!-- <div *ngFor=\"let deliveryHistoryDetail of deliveryHistoryDetail.order_list let i = index\"\r\n                                        class=\"card card-body mb-5 shadow rounded\"> -->\r\n                                        <div class=\"d-flex justify-content-between mb-3\">\r\n                                            <div>\r\n                                                <!-- <span style=\"color:#0ea8db; font-weight: bold;\">{{deliveryHistoryDetail.merchant_name}}</span> -->\r\n                                                <!-- <img style=\"width: 40px;\" src=\"{{deliveryHistoryDetail.image_url}}\"\r\n                                                    alt=\"Locard Icon\">\r\n                                                <span style=\"color:#0ea8db; font-weight: bold;margin-left: 10px;\">{{deliveryHistoryDetail.merchant_name}}</span> -->\r\n                                                <!-- <span style=\"border-left:solid 0.5px grey; margin: 0px 10px\"></span> -->\r\n                                                <!-- <span style=\"color:#0ea8db; font-weight: bold;\">Order Id : </span>\r\n                                                {{deliveryHistoryDetail.order_id}} -->\r\n                                            </div>\r\n                                            <!-- <button class=\"common-button blue-fill\"\r\n                                                (click)=\"openDetail()\">Go To Order Detail</button> -->\r\n                                        </div>\r\n                                        <div class=\"product-group\">\r\n                                            <table class=\"product\">\r\n                                                <tr>\r\n                                                    <th colspan=\"5\">{{deliveryHistoryDetail.delivery_type}}</th>\r\n                                                </tr>\r\n                                                <tr>\r\n                                                    <td class=\"product\">Product Name</td>\r\n                                                    <td>\r\n                                                        Varian\r\n                                                    </td>\r\n                                                    <td class=\"qty\">qty</td>\r\n                                                    <td class=\"ut\">unit price</td>\r\n                                                    <td class=\"price\">price</td>\r\n                                                </tr>\r\n                                                <tr *ngFor=\"let product of deliveryHistoryDetail.product_list; let i = index\">\r\n                                                    <td>{{product.product_name}}\r\n                                                         <!-- <br />\r\n                                                        <em><strong>note</strong>: {{product.note_to_merchant}}</em> -->\r\n                                                    </td>\r\n                                                    <td>\r\n                                                        {{product.variant && ((product.variant | json) != '{}') && ((product.variant | json) != '[]') ? replaceVarian(product.variant) : '-'}}\r\n                                                    </td>\r\n                                                    <td class=\"qty\">{{product.quantity | number}}x</td>\r\n                                                    <td class=\"qty\">{{product.sku_value | number}}</td>\r\n                                                    <td class=\"qty\">{{product.total_product_price | number}}\r\n                                                    </td>\r\n                                                    <!-- <td>{{deliveryHistoryDetail.unique_amount}}</td>  -->\r\n                                                </tr>\r\n                                                <tr style=\"border-top: 1.5px solid #eaeaea; width:80%;\">\r\n                                                    <td colspan=\"4\" class=\"priceTot\">Total Price</td>\r\n                                                    <td\r\n                                                        class=\"priceTotal\">\r\n                                                        {{deliveryHistoryDetail.total_item_value | number}}</td>\r\n                                                </tr>\r\n                                            </table>\r\n                                        </div>\r\n\r\n                                        <!-- ADMIN SHIPPING INFO -->\r\n                                        <div class=\"card mb-3\">\r\n                                            <div class=\"card-header\">\r\n                                                <h2>Shipping Information </h2>\r\n                                            </div>\r\n                                            <div class=\"card-content\">\r\n                                                <!-- <div class=\"col-md-12\" *ngIf=\"deliveryHistoryDetail.shipping_info && deliveryHistoryDetail.shipping_info.length <= 1\">\r\n                                                    <label>no shipping info</label>\r\n                                                </div> -->\r\n                                                <div class=\"col-md-12\" *ngIf=\"deliveryHistoryDetail.shipping_info\">\r\n                                                    <label *ngIf=\"deliveryHistoryDetail.delivery_detail\">Delivery Service :\r\n                                                        <strong><span style=\"color:#0EA8DB\">{{deliveryHistoryDetail.delivery_detail.courier}}\r\n                                                                ({{deliveryHistoryDetail.delivery_detail.delivery_service}})</span></strong></label>\r\n\r\n                                                    <div class=\"line\" style=\"width:100%; border-top: 1.5px solid #eaeaea; \"></div>\r\n                                                    <p style=\"margin-bottom:30px; margin-top:15px;\">Shipping Status</p>\r\n                                                    <p style=\"font-size:12px;\">Click each button to process your Shipping</p>\r\n\r\n                                                    <div class=\"form-group\">\r\n                                                        <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                                            <div class=\"radio-button checked-{{deliveryHistoryDetail.shipping_info[i]?true:'' || sSL.checked}} hovered-{{sSL.hovered}}\"\r\n                                                                (click)=\"changeshippingStatusList(i)\"\r\n                                                                (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                                                (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                                            </div>\r\n                                                            <label (click)=\"changeshippingStatusList(i)\"\r\n                                                                (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                                                (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                                                {{sSL.label}}<br />\r\n                                                                <div class=\"shipping-date\" *ngIf=\"sSL.created_date\">\r\n                                                                    <i class=\"fa fa-check\"></i>{{sSL.created_date}}</div>\r\n                                                                <!-- <span *ngIf=\"deliveryHistoryDetail.shipping_info[i]\"><i class=\"fa fa-check\"></i>Done</span> -->\r\n\r\n                                                                <div *ngIf=\"sSL.value=='on_processing'\" class=\"awb-list\">\r\n                                                                    <div class=\"input-delivery-container\">\r\n\r\n                                                                        <ng-container>\r\n                                                                            <label for=\"weight\">Weight <span class=\"star-required\">*</span></label>\r\n                                                                            <input\r\n                                                                                type=\"number\"\r\n                                                                                name=\"weight\" class=\"form-control awb\" type=\"text\"\r\n                                                                                [(ngModel)]=\"package_weight\"\r\n                                                                                required\r\n                                                                                [disabled]=isPackaging />\r\n                                                                            \r\n                                                                            <label for=\"length\">Length <span class=\"star-required\">*</span></label>\r\n                                                                            <input\r\n                                                                                type=\"number\"\r\n                                                                                name=\"length\" class=\"form-control awb\" type=\"text\"\r\n                                                                                [(ngModel)]=\"package_length\"\r\n                                                                                required\r\n                                                                                [disabled]=isPackaging />\r\n\r\n                                                                            <label for=\"width\">Width <span class=\"star-required\">*</span></label>\r\n                                                                            <input\r\n                                                                                type=\"number\"\r\n                                                                                name=\"width\" class=\"form-control awb\" type=\"text\"\r\n                                                                                [(ngModel)]=\"package_width\"\r\n                                                                                required\r\n                                                                                [disabled]=isPackaging />\r\n                                                                            \r\n                                                                            <label for=\"height\">Height <span class=\"star-required\">*</span></label>\r\n                                                                            <input\r\n                                                                                type=\"number\"\r\n                                                                                name=\"height\" class=\"form-control awb\" type=\"text\"\r\n                                                                                [(ngModel)]=\"package_height\"\r\n                                                                                required\r\n                                                                                [disabled]=isPackaging />\r\n\r\n                                                                        </ng-container>\r\n                                                                        \r\n                                                                        <div style=\"display:flex;width:100%;justify-content:space-around;align-items:center;cursor:auto;\">\r\n\r\n                                                                            <button *ngIf=\"isPackaging == false && isSubmitPackagingLoading == false\"\r\n                                                                                class=\"btn btn-primary\"\r\n                                                                                type=\"submit\"\r\n                                                                                (click)=\"submitPackaging()\"\r\n                                                                                value=\"submit\"\r\n                                                                                [disabled]=isPackaging>\r\n                                                                                Submit\r\n                                                                            </button>\r\n                                                                            <button *ngIf=\"isSubmitPackagingLoading == true\" style=\"margin:auto;width:85px;\" class=\"btn sbtn-primary\"><i class=\"fa fa-spinner fa-pulse\"></i></button>\r\n                                                                        </div>\r\n                                                                    </div>\r\n                                                                </div>\r\n\r\n                                                                <div *ngIf=\"sSL.value=='on_delivery'\" class=\"awb-list\">\r\n                                                                    <div class=\"input-delivery-container\" *ngIf=\"shippingStatusList[1].checked == true && changeAWB\">\r\n\r\n                                                                        \r\n                                                                        <ng-container *ngIf=\"deliveryHistoryDetail.available_courier\">\r\n\r\n                                                                            <select name=\"courier\" class=\"form-control awb\" [(ngModel)]=\"courier\">\r\n                                                                                <option value=\"\">Jenis Kurir</option>\r\n                                                                                <ng-container *ngFor=\"let typeCourier of deliveryHistoryDetail.available_courier\">\r\n                                                                                    <option [value]=\"typeCourier.courier_code\">{{typeCourier.courier_code.toUpperCase()}}</option>\r\n                                                                                </ng-container>\r\n                                                                            </select>\r\n                                                                            \r\n                                                                            <ng-container *ngIf=\"courier == 'REALS-SAP'\">\r\n                                                                                <ng-container *ngFor=\"let typeCourier of deliveryHistoryDetail.available_courier\">\r\n                                                                                    <ng-container *ngIf=\"typeCourier.courier_code == 'REALS-SAP'\">\r\n                                                                                        <select name=\"delivery_service\" class=\"form-control awb\" [(ngModel)]=\"delivery_service\">\r\n                                                                                            <option value=\"\">Delivery Service</option>\r\n                                                                                            <ng-container *ngFor=\"let typeDeliveryService of typeCourier.delivery_service | keyvalue\">\r\n                                                                                                <option [value] = \"typeDeliveryService.key\">{{typeDeliveryService.key}}</option>\r\n                                                                                            </ng-container>\r\n                                                                                        </select>\r\n                                                                                        <select name=\"delivery_method\" class=\"form-control awb\" [(ngModel)]=\"delivery_method\">\r\n                                                                                            <option value=\"\">Delivery Method</option>\r\n                                                                                            <ng-container *ngFor=\"let typeDeliveryMethod of typeCourier.delivery_method\">\r\n                                                                                                <option [value] = \"typeDeliveryMethod\">{{typeDeliveryMethod}}</option>\r\n                                                                                            </ng-container>\r\n                                                                                        </select>\r\n                                                                                    </ng-container>\r\n                                                                                </ng-container>\r\n\r\n                                                                                <input    \r\n                                                                                    name=\"delivery_remarks\" class=\"form-control awb\" type=\"text\"\r\n                                                                                    placeholder=\"Masukan remarks\"\r\n                                                                                    [(ngModel)]=\"delivery_remarks\"\r\n                                                                                />\r\n                                                                                \r\n                                                                            </ng-container>\r\n                                                                    \r\n                                                                    \r\n\r\n                                                                            <ng-container *ngIf=\"courier == 'others'\">\r\n                                                                                <label for=\"weight\">Courier Name <span class=\"star-required\">*</span></label>\r\n                                                                                <input\r\n                                                                                    \r\n                                                                                    name=\"courier\" class=\"form-control awb\" type=\"text\"\r\n                                                                                    placeholder=\"Masukan nama kurir\"\r\n                                                                                    [(ngModel)]=\"courier_name\"\r\n                                                                                    required />\r\n                                                                        \r\n                                                                                <label for=\"weight\">Delivery Service <span class=\"star-required\">*</span></label>\r\n                                                                                <input\r\n                                                                                    \r\n                                                                                    name=\"delivery_service\" class=\"form-control awb\" type=\"text\"\r\n                                                                                    placeholder=\"Masukan delivery service\"\r\n                                                                                    [(ngModel)]=\"delivery_service\"\r\n                                                                                    required />\r\n\r\n                                                                                <label for=\"weight\">Delivery Method <span class=\"star-required\">*</span></label>\r\n                                                                                <input\r\n                                                                                    \r\n                                                                                    name=\"delivery_method\" class=\"form-control awb\" type=\"text\"\r\n                                                                                    placeholder=\"Masukan delivery method\"\r\n                                                                                    [(ngModel)]=\"delivery_method\"\r\n                                                                                    required />\r\n                                                                            \r\n                                                                                <label for=\"weight\">Awb Number</label>\r\n                                                                                <input\r\n                                                                                    \r\n                                                                                    name=\"awb_number\" class=\"form-control awb\" type=\"text\"\r\n                                                                                    placeholder=\"Masukan nomor resi\"\r\n                                                                                    [(ngModel)]=\"awb_number\"\r\n                                                                                    required />\r\n                                                                            \r\n                                                                                <label for=\"weight\">Remark</label>\r\n                                                                                <input\r\n                                                                                    \r\n                                                                                    name=\"delivery_remarks\" class=\"form-control awb\" type=\"text\"\r\n                                                                                    placeholder=\"Masukan remarks\"\r\n                                                                                    [(ngModel)]=\"delivery_remarks\"\r\n                                                                                    required />\r\n                                                                            </ng-container>\r\n                                                                            \r\n                                                                        </ng-container>\r\n\r\n                                                                        <ng-container *ngIf=\"!deliveryHistoryDetail.available_courier\">\r\n                                                                            <label for=\"weight\">Courier Name</label>\r\n                                                                            <input\r\n                                                                            \r\n                                                                            name=\"courier\" class=\"form-control awb\" type=\"text\"\r\n                                                                            [(ngModel)] = \"courier\"\r\n                                                                            disabled />\r\n                                                                    \r\n                                                                            <label for=\"weight\">Courier Name <span class=\"star-required\">*</span></label>\r\n                                                                            <input\r\n                                                                                \r\n                                                                                name=\"courier_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                                placeholder=\"Masukan nama kurir\"\r\n                                                                                [(ngModel)]=\"courier_name\"\r\n                                                                                required />\r\n                                                                        \r\n                                                                            <label for=\"weight\">Delivery Service <span class=\"star-required\">*</span></label>\r\n                                                                            <input\r\n                                                                                \r\n                                                                                name=\"delivery_service\" class=\"form-control awb\" type=\"text\"\r\n                                                                                placeholder=\"Masukan delivery service\"\r\n                                                                                [(ngModel)]=\"delivery_service\"\r\n                                                                                required />\r\n                                                                        \r\n                                                                            <label for=\"weight\">Delivery Method <span class=\"star-required\">*</span></label>\r\n                                                                            <input\r\n                                                                                \r\n                                                                                name=\"delivery_method\" class=\"form-control awb\" type=\"text\"\r\n                                                                                placeholder=\"Masukan delivery method\"\r\n                                                                                [(ngModel)]=\"delivery_method\"\r\n                                                                                required />\r\n                                                                            \r\n                                                                            <label for=\"weight\">Awb Number</label>\r\n                                                                            <input\r\n                                                                                \r\n                                                                                name=\"awb_number\" class=\"form-control awb\" type=\"text\"\r\n                                                                                placeholder=\"Masukan nomor resi\"\r\n                                                                                [(ngModel)]=\"awb_number\"\r\n                                                                                required />\r\n                                                                            \r\n                                                                            <label for=\"weight\">Remark</label>\r\n                                                                            <input\r\n                                                                                \r\n                                                                                name=\"delivery_remarks\" class=\"form-control awb\" type=\"text\"\r\n                                                                                placeholder=\"Masukan remarks\"\r\n                                                                                [(ngModel)]=\"delivery_remarks\"\r\n                                                                                required />\r\n\r\n                                                                        </ng-container>\r\n                                                                        \r\n                                                                            <div style=\"display:flex;width:100%;justify-content:space-around;align-items:center;cursor:auto;\">\r\n\r\n                                                                                <button\r\n                                                                                    class=\"btn btn-primary\"\r\n                                                                                    *ngIf=\"shippingStatusList[1].checked == true && isChangeAWBLoading != true\"\r\n                                                                                    type=\"submit\"\r\n                                                                                    (click)=\"updateAWB()\"\r\n                                                                                    value=\"submit\">\r\n                                                                                    Update\r\n                                                                                </button>\r\n                                                                                \r\n                                                                                <!-- <button\r\n                                                                                    class=\"btn sbtn-primary\"\r\n                                                                                    *ngIf=\"shippingStatusList[1].checked == true && isChangeAWBLoading != true && deliveryHistoryDetail.delivery_detail.courier\"\r\n                                                                                    type=\"submit\"\r\n                                                                                    (click)=\"changeAWB = false\"\r\n                                                                                    value=\"submit\">\r\n                                                                                    Cancel\r\n                                                                                </button> -->\r\n\r\n                                                                                <button *ngIf=\"isChangeAWBLoading == true\" style=\"margin:auto;width:85px;\" class=\"btn sbtn-primary\"><i class=\"fa fa-spinner fa-pulse\"></i></button>\r\n                                                                            </div>\r\n                                                                    </div>\r\n\r\n                                                                    <div class=\"input-delivery-container\" *ngIf=\"deliveryHistoryDetail.courier && changeAWB == false\">\r\n                                                                            <label for=\"weight\">Courier Name <span class=\"star-required\">*</span></label>\r\n                                                                            <input\r\n                                                                                *ngIf=\"deliveryHistoryDetail.courier == 'others'\"\r\n                                                                                name=\"courier_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                                [(ngModel)]=\"deliveryHistoryDetail.courier_name\"\r\n                                                                                disabled />\r\n\r\n                                                                                <input\r\n                                                                                *ngIf=\"deliveryHistoryDetail.courier != 'others'\"\r\n                                                                                name=\"courier\" class=\"form-control awb\" type=\"text\"\r\n                                                                                [(ngModel)]=\"deliveryHistoryDetail.courier\"\r\n                                                                                disabled />\r\n                                                                        \r\n                                                                            <label for=\"weight\">Delivery Service <span class=\"star-required\">*</span></label>\r\n                                                                            <input\r\n                                                                                name=\"delivery_service\" class=\"form-control awb\" type=\"text\"\r\n                                                                                placeholder=\"empty delivery service\"\r\n                                                                                [(ngModel)]=\"deliveryHistoryDetail.delivery_service\"\r\n                                                                                disabled />\r\n                                                                        \r\n                                                                            <label for=\"weight\">Delivery Method <span class=\"star-required\">*</span></label>\r\n                                                                            <input\r\n                                                                                name=\"delivery_method\" class=\"form-control awb\" type=\"text\"\r\n                                                                                placeholder=\"empty delivery method\"\r\n                                                                                [(ngModel)]=\"deliveryHistoryDetail.delivery_method\"\r\n                                                                                disabled />\r\n                                                                            \r\n                                                                            <label for=\"weight\">Awb Number</label>\r\n                                                                            <input\r\n                                                                                name=\"awb_number\" class=\"form-control awb\" type=\"text\"\r\n                                                                                placeholder=\"empty AWB\"\r\n                                                                                [(ngModel)]=\"deliveryHistoryDetail.awb_number\"\r\n                                                                                disabled />\r\n\r\n                                                                            <label for=\"weight\">Remark</label>\r\n                                                                            <input\r\n                                                                                name=\"delivery_remarks\" class=\"form-control awb\" type=\"text\"\r\n                                                                                placeholder=\"empty remarks\"\r\n                                                                                [(ngModel)]=\"delivery_remarks\"\r\n                                                                                disabled />\r\n\r\n                                                                            <button\r\n                                                                                style=\"margin:auto;\"\r\n                                                                                class=\"btn btn-primary\"\r\n                                                                                *ngIf=\"deliveryHistoryDetail.shipping_info && isArray(deliveryHistoryDetail.shipping_info) && deliveryHistoryDetail.shipping_info.length > 0 && deliveryHistoryDetail.shipping_info.at(-1).label == 'on_delivery' && deliveryHistoryDetail.courier == 'others' \"\r\n                                                                                type=\"submit\"\r\n                                                                                (click)=\"changeAWB = true\"\r\n                                                                                value=\"Submit\">\r\n                                                                            Change AWB Number\r\n                                                                            </button>\r\n                                                                    </div>\r\n                                                                </div>\r\n\r\n                                                                <div>\r\n                                                                    <button *ngIf=\"sSL.value=='on_delivery' && deliveryHistoryDetail && deliveryHistoryDetail.track_history && deliveryHistoryDetail.track_history.length > 0\" class=\"btn btn-outline-primary mb-2 mr-2\" (click)=\"openScrollableTrackingContent(trackingContent)\"><i class=\"fa fa-truck-pickup\"></i>Tracking Info Detail</button>\r\n                                                                </div>\r\n\r\n                                                                <div  *ngIf=\"sSL.value=='delivered' && shippingStatusList[2].checked == true\" class=\"awb-list delivered-form\">\r\n                                                                    <div class=\"input-delivery-container\">\r\n                                                                        <label for=\"weight\">Date and time</label>\r\n                                                                        <div class=\"input-date-form\">\r\n                                                                            <form class=\"form-inline\">\r\n                                                                                <div class=\"form-group\">\r\n                                                                                    <div class=\"input-group\">\r\n                                                                                    <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                                            name=\"dp\" [(ngModel)]=\"date\" ngbDatepicker [footerTemplate]=\"footerTemplate\" #d=\"ngbDatepicker\" [disabled] = \"isCompleteOrder == true\">\r\n                                                                                    <div class=\"input-group-append\">\r\n                                                                                        <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\">\r\n                                                                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                                        </button>\r\n                                                                                    </div>\r\n                                                                                    </div>\r\n                                                                                </div>\r\n                                                                            </form>\r\n                                                                            <ngb-timepicker [(ngModel)]=\"time\" [seconds]=\"true\" [disabled] = \"isCompleteOrder == true\"></ngb-timepicker>\r\n                                                                        </div>\r\n                                                                    \r\n                                                                        <label for=\"weight\">Receiver Name</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Masukan nama penerima\"\r\n                                                                            [(ngModel)]=\"receiver_name\"\r\n                                                                            [disabled] = \"isCompleteOrder == true\"\r\n                                                                            required />\r\n                                                                    \r\n                                                                        <label for=\"weight\">Receiver Status</label>\r\n                                                                        <input\r\n                                                                            name=\"delivery_status\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Masukan status penerima\"\r\n                                                                            [(ngModel)]=\"delivery_status\"\r\n                                                                            [disabled] = \"isCompleteOrder == true\"\r\n                                                                            required />\r\n                                                                        \r\n                                                                        <label for=\"weight\">Remark</label>\r\n                                                                        <input\r\n                                                                            name=\"remark\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Masukan remark\"\r\n                                                                            [(ngModel)]=\"remark\"\r\n                                                                            [disabled] = \"isCompleteOrder == true\"\r\n                                                                            required />\r\n\r\n\r\n                                                                        <div *ngIf=\"isCompleteOrder == false\" class=\"button-complete\">\r\n                                                                            <div *ngIf=\"completeButtonConfirmation\">\r\n                                                                                <span>Anda yakin akan mengubah menjadi delivered?</span>\r\n                                                                                <div style=\"display:flex;width:100%;justify-content:space-around;align-items:center;cursor:auto;\">\r\n                                                                                    <button\r\n                                                                                        *ngIf=\"isChangeLoading == false\"\r\n                                                                                        class=\" btn btn-success half-button\"\r\n                                                                                        type=\"submit\"\r\n                                                                                        value=\"Submit\"\r\n                                                                                        (click)=\"updateComplete()\"\r\n                                                                                    >\r\n                                                                                        Ya\r\n                                                                                    </button>\r\n                                                                                    <button *ngIf=\"isChangeLoading == true\" class=\"btn rounded-btn open-login\" type=\"submit\" ><i class=\"fa fa-spinner fa-pulse\"></i></button>&nbsp;\r\n                                                                                    <span>&nbsp;</span>\r\n                                                                                    <button\r\n                                                                                        class=\" btn btn-cancel half-button\"\r\n                                                                                        type=\"submit\"\r\n                                                                                        value=\"Submit\"\r\n                                                                                        (click)=\"completeButtonConfirmation = false\"\r\n                                                                                    >\r\n                                                                                        Tidak\r\n                                                                                    </button>\r\n                                                                                </div>\r\n                                                                            </div>\r\n                                                                            <button *ngIf=\"!completeButtonConfirmation\"\r\n                                                                                class=\" btn btn-success full-button\"\r\n                                                                                type=\"submit\"\r\n                                                                                value=\"Submit\"\r\n                                                                                (click)=\"completeButtonConfirmation = true\"\r\n                                                                                [disabled]=\"!receiver_name || !delivery_status || !remark || !date || !time\"\r\n                                                                                >\r\n                                                                                Delivered this Pickup\r\n                                                                            </button>\r\n                                                                        </div>\r\n\r\n                                                                        <div *ngIf=\"isCompleteOrder == true\" class=\"order-complete-text\">\r\n                                                                            Pick Up Delivered\r\n                                                                        </div>\r\n\r\n                                                                    </div>\r\n                                                                </div>\r\n\r\n                                                            </label>\r\n\r\n                                                        </div>\r\n                                                    </div>\r\n\r\n\r\n                                                    <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services=='no-services'\">\r\n                                                        <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                                            <div class=\"radio-button checked-false hovered-false\">\r\n                                                            </div>\r\n                                                            <label>\r\n                                                                {{sSL.label}}<br />\r\n                                                                <span class=\"shipping-date\"\r\n                                                                    *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                                            </label>\r\n                                                        </div>\r\n                                                    </div>\r\n\r\n\r\n                                                </div>\r\n\r\n                                                <div class=\"printLabel\" *ngIf=\"deliveryHistoryDetail.billings != undefined\">\r\n                                                    <button class=\"btn btn-primary\" (click)=\"openLabel(historyID)\">Open Label</button>\r\n                                                </div>\r\n\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <!-- RETURN REDELIVERY -->\r\n                                        <div  *ngIf=\"isEverDelivered\"\r\n                                            class=\"card mb-3 \">\r\n                                            <div class=\"card-header\">\r\n                                                <h2>Return Process </h2>\r\n                                            </div>\r\n                                            <div class=\"card-content\">\r\n                                                <!-- <ng-template *ngFor=\"let shippingInfo of deliveryDetail.shipping_info; index as i;\">\r\n                                                    <ng-template *ngIf=\"i > 1\"> -->\r\n                                                <div class=\"col-md-12\" *ngIf=\"deliveryHistoryDetail.shipping_info\">\r\n                                                    <!-- <div style=\"margin-bottom: 25px;\" *ngIf=\"deliveryHistoryDetail.last_shipping_info == 'delivered'\">\r\n                                                        <p style=\"font-size: 14px; margin-bottom:10px; font-weight: bold;\">Please select reorder or redeliver</p>\r\n                                                        <select class=\"form-control\" [(ngModel)]=\"returnType\">\r\n                                                            <option value=\"redeliver\">\r\n                                                                REDELIVER\r\n                                                            </option>\r\n                                                            <option value=\"reorder\">\r\n                                                                REORDER\r\n                                                            </option>\r\n                                                        </select>\r\n                                                    </div> -->\r\n\r\n\r\n                                                    <div *ngIf=\"deliveryHistoryDetail.status == 'REORDER'; else redeliverWord\">\r\n                                                        Reorder Info\r\n                                                    </div>\r\n                                                    <ng-template #redeliverWord>\r\n                                                        Re Info\r\n                                                    </ng-template>\r\n                                                    <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services'\" style=\"margin-top:30px;\">\r\n                                                        <div class=\"shipping-status\" *ngFor=\"let sSL of redeliverList; let i = index\">\r\n                                                            <div class=\"radio-button checked-{{sSL.checked}} hovered-{{sSL.hovered}}\">\r\n                                                            </div>\r\n\r\n                                                            <label \r\n                                                                [ngClass]=\"sSL.value=='redelivery'? 'redelivery-label' : ''\">\r\n                                                                {{i == lastIndexRedelivery && (returnType == 'reorder' || deliveryHistoryDetail.status == 'REORDER')  ? 'Reorder Process' : sSL.label}}<br />\r\n\r\n                                                                <div *ngIf=\"sSL.value=='return'\" class=\"awb-list return-form\">\r\n\r\n                                                                    <div style=\"margin-bottom: 25px;\" *ngIf=\"deliveryHistoryDetail.last_shipping_info == 'delivered' && i == lastIndexRedelivery\">\r\n                                                                        <p style=\"font-size: 14px; margin-bottom:10px; font-weight: bold;\">Please select reorder or redeliver</p>\r\n                                                                        <select class=\"form-control\" [(ngModel)]=\"returnType\">\r\n                                                                            <option value=\"redeliver\">\r\n                                                                                REDELIVER\r\n                                                                            </option>\r\n                                                                            <option value=\"reorder\">\r\n                                                                                REORDER\r\n                                                                            </option>\r\n                                                                        </select>\r\n                                                                    </div>\r\n\r\n                                                                    <div class=\"information-text\" *ngIf=\"(i == lastIndexRedelivery && returnType == 'reorder') || (i == lastIndexRedelivery && deliveryHistoryDetail.status=='REORDER');else redeliveryWord\">*proses pengembalian dari pelanggan ke admin (status akan menjadi cancel), kemudian dapat dilakukan order ulang</div>\r\n                                                                    <ng-template #redeliveryWord>\r\n                                                                        <div class=\"information-text\">*proses pengembalian dari pelanggan ke admin, kemudian akan dilakukan pengiriman ulang dengan order yang sama (tanpa harus order ulang)</div>\r\n                                                                    </ng-template>\r\n                                                                    <br/>\r\n\r\n\r\n                                                                    <div *ngIf=\"(i == lastIndexRedelivery && returnType == 'reorder' && deliveryHistoryDetail.status == 'DELIVERED')\" class=\"input-form\">\r\n                                                                        <div *ngFor=\"let product of deliveryHistoryDetail.product_list; let ip = index\">\r\n                                                                            <mat-checkbox  type=\"checkbox\" name=\"cbox\" [(ngModel)]=\"reorderProductCheck[ip]\">\r\n                                                                            </mat-checkbox>\r\n                                                                            {{product.product_name+' - '}}\r\n                                                                            {{product.sku_code+' '}}\r\n                                                                            {{'( '+product.total_product_price+' )'}}\r\n                                                                        </div>\r\n                                                                    </div>\r\n\r\n                                                                    <div *ngIf=\"i == lastIndexRedelivery && deliveryHistoryDetail.status == 'REORDER'\">\r\n                                                                        <div *ngFor=\"let product of deliveryHistoryDetail.product_list; let ip = index\">\r\n                                                                            <div *ngIf=\"product.cancel_detail\">\r\n                                                                                {{product.product_name+' - '}}\r\n                                                                                {{product.sku_code+' '}}\r\n                                                                                {{'( '+product.total_product_price+' )'}}\r\n                                                                            </div>\r\n                                                                        </div>\r\n                                                                    </div>\r\n\r\n                                                                    <div class=\"input-form\">\r\n                                                                        <label>Date and time</label>\r\n                                                                        <div class=\"input-date-form awb\">\r\n                                                                            <form class=\"form-inline\">\r\n                                                                                <div class=\"form-group\">\r\n                                                                                    <div class=\"input-group\">\r\n                                                                                    <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                                            name=\"dp\" [(ngModel)]=\"return_date[i]\" ngbDatepicker #d=\"ngbDatepicker\" [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true || deliveryHistoryDetail.status == 'REORDER'\">\r\n                                                                                    <div class=\"input-group-append\">\r\n                                                                                        <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\" [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true  || deliveryHistoryDetail.status == 'REORDER'\">\r\n                                                                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                                        </button>\r\n                                                                                    </div>\r\n                                                                                    </div>\r\n                                                                                </div>\r\n                                                                            </form>\r\n                                                                        </div>\r\n                                                                    \r\n                                                                        <label>Courier Name</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Kurir\"\r\n                                                                            [(ngModel)]=\"return_courier[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true || deliveryHistoryDetail.status == 'REORDER'\"\r\n                                                                            required />\r\n                                                                        <label>AWB Number</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"AWB Number\"\r\n                                                                            [(ngModel)]=\"return_awb_number[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true || deliveryHistoryDetail.status == 'REORDER'\"\r\n                                                                            required />\r\n                                                                        <label>Remark</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Remarks\"\r\n                                                                            [(ngModel)]=\"return_remarks[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true || deliveryHistoryDetail.status == 'REORDER'\"\r\n                                                                            required />\r\n\r\n\r\n\r\n                                                                        <div *ngIf=\" i >= (lastIndexRedelivery - 1)\" class=\"button-complete\">\r\n                                                                            <div *ngIf=\"redeliverButtonConfirmation\">\r\n                                                                                <span>Anda yakin akan melanjutkan proses ini? </span>\r\n                                                                                <button\r\n                                                                                    *ngIf=\"isChangeRedeliverLoading == false\"\r\n                                                                                    class=\" btn btn-success\"\r\n                                                                                    type=\"submit\"\r\n                                                                                    value=\"Submit\"\r\n                                                                                    (click)=\"processReturn(i, returnType)\"\r\n                                                                                >\r\n                                                                                    Ya\r\n                                                                                </button>\r\n                                                                                <button *ngIf=\"isChangeRedeliverLoading == true\" class=\"btn rounded-btn open-login\" type=\"submit\" ><i class=\"fa fa-spinner fa-pulse\"></i></button>&nbsp;\r\n                                                                                <span>&nbsp;</span>\r\n                                                                                <button\r\n                                                                                    class=\" btn btn-cancel\"\r\n                                                                                    type=\"submit\"\r\n                                                                                    value=\"Submit\"\r\n                                                                                    (click)=\"redeliverButtonConfirmation = false; disableFormShipping[i] = true\"\r\n                                                                                >\r\n                                                                                    Tidak\r\n                                                                                </button>\r\n                                                                            </div>\r\n                                                                            <button *ngIf=\"!redeliverButtonConfirmation && disableFormShipping[i] == false\"\r\n                                                                                class=\" btn btn-success\"\r\n                                                                                type=\"submit\"\r\n                                                                                value=\"Submit\"\r\n                                                                                (click)=\"redeliverButtonConfirmation = true\"\r\n                                                                                [disabled]=\"!return_courier[i] || !return_awb_number[i] || !return_remarks[i] || !return_date[i]\"\r\n                                                                                >\r\n                                                                                {{(i == lastIndexRedelivery && returnType == 'reorder') ? 'Process Reorder' : 'Process Redeliver'}}\r\n                                                                            </button>\r\n\r\n                                                                            <button *ngIf=\"!redeliverButtonConfirmation && i == lastIndexRedelivery -1 && disableFormShipping[i] == true\"\r\n                                                                                class=\" btn btn-success\"\r\n                                                                                type=\"submit\"\r\n                                                                                value=\"Submit\"\r\n                                                                                (click)=\"disableFormShipping[i] = false\"\r\n                                                                                >\r\n                                                                                {{(i == lastIndexRedelivery && returnType == 'reorder') ? 'Update Reorder' : 'Update Redeliver'}}\r\n                                                                            </button>                                                                           \r\n                                                                        </div>\r\n                                                                    </div>\r\n                                                                </div>\r\n                                                                \r\n                                                                <!-- Return Order Redelivery -->\r\n                                                                <div *ngIf=\"sSL.value=='redelivery'\" class=\"awb-list return-form\">\r\n                                                                    <div class=\"information-text\">*proses pengiriman ulang dari admin ke pelanggan</div>\r\n                                                                    <div class=\"input-form\">\r\n                                                                        <label>Date and Time</label>\r\n                                                                        <div class=\"input-date-form awb\">\r\n                                                                            <form class=\"form-inline\">\r\n                                                                                <div class=\"form-group\">\r\n                                                                                    <div class=\"input-group\">\r\n                                                                                    <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                                            name=\"dp\" [(ngModel)]=\"redelivery_date[i]\" ngbDatepicker #d=\"ngbDatepicker\" [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\">\r\n                                                                                    <div class=\"input-group-append\">\r\n                                                                                        <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\" [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\">\r\n                                                                                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                                        </button>\r\n                                                                                    </div>\r\n                                                                                    </div>\r\n                                                                                </div>\r\n                                                                            </form>\r\n                                                                        </div>\r\n                                                                    \r\n                                                                        <label>Courier Name</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Kurir\"\r\n                                                                            [(ngModel)]=\"redelivery_courier[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\"\r\n                                                                            required />\r\n                                                                        <label>AWB Number</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"AWB Number\"\r\n                                                                            [(ngModel)]=\"redelivery_awb_number[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\"\r\n                                                                            required />\r\n                                                                        <label>Delivery Service</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Delivery Service\"\r\n                                                                            [(ngModel)]=\"redelivery_services[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\"\r\n                                                                            required />\r\n                                                                        <label>Delivery Method</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Delivery Method\"\r\n                                                                            [(ngModel)]=\"redelivery_method[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\"\r\n                                                                            required />\r\n                                                                        <label>Remark</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Remarks\"\r\n                                                                            [(ngModel)]=\"redelivery_remarks[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\"\r\n                                                                            required />\r\n\r\n\r\n\r\n                                                                        <div *ngIf=\"i >= (lastIndexRedelivery -1)\" class=\"button-complete\">\r\n                                                                            <div *ngIf=\"processOrderRedeliveryButtonConfirmation\">\r\n                                                                                <span>Anda yakin akan memproses order redelivery?</span>\r\n                                                                                <button\r\n                                                                                    *ngIf=\"isChangeProcessRedeliveryLoading == false\"\r\n                                                                                    class=\" btn btn-success\"\r\n                                                                                    type=\"submit\"\r\n                                                                                    value=\"Submit\"\r\n                                                                                    (click)=\"processOrderRedelivery(i)\"\r\n                                                                                >\r\n                                                                                    Ya\r\n                                                                                </button>\r\n                                                                                <button \r\n                                                                                    *ngIf=\"isChangeProcessRedeliveryLoading == true\"\r\n                                                                                    class=\"btn rounded-btn open-login\"\r\n                                                                                    type=\"submit\"\r\n                                                                                >\r\n                                                                                    <i class=\"fa fa-spinner fa-pulse\"></i>\r\n                                                                                </button> &nbsp;\r\n                                                                                <span>&nbsp;</span>\r\n                                                                                <button\r\n                                                                                    class=\" btn btn-cancel\"\r\n                                                                                    type=\"submit\"\r\n                                                                                    value=\"Submit\"\r\n                                                                                    (click)=\"processOrderRedeliveryButtonConfirmation = false; disableFormShipping[i] = true\"\r\n                                                                                >\r\n                                                                                    Tidak\r\n                                                                                </button>\r\n                                                                            </div>\r\n                                                                            <button *ngIf=\"!processOrderRedeliveryButtonConfirmation && disableFormShipping[i] == false\"\r\n                                                                                class=\" btn btn-success\"\r\n                                                                                type=\"submit\"\r\n                                                                                value=\"Submit\"\r\n                                                                                (click)=\"processOrderRedeliveryButtonConfirmation = true\"\r\n                                                                                [disabled]=\"!redelivery_courier[i] || !redelivery_awb_number[i] || !redelivery_services[i] || !redelivery_method[i] || !redelivery_remarks[i] || !redelivery_date[i]\"\r\n                                                                                >\r\n                                                                                Process Order Redelivery\r\n                                                                            </button>\r\n\r\n                                                                            <button *ngIf=\"!processOrderRedeliveryButtonConfirmation && disableFormShipping[i] == true && i == lastIndexRedelivery - 1\"\r\n                                                                                class=\" btn btn-success\"\r\n                                                                                type=\"submit\"\r\n                                                                                value=\"Submit\"\r\n                                                                                (click)=\"disableFormShipping[i] = false\"\r\n                                                                                >\r\n                                                                                update Order Redelivery\r\n                                                                            </button>\r\n\r\n                                                                        </div>\r\n                                                                    </div>\r\n                                                                </div>\r\n\r\n                                                                <div  *ngIf=\"sSL.value=='delivered'\" class=\"awb-list return-form\">\r\n                                                                    <div class=\"input-delivery-container\">\r\n                                                                        <label>Date and Time</label>\r\n                                                                        <div class=\"input-date-form\">\r\n                                                                            <form class=\"form-inline\">\r\n                                                                                <div class=\"form-group\">\r\n                                                                                    <div class=\"input-group\">\r\n                                                                                    <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                                            name=\"dp\" [(ngModel)]=\"redelivered_date[i]\" ngbDatepicker [footerTemplate]=\"footerTemplate\" #d=\"ngbDatepicker\" [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\">\r\n                                                                                    <div class=\"input-group-append\">\r\n                                                                                        <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\" [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\">\r\n                                                                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                                        </button>\r\n                                                                                    </div>\r\n                                                                                    </div>\r\n                                                                                </div>\r\n                                                                            </form>\r\n                                                                            <ngb-timepicker [(ngModel)]=\"redelivered_time[i]\" [seconds]=\"true\" [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\"></ngb-timepicker>\r\n                                                                        </div>\r\n                                                                    \r\n                                                                        <label>Receiver Name</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Masukan nama penerima\"\r\n                                                                            [(ngModel)]=\"redelivered_receiver_name[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\"\r\n                                                                            required />\r\n                                                                    \r\n                                                                        <label>Deliver Status</label>\r\n                                                                        <input\r\n                                                                            name=\"delivery_status\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Masukan status penerima\"\r\n                                                                            [(ngModel)]=\"redelivered_delivery_status[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\"\r\n                                                                            required />\r\n                                                                        \r\n                                                                        <label>Remark</label>\r\n                                                                        <input\r\n                                                                            name=\"remark\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Masukan remark\"\r\n                                                                            [(ngModel)]=\"redelivered_remark[i]\"\r\n                                                                            [disabled] = \"i != lastIndexRedelivery && disableFormShipping[i] == true\"\r\n                                                                            required />\r\n\r\n\r\n                                                                        <div *ngIf=\"i >= (lastIndexRedelivery - 1)\" class=\"button-complete\">\r\n                                                                            <div *ngIf=\"completeButtonConfirmation\">\r\n                                                                                <span>Anda yakin akan mengubah menjadi complete?</span>\r\n                                                                                <button\r\n                                                                                    *ngIf=\"isChangeLoading == false\"\r\n                                                                                    class=\" btn btn-success\"\r\n                                                                                    type=\"submit\"\r\n                                                                                    value=\"Submit\"\r\n                                                                                    (click)=\"updateComplete(i)\"\r\n                                                                                >\r\n                                                                                    Ya\r\n                                                                                </button>\r\n                                                                                <button *ngIf=\"isChangeLoading == true\" class=\"btn rounded-btn open-login\" type=\"submit\" ><i class=\"fa fa-spinner fa-pulse\"></i></button>&nbsp;\r\n                                                                                <span>&nbsp;</span>\r\n                                                                                <button\r\n                                                                                    class=\" btn btn-cancel\"\r\n                                                                                    type=\"submit\"\r\n                                                                                    value=\"Submit\"\r\n                                                                                    (click)=\"completeButtonConfirmation = false; disableFormShipping[i] = true\"\r\n                                                                                >\r\n                                                                                    Tidak\r\n                                                                                </button>\r\n                                                                            </div>\r\n                                                                            <button *ngIf=\"!completeButtonConfirmation && disableFormShipping[i] == false\"\r\n                                                                                class=\" btn btn-success\"\r\n                                                                                type=\"submit\"\r\n                                                                                value=\"Submit\"\r\n                                                                                (click)=\"completeButtonConfirmation = true\"\r\n                                                                                [disabled]=\"!redelivered_receiver_name[i] || !redelivered_delivery_status[i] || !redelivered_remark[i] || !redelivered_date[i] || !redelivered_time[i]\"\r\n                                                                                >\r\n                                                                                Completed this Order\r\n                                                                            </button>\r\n\r\n                                                                            <button *ngIf=\"!completeButtonConfirmation && disableFormShipping[i] == true && i == lastIndexRedelivery - 1 && deliveryHistoryDetail.status == 'REORDER'\"\r\n                                                                                class=\" btn btn-success\"\r\n                                                                                type=\"submit\"\r\n                                                                                value=\"Submit\"\r\n                                                                                (click)=\"disableFormShipping[i] = false\"\r\n                                                                                >\r\n                                                                                Update data completed\r\n                                                                            </button>\r\n                                                                        </div>\r\n\r\n                                                                        <!-- <div *ngIf=\"isCompleteOrder == true\" class=\"order-complete-text\">\r\n                                                                            Order Completed\r\n                                                                        </div> -->\r\n                                                                    </div>\r\n                                                                </div>\r\n                                                            </label>\r\n\r\n                                                        </div>\r\n\r\n                                                        <!-- options redelivery-->\r\n                                                        <!-- <div class=\"form-group\">\r\n                                                            <div class=\"shipping-status\">\r\n                                                                <div class=\"radio-button checked-false hovered-true\">\r\n                                                                </div>\r\n                                                                <label \r\n                                                                    class=\"redelivery-label\">\r\n                                                                    Return Order Redelivery<br />\r\n                                                                </label>\r\n                                                            </div>\r\n                                                        </div> -->\r\n\r\n                                                    </div>\r\n\r\n\r\n                                                    <!-- <div class=\"form-group\" *ngIf=\"deliveryHistoryDetail.shipping_services!='no-services' && returnType == 'reorder'\" style=\"margin-top:30px;\">\r\n                                                        <div class=\"shipping-status\">\r\n                                                            <div class=\"radio-button checked-true hovered-true\"\r\n                                                                (click)=\"changeshippingStatusList(i)\"\r\n                                                                (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                                                (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                                            </div>\r\n                                                            <label (click)=\"changeshippingStatusList(i)\"\r\n                                                                (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                                                (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                                                Reorder Process<br />\r\n                                                                <div class=\"awb-list return-form\">\r\n                                                                    <div class=\"information-text\">*proses pengembalian dari pelanggan ke admin (status akan menjadi cancel), kemudian dapat dilakukan order ulang</div>\r\n                                                                    <div class=\"input-form\">\r\n                                                                        <label>Date and Time</label>\r\n                                                                        <div class=\"input-date-form awb\">\r\n                                                                            <form class=\"form-inline\">\r\n                                                                                <div class=\"form-group\">\r\n                                                                                    <div class=\"input-group\">\r\n                                                                                    <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                                            name=\"dp\" [(ngModel)]=\"reorder_date\" ngbDatepicker #d=\"ngbDatepicker\" [disabled] = \"i != lastIndexRedelivery\">\r\n                                                                                    <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                                                                            name=\"dp\" [(ngModel)]=\"reorder_date\" ngbDatepicker #d=\"ngbDatepicker\">\r\n                                                                                    <div class=\"input-group-append\">\r\n                                                                                        <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\" [disabled] = \"i != lastIndexRedelivery\">\r\n                                                                                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                                        </button>\r\n                                                                                        <button class=\"btn btn-outline-secondary calendar\" (click)=\"d.toggle()\" type=\"button\">\r\n                                                                                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                                                                        </button>\r\n                                                                                    </div>\r\n                                                                                    </div>\r\n                                                                                </div>\r\n                                                                            </form>\r\n                                                                        </div>\r\n                                                                    \r\n                                                                        <label>Courier Name</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Kurir\"\r\n                                                                            [(ngModel)]=\"reorder_courier\"\r\n                                                                            required />\r\n                                                                        <label>Awb Number</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"AWB Number\"\r\n                                                                            [(ngModel)]=\"reorder_awb_number\"\r\n                                                                            required />\r\n                                                                        <label>Remark</label>\r\n                                                                        <input\r\n                                                                            name=\"receiver_name\" class=\"form-control awb\" type=\"text\"\r\n                                                                            placeholder=\"Remarks\"\r\n                                                                            [(ngModel)]=\"reorder_remarks\"\r\n                                                                            required />\r\n\r\n\r\n\r\n                                                                        <div *ngIf=\"isReorder == false\" class=\"button-complete\">\r\n                                                                            <div *ngIf=\"reorderButtonConfirmation\">\r\n                                                                                <span>Anda yakin akan memproses reorder?</span>\r\n                                                                                <button\r\n                                                                                    *ngIf=\"isChangeReorderLoading == false\"\r\n                                                                                    class=\" btn btn-success\"\r\n                                                                                    type=\"submit\"\r\n                                                                                    value=\"Submit\"\r\n                                                                                    (click)=\"processReorder()\"\r\n                                                                                >\r\n                                                                                    Ya\r\n                                                                                </button>\r\n                                                                                <button *ngIf=\"isChangeReorderLoading == true\" class=\"btn rounded-btn open-login\" type=\"submit\" ><i class=\"fa fa-spinner fa-pulse\"></i></button>&nbsp;\r\n                                                                                <span>&nbsp;</span>\r\n                                                                                <button\r\n                                                                                    class=\" btn btn-cancel\"\r\n                                                                                    type=\"submit\"\r\n                                                                                    value=\"Submit\"\r\n                                                                                    (click)=\"reorderButtonConfirmation = false\"\r\n                                                                                >\r\n                                                                                    Tidak\r\n                                                                                </button>\r\n                                                                            </div>\r\n                                                                            <button *ngIf=\"!reorderButtonConfirmation\"\r\n                                                                                class=\" btn btn-success\"\r\n                                                                                type=\"submit\"\r\n                                                                                value=\"Submit\"\r\n                                                                                (click)=\"reorderButtonConfirmation = true\"\r\n                                                                                [disabled]=\"!reorder_courier || !reorder_awb_number || !reorder_remarks || !reorder_date\"\r\n                                                                                >\r\n                                                                                Process Reorder\r\n                                                                            </button>\r\n                                                                        </div>\r\n\r\n\r\n\r\n\r\n                                                                    </div>\r\n                                                                </div>\r\n                                                            </label>\r\n\r\n                                                        </div>\r\n                                                    </div> -->\r\n                                                    \r\n                                                </div>\r\n                                                <!-- </ng-template>\r\n                                            </ng-template> -->\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                <!-- ADMIN EDIT PAGE -->\r\n\r\n                                <div *ngIf=\"currentPermission == 'admin' && openMoreDetail\">\r\n                                    <div>\r\n                                        <!-- <span style=\"color:#0ea8db; font-weight: bold;\">{{detailPage.merchant_name}}</span> -->\r\n                                        |\r\n                                        <span style=\"color:#0ea8db; font-weight: bold;\">Order Id : </span>\r\n                                        {{detailPage.booking_id}}\r\n                                        <div *ngIf=\"detailPage.products\" class=\"product-group\">\r\n                                            <table class=\"product\">\r\n                                                <tr>\r\n                                                    <th class=\"product\">Product Name</th>\r\n                                                    <th class=\"qty\">qty</th>\r\n                                                    <th class=\"ut\">unit price</th>\r\n                                                    <th class=\"price\">price</th>\r\n                                                </tr>\r\n                                                <tr *ngFor=\"let p of detailPage.products\">\r\n                                                    <td>{{p.product_name}} <br />\r\n                                                        <em><strong>note</strong>: {{p.note_to_merchant}}</em>\r\n                                                    </td>\r\n                                                    <td class=\"qty\">{{p.quantity | number }}x</td>\r\n                                                    <td class=\"qty\">{{p.fixed_price | number}}</td>\r\n                                                    <td class=\"qty\">{{p.total_product_price | number}}\r\n                                                    </td>\r\n                                                    <!-- <td>{{deliveryHistoryDetail.unique_amount}}</td>  -->\r\n                                                </tr>\r\n                                                <tr>\r\n                                                    <td>Shipping Fee</td>\r\n                                                    <td></td>\r\n                                                    <td></td>\r\n                                                    <td class=\"fee\">{{detailPage.shipping_fee | number }}</td>\r\n                                                    <!-- <td></td> -->\r\n\r\n                                                </tr>\r\n                                                <!-- <tr\r\n                                                    *ngIf=\"deliveryHistoryDetail.unique_amount && deliveryHistoryDetail.payment_method == 'manual_payment'\">\r\n                                                    <td>Unique Amount</td>\r\n                                                    <td></td>\r\n                                                    <td></td>\r\n                                                    <td class=\"fee\">\r\n                                                        {{deliveryHistoryDetail.unique_amount  | number}}</td>\r\n                                                   \r\n\r\n                                                </tr> -->\r\n                                                <tr style=\"border-top: 1.5px solid #eaeaea; width:80%;\">\r\n                                                    <td colspan=\"3\" class=\"priceTot\">Total Price</td>\r\n                                                    <td *ngIf=\"deliveryHistoryDetail.billings != undefined\"\r\n                                                        class=\"priceTotal\">\r\n                                                        {{moreOrderDetail.billings.sum_total | number }}</td>\r\n                                                    <td *ngIf=\"deliveryHistoryDetail.billings == undefined\"\r\n                                                        class=\"priceTotal\">\r\n                                                        {{moreOrderDetail.billings.sum_total | number }}</td>\r\n                                                    <td *ngIf=\"deliveryHistoryDetail.billings == undefined\"\r\n                                                        class=\"priceTotal\">\r\n                                                        {{moreOrderDetail.billings.sum_total | number }}</td>\r\n                                                </tr>\r\n                                            </table>\r\n                                        </div>\r\n                                        <!-- SHIPPING HISTORY -->\r\n                                        <div class=\"product-group\"\r\n                                            *ngIf=\"detailPage.status && detailPage.status != 'CANCEL'\">\r\n                                            <span style=\"color:#0EA8DB\">Shipping History</span>\r\n                                            <table>\r\n\r\n                                                <td>\r\n                                                    <tr>\r\n                                                        <td>Delivery Service :\r\n                                                            <span *ngIf=\"detailPage.delivery_detail\"\r\n                                                                style=\"color:#0EA8DB; font-weight: bold;\">{{detailPage.delivery_detail.awb_number}}</span>\r\n                                                        </td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"detailPage.delivery_detail\">\r\n                                                        <td>Courier : <span>{{detailPage.delivery_detail.courier}}</span> </td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"!detailPage.delivery_detail\">\r\n                                                        <td>Courier : <span>No Service</span> </td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"detailPage.courier && detailPage.courier.supported == 1\">\r\n                                                        <td style=\"color:#0EA8DB\"><b>Courier Kerjasama </b></td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                    <tr *ngIf=\"detailPage.courier && detailPage.courier.supported != 1\">\r\n                                                        <td style=\"color:green\">Courier Lain </td>\r\n                                                        <td></td>\r\n                                                    </tr>\r\n                                                </td>\r\n                                                <td style=\"vertical-align : middle;text-align:center;\">\r\n                                                    <tr rowspan=\"2\">\r\n                                                        Last Status : <br />\r\n                                                        <span\r\n                                                            style=\"font-size: 20px;\"><strong>{{infoDetail}}</strong></span>\r\n                                                        <br />\r\n                                                        {{deliveryHistoryDetail.detailPage}}\r\n\r\n                                                    </tr>\r\n                                                </td>\r\n\r\n                                            </table>\r\n                                        </div>\r\n\r\n                                        <div class=\"product-group\" *ngIf=\"detailPage.status == 'CANCEL'\">\r\n                                            <span style=\"color:#0EA8DB\">Shipping History</span>\r\n                                            <table>\r\n                                                <tr>\r\n                                                    <td>\r\n                                                        <span\r\n                                                            style=\"color:white; font-weight: bold;background-color: red;\">This\r\n                                                            order has been cancelled</span>\r\n                                                    </td>\r\n                                                </tr>\r\n                                            </table>\r\n                                        </div>\r\n\r\n                                        <!-- END SHIPPING HISTORY -->\r\n\r\n                                    </div>\r\n                                    <div class=\"product-group\" *ngIf=\"deliveryHistoryDetail.order_list\">\r\n                                        <span style=\"color:#0EA8DB; font-weight: bold; font-size:14px; \">Order\r\n                                            Summary</span>\r\n                                        <table>\r\n                                            <tr>\r\n                                                <th class=\"product\">Name</th>\r\n                                                <th></th>\r\n                                                <th></th>\r\n                                                <th class=\"price\">Price</th>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>Total Orders</td>\r\n                                                <td></td>\r\n                                                <td></td>\r\n                                                <td class=\"price\">{{deliveryHistoryDetail.billings.total_price | number}}\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr *ngIf=\"deliveryHistoryDetail.billings\">\r\n                                                <td><span *ngIf=\"deliveryHistoryDetail.billings.discount\"> {{deliveryHistoryDetail.billings.discount.product_name}}</span></td>\r\n                                                <td></td>\r\n                                                <td class=\"priceBil\"></td>\r\n                                                <td class=\"priceBil\" *ngIf=\"deliveryHistoryDetail.billings.discount\">\r\n                                                    {{deliveryHistoryDetail.billings.discount.value | number}}\r\n                                                </td>\r\n                                                <td class=\"priceBil\" *ngIf=\" !deliveryHistoryDetail.billings.discount\">\r\n                                                    {{0 | number}}</td>\r\n                                            </tr>\r\n                                            <tr\r\n                                            *ngIf=\"deliveryHistoryDetail.unique_amount && deliveryHistoryDetail.payment_method == 'manual_payment'\">\r\n                                            <td>Unique Amount</td>\r\n                                            <td></td>\r\n                                            <td></td>\r\n                                            <td class=\"price\">{{deliveryHistoryDetail.unique_amount | number}}\r\n                                            </td>\r\n                                        </tr>\r\n                                            <tr style=\"border-top: 1.5px solid #eaeaea; \">\r\n                                                <td>TOTAL PRICE</td>\r\n                                                <td></td>\r\n                                                <td></td>\r\n                                                <td class=\"price\">{{deliveryHistoryDetail.sum_total | number}}\r\n                                                </td>\r\n                                            </tr>\r\n                                        </table>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <!-- MERCHANT PART -->\r\n                                <div *ngIf=\"currentPermission == 'merchant'\">\r\n                                    <div class=\"product-group\" *ngIf=\"deliveryHistoryDetail.products\">\r\n                                        <table>\r\n                                            <tr>\r\n                                                <th class=\"product\">Product Name</th>\r\n                                                <th class=\"qty\">qty</th>\r\n                                                <th class=\"ut\">unit price</th>\r\n                                                <th class=\"price\">price</th>\r\n                                            </tr>\r\n                                            <tr *ngFor=\"let product of deliveryHistoryDetail.products; let i = index\">\r\n                                                <td>{{product.product_name}} <br />\r\n                                                    <em><strong>note</strong>: {{product.note_to_merchant}}</em></td>\r\n                                                <td class=\"qty\">{{product.quantity}}</td>\r\n                                                <td class=\"price\">{{product.fixed_price | number}}</td>\r\n                                                <td class=\"price\">{{product.total_product_price | number}}</td>\r\n                                                <!-- <td class=\"price\">{{deliveryHistoryDetail.shipping_fee}}</td> -->\r\n                                                <!-- <td>{{deliveryHistoryDetail.unique_amount}}</td>  -->\r\n                                            </tr>\r\n                                            <tr *ngFor=\"let bd of deliveryHistoryDetail.additionalBillings\">\r\n                                                <td>{{bd.product_name}}</td>\r\n                                                <td></td>\r\n                                                <td></td>\r\n                                                <td class=\"priceBil\" *ngIf=\"bd.discount == true\">\r\n                                                    {{bd.value  | number}}</td>\r\n                                                <td class=\"fee\" *ngIf=\"bd.discount == false\">\r\n                                                    {{bd.value  | number}}\r\n                                                </td>\r\n\r\n                                            <!-- </tr>\r\n                                            <td>Shipping Fee</td>\r\n                                            <td></td>\r\n                                            <td></td>\r\n                                            <td class=\"fee\">{{deliveryHistoryDetail.shipping_fee | currency: ' Rp '}}</td>\r\n                                            <tr>\r\n                                                <td></td>\r\n                                            </tr> -->\r\n\r\n                                            <tr style=\"border-top: 1.5px solid #eaeaea; width:80%;\">\r\n                                                <td colspan=\"3\" class=\"priceTot\">Total Price</td>\r\n                                                <td class=\"priceTotal\">\r\n                                                    {{deliveryHistoryDetail.sum_total| number}}</td>\r\n                                                <!-- <td *ngIf=\"deliveryHistoryDetail.billings == undefined\" class=\"priceTotal\">\r\n                                                    {{calculate| number}}</td> -->\r\n                                            </tr>\r\n                                        </table>\r\n\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <!-- <div class=\"buyer-detail\" *ngIf=\"deliveryHistoryDetail.destination\">\r\n                                    <div class=\"form-group\">\r\n                                        <div class=\"spacing\">\r\n                                            <label id=\"buyer-detail\">Buyer Detail</label>\r\n                                            <table id=\"custom_table\">\r\n                                                <tr>\r\n                                                    <th>Name</th>\r\n                                                    <th>Address</th>\r\n                                                    <th>Cell Phone</th>\r\n                                                </tr>\r\n                                                <tr>\r\n                                                    <td>{{deliveryHistoryDetail.destination.name}}</td>\r\n                                                    <td>{{deliveryHistoryDetail.destination.address}} , {{deliveryHistoryDetail.destination.district_name}} , {{deliveryHistoryDetail.destination.city_name}} , {{deliveryHistoryDetail.destination.province_name}} , <span *ngIf=\"deliveryHistoryDetail.destination.postal_code\">{{deliveryHistoryDetail.destination.postal_code}}</span></td>\r\n                                                    <td>{{deliveryHistoryDetail.destination.cell_phone}}</td>\r\n                                                </tr>\r\n                                            </table>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div> -->\r\n\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    <!-- <div *ngIf=\"!merchantMode && currentPermission == 'admin'\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\">\r\n                                <h2>Shipping Info</h2>\r\n\r\n                            </div>\r\n                            <div class=\"card-content\">\r\n                                <label>Status :\r\n                                    <strong>\r\n                                        <span style=\"color:#0EA8DB\">{{deliveryHistoryDetail.status}}</span>\r\n                                    </strong>\r\n                                </label>\r\n                            </div>\r\n                            <button class=\"btn_delete cancel-pickup\" (click)=\"cancelShipping()\" *ngIf=\"deliveryHistoryDetail.shipping_info && isArray(deliveryHistoryDetail.shipping_info) && deliveryHistoryDetail.shipping_info.length > 0 && deliveryHistoryDetail.shipping_info.at(-1).label == 'on_delivery'\">\r\n                                <i class=\"fa fa-fw fa-times-circle\"></i>\r\n                                <span>Cancel Pickup</span>\r\n                            </button>\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    <div>\r\n                        <span style=\"color:#0ea8db; font-weight: bold;\">Order Id : </span>{{deliveryHistoryDetail.order_id}}\r\n                    </div>\r\n                    <div class=\"see-order-button\">\r\n                        <button class=\"btn btn-outline-primary mb-2 mr-2\" (click)=\"openScrollableContent(longContent)\">Lihat Order Detail</button>\r\n                    </div>\r\n                    <div class=\"see-order-text\">\r\n                        Klik <strong>Lihat Order Detail</strong> untuk melihat semua pengiriman dalam Order ID MCI-439020029559694696-DEV\r\n                    </div>\r\n\r\n                    <div *ngIf=\"deliveryHistoryDetail.destination\" class=\"destination-card\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\">\r\n                                <h2>Buyer Detail</h2>\r\n\r\n                            </div>\r\n                            <table id=\"custom_table\">\r\n                                <tr>\r\n                                    <td class=\"first-column\">Name</td>\r\n                                    <td class=\"second-column\"> : </td>\r\n                                    <td colspan=\"5\">{{deliveryHistoryDetail.destination.name}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td class=\"first-column\">Address</td>\r\n                                    <td class=\"second-column\"> : </td>\r\n                                    <td>{{deliveryHistoryDetail.destination.address}} , {{deliveryHistoryDetail.destination.district_name}} , {{deliveryHistoryDetail.destination.city_name}} , {{deliveryHistoryDetail.destination.province_name}} , <span *ngIf=\"deliveryHistoryDetail.destination.postal_code\">{{deliveryHistoryDetail.destination.postal_code}}</span></td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td class=\"first-column\">Cell Phone</td>\r\n                                    <td class=\"second-column\"> : </td>\r\n                                    <td>{{deliveryHistoryDetail.destination.cell_phone}}</td>\r\n                                </tr>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div *ngIf=\"!merchantMode && currentPermission == 'admin' && detailPage\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\">\r\n                                <h2>Shipping Info</h2>\r\n\r\n                            </div>\r\n                            <div class=\"card-content\">\r\n                                <label>Delivery Service :\r\n                                    <strong>\r\n                                        <span *ngIf=\"detailPage.delivery_detail\" style=\"color:#0EA8DB; font-weight: bold;\">{{detailPage.delivery_detail.awb_number}}</span>\r\n                                    </strong>\r\n                                </label>\r\n                                <label>Courier :\r\n                                    <strong>\r\n                                        <span *ngIf=\"detailPage.delivery_detail\" style=\"color:#0EA8DB; font-weight: bold;\">{{detailPage.delivery_detail.courier}}</span>\r\n                                    </strong>\r\n                                </label>     \r\n                                <label>Last Status :\r\n                                    <strong>\r\n                                        <span *ngIf=\"detailPage.delivery_detail\" style=\"color:#0EA8DB; font-weight: bold;\">{{infoDetail}}</span>\r\n                                        <br />\r\n                                                        {{deliveryHistoryDetail.detailPage}}\r\n                                    </strong>\r\n                                </label>               \r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div *ngIf=\"deliveryHistoryDetail.status == 'CANCEL' && currentPermission == 'merchant' || deliveryHistoryDetail.status == 'CANCEL' && currentPermission == 'admin'\"\r\n                        class=\"cancel-order\">\r\n                        <div class=\"card mb-3\">\r\n                            <label class=\"cancel-status\">Status : <span style=\"color:red;\">Order Cancelled</span></label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!-- <div class=\"card mb-3\">\r\n                     \r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Email </label>\r\n                            <form-input name=\"email\" [type]=\"'text'\" [placeholder]=\"'Email'\"  [(ngModel)]=\"deliveryHistoryDetail.email\" autofocus required></form-input>\r\n\r\n                            <label>Cell Phone </label>\r\n                            <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Cell Phone'\"  [(ngModel)]=\"deliveryHistoryDetail.cell_phone\" autofocus required></form-input>\r\n                            \r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>  -->\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div *ngIf=\"!deliveryHistoryDetail\" class=\"default-loading\">\r\n        <i class=\"fa fa-spinner fa-pulse\"></i>\r\n    </div>\r\n\r\n</div>\r\n\r\n<ng-template #longContent let-modal>\r\n    <!-- <div class=\"outer\" *ngIf=\"deliveryHistoryDetail && deliveryHistoryDetail.track_history\">\r\n        <div class=\"progress\">\r\n            <div class=\"left\">\r\n                <div *ngFor=\"let track of deliveryHistoryDetail.track_history; let i = index\">\r\n                    <div class=\"created-date\">{{track.create_date}}</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"right\">\r\n                <div *ngFor=\"let track of deliveryHistoryDetail.track_history; let i = index\" [ngClass]=\"i == deliveryHistoryDetail.track_history.length - 1? 'current' : ''\">\r\n                    <div class=\"rowstate-name\">{{track.rowstate_name}}</div>\r\n                    <div class=\"description\" *ngIf=\"track.description\">{{track.description}}</div>\r\n                </div>\r\n            </div>\r\n        </div>  \r\n    </div> -->\r\n    <app-delivery-process-edit [back]=\"[this,backToHere]\" [detail]=\"orderDetail\" [propsDetail]=\"propsDetail\" class=\"parent-container\"></app-delivery-process-edit>\r\n\r\n</ng-template>\r\n\r\n<ng-template #trackingContent let-modal>\r\n    <div class=\"outer\" *ngIf=\"deliveryHistoryDetail && deliveryHistoryDetail.track_history\">\r\n        <div class=\"progress\">\r\n            <div class=\"left\">\r\n                <div *ngFor=\"let track of deliveryHistoryDetail.track_history; let i = index\">\r\n                    <div class=\"created-date\">{{track.create_date}}</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"right\">\r\n                <div *ngFor=\"let track of deliveryHistoryDetail.track_history; let i = index\" [ngClass]=\"i == deliveryHistoryDetail.track_history.length - 1? 'current' : ''\">\r\n                    <div class=\"rowstate-name\">{{track.rowstate_name}}</div>\r\n                    <div class=\"description\" *ngIf=\"track.description\">{{track.description}}</div>\r\n                    <div *ngIf=\"track.pod_camera\" class=\"photo\" style=\"padding-bottom:20px;\">\r\n                            <a href=\"{{track.pod_camera}}\" target=\"_blank\" style=\"text-decoration: none;\">\r\n                                <button class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Lihat Foto Penerima</button>\r\n                            </a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>  \r\n    </div>\r\n    <!-- <app-delivery-process-edit [back]=\"[this,backToHere]\" [detail]=\"orderDetail\" [propsDetail]=\"propsDetail\" class=\"parent-container\"></app-delivery-process-edit> -->\r\n\r\n</ng-template>\r\n"

/***/ }),

/***/ "./src/app/layout/modules/delivery-process/delivery-process-routing.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/delivery-process-routing.module.ts ***!
  \************************************************************************************/
/*! exports provided: DeliveryProcessRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryProcessRoutingModule", function() { return DeliveryProcessRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _delivery_process_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./delivery-process.component */ "./src/app/layout/modules/delivery-process/delivery-process.component.ts");
/* harmony import */ var _shipping_label_shipping_label_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shipping-label/shipping-label.component */ "./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '', component: _delivery_process_component__WEBPACK_IMPORTED_MODULE_2__["DeliveryProcessComponent"],
    },
    {
        path: 'shipping-label', component: _shipping_label_shipping_label_component__WEBPACK_IMPORTED_MODULE_3__["ShippingLabelComponent"],
    },
];
var DeliveryProcessRoutingModule = /** @class */ (function () {
    function DeliveryProcessRoutingModule() {
    }
    DeliveryProcessRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DeliveryProcessRoutingModule);
    return DeliveryProcessRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/delivery-process/delivery-process.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/delivery-process.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  left: 0px;\n  top: 50px;\n}\n\n*:focus {\n  box-shadow: unset;\n}\n\ndiv,\nspan:not(.fa) {\n  font-family: \"Poppins\";\n}\n\n.board {\n  background-color: #fcfcfc;\n  height: 100%;\n  width: 100%;\n  min-width: 720px;\n}\n\n.nav.toolbar {\n  background: #fcfcfc;\n  height: 150px;\n  padding-top: 40px;\n  padding-bottom: 20px;\n  margin-left: 200px;\n  justify-content: center;\n  align-items: center;\n}\n\n.nav.toolbar .parent-container {\n  width: 100%;\n}\n\n.nav.toolbar .select-all {\n  width: 100%;\n  align-items: right;\n}\n\n.nav.toolbar .card {\n  padding: 20px;\n  width: 95%;\n  align-items: center;\n  margin-bottom: 20px;\n}\n\n.nav.toolbar .card h3 {\n  text-align: center;\n}\n\n.nav.toolbar .card tr,\n.nav.toolbar .card td {\n  color: black;\n  vertical-align: top;\n}\n\n.nav.toolbar .card tr select,\n.nav.toolbar .card td select {\n  height: 30px;\n}\n\n.nav.toolbar .card .card-content {\n  width: 100%;\n}\n\n.nav.toolbar .card .card-content hr {\n  margin-top: 0rem;\n  margin-bottom: 0rem;\n  border: 0;\n  border-top: 1px solid rgba(0, 0, 0, 0.1);\n}\n\n.nav.toolbar .card .card-content table {\n  border-collapse: collapse;\n  border-radius: 5px;\n  width: 95%;\n}\n\n.nav.toolbar .card .card-content button {\n  color: #34495e;\n  border-radius: 10px;\n  background: white;\n  font-size: 15px;\n  margin-top: 10px;\n  border: 2px solid #eaeaea;\n  width: 95%;\n  padding: 16px;\n}\n\n.nav.toolbar .card .card-content button.true {\n  background: #2c3e50;\n  color: white;\n}\n\n@media screen and (max-width: 992px) {\n  .nav.toolbar {\n    margin-left: 0px;\n  }\n}\n\n.form-control {\n  font-size: 11px;\n  width: 150px;\n  display: inline-block;\n  margin: 2px 0px 2px 0px;\n}\n\n.div-transaction {\n  display: flex;\n  flex-wrap: wrap;\n  font-size: 11px;\n  width: 100%;\n}\n\n.div-transaction label {\n  margin-bottom: 0px;\n}\n\n.div-transaction div.searchbar {\n  flex-wrap: wrap;\n  display: inherit;\n  width: calc(100% - 140px);\n  padding-left: 10px;\n  justify-content: flex-end;\n}\n\n.div-transaction div.searchbar div {\n  display: flex;\n  align-items: center;\n}\n\n.div-transaction div.searchbar div label {\n  margin-right: 5px;\n}\n\n.div-footer {\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  width: 100%;\n  text-align: -webkit-right;\n  margin-bottom: 10px;\n  display: flex;\n  float: right;\n}\n\n.div-footer div {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: auto;\n  padding: 0px;\n  margin-bottom: 5px;\n  border: 1px solid rgba(0, 0, 0, 0.1);\n  border-radius: 10px;\n}\n\n.div-footer div button {\n  color: #34495e;\n  background: white;\n  border-radius: 10px;\n}\n\n.div-footer div button:hover {\n  background: #2c3e50;\n  color: white;\n  border-radius: 10px;\n}\n\n.div-footer div button:active {\n  background: #2c3e50;\n  color: white;\n  border-radius: 10px;\n}\n\n.div-footer div button:focus {\n  background: #2c3e50;\n  color: white;\n  border-radius: 10px;\n}\n\n.div-content-row {\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  border: 1px solid #ccc;\n  align-items: unset;\n  justify-content: unset;\n  border-radius: 10px;\n  margin-bottom: 15px;\n  padding: 10px;\n}\n\n.div-content-row .div-content-cell {\n  padding: 5px 12px;\n  max-width: unset;\n  border-right: 1px solid #ccc;\n  width: calc(100% - 560px);\n}\n\n.div-content-row .div-content-cell:last-child {\n  border-right: 0px solid transparent;\n  width: 280px;\n}\n\n.div-content-row > .div-content-cell:nth-child(2) {\n  width: 280px;\n}\n\n.default-product .product-detail .date-text {\n  font-size: 11px;\n  margin-bottom: 15px;\n  margin-top: 10px;\n}\n\n.more-product {\n  overflow-y: auto;\n  overflow-x: auto;\n  display: grid;\n  height: 200px;\n  padding: 0px;\n}\n\n.more-product .loop-product {\n  display: flex;\n  margin-bottom: 10px;\n  padding: 0px;\n}\n\n.more-product .loop-product .img-wrapper {\n  padding: 0px 20px 0px 0px;\n}\n\n.more-product .loop-product .img-wrapper .image {\n  border-radius: 5%;\n  width: 120px;\n  height: auto;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.div-default-product {\n  display: grid;\n  padding: 0px;\n}\n\n.div-default-product .default-product {\n  display: flex;\n  margin-bottom: 10px;\n  padding: 0px;\n}\n\n.div-default-product .default-product .img-wrapper {\n  padding: 0px 20px 0px 0px;\n}\n\n.div-default-product .default-product .img-wrapper .image {\n  border-radius: 5%;\n  width: 120px;\n  height: auto;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.products-left {\n  width: 20%;\n}\n\n.products-left .products-left-board {\n  display: flex;\n  padding: 0px;\n  overflow: hidden;\n  width: 100%;\n}\n\n.products-left .products-left-board .products-left-data {\n  padding: 0px;\n  width: calc(100% - 20px);\n  display: grid;\n}\n\n.products-left .products-left-board .products-left-checkbox {\n  width: 20px;\n  display: flex;\n  justify-content: center;\n  padding: 0px;\n}\n\n.products-left .products-left-status {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  padding-right: 0px;\n  padding-left: 0px;\n  flex-direction: row;\n  font-size: 13px;\n  color: #0ea8da;\n  padding-top: 10px;\n}\n\n.products-left .products-left-status .see-more {\n  flex: 1;\n  color: #0ea8da;\n  cursor: pointer;\n  text-align: right;\n}\n\n.products-left .products-left-status .see-more:hover {\n  color: #0275d8;\n}\n\n.products-left .products-left-status .col-md-3 {\n  position: relative;\n  justify-items: left;\n  padding-left: 0px;\n  padding-bottom: 0px;\n}\n\n.products-left .products-left-status .col-md-9 {\n  text-align: right;\n  width: 100%;\n  max-width: unset;\n  display: block;\n  padding-right: 0px;\n  padding-top: 0px;\n}\n\n#h3 {\n  width: 95%;\n  margin-bottom: 24px;\n  color: black;\n}\n\n#sku {\n  font-size: 14px;\n  color: #bebebe;\n  margin-bottom: 0px;\n}\n\n.price {\n  color: #0ea8da;\n}\n\n.price-total {\n  color: #0ea8da;\n}\n\n.normal-text {\n  font-size: 15px;\n  color: #777777;\n}\n\n.normal-text.date-text {\n  font-size: 12px;\n}\n\n.header-text {\n  font-size: 12px;\n  font-weight: bold;\n  margin-bottom: 1px;\n}\n\n.header-text.order-id-text {\n  font-size: 12px;\n  color: #0ea8da;\n}\n\n.header-text.order-id-text span {\n  font-size: 12px;\n  color: #777777;\n}\n\n.form-label > label {\n  display: block;\n}\n\n.form-label .product-name {\n  font-size: 12px;\n}\n\n.see-more-normal-text {\n  font-size: 13px;\n  color: #777777;\n}\n\n.see-more-normal-text.date-text {\n  font-size: 8px;\n}\n\n.see-more-header-text {\n  font-size: 10px;\n  font-weight: bold;\n  margin-bottom: 1px;\n}\n\n.see-more-header-text.order-id-text {\n  font-size: 12px;\n  color: #0ea8da;\n}\n\n.see-more-header-text.order-id-text span {\n  font-size: 10px;\n  color: #777777;\n}\n\n.see-more-form-label > label {\n  display: block;\n}\n\n.see-more-form-label .product-name {\n  font-size: 10px;\n}\n\n.price-see-more {\n  color: #0ea8da;\n  font-size: 10px;\n}\n\n#transaction {\n  width: 130px;\n}\n\n#download {\n  vertical-align: middle;\n}\n\n#label-header {\n  font-size: 20px;\n  color: black;\n}\n\n#button-swapper {\n  margin-left: 5px;\n  margin-right: 5px;\n  width: calc((100% / 6) - 10px);\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\n#card-transaction {\n  width: 100%;\n  align-items: baseline;\n}\n\n#search {\n  margin-right: 20px;\n}\n\n#search-field {\n  margin-right: 10px;\n}\n\n#search-text {\n  padding-right: 23px;\n}\n\n#search-icon {\n  margin-left: -20px;\n  margin-right: 20px;\n}\n\n#btn-print {\n  font-size: 11px;\n  width: 130px;\n  margin: 15px 20px 2px 0px;\n  height: 30px;\n  border-radius: 15px;\n}\n\n#btn-download {\n  font-size: 11px;\n  width: 130px;\n  margin: 15px 20px 2px 0px;\n  height: 30px;\n  border-radius: 15px;\n}\n\n#calendar-icon {\n  margin-left: -20px;\n  margin-right: 20px;\n}\n\n.payment-status {\n  padding: 2px 5px;\n  display: flex;\n  text-align: center;\n  align-items: center;\n  justify-content: center;\n  margin-bottom: 5px;\n  width: 97px;\n  height: 34px;\n  font-size: 14px;\n  border-radius: 35px;\n}\n\n.payment-status.PAID {\n  color: white;\n  background-color: #0ea8da;\n  text-align: center;\n}\n\n.payment-status.CANCEL {\n  color: white;\n  background-color: #e74c3c;\n  text-align: center;\n}\n\n.date-picker-wrapper {\n  position: relative;\n  margin-right: 10px;\n}\n\n.date-picker-wrapper .date-picker-overlay {\n  z-index: 99;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker {\n  background: white;\n  overflow: hidden;\n  border: 1px solid #dfdfdf;\n  position: absolute;\n  top: 30px;\n  display: grid;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker .btn.ok {\n  clear: both;\n  background-color: #3498db;\n  color: white;\n  margin: 0px 5px 10px;\n  border-color: transparent;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker .btn.clear {\n  clear: both;\n  background-color: #ac0a0a;\n  color: white;\n  margin: 0px 10px 10px;\n  border-color: transparent;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker ngb-datepicker {\n  float: left;\n  clear: both;\n  margin: 10px;\n  right: 0%;\n}\n\n.date-picker-wrapper .date-picker-overlay.false {\n  display: none;\n}\n\n.custom-day {\n  text-align: center;\n  padding: 0.185rem 0.25rem;\n  display: inline-block;\n  height: 2rem;\n  width: 2rem;\n}\n\n.custom-day.focused {\n  background-color: #e6e6e6;\n}\n\n.custom-day.range,\n.custom-day:hover {\n  background-color: #0275d8;\n  color: white;\n}\n\n.custom-day.faded {\n  background-color: rgba(2, 117, 216, 0.5);\n}\n\n.order-status {\n  display: flex;\n  flex-wrap: wrap;\n  place-content: center;\n}\n\n.order-status .status-name {\n  position: relative;\n  justify-content: center;\n  flex-direction: row;\n}\n\n.order-status .status-name .badge {\n  display: flex;\n  position: absolute;\n  justify-content: center;\n  align-items: center;\n  font-size: 10px;\n  color: white;\n  border-radius: 50px;\n  left: -15px;\n  width: 23px;\n  height: 23px;\n  background-color: rgba(231, 76, 60, 0.8);\n  border: 1px solid #e74c3c;\n  top: -14px;\n}\n\n.loading {\n  margin-bottom: 20px;\n}\n\n.sk-fading-circle {\n  margin: 10px auto;\n  width: 40px;\n  height: 40px;\n  position: relative;\n}\n\n.sk-fading-circle .sk-circle {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  left: 0;\n  top: 0;\n}\n\n.sk-fading-circle .sk-circle:before {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 15%;\n  height: 15%;\n  background-color: #333;\n  border-radius: 100%;\n  -webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;\n  animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;\n}\n\n.sk-fading-circle .sk-circle2 {\n  -webkit-transform: rotate(30deg);\n  transform: rotate(30deg);\n}\n\n.sk-fading-circle .sk-circle3 {\n  -webkit-transform: rotate(60deg);\n  transform: rotate(60deg);\n}\n\n.sk-fading-circle .sk-circle4 {\n  -webkit-transform: rotate(90deg);\n  transform: rotate(90deg);\n}\n\n.sk-fading-circle .sk-circle5 {\n  -webkit-transform: rotate(120deg);\n  transform: rotate(120deg);\n}\n\n.sk-fading-circle .sk-circle6 {\n  -webkit-transform: rotate(150deg);\n  transform: rotate(150deg);\n}\n\n.sk-fading-circle .sk-circle7 {\n  -webkit-transform: rotate(180deg);\n  transform: rotate(180deg);\n}\n\n.sk-fading-circle .sk-circle8 {\n  -webkit-transform: rotate(210deg);\n  transform: rotate(210deg);\n}\n\n.sk-fading-circle .sk-circle9 {\n  -webkit-transform: rotate(240deg);\n  transform: rotate(240deg);\n}\n\n.sk-fading-circle .sk-circle10 {\n  -webkit-transform: rotate(270deg);\n  transform: rotate(270deg);\n}\n\n.sk-fading-circle .sk-circle11 {\n  -webkit-transform: rotate(300deg);\n  transform: rotate(300deg);\n}\n\n.sk-fading-circle .sk-circle12 {\n  -webkit-transform: rotate(330deg);\n  transform: rotate(330deg);\n}\n\n.sk-fading-circle .sk-circle2:before {\n  -webkit-animation-delay: -1.1s;\n  animation-delay: -1.1s;\n}\n\n.sk-fading-circle .sk-circle3:before {\n  -webkit-animation-delay: -1s;\n  animation-delay: -1s;\n}\n\n.sk-fading-circle .sk-circle4:before {\n  -webkit-animation-delay: -0.9s;\n  animation-delay: -0.9s;\n}\n\n.sk-fading-circle .sk-circle5:before {\n  -webkit-animation-delay: -0.8s;\n  animation-delay: -0.8s;\n}\n\n.sk-fading-circle .sk-circle6:before {\n  -webkit-animation-delay: -0.7s;\n  animation-delay: -0.7s;\n}\n\n.sk-fading-circle .sk-circle7:before {\n  -webkit-animation-delay: -0.6s;\n  animation-delay: -0.6s;\n}\n\n.sk-fading-circle .sk-circle8:before {\n  -webkit-animation-delay: -0.5s;\n  animation-delay: -0.5s;\n}\n\n.sk-fading-circle .sk-circle9:before {\n  -webkit-animation-delay: -0.4s;\n  animation-delay: -0.4s;\n}\n\n.sk-fading-circle .sk-circle10:before {\n  -webkit-animation-delay: -0.3s;\n  animation-delay: -0.3s;\n}\n\n.sk-fading-circle .sk-circle11:before {\n  -webkit-animation-delay: -0.2s;\n  animation-delay: -0.2s;\n}\n\n.sk-fading-circle .sk-circle12:before {\n  -webkit-animation-delay: -0.1s;\n  animation-delay: -0.1s;\n}\n\n.see-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  padding-right: 0px;\n  padding-left: 0px;\n  flex-direction: row;\n  font-size: 13px;\n  color: #0ea8da;\n  padding-top: 10px;\n}\n\n.see-more-container .see-more {\n  flex: 1;\n  color: #0ea8da;\n  cursor: pointer;\n  text-align: right;\n}\n\n.see-more-container .see-more:hover {\n  color: #0275d8;\n}\n\n@media screen and (max-width: 1280px) {\n  .default-product {\n    flex-direction: column;\n  }\n  .default-product .product-detail .header-text.order-id-text {\n    margin-top: 16px;\n  }\n  .default-product .product-detail .header-text span {\n    font-size: 12px;\n    display: block;\n  }\n}\n\n@media screen and (max-width: 1080px) {\n  .default-product {\n    width: 100%;\n    overflow: hidden;\n  }\n  .default-product .product-detail .header-text span {\n    display: block;\n  }\n  .default-product .product-detail .date-text {\n    font-size: 11px;\n  }\n\n  .div-default-product .default-product .img-wrapper {\n    width: 100%;\n  }\n}\n\n@-webkit-keyframes sk-circleFadeDelay {\n  0%, 39%, 100% {\n    opacity: 0;\n  }\n  40% {\n    opacity: 1;\n  }\n}\n\n@keyframes sk-circleFadeDelay {\n  0%, 39%, 100% {\n    opacity: 0;\n  }\n  40% {\n    opacity: 1;\n  }\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n\n.div-product {\n  position: relative;\n}\n\n.printed-stamp {\n  position: absolute;\n  top: 30px;\n  right: 30px;\n  display: inline-block;\n  color: green;\n  padding: 14px;\n  background-color: white;\n  box-shadow: inset 0px 0px 0px 4px green;\n  font-size: 30px;\n  height: 76px;\n  font-weight: bold;\n  -webkit-transform: rotate(-20deg);\n          transform: rotate(-20deg);\n  width: 150px;\n  text-align: center;\n}\n\n.printed-stamp:before {\n  content: \" \";\n  position: absolute;\n  z-index: -1;\n  top: 7px;\n  left: 7px;\n  right: 7px;\n  bottom: 7px;\n  border: 2px solid green;\n}\n\n.shipping-info {\n  color: green;\n  font-weight: bold;\n}\n\n.red-return {\n  font-size: 12px;\n  color: red;\n}\n\n.green-return {\n  font-size: 12px;\n  color: green;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGVsaXZlcnktcHJvY2Vzcy9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGRlbGl2ZXJ5LXByb2Nlc3NcXGRlbGl2ZXJ5LXByb2Nlc3MuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2RlbGl2ZXJ5LXByb2Nlc3MvZGVsaXZlcnktcHJvY2Vzcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQTtFQUNDLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtBQ0hEOztBREtBO0VBQ0MsaUJBQUE7QUNGRDs7QURJQTs7RUFFQyxzQkFBQTtBQ0REOztBRElBO0VBQ0MseUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDREQ7O0FESUE7RUFDQyxtQkFBQTtFQUVBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDRkQ7O0FESUM7RUFDQyxXQUFBO0FDRkY7O0FES0M7RUFDQyxXQUFBO0VBQ0Esa0JBQUE7QUNIRjs7QURLQztFQUNDLGFBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0hGOztBRElFO0VBQ0Msa0JBQUE7QUNGSDs7QURJRTs7RUFFQyxZQUFBO0VBQ0EsbUJBQUE7QUNGSDs7QURHRzs7RUFDQyxZQUFBO0FDQUo7O0FER0U7RUFDQyxXQUFBO0FDREg7O0FERUc7RUFDQyxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsU0FBQTtFQUNBLHdDQUFBO0FDQUo7O0FERUc7RUFDQyx5QkFBQTtFQUVBLGtCQUFBO0VBQ0EsVUFBQTtBQ0RKOztBREdHO0VBQ0MsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0FDREo7O0FER0c7RUFDQyxtQkFBQTtFQUNBLFlBQUE7QUNESjs7QURPQTtFQUNDO0lBQ0UsZ0JBQUE7RUNKRDtBQUNGOztBRE9BO0VBQ0MsZUFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLHVCQUFBO0FDTEQ7O0FEUUE7RUFDQyxhQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDTEQ7O0FETUM7RUFDQyxrQkFBQTtBQ0pGOztBRE1DO0VBQ0MsZUFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FDSkY7O0FES0U7RUFDQyxhQUFBO0VBQ0EsbUJBQUE7QUNISDs7QURJRztFQUNDLGlCQUFBO0FDRko7O0FEUUE7RUFDQywyQkFBQTtFQUFBLHdCQUFBO0VBQUEsbUJBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FDTEQ7O0FETUM7RUFDQywwQkFBQTtFQUFBLHVCQUFBO0VBQUEsa0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esb0NBQUE7RUFDQSxtQkFBQTtBQ0pGOztBREtFO0VBQ0MsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNISDs7QURLRTtFQUNDLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDSEg7O0FETUU7RUFDQyxtQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ0pIOztBRE9FO0VBQ0MsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUNMSDs7QURVQTtFQUNDLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQ1BEOztBRFNDO0VBR0MsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLDRCQUFBO0VBQ0EseUJBQUE7QUNURjs7QURZQztFQUNDLG1DQUFBO0VBQ0EsWUFBQTtBQ1ZGOztBRFlDO0VBQ0MsWUFBQTtBQ1ZGOztBRGdCRTtFQUNDLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDYkg7O0FEa0JBO0VBQ0MsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ2ZEOztBRGdCQztFQUNDLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNkRjs7QURlRTtFQUNDLHlCQUFBO0FDYkg7O0FEbUJHO0VBQ0MsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7QUNqQko7O0FENENBO0VBQ0MsYUFBQTtFQUNBLFlBQUE7QUN6Q0Q7O0FEMENDO0VBQ0MsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ3hDRjs7QUR5Q0U7RUFDQyx5QkFBQTtBQ3ZDSDs7QUQ2Q0c7RUFDQyxpQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQzNDSjs7QURpREE7RUFDQyxVQUFBO0FDOUNEOztBRCtDQztFQUNDLGFBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FDN0NGOztBRDhDRTtFQUNDLFlBQUE7RUFDQSx3QkFBQTtFQUNBLGFBQUE7QUM1Q0g7O0FEOENFO0VBQ0MsV0FBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7QUM1Q0g7O0FEK0NDO0VBQ0MsYUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUM3Q0Y7O0FEOENFO0VBQ0MsT0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUM1Q0g7O0FEOENFO0VBQ0MsY0FBQTtBQzVDSDs7QUQ4Q0U7RUFDQyxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQzVDSDs7QUQ4Q0U7RUFDQyxpQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDNUNIOztBRHFGQTtFQUNDLFVBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNsRkQ7O0FEcUZBO0VBQ0MsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQ2xGRDs7QURxRkE7RUFDQyxjQUFBO0FDbEZEOztBRHFGQTtFQUNDLGNBQUE7QUNsRkQ7O0FEcUZBO0VBQ0MsZUFBQTtFQUNBLGNBdFlZO0FDb1RiOztBRHFGQTtFQUNDLGVBQUE7QUNsRkQ7O0FEcUZBO0VBQ0MsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNsRkQ7O0FEb0ZBO0VBQ0MsZUFBQTtFQUNBLGNBdFphO0FDcVVkOztBRGtGQztFQUNDLGVBQUE7RUFDQSxjQXZaVztBQ3VVYjs7QURxRkM7RUFDQyxjQUFBO0FDbEZGOztBRG9GQztFQUNDLGVBQUE7QUNsRkY7O0FEc0ZBO0VBQ0MsZUFBQTtFQUNBLGNBdGFZO0FDbVZiOztBRHNGQTtFQUNDLGNBQUE7QUNuRkQ7O0FEc0ZBO0VBQ0MsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNuRkQ7O0FEcUZBO0VBQ0MsZUFBQTtFQUNBLGNBdGJhO0FDb1dkOztBRG1GQztFQUNDLGVBQUE7RUFDQSxjQXZiVztBQ3NXYjs7QURzRkM7RUFDQyxjQUFBO0FDbkZGOztBRHFGQztFQUNDLGVBQUE7QUNuRkY7O0FEdUZBO0VBQ0MsY0FBQTtFQUNBLGVBQUE7QUNwRkQ7O0FEdUZBO0VBQ0MsWUFBQTtBQ3BGRDs7QUR1RkE7RUFDQyxzQkFBQTtBQ3BGRDs7QUR1RkE7RUFDQyxlQUFBO0VBQ0EsWUFBQTtBQ3BGRDs7QUR1RkE7RUFDQyxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7QUNwRkQ7O0FEdUZBO0VBQ0MsV0FBQTtFQUNBLHFCQUFBO0FDcEZEOztBRHVGQTtFQUNDLGtCQUFBO0FDcEZEOztBRHVGQTtFQUNDLGtCQUFBO0FDcEZEOztBRHVGQTtFQUNDLG1CQUFBO0FDcEZEOztBRHVGQTtFQUNDLGtCQUFBO0VBQ0Esa0JBQUE7QUNwRkQ7O0FEdUZBO0VBQ0MsZUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ3BGRDs7QUR1RkE7RUFDQyxlQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDcEZEOztBRHVGQTtFQUNDLGtCQUFBO0VBQ0Esa0JBQUE7QUNwRkQ7O0FEdUZBO0VBQ0MsZ0JBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ3BGRDs7QURzRkE7RUFDQyxZQUFBO0VBQ0EseUJBMWhCYTtFQTJoQmIsa0JBQUE7QUNuRkQ7O0FEc0ZBO0VBQ0MsWUFBQTtFQUNBLHlCQS9oQlU7RUFnaUJWLGtCQUFBO0FDbkZEOztBRHNGQTtFQUNDLGtCQUFBO0VBQ0Esa0JBQUE7QUNuRkQ7O0FEcUZDO0VBQ0MsV0FBQTtBQ25GRjs7QURvRkU7RUFDQyxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxhQUFBO0FDbEZIOztBRG9GRztFQUNDLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtFQUNBLHlCQUFBO0FDbEZKOztBRG9GRztFQUNDLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0FDbEZKOztBRG9GRztFQUNDLFdBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7QUNsRko7O0FEdUZDO0VBQ0MsYUFBQTtBQ3JGRjs7QUR5RkE7RUFDQyxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ3RGRDs7QUR5RkE7RUFDQyx5QkFBQTtBQ3RGRDs7QUR3RkE7O0VBRUMseUJBQUE7RUFDQSxZQUFBO0FDckZEOztBRHVGQTtFQUNDLHdDQUFBO0FDcEZEOztBRHVGQTtFQUNDLGFBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7QUNwRkQ7O0FEcUZDO0VBQ0Msa0JBQUE7RUFFQSx1QkFBQTtFQUNBLG1CQUFBO0FDcEZGOztBRHFGRTtFQUNDLGFBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esd0NBQUE7RUFDQSx5QkFBQTtFQUNBLFVBQUE7QUNuRkg7O0FEeUZBO0VBQ0MsbUJBQUE7QUN0RkQ7O0FEeUZBO0VBQ0MsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDdEZEOztBRHlGQTtFQUNDLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsTUFBQTtBQ3RGRDs7QUR5RkE7RUFDQyxXQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvRUFBQTtFQUNBLDREQUFBO0FDdEZEOztBRHdGQTtFQUNDLGdDQUFBO0VBRUEsd0JBQUE7QUNyRkQ7O0FEdUZBO0VBQ0MsZ0NBQUE7RUFFQSx3QkFBQTtBQ3BGRDs7QURzRkE7RUFDQyxnQ0FBQTtFQUVBLHdCQUFBO0FDbkZEOztBRHFGQTtFQUNDLGlDQUFBO0VBRUEseUJBQUE7QUNsRkQ7O0FEb0ZBO0VBQ0MsaUNBQUE7RUFFQSx5QkFBQTtBQ2pGRDs7QURtRkE7RUFDQyxpQ0FBQTtFQUVBLHlCQUFBO0FDaEZEOztBRGtGQTtFQUNDLGlDQUFBO0VBRUEseUJBQUE7QUMvRUQ7O0FEaUZBO0VBQ0MsaUNBQUE7RUFFQSx5QkFBQTtBQzlFRDs7QURnRkE7RUFDQyxpQ0FBQTtFQUVBLHlCQUFBO0FDN0VEOztBRCtFQTtFQUNDLGlDQUFBO0VBRUEseUJBQUE7QUM1RUQ7O0FEOEVBO0VBQ0MsaUNBQUE7RUFFQSx5QkFBQTtBQzNFRDs7QUQ2RUE7RUFDQyw4QkFBQTtFQUNBLHNCQUFBO0FDMUVEOztBRDRFQTtFQUNDLDRCQUFBO0VBQ0Esb0JBQUE7QUN6RUQ7O0FEMkVBO0VBQ0MsOEJBQUE7RUFDQSxzQkFBQTtBQ3hFRDs7QUQwRUE7RUFDQyw4QkFBQTtFQUNBLHNCQUFBO0FDdkVEOztBRHlFQTtFQUNDLDhCQUFBO0VBQ0Esc0JBQUE7QUN0RUQ7O0FEd0VBO0VBQ0MsOEJBQUE7RUFDQSxzQkFBQTtBQ3JFRDs7QUR1RUE7RUFDQyw4QkFBQTtFQUNBLHNCQUFBO0FDcEVEOztBRHNFQTtFQUNDLDhCQUFBO0VBQ0Esc0JBQUE7QUNuRUQ7O0FEcUVBO0VBQ0MsOEJBQUE7RUFDQSxzQkFBQTtBQ2xFRDs7QURvRUE7RUFDQyw4QkFBQTtFQUNBLHNCQUFBO0FDakVEOztBRG1FQTtFQUNDLDhCQUFBO0VBQ0Esc0JBQUE7QUNoRUQ7O0FEb0VBO0VBQ0MsYUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUNqRUQ7O0FEa0VDO0VBQ0MsT0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNoRUY7O0FEa0VDO0VBQ0MsY0FBQTtBQ2hFRjs7QURvRUE7RUFDQztJQUNDLHNCQUFBO0VDakVBO0VEbUVDO0lBQ0MsZ0JBQUE7RUNqRUY7RURvRUU7SUFDQyxlQUFBO0lBQ0EsY0FBQTtFQ2xFSDtBQUNGOztBRHdFQTtFQUNDO0lBQ0MsV0FBQTtJQUNBLGdCQUFBO0VDdEVBO0VEeUVFO0lBQ0MsY0FBQTtFQ3ZFSDtFRDBFQztJQUNDLGVBQUE7RUN4RUY7O0VEK0VDO0lBQ0MsV0FBQTtFQzVFRjtBQUNGOztBRHVGQTtFQUNDO0lBR0MsVUFBQTtFQ3ZGQTtFRHlGRDtJQUNDLFVBQUE7RUN2RkE7QUFDRjs7QUQwRkE7RUFDQztJQUdDLFVBQUE7RUMxRkE7RUQ0RkQ7SUFDQyxVQUFBO0VDMUZBO0FBQ0Y7O0FENkZBO0VBQ0UsaUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUMzRkY7O0FENkZFO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQzNGTjs7QUQrRkE7RUFDQyxrQkFBQTtBQzVGRDs7QUQrRkE7RUFDQyxrQkFBQTtFQUNHLFNBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsdUNBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUNBQUE7VUFBQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQzVGSjs7QUQrRkE7RUFDQyxZQUFBO0VBQ0csa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0FDNUZKOztBRCtGQTtFQUNDLFlBQUE7RUFDQSxpQkFBQTtBQzVGRDs7QUQrRkE7RUFDQyxlQUFBO0VBQ0csVUFBQTtBQzVGSjs7QUQrRkE7RUFDQyxlQUFBO0VBQ0csWUFBQTtBQzVGSiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2RlbGl2ZXJ5LXByb2Nlc3MvZGVsaXZlcnktcHJvY2Vzcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRibHVlLXN0YXR1czogIzBlYThkYTtcclxuJHJlZC10ZXh0OiAjZTc0YzNjO1xyXG4kY29sb3ItZ3JleTogIzc3Nzc3NztcclxuXHJcbjpob3N0IHtcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0aGVpZ2h0OiAxMDAlO1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGxlZnQ6IDBweDtcclxuXHR0b3A6IDUwcHg7XHJcbn1cclxuKjpmb2N1cyB7XHJcblx0Ym94LXNoYWRvdzogdW5zZXQ7XHJcbn1cclxuZGl2LFxyXG5zcGFuOm5vdCguZmEpIHtcclxuXHRmb250LWZhbWlseTogXCJQb3BwaW5zXCI7XHJcbn1cclxuXHJcbi5ib2FyZCB7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogI2ZjZmNmYztcclxuXHRoZWlnaHQ6IDEwMCU7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0bWluLXdpZHRoOiA3MjBweDtcclxufVxyXG5cclxuLm5hdi50b29sYmFyIHtcclxuXHRiYWNrZ3JvdW5kOiAjZmNmY2ZjO1xyXG5cdC8vIGNvbG9yOiB3aGl0ZTtcclxuXHRoZWlnaHQ6IDE1MHB4O1xyXG5cdHBhZGRpbmctdG9wOiA0MHB4O1xyXG5cdHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG5cdG1hcmdpbi1sZWZ0OiAyMDBweDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuXHQucGFyZW50LWNvbnRhaW5lciB7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHR9XHJcblxyXG5cdC5zZWxlY3QtYWxsIHtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0YWxpZ24taXRlbXM6IHJpZ2h0O1xyXG5cdH1cclxuXHQuY2FyZCB7XHJcblx0XHRwYWRkaW5nOiAyMHB4O1xyXG5cdFx0d2lkdGg6IDk1JTtcclxuXHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0XHRtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG5cdFx0aDMge1xyXG5cdFx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0XHR9XHJcblx0XHR0cixcclxuXHRcdHRkIHtcclxuXHRcdFx0Y29sb3I6IGJsYWNrO1xyXG5cdFx0XHR2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG5cdFx0XHRzZWxlY3Qge1xyXG5cdFx0XHRcdGhlaWdodDogMzBweDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0LmNhcmQtY29udGVudCB7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRociB7XHJcblx0XHRcdFx0bWFyZ2luLXRvcDogMHJlbTtcclxuXHRcdFx0XHRtYXJnaW4tYm90dG9tOiAwcmVtO1xyXG5cdFx0XHRcdGJvcmRlcjogMDtcclxuXHRcdFx0XHRib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHRhYmxlIHtcclxuXHRcdFx0XHRib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG5cdFx0XHRcdC8vIGJvcmRlcjogMnB4IHNvbGlkICNlYWVhZWE7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogNXB4O1xyXG5cdFx0XHRcdHdpZHRoOiA5NSU7XHJcblx0XHRcdH1cclxuXHRcdFx0YnV0dG9uIHtcclxuXHRcdFx0XHRjb2xvcjogcmdiYSg1MiwgNzMsIDk0LCAxKTtcclxuXHRcdFx0XHRib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6IHdoaXRlO1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMTVweDtcclxuXHRcdFx0XHRtYXJnaW4tdG9wOiAxMHB4O1xyXG5cdFx0XHRcdGJvcmRlcjogMnB4IHNvbGlkICNlYWVhZWE7XHJcblx0XHRcdFx0d2lkdGg6IDk1JTtcclxuXHRcdFx0XHRwYWRkaW5nOiAxNnB4O1xyXG5cdFx0XHR9XHJcblx0XHRcdGJ1dHRvbi50cnVlIHtcclxuXHRcdFx0XHRiYWNrZ3JvdW5kOiAjMmMzZTUwO1xyXG5cdFx0XHRcdGNvbG9yOiB3aGl0ZTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcclxuXHQubmF2LnRvb2xiYXIge1xyXG5cdFx0XHRtYXJnaW4tbGVmdDogMHB4O1xyXG5cdH1cclxufVxyXG5cclxuLmZvcm0tY29udHJvbCB7XHJcblx0Zm9udC1zaXplOiAxMXB4O1xyXG5cdHdpZHRoOiAxNTBweDtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0bWFyZ2luOiAycHggMHB4IDJweCAwcHg7XHJcbn1cclxuXHJcbi5kaXYtdHJhbnNhY3Rpb24ge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0ZmxleC13cmFwOiB3cmFwO1xyXG5cdGZvbnQtc2l6ZTogMTFweDtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRsYWJlbCB7XHJcblx0XHRtYXJnaW4tYm90dG9tOiAwcHg7XHJcblx0fVxyXG5cdGRpdi5zZWFyY2hiYXIge1xyXG5cdFx0ZmxleC13cmFwOiB3cmFwO1xyXG5cdFx0ZGlzcGxheTogaW5oZXJpdDtcclxuXHRcdHdpZHRoOiBjYWxjKDEwMCUgLSAxNDBweCk7XHJcblx0XHRwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcblx0XHRqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG5cdFx0ZGl2IHtcclxuXHRcdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRcdFx0bGFiZWwge1xyXG5cdFx0XHRcdG1hcmdpbi1yaWdodDogNXB4O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG4uZGl2LWZvb3RlciB7XHJcblx0aGVpZ2h0OiBmaXQtY29udGVudDtcclxuXHR3aWR0aDogMTAwJTtcclxuXHR0ZXh0LWFsaWduOiAtd2Via2l0LXJpZ2h0O1xyXG5cdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRmbG9hdDpyaWdodDtcclxuXHRkaXYge1xyXG5cdFx0d2lkdGg6IGZpdC1jb250ZW50O1xyXG5cdFx0aGVpZ2h0OiBhdXRvO1xyXG5cdFx0cGFkZGluZzogMHB4O1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogNXB4O1xyXG5cdFx0Ym9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogMTBweDtcclxuXHRcdGJ1dHRvbiB7XHJcblx0XHRcdGNvbG9yOiByZ2JhKDUyLCA3MywgOTQsIDEpO1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czogMTBweDtcclxuXHRcdH1cclxuXHRcdGJ1dHRvbjpob3ZlciB7XHJcblx0XHRcdGJhY2tncm91bmQ6ICMyYzNlNTA7XHJcblx0XHRcdGNvbG9yOiB3aGl0ZTtcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czogMTBweDtcclxuXHRcdH1cclxuXHJcblx0XHRidXR0b246YWN0aXZle1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiAjMmMzZTUwO1xyXG5cdFx0XHRjb2xvcjogd2hpdGU7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcblx0XHR9XHJcblxyXG5cdFx0YnV0dG9uOmZvY3Vze1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiAjMmMzZTUwO1xyXG5cdFx0XHRjb2xvcjogd2hpdGU7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG4uZGl2LWNvbnRlbnQtcm93IHtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcblx0Ym9yZGVyOiAxcHggc29saWQgI2NjYztcclxuXHRhbGlnbi1pdGVtczogdW5zZXQ7XHJcblx0anVzdGlmeS1jb250ZW50OiB1bnNldDtcclxuXHRib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5cdG1hcmdpbi1ib3R0b206IDE1cHg7XHJcblx0cGFkZGluZzogMTBweDtcclxuXHJcblx0LmRpdi1jb250ZW50LWNlbGwge1xyXG5cdFx0Ly8gYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG5cdFx0Ly8gZmxleDogMTtcclxuXHRcdHBhZGRpbmc6IDVweCAxMnB4O1xyXG5cdFx0bWF4LXdpZHRoOiB1bnNldDtcclxuXHRcdGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNjY2M7XHJcblx0XHR3aWR0aDogY2FsYygxMDAlIC0gNTYwcHgpO1xyXG5cdH1cclxuXHJcblx0LmRpdi1jb250ZW50LWNlbGw6bGFzdC1jaGlsZCB7XHJcblx0XHRib3JkZXItcmlnaHQ6IDBweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuXHRcdHdpZHRoOiAyODBweDtcclxuXHR9XHJcblx0PiAuZGl2LWNvbnRlbnQtY2VsbDpudGgtY2hpbGQoMikge1xyXG5cdFx0d2lkdGg6IDI4MHB4O1xyXG5cdH1cclxufVxyXG5cclxuLmRlZmF1bHQtcHJvZHVjdCB7XHJcblx0LnByb2R1Y3QtZGV0YWlsIHtcclxuXHRcdC5kYXRlLXRleHQge1xyXG5cdFx0XHRmb250LXNpemU6IDExcHg7XHJcblx0XHRcdG1hcmdpbi1ib3R0b206IDE1cHg7XHJcblx0XHRcdG1hcmdpbi10b3A6IDEwcHg7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG4ubW9yZS1wcm9kdWN0IHtcclxuXHRvdmVyZmxvdy15OiBhdXRvO1xyXG5cdG92ZXJmbG93LXg6IGF1dG87XHJcblx0ZGlzcGxheTogZ3JpZDtcclxuXHRoZWlnaHQ6IDIwMHB4O1xyXG5cdHBhZGRpbmc6IDBweDtcclxuXHQubG9vcC1wcm9kdWN0IHtcclxuXHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG5cdFx0cGFkZGluZzogMHB4O1xyXG5cdFx0LmltZy13cmFwcGVyIHtcclxuXHRcdFx0cGFkZGluZzogMHB4IDIwcHggMHB4IDBweDtcclxuXHJcblx0XHRcdC8vIGhlaWdodDogLXdlYmtpdC1maWxsLWF2YWlsYWJsZTtcclxuXHRcdFx0Ly8gYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG5cdFx0XHQvLyBvYmplY3QtZml0OiBmaWxsO1xyXG5cdFx0XHQvLyBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5cdFx0XHQuaW1hZ2Uge1xyXG5cdFx0XHRcdGJvcmRlci1yYWRpdXM6IDUlO1xyXG5cdFx0XHRcdHdpZHRoOiAxMjBweDtcclxuXHRcdFx0XHRoZWlnaHQ6IGF1dG87XHJcblx0XHRcdFx0b2JqZWN0LWZpdDogY292ZXI7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdC8vIC5jYXJkIHtcclxuXHRcdFx0Ly8gbWFyZ2luLWxlZnQ6IDBweDtcclxuXHRcdFx0Ly8gbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG5cdFx0XHQvLyBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5cdFx0XHQvLyBoZWlnaHQ6IDE2NXB4O1xyXG5cdFx0XHQvLyB3aWR0aDogMjAwcHg7XHJcblx0XHRcdC8vIHBhZGRpbmc6IDBweDtcclxuXHRcdFx0Ly8gLmltZyB7XHJcblx0XHRcdC8vIFx0aGVpZ2h0OiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xyXG5cdFx0XHQvLyBcdGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuXHRcdFx0Ly8gXHRvYmplY3QtZml0OiBmaWxsO1xyXG5cdFx0XHQvLyBcdGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcblx0XHRcdC8vIH1cclxuXHRcdFx0Ly8gLmltZy13cmFwcGVyIHtcclxuXHRcdFx0Ly8gXHR3aWR0aDoxMDAlO1xyXG5cdFx0XHQvLyBcdGhlaWdodDoxMDAlO1xyXG5cdFx0XHQvLyBcdGJhY2tncm91bmQtcmVwZWF0Om5vLXJlcGVhdDtcclxuXHRcdFx0Ly8gXHRiYWNrZ3JvdW5kLXNpemU6Y29udGFpbjtcclxuXHRcdFx0Ly8gfVxyXG5cdFx0Ly8gfVxyXG5cdH1cclxuXHRcclxufVxyXG5cclxuLmRpdi1kZWZhdWx0LXByb2R1Y3Qge1xyXG5cdGRpc3BsYXk6IGdyaWQ7XHJcblx0cGFkZGluZzogMHB4O1xyXG5cdC5kZWZhdWx0LXByb2R1Y3Qge1xyXG5cdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcblx0XHRwYWRkaW5nOiAwcHg7XHJcblx0XHQuaW1nLXdyYXBwZXIge1xyXG5cdFx0XHRwYWRkaW5nOiAwcHggMjBweCAwcHggMHB4O1xyXG5cclxuXHRcdFx0Ly8gaGVpZ2h0OiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xyXG5cdFx0XHQvLyBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcblx0XHRcdC8vIG9iamVjdC1maXQ6IGZpbGw7XHJcblx0XHRcdC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcblx0XHRcdC5pbWFnZSB7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogNSU7XHJcblx0XHRcdFx0d2lkdGg6IDEyMHB4O1xyXG5cdFx0XHRcdGhlaWdodDogYXV0bztcclxuXHRcdFx0XHRvYmplY3QtZml0OiBjb3ZlcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLnByb2R1Y3RzLWxlZnQge1xyXG5cdHdpZHRoOiAyMCU7XHJcblx0LnByb2R1Y3RzLWxlZnQtYm9hcmQge1xyXG5cdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdHBhZGRpbmc6IDBweDtcclxuXHRcdG92ZXJmbG93OiBoaWRkZW47XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdC5wcm9kdWN0cy1sZWZ0LWRhdGEge1xyXG5cdFx0XHRwYWRkaW5nOiAwcHg7XHJcblx0XHRcdHdpZHRoOiBjYWxjKDEwMCUgLSAyMHB4KTtcclxuXHRcdFx0ZGlzcGxheTogZ3JpZDtcclxuXHRcdH1cclxuXHRcdC5wcm9kdWN0cy1sZWZ0LWNoZWNrYm94IHtcclxuXHRcdFx0d2lkdGg6IDIwcHg7XHJcblx0XHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdFx0XHRwYWRkaW5nOiAwcHg7XHJcblx0XHR9XHJcblx0fVxyXG5cdC5wcm9kdWN0cy1sZWZ0LXN0YXR1cyB7XHJcblx0XHRkaXNwbGF5OiBmbGV4O1xyXG5cdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdFx0cGFkZGluZy1yaWdodDogMHB4O1xyXG5cdFx0cGFkZGluZy1sZWZ0OiAwcHg7XHJcblx0XHRmbGV4LWRpcmVjdGlvbjogcm93O1xyXG5cdFx0Zm9udC1zaXplOiAxM3B4O1xyXG5cdFx0Y29sb3I6ICMwZWE4ZGE7XHJcblx0XHRwYWRkaW5nLXRvcDogMTBweDtcclxuXHRcdC5zZWUtbW9yZSB7XHJcblx0XHRcdGZsZXg6IDE7XHJcblx0XHRcdGNvbG9yOiAjMGVhOGRhO1xyXG5cdFx0XHRjdXJzb3I6IHBvaW50ZXI7XHJcblx0XHRcdHRleHQtYWxpZ246IHJpZ2h0O1xyXG5cdFx0fVxyXG5cdFx0LnNlZS1tb3JlOmhvdmVyIHtcclxuXHRcdFx0Y29sb3I6IHJnYigyLCAxMTcsIDIxNik7XHJcblx0XHR9XHJcblx0XHQuY29sLW1kLTMge1xyXG5cdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdGp1c3RpZnktaXRlbXM6IGxlZnQ7XHJcblx0XHRcdHBhZGRpbmctbGVmdDogMHB4O1xyXG5cdFx0XHRwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG5cdFx0fVxyXG5cdFx0LmNvbC1tZC05IHtcclxuXHRcdFx0dGV4dC1hbGlnbjogcmlnaHQ7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRtYXgtd2lkdGg6IHVuc2V0O1xyXG5cdFx0XHRkaXNwbGF5OiBibG9jaztcclxuXHRcdFx0cGFkZGluZy1yaWdodDogMHB4O1xyXG5cdFx0XHRwYWRkaW5nLXRvcDogMHB4O1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLy8gQG1lZGlhIChtaW4td2lkdGg6IDc2OXB4KSBhbmQgKG1heC13aWR0aDogMTA3NHB4KSB7XHJcbi8vICAgICAuZ3JpZC12aWV3e1xyXG4vLyAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuLy8gICAgICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgICAgICBoZWlnaHQ6MTAwJTtcclxuLy8gICAgICAgICAuZ3JpZC12aWV3LWl0ZW17XHJcbi8vICAgICAgICAgICAgIHdpZHRoOiBjYWxjKDI1dncgLSA1NnB4KTtcclxuLy8gICAgICAgICAgICAgaGVpZ2h0OiBjYWxjKDI1dncgIC0gNTZweCArIDUwcHgpO1xyXG4vLyAgICAgICAgIH1cclxuLy8gICAgIH1cclxuLy8gfVxyXG5cclxuLy8gQG1lZGlhIChtaW4td2lkdGg6IDQ2MHB4KSBhbmQgKG1heC13aWR0aDogNzY5cHgpIHtcclxuLy8gICAgIC5ncmlkLXZpZXd7XHJcbi8vICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4vLyAgICAgICAgIGhlaWdodDoxMDAlO1xyXG4vLyAgICAgICAgIC5ncmlkLXZpZXctaXRlbXtcclxuLy8gICAgICAgICAgICAgd2lkdGg6IGNhbGMoMzMuMzMzdncgLSA3M3B4KTtcclxuLy8gICAgICAgICAgICAgaGVpZ2h0OiBjYWxjKDMzLjMzM3Z3ICAtIDczcHggKyA1MHB4KTtcclxuLy8gICAgICAgICB9XHJcbi8vICAgICB9XHJcbi8vIH1cclxuXHJcbi8vIEBtZWRpYSAobWluLXdpZHRoOiAzMjBweCkgYW5kIChtYXgtd2lkdGg6IDQ2MHB4KSB7XHJcbi8vICAgICAuZ3JpZC12aWV3e1xyXG4vLyAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuLy8gICAgICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgICAgICBoZWlnaHQ6MTAwJTtcclxuLy8gICAgICAgICAuZ3JpZC12aWV3LWl0ZW17XHJcbi8vICAgICAgICAgICAgIHdpZHRoOiBjYWxjKDUwdncgLSAxMDdweCk7XHJcbi8vICAgICAgICAgICAgIGhlaWdodDogY2FsYyg1MHZ3ICAtIDEwN3B4ICsgNTBweCk7XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgfVxyXG4vLyB9XHJcblxyXG4jaDMge1xyXG5cdHdpZHRoOiA5NSU7XHJcblx0bWFyZ2luLWJvdHRvbTogMjRweDtcclxuXHRjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbiNza3Uge1xyXG5cdGZvbnQtc2l6ZTogMTRweDtcclxuXHRjb2xvcjogI2JlYmViZTtcclxuXHRtYXJnaW4tYm90dG9tOiAwcHg7XHJcbn1cclxuXHJcbi5wcmljZSB7XHJcblx0Y29sb3I6ICMwZWE4ZGE7XHJcbn1cclxuXHJcbi5wcmljZS10b3RhbCB7XHJcblx0Y29sb3I6ICMwZWE4ZGE7XHJcbn1cclxuXHJcbi5ub3JtYWwtdGV4dCB7XHJcblx0Zm9udC1zaXplOiAxNXB4O1xyXG5cdGNvbG9yOiAkY29sb3ItZ3JleTtcclxufVxyXG5cclxuLm5vcm1hbC10ZXh0LmRhdGUtdGV4dCB7XHJcblx0Zm9udC1zaXplOiAxMnB4O1xyXG59XHJcblxyXG4uaGVhZGVyLXRleHQge1xyXG5cdGZvbnQtc2l6ZTogMTJweDtcclxuXHRmb250LXdlaWdodDogYm9sZDtcclxuXHRtYXJnaW4tYm90dG9tOiAxcHg7XHJcbn1cclxuLmhlYWRlci10ZXh0Lm9yZGVyLWlkLXRleHQge1xyXG5cdGZvbnQtc2l6ZToxMnB4O1xyXG5cdGNvbG9yOiAkYmx1ZS1zdGF0dXM7XHJcblx0c3BhbiB7XHJcblx0XHRmb250LXNpemU6MTJweDtcclxuXHRcdGNvbG9yOiAkY29sb3ItZ3JleTtcclxuXHR9XHJcbn1cclxuXHJcbi5mb3JtLWxhYmVsIHtcclxuXHQ+IGxhYmVsIHtcclxuXHRcdGRpc3BsYXk6IGJsb2NrO1xyXG5cdH1cclxuXHQucHJvZHVjdC1uYW1le1xyXG5cdFx0Zm9udC1zaXplOjEycHg7XHJcblx0fVxyXG59XHJcblxyXG4uc2VlLW1vcmUtbm9ybWFsLXRleHQge1xyXG5cdGZvbnQtc2l6ZTogMTNweDtcclxuXHRjb2xvcjogJGNvbG9yLWdyZXk7XHJcbn1cclxuXHJcbi5zZWUtbW9yZS1ub3JtYWwtdGV4dC5kYXRlLXRleHQge1xyXG5cdGZvbnQtc2l6ZTogOHB4O1xyXG59XHJcblxyXG4uc2VlLW1vcmUtaGVhZGVyLXRleHQge1xyXG5cdGZvbnQtc2l6ZTogMTBweDtcclxuXHRmb250LXdlaWdodDogYm9sZDtcclxuXHRtYXJnaW4tYm90dG9tOiAxcHg7XHJcbn1cclxuLnNlZS1tb3JlLWhlYWRlci10ZXh0Lm9yZGVyLWlkLXRleHQge1xyXG5cdGZvbnQtc2l6ZToxMnB4O1xyXG5cdGNvbG9yOiAkYmx1ZS1zdGF0dXM7XHJcblx0c3BhbiB7XHJcblx0XHRmb250LXNpemU6MTBweDtcclxuXHRcdGNvbG9yOiAkY29sb3ItZ3JleTtcclxuXHR9XHJcbn1cclxuXHJcbi5zZWUtbW9yZS1mb3JtLWxhYmVsIHtcclxuXHQ+IGxhYmVsIHtcclxuXHRcdGRpc3BsYXk6IGJsb2NrO1xyXG5cdH1cclxuXHQucHJvZHVjdC1uYW1le1xyXG5cdFx0Zm9udC1zaXplOjEwcHg7XHJcblx0fVxyXG59XHJcblxyXG4ucHJpY2Utc2VlLW1vcmUge1xyXG5cdGNvbG9yOiAjMGVhOGRhO1xyXG5cdGZvbnQtc2l6ZToxMHB4O1xyXG59XHJcblxyXG4jdHJhbnNhY3Rpb24ge1xyXG5cdHdpZHRoOiAxMzBweDtcclxufVxyXG5cclxuI2Rvd25sb2FkIHtcclxuXHR2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcblxyXG4jbGFiZWwtaGVhZGVyIHtcclxuXHRmb250LXNpemU6IDIwcHg7XHJcblx0Y29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4jYnV0dG9uLXN3YXBwZXIge1xyXG5cdG1hcmdpbi1sZWZ0OiA1cHg7XHJcblx0bWFyZ2luLXJpZ2h0OiA1cHg7XHJcblx0d2lkdGg6IGNhbGMoKDEwMCUgLyA2KSAtIDEwcHgpO1xyXG5cdHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcblx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHR0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxufVxyXG5cclxuI2NhcmQtdHJhbnNhY3Rpb24ge1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGFsaWduLWl0ZW1zOiBiYXNlbGluZTtcclxufVxyXG5cclxuI3NlYXJjaCB7XHJcblx0bWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG59XHJcblxyXG4jc2VhcmNoLWZpZWxkIHtcclxuXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbiNzZWFyY2gtdGV4dCB7XHJcblx0cGFkZGluZy1yaWdodDogMjNweDtcclxufVxyXG5cclxuI3NlYXJjaC1pY29uIHtcclxuXHRtYXJnaW4tbGVmdDogLTIwcHg7XHJcblx0bWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG59XHJcblxyXG4jYnRuLXByaW50IHtcclxuXHRmb250LXNpemU6IDExcHg7XHJcblx0d2lkdGg6IDEzMHB4O1xyXG5cdG1hcmdpbjogMTVweCAyMHB4IDJweCAwcHg7XHJcblx0aGVpZ2h0OiAzMHB4O1xyXG5cdGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbn1cclxuXHJcbiNidG4tZG93bmxvYWQge1xyXG5cdGZvbnQtc2l6ZTogMTFweDtcclxuXHR3aWR0aDogMTMwcHg7XHJcblx0bWFyZ2luOiAxNXB4IDIwcHggMnB4IDBweDtcclxuXHRoZWlnaHQ6IDMwcHg7XHJcblx0Ym9yZGVyLXJhZGl1czogMTVweDtcclxufVxyXG5cclxuI2NhbGVuZGFyLWljb24ge1xyXG5cdG1hcmdpbi1sZWZ0OiAtMjBweDtcclxuXHRtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbi5wYXltZW50LXN0YXR1cyB7XHJcblx0cGFkZGluZzogMnB4IDVweDtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdG1hcmdpbi1ib3R0b206IDVweDtcclxuXHR3aWR0aDogOTdweDtcclxuXHRoZWlnaHQ6IDM0cHg7XHJcblx0Zm9udC1zaXplOiAxNHB4O1xyXG5cdGJvcmRlci1yYWRpdXM6IDM1cHg7XHJcbn1cclxuLnBheW1lbnQtc3RhdHVzLlBBSUQge1xyXG5cdGNvbG9yOiB3aGl0ZTtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiAkYmx1ZS1zdGF0dXM7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ucGF5bWVudC1zdGF0dXMuQ0FOQ0VMIHtcclxuXHRjb2xvcjogd2hpdGU7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogJHJlZC10ZXh0O1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLmRhdGUtcGlja2VyLXdyYXBwZXIge1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcblxyXG5cdC5kYXRlLXBpY2tlci1vdmVybGF5IHtcclxuXHRcdHotaW5kZXg6IDk5O1xyXG5cdFx0LmRwaWNrZXIge1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuXHRcdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHRcdFx0Ym9yZGVyOiAxcHggc29saWQgI2RmZGZkZjtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHR0b3A6IDMwcHg7XHJcblx0XHRcdGRpc3BsYXk6IGdyaWQ7XHJcblxyXG5cdFx0XHQuYnRuLm9rIHtcclxuXHRcdFx0XHRjbGVhcjogYm90aDtcclxuXHRcdFx0XHRiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG5cdFx0XHRcdGNvbG9yOiB3aGl0ZTtcclxuXHRcdFx0XHRtYXJnaW46IDBweCA1cHggMTBweDtcclxuXHRcdFx0XHRib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xyXG5cdFx0XHR9XHJcblx0XHRcdC5idG4uY2xlYXIge1xyXG5cdFx0XHRcdGNsZWFyOiBib3RoO1xyXG5cdFx0XHRcdGJhY2tncm91bmQtY29sb3I6IHJnYigxNzIsIDEwLCAxMCk7XHJcblx0XHRcdFx0Y29sb3I6IHdoaXRlO1xyXG5cdFx0XHRcdG1hcmdpbjogMHB4IDEwcHggMTBweDtcclxuXHRcdFx0XHRib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xyXG5cdFx0XHR9XHJcblx0XHRcdG5nYi1kYXRlcGlja2VyIHtcclxuXHRcdFx0XHRmbG9hdDogbGVmdDtcclxuXHRcdFx0XHRjbGVhcjogYm90aDtcclxuXHRcdFx0XHRtYXJnaW46IDEwcHg7XHJcblx0XHRcdFx0cmlnaHQ6IDAlO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQuZGF0ZS1waWNrZXItb3ZlcmxheS5mYWxzZSB7XHJcblx0XHRkaXNwbGF5OiBub25lO1xyXG5cdH1cclxufVxyXG5cclxuLmN1c3RvbS1kYXkge1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRwYWRkaW5nOiAwLjE4NXJlbSAwLjI1cmVtO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDJyZW07XHJcblx0d2lkdGg6IDJyZW07XHJcbn1cclxuXHJcbi5jdXN0b20tZGF5LmZvY3VzZWQge1xyXG5cdGJhY2tncm91bmQtY29sb3I6ICNlNmU2ZTY7XHJcbn1cclxuLmN1c3RvbS1kYXkucmFuZ2UsXHJcbi5jdXN0b20tZGF5OmhvdmVyIHtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMiwgMTE3LCAyMTYpO1xyXG5cdGNvbG9yOiB3aGl0ZTtcclxufVxyXG4uY3VzdG9tLWRheS5mYWRlZCB7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogcmdiYSgyLCAxMTcsIDIxNiwgMC41KTtcclxufVxyXG5cclxuLm9yZGVyLXN0YXR1cyB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRmbGV4LXdyYXA6IHdyYXA7XHJcblx0cGxhY2UtY29udGVudDogY2VudGVyO1xyXG5cdC5zdGF0dXMtbmFtZSB7XHJcblx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHQvLyBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuXHRcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdFx0ZmxleC1kaXJlY3Rpb246IHJvdztcclxuXHRcdC5iYWRnZSB7XHJcblx0XHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdFx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblx0XHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0XHRcdGZvbnQtc2l6ZTogMTBweDtcclxuXHRcdFx0Y29sb3I6IHdoaXRlO1xyXG5cdFx0XHRib3JkZXItcmFkaXVzOiA1MHB4O1xyXG5cdFx0XHRsZWZ0OiAtMTVweDtcclxuXHRcdFx0d2lkdGg6IDIzcHg7XHJcblx0XHRcdGhlaWdodDogMjNweDtcclxuXHRcdFx0YmFja2dyb3VuZC1jb2xvcjogcmdiYSgyMzEsIDc2LCA2MCwgMC44KTtcclxuXHRcdFx0Ym9yZGVyOiAxcHggc29saWQgI2U3NGMzYztcclxuXHRcdFx0dG9wOiAtMTRweDtcclxuXHRcdFx0Ly8gdHJhbnNmb3JtOiBzY2FsZSgwLjgpO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLmxvYWRpbmcge1xyXG5cdG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5zay1mYWRpbmctY2lyY2xlIHtcclxuXHRtYXJnaW46IDEwcHggYXV0bztcclxuXHR3aWR0aDogNDBweDtcclxuXHRoZWlnaHQ6IDQwcHg7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlIHtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRoZWlnaHQ6IDEwMCU7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdGxlZnQ6IDA7XHJcblx0dG9wOiAwO1xyXG59XHJcblxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlOmJlZm9yZSB7XHJcblx0Y29udGVudDogXCJcIjtcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHRtYXJnaW46IDAgYXV0bztcclxuXHR3aWR0aDogMTUlO1xyXG5cdGhlaWdodDogMTUlO1xyXG5cdGJhY2tncm91bmQtY29sb3I6ICMzMzM7XHJcblx0Ym9yZGVyLXJhZGl1czogMTAwJTtcclxuXHQtd2Via2l0LWFuaW1hdGlvbjogc2stY2lyY2xlRmFkZURlbGF5IDEuMnMgaW5maW5pdGUgZWFzZS1pbi1vdXQgYm90aDtcclxuXHRhbmltYXRpb246IHNrLWNpcmNsZUZhZGVEZWxheSAxLjJzIGluZmluaXRlIGVhc2UtaW4tb3V0IGJvdGg7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTIge1xyXG5cdC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzBkZWcpO1xyXG5cdC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgzMGRlZyk7XHJcblx0dHJhbnNmb3JtOiByb3RhdGUoMzBkZWcpO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUzIHtcclxuXHQtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDYwZGVnKTtcclxuXHQtbXMtdHJhbnNmb3JtOiByb3RhdGUoNjBkZWcpO1xyXG5cdHRyYW5zZm9ybTogcm90YXRlKDYwZGVnKTtcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNCB7XHJcblx0LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XHJcblx0LW1zLXRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcclxuXHR0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTUge1xyXG5cdC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTIwZGVnKTtcclxuXHQtbXMtdHJhbnNmb3JtOiByb3RhdGUoMTIwZGVnKTtcclxuXHR0cmFuc2Zvcm06IHJvdGF0ZSgxMjBkZWcpO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU2IHtcclxuXHQtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE1MGRlZyk7XHJcblx0LW1zLXRyYW5zZm9ybTogcm90YXRlKDE1MGRlZyk7XHJcblx0dHJhbnNmb3JtOiByb3RhdGUoMTUwZGVnKTtcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNyB7XHJcblx0LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xyXG5cdC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xyXG5cdHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTgge1xyXG5cdC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMjEwZGVnKTtcclxuXHQtbXMtdHJhbnNmb3JtOiByb3RhdGUoMjEwZGVnKTtcclxuXHR0cmFuc2Zvcm06IHJvdGF0ZSgyMTBkZWcpO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU5IHtcclxuXHQtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDI0MGRlZyk7XHJcblx0LW1zLXRyYW5zZm9ybTogcm90YXRlKDI0MGRlZyk7XHJcblx0dHJhbnNmb3JtOiByb3RhdGUoMjQwZGVnKTtcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTAge1xyXG5cdC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMjcwZGVnKTtcclxuXHQtbXMtdHJhbnNmb3JtOiByb3RhdGUoMjcwZGVnKTtcclxuXHR0cmFuc2Zvcm06IHJvdGF0ZSgyNzBkZWcpO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMSB7XHJcblx0LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzMDBkZWcpO1xyXG5cdC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgzMDBkZWcpO1xyXG5cdHRyYW5zZm9ybTogcm90YXRlKDMwMGRlZyk7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTEyIHtcclxuXHQtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDMzMGRlZyk7XHJcblx0LW1zLXRyYW5zZm9ybTogcm90YXRlKDMzMGRlZyk7XHJcblx0dHJhbnNmb3JtOiByb3RhdGUoMzMwZGVnKTtcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMjpiZWZvcmUge1xyXG5cdC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMS4xcztcclxuXHRhbmltYXRpb24tZGVsYXk6IC0xLjFzO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUzOmJlZm9yZSB7XHJcblx0LXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0xcztcclxuXHRhbmltYXRpb24tZGVsYXk6IC0xcztcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNDpiZWZvcmUge1xyXG5cdC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC45cztcclxuXHRhbmltYXRpb24tZGVsYXk6IC0wLjlzO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU1OmJlZm9yZSB7XHJcblx0LXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjhzO1xyXG5cdGFuaW1hdGlvbi1kZWxheTogLTAuOHM7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTY6YmVmb3JlIHtcclxuXHQtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuN3M7XHJcblx0YW5pbWF0aW9uLWRlbGF5OiAtMC43cztcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNzpiZWZvcmUge1xyXG5cdC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC42cztcclxuXHRhbmltYXRpb24tZGVsYXk6IC0wLjZzO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU4OmJlZm9yZSB7XHJcblx0LXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjVzO1xyXG5cdGFuaW1hdGlvbi1kZWxheTogLTAuNXM7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTk6YmVmb3JlIHtcclxuXHQtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuNHM7XHJcblx0YW5pbWF0aW9uLWRlbGF5OiAtMC40cztcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTA6YmVmb3JlIHtcclxuXHQtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuM3M7XHJcblx0YW5pbWF0aW9uLWRlbGF5OiAtMC4zcztcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTE6YmVmb3JlIHtcclxuXHQtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuMnM7XHJcblx0YW5pbWF0aW9uLWRlbGF5OiAtMC4ycztcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTI6YmVmb3JlIHtcclxuXHQtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuMXM7XHJcblx0YW5pbWF0aW9uLWRlbGF5OiAtMC4xcztcclxufVxyXG5cclxuXHJcbi5zZWUtbW9yZS1jb250YWluZXIge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcblx0cGFkZGluZy1sZWZ0OiAwcHg7XHJcblx0ZmxleC1kaXJlY3Rpb246IHJvdztcclxuXHRmb250LXNpemU6IDEzcHg7XHJcblx0Y29sb3I6ICMwZWE4ZGE7XHJcblx0cGFkZGluZy10b3A6IDEwcHg7XHJcblx0LnNlZS1tb3JlIHtcclxuXHRcdGZsZXg6IDE7XHJcblx0XHRjb2xvcjogIzBlYThkYTtcclxuXHRcdGN1cnNvcjogcG9pbnRlcjtcclxuXHRcdHRleHQtYWxpZ246IHJpZ2h0O1xyXG5cdH1cclxuXHQuc2VlLW1vcmU6aG92ZXIge1xyXG5cdFx0Y29sb3I6IHJnYigyLCAxMTcsIDIxNik7XHJcblx0fVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjgwcHgpIHtcclxuXHQuZGVmYXVsdC1wcm9kdWN0IHtcclxuXHRcdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0XHQucHJvZHVjdC1kZXRhaWwge1xyXG5cdFx0XHQuaGVhZGVyLXRleHQub3JkZXItaWQtdGV4dCB7XHJcblx0XHRcdFx0bWFyZ2luLXRvcDogMTZweDtcclxuXHRcdFx0fVxyXG5cdFx0XHQuaGVhZGVyLXRleHQge1xyXG5cdFx0XHRcdHNwYW4ge1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdFx0XHRcdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMDgwcHgpIHtcclxuXHQuZGVmYXVsdC1wcm9kdWN0IHtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHRcdC5wcm9kdWN0LWRldGFpbCB7XHJcblx0XHRcdC5oZWFkZXItdGV4dCB7XHJcblx0XHRcdFx0c3BhbiB7XHJcblx0XHRcdFx0XHRkaXNwbGF5OiBibG9jaztcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0LmRhdGUtdGV4dCB7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxMXB4O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQuZGl2LWRlZmF1bHQtcHJvZHVjdCB7XHJcblx0XHQuZGVmYXVsdC1wcm9kdWN0IHtcclxuXHRcdFx0LmltZy13cmFwcGVyIHtcclxuXHRcdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0LmRpdi1jb250ZW50LXJvdyB7XHJcblx0XHQuZGl2LWNvbnRlbnQtY2VsbDpmaXJzdC1jaGlsZCB7XHJcblx0XHRcdC8vIHdpZHRoOlxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuQC13ZWJraXQta2V5ZnJhbWVzIHNrLWNpcmNsZUZhZGVEZWxheSB7XHJcblx0MCUsXHJcblx0MzklLFxyXG5cdDEwMCUge1xyXG5cdFx0b3BhY2l0eTogMDtcclxuXHR9XHJcblx0NDAlIHtcclxuXHRcdG9wYWNpdHk6IDE7XHJcblx0fVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIHNrLWNpcmNsZUZhZGVEZWxheSB7XHJcblx0MCUsXHJcblx0MzklLFxyXG5cdDEwMCUge1xyXG5cdFx0b3BhY2l0eTogMDtcclxuXHR9XHJcblx0NDAlIHtcclxuXHRcdG9wYWNpdHk6IDE7XHJcblx0fVxyXG59XHJcblxyXG4uZXJyb3ItbWVzc2FnZSB7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgaGVpZ2h0OjEwMHZoO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LXNpemU6MjZweDtcclxuICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgLnRleHQtbWVzc2FnZSB7XHJcbiAgICAgIGJhY2tncm91bmQ6ICNFNzRDM0M7XHJcbiAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgIG1hcmdpbjogMCAxMHB4O1xyXG4gIH1cclxufVxyXG5cclxuLmRpdi1wcm9kdWN0IHtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5wcmludGVkLXN0YW1wIHtcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMwcHg7XHJcbiAgICByaWdodDogMzBweDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGNvbG9yOiBncmVlbjtcclxuICAgIHBhZGRpbmc6IDE0cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJveC1zaGFkb3c6IGluc2V0IDBweCAwcHggMHB4IDRweCBncmVlbjtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIGhlaWdodDogNzZweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoLTIwZGVnKTtcclxuICAgIHdpZHRoOiAxNTBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnByaW50ZWQtc3RhbXA6YmVmb3JlIHtcclxuXHRjb250ZW50OiBcIiBcIjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHotaW5kZXg6IC0xO1xyXG4gICAgdG9wOiA3cHg7XHJcbiAgICBsZWZ0OiA3cHg7XHJcbiAgICByaWdodDogN3B4O1xyXG4gICAgYm90dG9tOiA3cHg7XHJcbiAgICBib3JkZXI6IDJweCBzb2xpZCBncmVlbjtcclxufVxyXG5cclxuLnNoaXBwaW5nLWluZm8ge1xyXG5cdGNvbG9yOiBncmVlbjtcclxuXHRmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuLnJlZC1yZXR1cm4ge1xyXG5cdGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGNvbG9yOiByZWQ7XHJcbn1cclxuXHJcbi5ncmVlbi1yZXR1cm4ge1xyXG5cdGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGNvbG9yOiBncmVlbjtcclxufSIsIjpob3N0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBsZWZ0OiAwcHg7XG4gIHRvcDogNTBweDtcbn1cblxuKjpmb2N1cyB7XG4gIGJveC1zaGFkb3c6IHVuc2V0O1xufVxuXG5kaXYsXG5zcGFuOm5vdCguZmEpIHtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xufVxuXG4uYm9hcmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmNmY2ZjO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBtaW4td2lkdGg6IDcyMHB4O1xufVxuXG4ubmF2LnRvb2xiYXIge1xuICBiYWNrZ3JvdW5kOiAjZmNmY2ZjO1xuICBoZWlnaHQ6IDE1MHB4O1xuICBwYWRkaW5nLXRvcDogNDBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gIG1hcmdpbi1sZWZ0OiAyMDBweDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubmF2LnRvb2xiYXIgLnBhcmVudC1jb250YWluZXIge1xuICB3aWR0aDogMTAwJTtcbn1cbi5uYXYudG9vbGJhciAuc2VsZWN0LWFsbCB7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogcmlnaHQ7XG59XG4ubmF2LnRvb2xiYXIgLmNhcmQge1xuICBwYWRkaW5nOiAyMHB4O1xuICB3aWR0aDogOTUlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuLm5hdi50b29sYmFyIC5jYXJkIGgzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLm5hdi50b29sYmFyIC5jYXJkIHRyLFxuLm5hdi50b29sYmFyIC5jYXJkIHRkIHtcbiAgY29sb3I6IGJsYWNrO1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xufVxuLm5hdi50b29sYmFyIC5jYXJkIHRyIHNlbGVjdCxcbi5uYXYudG9vbGJhciAuY2FyZCB0ZCBzZWxlY3Qge1xuICBoZWlnaHQ6IDMwcHg7XG59XG4ubmF2LnRvb2xiYXIgLmNhcmQgLmNhcmQtY29udGVudCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm5hdi50b29sYmFyIC5jYXJkIC5jYXJkLWNvbnRlbnQgaHIge1xuICBtYXJnaW4tdG9wOiAwcmVtO1xuICBtYXJnaW4tYm90dG9tOiAwcmVtO1xuICBib3JkZXI6IDA7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMSk7XG59XG4ubmF2LnRvb2xiYXIgLmNhcmQgLmNhcmQtY29udGVudCB0YWJsZSB7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgd2lkdGg6IDk1JTtcbn1cbi5uYXYudG9vbGJhciAuY2FyZCAuY2FyZC1jb250ZW50IGJ1dHRvbiB7XG4gIGNvbG9yOiAjMzQ0OTVlO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBib3JkZXI6IDJweCBzb2xpZCAjZWFlYWVhO1xuICB3aWR0aDogOTUlO1xuICBwYWRkaW5nOiAxNnB4O1xufVxuLm5hdi50b29sYmFyIC5jYXJkIC5jYXJkLWNvbnRlbnQgYnV0dG9uLnRydWUge1xuICBiYWNrZ3JvdW5kOiAjMmMzZTUwO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XG4gIC5uYXYudG9vbGJhciB7XG4gICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgfVxufVxuLmZvcm0tY29udHJvbCB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgd2lkdGg6IDE1MHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbjogMnB4IDBweCAycHggMHB4O1xufVxuXG4uZGl2LXRyYW5zYWN0aW9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBmb250LXNpemU6IDExcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmRpdi10cmFuc2FjdGlvbiBsYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5kaXYtdHJhbnNhY3Rpb24gZGl2LnNlYXJjaGJhciB7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgZGlzcGxheTogaW5oZXJpdDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDE0MHB4KTtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuLmRpdi10cmFuc2FjdGlvbiBkaXYuc2VhcmNoYmFyIGRpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZGl2LXRyYW5zYWN0aW9uIGRpdi5zZWFyY2hiYXIgZGl2IGxhYmVsIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5kaXYtZm9vdGVyIHtcbiAgaGVpZ2h0OiBmaXQtY29udGVudDtcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IC13ZWJraXQtcmlnaHQ7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsb2F0OiByaWdodDtcbn1cbi5kaXYtZm9vdGVyIGRpdiB7XG4gIHdpZHRoOiBmaXQtY29udGVudDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuLmRpdi1mb290ZXIgZGl2IGJ1dHRvbiB7XG4gIGNvbG9yOiAjMzQ0OTVlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cbi5kaXYtZm9vdGVyIGRpdiBidXR0b246aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMmMzZTUwO1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG4uZGl2LWZvb3RlciBkaXYgYnV0dG9uOmFjdGl2ZSB7XG4gIGJhY2tncm91bmQ6ICMyYzNlNTA7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cbi5kaXYtZm9vdGVyIGRpdiBidXR0b246Zm9jdXMge1xuICBiYWNrZ3JvdW5kOiAjMmMzZTUwO1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi5kaXYtY29udGVudC1yb3cge1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgYWxpZ24taXRlbXM6IHVuc2V0O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2V0O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBwYWRkaW5nOiAxMHB4O1xufVxuLmRpdi1jb250ZW50LXJvdyAuZGl2LWNvbnRlbnQtY2VsbCB7XG4gIHBhZGRpbmc6IDVweCAxMnB4O1xuICBtYXgtd2lkdGg6IHVuc2V0O1xuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjY2NjO1xuICB3aWR0aDogY2FsYygxMDAlIC0gNTYwcHgpO1xufVxuLmRpdi1jb250ZW50LXJvdyAuZGl2LWNvbnRlbnQtY2VsbDpsYXN0LWNoaWxkIHtcbiAgYm9yZGVyLXJpZ2h0OiAwcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIHdpZHRoOiAyODBweDtcbn1cbi5kaXYtY29udGVudC1yb3cgPiAuZGl2LWNvbnRlbnQtY2VsbDpudGgtY2hpbGQoMikge1xuICB3aWR0aDogMjgwcHg7XG59XG5cbi5kZWZhdWx0LXByb2R1Y3QgLnByb2R1Y3QtZGV0YWlsIC5kYXRlLXRleHQge1xuICBmb250LXNpemU6IDExcHg7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbi5tb3JlLXByb2R1Y3Qge1xuICBvdmVyZmxvdy15OiBhdXRvO1xuICBvdmVyZmxvdy14OiBhdXRvO1xuICBkaXNwbGF5OiBncmlkO1xuICBoZWlnaHQ6IDIwMHB4O1xuICBwYWRkaW5nOiAwcHg7XG59XG4ubW9yZS1wcm9kdWN0IC5sb29wLXByb2R1Y3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBwYWRkaW5nOiAwcHg7XG59XG4ubW9yZS1wcm9kdWN0IC5sb29wLXByb2R1Y3QgLmltZy13cmFwcGVyIHtcbiAgcGFkZGluZzogMHB4IDIwcHggMHB4IDBweDtcbn1cbi5tb3JlLXByb2R1Y3QgLmxvb3AtcHJvZHVjdCAuaW1nLXdyYXBwZXIgLmltYWdlIHtcbiAgYm9yZGVyLXJhZGl1czogNSU7XG4gIHdpZHRoOiAxMjBweDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBvYmplY3QtZml0OiBjb3Zlcjtcbn1cblxuLmRpdi1kZWZhdWx0LXByb2R1Y3Qge1xuICBkaXNwbGF5OiBncmlkO1xuICBwYWRkaW5nOiAwcHg7XG59XG4uZGl2LWRlZmF1bHQtcHJvZHVjdCAuZGVmYXVsdC1wcm9kdWN0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgcGFkZGluZzogMHB4O1xufVxuLmRpdi1kZWZhdWx0LXByb2R1Y3QgLmRlZmF1bHQtcHJvZHVjdCAuaW1nLXdyYXBwZXIge1xuICBwYWRkaW5nOiAwcHggMjBweCAwcHggMHB4O1xufVxuLmRpdi1kZWZhdWx0LXByb2R1Y3QgLmRlZmF1bHQtcHJvZHVjdCAuaW1nLXdyYXBwZXIgLmltYWdlIHtcbiAgYm9yZGVyLXJhZGl1czogNSU7XG4gIHdpZHRoOiAxMjBweDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBvYmplY3QtZml0OiBjb3Zlcjtcbn1cblxuLnByb2R1Y3RzLWxlZnQge1xuICB3aWR0aDogMjAlO1xufVxuLnByb2R1Y3RzLWxlZnQgLnByb2R1Y3RzLWxlZnQtYm9hcmQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nOiAwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdpZHRoOiAxMDAlO1xufVxuLnByb2R1Y3RzLWxlZnQgLnByb2R1Y3RzLWxlZnQtYm9hcmQgLnByb2R1Y3RzLWxlZnQtZGF0YSB7XG4gIHBhZGRpbmc6IDBweDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDIwcHgpO1xuICBkaXNwbGF5OiBncmlkO1xufVxuLnByb2R1Y3RzLWxlZnQgLnByb2R1Y3RzLWxlZnQtYm9hcmQgLnByb2R1Y3RzLWxlZnQtY2hlY2tib3gge1xuICB3aWR0aDogMjBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmc6IDBweDtcbn1cbi5wcm9kdWN0cy1sZWZ0IC5wcm9kdWN0cy1sZWZ0LXN0YXR1cyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBmb250LXNpemU6IDEzcHg7XG4gIGNvbG9yOiAjMGVhOGRhO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5wcm9kdWN0cy1sZWZ0IC5wcm9kdWN0cy1sZWZ0LXN0YXR1cyAuc2VlLW1vcmUge1xuICBmbGV4OiAxO1xuICBjb2xvcjogIzBlYThkYTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0ZXh0LWFsaWduOiByaWdodDtcbn1cbi5wcm9kdWN0cy1sZWZ0IC5wcm9kdWN0cy1sZWZ0LXN0YXR1cyAuc2VlLW1vcmU6aG92ZXIge1xuICBjb2xvcjogIzAyNzVkODtcbn1cbi5wcm9kdWN0cy1sZWZ0IC5wcm9kdWN0cy1sZWZ0LXN0YXR1cyAuY29sLW1kLTMge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGp1c3RpZnktaXRlbXM6IGxlZnQ7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuLnByb2R1Y3RzLWxlZnQgLnByb2R1Y3RzLWxlZnQtc3RhdHVzIC5jb2wtbWQtOSB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LXdpZHRoOiB1bnNldDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbiAgcGFkZGluZy10b3A6IDBweDtcbn1cblxuI2gzIHtcbiAgd2lkdGg6IDk1JTtcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4jc2t1IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogI2JlYmViZTtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuXG4ucHJpY2Uge1xuICBjb2xvcjogIzBlYThkYTtcbn1cblxuLnByaWNlLXRvdGFsIHtcbiAgY29sb3I6ICMwZWE4ZGE7XG59XG5cbi5ub3JtYWwtdGV4dCB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgY29sb3I6ICM3Nzc3Nzc7XG59XG5cbi5ub3JtYWwtdGV4dC5kYXRlLXRleHQge1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5oZWFkZXItdGV4dCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG1hcmdpbi1ib3R0b206IDFweDtcbn1cblxuLmhlYWRlci10ZXh0Lm9yZGVyLWlkLXRleHQge1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjMGVhOGRhO1xufVxuLmhlYWRlci10ZXh0Lm9yZGVyLWlkLXRleHQgc3BhbiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM3Nzc3Nzc7XG59XG5cbi5mb3JtLWxhYmVsID4gbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5mb3JtLWxhYmVsIC5wcm9kdWN0LW5hbWUge1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5zZWUtbW9yZS1ub3JtYWwtdGV4dCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgY29sb3I6ICM3Nzc3Nzc7XG59XG5cbi5zZWUtbW9yZS1ub3JtYWwtdGV4dC5kYXRlLXRleHQge1xuICBmb250LXNpemU6IDhweDtcbn1cblxuLnNlZS1tb3JlLWhlYWRlci10ZXh0IHtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luLWJvdHRvbTogMXB4O1xufVxuXG4uc2VlLW1vcmUtaGVhZGVyLXRleHQub3JkZXItaWQtdGV4dCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICMwZWE4ZGE7XG59XG4uc2VlLW1vcmUtaGVhZGVyLXRleHQub3JkZXItaWQtdGV4dCBzcGFuIHtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBjb2xvcjogIzc3Nzc3Nztcbn1cblxuLnNlZS1tb3JlLWZvcm0tbGFiZWwgPiBsYWJlbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLnNlZS1tb3JlLWZvcm0tbGFiZWwgLnByb2R1Y3QtbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuLnByaWNlLXNlZS1tb3JlIHtcbiAgY29sb3I6ICMwZWE4ZGE7XG4gIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuI3RyYW5zYWN0aW9uIHtcbiAgd2lkdGg6IDEzMHB4O1xufVxuXG4jZG93bmxvYWQge1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuXG4jbGFiZWwtaGVhZGVyIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBjb2xvcjogYmxhY2s7XG59XG5cbiNidXR0b24tc3dhcHBlciB7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICB3aWR0aDogY2FsYygoMTAwJSAvIDYpIC0gMTBweCk7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuXG4jY2FyZC10cmFuc2FjdGlvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogYmFzZWxpbmU7XG59XG5cbiNzZWFyY2gge1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG5cbiNzZWFyY2gtZmllbGQge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbiNzZWFyY2gtdGV4dCB7XG4gIHBhZGRpbmctcmlnaHQ6IDIzcHg7XG59XG5cbiNzZWFyY2gtaWNvbiB7XG4gIG1hcmdpbi1sZWZ0OiAtMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xufVxuXG4jYnRuLXByaW50IHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICB3aWR0aDogMTMwcHg7XG4gIG1hcmdpbjogMTVweCAyMHB4IDJweCAwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbn1cblxuI2J0bi1kb3dubG9hZCB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgd2lkdGg6IDEzMHB4O1xuICBtYXJnaW46IDE1cHggMjBweCAycHggMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG59XG5cbiNjYWxlbmRhci1pY29uIHtcbiAgbWFyZ2luLWxlZnQ6IC0yMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG5cbi5wYXltZW50LXN0YXR1cyB7XG4gIHBhZGRpbmc6IDJweCA1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgd2lkdGg6IDk3cHg7XG4gIGhlaWdodDogMzRweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiAzNXB4O1xufVxuXG4ucGF5bWVudC1zdGF0dXMuUEFJRCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzBlYThkYTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ucGF5bWVudC1zdGF0dXMuQ0FOQ0VMIHtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTc0YzNjO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5kYXRlLXBpY2tlci13cmFwcGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uZGF0ZS1waWNrZXItd3JhcHBlciAuZGF0ZS1waWNrZXItb3ZlcmxheSB7XG4gIHotaW5kZXg6IDk5O1xufVxuLmRhdGUtcGlja2VyLXdyYXBwZXIgLmRhdGUtcGlja2VyLW92ZXJsYXkgLmRwaWNrZXIge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2RmZGZkZjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDMwcHg7XG4gIGRpc3BsYXk6IGdyaWQ7XG59XG4uZGF0ZS1waWNrZXItd3JhcHBlciAuZGF0ZS1waWNrZXItb3ZlcmxheSAuZHBpY2tlciAuYnRuLm9rIHtcbiAgY2xlYXI6IGJvdGg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luOiAwcHggNXB4IDEwcHg7XG4gIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG4uZGF0ZS1waWNrZXItd3JhcHBlciAuZGF0ZS1waWNrZXItb3ZlcmxheSAuZHBpY2tlciAuYnRuLmNsZWFyIHtcbiAgY2xlYXI6IGJvdGg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhYzBhMGE7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luOiAwcHggMTBweCAxMHB4O1xuICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xufVxuLmRhdGUtcGlja2VyLXdyYXBwZXIgLmRhdGUtcGlja2VyLW92ZXJsYXkgLmRwaWNrZXIgbmdiLWRhdGVwaWNrZXIge1xuICBmbG9hdDogbGVmdDtcbiAgY2xlYXI6IGJvdGg7XG4gIG1hcmdpbjogMTBweDtcbiAgcmlnaHQ6IDAlO1xufVxuLmRhdGUtcGlja2VyLXdyYXBwZXIgLmRhdGUtcGlja2VyLW92ZXJsYXkuZmFsc2Uge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4uY3VzdG9tLWRheSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZzogMC4xODVyZW0gMC4yNXJlbTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBoZWlnaHQ6IDJyZW07XG4gIHdpZHRoOiAycmVtO1xufVxuXG4uY3VzdG9tLWRheS5mb2N1c2VkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2ZTZlNjtcbn1cblxuLmN1c3RvbS1kYXkucmFuZ2UsXG4uY3VzdG9tLWRheTpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMjc1ZDg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmN1c3RvbS1kYXkuZmFkZWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIsIDExNywgMjE2LCAwLjUpO1xufVxuXG4ub3JkZXItc3RhdHVzIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBwbGFjZS1jb250ZW50OiBjZW50ZXI7XG59XG4ub3JkZXItc3RhdHVzIC5zdGF0dXMtbmFtZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG59XG4ub3JkZXItc3RhdHVzIC5zdGF0dXMtbmFtZSAuYmFkZ2Uge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmb250LXNpemU6IDEwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgbGVmdDogLTE1cHg7XG4gIHdpZHRoOiAyM3B4O1xuICBoZWlnaHQ6IDIzcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjMxLCA3NiwgNjAsIDAuOCk7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlNzRjM2M7XG4gIHRvcDogLTE0cHg7XG59XG5cbi5sb2FkaW5nIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUge1xuICBtYXJnaW46IDEwcHggYXV0bztcbiAgd2lkdGg6IDQwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICB0b3A6IDA7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogMCBhdXRvO1xuICB3aWR0aDogMTUlO1xuICBoZWlnaHQ6IDE1JTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzMzMztcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgLXdlYmtpdC1hbmltYXRpb246IHNrLWNpcmNsZUZhZGVEZWxheSAxLjJzIGluZmluaXRlIGVhc2UtaW4tb3V0IGJvdGg7XG4gIGFuaW1hdGlvbjogc2stY2lyY2xlRmFkZURlbGF5IDEuMnMgaW5maW5pdGUgZWFzZS1pbi1vdXQgYm90aDtcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTIge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDMwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDMwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMzBkZWcpO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMyB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNjBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNjBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg2MGRlZyk7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU0IHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTUge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDEyMGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgxMjBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgxMjBkZWcpO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNiB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTUwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDE1MGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDE1MGRlZyk7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU3IHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTgge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDIxMGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgyMTBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgyMTBkZWcpO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlOSB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMjQwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDI0MGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDI0MGRlZyk7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMCB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMjcwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDI3MGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDI3MGRlZyk7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMSB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzAwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDMwMGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDMwMGRlZyk7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMiB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzMwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDMzMGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDMzMGRlZyk7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUyOmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMS4xcztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMS4xcztcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTM6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0xcztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMXM7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU0OmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC45cztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC45cztcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTU6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjhzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjhzO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNjpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuN3M7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuN3M7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU3OmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC42cztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC42cztcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTg6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjVzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjVzO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlOTpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuNHM7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuNHM7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMDpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuM3M7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuM3M7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMTpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuMnM7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuMnM7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMjpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuMXM7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuMXM7XG59XG5cbi5zZWUtbW9yZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgZm9udC1zaXplOiAxM3B4O1xuICBjb2xvcjogIzBlYThkYTtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uc2VlLW1vcmUtY29udGFpbmVyIC5zZWUtbW9yZSB7XG4gIGZsZXg6IDE7XG4gIGNvbG9yOiAjMGVhOGRhO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuLnNlZS1tb3JlLWNvbnRhaW5lciAuc2VlLW1vcmU6aG92ZXIge1xuICBjb2xvcjogIzAyNzVkODtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTI4MHB4KSB7XG4gIC5kZWZhdWx0LXByb2R1Y3Qge1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIH1cbiAgLmRlZmF1bHQtcHJvZHVjdCAucHJvZHVjdC1kZXRhaWwgLmhlYWRlci10ZXh0Lm9yZGVyLWlkLXRleHQge1xuICAgIG1hcmdpbi10b3A6IDE2cHg7XG4gIH1cbiAgLmRlZmF1bHQtcHJvZHVjdCAucHJvZHVjdC1kZXRhaWwgLmhlYWRlci10ZXh0IHNwYW4ge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTA4MHB4KSB7XG4gIC5kZWZhdWx0LXByb2R1Y3Qge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gIH1cbiAgLmRlZmF1bHQtcHJvZHVjdCAucHJvZHVjdC1kZXRhaWwgLmhlYWRlci10ZXh0IHNwYW4ge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG4gIC5kZWZhdWx0LXByb2R1Y3QgLnByb2R1Y3QtZGV0YWlsIC5kYXRlLXRleHQge1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgfVxuXG4gIC5kaXYtZGVmYXVsdC1wcm9kdWN0IC5kZWZhdWx0LXByb2R1Y3QgLmltZy13cmFwcGVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufVxuQC13ZWJraXQta2V5ZnJhbWVzIHNrLWNpcmNsZUZhZGVEZWxheSB7XG4gIDAlLCAzOSUsIDEwMCUge1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbiAgNDAlIHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG5Aa2V5ZnJhbWVzIHNrLWNpcmNsZUZhZGVEZWxheSB7XG4gIDAlLCAzOSUsIDEwMCUge1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbiAgNDAlIHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG4uZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59XG5cbi5kaXYtcHJvZHVjdCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLnByaW50ZWQtc3RhbXAge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMzBweDtcbiAgcmlnaHQ6IDMwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgY29sb3I6IGdyZWVuO1xuICBwYWRkaW5nOiAxNHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm94LXNoYWRvdzogaW5zZXQgMHB4IDBweCAwcHggNHB4IGdyZWVuO1xuICBmb250LXNpemU6IDMwcHg7XG4gIGhlaWdodDogNzZweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRyYW5zZm9ybTogcm90YXRlKC0yMGRlZyk7XG4gIHdpZHRoOiAxNTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ucHJpbnRlZC1zdGFtcDpiZWZvcmUge1xuICBjb250ZW50OiBcIiBcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAtMTtcbiAgdG9wOiA3cHg7XG4gIGxlZnQ6IDdweDtcbiAgcmlnaHQ6IDdweDtcbiAgYm90dG9tOiA3cHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkIGdyZWVuO1xufVxuXG4uc2hpcHBpbmctaW5mbyB7XG4gIGNvbG9yOiBncmVlbjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5yZWQtcmV0dXJuIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogcmVkO1xufVxuXG4uZ3JlZW4tcmV0dXJuIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogZ3JlZW47XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/delivery-process/delivery-process.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/delivery-process.component.ts ***!
  \*******************************************************************************/
/*! exports provided: DeliveryProcessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryProcessComponent", function() { return DeliveryProcessComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_6___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var DeliveryProcessComponent = /** @class */ (function () {
    function DeliveryProcessComponent(orderhistoryService, router, route, memberService) {
        this.orderhistoryService = orderhistoryService;
        this.router = router;
        this.route = route;
        this.memberService = memberService;
        this.onSearchActive = false;
        this.showLoading = false;
        this.allData = [];
        this.allDataToDownload = [];
        this.incomingToDownload = [];
        this.deliveryToDownload = [];
        this.completeToDownload = [];
        this.form_input = {};
        this.errorLabel = false;
        this.errorMessage = false;
        this.allMemberDetail = false;
        this.swaper = [
            { name: 'All Order', val: true, value: 'all order', values: "all_order" },
            { name: 'Incoming', val: false, value: 'on process', values: 'pending' },
            { name: 'On Packaging', val: false, value: 'on packaging', values: 'on_processing' },
            { name: 'On Delivery', val: false, value: 'on delivery', values: 'on_delivery' },
            { name: 'Delivered', val: false, value: 'delivered', values: 'delivered' },
        ];
        this.filterBy = [
            { name: 'Reference No', value: 'reference_no' },
            { name: 'Order ID', value: 'order_id' },
            { name: 'Receiver', value: 'receiver' },
            // { name: 'Product Name', value: 'product_name' },
            { name: 'AWB Number', value: 'awb_number' }
        ];
        this.sortBy = [
            { name: 'Newest', value: 'newest' },
            { name: 'Highest Transactions', value: 'highest transactions' },
            { name: 'Lowest Transactions', value: 'lowest transactions' }
        ];
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_6__;
        this.orderDetail = false;
        this.shippingLabel = false;
        this.toggler = {};
        this.pageNumbering = [];
        this.currentPage = 1;
        this.pageLimits = 20;
        this.pagesLimiter = [];
        this.orderBy = "newest";
        this.filter = "reference_no";
        this.checkBox = [];
        this.activeCheckbox = false;
        this.getDataCheck = [];
        this.filterResult = {};
        this.orderByResult = { created_date: -1 };
        this.indexSwap = 0;
        this.mci_project = false;
        this.service = this.orderhistoryService;
    }
    DeliveryProcessComponent.prototype.callDetail = function (order_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    DeliveryProcessComponent.prototype.openDetail = function (order_id) {
        // this.router.navigate(['merchant-portal/order-detail/edit'],  {queryParams: {id: order_id }})
    };
    //Shipping Status Button Swapping Function
    DeliveryProcessComponent.prototype.swapClick = function (index) {
        return __awaiter(this, void 0, void 0, function () {
            var status, process_1, process_2, process_3, process_4;
            return __generator(this, function (_a) {
                this.indexSwap = index;
                this.currentPage = 1;
                this.pageLimits = 20;
                if (this.form_input.transaction_status) {
                    delete this.form_input.transaction_status;
                }
                this.swaper.forEach(function (e) {
                    e.val = false;
                });
                this.swaper[index].val = true;
                status = this.swaper[index].values;
                delete this.form_input.status;
                delete this.form_input.last_shipping_info;
                if (status == "all_order") {
                    this.firstLoad();
                    return [2 /*return*/];
                }
                else if (status == "pending") {
                    process_1 = {
                        status: "PENDING",
                    };
                    Object.assign(this.form_input, process_1);
                }
                else if (status == "on_processing") {
                    process_2 = {
                        status: "ACTIVE",
                        last_shipping_info: "on_processing"
                    };
                    Object.assign(this.form_input, process_2);
                }
                else if (status == "on_delivery") {
                    process_3 = {
                        status: "ACTIVE",
                        last_shipping_info: "on_delivery"
                    };
                    Object.assign(this.form_input, process_3);
                }
                else if (status == "delivered") {
                    process_4 = {
                        // status: "COMPLETED",
                        // status: {
                        //   in:["PROCESSED","COMPLETED","RETURN", "CANCEL"]
                        // },
                        // last_shipping_info:"delivered"
                        last_shipping_info: {
                            in: ["return", "redelivery", "delivered"],
                        }
                    };
                    Object.assign(this.form_input, process_4);
                }
                // else {
                //   let swaper = {
                //     shipping_status: status,
                //     payment_status: 'PROCESSED',
                //   }
                //   Object.assign(this.form_input, swaper)
                // }
                this.valuechange({}, false, false);
                return [2 /*return*/];
            });
        });
    };
    DeliveryProcessComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    DeliveryProcessComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program_1, _this_1, filter, filter_pending, resultPending, status_1, process_5, process_6, process_7, process_8, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        program_1 = localStorage.getItem('programName');
                        _this_1 = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program_1) {
                                if (element.type == "reguler") {
                                    _this_1.mci_project = true;
                                }
                                else {
                                    _this_1.mci_project = false;
                                }
                            }
                        });
                        filter = {
                            search: {
                                status: {
                                    in: ["ACTIVE", "PENDING", "DELIVERED", "RETURN", "CANCEL"]
                                }
                            },
                            order_by: this.orderByResult,
                            limit_per_page: this.pageLimits,
                            current_page: this.currentPage,
                            download: false
                        };
                        filter_pending = {
                            search: {
                                status: "PENDING",
                            },
                            order_by: this.orderByResult,
                            limit_per_page: this.pageLimits,
                            current_page: this.currentPage,
                            download: false
                        };
                        // // Download All Order Report
                        // let no_filter = {
                        //   search: {
                        //     status: {
                        //       in:["PROCESSED","COMPLETED"]
                        //     }
                        //   },
                        //   order_by: this.orderByResult,
                        // }
                        // // Download Incoming Order Report
                        // let no_filter_incoming = {
                        //   search: {
                        //     status: "PROCESSED",
                        //     last_shipping_info: "on_processing"
                        //   },
                        //   order_by: this.orderByResult,
                        // }
                        // // Download On Delivery Order Report
                        // let no_filter_delivery = {
                        //   search: {
                        //     status: "PROCESSED",
                        //     last_shipping_info: "on_delivery"
                        //   },
                        //   order_by: this.orderByResult,
                        // }
                        // // Download Complete Order Report
                        // let no_filter_complete = {
                        //   search: {
                        //     status: "COMPLETED",
                        //     last_shipping_info: "delivered"
                        //   },
                        //   order_by: this.orderByResult,
                        // }
                        this.showLoading = true;
                        return [4 /*yield*/, this.orderhistoryService.searchShippingReport(filter_pending)];
                    case 1:
                        resultPending = _a.sent();
                        this.totalValuePending = resultPending.total_all_values;
                        status_1 = this.swaper[this.indexSwap].values;
                        if (status_1 == "all_order") {
                            // let result = await this.orderhistoryService.searchOrderHistory(filter);
                            // let resultPending = await this.orderhistoryService.searchShippingReport(filter_pending);
                            // this.totalValuePending = resultPending.total_all_values;
                            // this.allData = result.values;
                            // this.totalPage = result.total_page;
                            // this.buildPagesNumbers(this.totalPage)
                            delete this.form_input.last_shipping_info;
                            Object.assign(this.form_input, filter.search);
                            this.valuechange({}, false, false);
                        }
                        else if (status_1 == "pending") {
                            process_5 = {
                                status: "PENDING"
                                // last_shipping_info:"on_processing"
                            };
                            Object.assign(this.form_input, process_5);
                            this.valuechange({}, false, false);
                        }
                        else if (status_1 == "on_processing") {
                            process_6 = {
                                // status: "PROCESSED",
                                last_shipping_info: "on_processing"
                            };
                            Object.assign(this.form_input, process_6);
                            this.valuechange({}, false, false);
                        }
                        else if (status_1 == "on_delivery") {
                            process_7 = {
                                // status: "PROCESSED",
                                last_shipping_info: "on_delivery"
                            };
                            Object.assign(this.form_input, process_7);
                            this.valuechange({}, false, false);
                        }
                        else if (status_1 == "delivered") {
                            process_8 = {
                                // status: "COMPLETED",
                                // status: {
                                //   in:["PROCESSED","COMPLETED","RETURN", "CANCEL"]
                                // },
                                // last_shipping_info:"delivered"
                                last_shipping_info: {
                                    in: ["return", "redelivery", "delivered"],
                                }
                            };
                            Object.assign(this.form_input, process_8);
                            this.valuechange({}, false, false);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessComponent.prototype.type = function (array) {
        var res = [];
        this.allData.forEach(function (el, i) {
            el.products.forEach(function (element) {
                res.push(element.type);
            });
            // Object.assign(this.allData[i], this.prodType)
        });
        // console.log("hasil",res[0])
        return res[array];
    };
    //When Page Changed
    DeliveryProcessComponent.prototype.onChangePage = function (pageNumber) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (pageNumber <= 0) {
                    pageNumber = 1;
                }
                if (pageNumber >= this.total_page) {
                    pageNumber = this.total_page;
                }
                this.currentPage = pageNumber;
                console.log(pageNumber, this.currentPage, this.total_page);
                this.valuechange({}, false, false);
                return [2 /*return*/];
            });
        });
    };
    //Order by Function
    DeliveryProcessComponent.prototype.orderBySelected = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                if (this.orderBy == "highest transactions") {
                    this.orderByResult = {
                        sum_total: -1
                    };
                    this.valuechange({}, false, false);
                }
                else if (this.orderBy == "lowest transactions") {
                    this.orderByResult = {
                        sum_total: 1
                    };
                    this.valuechange({}, false, false);
                }
                else if (this.orderBy == "newest") {
                    this.orderByResult = {
                        request_date: -1
                    };
                    this.valuechange({}, false, false);
                }
                return [2 /*return*/];
            });
        });
    };
    //Start of Pagination
    DeliveryProcessComponent.prototype.pagination = function (c, m) {
        var current = c, last = m, delta = 2, left = current - delta, right = current + delta + 1, range = [], rangeWithDots = [], l;
        for (var i = 1; i <= last; i++) {
            if (i == 1 || i == last || i >= left && i < right) {
                range.push(i);
            }
        }
        for (var _i = 0, range_1 = range; _i < range_1.length; _i++) {
            var i = range_1[_i];
            if (l) {
                if (i - l === 2) {
                    rangeWithDots.push(l + 1);
                }
                else if (i - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(i);
            l = i;
        }
        return rangeWithDots;
    };
    DeliveryProcessComponent.prototype.buildPagesNumbers = function (totalPage) {
        this.pageNumbering = [];
        this.total_page = totalPage;
        this.pageNumbering = this.pagination(this.currentPage, totalPage);
    };
    //End of Pagination
    //Search by text function
    DeliveryProcessComponent.prototype.searchText = function ($event) {
        return __awaiter(this, void 0, void 0, function () {
            var result, params, order_id, reference_no, product_name, receiver, awb_number;
            return __generator(this, function (_a) {
                result = $event.target.value;
                console.log("Search", result);
                if (result.length > 2) {
                    this.currentPage = 1;
                    params = void 0;
                    if (this.filter == "order_id") {
                        order_id = {
                            order_id: result,
                            get_detail: true
                        };
                        Object.assign(this.form_input, order_id);
                    }
                    else if (this.filter == "reference_no") {
                        reference_no = {
                            reference_no: result,
                            get_detail: true
                        };
                        Object.assign(this.form_input, reference_no);
                    }
                    else if (this.filter == "product_name") {
                        product_name = {
                            "product_list.product_name": result,
                            get_detail: true
                        };
                        Object.assign(this.form_input, product_name);
                    }
                    else if (this.filter == "receiver") {
                        receiver = {
                            "destination.name": result,
                            get_detail: true
                        };
                        Object.assign(this.form_input, receiver);
                    }
                    else if (this.filter == "awb_number") {
                        awb_number = {
                            awb_number: result,
                            get_detail: true
                        };
                        Object.assign(this.form_input, awb_number);
                    }
                    this.valuechange({}, false, false);
                }
                else if (!result) {
                    this.form_input = {};
                    this.valuechange({}, false, false);
                }
                return [2 /*return*/];
            });
        });
    };
    //Get the detail Function
    DeliveryProcessComponent.prototype.getDetail = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                // this.orderDetail = await this.orderhistoryService.getOrderDetail(idDetail);
                this.propsDetail = data;
                this.orderDetail = true;
                return [2 /*return*/];
            });
        });
    };
    //When value change
    DeliveryProcessComponent.prototype.valuechange = function (event, input_name, download) {
        var _this_1 = this;
        if (this.onSearchActive == false) {
            this.onSearchActive = true;
            this.showLoading = true;
            var myVar = setTimeout(function () { return __awaiter(_this_1, void 0, void 0, function () {
                var clearFormInput, searchDate, filter, result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!this.form_input) return [3 /*break*/, 2];
                            clearFormInput = this.form_input;
                            searchDate = {};
                            // if (clearFormInput.request_date) {
                            //   if (clearFormInput.request_date.to) {
                            //     let request_date_from_to = {
                            //       request_date_from: clearFormInput.request_date.from,
                            //       request_date_to: clearFormInput.request_date.to
                            //     }
                            //     Object.assign(clearFormInput, request_date_from_to)
                            //   }
                            //   else {
                            //     let request_date_from = {
                            //       request_date_from: clearFormInput.request_date.from,
                            //     }
                            //     Object.assign(clearFormInput, request_date_from)
                            //   }
                            //   delete clearFormInput.request_date
                            // }
                            if (clearFormInput.approve_date && Object.keys(clearFormInput.approve_date).length === 0 && clearFormInput.approve_date.constructor === Object) {
                                delete clearFormInput.approve_date;
                            }
                            filter = {
                                search: clearFormInput,
                                order_by: this.orderByResult,
                                limit_per_page: this.pageLimits,
                                current_page: this.currentPage,
                                download: false
                            };
                            return [4 /*yield*/, this.orderhistoryService.searchShippingReport(filter)];
                        case 1:
                            result = _a.sent();
                            this.allData = result.values;
                            this.buildPagesNumbers(result.total_page);
                            return [3 /*break*/, 3];
                        case 2:
                            this.firstLoad();
                            _a.label = 3;
                        case 3:
                            this.onSearchActive = false;
                            this.showLoading = false;
                            return [2 /*return*/];
                    }
                });
            }); }, 2000);
        }
    };
    //Start of DatePicker Function
    DeliveryProcessComponent.prototype.datePickerOnDateSelection = function (date, formName) {
        var _this = this.make_This(formName);
        if (!_this.fromDate && !_this.toDate) {
            _this.fromDate = date;
        }
        else if (_this.fromDate && !_this.toDate && this.datePickerDateAfter(date, _this.fromDate)) {
            _this.toDate = date;
        }
        else {
            _this.toDate = null;
            _this.fromDate = date;
        }
        _this.from = (_this.fromDate != undefined && _this.fromDate != null) ? this.convertToDateString(_this.fromDate) : '';
        _this.to = (_this.toDate != undefined && _this.toDate != null) ? this.convertToDateString(_this.toDate) : '';
    };
    DeliveryProcessComponent.prototype.typeof = function (object) {
        return typeof object;
    };
    DeliveryProcessComponent.prototype.make_This = function (formName) {
        if (typeof formName == 'object') {
            return formName;
        }
        if (this.form_input[formName] == undefined || this.form_input[formName] == '') {
            this.form_input[formName] = {};
        }
        return this.form_input[formName];
    };
    DeliveryProcessComponent.prototype.dateToggler = function (formName) {
        this.toggler[formName] = (typeof this.toggler == undefined) ? true : !this.toggler[formName];
        if (this.toggler[formName] == false && this.form_input[formName] !== '') {
            this.valuechange({}, false, false);
        }
    };
    DeliveryProcessComponent.prototype.dateClear = function (event, dataInput, deleteInput) {
        this.form_input[dataInput] = '';
        this.valuechange(event, deleteInput);
    };
    DeliveryProcessComponent.prototype.datePickerOnDateIsInside = function (date, formName) {
        var _this = this.make_This(formName);
        return this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.toDate);
    };
    DeliveryProcessComponent.prototype.datePickerOnDateIsRange = function (date, formName) {
        var _this = this.make_This(formName);
        return this.datePickerDateEquals(date, _this.fromDate) || this.datePickerDateEquals(date, _this.toDate) || this.datePickerOnDateIsInside(date, _this) || this.datePickerOnDateIsHovered(date, _this);
    };
    DeliveryProcessComponent.prototype.datePickerOnDateIsHovered = function (date, formName) {
        var _this = this.make_This(formName);
        return _this.fromDate && !_this.toDate && _this.hoveredDate && this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.hoveredDate);
    };
    DeliveryProcessComponent.prototype.datePickerDateAfter = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day);
        if (iToDate.getTime() < iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    DeliveryProcessComponent.prototype.convertToDateString = function (datePrev) {
        console.log(datePrev);
        return datePrev.year.toString().padStart(2, "0") + '-' + datePrev.month.toString().padStart(2, "0") + '-' + datePrev.day.toString().padStart(2, "0");
    };
    DeliveryProcessComponent.prototype.datePickerDateEquals = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day);
        if (iToDate.getTime() == iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    DeliveryProcessComponent.prototype.datePickerDateBefore = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day);
        if (iToDate.getTime() > iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    //End of DatePicker Function
    //Start of Checkbox Function
    DeliveryProcessComponent.prototype.onChecked = function () {
        var _this_1 = this;
        setTimeout(function () {
            var getData = [];
            _this_1.checkBox.forEach(function (el, i) {
                var data = _this_1.allData[i];
                console.warn("var data", data);
                console.log(el);
                if (el) {
                    getData.push(data.reference_no);
                }
                else {
                    var index = getData.indexOf(data.booking_id);
                    if (index !== -1)
                        getData.splice(index, 1);
                }
            });
            _this_1.getDataCheck = getData;
            // console.warn("getData", getData);
        }, 100);
    };
    DeliveryProcessComponent.prototype.checkedAll = function () {
        var _this_1 = this;
        setTimeout(function () {
            var getData = [];
            _this_1.allData.forEach(function (e, i) {
                // console.log("result", e);
                if (_this_1.activeCheckbox) {
                    _this_1.checkBox[i] = _this_1.activeCheckbox;
                    getData.push(e.reference_no);
                }
                else {
                    _this_1.checkBox = [];
                }
            });
            _this_1.getDataCheck = getData;
            // console.log(getData);
        }, 100);
    };
    DeliveryProcessComponent.prototype.printLabel = function () {
        var _this_1 = this;
        try {
            var payload = {
                "reference_no": this.getDataCheck,
                "status": "ya"
            };
            this.orderhistoryService.setPrintedMark(payload);
            if (this.getDataCheck.length > 0)
                this.shippingLabel = this.getDataCheck.map(function (list) { return _this_1.allData.find(function (delivery) { return delivery.reference_no == list; }); });
        }
        catch (error) {
        }
    };
    DeliveryProcessComponent.prototype.addToPackingList = function () {
        var _this_1 = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: 'Confirmation',
            text: 'Apakah anda yakin ingin menambahkan order yang dipilih ke dalam packing list?',
            confirmButtonText: 'Process',
            cancelButtonText: "Cancel",
            showCancelButton: true
        }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
            var payload;
            var _this_1 = this;
            return __generator(this, function (_a) {
                if (result.isConfirmed) {
                    try {
                        payload = {
                            "reference_no": this.getDataCheck,
                        };
                        this.orderhistoryService.createPackingList(payload).then(function (result) {
                            console.log("result", result);
                            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                                title: 'Success',
                                text: 'Semua order yang terpilih telah ditambahkan kedalam packing list.',
                                icon: 'success',
                                confirmButtonText: 'Ok',
                                showCancelButton: false
                            }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    if (result.isConfirmed) {
                                        this.activeCheckbox = false;
                                        this.checkBox = [];
                                        this.getDataCheck = [];
                                        this.firstLoad();
                                    }
                                    return [2 /*return*/];
                                });
                            }); });
                        }, function (err) {
                            var _error = (err.message);
                            _this_1.errorLabel = (err.message);
                            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                                title: 'Error',
                                text: _error,
                                icon: 'error',
                                confirmButtonText: 'Ok',
                                showCancelButton: false
                            }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    if (result.isConfirmed) {
                                        this.activeCheckbox = false;
                                        this.checkBox = [];
                                        this.getDataCheck = [];
                                        this.firstLoad();
                                    }
                                    return [2 /*return*/];
                                });
                            }); });
                        });
                    }
                    catch (error) {
                        console.log(error);
                    }
                }
                return [2 /*return*/];
            });
        }); });
    };
    DeliveryProcessComponent.prototype.deletePrintLabel = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this_1 = this;
            return __generator(this, function (_a) {
                if (this.getDataCheck.length > 0) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                        title: 'Confirmation',
                        text: 'Apakah anda yakin ingin menghapus print marked order yang dipilih?',
                        // icon: 'success',
                        confirmButtonText: 'Process',
                        cancelButtonText: "Cancel",
                        showCancelButton: true
                    }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
                        var payload, error_1;
                        var _this_1 = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!result.isConfirmed) return [3 /*break*/, 4];
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    payload = {
                                        "reference_no": this.getDataCheck,
                                        "status": ""
                                    };
                                    return [4 /*yield*/, this.orderhistoryService.setPrintedMark(payload)];
                                case 2:
                                    _a.sent();
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                                        title: 'Success',
                                        text: 'Semua order yang terpilih telah dihapus print marked nya.',
                                        icon: 'success',
                                        confirmButtonText: 'Ok',
                                        showCancelButton: false
                                    }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            if (result.isConfirmed) {
                                                this.activeCheckbox = false;
                                                this.checkBox = [];
                                                this.getDataCheck = [];
                                                this.firstLoad();
                                            }
                                            return [2 /*return*/];
                                        });
                                    }); });
                                    return [3 /*break*/, 4];
                                case 3:
                                    error_1 = _a.sent();
                                    return [3 /*break*/, 4];
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); });
                }
                return [2 /*return*/];
            });
        });
    };
    DeliveryProcessComponent.prototype.clickToDownloadReport = function () {
        return __awaiter(this, void 0, void 0, function () {
            var clearFormInput, filter, result, e_2;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        // // Download All Order Report
                        // let no_filter = {
                        //   search: {
                        //     status: {
                        //       in:["PROCESSED","COMPLETED"]
                        //     }
                        //   },
                        //   order_by: this.orderByResult,
                        // }
                        // // Download Incoming Order Report
                        // let no_filter_incoming = {
                        //   search: {
                        //     status: "PROCESSED",
                        //     last_shipping_info: "on_processing"
                        //   },
                        //   order_by: this.orderByResult,
                        // }
                        // // Download On Delivery Order Report
                        // let no_filter_delivery = {
                        //   search: {
                        //     status: "PROCESSED",
                        //     last_shipping_info: "on_delivery"
                        //   },
                        //   order_by: this.orderByResult,
                        // }
                        // // Download Complete Order Report
                        // let no_filter_complete = {
                        //   search: {
                        //     status: "COMPLETED",
                        //     last_shipping_info: "delivered"
                        //   },
                        //   order_by: this.orderByResult,
                        // }
                        this.showLoading = true;
                        clearFormInput = this.form_input;
                        filter = {
                            search: clearFormInput,
                            order_by: this.orderByResult,
                        };
                        return [4 /*yield*/, this.orderhistoryService.searchOrderHistory(filter)];
                    case 1:
                        result = _a.sent();
                        this.allDataToDownload = result.values;
                        console.warn("allDataDownload", this.allDataToDownload);
                        setTimeout(function () { return __awaiter(_this_1, void 0, void 0, function () {
                            var dataType, tableSelect, tableHTML, filename, downloadLink, blob;
                            return __generator(this, function (_a) {
                                dataType = 'application/vnd.ms-excel';
                                tableSelect = document.getElementById('reportTable');
                                tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
                                filename = 'excel_data.xls';
                                downloadLink = document.createElement("a");
                                document.body.appendChild(downloadLink);
                                if (navigator.msSaveOrOpenBlob) {
                                    blob = new Blob(['\ufeff', tableHTML], {
                                        type: dataType
                                    });
                                    navigator.msSaveOrOpenBlob(blob, filename);
                                }
                                else {
                                    // Create a link to the file
                                    downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
                                    // Setting the file name
                                    downloadLink.download = filename;
                                    //triggering the function
                                    downloadLink.click();
                                    this.showLoading = false;
                                }
                                return [2 /*return*/];
                            });
                        }); }, 2000);
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.showLoading = false;
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    //see more detail function
    DeliveryProcessComponent.prototype.setSeeMore = function (index) {
        this.allData[index].product_status = true;
        // console.log(this.allData)
    };
    //see more detail function
    DeliveryProcessComponent.prototype.setSeeLess = function (index) {
        this.allData[index].product_status = false;
        // console.log(this.allData)
    };
    DeliveryProcessComponent.prototype.backToHere = function (obj) {
        obj.shippingLabel = false;
        obj.orderDetail = false;
        obj.checkBox = [];
        obj.activeCheckbox = false;
        obj.getDataCheck = [];
        obj.firstLoad();
    };
    DeliveryProcessComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_4__["MemberService"] }
    ]; };
    DeliveryProcessComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-delivery-process',
            template: __webpack_require__(/*! raw-loader!./delivery-process.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/delivery-process/delivery-process.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./delivery-process.component.scss */ "./src/app/layout/modules/delivery-process/delivery-process.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _services_member_member_service__WEBPACK_IMPORTED_MODULE_4__["MemberService"]])
    ], DeliveryProcessComponent);
    return DeliveryProcessComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/delivery-process/delivery-process.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/delivery-process.module.ts ***!
  \****************************************************************************/
/*! exports provided: DeliveryProcessModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryProcessModule", function() { return DeliveryProcessModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _delivery_process_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./delivery-process.component */ "./src/app/layout/modules/delivery-process/delivery-process.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _delivery_process_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./delivery-process-routing.module */ "./src/app/layout/modules/delivery-process/delivery-process-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var ngx_barcode__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-barcode */ "./node_modules/ngx-barcode/index.js");
/* harmony import */ var _shipping_label_shipping_label_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shipping-label/shipping-label.module */ "./src/app/layout/modules/delivery-process/shipping-label/shipping-label.module.ts");
/* harmony import */ var _edit_delivery_process_edit_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./edit/delivery-process.edit.component */ "./src/app/layout/modules/delivery-process/edit/delivery-process.edit.component.ts");
/* harmony import */ var _shipping_delivery_process_shipping_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shipping/delivery-process.shipping.component */ "./src/app/layout/modules/delivery-process/shipping/delivery-process.shipping.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












// import { OrderHistoriesAddComponent } from './add/order-histories.add.component';
// import { OrderHistoriesDetailComponent } from './detail/order-histories.detail.component';


var DeliveryProcessModule = /** @class */ (function () {
    function DeliveryProcessModule() {
    }
    DeliveryProcessModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _delivery_process_routing_module__WEBPACK_IMPORTED_MODULE_7__["DeliveryProcessRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__["MatCheckboxModule"],
                ngx_barcode__WEBPACK_IMPORTED_MODULE_10__["NgxBarcodeModule"],
                _shipping_label_shipping_label_module__WEBPACK_IMPORTED_MODULE_11__["ShippingLabelModule"]
            ],
            declarations: [
                _delivery_process_component__WEBPACK_IMPORTED_MODULE_2__["DeliveryProcessComponent"],
                _edit_delivery_process_edit_component__WEBPACK_IMPORTED_MODULE_12__["DeliveryProcessEditComponent"],
                _shipping_delivery_process_shipping_component__WEBPACK_IMPORTED_MODULE_13__["DeliveryProcessShippingComponent"]
                // ShippingLabelComponent
            ],
        })
    ], DeliveryProcessModule);
    return DeliveryProcessModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/delivery-process/edit/delivery-process.edit.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/edit/delivery-process.edit.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n  margin-top: 40px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -5px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 16px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail table {\n  width: 100%;\n  font-size: 14px;\n  box-shadow: 5px 5px 10px #999;\n  border-radius: 5px;\n  text-transform: capitalize;\n  overflow: hidden;\n}\n.card-detail table td {\n  border-width: 0px 1px 0px 0px;\n  padding: 15px;\n  vertical-align: top;\n}\n.card-detail table td em {\n  display: block;\n  margin-left: 5px;\n  font-size: 10px;\n  font-style: italic;\n}\n.card-detail table td.qty {\n  text-align: center;\n  height: 100%;\n}\n.card-detail table td.price {\n  text-align: center;\n}\n.card-detail table td.priceBil {\n  color: red;\n  text-align: center;\n}\n.card-detail table td.fee {\n  text-align: center;\n}\n.card-detail table td.priceTot {\n  text-transform: uppercase;\n  font-weight: bolder;\n  text-align: left;\n}\n.card-detail table td.priceTotal {\n  text-align: center;\n  font-weight: bolder;\n}\n.card-detail table th {\n  background-color: #F2F2F2;\n  color: #545454;\n  padding: 15px;\n  font-weight: bold;\n  text-transform: capitalize;\n}\n.card-detail table.services {\n  border-collapse: collapse;\n  text-align: center;\n}\n.card-detail th.product {\n  text-align: left;\n}\n.card-detail th.qty {\n  text-align: center;\n}\n.card-detail th.ut {\n  text-align: center;\n}\n.card-detail th.price {\n  text-align: center;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content .status-button-container {\n  display: flex;\n  justify-content: space-between;\n}\n.card-detail .card-content .status-button-container .option {\n  background-color: white;\n  width: 45%;\n  color: grey;\n  border: grey 1px solid;\n}\n.card-detail .card-content .status-button-container .paid {\n  background-color: blue;\n  border-color: blue;\n  color: white;\n}\n.card-detail .card-content .status-button-container .cancel {\n  background-color: #dc3545;\n  border-color: #dc3545;\n  color: white;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .orderhistoryallhistory-detail label {\n  margin-top: 10px;\n}\n.card-detail .orderhistoryallhistory-detail .head-one {\n  line-height: 33px;\n  border-bottom: 1px solid #333;\n}\n.card-detail .orderhistoryallhistory-detail .head-one label.order-id {\n  font-size: 18px;\n  overflow: hidden;\n  width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n}\n.card-detail .orderhistoryallhistory-detail .head-one label.buyer-name {\n  font-size: 24px;\n  overflow: hidden;\n  width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  color: #0EA8DB;\n}\n.card-detail .orderhistoryallhistory-detail .buyer-detail {\n  border-top: 1.5px solid #eaeaea;\n}\n.card-detail .orderhistoryallhistory-detail .buyer-detail label#buyer-detail {\n  font-size: 24px;\n  font-weight: bolder;\n  color: #0EA8DB;\n}\n.card-detail .orderhistoryallhistory-detail .buyer-detail label#buyer-detail .buyer-name {\n  font-size: 12px;\n}\n.card-detail .orderhistoryallhistory-detail .product-group {\n  padding-bottom: 25px;\n}\n.card-detail .orderhistoryallhistory-detail label + div {\n  font-weight: normal;\n}\n.card-detail .orderhistoryallhistory-detail .card-header {\n  background-color: white;\n  border-left: 3px solid blue;\n}\n.card-detail .orderhistoryallhistory-detail .card-header h2 {\n  color: #555;\n  font-size: 20px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list {\n  float: left;\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  overflow: hidden;\n  align-items: end;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list .awb {\n  font-size: 14px;\n  margin-bottom: 10px;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list .input-form {\n  width: 100%;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list > .process {\n  margin-left: 10px;\n  padding: 0px;\n  height: 32px;\n  width: 80px;\n  border-radius: 5px;\n}\n.shipping-date {\n  color: #999;\n  font-size: 10px;\n}\n.shipping-status {\n  float: left;\n  width: 100%;\n  border-left: 1.5px solid #eaeaea;\n  position: relative;\n  text-transform: capitalize;\n  font-weight: bold;\n}\n.shipping-status .radio-button {\n  display: flex;\n  width: 16px;\n  height: 16px;\n  border: 1.5px solid #eaeaea;\n  background: white;\n  border-radius: 16px;\n  float: left;\n  -webkit-transform: translate(-9px, -7px);\n          transform: translate(-9px, -7px);\n  position: absolute;\n  cursor: pointer;\n  justify-content: center;\n  align-items: center;\n}\n.shipping-status .radio-button + label {\n  display: block;\n  float: left;\n  width: 100%;\n  padding: 0px 5px 10px 10px;\n  -webkit-transform: translate(0px, -22px);\n          transform: translate(0px, -22px);\n  cursor: pointer;\n}\n.shipping-status .radio-button.checked-true {\n  border: none;\n  background-color: #2ecc71;\n}\n.shipping-status .radio-button.checked-true:before {\n  content: \"\";\n  background-color: #2ecc71;\n  display: block;\n  border-radius: 9px;\n  width: 8px !important;\n  height: 8px !important;\n}\n.shipping-status .radio-button.hovered-true:before {\n  background-color: #2ecc71;\n  width: 6px;\n  height: 6px;\n  content: \"\";\n  border-radius: 8px;\n  background-color: #2ecc71;\n  display: block;\n}\n.shipping-statuss {\n  float: left;\n  width: 100%;\n  border-left: 1.5px solid #eaeaea;\n  position: relative;\n  text-transform: capitalize;\n  font-weight: bold;\n}\n.shipping-statuss .radio-button {\n  display: flex;\n  width: 16px;\n  height: 16px;\n  border: 1.5px solid #eaeaea;\n  background: white;\n  border-radius: 16px;\n  float: left;\n  -webkit-transform: translate(-9px, -7px);\n          transform: translate(-9px, -7px);\n  position: absolute;\n  cursor: pointer;\n  justify-content: center;\n  align-items: center;\n  pointer-events: none;\n}\n.shipping-statuss .radio-button + label {\n  display: block;\n  float: left;\n  width: 100%;\n  padding: 0px 5px 10px 10px;\n  -webkit-transform: translate(0px, -22px);\n          transform: translate(0px, -22px);\n  pointer-events: none;\n}\n.shipping-statuss .radio-button.checked-true {\n  border: none;\n  background-color: #2ecc71;\n  pointer-events: none;\n}\n.shipping-statuss .radio-button.checked-true:before {\n  content: \"\";\n  display: block;\n  border-radius: 9px;\n  width: 8px !important;\n  height: 8px !important;\n}\n.shipping-statuss .radio-button.hovered-true:before {\n  width: 6px;\n  height: 6px;\n  content: \"\";\n  border-radius: 8px;\n  display: block;\n}\n.shipping-status:last-child {\n  border-left-color: transparent;\n}\n.printLabel {\n  text-align: center;\n}\n#line {\n  border-right: 1px solid black;\n  height: 20px;\n}\n.spacing {\n  font-size: 14px;\n}\ntextarea {\n  width: 100%;\n  height: 100px;\n  border-radius: 5px;\n}\n#cancel-note {\n  margin-top: 20px;\n}\n.payer {\n  text-align: left;\n  font-weight: bold;\n  padding: initial;\n}\n.payer table td {\n  color: #0EA8DB;\n}\n#header {\n  margin-top: 0px;\n}\n.save-active {\n  background-color: blue;\n}\n.save {\n  border: none;\n  color: white;\n  padding: 8px 6px;\n  text-align: center;\n  text-decoration: none;\n  font-size: 14px;\n  margin: -34px -7px;\n  float: right;\n  border-radius: 5px;\n}\n.change {\n  background-color: blue !important;\n}\n.edit {\n  background-color: #4CAF50;\n  border: none;\n  color: white;\n  padding: 8px 18px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  font-size: 14px;\n  margin: -31px 3px;\n  float: right;\n  border-radius: 5px;\n}\n.done {\n  background-color: blue;\n  border: none;\n  color: white;\n  padding: 8px 18px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  font-size: 14px;\n  margin: -31px 3px;\n  float: right;\n  border-radius: 5px;\n}\n.modal.false {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n.full {\n  width: 100% !important;\n}\n.half {\n  width: 50%;\n}\n.half-10 {\n  width: 47%;\n  height: 45px;\n}\n.cancel-hold {\n  background-color: white;\n  border-color: #8854D0;\n  color: #8854D0;\n}\n.confirm-hold {\n  background-color: #8854D0;\n  border-color: #8854D0;\n  color: white;\n}\n.modal.true {\n  display: block;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 100px;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n/* Modal Header */\n.modal-header {\n  padding: 2px 16px;\n  color: white;\n}\n/* Modal Body */\n.modal-body {\n  padding: 2px 16px;\n}\n.modal-body2 {\n  padding: 2px 16px;\n  font-size: 12px;\n}\n.modal-buttons {\n  display: flex;\n  padding: 2px 16px;\n  justify-content: space-between;\n}\n.container-nominal {\n  display: flex;\n  flex-direction: row;\n}\n/* Modal Footer */\n.modal-footer {\n  padding: 2px 16px;\n  color: white;\n}\n.red {\n  color: red;\n}\n/* Modal Content */\n.modal-content {\n  margin-left: 256px;\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  padding: 0;\n  border: 1px solid #888;\n  width: 50%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n.modal-content2 {\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  padding: 0;\n  border: 1px solid #888;\n  width: 35vw;\n  min-width: 300px;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n/* The Close Button */\n.close {\n  color: #000;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n.close:hover,\n.close:focus {\n  color: #000;\n  font-size: 30px;\n  font-weight: bolder;\n  text-decoration: none;\n  cursor: pointer;\n}\n/* Add Animation */\n@-webkit-keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n@keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\ntextarea {\n  padding: 10px;\n  line-height: 1.5;\n  border-radius: 5px;\n  border: 1px solid #ccc;\n  box-shadow: 1px 1px 1px #999;\n}\ncancel-order {\n  margin-left: 5px;\n}\n.logo {\n  position: relative;\n}\n.bg {\n  position: absolute;\n}\n@media only screen and (max-width: 1150px) {\n  .awb-list {\n    float: left;\n    width: 100%;\n    display: flex;\n    flex-direction: column;\n  }\n  .awb-list .awb {\n    font-size: 11px;\n  }\n\n  .awb-list > .process {\n    margin-left: 10px;\n    height: 32px;\n    width: 90px;\n  }\n}\n.example-radio-group {\n  display: flex;\n  flex-direction: column;\n  margin: 15px 0;\n}\n.example-radio-button {\n  margin: 5px;\n}\n.img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 10px 5px;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n  white-space: nowrap;\n  font-size: 12px;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.input-delivery-container {\n  display: flex;\n  flex-direction: column;\n  align-items: flex-start;\n  width: 100%;\n}\n.input-delivery-container input {\n  font-size: 14px;\n}\n.complete-order-container {\n  margin: 0 16px 16px;\n}\n.complete-order-container .completed-order-head {\n  font-size: 12px;\n  color: red;\n}\n.complete-order-container .complete-comfirmation {\n  font-size: 13px;\n  margin-top: 5px;\n}\n.complete-order-container label {\n  font-size: 12px;\n  margin-top: 0 !important;\n  margin-bottom: 0.2rem;\n}\n.complete-order-container .button-complete .half-button {\n  width: 48%;\n}\n.complete-order-container .button-complete .full-button {\n  margin-top: 16px;\n  width: 100%;\n}\nngb-timepicker ::ng-deep .ngb-tp-hour, ngb-timepicker ::ng-deep .ngb-tp-meridian, ngb-timepicker ::ng-deep .ngb-tp-minute, ngb-timepicker ::ng-deep .ngb-tp-second {\n  position: relative !important;\n  margin: 10px 0;\n}\nngb-timepicker ::ng-deep .ngb-tp-hour input, ngb-timepicker ::ng-deep .ngb-tp-meridian input, ngb-timepicker ::ng-deep .ngb-tp-minute input, ngb-timepicker ::ng-deep .ngb-tp-second input {\n  font-size: 14px;\n}\nngb-timepicker ::ng-deep .ngb-tp-hour .btn, ngb-timepicker ::ng-deep .ngb-tp-meridian .btn, ngb-timepicker ::ng-deep .ngb-tp-minute .btn, ngb-timepicker ::ng-deep .ngb-tp-second .btn {\n  position: absolute;\n  right: 5px;\n  padding: 0;\n  line-height: 1;\n  font-size: 10px;\n  color: grey;\n}\nngb-timepicker ::ng-deep .ngb-tp-hour .btn:first-child, ngb-timepicker ::ng-deep .ngb-tp-meridian .btn:first-child, ngb-timepicker ::ng-deep .ngb-tp-minute .btn:first-child, ngb-timepicker ::ng-deep .ngb-tp-second .btn:first-child {\n  top: 2px;\n}\nngb-timepicker ::ng-deep .ngb-tp-hour .btn:nth-of-type(2), ngb-timepicker ::ng-deep .ngb-tp-meridian .btn:nth-of-type(2), ngb-timepicker ::ng-deep .ngb-tp-minute .btn:nth-of-type(2), ngb-timepicker ::ng-deep .ngb-tp-second .btn:nth-of-type(2) {\n  bottom: 2px;\n}\n.delivered-form {\n  overflow: visible !important;\n}\n.redelivery-label {\n  position: relative !important;\n  z-index: 2 !important;\n}\n.return-form {\n  overflow: visible !important;\n  flex-direction: column !important;\n  align-items: start !important;\n}\n.return-form .information-text {\n  position: relative;\n  color: red;\n  font-size: 11px;\n  bottom: 3px;\n  text-transform: initial;\n}\n.order-complete-text {\n  font-size: 14px;\n  color: #0EA8DB;\n}\n.cancel-status {\n  margin-left: 20px;\n}\n.outer {\n  min-width: 20vw;\n  flex: 1;\n}\n.progress {\n  display: inline-flex;\n  height: 100%;\n  padding: 30px 20px;\n}\n.progress > div {\n  display: flex;\n  flex-direction: column;\n  color: #333;\n}\n.progress > div.left {\n  flex: 1;\n  padding-right: 20px;\n  text-align: right;\n}\n.progress > div.left div:last-of-type:after {\n  display: none;\n}\n.progress > div.left div:after {\n  content: \"\";\n  background: rgba(0, 128, 0, 0.1);\n  border-radius: 2px;\n  position: absolute;\n  right: -20px;\n  top: 10px;\n  height: 101%;\n  width: 1px;\n  -webkit-transform: translateX(50%);\n          transform: translateX(50%);\n}\n.progress > div.left .created-date {\n  font-size: 10px;\n  color: #767474;\n}\n.progress > div.right {\n  flex: 2;\n  padding-left: 20px;\n}\n.progress > div.right div:after {\n  content: \"\";\n  background: green;\n  border-radius: 2px;\n  position: absolute;\n  left: -20px;\n  top: 10px;\n  height: 101%;\n  width: 2px;\n  -webkit-transform: translateX(-50%);\n          transform: translateX(-50%);\n  transition: height 0.2s ease;\n}\n.progress > div.right .rowstate-name.prev:after {\n  transition: none;\n}\n.progress > div.right .rowstate-name:before {\n  content: \"\";\n  background: green;\n  padding: 5px;\n  border-radius: 50%;\n  position: absolute;\n  left: -20px;\n  top: 10px;\n  -webkit-transform: translateX(-50%) translateY(-50%);\n          transform: translateX(-50%) translateY(-50%);\n  transition: padding 0.2s ease;\n}\n.progress > div.right .description {\n  line-height: 1.5;\n  font-size: 10px;\n  position: relative;\n  bottom: 20px;\n  color: #767474;\n}\n.progress > div.right .current:after {\n  height: 0%;\n  transition: height 0.2s ease-out;\n}\n.progress > div.right .current .rowstate-name {\n  color: #333;\n  font-weight: bold;\n}\n.progress > div.right .current .rowstate-name:before {\n  background: green;\n  padding: 10px;\n  transition: all 0.2s 0.15s cubic-bezier(0.175, 0.885, 0.32, 2);\n}\n.progress > div.right .current .rowstate-name:after {\n  height: 0%;\n  transition: height 0.2s ease-out;\n}\n.progress > div.right .current .rowstate-name ~ div:before {\n  background: #a8a5a5;\n  padding: 2.5px;\n}\n.progress > div.right .current .rowstate-name ~ div:after {\n  height: 0%;\n  transition: none;\n}\n.progress > div div {\n  flex: 1;\n  position: relative;\n  line-height: 20px;\n  cursor: default;\n  min-height: 42px;\n}\n.button-tracking {\n  color: #007bff;\n  padding-top: 6px;\n  clear: both;\n}\n.shipping-id {\n  padding-bottom: 0 !important;\n  color: #0EA8DB;\n}\n.shipping-status-row {\n  color: #016918;\n  border-bottom: 1.5px solid #eaeaea;\n  border-width: unset !important;\n}\n.shipping-detail {\n  text-align: right;\n  cursor: pointer;\n}\n.default-loading {\n  text-align: center;\n}\n.default-loading i {\n  font-size: 50px;\n  color: #0780f8;\n  margin: 40px 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGVsaXZlcnktcHJvY2Vzcy9lZGl0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZGVsaXZlcnktcHJvY2Vzc1xcZWRpdFxcZGVsaXZlcnktcHJvY2Vzcy5lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9kZWxpdmVyeS1wcm9jZXNzL2VkaXQvZGVsaXZlcnktcHJvY2Vzcy5lZGl0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDQVI7QURDUTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNBWjtBRENZO0VBQ0ksaUJBQUE7QUNDaEI7QURFUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBS0EsV0FBQTtBQ0xaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBRElRO0VBQ0kseUJBQUE7QUNGWjtBREtJO0VBQ0ksV0FBQTtFQUdBLGVBQUE7RUFDQSw2QkFBQTtFQUNBLGtCQUFBO0VBQ0EsMEJBQUE7RUFDQSxnQkFBQTtBQ0xSO0FETVE7RUFHQyw2QkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQ05UO0FET1M7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNMYjtBRFVRO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0FDUlo7QURVUTtFQUNJLGtCQUFBO0FDUlo7QURXUTtFQUNJLFVBQUE7RUFDQSxrQkFBQTtBQ1RaO0FEWVE7RUFDSSxrQkFBQTtBQ1ZaO0FEYVE7RUFFSSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUNaWjtBRGVRO0VBRUksa0JBQUE7RUFDQSxtQkFBQTtBQ2RaO0FEaUJRO0VBQ0kseUJBQUE7RUFDQSxjQUFBO0VBRUEsYUFBQTtFQUVBLGlCQUFBO0VBQ0EsMEJBQUE7QUNqQlo7QUR1Q0k7RUFFSSx5QkFBQTtFQUNBLGtCQUFBO0FDdENSO0FEeUNJO0VBQ0ksZ0JBQUE7QUN2Q1I7QUQyQ0k7RUFDSSxrQkFBQTtBQ3pDUjtBRDRDSTtFQUNJLGtCQUFBO0FDMUNSO0FENkNJO0VBQ0ksa0JBQUE7QUMzQ1I7QUQ4Q0k7RUFDSSxhQUFBO0FDNUNSO0FENkNPO0VBQ0ksYUFBQTtFQUNBLDhCQUFBO0FDM0NYO0FENENXO0VBQ0ksdUJBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0FDMUNmO0FENENXO0VBQ0Msc0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUMxQ1o7QUQ0Q1c7RUFDQyx5QkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtBQzFDWjtBRDZDUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUMzQ1o7QUQrQ0k7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQzdDUjtBRGlEUTtFQUNJLGdCQUFBO0FDL0NaO0FEaURRO0VBQ0ksaUJBQUE7RUFDQSw2QkFBQTtBQy9DWjtBRGdEWTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDOUNoQjtBRGdEWTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQzlDaEI7QURrRFE7RUFDSSwrQkFBQTtBQ2hEWjtBRGlEWTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUMvQ2hCO0FEZ0RnQjtFQUVJLGVBQUE7QUMvQ3BCO0FEb0RRO0VBQ0ksb0JBQUE7QUNsRFo7QURxRFE7RUFFSSxtQkFBQTtBQ3BEWjtBRHVGUTtFQUNJLHVCQUFBO0VBQ0EsMkJBQUE7QUNyRlo7QURzRlk7RUFDSSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ3BGaEI7QUR3RlE7RUFDSSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUN0Rlo7QUR1Rlk7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUNyRmhCO0FEeUZZO0VBQ0ksV0FBQTtBQ3ZGaEI7QUQwRlE7RUFDSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDeEZaO0FENkZBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUMxRko7QUQ0RkE7RUFDSSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGdDQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtFQUNBLGlCQUFBO0FDekZKO0FEMEZJO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMkJBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLHdDQUFBO1VBQUEsZ0NBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDeEZSO0FEMkZJO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFFQSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSx3Q0FBQTtVQUFBLGdDQUFBO0VBQ0EsZUFBQTtBQzFGUjtBRDhGSTtFQUVJLFlBQUE7RUFDQSx5QkFBQTtBQzdGUjtBRGdHSTtFQUNJLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7QUM5RlI7QURnR0k7RUFDSSx5QkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0FDOUZSO0FEa0dBO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxnQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsMEJBQUE7RUFDQSxpQkFBQTtBQy9GSjtBRGdHSTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDJCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSx3Q0FBQTtVQUFBLGdDQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0FDOUZSO0FEaUdJO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFFQSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSx3Q0FBQTtVQUFBLGdDQUFBO0VBQ0Esb0JBQUE7QUNoR1I7QURxR0k7RUFFSSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxvQkFBQTtBQ3BHUjtBRHVHSTtFQUNJLFdBQUE7RUFFQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLHNCQUFBO0FDdEdSO0FEd0dJO0VBRUksVUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFFQSxjQUFBO0FDeEdSO0FENEdBO0VBQ0ksOEJBQUE7QUN6R0o7QUQ0R0E7RUFDSSxrQkFBQTtBQ3pHSjtBRDRHQztFQUNJLDZCQUFBO0VBQ0EsWUFBQTtBQ3pHTDtBRG9IQTtFQUNJLGVBQUE7QUNqSEo7QURxSEE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0FDbEhKO0FEc0hBO0VBQ0ksZ0JBQUE7QUNuSEo7QURzSEE7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNuSEo7QURzSFM7RUFDSSxjQUFBO0FDcEhiO0FEeUhBO0VBQ0ksZUFBQTtBQ3RISjtBRHlIQTtFQUNJLHNCQUFBO0FDdEhKO0FEd0hBO0VBRUksWUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFFQSxlQUFBO0VBQ0Esa0JBQUE7RUFFQSxZQUFBO0VBQ0Esa0JBQUE7QUN4SEo7QUQwSEE7RUFDSSxpQ0FBQTtBQ3ZISjtBRDBIQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUVBLFlBQUE7RUFDQSxrQkFBQTtBQ3hISjtBRDJIQTtFQUNJLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUVBLFlBQUE7RUFDQSxrQkFBQTtBQ3pISjtBRDRIQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUNBLE9BQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsNEJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0NBQUE7RUFDQSxxQkFBQTtBQ3pISjtBRDJIQTtFQUNJLHNCQUFBO0FDeEhKO0FEMEhBO0VBQ0ksVUFBQTtBQ3ZISjtBRHlIQTtFQUNJLFVBQUE7RUFDQSxZQUFBO0FDdEhKO0FEeUhBO0VBQ0ksdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7QUN0SEo7QUR3SEE7RUFDSSx5QkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtBQ3JISjtBRHdIQTtFQUNJLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUNBLFdBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsNEJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0NBQUE7RUFDQSxxQkFBQTtBQ3JISjtBRHlIQSxpQkFBQTtBQUNBO0VBQ0ksaUJBQUE7RUFFQSxZQUFBO0FDdkhKO0FEMEhBLGVBQUE7QUFDQTtFQUNJLGlCQUFBO0FDdkhKO0FEeUhBO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0FDdEhKO0FEd0hBO0VBQ0ksYUFBQTtFQUNBLGlCQUFBO0VBQ0EsOEJBQUE7QUNySEo7QUR1SEE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUNwSEo7QUR1SEEsaUJBQUE7QUFDQTtFQUNJLGlCQUFBO0VBRUEsWUFBQTtBQ3JISjtBRHVIQTtFQUNJLFVBQUE7QUNwSEo7QURzSEEsa0JBQUE7QUFDQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esc0JBQUE7RUFDQSxVQUFBO0VBQ0EsNEVBQUE7RUFDQSxrQ0FBQTtVQUFBLDBCQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLGtCQUFBO0FDbkhKO0FEcUhBO0VBRUksa0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBRUEsZ0JBQUE7RUFDQSw0RUFBQTtFQUNBLGtDQUFBO1VBQUEsMEJBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0Esa0JBQUE7QUNwSEo7QUR3SEEscUJBQUE7QUFDQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDckhKO0FEd0hBOztFQUVJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7QUNySEo7QUR5SEEsa0JBQUE7QUFDQTtFQUNJO0lBQ0ksV0FBQTtJQUNBLFVBQUE7RUN0SE47RUR5SEU7SUFDSSxNQUFBO0lBQ0EsVUFBQTtFQ3ZITjtBQUNGO0FEOEdBO0VBQ0k7SUFDSSxXQUFBO0lBQ0EsVUFBQTtFQ3RITjtFRHlIRTtJQUNJLE1BQUE7SUFDQSxVQUFBO0VDdkhOO0FBQ0Y7QUQwSEE7RUFDSSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUN4SEo7QUQySEE7RUFDSSxnQkFBQTtBQ3hISjtBRDJIQTtFQUNJLGtCQUFBO0FDeEhKO0FEMkhBO0VBQ0ksa0JBQUE7QUN4SEo7QUQySEE7RUFDSztJQUNJLFdBQUE7SUFDQSxXQUFBO0lBQ0EsYUFBQTtJQUNBLHNCQUFBO0VDeEhQO0VEMEhPO0lBQ0ksZUFBQTtFQ3hIWDs7RUQ4SEc7SUFDSSxpQkFBQTtJQUVBLFlBQUE7SUFDQSxXQUFBO0VDNUhQO0FBQ0Y7QURnSUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0FDOUhKO0FEaUlFO0VBQ0UsV0FBQTtBQzlISjtBRGtKQTtFQUNJLGFBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUMvSUo7QURrSkE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQy9JSjtBRGtKQTtFQUNJLHdCQUFBO0FDL0lKO0FEa0pBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0FDL0lKO0FEaUpJO0VBQ0ksZUFBQTtBQy9JUjtBRG1KQTtFQUNJLG1CQUFBO0FDaEpKO0FEa0pJO0VBQ0ksZUFBQTtFQUNBLFVBQUE7QUNoSlI7QURtSkk7RUFDSSxlQUFBO0VBQ0EsZUFBQTtBQ2pKUjtBRG9KSTtFQUNJLGVBQUE7RUFDQSx3QkFBQTtFQUNBLHFCQUFBO0FDbEpSO0FEcUpRO0VBQ0ksVUFBQTtBQ25KWjtBRHNKUTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtBQ3BKWjtBRDJKUTtFQUNJLDZCQUFBO0VBQ0EsY0FBQTtBQ3hKWjtBRDBKWTtFQUNJLGVBQUE7QUN4SmhCO0FEMkpZO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ3pKaEI7QUQ0Slk7RUFDSSxRQUFBO0FDMUpoQjtBRDZKWTtFQUNJLFdBQUE7QUMzSmhCO0FEaUtBO0VBQ0ksNEJBQUE7QUM5Sko7QURpS0E7RUFDSSw2QkFBQTtFQUNBLHFCQUFBO0FDOUpKO0FEaUtBO0VBQ0ksNEJBQUE7RUFDQSxpQ0FBQTtFQUNBLDZCQUFBO0FDOUpKO0FEZ0tJO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtBQzlKUjtBRGtLQTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDL0pKO0FEa0tBO0VBQ0ksaUJBQUE7QUMvSko7QURtS0E7RUFDSSxlQUFBO0VBQ0EsT0FBQTtBQ2hLSjtBRG1LRTtFQVdFLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDMUtKO0FENEtJO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FWVztBQ2hLakI7QUQ0S007RUFDRSxPQUFBO0VBQ0EsbUJBckJFO0VBc0JGLGlCQUFBO0FDMUtSO0FEOEtVO0VBQ0UsYUFBQTtBQzVLWjtBRCtLVTtFQUNFLFdBQUE7RUFDQSxnQ0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0NBQUE7VUFBQSwwQkFBQTtBQzdLWjtBRGlMUTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDL0taO0FEbUxNO0VBQ0UsT0FBQTtFQUNBLGtCQW5ERTtBQzlIVjtBRHFMWTtFQUNJLFdBQUE7RUFDQSxpQkFyREU7RUFzREYsa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLFVBNURDO0VBNkRELG1DQUFBO1VBQUEsMkJBQUE7RUFDQSw0QkFBQTtBQ25MaEI7QUR5TGM7RUFDRSxnQkFBQTtBQ3ZMaEI7QUQ0TFU7RUFDRSxXQUFBO0VBQ0EsaUJBM0VNO0VBNEVOLFlBOUVRO0VBK0VSLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLG9EQUFBO1VBQUEsNENBQUE7RUFDQSw2QkFBQTtBQzFMWjtBRDhMUTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUM1TFo7QURnTVk7RUFDSSxVQUFBO0VBQ0EsZ0NBQUE7QUM5TGhCO0FEZ01ZO0VBQ0ksV0FsR0k7RUFtR0osaUJBQUE7QUM5TGhCO0FEZ01nQjtFQUNJLGlCQXhHRjtFQXlHRSxhQUFBO0VBQ0EsOERBQUE7QUM5THBCO0FEaU1nQjtFQUNJLFVBQUE7RUFDQSxnQ0FBQTtBQy9McEI7QURxTW9CO0VBQ0ksbUJBckhQO0VBc0hPLGNBQUE7QUNuTXhCO0FEc01vQjtFQUNJLFVBQUE7RUFDQSxnQkFBQTtBQ3BNeEI7QUQyTU07RUFDRSxPQUFBO0VBRUEsa0JBQUE7RUFDQSxpQkExSVU7RUEySVYsZUFBQTtFQUNBLGdCQUFBO0FDMU1SO0FEbU5FO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQ2hOSjtBRG1ORTtFQUNFLDRCQUFBO0VBQ0EsY0FBQTtBQ2hOSjtBRG1ORTtFQUNFLGNBQUE7RUFDQSxrQ0FBQTtFQUNBLDhCQUFBO0FDaE5KO0FEbU5FO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0FDaE5KO0FEdU5FO0VBQ0ksa0JBQUE7QUNwTk47QURxTk07RUFDRSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUNuTlIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9kZWxpdmVyeS1wcm9jZXNzL2VkaXQvZGVsaXZlcnktcHJvY2Vzcy5lZGl0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDhweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA0MHB4O1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTVweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxNnB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b24udHJ1ZXtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICB0YWJsZXtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAvLyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDVweCA1cHggMTBweCAjOTk5O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBvdmVyZmxvdzpoaWRkZW47XHJcbiAgICAgICAgdGR7XHJcbiAgICAgICAgLy8gIGJvcmRlci1zdHlsZTpzb2xpZDtcclxuICAgICAgICAvLyAgYm9yZGVyLWNvbG9yOiAjMzMzO1xyXG4gICAgICAgICBib3JkZXItd2lkdGg6IDBweCAxcHggICAwcHggMHB4O1xyXG4gICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG4gICAgICAgICBlbXtcclxuICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuXHJcbiAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRkLnF0eXtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCVcclxuICAgICAgICB9XHJcbiAgICAgICAgdGQucHJpY2V7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGQucHJpY2VCaWx7XHJcbiAgICAgICAgICAgIGNvbG9yOnJlZDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGQuZmVle1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRkLnByaWNlVG90e1xyXG4gICAgICAgICAgICAvLyBib3JkZXItdG9wOiAxLjVweCBzb2xpZCAjZWFlYWVhO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOmxlZnQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0ZC5wcmljZVRvdGFse1xyXG4gICAgICAgICAgICAvLyBib3JkZXItdG9wOiAxLjVweCBzb2xpZCAjZWFlYWVhO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoe1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjJGMkYyO1xyXG4gICAgICAgICAgICBjb2xvcjojNTQ1NDU0O1xyXG4gICAgICAgICAgICAvLyBib3JkZXI6IDFweCBzb2xpZCAjMzMzO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgICAgICAvLyB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICAgICAgLy8gYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHRoLnByb2R1Y3R7XHJcbiAgICAgICAgLy8gICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgLy8gICAgIC8vIGRpc3BsYXk6IGZsZXhcclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIC8vIHRoLnF0eXtcclxuICAgICAgICAvLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIC8vIH1cclxuXHJcbiAgICAgICAgLy8gdGgudXR7XHJcbiAgICAgICAgLy8gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIC8vIHRoLnByaWNle1xyXG4gICAgICAgIC8vICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgfVxyXG5cclxuICAgIHRhYmxlLnNlcnZpY2Vze1xyXG4gICAgICAgIC8vIGJvcmRlci1yaWdodDogM3B4IHNvbGlkIGJsYWNrO1xyXG4gICAgICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIH1cclxuICAgIHRoLnByb2R1Y3Qge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgLy8gZGlzcGxheTogZmxleFxyXG4gICAgfVxyXG5cclxuICAgIHRoLnF0eSB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIHRoLnV0IHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgdGgucHJpY2Uge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAuc3RhdHVzLWJ1dHRvbi1jb250YWluZXIge1xyXG4gICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgICAgIC5vcHRpb24ge1xyXG4gICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgd2lkdGg6IDQ1JTtcclxuICAgICAgICAgICAgICAgY29sb3I6IGdyZXk7XHJcbiAgICAgICAgICAgICAgIGJvcmRlcjogZ3JleSAxcHggc29saWQ7XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgICAgIC5wYWlkIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiBibHVlO1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgICAgIC5jYW5jZWwge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGMzNTQ1O1xyXG4gICAgICAgICAgICBib3JkZXItY29sb3I6ICNkYzM1NDU7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICB9XHJcbiAgICAgICB9XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbHtcclxuICAgICAgIFxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaGVhZC1vbmV7XHJcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAzM3B4O1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzMzMztcclxuICAgICAgICAgICAgbGFiZWwub3JkZXItaWR7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbGFiZWwuYnV5ZXItbmFtZXtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiMwRUE4REI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgICAgLmJ1eWVyLWRldGFpbHtcclxuICAgICAgICAgICAgYm9yZGVyLXRvcDogMS41cHggc29saWQgI2VhZWFlYTtcclxuICAgICAgICAgICAgbGFiZWwjYnV5ZXItZGV0YWlse1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyNHB4O1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiMwRUE4REI7XHJcbiAgICAgICAgICAgICAgICAuYnV5ZXItbmFtZXtcclxuICAgICAgICAgICAgICAgICAgICAvLyBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICBcclxuICAgICAgICAucHJvZHVjdC1ncm91cHtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDI1cHg7XHJcbiAgICAgICAgICAgIC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWFsO1xyXG4gICAgICAgICAgICAvLyBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICAvLyBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICAvLyBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICAvLyBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICAvLyBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIC8vIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyAuaW1hZ2V7XHJcbiAgICAgICAgLy8gICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgLy8gICAgID5kaXZ7XHJcbiAgICAgICAgLy8gICAgICAgICB3aWR0aDogMzMuMzMzMyU7XHJcbiAgICAgICAgLy8gICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgLy8gICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vICAgICAuY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgLy8gICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gICAgIGgze1xyXG4gICAgICAgIC8vICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIC8vICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIC8vICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICAvLyAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgLy8gICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAvLyAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAvLyAgICAgICAgIGltZ3tcclxuICAgICAgICAvLyAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vIH1cclxuXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGJvcmRlci1sZWZ0OiAzcHggc29saWQgYmx1ZTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5hd2ItbGlzdHtcclxuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIG92ZXJmbG93OmhpZGRlbjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6ZW5kO1xyXG4gICAgICAgICAgICAuYXdie1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbToxMHB4O1xyXG4gICAgICAgICAgICAgICAgLy8gbWFyZ2luLXRvcDogMTBweDtcclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmlucHV0LWZvcm0ge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmF3Yi1saXN0ID4gLnByb2Nlc3N7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OjEwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMnB4O1xyXG4gICAgICAgICAgICB3aWR0aDo4MHB4O1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4uc2hpcHBpbmctZGF0ZXtcclxuICAgIGNvbG9yOiAjOTk5O1xyXG4gICAgZm9udC1zaXplOiAxMHB4O1xyXG59XHJcbi5zaGlwcGluZy1zdGF0dXN7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyLWxlZnQ6IDEuNXB4IHNvbGlkICNlYWVhZWE7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgLnJhZGlvLWJ1dHRvbntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgICAgIGhlaWdodDogMTZweDtcclxuICAgICAgICBib3JkZXI6IDEuNXB4IHNvbGlkICNlYWVhZWE7XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTZweDtcclxuICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtOXB4LCAtN3B4KTtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG4gICAgfVxyXG4gICAgLnJhZGlvLWJ1dHRvbitsYWJlbHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAvLyB3aWR0aDogY2FsYyg3MCUgLSAyN3B4KTtcclxuICAgICAgICB3aWR0aDoxMDAlO1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCA1cHggMTBweCAxMHB4O1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgLTIycHgpO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIC5yYWRpby1idXR0b24uY2hlY2tlZC10cnVle1xyXG4gICAgICAgIC8vIGJvcmRlci1jb2xvcjogIzJlY2M3MTtcclxuICAgICAgICBib3JkZXI6bm9uZTtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiMyZWNjNzE7XHJcbiAgICB9XHJcblxyXG4gICAgLnJhZGlvLWJ1dHRvbi5jaGVja2VkLXRydWU6YmVmb3Jle1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA5cHg7XHJcbiAgICAgICAgd2lkdGg6IDhweCAgIWltcG9ydGFudDtcclxuICAgICAgICBoZWlnaHQ6IDhweCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gICAgLnJhZGlvLWJ1dHRvbi5ob3ZlcmVkLXRydWU6YmVmb3Jle1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyZWNjNzE7XHJcbiAgICAgICAgd2lkdGg6IDZweDtcclxuICAgICAgICBoZWlnaHQ6IDZweDtcclxuICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmVjYzcxO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgfVxyXG59XHJcblxyXG4uc2hpcHBpbmctc3RhdHVzc3tcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXItbGVmdDogMS41cHggc29saWQgI2VhZWFlYTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAucmFkaW8tYnV0dG9ue1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgd2lkdGg6IDE2cHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxNnB4O1xyXG4gICAgICAgIGJvcmRlcjogMS41cHggc29saWQgI2VhZWFlYTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxNnB4O1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC05cHgsIC03cHgpO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuXHJcbiAgICB9XHJcbiAgICAucmFkaW8tYnV0dG9uK2xhYmVse1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIC8vIHdpZHRoOiBjYWxjKDcwJSAtIDI3cHgpO1xyXG4gICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogMHB4IDVweCAxMHB4IDEwcHg7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAtMjJweCk7XHJcbiAgICAgICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XHJcbiAgICAgICAgLy8gY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgLnJhZGlvLWJ1dHRvbi5jaGVja2VkLXRydWV7XHJcbiAgICAgICAgLy8gYm9yZGVyLWNvbG9yOiAjMmVjYzcxO1xyXG4gICAgICAgIGJvcmRlcjpub25lO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IzJlY2M3MTtcclxuICAgICAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICAgIH1cclxuXHJcbiAgICAucmFkaW8tYnV0dG9uLmNoZWNrZWQtdHJ1ZTpiZWZvcmV7XHJcbiAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMmVjYzcxO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDlweDtcclxuICAgICAgICB3aWR0aDogOHB4ICAhaW1wb3J0YW50O1xyXG4gICAgICAgIGhlaWdodDogOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgICAucmFkaW8tYnV0dG9uLmhvdmVyZWQtdHJ1ZTpiZWZvcmV7XHJcbiAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcclxuICAgICAgICB3aWR0aDogNnB4O1xyXG4gICAgICAgIGhlaWdodDogNnB4O1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICMyZWNjNzE7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5zaGlwcGluZy1zdGF0dXM6bGFzdC1jaGlsZHtcclxuICAgIGJvcmRlci1sZWZ0LWNvbG9yOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuLnByaW50TGFiZWx7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbiAjbGluZSB7XHJcbiAgICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgYmxhY2s7XHJcbiAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gfVxyXG5cclxuLy8gIC5idG4tcHJpbnR7XHJcbi8vICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzJhYWQyYztcclxuLy8gICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4vLyAgICAgIGNvbG9yOndoaXRlO1xyXG4vLyAgICAgIHdpZHRoOiA1MCU7XHJcbi8vICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgfVxyXG5cclxuLnNwYWNpbmd7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAvLyB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxufVxyXG5cclxudGV4dGFyZWF7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIC8vIGJvcmRlci10b3A6IDFweCBzb2xpZDtcclxufVxyXG5cclxuI2NhbmNlbC1ub3Rle1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxufSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG5cclxuLnBheWVye1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgcGFkZGluZzppbml0aWFsO1xyXG4gICAgXHJcbiAgICAgdGFibGV7XHJcbiAgICAgICAgIHRke1xyXG4gICAgICAgICAgICAgY29sb3I6IzBFQThEQjtcclxuICAgICAgICAgfVxyXG4gICAgIH1cclxufVxyXG5cclxuI2hlYWRlcntcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxufVxyXG5cclxuLnNhdmUtYWN0aXZlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XHJcbn1cclxuLnNhdmV7XHJcbiAgICBcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDhweCA2cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAvLyBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBtYXJnaW46IC0zNHB4IC03cHg7XHJcbiAgICAvLyBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuLmNoYW5nZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsdWUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmVkaXR7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNENBRjUwO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZzogOHB4IDE4cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBtYXJnaW46IC0zMXB4IDNweDtcclxuICAgIC8vIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5cclxuLmRvbmV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZzogOHB4IDE4cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBtYXJnaW46IC0zMXB4IDNweDtcclxuICAgIC8vIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5cclxuLm1vZGFsLmZhbHNlIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgLyogU3RheSBpbiBwbGFjZSAqL1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIC8qIFNpdCBvbiB0b3AgKi9cclxuICAgIHBhZGRpbmctdG9wOiAxMDBweDtcclxuICAgIC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0b3A6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIC8qIEZ1bGwgd2lkdGggKi9cclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIC8qIEZ1bGwgaGVpZ2h0ICovXHJcbiAgICBvdmVyZmxvdzogYXV0bztcclxuICAgIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcbiAgICAvKiBGYWxsYmFjayBjb2xvciAqL1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xyXG4gICAgLyogQmxhY2sgdy8gb3BhY2l0eSAqL1xyXG59XHJcbi5mdWxse1xyXG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG4uaGFsZiB7XHJcbiAgICB3aWR0aDogNTAlO1xyXG59XHJcbi5oYWxmLTEwIHtcclxuICAgIHdpZHRoOiA0NyU7XHJcbiAgICBoZWlnaHQgOiA0NXB4O1xyXG59XHJcblxyXG4uY2FuY2VsLWhvbGQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBib3JkZXItY29sb3I6ICM4ODU0RDA7XHJcbiAgICBjb2xvcjogIzg4NTREMDtcclxufVxyXG4uY29uZmlybS1ob2xke1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzg4NTREMDtcclxuICAgIGJvcmRlci1jb2xvcjogIzg4NTREMDtcclxuICAgIGNvbG9yOiB3aGl0ZVxyXG59XHJcblxyXG4ubW9kYWwudHJ1ZSB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAvKiBTdGF5IGluIHBsYWNlICovXHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgLyogU2l0IG9uIHRvcCAqL1xyXG4gICAgcGFkZGluZy10b3A6IDEwMHB4O1xyXG4gICAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xyXG4gICAgbGVmdDogMTAwcHg7XHJcbiAgICB0b3A6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIC8qIEZ1bGwgd2lkdGggKi9cclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIC8qIEZ1bGwgaGVpZ2h0ICovXHJcbiAgICBvdmVyZmxvdzogYXV0bztcclxuICAgIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcbiAgICAvKiBGYWxsYmFjayBjb2xvciAqL1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xyXG4gICAgLyogQmxhY2sgdy8gb3BhY2l0eSAqL1xyXG59XHJcblxyXG5cclxuLyogTW9kYWwgSGVhZGVyICovXHJcbi5tb2RhbC1oZWFkZXIge1xyXG4gICAgcGFkZGluZzogMnB4IDE2cHg7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjNWNiODVjO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4vKiBNb2RhbCBCb2R5ICovXHJcbi5tb2RhbC1ib2R5IHtcclxuICAgIHBhZGRpbmc6IDJweCAxNnB4O1xyXG59XHJcbi5tb2RhbC1ib2R5MiB7XHJcbiAgICBwYWRkaW5nOiAycHggMTZweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxufVxyXG4ubW9kYWwtYnV0dG9ucyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgcGFkZGluZzogMnB4IDE2cHg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuLmNvbnRhaW5lci1ub21pbmFsIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG59XHJcblxyXG4vKiBNb2RhbCBGb290ZXIgKi9cclxuLm1vZGFsLWZvb3RlciB7XHJcbiAgICBwYWRkaW5nOiAycHggMTZweDtcclxuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICM1Y2I4NWM7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLnJlZHtcclxuICAgIGNvbG9yOnJlZDtcclxufVxyXG4vKiBNb2RhbCBDb250ZW50ICovXHJcbi5tb2RhbC1jb250ZW50IHtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNTZweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzg4ODtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDAuNHM7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLm1vZGFsLWNvbnRlbnQyIHtcclxuICAgIFxyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xyXG4gICAgd2lkdGg6IDM1dnc7XHJcbiAgIFxyXG4gICAgbWluLXdpZHRoOiAzMDBweDtcclxuICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XHJcbiAgICBhbmltYXRpb24tbmFtZTogYW5pbWF0ZXRvcDtcclxuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuXHJcbi8qIFRoZSBDbG9zZSBCdXR0b24gKi9cclxuLmNsb3NlIHtcclxuICAgIGNvbG9yOiAjMDAwOztcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGZvbnQtc2l6ZTogMjhweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4uY2xvc2U6aG92ZXIsXHJcbi5jbG9zZTpmb2N1cyB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcblxyXG4vKiBBZGQgQW5pbWF0aW9uICovXHJcbkBrZXlmcmFtZXMgYW5pbWF0ZXRvcCB7XHJcbiAgICBmcm9tIHtcclxuICAgICAgICB0b3A6IC0zMDBweDtcclxuICAgICAgICBvcGFjaXR5OiAwXHJcbiAgICB9XHJcblxyXG4gICAgdG8ge1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgICBvcGFjaXR5OiAxXHJcbiAgICB9XHJcbn1cclxuXHJcbnRleHRhcmVhe1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCAxcHggIzk5OTtcclxufVxyXG5cclxuY2FuY2VsLW9yZGVye1xyXG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcclxufVxyXG5cclxuLmxvZ297XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5iZ3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMTUwcHgpe1xyXG4gICAgIC5hd2ItbGlzdCB7XHJcbiAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuXHJcbiAgICAgICAgIC5hd2Ige1xyXG4gICAgICAgICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICAgICAgICAvLyAgbWFyZ2luLXRvcDogMTBweDtcclxuXHJcbiAgICAgICAgIH1cclxuICAgICB9XHJcblxyXG4gICAgIC5hd2ItbGlzdD4ucHJvY2VzcyB7XHJcbiAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIC8vICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgIGhlaWdodDogMzJweDtcclxuICAgICAgICAgd2lkdGg6IDkwcHg7XHJcbiAgICAgfVxyXG59XHJcblxyXG5cclxuLmV4YW1wbGUtcmFkaW8tZ3JvdXAge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBtYXJnaW46IDE1cHggMDtcclxuICB9XHJcbiAgXHJcbiAgLmV4YW1wbGUtcmFkaW8tYnV0dG9uIHtcclxuICAgIG1hcmdpbjogNXB4O1xyXG4gIH1cclxuICBcclxuLy8gLnByb2Nlc3N7XHJcbi8vICAgIGRpc3BsYXk6ZmxleDtcclxuLy8gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuLy8gfVxyXG5cclxuXHJcblxyXG4vLyB0YWJsZSNjdXN0b21fdGFibGV7XHJcbi8vICAgYmFja2dyb3VuZC1jb2xvcjp3aGl0ZTtcclxuXHJcblxyXG5cclxuLy8gICB0aHtcclxuLy8gICAgICAgYmFja2dyb3VuZC1jb2xvcjp3aGl0ZTtcclxuLy8gICB9XHJcbi8vIH1cclxuXHJcbi5pbWctdXBsb2FkIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBtYXJnaW46MTBweCA1cHg7XHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIGJvcmRlcjowO1xyXG4gICAgY3Vyc29yOnBvaW50ZXI7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcblxyXG5idXR0b24uYnRuX2RlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcclxufVxyXG5cclxuLmlucHV0LWRlbGl2ZXJ5LWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG5cclxuICAgIGlucHV0IHtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jb21wbGV0ZS1vcmRlci1jb250YWluZXIge1xyXG4gICAgbWFyZ2luOiAwIDE2cHggMTZweDtcclxuXHJcbiAgICAuY29tcGxldGVkLW9yZGVyLWhlYWQge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgfVxyXG5cclxuICAgIC5jb21wbGV0ZS1jb21maXJtYXRpb24ge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgbGFiZWwge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMC4ycmVtO1xyXG4gICAgfVxyXG4gICAgLmJ1dHRvbi1jb21wbGV0ZSB7XHJcbiAgICAgICAgLmhhbGYtYnV0dG9uIHtcclxuICAgICAgICAgICAgd2lkdGg6NDglO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmZ1bGwtYnV0dG9uIHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTZweDtcclxuICAgICAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbm5nYi10aW1lcGlja2VyIHtcclxuICAgIDo6bmctZGVlcCB7XHJcbiAgICAgICAgLm5nYi10cC1ob3VyLCAubmdiLXRwLW1lcmlkaWFuLCAubmdiLXRwLW1pbnV0ZSwgLm5nYi10cC1zZWNvbmQge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgbWFyZ2luOiAxMHB4IDA7XHJcblxyXG4gICAgICAgICAgICBpbnB1dCB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC5idG4ge1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgcmlnaHQ6IDVweDtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiBncmV5O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAuYnRuOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgICAgICAgIHRvcDogMnB4O1xyXG4gICAgICAgICAgICB9IFxyXG5cclxuICAgICAgICAgICAgLmJ0bjpudGgtb2YtdHlwZSgyKSB7XHJcbiAgICAgICAgICAgICAgICBib3R0b206IDJweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmRlbGl2ZXJlZC1mb3JtIHtcclxuICAgIG92ZXJmbG93OiB2aXNpYmxlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5yZWRlbGl2ZXJ5LWxhYmVsIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xyXG4gICAgei1pbmRleDogMiAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ucmV0dXJuLWZvcm0ge1xyXG4gICAgb3ZlcmZsb3c6IHZpc2libGUgIWltcG9ydGFudDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW4gIWltcG9ydGFudDtcclxuICAgIGFsaWduLWl0ZW1zOiBzdGFydCAhaW1wb3J0YW50O1xyXG5cclxuICAgIC5pbmZvcm1hdGlvbi10ZXh0IHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgY29sb3I6IHJlZDtcclxuICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgYm90dG9tOiAzcHg7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IGluaXRpYWw7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5vcmRlci1jb21wbGV0ZS10ZXh0IHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGNvbG9yOiAjMEVBOERCO1xyXG59XHJcblxyXG4uY2FuY2VsLXN0YXR1c3tcclxuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG5cclxuLm91dGVyIHtcclxuICAgIG1pbi13aWR0aDogMjB2dztcclxuICAgIGZsZXg6IDE7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzcyB7XHJcbiAgICAkZ2FwOiAyMHB4O1xyXG4gICAgJGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgJGJ1bGxldC1yYWRpdXM6IDVweDtcclxuICAgICRsaW5lLXRoaWNrOiAycHg7XHJcbiAgICAkc3RyaXAtY29sb3I6IGdyZWVuO1xyXG4gICAgJG5leHQtY29sb3I6ICNhOGE1YTU7XHJcbiAgICAkY3VycmVudC1jb2xvcjogIzMzMztcclxuICAgICRwcmV2LWNvbG9yOiAjMzMzO1xyXG4gICAgXHJcbiAgXHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHBhZGRpbmc6IDMwcHggMjBweDtcclxuICBcclxuICAgID4gZGl2IHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgY29sb3I6ICRwcmV2LWNvbG9yO1xyXG4gIFxyXG4gICAgICAmLmxlZnQge1xyXG4gICAgICAgIGZsZXg6MTtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAkZ2FwO1xyXG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vIExpbmVcclxuICAgICAgICBkaXYge1xyXG4gICAgICAgICAgJjpsYXN0LW9mLXR5cGU6YWZ0ZXIge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgXHJcbiAgICAgICAgICAmOmFmdGVyIHtcclxuICAgICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogZmFkZV9vdXQoJHN0cmlwLWNvbG9yLCAuOSk7IC8vcmdiYSgwLCAwLCAwLCAwLjYpO1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgcmlnaHQ6IC0kZ2FwO1xyXG4gICAgICAgICAgICB0b3A6ICRsaW5lLWhlaWdodC8yO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMSU7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxcHg7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCg1MCUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNyZWF0ZWQtZGF0ZSB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgICYucmlnaHQge1xyXG4gICAgICAgIGZsZXg6MjtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6ICRnYXA7XHJcblxyXG4gICAgICAgIGRpdiB7XHJcbiAgICAgICAgICAgIC8vIExpbmVcclxuICAgICAgICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogJHN0cmlwLWNvbG9yOyAvL3JnYmEoMCwgMCwgMCwgMC42KTtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIGxlZnQ6IC0kZ2FwO1xyXG4gICAgICAgICAgICAgICAgdG9wOiAkbGluZS1oZWlnaHQvMjtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTAxJTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAkbGluZS10aGljaztcclxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcclxuICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IGhlaWdodCAwLjJzIGVhc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgXHJcbiAgICAgICAgLnJvd3N0YXRlLW5hbWUgeyAgICAgICAgXHJcbiAgICAgICAgICAmLnByZXYgeyAgICAgICAgICBcclxuICAgICAgICAgICAgICAmOmFmdGVyIHtcclxuICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgICAgLy8gRG90XHJcbiAgICAgICAgICAmOmJlZm9yZSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICRzdHJpcC1jb2xvcjtcclxuICAgICAgICAgICAgcGFkZGluZzogJGJ1bGxldC1yYWRpdXM7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBsZWZ0OiAtJGdhcDtcclxuICAgICAgICAgICAgdG9wOiAkbGluZS1oZWlnaHQvMjtcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpIHRyYW5zbGF0ZVkoLTUwJSk7XHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IHBhZGRpbmcgMC4ycyBlYXNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmRlc2NyaXB0aW9ue1xyXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgYm90dG9tOiAyMHB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzc2NzQ3NDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jdXJyZW50IHtcclxuICAgICAgICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDAlO1xyXG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogaGVpZ2h0IC4ycyBlYXNlLW91dDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAucm93c3RhdGUtbmFtZSB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogJGN1cnJlbnQtY29sb3I7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuXHJcbiAgICAgICAgICAgICAgICAmOmJlZm9yZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogJHN0cmlwLWNvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6ICRidWxsZXQtcmFkaXVzICogMjtcclxuICAgICAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4ycyAuMTVzIGN1YmljLWJlemllcigwLjE3NSwgMC44ODUsIDAuMzIsIDIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICY6YWZ0ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogaGVpZ2h0IC4ycyBlYXNlLW91dDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB+IGRpdiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gY29sb3I6ICRuZXh0LWNvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogJG5leHQtY29sb3I7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6ICRidWxsZXQtcmFkaXVzICogMC41O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIGRpdiB7XHJcbiAgICAgICAgZmxleDogMTtcclxuICAgICAgICAvL291dGxpbmU6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMSk7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAkbGluZS1oZWlnaHQ7XHJcbiAgICAgICAgY3Vyc29yOiBkZWZhdWx0O1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDQycHg7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy8gJjpsYXN0LW9mLXR5cGUge1xyXG4gICAgICAgIC8vICAgZmxleDogMDtcclxuICAgICAgICAvLyB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5idXR0b24tdHJhY2tpbmcge1xyXG4gICAgY29sb3I6ICMwMDdiZmY7XHJcbiAgICBwYWRkaW5nLXRvcDogNnB4OztcclxuICAgIGNsZWFyOiBib3RoO1xyXG4gIH1cclxuXHJcbiAgLnNoaXBwaW5nLWlkIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogIzBFQThEQjtcclxuICB9XHJcblxyXG4gIC5zaGlwcGluZy1zdGF0dXMtcm93IHtcclxuICAgIGNvbG9yOiAjMDE2OTE4O1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMS41cHggc29saWQgI2VhZWFlYTtcclxuICAgIGJvcmRlci13aWR0aDogdW5zZXQgIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIC5zaGlwcGluZy1kZXRhaWwge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG5cclxuICAucHJvZHVjdC1saXN0IHtcclxuICAgICAgXHJcbiAgfVxyXG5cclxuICAuZGVmYXVsdC1sb2FkaW5nIHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBpIHtcclxuICAgICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgY29sb3I6ICMwNzgwZjg7XHJcbiAgICAgICAgbWFyZ2luOiA0MHB4IDA7XHJcbiAgICAgIH1cclxuICB9IiwiLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiA0OHB4O1xuICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC01cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDE2cHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XG59XG4uY2FyZC1kZXRhaWwgdGFibGUge1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3gtc2hhZG93OiA1cHggNXB4IDEwcHggIzk5OTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5jYXJkLWRldGFpbCB0YWJsZSB0ZCB7XG4gIGJvcmRlci13aWR0aDogMHB4IDFweCAwcHggMHB4O1xuICBwYWRkaW5nOiAxNXB4O1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRkIGVtIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRkLnF0eSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRkLnByaWNlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRkLnByaWNlQmlsIHtcbiAgY29sb3I6IHJlZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRkLmZlZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jYXJkLWRldGFpbCB0YWJsZSB0ZC5wcmljZVRvdCB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG4uY2FyZC1kZXRhaWwgdGFibGUgdGQucHJpY2VUb3RhbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cbi5jYXJkLWRldGFpbCB0YWJsZSB0aCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGMkYyRjI7XG4gIGNvbG9yOiAjNTQ1NDU0O1xuICBwYWRkaW5nOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG4uY2FyZC1kZXRhaWwgdGFibGUuc2VydmljZXMge1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uY2FyZC1kZXRhaWwgdGgucHJvZHVjdCB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG4uY2FyZC1kZXRhaWwgdGgucXR5IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmNhcmQtZGV0YWlsIHRoLnV0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmNhcmQtZGV0YWlsIHRoLnByaWNlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgLnN0YXR1cy1idXR0b24tY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgLnN0YXR1cy1idXR0b24tY29udGFpbmVyIC5vcHRpb24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDQ1JTtcbiAgY29sb3I6IGdyZXk7XG4gIGJvcmRlcjogZ3JleSAxcHggc29saWQ7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCAuc3RhdHVzLWJ1dHRvbi1jb250YWluZXIgLnBhaWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xuICBib3JkZXItY29sb3I6IGJsdWU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IC5zdGF0dXMtYnV0dG9uLWNvbnRhaW5lciAuY2FuY2VsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2RjMzU0NTtcbiAgYm9yZGVyLWNvbG9yOiAjZGMzNTQ1O1xuICBjb2xvcjogd2hpdGU7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5oZWFkLW9uZSB7XG4gIGxpbmUtaGVpZ2h0OiAzM3B4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzMzMztcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmhlYWQtb25lIGxhYmVsLm9yZGVyLWlkIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5oZWFkLW9uZSBsYWJlbC5idXllci1uYW1lIHtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIGNvbG9yOiAjMEVBOERCO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuYnV5ZXItZGV0YWlsIHtcbiAgYm9yZGVyLXRvcDogMS41cHggc29saWQgI2VhZWFlYTtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmJ1eWVyLWRldGFpbCBsYWJlbCNidXllci1kZXRhaWwge1xuICBmb250LXNpemU6IDI0cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIGNvbG9yOiAjMEVBOERCO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuYnV5ZXItZGV0YWlsIGxhYmVsI2J1eWVyLWRldGFpbCAuYnV5ZXItbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLnByb2R1Y3QtZ3JvdXAge1xuICBwYWRkaW5nLWJvdHRvbTogMjVweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCBibHVlO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogIzU1NTtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmF3Yi1saXN0IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBhbGlnbi1pdGVtczogZW5kO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuYXdiLWxpc3QgLmF3YiB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmF3Yi1saXN0IC5pbnB1dC1mb3JtIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5hd2ItbGlzdCA+IC5wcm9jZXNzIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmc6IDBweDtcbiAgaGVpZ2h0OiAzMnB4O1xuICB3aWR0aDogODBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4uc2hpcHBpbmctZGF0ZSB7XG4gIGNvbG9yOiAjOTk5O1xuICBmb250LXNpemU6IDEwcHg7XG59XG5cbi5zaGlwcGluZy1zdGF0dXMge1xuICBmbG9hdDogbGVmdDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1sZWZ0OiAxLjVweCBzb2xpZCAjZWFlYWVhO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5zaGlwcGluZy1zdGF0dXMgLnJhZGlvLWJ1dHRvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxNnB4O1xuICBoZWlnaHQ6IDE2cHg7XG4gIGJvcmRlcjogMS41cHggc29saWQgI2VhZWFlYTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDE2cHg7XG4gIGZsb2F0OiBsZWZ0O1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtOXB4LCAtN3B4KTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnNoaXBwaW5nLXN0YXR1cyAucmFkaW8tYnV0dG9uICsgbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwcHggNXB4IDEwcHggMTBweDtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAtMjJweCk7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5zaGlwcGluZy1zdGF0dXMgLnJhZGlvLWJ1dHRvbi5jaGVja2VkLXRydWUge1xuICBib3JkZXI6IG5vbmU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyZWNjNzE7XG59XG4uc2hpcHBpbmctc3RhdHVzIC5yYWRpby1idXR0b24uY2hlY2tlZC10cnVlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyZWNjNzE7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBib3JkZXItcmFkaXVzOiA5cHg7XG4gIHdpZHRoOiA4cHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiA4cHggIWltcG9ydGFudDtcbn1cbi5zaGlwcGluZy1zdGF0dXMgLnJhZGlvLWJ1dHRvbi5ob3ZlcmVkLXRydWU6YmVmb3JlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcbiAgd2lkdGg6IDZweDtcbiAgaGVpZ2h0OiA2cHg7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5zaGlwcGluZy1zdGF0dXNzIHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItbGVmdDogMS41cHggc29saWQgI2VhZWFlYTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uc2hpcHBpbmctc3RhdHVzcyAucmFkaW8tYnV0dG9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDE2cHg7XG4gIGhlaWdodDogMTZweDtcbiAgYm9yZGVyOiAxLjVweCBzb2xpZCAjZWFlYWVhO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTZweDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC05cHgsIC03cHgpO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuLnNoaXBwaW5nLXN0YXR1c3MgLnJhZGlvLWJ1dHRvbiArIGxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZsb2F0OiBsZWZ0O1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMHB4IDVweCAxMHB4IDEwcHg7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgLTIycHgpO1xuICBwb2ludGVyLWV2ZW50czogbm9uZTtcbn1cbi5zaGlwcGluZy1zdGF0dXNzIC5yYWRpby1idXR0b24uY2hlY2tlZC10cnVlIHtcbiAgYm9yZGVyOiBub25lO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmVjYzcxO1xuICBwb2ludGVyLWV2ZW50czogbm9uZTtcbn1cbi5zaGlwcGluZy1zdGF0dXNzIC5yYWRpby1idXR0b24uY2hlY2tlZC10cnVlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBib3JkZXItcmFkaXVzOiA5cHg7XG4gIHdpZHRoOiA4cHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiA4cHggIWltcG9ydGFudDtcbn1cbi5zaGlwcGluZy1zdGF0dXNzIC5yYWRpby1idXR0b24uaG92ZXJlZC10cnVlOmJlZm9yZSB7XG4gIHdpZHRoOiA2cHg7XG4gIGhlaWdodDogNnB4O1xuICBjb250ZW50OiBcIlwiO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4uc2hpcHBpbmctc3RhdHVzOmxhc3QtY2hpbGQge1xuICBib3JkZXItbGVmdC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5cbi5wcmludExhYmVsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4jbGluZSB7XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIGJsYWNrO1xuICBoZWlnaHQ6IDIwcHg7XG59XG5cbi5zcGFjaW5nIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG50ZXh0YXJlYSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbiNjYW5jZWwtbm90ZSB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbi5wYXllciB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nOiBpbml0aWFsO1xufVxuLnBheWVyIHRhYmxlIHRkIHtcbiAgY29sb3I6ICMwRUE4REI7XG59XG5cbiNoZWFkZXIge1xuICBtYXJnaW4tdG9wOiAwcHg7XG59XG5cbi5zYXZlLWFjdGl2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XG59XG5cbi5zYXZlIHtcbiAgYm9yZGVyOiBub25lO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDhweCA2cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbjogLTM0cHggLTdweDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5jaGFuZ2Uge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlICFpbXBvcnRhbnQ7XG59XG5cbi5lZGl0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzRDQUY1MDtcbiAgYm9yZGVyOiBub25lO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDhweCAxOHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbjogLTMxcHggM3B4O1xuICBmbG9hdDogcmlnaHQ7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLmRvbmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xuICBib3JkZXI6IG5vbmU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogOHB4IDE4cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luOiAtMzFweCAzcHg7XG4gIGZsb2F0OiByaWdodDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4ubW9kYWwuZmFsc2Uge1xuICBkaXNwbGF5OiBub25lO1xuICAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgei1pbmRleDogMTtcbiAgLyogU2l0IG9uIHRvcCAqL1xuICBwYWRkaW5nLXRvcDogMTAwcHg7XG4gIC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgLyogRnVsbCB3aWR0aCAqL1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIEZ1bGwgaGVpZ2h0ICovXG4gIG92ZXJmbG93OiBhdXRvO1xuICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG59XG5cbi5mdWxsIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbn1cblxuLmhhbGYge1xuICB3aWR0aDogNTAlO1xufVxuXG4uaGFsZi0xMCB7XG4gIHdpZHRoOiA0NyU7XG4gIGhlaWdodDogNDVweDtcbn1cblxuLmNhbmNlbC1ob2xkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1jb2xvcjogIzg4NTREMDtcbiAgY29sb3I6ICM4ODU0RDA7XG59XG5cbi5jb25maXJtLWhvbGQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjODg1NEQwO1xuICBib3JkZXItY29sb3I6ICM4ODU0RDA7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLm1vZGFsLnRydWUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cbiAgcG9zaXRpb246IGZpeGVkO1xuICAvKiBTdGF5IGluIHBsYWNlICovXG4gIHotaW5kZXg6IDE7XG4gIC8qIFNpdCBvbiB0b3AgKi9cbiAgcGFkZGluZy10b3A6IDEwMHB4O1xuICAvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXG4gIGxlZnQ6IDEwMHB4O1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICAvKiBGdWxsIHdpZHRoICovXG4gIGhlaWdodDogMTAwJTtcbiAgLyogRnVsbCBoZWlnaHQgKi9cbiAgb3ZlcmZsb3c6IGF1dG87XG4gIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICAvKiBGYWxsYmFjayBjb2xvciAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cbn1cblxuLyogTW9kYWwgSGVhZGVyICovXG4ubW9kYWwtaGVhZGVyIHtcbiAgcGFkZGluZzogMnB4IDE2cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLyogTW9kYWwgQm9keSAqL1xuLm1vZGFsLWJvZHkge1xuICBwYWRkaW5nOiAycHggMTZweDtcbn1cblxuLm1vZGFsLWJvZHkyIHtcbiAgcGFkZGluZzogMnB4IDE2cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm1vZGFsLWJ1dHRvbnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nOiAycHggMTZweDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuXG4uY29udGFpbmVyLW5vbWluYWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xufVxuXG4vKiBNb2RhbCBGb290ZXIgKi9cbi5tb2RhbC1mb290ZXIge1xuICBwYWRkaW5nOiAycHggMTZweDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4ucmVkIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLyogTW9kYWwgQ29udGVudCAqL1xuLm1vZGFsLWNvbnRlbnQge1xuICBtYXJnaW4tbGVmdDogMjU2cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiAwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xuICB3aWR0aDogNTAlO1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICBhbmltYXRpb24tbmFtZTogYW5pbWF0ZXRvcDtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjRzO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5tb2RhbC1jb250ZW50MiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiAwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xuICB3aWR0aDogMzV2dztcbiAgbWluLXdpZHRoOiAzMDBweDtcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vKiBUaGUgQ2xvc2UgQnV0dG9uICovXG4uY2xvc2Uge1xuICBjb2xvcjogIzAwMDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDI4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uY2xvc2U6aG92ZXIsXG4uY2xvc2U6Zm9jdXMge1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLyogQWRkIEFuaW1hdGlvbiAqL1xuQGtleWZyYW1lcyBhbmltYXRldG9wIHtcbiAgZnJvbSB7XG4gICAgdG9wOiAtMzAwcHg7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICB0byB7XG4gICAgdG9wOiAwO1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbn1cbnRleHRhcmVhIHtcbiAgcGFkZGluZzogMTBweDtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBib3gtc2hhZG93OiAxcHggMXB4IDFweCAjOTk5O1xufVxuXG5jYW5jZWwtb3JkZXIge1xuICBtYXJnaW4tbGVmdDogNXB4O1xufVxuXG4ubG9nbyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmJnIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDExNTBweCkge1xuICAuYXdiLWxpc3Qge1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuICAuYXdiLWxpc3QgLmF3YiB7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICB9XG5cbiAgLmF3Yi1saXN0ID4gLnByb2Nlc3Mge1xuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgIGhlaWdodDogMzJweDtcbiAgICB3aWR0aDogOTBweDtcbiAgfVxufVxuLmV4YW1wbGUtcmFkaW8tZ3JvdXAge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBtYXJnaW46IDE1cHggMDtcbn1cblxuLmV4YW1wbGUtcmFkaW8tYnV0dG9uIHtcbiAgbWFyZ2luOiA1cHg7XG59XG5cbi5pbWctdXBsb2FkIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbjogMTBweCA1cHg7XG59XG5cbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXI6IDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG5idXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcbn1cblxuLmlucHV0LWRlbGl2ZXJ5LWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICB3aWR0aDogMTAwJTtcbn1cbi5pbnB1dC1kZWxpdmVyeS1jb250YWluZXIgaW5wdXQge1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5jb21wbGV0ZS1vcmRlci1jb250YWluZXIge1xuICBtYXJnaW46IDAgMTZweCAxNnB4O1xufVxuLmNvbXBsZXRlLW9yZGVyLWNvbnRhaW5lciAuY29tcGxldGVkLW9yZGVyLWhlYWQge1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiByZWQ7XG59XG4uY29tcGxldGUtb3JkZXItY29udGFpbmVyIC5jb21wbGV0ZS1jb21maXJtYXRpb24ge1xuICBmb250LXNpemU6IDEzcHg7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5jb21wbGV0ZS1vcmRlci1jb250YWluZXIgbGFiZWwge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi10b3A6IDAgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogMC4ycmVtO1xufVxuLmNvbXBsZXRlLW9yZGVyLWNvbnRhaW5lciAuYnV0dG9uLWNvbXBsZXRlIC5oYWxmLWJ1dHRvbiB7XG4gIHdpZHRoOiA0OCU7XG59XG4uY29tcGxldGUtb3JkZXItY29udGFpbmVyIC5idXR0b24tY29tcGxldGUgLmZ1bGwtYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogMTZweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbm5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLWhvdXIsIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLW1lcmlkaWFuLCBuZ2ItdGltZXBpY2tlciA6Om5nLWRlZXAgLm5nYi10cC1taW51dGUsIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLXNlY29uZCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xuICBtYXJnaW46IDEwcHggMDtcbn1cbm5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLWhvdXIgaW5wdXQsIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLW1lcmlkaWFuIGlucHV0LCBuZ2ItdGltZXBpY2tlciA6Om5nLWRlZXAgLm5nYi10cC1taW51dGUgaW5wdXQsIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLXNlY29uZCBpbnB1dCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbm5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLWhvdXIgLmJ0biwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtbWVyaWRpYW4gLmJ0biwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtbWludXRlIC5idG4sIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLXNlY29uZCAuYnRuIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogNXB4O1xuICBwYWRkaW5nOiAwO1xuICBsaW5lLWhlaWdodDogMTtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBjb2xvcjogZ3JleTtcbn1cbm5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLWhvdXIgLmJ0bjpmaXJzdC1jaGlsZCwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtbWVyaWRpYW4gLmJ0bjpmaXJzdC1jaGlsZCwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtbWludXRlIC5idG46Zmlyc3QtY2hpbGQsIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLXNlY29uZCAuYnRuOmZpcnN0LWNoaWxkIHtcbiAgdG9wOiAycHg7XG59XG5uZ2ItdGltZXBpY2tlciA6Om5nLWRlZXAgLm5nYi10cC1ob3VyIC5idG46bnRoLW9mLXR5cGUoMiksIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLW1lcmlkaWFuIC5idG46bnRoLW9mLXR5cGUoMiksIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLW1pbnV0ZSAuYnRuOm50aC1vZi10eXBlKDIpLCBuZ2ItdGltZXBpY2tlciA6Om5nLWRlZXAgLm5nYi10cC1zZWNvbmQgLmJ0bjpudGgtb2YtdHlwZSgyKSB7XG4gIGJvdHRvbTogMnB4O1xufVxuXG4uZGVsaXZlcmVkLWZvcm0ge1xuICBvdmVyZmxvdzogdmlzaWJsZSAhaW1wb3J0YW50O1xufVxuXG4ucmVkZWxpdmVyeS1sYWJlbCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xuICB6LWluZGV4OiAyICFpbXBvcnRhbnQ7XG59XG5cbi5yZXR1cm4tZm9ybSB7XG4gIG92ZXJmbG93OiB2aXNpYmxlICFpbXBvcnRhbnQ7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW4gIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IHN0YXJ0ICFpbXBvcnRhbnQ7XG59XG4ucmV0dXJuLWZvcm0gLmluZm9ybWF0aW9uLXRleHQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGNvbG9yOiByZWQ7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgYm90dG9tOiAzcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBpbml0aWFsO1xufVxuXG4ub3JkZXItY29tcGxldGUtdGV4dCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICMwRUE4REI7XG59XG5cbi5jYW5jZWwtc3RhdHVzIHtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5cbi5vdXRlciB7XG4gIG1pbi13aWR0aDogMjB2dztcbiAgZmxleDogMTtcbn1cblxuLnByb2dyZXNzIHtcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gIGhlaWdodDogMTAwJTtcbiAgcGFkZGluZzogMzBweCAyMHB4O1xufVxuLnByb2dyZXNzID4gZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgY29sb3I6ICMzMzM7XG59XG4ucHJvZ3Jlc3MgPiBkaXYubGVmdCB7XG4gIGZsZXg6IDE7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuLnByb2dyZXNzID4gZGl2LmxlZnQgZGl2Omxhc3Qtb2YtdHlwZTphZnRlciB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG4ucHJvZ3Jlc3MgPiBkaXYubGVmdCBkaXY6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDEyOCwgMCwgMC4xKTtcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAtMjBweDtcbiAgdG9wOiAxMHB4O1xuICBoZWlnaHQ6IDEwMSU7XG4gIHdpZHRoOiAxcHg7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCg1MCUpO1xufVxuLnByb2dyZXNzID4gZGl2LmxlZnQgLmNyZWF0ZWQtZGF0ZSB7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgY29sb3I6ICM3Njc0NzQ7XG59XG4ucHJvZ3Jlc3MgPiBkaXYucmlnaHQge1xuICBmbGV4OiAyO1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG59XG4ucHJvZ3Jlc3MgPiBkaXYucmlnaHQgZGl2OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgYmFja2dyb3VuZDogZ3JlZW47XG4gIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAtMjBweDtcbiAgdG9wOiAxMHB4O1xuICBoZWlnaHQ6IDEwMSU7XG4gIHdpZHRoOiAycHg7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbiAgdHJhbnNpdGlvbjogaGVpZ2h0IDAuMnMgZWFzZTtcbn1cbi5wcm9ncmVzcyA+IGRpdi5yaWdodCAucm93c3RhdGUtbmFtZS5wcmV2OmFmdGVyIHtcbiAgdHJhbnNpdGlvbjogbm9uZTtcbn1cbi5wcm9ncmVzcyA+IGRpdi5yaWdodCAucm93c3RhdGUtbmFtZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBiYWNrZ3JvdW5kOiBncmVlbjtcbiAgcGFkZGluZzogNXB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogLTIwcHg7XG4gIHRvcDogMTBweDtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpIHRyYW5zbGF0ZVkoLTUwJSk7XG4gIHRyYW5zaXRpb246IHBhZGRpbmcgMC4ycyBlYXNlO1xufVxuLnByb2dyZXNzID4gZGl2LnJpZ2h0IC5kZXNjcmlwdGlvbiB7XG4gIGxpbmUtaGVpZ2h0OiAxLjU7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3R0b206IDIwcHg7XG4gIGNvbG9yOiAjNzY3NDc0O1xufVxuLnByb2dyZXNzID4gZGl2LnJpZ2h0IC5jdXJyZW50OmFmdGVyIHtcbiAgaGVpZ2h0OiAwJTtcbiAgdHJhbnNpdGlvbjogaGVpZ2h0IDAuMnMgZWFzZS1vdXQ7XG59XG4ucHJvZ3Jlc3MgPiBkaXYucmlnaHQgLmN1cnJlbnQgLnJvd3N0YXRlLW5hbWUge1xuICBjb2xvcjogIzMzMztcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4ucHJvZ3Jlc3MgPiBkaXYucmlnaHQgLmN1cnJlbnQgLnJvd3N0YXRlLW5hbWU6YmVmb3JlIHtcbiAgYmFja2dyb3VuZDogZ3JlZW47XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIDAuMTVzIGN1YmljLWJlemllcigwLjE3NSwgMC44ODUsIDAuMzIsIDIpO1xufVxuLnByb2dyZXNzID4gZGl2LnJpZ2h0IC5jdXJyZW50IC5yb3dzdGF0ZS1uYW1lOmFmdGVyIHtcbiAgaGVpZ2h0OiAwJTtcbiAgdHJhbnNpdGlvbjogaGVpZ2h0IDAuMnMgZWFzZS1vdXQ7XG59XG4ucHJvZ3Jlc3MgPiBkaXYucmlnaHQgLmN1cnJlbnQgLnJvd3N0YXRlLW5hbWUgfiBkaXY6YmVmb3JlIHtcbiAgYmFja2dyb3VuZDogI2E4YTVhNTtcbiAgcGFkZGluZzogMi41cHg7XG59XG4ucHJvZ3Jlc3MgPiBkaXYucmlnaHQgLmN1cnJlbnQgLnJvd3N0YXRlLW5hbWUgfiBkaXY6YWZ0ZXIge1xuICBoZWlnaHQ6IDAlO1xuICB0cmFuc2l0aW9uOiBub25lO1xufVxuLnByb2dyZXNzID4gZGl2IGRpdiB7XG4gIGZsZXg6IDE7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGN1cnNvcjogZGVmYXVsdDtcbiAgbWluLWhlaWdodDogNDJweDtcbn1cblxuLmJ1dHRvbi10cmFja2luZyB7XG4gIGNvbG9yOiAjMDA3YmZmO1xuICBwYWRkaW5nLXRvcDogNnB4O1xuICBjbGVhcjogYm90aDtcbn1cblxuLnNoaXBwaW5nLWlkIHtcbiAgcGFkZGluZy1ib3R0b206IDAgIWltcG9ydGFudDtcbiAgY29sb3I6ICMwRUE4REI7XG59XG5cbi5zaGlwcGluZy1zdGF0dXMtcm93IHtcbiAgY29sb3I6ICMwMTY5MTg7XG4gIGJvcmRlci1ib3R0b206IDEuNXB4IHNvbGlkICNlYWVhZWE7XG4gIGJvcmRlci13aWR0aDogdW5zZXQgIWltcG9ydGFudDtcbn1cblxuLnNoaXBwaW5nLWRldGFpbCB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5kZWZhdWx0LWxvYWRpbmcge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uZGVmYXVsdC1sb2FkaW5nIGkge1xuICBmb250LXNpemU6IDUwcHg7XG4gIGNvbG9yOiAjMDc4MGY4O1xuICBtYXJnaW46IDQwcHggMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/delivery-process/edit/delivery-process.edit.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/edit/delivery-process.edit.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: DeliveryProcessEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryProcessEditComponent", function() { return DeliveryProcessEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



// import { isFunction } from '../../../../object-interface/common.function';
// import { NgxBarcodeModule } from 'ngx-barcode';

// import { MemberService } from '../../../../services/member/member.service';


var DeliveryProcessEditComponent = /** @class */ (function () {
    function DeliveryProcessEditComponent(orderhistoryService, 
    // private route: ActivatedRoute,
    router, 
    // public memberService: MemberService
    modalService) {
        this.orderhistoryService = orderhistoryService;
        this.router = router;
        this.modalService = modalService;
        this.awb_check = false;
        this.infoDetail = [];
        this.openMoreDetail = false;
        this.loading = false;
        this.editThis = false;
        this.done = false;
        this.notesCategory = [
            {
                label: 'Product out of stock',
                value: false
            },
            {
                label: 'Color options are not available',
                value: false
            },
            {
                label: 'Product size is not available',
                value: false
            },
            {
                label: 'Other',
                value: false
            }
        ];
        this.shippingStatusList = [
            {
                label: 'on checking and processing',
                value: 'on_processing',
                id: 'on-process',
                checked: true,
                hovered: false,
                values: 'on_processing'
            },
            {
                label: 'On Delivery process',
                value: 'on_delivery',
                id: 'on-delivery',
                hovered: false,
                values: 'on_delivery'
            },
            {
                label: 'Delivered',
                value: 'delivered',
                id: 'delivered',
                hovered: false,
                values: 'delivered'
            }
        ];
        this.redeliverList = [
            {
                label: 'Return Order Process',
                value: 'redeliver',
                checked: true,
                hovered: false,
            },
            {
                label: 'Return Order Redelivery',
                value: 'redelivery',
                checked: false,
                hovered: false,
            },
            {
                label: 'Delivered',
                value: 'delivered',
                id: 'delivered',
                hovered: false,
                values: 'delivered'
            }
        ];
        this.packaging = false;
        this.transactionStatus = [
            { label: 'PAID', value: 'PAID' },
            { label: 'CANCEL', value: 'CANCEL' },
            { label: 'WAITING FOR CONFIRMATION', value: 'WAITING FOR CONFIRMATION' }
        ];
        this.service_courier = [
            { label: 'Drop', value: 'DROP' },
            { label: 'Pickup', value: 'PICKUP' }
        ];
        this.errorLabel = false;
        this.merchantMode = false;
        this.info = [];
        this.change = false;
        this.isClicked = false;
        this.isClicked2 = false;
        this.courier_list = [];
        this.tempstatus = '';
        this.openNotes = false;
        this.editAwb = false;
        this.othercour = false;
        this.supported = false;
        this.courier = '';
        this.delivery_service = '';
        this.delivery_method = '';
        this.courier_name = '';
        this.changeAWB = false;
        this.delivery_remarks = "";
        this.isChangeLoading = false;
        this.isChangeAWBLoading = false;
        // detailToLowerCase: string = "";
        this.isEverDelivered = false;
        this.completeButtonConfirmation = false;
        this.isCompleteOrder = false;
        this.returnType = "redeliver";
        this.isRedeliver = false;
        this.isChangeRedeliverLoading = false;
        this.redeliverButtonConfirmation = false;
        this.isProcessRedelivery = false;
        this.isChangeProcessRedeliveryLoading = false;
        this.processOrderRedeliveryButtonConfirmation = false;
        this.isRedeliveryCompleted = false;
        this.isReorder = false;
        this.isChangeReorderLoading = false;
        this.reorderButtonConfirmation = false;
    }
    DeliveryProcessEditComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.currentPermission = 'admin';
                        // this.deliveryHistoryDetail = this.detail.values;
                        this.order_id = this.propsDetail.order_id;
                        console.log("this.propsDetail", this.propsDetail);
                        return [4 /*yield*/, this.firstLoad()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, _a, completedReturn, billings, additionalBillings, prop;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        console.warn("detail", this.detail);
                        console.warn("props detail", this.propsDetail);
                        return [4 /*yield*/, this.orderhistoryService.getShippingDetail(this.order_id)];
                    case 1:
                        result = _b.sent();
                        if (result && result.values) {
                            this.deliveryHistoryDetail = result.values;
                        }
                        _a = this;
                        return [4 /*yield*/, this.orderhistoryService.getOrderDetail(this.order_id)];
                    case 2:
                        _a.orderDetail = _b.sent();
                        console.log("orderDetail", this.orderDetail);
                        // await this.updateDetailOrder();
                        // console.log("courier name",this.courier);
                        this.changeAWB = false;
                        if (this.deliveryHistoryDetail.available_courier) {
                            this.deliveryHistoryDetail.available_courier.push({
                                courier: "others",
                                courier_code: "others"
                            });
                        }
                        else {
                            this.courier = 'others';
                        }
                        // if(this.deliveryHistoryDetail.delivery_detail.courier){
                        // 	this.courier = this.deliveryHistoryDetail.delivery_detail.courier;
                        // 	this.delivery_service = this.deliveryHistoryDetail.delivery_detail.delivery_service;
                        // 	this.delivery_method = this.deliveryHistoryDetail.delivery_detail.delivery_method;
                        // 	this.awb_number = this.deliveryHistoryDetail.delivery_detail.awb_number;
                        // 	if(this.deliveryHistoryDetail.delivery_detail.courier == 'others') this.courier_name = this.deliveryHistoryDetail.delivery_detail.courier_name;
                        // }
                        // this.deliveryHistoryDetail.shipping_info.forEach((element) => {
                        // 	if(element.label == "on_delivery") {
                        // 		this.delivery_remarks = element.remarks != null? element.remarks : "";
                        // 	}
                        // 	if(element.label == "return") {
                        // 		this.return_remarks = element.remarks;
                        // 		this.reorder_remarks = element.remarks;
                        // 	}
                        // 	if(element.label == "redelivery") {
                        // 		this.redelivery_remarks = element.remarks;
                        // 	}
                        // 	if(element.label == "delivered") {
                        // 		this.isEverDelivered = true;
                        // 	}
                        // });
                        if (this.deliveryHistoryDetail.status == "RETURN" && this.deliveryHistoryDetail.return_detail) {
                            this.isRedeliver = true;
                            this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
                            this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
                            this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
                            this.return_awb_number = this.deliveryHistoryDetail.return_detail.awb_number;
                            // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
                        }
                        if (this.deliveryHistoryDetail.status == "PROCESSED" && this.deliveryHistoryDetail.redelivery_detail && this.deliveryHistoryDetail.return_detail) {
                            this.isRedeliver = true;
                            this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
                            this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
                            this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
                            this.return_awb_number = this.deliveryHistoryDetail.return_detail.awb_number;
                            // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
                            this.isProcessRedelivery = true;
                            this.redeliverList[1].checked = true;
                            this.redelivery_date = this.convertStringToNgDate(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
                            this.redelivery_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
                            this.redelivery_courier = this.deliveryHistoryDetail.redelivery_detail.courier;
                            this.redelivery_awb_number = this.deliveryHistoryDetail.redelivery_detail.awb_number;
                            // this.redelivery_remarks = this.deliveryHistoryDetail.redelivery_detail.remarks;
                            this.redelivery_method = this.deliveryHistoryDetail.redelivery_detail.delivery_method;
                            this.redelivery_services = this.deliveryHistoryDetail.redelivery_detail.delivery_service;
                        }
                        if (this.deliveryHistoryDetail.status == "COMPLETED" && this.deliveryHistoryDetail.redelivery_detail && this.deliveryHistoryDetail.return_detail) {
                            this.isRedeliver = true;
                            this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
                            this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
                            this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
                            this.return_awb_number = this.deliveryHistoryDetail.return_detail.awb_number;
                            // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
                            this.isProcessRedelivery = true;
                            this.redeliverList[1].checked = true;
                            this.redeliverList[2].checked = true;
                            this.redelivery_date = this.convertStringToNgDate(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
                            this.redelivery_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
                            this.redelivery_courier = this.deliveryHistoryDetail.redelivery_detail.courier;
                            this.redelivery_awb_number = this.deliveryHistoryDetail.redelivery_detail.awb_number;
                            // this.redelivery_remarks = this.deliveryHistoryDetail.redelivery_detail.remarks;
                            this.redelivery_method = this.deliveryHistoryDetail.redelivery_detail.delivery_method;
                            this.redelivery_services = this.deliveryHistoryDetail.redelivery_detail.delivery_service;
                            completedReturn = this.deliveryHistoryDetail.shipping_info[this.deliveryHistoryDetail.shipping_info.length - 1];
                            this.date = this.convertStringToNgDate(completedReturn.created_date);
                            this.time = this.convertStringToNgbTimeStruct(completedReturn.created_date);
                            this.receiver_name = completedReturn.receiver_name;
                            this.delivery_status = completedReturn.delivery_status;
                            this.remark = completedReturn.remarks;
                            this.isRedeliveryCompleted = true;
                        }
                        if (this.deliveryHistoryDetail.status == "CANCEL" && this.deliveryHistoryDetail.return_detail) {
                            this.isReorder = true;
                            this.returnType = "reorder";
                            this.reorder_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
                            this.reorder_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
                            this.reorder_courier = this.deliveryHistoryDetail.return_detail.courier;
                            this.reorder_awb_number = this.deliveryHistoryDetail.return_detail.awb_number;
                            // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
                        }
                        // STEP_3_GET_MEMBER_DETAIL_AND_SETUP_CURRENT_PERMISSION: {
                        // 	/** I need to get Member detail for getting their permission status */
                        // 	if (this.currentPermission == 'admin') {
                        // 		this.setShippingStatusList();
                        // 	} else if (this.currentPermission == 'merchant') {
                        // 		this.setShippingStatus();
                        // 	} else {
                        // 		console.log('error');
                        // 	}
                        // 	if (this.currentPermission == 'admin') {
                        // 		this.deliveryHistoryDetail.order_list.forEach((element, index) => {
                        // 			if (element.shipping_info == null) {
                        // 				// let no_history = "No History"
                        // 				this.info.push('No History');
                        // 			} else {
                        // 				let array_info = element.shipping_info.length - 1;
                        // 				console.log('array', element.shipping_info);
                        // 				this.info.push(element.shipping_info[array_info].title);
                        // 			}
                        // 		});
                        // 	}
                        // }
                        // console.log("test", this.currentPermission)
                        // try {
                        // this.deliveryHistoryDetailToLowerCase = this.deliveryHistoryDetail.status.toLowerCase();
                        if (typeof this.deliveryHistoryDetail.billings != 'undefined') {
                            billings = this.deliveryHistoryDetail.billings;
                            additionalBillings = [];
                            if (billings.discount) {
                                additionalBillings.push({
                                    product_name: billings.discount.product_name,
                                    value: billings.discount.value,
                                    discount: true
                                });
                            }
                            if (this.deliveryHistoryDetail.status == 'CHECKOUT') {
                                // if (this.deliveryHistoryDetail.payment_detail.payment_via == 'manual banking') {
                                if (billings.unique_amount) {
                                    billings.unique_amount;
                                }
                                // }
                            }
                            else {
                                delete billings.unique_amount;
                            }
                            for (prop in billings) {
                                // console.log("PROP", prop);
                                if (prop == 'shipping_fee' || prop == 'unique_amount') {
                                    additionalBillings.push({
                                        product_name: prop,
                                        value: billings[prop],
                                        discount: false
                                    });
                                }
                            }
                            console.log(this.deliveryHistoryDetail.billings);
                            this.deliveryHistoryDetail.additionalBillings = additionalBillings;
                        }
                        else if (typeof this.deliveryHistoryDetail.billings == 'undefined') {
                            // let additionalBillings = [];
                            // let shipping_services
                            // additionalBillings.push(
                            //   {
                            //     product_name: "Shipping Fee",
                            //     value: this.deliveryHistoryDetail.shipping_fee,
                            //     discount: false
                            //   }
                            // )
                            // let shipping_service = {
                            // 	shipping: {
                            // 		service: this.deliveryHistoryDetail.delivery_detail.delivery_service
                            // 	}
                            // };
                            // let sum_total = {
                            // 	sum_total: this.deliveryHistoryDetail.total_price
                            // };
                            // let detail_courier = this.deliveryHistoryDetail.courier
                            // 	? this.deliveryHistoryDetail.courier.courier
                            // 	: this.deliveryHistoryDetail.shipping_service;
                            // let courier = {
                            // 	shipping_services: detail_courier
                            // };
                            // let courier_detail = {
                            //   courier : {
                            //     courier : "",
                            //     awb_number : "",
                            //     courier_code : "",
                            //   }
                            // }
                            ASSIGN_OBJECT_TO_ORDER_HISTORY_DETAIL: {
                                // Object.assign(this.deliveryHistoryDetail, courier);
                                // Object.assign(this.deliveryHistoryDetail, courier_detail)
                                // Object.assign(this.deliveryHistoryDetail, sum_total);
                                // Object.assign(this.deliveryHistoryDetail, shipping_service);
                                // this.deliveryHistoryDetail.additionalBillings = additionalBillings;
                            }
                            // console.log('Detail', this.deliveryHistoryDetail);
                        }
                        // if (this.deliveryHistoryDetail.merchant) {
                        // 	this.merchantMode = true;
                        // }
                        // if (!this.deliveryHistoryDetail.shipping.service) {
                        // 	this.deliveryHistoryDetail.shipping.service = 'no-services';
                        // 	// console.log("Result", this.deliveryHistoryDetail.shipping)
                        // }
                        // if (this.deliveryHistoryDetail.status) {
                        // 	this.deliveryHistoryDetail.status = this.deliveryHistoryDetail.status.trim().toUpperCase();
                        // }
                        // if (this.deliveryHistoryDetail.products) {
                        // 	// console.log("disini", this.deliveryHistoryDetail.products)
                        // 	this.deliveryHistoryDetail.products.forEach((element, index) => {});
                        // }
                        if (this.deliveryHistoryDetail.shipping_info) {
                            console.log('checking again', this.deliveryHistoryDetail.shipping_info);
                            // console.log('this.deliveryHistoryDetail.shipping_info', this.deliveryHistoryDetail.shipping_info)
                            this.shippingStatusList.forEach(function (element, index) {
                                var check = _this.isShippingValueExists(element.value);
                                console.log('check', check);
                                if (check) {
                                    _this.shippingStatusList[index].checked = check[0];
                                    _this.shippingStatusList[index].created_date = check[1];
                                }
                                else {
                                    _this.shippingStatusList[index].checked = false;
                                }
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.isArray = function (obj) {
        return Array.isArray(obj);
    };
    DeliveryProcessEditComponent.prototype.openScrollableContent = function (longContent) {
        this.modalService.open(longContent, { centered: true });
    };
    DeliveryProcessEditComponent.prototype.replaceVarian = function (value) {
        return JSON.stringify(value).replace('{', '').replace('}', '').replace(/[',]+/g, ', ').replace(/['"]+/g, '');
    };
    DeliveryProcessEditComponent.prototype.updateComplete = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payloadData, result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        this.isChangeLoading = true;
                        payloadData = {
                            "order_id": [
                                this.orderDetail.order_id
                            ]
                        };
                        return [4 /*yield*/, this.orderhistoryService.updateStatusOrderCompleted(payloadData, this)];
                    case 1:
                        result = _a.sent();
                        if (!(result.status == "success")) return [3 /*break*/, 3];
                        // this.deliveryHistoryDetail = await this.orderhistoryService.getOrderDetail(payloadData.order_id);
                        return [4 /*yield*/, this.firstLoad()];
                    case 2:
                        // this.deliveryHistoryDetail = await this.orderhistoryService.getOrderDetail(payloadData.order_id);
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        this.isChangeLoading = false;
                        this.completeButtonConfirmation = false;
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.warn("error???", error_1);
                        this.isChangeLoading = false;
                        this.completeButtonConfirmation = false;
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.convertDateToString = function (date, time) {
        return date.year.toString().padStart(2, "0") + '-' + date.month.toString().padStart(2, "0") + '-' + date.day.toString().padStart(2, "0") + " " + time.hour.toString().padStart(2, "0") + ':' + time.minute.toString().padStart(2, "0") + ':' + time.second.toString().padStart(2, "0");
    };
    DeliveryProcessEditComponent.prototype.convertDateOnlyToString = function (date) {
        return date.year.toString().padStart(2, "0") + '-' + date.month.toString().padStart(2, "0") + '-' + date.day.toString().padStart(2, "0");
    };
    DeliveryProcessEditComponent.prototype.convertStringToNgDate = function (value) {
        var _value = value.split(" ")[0].split("-");
        var result = new _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbDate"](0, 0, 0);
        result.year = parseInt(_value[0]);
        result.month = parseInt(_value[1]);
        result.day = parseInt(_value[2]);
        return result;
    };
    DeliveryProcessEditComponent.prototype.convertStringToNgbTimeStruct = function (value) {
        var _value = value.split(" ")[1].split(":");
        var result = { hour: parseInt(_value[0]), minute: parseInt(_value[1]), second: parseInt(_value[1]) };
        return result;
    };
    DeliveryProcessEditComponent.prototype.backToTable = function () {
        this.back[1](this.back[0]);
    };
    DeliveryProcessEditComponent.prototype.backHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.shippingDetail = false;
                // obj.detail = await obj.orderhistoryService.getShippingDetail(obj.order_id);
                // obj.orderDetail = obj.detail.values;
                // console.log("obj.orderDetail",obj.orderDetail);
                obj.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    DeliveryProcessEditComponent.prototype.cancelOrder = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                if (!this.detail.order_id)
                    return [2 /*return*/];
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                    title: 'Confirmation',
                    text: 'Apakah anda yakin ingin membatalkan order : ' + this.detail.order_id,
                    icon: 'error',
                    confirmButtonText: 'Ok',
                    cancelButtonText: "cancel",
                    showCancelButton: true
                }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                    var payload, result_1, e_1;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!result.isConfirmed) return [3 /*break*/, 4];
                                payload = {
                                    order_id: this.detail.order_id
                                };
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 3, , 4]);
                                return [4 /*yield*/, this.orderhistoryService.cancelOrder(payload)];
                            case 2:
                                result_1 = _a.sent();
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                    title: "Cancel",
                                    text: 'Order : ' + this.detail.order_id + ' telah dibatalkan',
                                    icon: 'error',
                                    confirmButtonText: 'Ok',
                                    showCancelButton: false,
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        _this.backToTable();
                                    }
                                });
                                return [3 /*break*/, 4];
                            case 3:
                                e_1 = _a.sent();
                                this.errorLabel = (e_1.message);
                                return [3 /*break*/, 4];
                            case 4: return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    DeliveryProcessEditComponent.prototype.processOrder = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                if (!this.detail.order_id)
                    return [2 /*return*/];
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                    title: 'Confirmation',
                    text: 'Apakah anda yakin ingin memproses order : ' + this.detail.order_id,
                    icon: 'success',
                    confirmButtonText: 'process',
                    cancelButtonText: "cancel",
                    showCancelButton: true
                }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                    var payload, result_2, e_2;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!result.isConfirmed) return [3 /*break*/, 4];
                                payload = {
                                    order_id: this.detail.order_id
                                };
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 3, , 4]);
                                return [4 /*yield*/, this.orderhistoryService.processOrder(payload)];
                            case 2:
                                result_2 = _a.sent();
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                    title: "Success",
                                    text: 'Order : ' + this.detail.order_id + ' telah disetujui',
                                    icon: 'success',
                                    confirmButtonText: 'Ok',
                                    showCancelButton: false,
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        _this.backToTable();
                                    }
                                });
                                return [3 /*break*/, 4];
                            case 3:
                                e_2 = _a.sent();
                                this.errorLabel = (e_2.message);
                                return [3 /*break*/, 4];
                            case 4: return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    DeliveryProcessEditComponent.prototype.calculateSumPrice = function (index) {
        var sum_price = this.deliveryHistoryDetail.products[index].fixed_price * this.deliveryHistoryDetail.products[index].quantity;
        return sum_price;
    };
    // async getCourier() {
    // 	let couriers = await this.orderhistoryService.getCourier();
    // 	this.courier_list = couriers.result;
    // 	console.log('result', this.courier_list);
    // 	this.courier_list.forEach((element, index) => {
    // 		let values = {
    // 			value: element.courier
    // 		};
    // 		Object.assign(this.courier_list[index], values);
    // 	});
    // 	console.log(this.courier_list);
    // }
    // backToTable() {
    // 	if (this.back[0][this.back[1]] && !isFunction(this.back[0])) {
    // 		this.back[0][this.back[1]];
    // 	} else if (isFunction(this.back[0])) {
    // 		this.back[0]();
    // 	} else {
    // 		window.history.back();
    // 	}
    // }
    DeliveryProcessEditComponent.prototype.backToList = function () {
        console.log(true);
        if (this.openMoreDetail) {
            this.openMoreDetail = false;
        }
    };
    // setShippingStatusList() {
    // 	// console.log("here", this.deliveryHistoryDetail.shipping_info);
    // 	this.deliveryHistoryDetail.order_list.forEach((element, index) => {
    // 		if (this.shippingStatusList[index].id == 'on-process') {
    // 			this.shippingStatusList[index].checked = true;
    // 		} else if (this.shippingStatusList[index].id == 'warehouse-packaging') {
    // 			this.shippingStatusList[index].checked = true;
    // 		} else if (this.shippingStatusList[index].id == 'on-delivery') {
    // 			this.shippingStatusList[index].checked = true;
    // 		} else if (this.shippingStatusList[index].id == 'delivered') {
    // 			this.shippingStatusList[index].checked = true;
    // 		}
    // 	});
    // }
    DeliveryProcessEditComponent.prototype.setShippingStatus = function () {
        var _this = this;
        // console.log("here", this.deliveryHistoryDetail.shipping_info);
        this.deliveryHistoryDetail.shipping_info.forEach(function (element, index) {
            if (_this.shippingStatusList[index].id == 'on-process') {
                _this.shippingStatusList[index].checked = true;
            }
            else if (_this.shippingStatusList[index].id == 'warehouse-packaging') {
                _this.shippingStatusList[index].checked = true;
            }
            else if (_this.shippingStatusList[index].id == 'on-delivery') {
                _this.shippingStatusList[index].checked = true;
            }
            else if (_this.shippingStatusList[index].id == 'delivered') {
                _this.shippingStatusList[index].checked = true;
            }
        });
    };
    DeliveryProcessEditComponent.prototype.changeshippingStatusList = function (index) {
        //  
        for (var n = 0; n <= index; n++) {
            if (this.shippingStatusList[n].id == 'on-delivery') {
                if (this.deliveryHistoryDetail.last_shipping_info == 'on_processing' || this.deliveryHistoryDetail.last_shipping_info == 'on_delivery') {
                    if (this.deliveryHistoryDetail.last_shipping_info == 'on_processing')
                        this.changeAWB = true;
                    this.shippingStatusList[n].checked = true;
                }
                // if (this.deliveryHistoryDetail.shipping_services) {
                // 	this.shippingStatusList[n].checked = true;
                // }	
                // if (!this.deliveryHistoryDetail.shipping){
                // 	this.shippingStatusList[n].checked = true;
                // }
            }
            else if (this.shippingStatusList[n].id == 'delivered') {
                if (this.deliveryHistoryDetail.delivery_detail.awb_number) {
                    this.shippingStatusList[n].checked = true;
                }
            }
            //  else if(this.shippingStatusList[n].id == 'on_processing'){
            // 	// else if (this.shippingStatusList[n].id == 'warehouse-packaging') {
            // 	//   let warehouse_packaging = (this.deliveryHistoryDetail.billings != undefined) ? this.deliveryHistoryDetail.shipping_services : this.deliveryHistoryDetail.courier.courier
            // 	//   if (warehouse_packaging) {
            // 	//     this.shippingStatusList[n].checked = true;
            // 	//   }
            // 	// }
            // 	this.shippingStatusList[n].checked = true;
            // }
            // if (this.shippingStatusList[n].id == 'warehouse-packaging') {
            // 	this.packaging = true;
            // 	// this.shippingStatusList[n].checked = true;
            // } else {
            // 	this.packaging = false;
            // }
        }
        for (var n = index + 1; n < this.shippingStatusList.length; n++) {
            this.shippingStatusList[n].checked = false;
        }
    };
    DeliveryProcessEditComponent.prototype.hoverShippingStatusList = function (index, objectHover) {
        for (var n = 0; n <= index; n++) {
            this.shippingStatusList[n].hovered = true;
        }
        for (var n = index + 1; n < this.shippingStatusList.length; n++) {
            this.shippingStatusList[n].hovered = false;
        }
    };
    DeliveryProcessEditComponent.prototype.convertShippingStatusToShippingInfo = function () {
        var newData = [];
        this.shippingStatusList.forEach(function (element, index) {
            if (element.checked) {
                newData.push({ value: element.values });
            }
        });
        return newData;
    };
    DeliveryProcessEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var frm, all_shipping_label, shipping_label, awb, courier, payload, newpayload, temp_payload, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.currentPermission == 'admin' && this.isClicked2)) return [3 /*break*/, 1];
                        this.deliveryHistoryDetail.status = this.tempstatus;
                        this.cancelMerchant();
                        return [3 /*break*/, 10];
                    case 1:
                        _a.trys.push([1, 9, , 10]);
                        this.loading = !this.loading;
                        this.deliveryHistoryDetail.status = this.tempstatus;
                        if (!(this.deliveryHistoryDetail.billings != undefined)) return [3 /*break*/, 3];
                        frm = JSON.parse(JSON.stringify(this.deliveryHistoryDetail));
                        frm.shipping_info = this.convertShippingStatusToShippingInfo();
                        // this.deliveryHistoryDetail.shipping_status = this.valu
                        // if (frm.status != "CANCEL") {
                        delete frm.buyer_detail;
                        delete frm.payment_expire_date;
                        delete frm.products;
                        delete frm.billings;
                        delete frm.user_id;
                        console.log('frm', frm);
                        return [4 /*yield*/, this.orderhistoryService.updateOrderHistoryAllHistory(frm)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 8];
                    case 3:
                        all_shipping_label = this.convertShippingStatusToShippingInfo();
                        shipping_label = all_shipping_label[all_shipping_label.length - 1].value;
                        awb = this.deliveryHistoryDetail.awb_receipt
                            ? this.deliveryHistoryDetail.awb_receipt
                            : this.deliveryHistoryDetail.courier ? this.deliveryHistoryDetail.courier.awb_number : null;
                        courier = this.deliveryHistoryDetail.shipping_services
                            ? this.deliveryHistoryDetail.shipping_services
                            : 'no-services';
                        console.log('courier', this.deliveryHistoryDetail.billings, this.deliveryHistoryDetail.courier);
                        if (this.deliveryHistoryDetail.billings == undefined &&
                            this.deliveryHistoryDetail.courier != undefined &&
                            this.deliveryHistoryDetail.courier.courier_code) {
                            courier = this.deliveryHistoryDetail.courier.courier_code;
                        }
                        payload = {
                            booking_id: this.deliveryHistoryDetail.booking_id,
                            shipping_label: shipping_label,
                            courier_code: courier,
                            awb_number: awb,
                            delivery_method: this.choose_service,
                            status: this.deliveryHistoryDetail.status
                        };
                        console.log('payload', payload);
                        if (!this.othercour) return [3 /*break*/, 5];
                        newpayload = {
                            booking_id: this.deliveryHistoryDetail.booking_id,
                            courier: this.otherCourValue,
                            courier_support: 0,
                            awb_number: awb,
                            shipping_label: shipping_label
                        };
                        return [4 /*yield*/, this.orderhistoryService.updateOrderHistoryAllHistoryMerchant(newpayload)];
                    case 4:
                        _a.sent();
                        return [3 /*break*/, 7];
                    case 5:
                        if (shipping_label == 'on_packaging' && payload.courier_code == 'LO-CR-GR') { // special case for INSTANT courier , add courier_support =0 and courier name 
                            temp_payload = {
                                courier_support: 0,
                                courier: 'GRAB'
                            };
                            Object.assign(payload, temp_payload);
                        }
                        return [4 /*yield*/, this.orderhistoryService.updateOrderHistoryAllHistoryMerchant(payload)];
                    case 6:
                        _a.sent();
                        console.log('choose', this.choose_service);
                        if (this.choose_service == 'PICKUP') {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                title: 'Pemberitahuan Pickup',
                                html: 'Barang anda akan segera di pickup oleh kurir minimal<br/> 2 jam setelah anda melakukan permintaan pickup <br/> <a style="color:red">Baca Term of Service pickup Courier</a>',
                                imageUrl: 'assets/images/alarm.png',
                                imageWidth: 115,
                                imageHeight: 129
                            });
                        }
                        _a.label = 7;
                    case 7:
                        console.log(this.convertShippingStatusToShippingInfo());
                        console.log(this.deliveryHistoryDetail);
                        console.log(payload);
                        _a.label = 8;
                    case 8:
                        // else{
                        //   await this.orderhistoryService.updateOrderHistoryAllHistory(frm.status);
                        // }
                        this.loading = !this.loading;
                        this.done = true;
                        this.change = false;
                        this.editThis = false;
                        alert('Edit submitted');
                        this.firstLoad();
                        return [3 /*break*/, 10];
                    case 9:
                        e_3 = _a.sent();
                        this.errorLabel = e_3.message; //conversion to Error type
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.receipt = function () {
        this.urlReceipt = this.deliveryHistoryDetail.uploaded_receipt;
        window.open(this.urlReceipt);
    };
    DeliveryProcessEditComponent.prototype.openLabel = function () {
        this.router.navigate(['administrator/orderhistoryallhistoryadmin/receipt'], {
            queryParams: { id: this.deliveryHistoryDetail._id }
        });
    };
    DeliveryProcessEditComponent.prototype.openNote = function () {
        this.openNotes = true;
        // this.deliveryHistoryDetail.status = 'CANCEL'
        // if(this.notesCategory[0] == true){
        // 	console.log(this.notesCategory[0].label);
        // } else if(this.notesCategory[1]){
        // 	value: true;
        // } else if(this.notesCategory[2]){
        // 	value: true;
        // }
    };
    DeliveryProcessEditComponent.prototype.onAWBchange = function () {
        console.log(' here', this.deliveryHistoryDetail.courier.awb_number);
        if (this.deliveryHistoryDetail.courier.awb_number) {
            if (this.deliveryHistoryDetail.courier.awb_number.length == 0) {
                this.awb_check = false;
            }
            if (this.deliveryHistoryDetail.courier.awb_number.length > 0) {
                this.awb_check = true;
            }
        }
    };
    DeliveryProcessEditComponent.prototype.openNote2 = function () {
        this.openNotes2 = true;
        // this.deliveryHistoryDetail.status = 'CANCEL'
    };
    DeliveryProcessEditComponent.prototype.clickCategory = function (index) {
        var _this = this;
        this.notesCategory[index].value = true;
        this.notesCategory.forEach(function (element, i) {
            if ((_this.notesCategory[index].value = true)) {
                _this.notesCategory[i].value = false;
                _this.notesCategory[index].value = true;
            }
        });
        this.notes = this.notesCategory[index].label;
        console.log(this.notesCategory);
    };
    DeliveryProcessEditComponent.prototype.noteClose = function () {
        this.openNotes = this.openNotes2 = false;
    };
    DeliveryProcessEditComponent.prototype.hold = function () {
        return __awaiter(this, void 0, void 0, function () {
            var form, holdMerchant, e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        form = {
                            value: this.holdvalue,
                            description: this.notes,
                            remark: 'hold transaction process'
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderhistoryService.holdMerchant(this.detailPage.booking_id, form)];
                    case 2:
                        holdMerchant = _a.sent();
                        this.closeNote2();
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_4 = _a.sent();
                        this.errorLabel = e_4.message; //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.closeNote = function () {
        this.openNotes = this.openNotes2 = false;
        this.firstLoad();
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Cancelled', 'Your order has been Cancelled', 'success');
    };
    DeliveryProcessEditComponent.prototype.closeNote2 = function () {
        this.openNotes = this.openNotes2 = false;
        this.firstLoad();
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('HOLD', 'The order has been hold', 'success');
    };
    DeliveryProcessEditComponent.prototype.cancelOrders = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, cancel;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.currentPermission == 'merchant') {
                            payload = {
                                booking_id: this.deliveryHistoryDetail.booking_id,
                                note: this.notes
                            };
                        }
                        else {
                            payload = {
                                booking_id: this.detailPage.booking_id,
                                note: this.notes
                            };
                            console.log('payload', this.deliveryHistoryDetail);
                        }
                        return [4 /*yield*/, this.orderhistoryService.cancelOrder(payload)];
                    case 1:
                        cancel = _a.sent();
                        this.closeNote();
                        return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.cancelMerchant = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, cancel, e_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        payload = {
                            status: this.tempstatus
                        };
                        console.log('payload', payload.status);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderhistoryService.updateOrderHistoryAllHistoryCancel(this.deliveryHistoryDetail._id, payload)];
                    case 2:
                        cancel = _a.sent();
                        if (cancel) {
                            this.loading = !this.loading;
                            this.done = true;
                            this.change = false;
                            this.editThis = false;
                            alert('Edit submitted');
                            this.firstLoad();
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_5 = _a.sent();
                        this.errorLabel = e_5.message; //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.edit = function () {
        this.editThis = true;
        console.log('edit true');
    };
    DeliveryProcessEditComponent.prototype.doneEdit = function () {
        this.editThis = false;
        console.log('done');
    };
    DeliveryProcessEditComponent.prototype.changestatus = function () {
        this.change = true;
    };
    DeliveryProcessEditComponent.prototype.onpaidclick = function () {
        //   this.deliveryHistoryDetail.status = "PAID";
        this.tempstatus = 'PAID';
        this.isClicked = true;
        this.isClicked2 = false;
        console.log(this.deliveryHistoryDetail);
    };
    DeliveryProcessEditComponent.prototype.oncancelclick = function () {
        // this.deliveryHistoryDetail.status = "CANCEL";
        this.tempstatus = 'CANCEL';
        this.isClicked2 = true;
        this.isClicked = false;
        console.log(this.deliveryHistoryDetail);
    };
    DeliveryProcessEditComponent.prototype.openDetail = function (bookingID, order) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.openMoreDetail = true;
                        return [4 /*yield*/, this.orderhistoryService.getDetailOrderhistory(bookingID)];
                    case 1:
                        result = _a.sent();
                        this.moreOrderDetail = order;
                        console.log('moreorderdetail', this.moreOrderDetail);
                        this.detailPage = result.result[0];
                        console.log('detail', this.detailPage);
                        // this.deliveryHistoryDetail.order_list.forEach((element, index) =>{
                        // })
                        if (this.detailPage.shipping_info) {
                            this.detailPage.shipping_info.forEach(function (element, index) {
                                if (element == null) {
                                    console.log('part 1', element);
                                    _this.infoDetail.push('No Service');
                                }
                                else {
                                    console.log('part 2', element);
                                    var array = _this.detailPage.shipping_info.length - 1;
                                    console.log('test', array);
                                    _this.infoDetail = _this.detailPage.shipping_info[array].title;
                                    _this.infoDetailLabel = _this.detailPage.shipping_info[array].label;
                                    console.log('this2', _this.infoDetail);
                                }
                            });
                        }
                        // if(this.detailPage.shipping_info){
                        // 	this.detailPage.shipping_info.forEach((element, index) => {
                        // 		console.log(element.shipping_info)
                        // 	})
                        // }
                        if (this.openMoreDetail &&
                            this.deliveryHistoryDetail.status == 'PAID' &&
                            this.deliveryHistoryDetail.status != 'COMPLETED' &&
                            (this.infoDetailLabel == 'on_delivery' || this.infoDetailLabel == 'delivered')) {
                            console.log('true bro');
                        }
                        else {
                            console.log('salah cok');
                        }
                        console.log('lkawdj', this.deliveryHistoryDetail.status, this.infoDetailLabel);
                        console.log('booking id', bookingID);
                        return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.other_cour = function () {
        console.log('dipilih', this.deliveryHistoryDetail.shipping_services);
        if (this.deliveryHistoryDetail.shipping_services == 'other') {
            console.log("othernih");
            this.othercour = true;
            this.supported = false;
        }
        else {
            this.supported = true;
            this.othercour = false;
        }
        console.log("courier_list", this.courier_list, "method", this.choose_service);
    };
    DeliveryProcessEditComponent.prototype.listService = function () {
        var _this = this;
        this.courier_list.forEach(function (entry) {
            console.log(entry);
            console.log(entry.delivery_method);
            if (_this.deliveryHistoryDetail.shipping_services == entry.courier_code || _this.deliveryHistoryDetail.shipping_services == entry.courier) {
                _this.method_delivery = entry.delivery_method;
            }
        });
    };
    DeliveryProcessEditComponent.prototype.checkStatus = function () {
        // console.log("Status", this.deliveryHistoryDetail.status)
        this.statusValue = this.deliveryHistoryDetail.status;
        this.statusLabel = this.deliveryHistoryDetail.status;
        if (this.statusValue != 'PAID' && this.statusValue != 'CANCEL') {
            this.transactionStatus.push({ label: this.statusLabel, value: this.statusValue });
        }
    };
    DeliveryProcessEditComponent.prototype.isShippingValueExists = function (value) {
        for (var _i = 0, _a = this.deliveryHistoryDetail.shipping_info; _i < _a.length; _i++) {
            var element = _a[_i];
            console.log('element', element.label);
            if (element.label.trim().toLowerCase() == value.trim().toLowerCase()) {
                return [true, element.created_date];
            }
        }
        return false;
    };
    DeliveryProcessEditComponent.prototype.updateAWB = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.deliveryHistoryDetail.last_shipping_info == 'on_delivery' || this.deliveryHistoryDetail.last_shipping_info == 'on_processing')) return [3 /*break*/, 7];
                        if (!(this.courier &&
                            this.delivery_service &&
                            this.delivery_method)) return [3 /*break*/, 6];
                        payload = {
                            order_id: this.deliveryHistoryDetail.order_id,
                            courier: this.courier,
                            delivery_method: this.delivery_method,
                            delivery_service: this.delivery_service,
                            remarks: this.delivery_remarks
                        };
                        if (this.courier == 'others') {
                            if (!this.courier_name) {
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "mohon masukan nama kurir terlebih dahulu", 'warning');
                                return [2 /*return*/];
                            }
                            // if(!this.awb_number){
                            // 	Swal.fire("Oops", "mohon masukan nomor resi terlebih dahulu", 'warning');
                            // 	return;
                            // }
                            payload.courier_name = this.courier_name;
                            payload.awb_number = this.awb_number ? this.awb_number : '';
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.isChangeAWBLoading = true;
                        return [4 /*yield*/, this.orderhistoryService.updateStatusOrder(payload, 'delivery')];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 3:
                        _a.sent();
                        this.isChangeAWBLoading = false;
                        this.firstLoad();
                        return [3 /*break*/, 5];
                    case 4:
                        e_6 = _a.sent();
                        this.isChangeAWBLoading = false;
                        this.errorLabel = e_6.message; //conversion to Error type	
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Service dan metode pengiriman harus di input terlebih dahulu", 'warning');
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.updateDetailOrder = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, e_7;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.orderhistoryService.getOrderDetail(this.deliveryHistoryDetail.order_id)];
                    case 1:
                        _a.deliveryHistoryDetail = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_7 = _b.sent();
                        this.errorLabel = e_7.message; //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.updateStatusCompleted = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.deliveryHistoryDetail.last_shipping_info == 'on_delivery')) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        payload = {
                            order_id: this.deliveryHistoryDetail.order_id,
                        };
                        return [4 /*yield*/, this.orderhistoryService.updateStatusOrder(payload, 'complete')];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 3:
                        _a.sent();
                        this.firstLoad();
                        return [3 /*break*/, 5];
                    case 4:
                        e_8 = _a.sent();
                        this.errorLabel = e_8.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.processRedeliver = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.isChangeRedeliverLoading = true;
                        payload = {
                            "return_type": "REDELIVER",
                            "order_id": this.detail.order_id,
                            "courier": this.return_courier,
                            "return_date": this.convertDateOnlyToString(this.return_date),
                            "remarks": this.return_remarks,
                            "awb_number": this.return_awb_number,
                        };
                        return [4 /*yield*/, this.orderhistoryService.processRedeliver(payload)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 2:
                        _a.sent();
                        this.isChangeRedeliverLoading = false;
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_9 = _a.sent();
                        this.isChangeRedeliverLoading = false;
                        this.errorLabel = e_9.message; //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.processOrderRedelivery = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.isChangeProcessRedeliveryLoading = true;
                        payload = {
                            "order_id": this.detail.order_id,
                            "courier": this.redelivery_courier,
                            "delivery_method": this.redelivery_method,
                            "delivery_service": this.redelivery_services,
                            "delivery_date": this.convertDateOnlyToString(this.redelivery_date),
                            "remarks": this.redelivery_remarks,
                            "awb_number": this.redelivery_remarks,
                        };
                        return [4 /*yield*/, this.orderhistoryService.processOrderRedelivery(payload)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 2:
                        _a.sent();
                        this.isChangeProcessRedeliveryLoading = false;
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_10 = _a.sent();
                        this.isChangeProcessRedeliveryLoading = false;
                        this.errorLabel = e_10.message; //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.processReorder = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.isChangeReorderLoading = true;
                        payload = {
                            "return_type": "REORDER",
                            "order_id": this.detail.order_id,
                            "courier": this.reorder_courier,
                            "return_date": this.convertDateOnlyToString(this.reorder_date),
                            "remarks": this.reorder_remarks,
                            "awb_number": this.reorder_awb_number,
                        };
                        return [4 /*yield*/, this.orderhistoryService.processRedeliver(payload)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 2:
                        _a.sent();
                        this.isChangeReorderLoading = false;
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_11 = _a.sent();
                        this.isChangeReorderLoading = false;
                        this.errorLabel = e_11.message; //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessEditComponent.prototype.goShippingDetail = function (value) {
        this.shippingDetail = value;
    };
    DeliveryProcessEditComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModal"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DeliveryProcessEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DeliveryProcessEditComponent.prototype, "back", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DeliveryProcessEditComponent.prototype, "propsDetail", void 0);
    DeliveryProcessEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-delivery-process-edit',
            template: __webpack_require__(/*! raw-loader!./delivery-process.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/delivery-process/edit/delivery-process.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./delivery-process.edit.component.scss */ "./src/app/layout/modules/delivery-process/edit/delivery-process.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModal"]])
    ], DeliveryProcessEditComponent);
    return DeliveryProcessEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/delivery-process/shipping-label/shipping-label-routing.module.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/shipping-label/shipping-label-routing.module.ts ***!
  \*************************************************************************************************/
/*! exports provided: ShippingLabelRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingLabelRoutingModule", function() { return ShippingLabelRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shipping_label_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shipping-label.component */ "./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _shipping_label_component__WEBPACK_IMPORTED_MODULE_2__["ShippingLabelComponent"],
    },
];
var ShippingLabelRoutingModule = /** @class */ (function () {
    function ShippingLabelRoutingModule() {
    }
    ShippingLabelRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ShippingLabelRoutingModule);
    return ShippingLabelRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media print {\n  @page {\n    size: A4 portrait;\n    margin: 0.4mm 0.4mm 1.65mm 0.4mm;\n    display: block;\n  }\n  #header {\n    display: none;\n  }\n\n  h5 {\n    display: none;\n  }\n\n  app-page-header {\n    display: none;\n  }\n\n  button {\n    display: none;\n  }\n\n  * {\n    font-size: 13px;\n  }\n\n  div.table-order {\n    -webkit-transform: scale(0.7);\n            transform: scale(0.7);\n    padding-bottom: 20px;\n    width: 20cm;\n    height: 9cm;\n  }\n\n  div._container {\n    padding-top: 0px;\n    -webkit-column-break-inside: avoid;\n       -moz-column-break-inside: avoid;\n            break-inside: avoid;\n  }\n\n  .page-number {\n    display: none;\n  }\n}\n.header-title {\n  color: #ffffff;\n}\n.header-title strong {\n  font-weight: normal !important;\n}\nbody {\n  background-color: #eaeaea;\n  font-family: \"Poppins\";\n  color: #eaeaea;\n  text-align: left;\n  font-size: 11px;\n  margin: 0;\n}\n.mat-raised-button.mat-primary {\n  background-color: #2481fb;\n  float: right;\n  padding: 5px 20px;\n  font-family: \"Poppins\";\n  margin: 10px 40px;\n  border-radius: 5px;\n}\n.mat-raised-button.mat-primary .fa {\n  margin-left: 8px;\n}\n._container {\n  background-color: #fff;\n  margin: 0px 0px 20px 0px;\n  border-radius: 5px;\n  text-align: -webkit-center;\n  padding-top: 20px;\n}\n._container .content .layanan {\n  text-align: center;\n  margin-bottom: 30px;\n}\n._container .content .page-number {\n  text-align: center;\n  padding-top: 25px;\n  padding-bottom: 25px;\n  font-size: 18px;\n}\n._container .content .table-order {\n  height: 100%;\n  width: 100%;\n  font-size: 11px;\n}\n._container .content .table-order .order-id-header {\n  width: 70%;\n  table-layout: fixed;\n  border-top: 0;\n  width: 80%;\n}\n._container .content .table-order .header {\n  table-layout: auto;\n  border-bottom: 0;\n  width: 20cm;\n}\n._container .content .table-order .detail-barang-pengiriman {\n  text-align: left;\n  width: 20cm;\n}\n._container .content .table-order .detail-barang-pengiriman .row-eq-height {\n  display: flex;\n}\n._container .content .table-order .detail-barang-pengiriman .row {\n  margin-left: 0px;\n  height: 100%;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-barang {\n  width: 100%;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-barang .daftar-barang {\n  padding-left: 20px;\n  padding-top: 20px;\n  padding-bottom: 20px;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-barang .catatan {\n  padding-top: 20px;\n  padding-bottom: 20px;\n  font-weight: bold;\n  text-transform: uppercase;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman {\n  width: 50%;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman .jenis-pengiriman {\n  padding-top: 20px;\n  padding-bottom: 20px;\n  padding-left: 20px;\n  font-weight: bold;\n  text-align: left;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman .total-harga {\n  padding-top: 30px;\n  padding-bottom: 30px;\n  text-align: center;\n  font-weight: bold;\n  padding-left: 0px;\n}\n._container .content .table-order .detail-header {\n  width: 20cm;\n  text-align: left;\n  border-top: 0;\n  border-bottom: 0;\n}\n._container .content .table-order .detail-header .row-detail-shipping {\n  width: 100%;\n  flex-wrap: unset !important;\n}\n._container .content .table-order .detail-header .detail-person {\n  border-right: solid #eaeaea 1px;\n  padding: 20px;\n  width: 45%;\n}\n._container .content .table-order .detail-header .detail-person .receiver {\n  border-bottom: #eaeaea solid 1px;\n  padding-bottom: 10px;\n  margin-bottom: 15px;\n  margin-top: 15px;\n}\n._container .content .table-order .detail-header .detail-order {\n  padding: 20px;\n}\n._container .content .table-order .detail-header .detail-order .tlc {\n  font-size: 20px;\n}\n._container .content .table-order .detail-header .row {\n  margin-left: 0px;\n}\n._container .content .table-order .additional-info-header {\n  width: 20cm;\n}\n._container .content .table-order .additional-info-header .row-additional-info {\n  text-align: center;\n  font-weight: bold;\n  font-size: 13px;\n}\n._container .content .table-order .locard-logo {\n  width: 100px;\n}\n._container .content .table-order .locard-logo .logo-avatar {\n  border-radius: 0%;\n  width: 60px;\n}\n._container .content .table-order .courier-logo {\n  text-align: center;\n}\n._container .content .table-order #courier-logo {\n  text-align: center;\n}\n._container .content .table-order #courier-logo .logo-avatar {\n  width: 120px;\n}\n._container .content .table-order .awb {\n  text-align: center;\n}\n._container .content .table-order .order-id {\n  text-align: center;\n}\n._container .content .table-order .order-id p {\n  font-size: 20px;\n  font-weight: bold;\n}\n.btn-print {\n  text-align: center;\n  margin: 15px 0px;\n}\n.page-break:last-child {\n  page-break-after: auto;\n}\ndiv._container:last-child {\n  page-break-after: auto;\n}\ntable {\n  border: 2px solid #eaeaea;\n  border-collapse: collapse;\n}\ntd,\ntr,\nth {\n  padding: 12px;\n  border: 1px solid #eaeaea;\n  width: 185px;\n}\nth {\n  background-color: #f0f0f0;\n}\nh4,\np {\n  margin: 0px;\n}\n#garis {\n  height: 200px;\n  color: #eaeaea;\n}\n#thanks {\n  font-size: 15px;\n  text-align: center;\n  color: #0ea8db;\n  font-weight: bold;\n}\n#header {\n  text-align: center;\n  margin-top: 10px;\n}\n#non {\n  border: 1px solid #eaeaea;\n  font-size: 20px;\n  padding: 4px;\n}\n#shipping {\n  font-size: 20px;\n}\n#order {\n  position: relative;\n  bottom: 10px;\n}\n.send {\n  text-align: center;\n}\n.sticky {\n  position: fixed;\n  top: 0;\n  width: 100%;\n}\n.sticky + #card-transaction {\n  padding-top: 50px;\n}\n.table-no-border {\n  border: none;\n  padding: 0px;\n}\n.td-fit {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGVsaXZlcnktcHJvY2Vzcy9zaGlwcGluZy1sYWJlbC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGRlbGl2ZXJ5LXByb2Nlc3NcXHNoaXBwaW5nLWxhYmVsXFxzaGlwcGluZy1sYWJlbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGVsaXZlcnktcHJvY2Vzcy9zaGlwcGluZy1sYWJlbC9zaGlwcGluZy1sYWJlbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFO0lBQ0UsaUJBQUE7SUFDQSxnQ0FBQTtJQUNBLGNBQUE7RUNBRjtFREdBO0lBQ0UsYUFBQTtFQ0RGOztFRElBO0lBQ0UsYUFBQTtFQ0RGOztFRElBO0lBQ0UsYUFBQTtFQ0RGOztFRElBO0lBQ0UsYUFBQTtFQ0RGOztFRElBO0lBQ0UsZUFBQTtFQ0RGOztFRElBO0lBQ0UsNkJBQUE7WUFBQSxxQkFBQTtJQUNBLG9CQUFBO0lBQ0EsV0FBQTtJQUNBLFdBQUE7RUNERjs7RURJQTtJQUNFLGdCQUFBO0lBRUEsa0NBQUE7T0FBQSwrQkFBQTtZQUFBLG1CQUFBO0VDRkY7O0VES0E7SUFDRSxhQUFBO0VDRkY7QUFDRjtBREtBO0VBQ0UsY0FBQTtBQ0hGO0FESUU7RUFDRSw4QkFBQTtBQ0ZKO0FETUE7RUFDRSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7QUNIRjtBREtBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNGRjtBRElFO0VBQ0UsZ0JBQUE7QUNGSjtBRE1BO0VBQ0Usc0JBQUE7RUFDQSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsMEJBQUE7RUFFQSxpQkFBQTtBQ0pGO0FET0k7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0FDTE47QURRSTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7QUNOTjtBRFNJO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FDUE47QURTTTtFQUNFLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0FDUFI7QURVTTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FDUlI7QURXTTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBQ1RSO0FEV1E7RUFJRSxhQUFBO0FDVFY7QURZUTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtBQ1ZWO0FEYVE7RUFFRSxXQUFBO0FDWlY7QURhVTtFQUNFLGtCQUFBO0VBRUEsaUJBQUE7RUFDQSxvQkFBQTtBQ1paO0FEZVU7RUFDRSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtBQ2JaO0FEaUJRO0VBQ0UsVUFBQTtBQ2ZWO0FEZ0JVO0VBQ0UsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQ2RaO0FEaUJVO0VBQ0UsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQ2ZaO0FEb0JNO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FDbEJSO0FEb0JRO0VBQ0UsV0FBQTtFQUNBLDJCQUFBO0FDbEJWO0FEcUJRO0VBQ0UsK0JBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtBQ25CVjtBRG9CVTtFQUNFLGdDQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDbEJaO0FEc0JRO0VBQ0UsYUFBQTtBQ3BCVjtBRHFCVTtFQUNFLGVBQUE7QUNuQlo7QUR1QlE7RUFDRSxnQkFBQTtBQ3JCVjtBRHlCTTtFQUNFLFdBQUE7QUN2QlI7QUR3QlE7RUFDRSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ3RCVjtBRDBCTTtFQUNFLFlBQUE7QUN4QlI7QUQwQlE7RUFFRSxpQkFBQTtFQUNBLFdBQUE7QUN4QlY7QUQ0Qk07RUFDRSxrQkFBQTtBQzFCUjtBRDZCTTtFQUNFLGtCQUFBO0FDM0JSO0FENkJRO0VBQ0UsWUFBQTtBQzNCVjtBRCtCTTtFQUNFLGtCQUFBO0FDN0JSO0FEZ0NNO0VBQ0Usa0JBQUE7QUM5QlI7QUQrQlE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUM3QlY7QURvQ0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FDakNGO0FEb0NBO0VBQ0Usc0JBQUE7QUNqQ0Y7QURvQ0E7RUFDRSxzQkFBQTtBQ2pDRjtBRG1DQTtFQUNFLHlCQUFBO0VBQ0EseUJBQUE7QUNoQ0Y7QURrQ0E7OztFQUdFLGFBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUMvQkY7QURpQ0E7RUFDRSx5QkFBQTtBQzlCRjtBRGdDQTs7RUFFRSxXQUFBO0FDN0JGO0FEZ0NBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7QUM3QkY7QURnQ0E7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUM3QkY7QURnQ0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FDN0JGO0FEZ0NBO0VBQ0UseUJBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQzdCRjtBRGdDQTtFQUNFLGVBQUE7QUM3QkY7QURnQ0E7RUFDRSxrQkFBQTtFQUNBLFlBQUE7QUM3QkY7QURnQ0E7RUFDRSxrQkFBQTtBQzdCRjtBRGdDQTtFQUNFLGVBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtBQzdCRjtBRGdDQTtFQUNFLGlCQUFBO0FDN0JGO0FEZ0NBO0VBQ0UsWUFBQTtFQUNBLFlBQUE7QUM3QkY7QURnQ0E7RUFDRSwwQkFBQTtFQUFBLHVCQUFBO0VBQUEsa0JBQUE7QUM3QkYiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9kZWxpdmVyeS1wcm9jZXNzL3NoaXBwaW5nLWxhYmVsL3NoaXBwaW5nLWxhYmVsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbkBtZWRpYSBwcmludCB7XHJcbiAgQHBhZ2Uge1xyXG4gICAgc2l6ZTogQTQgcG9ydHJhaXQ7XHJcbiAgICBtYXJnaW46IDAuNG1tIDAuNG1tIDEuNjVtbSAwLjRtbTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gIH1cclxuXHJcbiAgI2hlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgaDUge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcblxyXG4gIGFwcC1wYWdlLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgYnV0dG9uIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAqIHtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICB9XHJcblxyXG4gIGRpdi50YWJsZS1vcmRlciB7XHJcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAgIHdpZHRoOiAyMGNtO1xyXG4gICAgaGVpZ2h0OiA5Y207XHJcbiAgfVxyXG5cclxuICBkaXYuX2NvbnRhaW5lciB7XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgLy8gcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xyXG4gICAgYnJlYWstaW5zaWRlOiBhdm9pZDtcclxuICB9XHJcblxyXG4gIC5wYWdlLW51bWJlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxufVxyXG5cclxuLmhlYWRlci10aXRsZSB7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgc3Ryb25nIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWwgIWltcG9ydGFudDtcclxuICB9XHJcbn1cclxuXHJcbmJvZHkge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYWVhZWE7XHJcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xyXG4gIGNvbG9yOiAjZWFlYWVhO1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgZm9udC1zaXplOiAxMXB4O1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG4ubWF0LXJhaXNlZC1idXR0b24ubWF0LXByaW1hcnkge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyNDgxZmI7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG4gIHBhZGRpbmc6IDVweCAyMHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcclxuICBtYXJnaW46IDEwcHggNDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcblxyXG4gIC5mYSB7XHJcbiAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG4gIH1cclxufVxyXG5cclxuLl9jb250YWluZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgbWFyZ2luOiAwcHggMHB4IDIwcHggMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcclxuICAvLyBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXM7XHJcbiAgcGFkZGluZy10b3A6IDIwcHg7XHJcblxyXG4gIC5jb250ZW50IHtcclxuICAgIC5sYXlhbmFuIHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5wYWdlLW51bWJlciB7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgcGFkZGluZy10b3A6IDI1cHg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAyNXB4O1xyXG4gICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnRhYmxlLW9yZGVyIHtcclxuICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG5cclxuICAgICAgLm9yZGVyLWlkLWhlYWRlciB7XHJcbiAgICAgICAgd2lkdGg6IDcwJTtcclxuICAgICAgICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xyXG4gICAgICAgIGJvcmRlci10b3A6IDA7XHJcbiAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmhlYWRlciB7XHJcbiAgICAgICAgdGFibGUtbGF5b3V0OiBhdXRvO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDA7XHJcbiAgICAgICAgd2lkdGg6IDIwY207XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4ge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgd2lkdGg6IDIwY207XHJcblxyXG4gICAgICAgIC5yb3ctZXEtaGVpZ2h0IHtcclxuICAgICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gICAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLnJvdyB7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmRldGFpbC1iYXJhbmcge1xyXG4gICAgICAgICAgLy8gYm9yZGVyLXJpZ2h0OiBzb2xpZCAjZWFlYWVhIDFweDtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgLmRhZnRhci1iYXJhbmcge1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbiAgICAgICAgICAgIC8vIGJvcmRlci1yaWdodDogc29saWQgI2VhZWFlYSAxcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAuY2F0YXRhbiB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuZGV0YWlsLXBlbmdpcmltYW4ge1xyXG4gICAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICAgIC5qZW5pcy1wZW5naXJpbWFuIHtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIC50b3RhbC1oYXJnYSB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAzMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMzBweDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAuZGV0YWlsLWhlYWRlciB7XHJcbiAgICAgICAgd2lkdGg6IDIwY207XHJcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICBib3JkZXItdG9wOiAwO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDA7XHJcblxyXG4gICAgICAgIC5yb3ctZGV0YWlsLXNoaXBwaW5nIHtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgZmxleC13cmFwOiB1bnNldCAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmRldGFpbC1wZXJzb24ge1xyXG4gICAgICAgICAgYm9yZGVyLXJpZ2h0OiBzb2xpZCAjZWFlYWVhIDFweDtcclxuICAgICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgICB3aWR0aDogNDUlO1xyXG4gICAgICAgICAgLnJlY2VpdmVyIHtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogI2VhZWFlYSBzb2xpZCAxcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmRldGFpbC1vcmRlciB7XHJcbiAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgICAgLnRsYyB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5yb3cge1xyXG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5hZGRpdGlvbmFsLWluZm8taGVhZGVyIHtcclxuICAgICAgICB3aWR0aDogMjBjbTtcclxuICAgICAgICAucm93LWFkZGl0aW9uYWwtaW5mbyB7XHJcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5sb2NhcmQtbG9nbyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgICAgIC8vIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAubG9nby1hdmF0YXIge1xyXG4gICAgICAgICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAwJTtcclxuICAgICAgICAgIHdpZHRoOiA2MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLmNvdXJpZXItbG9nbyB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAjY291cmllci1sb2dvIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgICAgIC5sb2dvLWF2YXRhciB7XHJcbiAgICAgICAgICB3aWR0aDogMTIwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAuYXdiIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5vcmRlci1pZCB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIHAge1xyXG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4uYnRuLXByaW50IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAxNXB4IDBweDtcclxufVxyXG5cclxuLnBhZ2UtYnJlYWs6bGFzdC1jaGlsZCB7XHJcbiAgcGFnZS1icmVhay1hZnRlcjogYXV0bztcclxufVxyXG5cclxuZGl2Ll9jb250YWluZXI6bGFzdC1jaGlsZCB7XHJcbiAgcGFnZS1icmVhay1hZnRlcjogYXV0bztcclxufVxyXG50YWJsZSB7XHJcbiAgYm9yZGVyOiAycHggc29saWQgI2VhZWFlYTtcclxuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG59XHJcbnRkLFxyXG50cixcclxudGgge1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2VhZWFlYTtcclxuICB3aWR0aDogMTg1cHg7XHJcbn1cclxudGgge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbn1cclxuaDQsXHJcbnAge1xyXG4gIG1hcmdpbjogMHB4O1xyXG59XHJcblxyXG4jZ2FyaXMge1xyXG4gIGhlaWdodDogMjAwcHg7XHJcbiAgY29sb3I6ICNlYWVhZWE7XHJcbn1cclxuXHJcbiN0aGFua3Mge1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6ICMwZWE4ZGI7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbiNoZWFkZXIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG4jbm9uIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZWFlYWVhO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBwYWRkaW5nOiA0cHg7XHJcbn1cclxuXHJcbiNzaGlwcGluZyB7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG59XHJcblxyXG4jb3JkZXIge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5zZW5kIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zdGlja3kge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB0b3A6IDA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5zdGlja3kgKyAjY2FyZC10cmFuc2FjdGlvbiB7XHJcbiAgcGFkZGluZy10b3A6IDUwcHg7XHJcbn1cclxuXHJcbi50YWJsZS1uby1ib3JkZXIge1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuXHJcbi50ZC1maXQge1xyXG4gIHdpZHRoOiBmaXQtY29udGVudDtcclxufSIsIkBtZWRpYSBwcmludCB7XG4gIEBwYWdlIHtcbiAgICBzaXplOiBBNCBwb3J0cmFpdDtcbiAgICBtYXJnaW46IDAuNG1tIDAuNG1tIDEuNjVtbSAwLjRtbTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuICAjaGVhZGVyIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgaDUge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICBhcHAtcGFnZS1oZWFkZXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICBidXR0b24ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAqIHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gIH1cblxuICBkaXYudGFibGUtb3JkZXIge1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMC43KTtcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgICB3aWR0aDogMjBjbTtcbiAgICBoZWlnaHQ6IDljbTtcbiAgfVxuXG4gIGRpdi5fY29udGFpbmVyIHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIGJyZWFrLWluc2lkZTogYXZvaWQ7XG4gIH1cblxuICAucGFnZS1udW1iZXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbn1cbi5oZWFkZXItdGl0bGUge1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbi5oZWFkZXItdGl0bGUgc3Ryb25nIHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbCAhaW1wb3J0YW50O1xufVxuXG5ib2R5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VhZWFlYTtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xuICBjb2xvcjogI2VhZWFlYTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBtYXJnaW46IDA7XG59XG5cbi5tYXQtcmFpc2VkLWJ1dHRvbi5tYXQtcHJpbWFyeSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNDgxZmI7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZzogNXB4IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcbiAgbWFyZ2luOiAxMHB4IDQwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cbi5tYXQtcmFpc2VkLWJ1dHRvbi5tYXQtcHJpbWFyeSAuZmEge1xuICBtYXJnaW4tbGVmdDogOHB4O1xufVxuXG4uX2NvbnRhaW5lciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gIG1hcmdpbjogMHB4IDBweCAyMHB4IDBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAubGF5YW5hbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5wYWdlLW51bWJlciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDI1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAyNXB4O1xuICBmb250LXNpemU6IDE4cHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXNpemU6IDExcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLm9yZGVyLWlkLWhlYWRlciB7XG4gIHdpZHRoOiA3MCU7XG4gIHRhYmxlLWxheW91dDogZml4ZWQ7XG4gIGJvcmRlci10b3A6IDA7XG4gIHdpZHRoOiA4MCU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmhlYWRlciB7XG4gIHRhYmxlLWxheW91dDogYXV0bztcbiAgYm9yZGVyLWJvdHRvbTogMDtcbiAgd2lkdGg6IDIwY207XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIHdpZHRoOiAyMGNtO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLnJvdy1lcS1oZWlnaHQge1xuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5yb3cge1xuICBtYXJnaW4tbGVmdDogMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLWJhcmFuZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLmRldGFpbC1iYXJhbmcgLmRhZnRhci1iYXJhbmcge1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5kZXRhaWwtYmFyYW5nIC5jYXRhdGFuIHtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5kZXRhaWwtcGVuZ2lyaW1hbiB7XG4gIHdpZHRoOiA1MCU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLXBlbmdpcmltYW4gLmplbmlzLXBlbmdpcmltYW4ge1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gIHBhZGRpbmctbGVmdDogMjBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLXBlbmdpcmltYW4gLnRvdGFsLWhhcmdhIHtcbiAgcGFkZGluZy10b3A6IDMwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAzMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWhlYWRlciB7XG4gIHdpZHRoOiAyMGNtO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBib3JkZXItdG9wOiAwO1xuICBib3JkZXItYm90dG9tOiAwO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIC5yb3ctZGV0YWlsLXNoaXBwaW5nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZsZXgtd3JhcDogdW5zZXQgIWltcG9ydGFudDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWhlYWRlciAuZGV0YWlsLXBlcnNvbiB7XG4gIGJvcmRlci1yaWdodDogc29saWQgI2VhZWFlYSAxcHg7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIHdpZHRoOiA0NSU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1oZWFkZXIgLmRldGFpbC1wZXJzb24gLnJlY2VpdmVyIHtcbiAgYm9yZGVyLWJvdHRvbTogI2VhZWFlYSBzb2xpZCAxcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIC5kZXRhaWwtb3JkZXIge1xuICBwYWRkaW5nOiAyMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIC5kZXRhaWwtb3JkZXIgLnRsYyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWhlYWRlciAucm93IHtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuYWRkaXRpb25hbC1pbmZvLWhlYWRlciB7XG4gIHdpZHRoOiAyMGNtO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5hZGRpdGlvbmFsLWluZm8taGVhZGVyIC5yb3ctYWRkaXRpb25hbC1pbmZvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5sb2NhcmQtbG9nbyB7XG4gIHdpZHRoOiAxMDBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAubG9jYXJkLWxvZ28gLmxvZ28tYXZhdGFyIHtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlci1yYWRpdXM6IDAlO1xuICB3aWR0aDogNjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuY291cmllci1sb2dvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyICNjb3VyaWVyLWxvZ28ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgI2NvdXJpZXItbG9nbyAubG9nby1hdmF0YXIge1xuICB3aWR0aDogMTIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmF3YiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAub3JkZXItaWQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLm9yZGVyLWlkIHAge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYnRuLXByaW50IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDE1cHggMHB4O1xufVxuXG4ucGFnZS1icmVhazpsYXN0LWNoaWxkIHtcbiAgcGFnZS1icmVhay1hZnRlcjogYXV0bztcbn1cblxuZGl2Ll9jb250YWluZXI6bGFzdC1jaGlsZCB7XG4gIHBhZ2UtYnJlYWstYWZ0ZXI6IGF1dG87XG59XG5cbnRhYmxlIHtcbiAgYm9yZGVyOiAycHggc29saWQgI2VhZWFlYTtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbn1cblxudGQsXG50cixcbnRoIHtcbiAgcGFkZGluZzogMTJweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2VhZWFlYTtcbiAgd2lkdGg6IDE4NXB4O1xufVxuXG50aCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG5cbmg0LFxucCB7XG4gIG1hcmdpbjogMHB4O1xufVxuXG4jZ2FyaXMge1xuICBoZWlnaHQ6IDIwMHB4O1xuICBjb2xvcjogI2VhZWFlYTtcbn1cblxuI3RoYW5rcyB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzBlYThkYjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbiNoZWFkZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbiNub24ge1xuICBib3JkZXI6IDFweCBzb2xpZCAjZWFlYWVhO1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmc6IDRweDtcbn1cblxuI3NoaXBwaW5nIHtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuXG4jb3JkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJvdHRvbTogMTBweDtcbn1cblxuLnNlbmQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5zdGlja3kge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5zdGlja3kgKyAjY2FyZC10cmFuc2FjdGlvbiB7XG4gIHBhZGRpbmctdG9wOiA1MHB4O1xufVxuXG4udGFibGUtbm8tYm9yZGVyIHtcbiAgYm9yZGVyOiBub25lO1xuICBwYWRkaW5nOiAwcHg7XG59XG5cbi50ZC1maXQge1xuICB3aWR0aDogZml0LWNvbnRlbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.ts ***!
  \********************************************************************************************/
/*! exports provided: ShippingLabelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingLabelComponent", function() { return ShippingLabelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ShippingLabelComponent = /** @class */ (function () {
    function ShippingLabelComponent(OrderhistoryService, router, route, memberService) {
        this.OrderhistoryService = OrderhistoryService;
        this.router = router;
        this.route = route;
        this.memberService = memberService;
        this.getData = [];
        this.totalResult = [];
        this.totalWeight = [];
        this.perWeight = [];
        this.endTotalResult = [];
        this.loading = true;
        this.same = false;
        this.merchant_data = {};
        this.courier_image_array = [];
        this.mci_project = false;
        this.sig_project = false;
        this.sig_kontraktual = false;
        this.sig_promotion_2 = false;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__;
        this.programTitle = "";
        // this.getData = route.snapshot.params['invoiceIds']
        //   .split(',');
    }
    ShippingLabelComponent.prototype.ngOnInit = function () {
        this.firstload();
        // this.print();
    };
    ShippingLabelComponent.prototype.firstload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program_1, _this_1;
            return __generator(this, function (_a) {
                this.totalResult = this.detail;
                // this.totalResult = this.detail;
                console.warn("this detail total res", this.totalResult);
                try {
                    program_1 = localStorage.getItem('programName');
                    _this_1 = this;
                    this.contentList.filter(function (element) {
                        if (element.appLabel == program_1) {
                            if (program_1) {
                                _this_1.programTitle = element.title;
                                if (program_1 == "retail_poin_sahabat") {
                                    _this_1.mci_project = false;
                                    _this_1.sig_project = true;
                                }
                                else if (program_1 == "sig_kontraktual") {
                                    _this_1.mci_project = false;
                                    _this_1.sig_kontraktual = true;
                                }
                                else if (program_1 == "sig_promotion_2") {
                                    _this_1.mci_project = false;
                                    _this_1.sig_promotion_2 = true;
                                }
                                else if (program_1 != "dynamix_sbi") {
                                    _this_1.mci_project = true;
                                }
                            }
                        }
                    });
                }
                catch (e) {
                    console.warn(e);
                }
                // try {
                //   let member = await this.memberService.getMemberDetail();
                //   this.merchant_data = member.result
                //   console.log('data' , this.merchant_data)
                // if (member.result){
                //   this.memberCity = member.result.mcity
                //   console.log("member",member.result)
                // }
                // }catch(e){
                // 	this.errorMessage = ((<Error>e).message)
                // }
                // this.route.queryParams.subscribe(async (params) => {
                //   this.getData = params.data
                //   this.service = this.OrderhistoryService
                //   console.log(this.getData)
                // })
                //   for (let element of this.getData) {
                //     try {
                //       let result = await this.OrderhistoryService.getDetailOrderhistory(element);
                //     console.log(result)
                //     this.totalResult.push(result.result[0]) 
                //     }catch(e){
                //       this.errorMessage = ((<Error>e).message)
                //       this.loading = false
                //     }
                //   } 
                // console.log(this.totalResult)
                // this.totalResult.forEach((element,index) => {
                //   console.log("asd",element)
                //   this.current_courier = element.courier.courier
                //   this.courier_list = element.courier_list.supported
                //   this.courier_list.forEach((entry) => {
                //     // console.log(entry)
                //     console.log(entry.courier)
                //     if( entry.courier == this.current_courier){ 
                //       let format = {
                //         'image' : entry.image_url,'courier':this.current_courier, 'index': index, 'same': true
                //       }
                //       this.courier_image_array.push(format)
                //       this.courier_image = entry.image_url
                //       console.log("image", this.courier_image)
                //       this.same = true; 
                //     }
                //   })
                //   if(this.same == false){
                //     let format = {
                //       'image' : '','courier': this.current_courier, 'index': index, 'same': false
                //     }
                //     this.courier_image_array.push(format)
                //   }
                //   element.products.forEach((el,i) => {
                //     this.perWeight = el.weight;
                //   });
                //   this.same = false;
                //   console.log('courier image',this.courier_image_array)
                //   this.totalWeight[index] = this.perWeight
                // });
                // window.print();
                this.loading = false;
                return [2 /*return*/];
            });
        });
    };
    ShippingLabelComponent.prototype.prints = function () {
        window.print();
    };
    ShippingLabelComponent.prototype.backToTable = function () {
        this.back[1](this.back[0]);
    };
    ShippingLabelComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ShippingLabelComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ShippingLabelComponent.prototype, "back", void 0);
    ShippingLabelComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-shipping-label',
            template: __webpack_require__(/*! raw-loader!./shipping-label.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.html"),
            styles: [__webpack_require__(/*! ./shipping-label.component.scss */ "./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"]])
    ], ShippingLabelComponent);
    return ShippingLabelComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/delivery-process/shipping-label/shipping-label.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/shipping-label/shipping-label.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: ShippingLabelModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingLabelModule", function() { return ShippingLabelModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _shipping_label_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shipping-label-routing.module */ "./src/app/layout/modules/delivery-process/shipping-label/shipping-label-routing.module.ts");
/* harmony import */ var _shipping_label_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shipping-label.component */ "./src/app/layout/modules/delivery-process/shipping-label/shipping-label.component.ts");
/* harmony import */ var ngx_barcode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-barcode */ "./node_modules/ngx-barcode/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ShippingLabelModule = /** @class */ (function () {
    function ShippingLabelModule() {
    }
    ShippingLabelModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _shipping_label_component__WEBPACK_IMPORTED_MODULE_3__["ShippingLabelComponent"]
            ],
            imports: [
                ngx_barcode__WEBPACK_IMPORTED_MODULE_4__["NgxBarcodeModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _shipping_label_routing_module__WEBPACK_IMPORTED_MODULE_2__["ShippingLabelRoutingModule"],
            ],
            exports: [
                _shipping_label_component__WEBPACK_IMPORTED_MODULE_3__["ShippingLabelComponent"]
            ]
        })
    ], ShippingLabelModule);
    return ShippingLabelModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/delivery-process/shipping/delivery-process.shipping.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/shipping/delivery-process.shipping.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n  margin-top: 40px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -5px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 16px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail table {\n  width: 100%;\n  font-size: 14px;\n  box-shadow: 5px 5px 10px #999;\n  border-radius: 5px;\n  text-transform: capitalize;\n  overflow: hidden;\n}\n.card-detail table td {\n  border-width: 0px 1px 0px 0px;\n  padding: 15px;\n  vertical-align: top;\n}\n.card-detail table td em {\n  display: block;\n  margin-left: 5px;\n  font-size: 10px;\n  font-style: italic;\n}\n.card-detail table td.qty {\n  text-align: center;\n  height: 100%;\n}\n.card-detail table td.price {\n  text-align: center;\n}\n.card-detail table td.priceBil {\n  color: red;\n  text-align: center;\n}\n.card-detail table td.fee {\n  text-align: center;\n}\n.card-detail table td.priceTot {\n  text-transform: uppercase;\n  font-weight: bolder;\n  text-align: left;\n}\n.card-detail table td.priceTotal {\n  text-align: center;\n  font-weight: bolder;\n}\n.card-detail table th {\n  background-color: #F2F2F2;\n  color: #545454;\n  padding: 15px;\n  font-weight: bold;\n  text-transform: capitalize;\n}\n.card-detail table td.ut {\n  text-align: center;\n}\n.card-detail table.services {\n  border-collapse: collapse;\n  text-align: center;\n}\n.card-detail th.product {\n  text-align: left;\n}\n.card-detail th.qty {\n  text-align: center;\n}\n.card-detail th.ut {\n  text-align: center;\n}\n.card-detail th.price {\n  text-align: center;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content .status-button-container {\n  display: flex;\n  justify-content: space-between;\n}\n.card-detail .card-content .status-button-container .option {\n  background-color: white;\n  width: 45%;\n  color: grey;\n  border: grey 1px solid;\n}\n.card-detail .card-content .status-button-container .paid {\n  background-color: blue;\n  border-color: blue;\n  color: white;\n}\n.card-detail .card-content .status-button-container .cancel {\n  background-color: #dc3545;\n  border-color: #dc3545;\n  color: white;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .orderhistoryallhistory-detail label {\n  margin-top: 10px;\n}\n.card-detail .orderhistoryallhistory-detail .head-one {\n  line-height: 33px;\n  display: flex;\n  align-items: center;\n  margin-bottom: 0;\n}\n.card-detail .orderhistoryallhistory-detail .head-one label.order-id {\n  font-size: 18px;\n  overflow: hidden;\n  width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  margin-bottom: 5px;\n}\n.card-detail .orderhistoryallhistory-detail .head-one label.buyer-name {\n  font-size: 24px;\n  overflow: hidden;\n  width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  color: #0EA8DB;\n}\n.card-detail .orderhistoryallhistory-detail .head-one button {\n  margin: 0;\n  background-color: #e74c3c;\n}\n.card-detail .orderhistoryallhistory-detail .delivery-status-head {\n  color: #016918;\n  border-bottom: 1px solid #333;\n}\n.card-detail .orderhistoryallhistory-detail .delivery-status-head .delivery-status {\n  font-size: 16px;\n}\n.card-detail .orderhistoryallhistory-detail .delivery-status-head .delivery-status label {\n  margin: 0;\n}\n.card-detail .orderhistoryallhistory-detail .process-date-head .process-date {\n  font-size: 16px;\n  margin-bottom: 0;\n}\n.card-detail .orderhistoryallhistory-detail .process-date-head .process-date label {\n  margin-top: 0;\n}\n.card-detail .orderhistoryallhistory-detail .buyer-detail {\n  border-top: 1.5px solid #eaeaea;\n}\n.card-detail .orderhistoryallhistory-detail .buyer-detail label#buyer-detail {\n  font-size: 24px;\n  font-weight: bolder;\n  color: #0EA8DB;\n}\n.card-detail .orderhistoryallhistory-detail .buyer-detail label#buyer-detail .buyer-name {\n  font-size: 12px;\n}\n.card-detail .orderhistoryallhistory-detail .product-group {\n  padding-bottom: 25px;\n}\n.card-detail .orderhistoryallhistory-detail label + div {\n  font-weight: normal;\n}\n.card-detail .orderhistoryallhistory-detail .card-header {\n  background-color: white;\n  border-left: 3px solid blue;\n}\n.card-detail .orderhistoryallhistory-detail .card-header h2 {\n  color: #555;\n  font-size: 20px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list {\n  float: left;\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  overflow: hidden;\n  align-items: end;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list .awb {\n  font-size: 14px;\n  margin-bottom: 10px;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list .input-form {\n  width: 100%;\n}\n.card-detail .orderhistoryallhistory-detail .awb-list > .process {\n  margin-left: 10px;\n  padding: 0px;\n  height: 32px;\n  width: 80px;\n  border-radius: 5px;\n}\n.shipping-date {\n  color: #999;\n  font-size: 10px;\n}\n.shipping-status {\n  float: left;\n  width: 100%;\n  border-left: 1.5px solid #eaeaea;\n  position: relative;\n  text-transform: capitalize;\n  font-weight: bold;\n}\n.shipping-status .radio-button {\n  display: flex;\n  width: 16px;\n  height: 16px;\n  border: 1.5px solid #eaeaea;\n  background: white;\n  border-radius: 16px;\n  float: left;\n  -webkit-transform: translate(-9px, -7px);\n          transform: translate(-9px, -7px);\n  position: absolute;\n  cursor: pointer;\n  justify-content: center;\n  align-items: center;\n}\n.shipping-status .radio-button + label {\n  display: block;\n  float: left;\n  width: 100%;\n  padding: 0px 5px 10px 10px;\n  -webkit-transform: translate(0px, -22px);\n          transform: translate(0px, -22px);\n  cursor: pointer;\n}\n.shipping-status .radio-button.checked-true {\n  border: none;\n  background-color: #2ecc71;\n}\n.shipping-status .radio-button.checked-true:before {\n  content: \"\";\n  background-color: #2ecc71;\n  display: block;\n  border-radius: 9px;\n  width: 8px !important;\n  height: 8px !important;\n}\n.shipping-status .radio-button.hovered-true:before {\n  background-color: #2ecc71;\n  width: 6px;\n  height: 6px;\n  content: \"\";\n  border-radius: 8px;\n  background-color: #2ecc71;\n  display: block;\n}\n.shipping-statuss {\n  float: left;\n  width: 100%;\n  border-left: 1.5px solid #eaeaea;\n  position: relative;\n  text-transform: capitalize;\n  font-weight: bold;\n}\n.shipping-statuss .radio-button {\n  display: flex;\n  width: 16px;\n  height: 16px;\n  border: 1.5px solid #eaeaea;\n  background: white;\n  border-radius: 16px;\n  float: left;\n  -webkit-transform: translate(-9px, -7px);\n          transform: translate(-9px, -7px);\n  position: absolute;\n  cursor: pointer;\n  justify-content: center;\n  align-items: center;\n  pointer-events: none;\n}\n.shipping-statuss .radio-button + label {\n  display: block;\n  float: left;\n  width: 100%;\n  padding: 0px 5px 10px 10px;\n  -webkit-transform: translate(0px, -22px);\n          transform: translate(0px, -22px);\n  pointer-events: none;\n}\n.shipping-statuss .radio-button.checked-true {\n  border: none;\n  background-color: #2ecc71;\n  pointer-events: none;\n}\n.shipping-statuss .radio-button.checked-true:before {\n  content: \"\";\n  display: block;\n  border-radius: 9px;\n  width: 8px !important;\n  height: 8px !important;\n}\n.shipping-statuss .radio-button.hovered-true:before {\n  width: 6px;\n  height: 6px;\n  content: \"\";\n  border-radius: 8px;\n  display: block;\n}\n.shipping-status:last-child {\n  border-left-color: transparent;\n}\n.printLabel {\n  text-align: center;\n}\n#line {\n  border-right: 1px solid black;\n  height: 20px;\n}\n.spacing {\n  font-size: 14px;\n}\ntextarea {\n  width: 100%;\n  height: 100px;\n  border-radius: 5px;\n}\n#cancel-note {\n  margin-top: 20px;\n}\n.payer {\n  text-align: left;\n  font-weight: bold;\n  padding: initial;\n}\n.payer table td {\n  color: #0EA8DB;\n}\n#header {\n  margin-top: 0px;\n}\n.save-active {\n  background-color: blue;\n}\n.save {\n  border: none;\n  color: white;\n  padding: 8px 6px;\n  text-align: center;\n  text-decoration: none;\n  font-size: 14px;\n  margin: -34px -7px;\n  float: right;\n  border-radius: 5px;\n}\n.change {\n  background-color: blue !important;\n}\n.edit {\n  background-color: #4CAF50;\n  border: none;\n  color: white;\n  padding: 8px 18px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  font-size: 14px;\n  margin: -31px 3px;\n  float: right;\n  border-radius: 5px;\n}\n.done {\n  background-color: blue;\n  border: none;\n  color: white;\n  padding: 8px 18px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  font-size: 14px;\n  margin: -31px 3px;\n  float: right;\n  border-radius: 5px;\n}\n.modal.false {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n.full {\n  width: 100% !important;\n}\n.half {\n  width: 50%;\n}\n.half-10 {\n  width: 47%;\n  height: 45px;\n}\n.cancel-hold {\n  background-color: white;\n  border-color: #8854D0;\n  color: #8854D0;\n}\n.confirm-hold {\n  background-color: #8854D0;\n  border-color: #8854D0;\n  color: white;\n}\n.modal.true {\n  display: block;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 100px;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n/* Modal Header */\n.modal-header {\n  padding: 2px 16px;\n  color: white;\n}\n/* Modal Body */\n.modal-body {\n  padding: 2px 16px;\n}\n.modal-body2 {\n  padding: 2px 16px;\n  font-size: 12px;\n}\n.modal-buttons {\n  display: flex;\n  padding: 2px 16px;\n  justify-content: space-between;\n}\n.container-nominal {\n  display: flex;\n  flex-direction: row;\n}\n/* Modal Footer */\n.modal-footer {\n  padding: 2px 16px;\n  color: white;\n}\n.red {\n  color: red;\n}\n/* Modal Content */\n.modal-content {\n  margin-left: 256px;\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  padding: 0;\n  border: 1px solid #888;\n  width: 50%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n.modal-content2 {\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  padding: 0;\n  border: 1px solid #888;\n  width: 35vw;\n  min-width: 300px;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n/* The Close Button */\n.close {\n  color: #000;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n.close:hover,\n.close:focus {\n  color: #000;\n  font-size: 30px;\n  font-weight: bolder;\n  text-decoration: none;\n  cursor: pointer;\n}\n/* Add Animation */\n@-webkit-keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n@keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\ntextarea {\n  padding: 10px;\n  line-height: 1.5;\n  border-radius: 5px;\n  border: 1px solid #ccc;\n  box-shadow: 1px 1px 1px #999;\n}\ncancel-order {\n  margin-left: 5px;\n}\n.logo {\n  position: relative;\n}\n.bg {\n  position: absolute;\n}\n@media only screen and (max-width: 1150px) {\n  .awb-list {\n    float: left;\n    width: 100%;\n    display: flex;\n    flex-direction: column;\n  }\n  .awb-list .awb {\n    font-size: 11px;\n  }\n\n  .awb-list > .process {\n    margin-left: 10px;\n    height: 32px;\n    width: 90px;\n  }\n}\n.example-radio-group {\n  display: flex;\n  flex-direction: column;\n  margin: 15px 0;\n}\n.example-radio-button {\n  margin: 5px;\n}\n.img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 10px 5px;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n  white-space: nowrap;\n  font-size: 12px;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.input-delivery-container {\n  display: flex;\n  flex-direction: column;\n  align-items: flex-start;\n  width: 100%;\n  max-width: 300px;\n}\n.input-delivery-container label {\n  font-size: 12px;\n  margin-top: 0 !important;\n  margin-bottom: 0.2rem;\n}\n.input-delivery-container input {\n  font-size: 14px;\n}\n.button-complete {\n  width: 100%;\n  margin-top: 10px;\n}\n.button-complete .half-button {\n  width: 48%;\n}\n.button-complete .full-button {\n  width: 100%;\n}\nngb-timepicker ::ng-deep .ngb-tp-hour, ngb-timepicker ::ng-deep .ngb-tp-meridian, ngb-timepicker ::ng-deep .ngb-tp-minute, ngb-timepicker ::ng-deep .ngb-tp-second {\n  position: relative !important;\n  margin: 10px 0;\n}\nngb-timepicker ::ng-deep .ngb-tp-hour input, ngb-timepicker ::ng-deep .ngb-tp-meridian input, ngb-timepicker ::ng-deep .ngb-tp-minute input, ngb-timepicker ::ng-deep .ngb-tp-second input {\n  font-size: 14px;\n}\nngb-timepicker ::ng-deep .ngb-tp-hour .btn, ngb-timepicker ::ng-deep .ngb-tp-meridian .btn, ngb-timepicker ::ng-deep .ngb-tp-minute .btn, ngb-timepicker ::ng-deep .ngb-tp-second .btn {\n  position: absolute;\n  right: 5px;\n  padding: 0;\n  line-height: 1;\n  font-size: 10px;\n  color: grey;\n}\nngb-timepicker ::ng-deep .ngb-tp-hour .btn:first-child, ngb-timepicker ::ng-deep .ngb-tp-meridian .btn:first-child, ngb-timepicker ::ng-deep .ngb-tp-minute .btn:first-child, ngb-timepicker ::ng-deep .ngb-tp-second .btn:first-child {\n  top: 2px;\n}\nngb-timepicker ::ng-deep .ngb-tp-hour .btn:nth-of-type(2), ngb-timepicker ::ng-deep .ngb-tp-meridian .btn:nth-of-type(2), ngb-timepicker ::ng-deep .ngb-tp-minute .btn:nth-of-type(2), ngb-timepicker ::ng-deep .ngb-tp-second .btn:nth-of-type(2) {\n  bottom: 2px;\n}\n.delivered-form {\n  overflow: visible !important;\n}\n.redelivery-label {\n  position: relative !important;\n  z-index: 2 !important;\n}\n.return-form {\n  overflow: visible !important;\n  flex-direction: column !important;\n  align-items: start !important;\n}\n.return-form .information-text {\n  position: relative;\n  color: red;\n  font-size: 11px;\n  bottom: 3px;\n  text-transform: initial;\n}\n.return-form label {\n  font-size: 12px;\n  margin-top: 0 !important;\n  margin-bottom: 0.2rem;\n}\n.order-complete-text {\n  font-size: 14px;\n  color: #0EA8DB;\n}\n.cancel-status {\n  margin-left: 20px;\n}\n.outer {\n  min-width: 20vw;\n  flex: 1;\n}\n.progress {\n  height: 100%;\n  padding: 30px 20px;\n}\n.progress > div {\n  display: flex;\n  flex-direction: column;\n  color: #333;\n}\n.progress > div.left {\n  flex: 1;\n  padding-right: 20px;\n  text-align: right;\n}\n.progress > div.left div:last-of-type:after {\n  display: none;\n}\n.progress > div.left div:after {\n  content: \"\";\n  background: rgba(0, 128, 0, 0.1);\n  border-radius: 2px;\n  position: absolute;\n  right: -20px;\n  top: 10px;\n  height: 101%;\n  width: 1px;\n  -webkit-transform: translateX(50%);\n          transform: translateX(50%);\n}\n.progress > div.left .created-date {\n  font-size: 10px;\n  color: #767474;\n}\n.progress > div.right {\n  flex: 2;\n  padding-left: 20px;\n}\n.progress > div.right div:after {\n  content: \"\";\n  background: green;\n  border-radius: 2px;\n  position: absolute;\n  left: -20px;\n  top: 10px;\n  height: 101%;\n  width: 2px;\n  -webkit-transform: translateX(-50%);\n          transform: translateX(-50%);\n  transition: height 0.2s ease;\n}\n.progress > div.right .rowstate-name.prev:after {\n  transition: none;\n}\n.progress > div.right .rowstate-name:before {\n  content: \"\";\n  background: green;\n  padding: 5px;\n  border-radius: 50%;\n  position: absolute;\n  left: -20px;\n  top: 10px;\n  -webkit-transform: translateX(-50%) translateY(-50%);\n          transform: translateX(-50%) translateY(-50%);\n  transition: padding 0.2s ease;\n}\n.progress > div.right .description {\n  line-height: 1.5;\n  font-size: 10px;\n  position: relative;\n  bottom: 20px;\n  color: #767474;\n}\n.progress > div.right .current:after {\n  height: 0%;\n  transition: height 0.2s ease-out;\n}\n.progress > div.right .current .rowstate-name {\n  color: #333;\n  font-weight: bold;\n}\n.progress > div.right .current .rowstate-name:before {\n  background: green;\n  padding: 10px;\n  transition: all 0.2s 0.15s cubic-bezier(0.175, 0.885, 0.32, 2);\n}\n.progress > div.right .current .rowstate-name:after {\n  height: 0%;\n  transition: height 0.2s ease-out;\n}\n.progress > div.right .current .rowstate-name ~ div:before {\n  background: #a8a5a5;\n  padding: 2.5px;\n}\n.progress > div.right .current .rowstate-name ~ div:after {\n  height: 0%;\n  transition: none;\n}\n.progress > div div {\n  flex: 1;\n  position: relative;\n  line-height: 20px;\n  cursor: default;\n  min-height: 42px;\n}\n.button-tracking {\n  color: #007bff;\n  padding-top: 6px;\n  clear: both;\n}\n.shipping-id {\n  padding-bottom: 0 !important;\n  color: #0EA8DB;\n}\n.shipping-status-row {\n  color: #016918;\n  border-bottom: 1.5px solid #eaeaea;\n  border-width: unset !important;\n}\n.shipping-detail {\n  text-align: right;\n}\n.cancel-pickup {\n  margin: 0 20px 20px;\n  height: 35px;\n}\n::ng-deep .modal-dialog {\n  max-width: 1000px !important;\n}\n.default-loading {\n  text-align: center;\n}\n.default-loading i {\n  font-size: 50px;\n  color: #0780f8;\n  margin: 40px 0;\n}\n.destination-card {\n  box-shadow: 5px 5px 10px #999;\n}\n.destination-card .first-column {\n  width: 105px !important;\n}\n.destination-card .second-column {\n  width: 10px;\n}\n.see-order-button button {\n  margin: 12px 0px 7px 0 !important;\n  background-color: #007BFF;\n  color: white;\n  box-shadow: 2px 3px 4px 0 rgba(0, 0, 0, 0.2);\n}\n.see-order-text {\n  font-size: 13px;\n  margin-bottom: 25px;\n  color: black;\n}\n.star-required {\n  color: red;\n}\n.member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\nbutton.btn-download-img {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGVsaXZlcnktcHJvY2Vzcy9zaGlwcGluZy9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGRlbGl2ZXJ5LXByb2Nlc3NcXHNoaXBwaW5nXFxkZWxpdmVyeS1wcm9jZXNzLnNoaXBwaW5nLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9kZWxpdmVyeS1wcm9jZXNzL3NoaXBwaW5nL2RlbGl2ZXJ5LXByb2Nlc3Muc2hpcHBpbmcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNBUjtBRENRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0FaO0FEQ1k7RUFDSSxpQkFBQTtBQ0NoQjtBREVRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTFo7QURFWTtFQUNJLGlCQUFBO0FDQWhCO0FESVE7RUFDSSx5QkFBQTtBQ0ZaO0FES0k7RUFDSSxXQUFBO0VBR0EsZUFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtFQUNBLGdCQUFBO0FDTFI7QURNUTtFQUdDLDZCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FDTlQ7QURPUztFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0xiO0FEVVE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7QUNSWjtBRFVRO0VBQ0ksa0JBQUE7QUNSWjtBRFdRO0VBQ0ksVUFBQTtFQUNBLGtCQUFBO0FDVFo7QURZUTtFQUNJLGtCQUFBO0FDVlo7QURhUTtFQUVJLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ1paO0FEZVE7RUFFSSxrQkFBQTtFQUNBLG1CQUFBO0FDZFo7QURpQlE7RUFDSSx5QkFBQTtFQUNBLGNBQUE7RUFFQSxhQUFBO0VBRUEsaUJBQUE7RUFDQSwwQkFBQTtBQ2pCWjtBRDhCUTtFQUNJLGtCQUFBO0FDNUJaO0FEb0NJO0VBRUkseUJBQUE7RUFDQSxrQkFBQTtBQ25DUjtBRHNDSTtFQUNJLGdCQUFBO0FDcENSO0FEd0NJO0VBQ0ksa0JBQUE7QUN0Q1I7QUR5Q0k7RUFDSSxrQkFBQTtBQ3ZDUjtBRDBDSTtFQUNJLGtCQUFBO0FDeENSO0FEMkNJO0VBQ0ksYUFBQTtBQ3pDUjtBRDBDTztFQUNJLGFBQUE7RUFDQSw4QkFBQTtBQ3hDWDtBRHlDVztFQUNJLHVCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtBQ3ZDZjtBRHlDVztFQUNDLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDdkNaO0FEeUNXO0VBQ0MseUJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7QUN2Q1o7QUQwQ1E7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDeENaO0FENENJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUMxQ1I7QUQ4Q1E7RUFDSSxnQkFBQTtBQzVDWjtBRDhDUTtFQUNJLGlCQUFBO0VBRUEsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUM3Q1o7QUQ4Q1k7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDNUNoQjtBRDhDWTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQzVDaEI7QUQrQ1k7RUFDSSxTQUFBO0VBQ0EseUJBQUE7QUM3Q2hCO0FEa0RRO0VBQ0ksY0FBQTtFQUNBLDZCQUFBO0FDaERaO0FEa0RZO0VBQ0ksZUFBQTtBQ2hEaEI7QURrRGdCO0VBQ0ksU0FBQTtBQ2hEcEI7QUR3RFk7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUN0RGhCO0FEd0RnQjtFQUNJLGFBQUE7QUN0RHBCO0FEMkRRO0VBQ0ksK0JBQUE7QUN6RFo7QUQwRFk7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FDeERoQjtBRHlEZ0I7RUFFSSxlQUFBO0FDeERwQjtBRDZEUTtFQUNJLG9CQUFBO0FDM0RaO0FEOERRO0VBRUksbUJBQUE7QUM3RFo7QURnR1E7RUFDSSx1QkFBQTtFQUNBLDJCQUFBO0FDOUZaO0FEK0ZZO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7QUM3RmhCO0FEaUdRO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDL0ZaO0FEZ0dZO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FDOUZoQjtBRGtHWTtFQUNJLFdBQUE7QUNoR2hCO0FEbUdRO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQ2pHWjtBRHNHQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FDbkdKO0FEcUdBO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxnQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsMEJBQUE7RUFDQSxpQkFBQTtBQ2xHSjtBRG1HSTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDJCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSx3Q0FBQTtVQUFBLGdDQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ2pHUjtBRG9HSTtFQUNJLGNBQUE7RUFDQSxXQUFBO0VBRUEsV0FBQTtFQUNBLDBCQUFBO0VBQ0Esd0NBQUE7VUFBQSxnQ0FBQTtFQUNBLGVBQUE7QUNuR1I7QUR1R0k7RUFFSSxZQUFBO0VBQ0EseUJBQUE7QUN0R1I7QUR5R0k7RUFDSSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLHNCQUFBO0FDdkdSO0FEeUdJO0VBQ0kseUJBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtBQ3ZHUjtBRDJHQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EsZ0NBQUE7RUFDQSxrQkFBQTtFQUNBLDBCQUFBO0VBQ0EsaUJBQUE7QUN4R0o7QUR5R0k7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSwyQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0Esd0NBQUE7VUFBQSxnQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtBQ3ZHUjtBRDBHSTtFQUNJLGNBQUE7RUFDQSxXQUFBO0VBRUEsV0FBQTtFQUNBLDBCQUFBO0VBQ0Esd0NBQUE7VUFBQSxnQ0FBQTtFQUNBLG9CQUFBO0FDekdSO0FEOEdJO0VBRUksWUFBQTtFQUNBLHlCQUFBO0VBQ0Esb0JBQUE7QUM3R1I7QURnSEk7RUFDSSxXQUFBO0VBRUEsY0FBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxzQkFBQTtBQy9HUjtBRGlISTtFQUVJLFVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBRUEsY0FBQTtBQ2pIUjtBRHFIQTtFQUNJLDhCQUFBO0FDbEhKO0FEcUhBO0VBQ0ksa0JBQUE7QUNsSEo7QURxSEM7RUFDSSw2QkFBQTtFQUNBLFlBQUE7QUNsSEw7QUQ2SEE7RUFDSSxlQUFBO0FDMUhKO0FEOEhBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtBQzNISjtBRCtIQTtFQUNJLGdCQUFBO0FDNUhKO0FEK0hBO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDNUhKO0FEK0hTO0VBQ0ksY0FBQTtBQzdIYjtBRGtJQTtFQUNJLGVBQUE7QUMvSEo7QURrSUE7RUFDSSxzQkFBQTtBQy9ISjtBRGlJQTtFQUVJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBRUEsZUFBQTtFQUNBLGtCQUFBO0VBRUEsWUFBQTtFQUNBLGtCQUFBO0FDaklKO0FEbUlBO0VBQ0ksaUNBQUE7QUNoSUo7QURtSUE7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFFQSxZQUFBO0VBQ0Esa0JBQUE7QUNqSUo7QURvSUE7RUFDSSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFFQSxZQUFBO0VBQ0Esa0JBQUE7QUNsSUo7QURxSUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxPQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLDRCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EscUJBQUE7QUNsSUo7QURvSUE7RUFDSSxzQkFBQTtBQ2pJSjtBRG1JQTtFQUNJLFVBQUE7QUNoSUo7QURrSUE7RUFDSSxVQUFBO0VBQ0EsWUFBQTtBQy9ISjtBRGtJQTtFQUNJLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0FDL0hKO0FEaUlBO0VBQ0kseUJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7QUM5SEo7QURpSUE7RUFDSSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxXQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLDRCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EscUJBQUE7QUM5SEo7QURrSUEsaUJBQUE7QUFDQTtFQUNJLGlCQUFBO0VBRUEsWUFBQTtBQ2hJSjtBRG1JQSxlQUFBO0FBQ0E7RUFDSSxpQkFBQTtBQ2hJSjtBRGtJQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQy9ISjtBRGlJQTtFQUNJLGFBQUE7RUFDQSxpQkFBQTtFQUNBLDhCQUFBO0FDOUhKO0FEZ0lBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDN0hKO0FEZ0lBLGlCQUFBO0FBQ0E7RUFDSSxpQkFBQTtFQUVBLFlBQUE7QUM5SEo7QURnSUE7RUFDSSxVQUFBO0FDN0hKO0FEK0hBLGtCQUFBO0FBQ0E7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLHNCQUFBO0VBQ0EsVUFBQTtFQUNBLDRFQUFBO0VBQ0Esa0NBQUE7VUFBQSwwQkFBQTtFQUNBLGdDQUFBO1VBQUEsd0JBQUE7RUFDQSxrQkFBQTtBQzVISjtBRDhIQTtFQUVJLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUVBLGdCQUFBO0VBQ0EsNEVBQUE7RUFDQSxrQ0FBQTtVQUFBLDBCQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLGtCQUFBO0FDN0hKO0FEaUlBLHFCQUFBO0FBQ0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQzlISjtBRGlJQTs7RUFFSSxXQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0FDOUhKO0FEa0lBLGtCQUFBO0FBQ0E7RUFDSTtJQUNJLFdBQUE7SUFDQSxVQUFBO0VDL0hOO0VEa0lFO0lBQ0ksTUFBQTtJQUNBLFVBQUE7RUNoSU47QUFDRjtBRHVIQTtFQUNJO0lBQ0ksV0FBQTtJQUNBLFVBQUE7RUMvSE47RURrSUU7SUFDSSxNQUFBO0lBQ0EsVUFBQTtFQ2hJTjtBQUNGO0FEbUlBO0VBQ0ksYUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FDaklKO0FEb0lBO0VBQ0ksZ0JBQUE7QUNqSUo7QURvSUE7RUFDSSxrQkFBQTtBQ2pJSjtBRG9JQTtFQUNJLGtCQUFBO0FDaklKO0FEb0lBO0VBQ0s7SUFDSSxXQUFBO0lBQ0EsV0FBQTtJQUNBLGFBQUE7SUFDQSxzQkFBQTtFQ2pJUDtFRG1JTztJQUNJLGVBQUE7RUNqSVg7O0VEdUlHO0lBQ0ksaUJBQUE7SUFFQSxZQUFBO0lBQ0EsV0FBQTtFQ3JJUDtBQUNGO0FEeUlBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtBQ3ZJSjtBRDBJRTtFQUNFLFdBQUE7QUN2SUo7QUQySkE7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDeEpKO0FEMkpBO0VBQ0kseUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUN4Sko7QUQySkE7RUFDSSx3QkFBQTtBQ3hKSjtBRDJKQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDeEpKO0FEMEpJO0VBQ0ksZUFBQTtFQUNBLHdCQUFBO0VBQ0EscUJBQUE7QUN4SlI7QUQySkk7RUFDSSxlQUFBO0FDekpSO0FENkpBO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0FDMUpKO0FENEpJO0VBQ0ksVUFBQTtBQzFKUjtBRDZKSTtFQUNJLFdBQUE7QUMzSlI7QURpS1E7RUFDSSw2QkFBQTtFQUNBLGNBQUE7QUM5Slo7QURnS1k7RUFDSSxlQUFBO0FDOUpoQjtBRGlLWTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUMvSmhCO0FEa0tZO0VBQ0ksUUFBQTtBQ2hLaEI7QURtS1k7RUFDSSxXQUFBO0FDaktoQjtBRHVLQTtFQUNJLDRCQUFBO0FDcEtKO0FEdUtBO0VBQ0ksNkJBQUE7RUFDQSxxQkFBQTtBQ3BLSjtBRHVLQTtFQUNJLDRCQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtBQ3BLSjtBRHNLSTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7QUNwS1I7QUR1S0k7RUFDSSxlQUFBO0VBQ0Esd0JBQUE7RUFDQSxxQkFBQTtBQ3JLUjtBRHlLQTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDdEtKO0FEeUtBO0VBQ0ksaUJBQUE7QUN0S0o7QUQwS0E7RUFDSSxlQUFBO0VBQ0EsT0FBQTtBQ3ZLSjtBRDBLRTtFQVdFLFlBQUE7RUFDQSxrQkFBQTtBQ2pMSjtBRG1MSTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLFdBVFc7QUN4S2pCO0FEbUxNO0VBQ0UsT0FBQTtFQUNBLG1CQXBCRTtFQXFCRixpQkFBQTtBQ2pMUjtBRHFMVTtFQUNFLGFBQUE7QUNuTFo7QURzTFU7RUFDRSxXQUFBO0VBQ0EsZ0NBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtDQUFBO1VBQUEsMEJBQUE7QUNwTFo7QUR3TFE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQ3RMWjtBRDBMTTtFQUNFLE9BQUE7RUFDQSxrQkFsREU7QUN0SVY7QUQ0TFk7RUFDSSxXQUFBO0VBQ0EsaUJBcERFO0VBcURGLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxVQTNEQztFQTRERCxtQ0FBQTtVQUFBLDJCQUFBO0VBQ0EsNEJBQUE7QUMxTGhCO0FEZ01jO0VBQ0UsZ0JBQUE7QUM5TGhCO0FEbU1VO0VBQ0UsV0FBQTtFQUNBLGlCQTFFTTtFQTJFTixZQTdFUTtFQThFUixrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxvREFBQTtVQUFBLDRDQUFBO0VBQ0EsNkJBQUE7QUNqTVo7QURxTVE7RUFDSSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDbk1aO0FEdU1ZO0VBQ0ksVUFBQTtFQUNBLGdDQUFBO0FDck1oQjtBRHVNWTtFQUNJLFdBakdJO0VBa0dKLGlCQUFBO0FDck1oQjtBRHVNZ0I7RUFDSSxpQkF2R0Y7RUF3R0UsYUFBQTtFQUNBLDhEQUFBO0FDck1wQjtBRHdNZ0I7RUFDSSxVQUFBO0VBQ0EsZ0NBQUE7QUN0TXBCO0FENE1vQjtFQUNJLG1CQXBIUDtFQXFITyxjQUFBO0FDMU14QjtBRDZNb0I7RUFDSSxVQUFBO0VBQ0EsZ0JBQUE7QUMzTXhCO0FEa05NO0VBQ0UsT0FBQTtFQUVBLGtCQUFBO0VBQ0EsaUJBeklVO0VBMElWLGVBQUE7RUFDQSxnQkFBQTtBQ2pOUjtBRDBORTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUN2Tko7QUQwTkU7RUFDRSw0QkFBQTtFQUNBLGNBQUE7QUN2Tko7QUQwTkU7RUFDRSxjQUFBO0VBQ0Esa0NBQUE7RUFDQSw4QkFBQTtBQ3ZOSjtBRDBORTtFQUNFLGlCQUFBO0FDdk5KO0FEME5FO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0FDdk5KO0FEME5FO0VBQ0UsNEJBQUE7QUN2Tko7QUQwTkU7RUFDRSxrQkFBQTtBQ3ZOSjtBRHdOSTtFQUNFLGVBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ3ROTjtBRDBOQTtFQUNJLDZCQUFBO0FDdk5KO0FEd05JO0VBQ0ksdUJBQUE7QUN0TlI7QUR5Tkk7RUFDSSxXQUFBO0FDdk5SO0FEMk5BO0VBQ0ksaUNBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSw0Q0FBQTtBQ3hOSjtBRDJOQTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUN4Tko7QUQyTkE7RUFDSSxVQUFBO0FDeE5KO0FEMk5BO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ3hOSjtBRDJOQTtFQUNJLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0FDeE5KIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGVsaXZlcnktcHJvY2Vzcy9zaGlwcGluZy9kZWxpdmVyeS1wcm9jZXNzLnNoaXBwaW5nLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDhweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA0MHB4O1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTVweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxNnB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b24udHJ1ZXtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICB0YWJsZXtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAvLyBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDVweCA1cHggMTBweCAjOTk5O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBvdmVyZmxvdzpoaWRkZW47XHJcbiAgICAgICAgdGR7XHJcbiAgICAgICAgLy8gIGJvcmRlci1zdHlsZTpzb2xpZDtcclxuICAgICAgICAvLyAgYm9yZGVyLWNvbG9yOiAjMzMzO1xyXG4gICAgICAgICBib3JkZXItd2lkdGg6IDBweCAxcHggICAwcHggMHB4O1xyXG4gICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG4gICAgICAgICBlbXtcclxuICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuXHJcbiAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRkLnF0eXtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCVcclxuICAgICAgICB9XHJcbiAgICAgICAgdGQucHJpY2V7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGQucHJpY2VCaWx7XHJcbiAgICAgICAgICAgIGNvbG9yOnJlZDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGQuZmVle1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRkLnByaWNlVG90e1xyXG4gICAgICAgICAgICAvLyBib3JkZXItdG9wOiAxLjVweCBzb2xpZCAjZWFlYWVhO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOmxlZnQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0ZC5wcmljZVRvdGFse1xyXG4gICAgICAgICAgICAvLyBib3JkZXItdG9wOiAxLjVweCBzb2xpZCAjZWFlYWVhO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoe1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjJGMkYyO1xyXG4gICAgICAgICAgICBjb2xvcjojNTQ1NDU0O1xyXG4gICAgICAgICAgICAvLyBib3JkZXI6IDFweCBzb2xpZCAjMzMzO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgICAgICAvLyB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICAgICAgLy8gYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHRoLnByb2R1Y3R7XHJcbiAgICAgICAgLy8gICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgLy8gICAgIC8vIGRpc3BsYXk6IGZsZXhcclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIC8vIHRoLnF0eXtcclxuICAgICAgICAvLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIC8vIH1cclxuXHJcbiAgICAgICAgdGQudXR7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHRoLnByaWNle1xyXG4gICAgICAgIC8vICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgfVxyXG5cclxuICAgIHRhYmxlLnNlcnZpY2Vze1xyXG4gICAgICAgIC8vIGJvcmRlci1yaWdodDogM3B4IHNvbGlkIGJsYWNrO1xyXG4gICAgICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIH1cclxuICAgIHRoLnByb2R1Y3Qge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgLy8gZGlzcGxheTogZmxleFxyXG4gICAgfVxyXG5cclxuICAgIHRoLnF0eSB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIHRoLnV0IHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgdGgucHJpY2Uge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAuc3RhdHVzLWJ1dHRvbi1jb250YWluZXIge1xyXG4gICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgICAgIC5vcHRpb24ge1xyXG4gICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgd2lkdGg6IDQ1JTtcclxuICAgICAgICAgICAgICAgY29sb3I6IGdyZXk7XHJcbiAgICAgICAgICAgICAgIGJvcmRlcjogZ3JleSAxcHggc29saWQ7XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgICAgIC5wYWlkIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiBibHVlO1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgICAgIC5jYW5jZWwge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGMzNTQ1O1xyXG4gICAgICAgICAgICBib3JkZXItY29sb3I6ICNkYzM1NDU7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICB9XHJcbiAgICAgICB9XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbHtcclxuICAgICAgIFxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaGVhZC1vbmV7XHJcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAzM3B4O1xyXG4gICAgICAgICAgICAvLyBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzMzMztcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICAgICAgbGFiZWwub3JkZXItaWR7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbGFiZWwuYnV5ZXItbmFtZXtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiMwRUE4REI7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTc0YzNjO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuZGVsaXZlcnktc3RhdHVzLWhlYWQge1xyXG4gICAgICAgICAgICBjb2xvcjogIzAxNjkxODtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMzMzM7XHJcblxyXG4gICAgICAgICAgICAuZGVsaXZlcnktc3RhdHVzIHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuXHJcbiAgICAgICAgICAgICAgICBsYWJlbCB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAucHJvY2Vzcy1kYXRlLWhlYWQge1xyXG4gICAgICAgICAgICBcclxuXHJcbiAgICAgICAgICAgIC5wcm9jZXNzLWRhdGUge1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuXHJcbiAgICAgICAgICAgICAgICBsYWJlbCB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDowO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuYnV5ZXItZGV0YWlse1xyXG4gICAgICAgICAgICBib3JkZXItdG9wOiAxLjVweCBzb2xpZCAjZWFlYWVhO1xyXG4gICAgICAgICAgICBsYWJlbCNidXllci1kZXRhaWx7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDI0cHg7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IzBFQThEQjtcclxuICAgICAgICAgICAgICAgIC5idXllci1uYW1le1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgIFxyXG4gICAgICAgIC5wcm9kdWN0LWdyb3Vwe1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMjVweDtcclxuICAgICAgICAgICAgLy8gYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYWw7XHJcbiAgICAgICAgICAgIC8vIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIC8vIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIC8vIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIC8vIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgLy8gY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIC5pbWFnZXtcclxuICAgICAgICAvLyAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAvLyAgICAgPmRpdntcclxuICAgICAgICAvLyAgICAgICAgIHdpZHRoOiAzMy4zMzMzJTtcclxuICAgICAgICAvLyAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAvLyAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gICAgIC5jYXJkLWhlYWRlcntcclxuICAgICAgICAvLyAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgICAgaDN7XHJcbiAgICAgICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgLy8gICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgLy8gICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIC8vICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAvLyAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIC8vICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgIC8vICAgICAgICAgaW1ne1xyXG4gICAgICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgICAgICAvLyAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIFxyXG4gICAgICAgIC8vICAgICAgICAgfVxyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCBibHVlO1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmF3Yi1saXN0e1xyXG4gICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6ZmxleDtcclxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICAgICAgb3ZlcmZsb3c6aGlkZGVuO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczplbmQ7XHJcbiAgICAgICAgICAgIC5hd2J7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOjEwcHg7XHJcbiAgICAgICAgICAgICAgICAvLyBtYXJnaW4tdG9wOiAxMHB4O1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuaW5wdXQtZm9ybSB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuYXdiLWxpc3QgPiAucHJvY2Vzc3tcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6MTBweDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgICAgICAgIHdpZHRoOjgwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5zaGlwcGluZy1kYXRle1xyXG4gICAgY29sb3I6ICM5OTk7XHJcbiAgICBmb250LXNpemU6IDEwcHg7XHJcbn1cclxuLnNoaXBwaW5nLXN0YXR1c3tcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXItbGVmdDogMS41cHggc29saWQgI2VhZWFlYTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAucmFkaW8tYnV0dG9ue1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgd2lkdGg6IDE2cHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxNnB4O1xyXG4gICAgICAgIGJvcmRlcjogMS41cHggc29saWQgI2VhZWFlYTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxNnB4O1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC05cHgsIC03cHgpO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICB9XHJcbiAgICAucmFkaW8tYnV0dG9uK2xhYmVse1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIC8vIHdpZHRoOiBjYWxjKDcwJSAtIDI3cHgpO1xyXG4gICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogMHB4IDVweCAxMHB4IDEwcHg7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAtMjJweCk7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgLnJhZGlvLWJ1dHRvbi5jaGVja2VkLXRydWV7XHJcbiAgICAgICAgLy8gYm9yZGVyLWNvbG9yOiAjMmVjYzcxO1xyXG4gICAgICAgIGJvcmRlcjpub25lO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IzJlY2M3MTtcclxuICAgIH1cclxuXHJcbiAgICAucmFkaW8tYnV0dG9uLmNoZWNrZWQtdHJ1ZTpiZWZvcmV7XHJcbiAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmVjYzcxO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDlweDtcclxuICAgICAgICB3aWR0aDogOHB4ICAhaW1wb3J0YW50O1xyXG4gICAgICAgIGhlaWdodDogOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgICAucmFkaW8tYnV0dG9uLmhvdmVyZWQtdHJ1ZTpiZWZvcmV7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcclxuICAgICAgICB3aWR0aDogNnB4O1xyXG4gICAgICAgIGhlaWdodDogNnB4O1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyZWNjNzE7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5zaGlwcGluZy1zdGF0dXNze1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1sZWZ0OiAxLjVweCBzb2xpZCAjZWFlYWVhO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIC5yYWRpby1idXR0b257XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICB3aWR0aDogMTZweDtcclxuICAgICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgICAgYm9yZGVyOiAxLjVweCBzb2xpZCAjZWFlYWVhO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE2cHg7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTlweCwgLTdweCk7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG5cclxuICAgIH1cclxuICAgIC5yYWRpby1idXR0b24rbGFiZWx7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgLy8gd2lkdGg6IGNhbGMoNzAlIC0gMjdweCk7XHJcbiAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICBwYWRkaW5nOiAwcHggNXB4IDEwcHggMTBweDtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIC0yMnB4KTtcclxuICAgICAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICAgICAgICAvLyBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICAucmFkaW8tYnV0dG9uLmNoZWNrZWQtdHJ1ZXtcclxuICAgICAgICAvLyBib3JkZXItY29sb3I6ICMyZWNjNzE7XHJcbiAgICAgICAgYm9yZGVyOm5vbmU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjojMmVjYzcxO1xyXG4gICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIC5yYWRpby1idXR0b24uY2hlY2tlZC10cnVlOmJlZm9yZXtcclxuICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICMyZWNjNzE7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOXB4O1xyXG4gICAgICAgIHdpZHRoOiA4cHggICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgaGVpZ2h0OiA4cHggIWltcG9ydGFudDtcclxuICAgIH1cclxuICAgIC5yYWRpby1idXR0b24uaG92ZXJlZC10cnVlOmJlZm9yZXtcclxuICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMmVjYzcxO1xyXG4gICAgICAgIHdpZHRoOiA2cHg7XHJcbiAgICAgICAgaGVpZ2h0OiA2cHg7XHJcbiAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIH1cclxufVxyXG5cclxuLnNoaXBwaW5nLXN0YXR1czpsYXN0LWNoaWxke1xyXG4gICAgYm9yZGVyLWxlZnQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG4ucHJpbnRMYWJlbHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuICNsaW5lIHtcclxuICAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCBibGFjaztcclxuICAgICBoZWlnaHQ6IDIwcHg7XHJcbiB9XHJcblxyXG4vLyAgLmJ0bi1wcmludHtcclxuLy8gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmFhZDJjO1xyXG4vLyAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbi8vICAgICAgY29sb3I6d2hpdGU7XHJcbi8vICAgICAgd2lkdGg6IDUwJTtcclxuLy8gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICB9XHJcblxyXG4uc3BhY2luZ3tcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIC8vIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG59XHJcblxyXG50ZXh0YXJlYXtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgLy8gYm9yZGVyLXRvcDogMXB4IHNvbGlkO1xyXG59XHJcblxyXG4jY2FuY2VsLW5vdGV7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcblxyXG4ucGF5ZXJ7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBwYWRkaW5nOmluaXRpYWw7XHJcbiAgICBcclxuICAgICB0YWJsZXtcclxuICAgICAgICAgdGR7XHJcbiAgICAgICAgICAgICBjb2xvcjojMEVBOERCO1xyXG4gICAgICAgICB9XHJcbiAgICAgfVxyXG59XHJcblxyXG4jaGVhZGVye1xyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG59XHJcblxyXG4uc2F2ZS1hY3RpdmUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcclxufVxyXG4uc2F2ZXtcclxuICAgIFxyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZzogOHB4IDZweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIC8vIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIG1hcmdpbjogLTM0cHggLTdweDtcclxuICAgIC8vIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG4uY2hhbmdle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uZWRpdHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM0Q0FGNTA7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nOiA4cHggMThweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIG1hcmdpbjogLTMxcHggM3B4O1xyXG4gICAgLy8gY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG59XHJcblxyXG4uZG9uZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nOiA4cHggMThweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIG1hcmdpbjogLTMxcHggM3B4O1xyXG4gICAgLy8gY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG59XHJcblxyXG4ubW9kYWwuZmFsc2Uge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICAgIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAvKiBTdGF5IGluIHBsYWNlICovXHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgLyogU2l0IG9uIHRvcCAqL1xyXG4gICAgcGFkZGluZy10b3A6IDEwMHB4O1xyXG4gICAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xyXG4gICAgbGVmdDogMDtcclxuICAgIHRvcDogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLyogRnVsbCB3aWR0aCAqL1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLyogRnVsbCBoZWlnaHQgKi9cclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgLyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigwLCAwLCAwKTtcclxuICAgIC8qIEZhbGxiYWNrIGNvbG9yICovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XHJcbiAgICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXHJcbn1cclxuLmZ1bGx7XHJcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG59XHJcbi5oYWxmIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbn1cclxuLmhhbGYtMTAge1xyXG4gICAgd2lkdGg6IDQ3JTtcclxuICAgIGhlaWdodCA6IDQ1cHg7XHJcbn1cclxuXHJcbi5jYW5jZWwtaG9sZCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1jb2xvcjogIzg4NTREMDtcclxuICAgIGNvbG9yOiAjODg1NEQwO1xyXG59XHJcbi5jb25maXJtLWhvbGR7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjODg1NEQwO1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjODg1NEQwO1xyXG4gICAgY29sb3I6IHdoaXRlXHJcbn1cclxuXHJcbi5tb2RhbC50cnVlIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIC8qIFN0YXkgaW4gcGxhY2UgKi9cclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICAvKiBTaXQgb24gdG9wICovXHJcbiAgICBwYWRkaW5nLXRvcDogMTAwcHg7XHJcbiAgICAvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXHJcbiAgICBsZWZ0OiAxMDBweDtcclxuICAgIHRvcDogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLyogRnVsbCB3aWR0aCAqL1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLyogRnVsbCBoZWlnaHQgKi9cclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgLyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigwLCAwLCAwKTtcclxuICAgIC8qIEZhbGxiYWNrIGNvbG9yICovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XHJcbiAgICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXHJcbn1cclxuXHJcblxyXG4vKiBNb2RhbCBIZWFkZXIgKi9cclxuLm1vZGFsLWhlYWRlciB7XHJcbiAgICBwYWRkaW5nOiAycHggMTZweDtcclxuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICM1Y2I4NWM7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi8qIE1vZGFsIEJvZHkgKi9cclxuLm1vZGFsLWJvZHkge1xyXG4gICAgcGFkZGluZzogMnB4IDE2cHg7XHJcbn1cclxuLm1vZGFsLWJvZHkyIHtcclxuICAgIHBhZGRpbmc6IDJweCAxNnB4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcbi5tb2RhbC1idXR0b25zIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBwYWRkaW5nOiAycHggMTZweDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG4uY29udGFpbmVyLW5vbWluYWwge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbn1cclxuXHJcbi8qIE1vZGFsIEZvb3RlciAqL1xyXG4ubW9kYWwtZm9vdGVyIHtcclxuICAgIHBhZGRpbmc6IDJweCAxNnB4O1xyXG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzVjYjg1YztcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4ucmVke1xyXG4gICAgY29sb3I6cmVkO1xyXG59XHJcbi8qIE1vZGFsIENvbnRlbnQgKi9cclxuLm1vZGFsLWNvbnRlbnQge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDI1NnB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XHJcbiAgICBhbmltYXRpb24tbmFtZTogYW5pbWF0ZXRvcDtcclxuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4ubW9kYWwtY29udGVudDIge1xyXG4gICAgXHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmVmZWZlO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICM4ODg7XHJcbiAgICB3aWR0aDogMzV2dztcclxuICAgXHJcbiAgICBtaW4td2lkdGg6IDMwMHB4O1xyXG4gICAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcclxuICAgIGFuaW1hdGlvbi1uYW1lOiBhbmltYXRldG9wO1xyXG4gICAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjRzO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5cclxuLyogVGhlIENsb3NlIEJ1dHRvbiAqL1xyXG4uY2xvc2Uge1xyXG4gICAgY29sb3I6ICMwMDA7O1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgZm9udC1zaXplOiAyOHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5jbG9zZTpob3ZlcixcclxuLmNsb3NlOmZvY3VzIHtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuXHJcbi8qIEFkZCBBbmltYXRpb24gKi9cclxuQGtleWZyYW1lcyBhbmltYXRldG9wIHtcclxuICAgIGZyb20ge1xyXG4gICAgICAgIHRvcDogLTMwMHB4O1xyXG4gICAgICAgIG9wYWNpdHk6IDBcclxuICAgIH1cclxuXHJcbiAgICB0byB7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIG9wYWNpdHk6IDFcclxuICAgIH1cclxufVxyXG5cclxudGV4dGFyZWF7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IDFweCAjOTk5O1xyXG59XHJcblxyXG5jYW5jZWwtb3JkZXJ7XHJcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG59XHJcblxyXG4ubG9nb3tcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuLmJne1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDExNTBweCl7XHJcbiAgICAgLmF3Yi1saXN0IHtcclxuICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5cclxuICAgICAgICAgLmF3YiB7XHJcbiAgICAgICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgICAgIC8vICBtYXJnaW4tdG9wOiAxMHB4O1xyXG5cclxuICAgICAgICAgfVxyXG4gICAgIH1cclxuXHJcbiAgICAgLmF3Yi1saXN0Pi5wcm9jZXNzIHtcclxuICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgLy8gIHBhZGRpbmc6IDBweDtcclxuICAgICAgICAgaGVpZ2h0OiAzMnB4O1xyXG4gICAgICAgICB3aWR0aDogOTBweDtcclxuICAgICB9XHJcbn1cclxuXHJcblxyXG4uZXhhbXBsZS1yYWRpby1ncm91cCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIG1hcmdpbjogMTVweCAwO1xyXG4gIH1cclxuICBcclxuICAuZXhhbXBsZS1yYWRpby1idXR0b24ge1xyXG4gICAgbWFyZ2luOiA1cHg7XHJcbiAgfVxyXG4gIFxyXG4vLyAucHJvY2Vzc3tcclxuLy8gICAgZGlzcGxheTpmbGV4O1xyXG4vLyAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4vLyB9XHJcblxyXG5cclxuXHJcbi8vIHRhYmxlI2N1c3RvbV90YWJsZXtcclxuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlO1xyXG5cclxuXHJcblxyXG4vLyAgIHRoe1xyXG4vLyAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlO1xyXG4vLyAgIH1cclxuLy8gfVxyXG5cclxuLmltZy11cGxvYWQge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIG1hcmdpbjoxMHB4IDVweDtcclxufVxyXG5cclxuYnV0dG9uLmJ0bl91cGxvYWQsIGJ1dHRvbi5hZGQtbW9yZS1pbWFnZSwgYnV0dG9uLmJ0bl9kZWxldGUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDE1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgYm9yZGVyOjA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogc2FsbW9uO1xyXG59XHJcblxyXG4uaW5wdXQtZGVsaXZlcnktY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDMwMHB4O1xyXG5cclxuICAgIGxhYmVsIHtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMCAhaW1wb3J0YW50O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDAuMnJlbTtcclxuICAgIH1cclxuXHJcbiAgICBpbnB1dCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uYnV0dG9uLWNvbXBsZXRlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuXHJcbiAgICAuaGFsZi1idXR0b24ge1xyXG4gICAgICAgIHdpZHRoOjQ4JTtcclxuICAgIH1cclxuXHJcbiAgICAuZnVsbC1idXR0b24ge1xyXG4gICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICB9XHJcbn1cclxuXHJcbm5nYi10aW1lcGlja2VyIHtcclxuICAgIDo6bmctZGVlcCB7XHJcbiAgICAgICAgLm5nYi10cC1ob3VyLCAubmdiLXRwLW1lcmlkaWFuLCAubmdiLXRwLW1pbnV0ZSwgLm5nYi10cC1zZWNvbmQge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgbWFyZ2luOiAxMHB4IDA7XHJcblxyXG4gICAgICAgICAgICBpbnB1dCB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC5idG4ge1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgcmlnaHQ6IDVweDtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiBncmV5O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAuYnRuOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgICAgICAgIHRvcDogMnB4O1xyXG4gICAgICAgICAgICB9IFxyXG5cclxuICAgICAgICAgICAgLmJ0bjpudGgtb2YtdHlwZSgyKSB7XHJcbiAgICAgICAgICAgICAgICBib3R0b206IDJweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmRlbGl2ZXJlZC1mb3JtIHtcclxuICAgIG92ZXJmbG93OiB2aXNpYmxlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5yZWRlbGl2ZXJ5LWxhYmVsIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xyXG4gICAgei1pbmRleDogMiAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ucmV0dXJuLWZvcm0ge1xyXG4gICAgb3ZlcmZsb3c6IHZpc2libGUgIWltcG9ydGFudDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW4gIWltcG9ydGFudDtcclxuICAgIGFsaWduLWl0ZW1zOiBzdGFydCAhaW1wb3J0YW50O1xyXG5cclxuICAgIC5pbmZvcm1hdGlvbi10ZXh0IHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgY29sb3I6IHJlZDtcclxuICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgYm90dG9tOiAzcHg7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IGluaXRpYWw7XHJcbiAgICB9XHJcblxyXG4gICAgbGFiZWwge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMC4ycmVtO1xyXG4gICAgfVxyXG59XHJcblxyXG4ub3JkZXItY29tcGxldGUtdGV4dCB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBjb2xvcjogIzBFQThEQjtcclxufVxyXG5cclxuLmNhbmNlbC1zdGF0dXN7XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5cclxuXHJcbi5vdXRlciB7XHJcbiAgICBtaW4td2lkdGg6IDIwdnc7XHJcbiAgICBmbGV4OiAxO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3Mge1xyXG4gICAgJGdhcDogMjBweDtcclxuICAgICRsaW5lLWhlaWdodDogMjBweDtcclxuICAgICRidWxsZXQtcmFkaXVzOiA1cHg7XHJcbiAgICAkbGluZS10aGljazogMnB4O1xyXG4gICAgJHN0cmlwLWNvbG9yOiBncmVlbjtcclxuICAgICRuZXh0LWNvbG9yOiAjYThhNWE1O1xyXG4gICAgJGN1cnJlbnQtY29sb3I6ICMzMzM7XHJcbiAgICAkcHJldi1jb2xvcjogIzMzMztcclxuICAgIFxyXG4gICAgLy8gZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAzMHB4IDIwcHg7XHJcbiAgXHJcbiAgICA+IGRpdiB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgIGNvbG9yOiAkcHJldi1jb2xvcjtcclxuICBcclxuICAgICAgJi5sZWZ0IHtcclxuICAgICAgICBmbGV4OjE7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogJGdhcDtcclxuICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgICAgICBcclxuICAgICAgICAvLyBMaW5lXHJcbiAgICAgICAgZGl2IHtcclxuICAgICAgICAgICY6bGFzdC1vZi10eXBlOmFmdGVyIHtcclxuICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IGZhZGVfb3V0KCRzdHJpcC1jb2xvciwgLjkpOyAvL3JnYmEoMCwgMCwgMCwgMC42KTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAtJGdhcDtcclxuICAgICAgICAgICAgdG9wOiAkbGluZS1oZWlnaHQvMjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDElO1xyXG4gICAgICAgICAgICB3aWR0aDogMXB4O1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoNTAlKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jcmVhdGVkLWRhdGUge1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNzY3NDc0O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gIFxyXG4gICAgICAmLnJpZ2h0IHtcclxuICAgICAgICBmbGV4OjI7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAkZ2FwO1xyXG5cclxuICAgICAgICBkaXYge1xyXG4gICAgICAgICAgICAvLyBMaW5lXHJcbiAgICAgICAgICAgICY6YWZ0ZXIge1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICRzdHJpcC1jb2xvcjsgLy9yZ2JhKDAsIDAsIDAsIDAuNik7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICBsZWZ0OiAtJGdhcDtcclxuICAgICAgICAgICAgICAgIHRvcDogJGxpbmUtaGVpZ2h0LzI7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMSU7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogJGxpbmUtdGhpY2s7XHJcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7XHJcbiAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiBoZWlnaHQgMC4ycyBlYXNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIC5yb3dzdGF0ZS1uYW1lIHsgICAgICAgIFxyXG4gICAgICAgICAgJi5wcmV2IHsgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiBub25lO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICBcclxuICAgICAgICAgIC8vIERvdFxyXG4gICAgICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAkc3RyaXAtY29sb3I7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6ICRidWxsZXQtcmFkaXVzO1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbGVmdDogLSRnYXA7XHJcbiAgICAgICAgICAgIHRvcDogJGxpbmUtaGVpZ2h0LzI7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKSB0cmFuc2xhdGVZKC01MCUpO1xyXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBwYWRkaW5nIDAuMnMgZWFzZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5kZXNjcmlwdGlvbntcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgIGJvdHRvbTogMjBweDtcclxuICAgICAgICAgICAgY29sb3I6ICM3Njc0NzQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuY3VycmVudCB7XHJcbiAgICAgICAgICAgICY6YWZ0ZXIge1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAwJTtcclxuICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IGhlaWdodCAuMnMgZWFzZS1vdXQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLnJvd3N0YXRlLW5hbWUge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICRjdXJyZW50LWNvbG9yO1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcblxyXG4gICAgICAgICAgICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICRzdHJpcC1jb2xvcjtcclxuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAkYnVsbGV0LXJhZGl1cyAqIDI7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuMnMgLjE1cyBjdWJpYy1iZXppZXIoMC4xNzUsIDAuODg1LCAwLjMyLCAyKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAmOmFmdGVyIHtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IGhlaWdodCAuMnMgZWFzZS1vdXQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgfiBkaXYge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbG9yOiAkbmV4dC1jb2xvcjtcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAmOmJlZm9yZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICRuZXh0LWNvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAkYnVsbGV0LXJhZGl1cyAqIDAuNTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICY6YWZ0ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiBub25lO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gIFxyXG4gICAgICBkaXYge1xyXG4gICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgLy9vdXRsaW5lOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBsaW5lLWhlaWdodDogJGxpbmUtaGVpZ2h0O1xyXG4gICAgICAgIGN1cnNvcjogZGVmYXVsdDtcclxuICAgICAgICBtaW4taGVpZ2h0OiA0MnB4O1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vICY6bGFzdC1vZi10eXBlIHtcclxuICAgICAgICAvLyAgIGZsZXg6IDA7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuYnV0dG9uLXRyYWNraW5nIHtcclxuICAgIGNvbG9yOiAjMDA3YmZmO1xyXG4gICAgcGFkZGluZy10b3A6IDZweDs7XHJcbiAgICBjbGVhcjogYm90aDtcclxuICB9XHJcblxyXG4gIC5zaGlwcGluZy1pZCB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMCAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6ICMwRUE4REI7XHJcbiAgfVxyXG5cclxuICAuc2hpcHBpbmctc3RhdHVzLXJvdyB7XHJcbiAgICBjb2xvcjogIzAxNjkxODtcclxuICAgIGJvcmRlci1ib3R0b206IDEuNXB4IHNvbGlkICNlYWVhZWE7XHJcbiAgICBib3JkZXItd2lkdGg6IHVuc2V0ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAuc2hpcHBpbmctZGV0YWlsIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gIH1cclxuXHJcbiAgLmNhbmNlbC1waWNrdXAge1xyXG4gICAgbWFyZ2luOiAwIDIwcHggMjBweDtcclxuICAgIGhlaWdodDogMzVweDtcclxuICB9XHJcblxyXG4gIDo6bmctZGVlcCAubW9kYWwtZGlhbG9nIHtcclxuICAgIG1heC13aWR0aDogMTAwMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAuZGVmYXVsdC1sb2FkaW5nIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGkge1xyXG4gICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgIGNvbG9yOiAjMDc4MGY4O1xyXG4gICAgICBtYXJnaW46IDQwcHggMDtcclxuICAgIH1cclxufVxyXG5cclxuLmRlc3RpbmF0aW9uLWNhcmQge1xyXG4gICAgYm94LXNoYWRvdzogNXB4IDVweCAxMHB4ICM5OTk7XHJcbiAgICAuZmlyc3QtY29sdW1uIHtcclxuICAgICAgICB3aWR0aDogMTA1cHggIWltcG9ydGFudDtcclxuICAgIH1cclxuXHJcbiAgICAuc2Vjb25kLWNvbHVtbiB7XHJcbiAgICAgICAgd2lkdGg6IDEwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5zZWUtb3JkZXItYnV0dG9uIGJ1dHRvbntcclxuICAgIG1hcmdpbjogMTJweCAwcHggN3B4IDAgIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDdCRkY7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBib3gtc2hhZG93OiAycHggM3B4IDRweCAwIHJnYigwIDAgMCAvIDIwJSk7XHJcbn1cclxuXHJcbi5zZWUtb3JkZXItdGV4dCB7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4uc3Rhci1yZXF1aXJlZCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4ubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICB3aWR0aDoyMDBweDtcclxuICAgIG1heC1oZWlnaHQ6IDIwMHB4O1xyXG4gICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxufVxyXG5cclxuYnV0dG9uLmJ0bi1kb3dubG9hZC1pbWcge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDE1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgYm9yZGVyOjA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxufSIsIi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWluLWhlaWdodDogNDhweDtcbiAgbWFyZ2luLXRvcDogNDBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxNnB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uLnRydWUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm94LXNoYWRvdzogNXB4IDVweCAxMHB4ICM5OTk7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uY2FyZC1kZXRhaWwgdGFibGUgdGQge1xuICBib3JkZXItd2lkdGg6IDBweCAxcHggMHB4IDBweDtcbiAgcGFkZGluZzogMTVweDtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbn1cbi5jYXJkLWRldGFpbCB0YWJsZSB0ZCBlbSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tbGVmdDogNXB4O1xuICBmb250LXNpemU6IDEwcHg7XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbn1cbi5jYXJkLWRldGFpbCB0YWJsZSB0ZC5xdHkge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5jYXJkLWRldGFpbCB0YWJsZSB0ZC5wcmljZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jYXJkLWRldGFpbCB0YWJsZSB0ZC5wcmljZUJpbCB7XG4gIGNvbG9yOiByZWQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jYXJkLWRldGFpbCB0YWJsZSB0ZC5mZWUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uY2FyZC1kZXRhaWwgdGFibGUgdGQucHJpY2VUb3Qge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRkLnByaWNlVG90YWwge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG4uY2FyZC1kZXRhaWwgdGFibGUgdGgge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjJGMkYyO1xuICBjb2xvcjogIzU0NTQ1NDtcbiAgcGFkZGluZzogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlIHRkLnV0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmNhcmQtZGV0YWlsIHRhYmxlLnNlcnZpY2VzIHtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmNhcmQtZGV0YWlsIHRoLnByb2R1Y3Qge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuLmNhcmQtZGV0YWlsIHRoLnF0eSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jYXJkLWRldGFpbCB0aC51dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jYXJkLWRldGFpbCB0aC5wcmljZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IC5zdGF0dXMtYnV0dG9uLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IC5zdGF0dXMtYnV0dG9uLWNvbnRhaW5lciAub3B0aW9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIHdpZHRoOiA0NSU7XG4gIGNvbG9yOiBncmV5O1xuICBib3JkZXI6IGdyZXkgMXB4IHNvbGlkO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgLnN0YXR1cy1idXR0b24tY29udGFpbmVyIC5wYWlkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcbiAgYm9yZGVyLWNvbG9yOiBibHVlO1xuICBjb2xvcjogd2hpdGU7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCAuc3RhdHVzLWJ1dHRvbi1jb250YWluZXIgLmNhbmNlbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkYzM1NDU7XG4gIGJvcmRlci1jb2xvcjogI2RjMzU0NTtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuaGVhZC1vbmUge1xuICBsaW5lLWhlaWdodDogMzNweDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmhlYWQtb25lIGxhYmVsLm9yZGVyLWlkIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmhlYWQtb25lIGxhYmVsLmJ1eWVyLW5hbWUge1xuICBmb250LXNpemU6IDI0cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgY29sb3I6ICMwRUE4REI7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5oZWFkLW9uZSBidXR0b24ge1xuICBtYXJnaW46IDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNzRjM2M7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5kZWxpdmVyeS1zdGF0dXMtaGVhZCB7XG4gIGNvbG9yOiAjMDE2OTE4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzMzMztcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmRlbGl2ZXJ5LXN0YXR1cy1oZWFkIC5kZWxpdmVyeS1zdGF0dXMge1xuICBmb250LXNpemU6IDE2cHg7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5kZWxpdmVyeS1zdGF0dXMtaGVhZCAuZGVsaXZlcnktc3RhdHVzIGxhYmVsIHtcbiAgbWFyZ2luOiAwO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAucHJvY2Vzcy1kYXRlLWhlYWQgLnByb2Nlc3MtZGF0ZSB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLnByb2Nlc3MtZGF0ZS1oZWFkIC5wcm9jZXNzLWRhdGUgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAwO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuYnV5ZXItZGV0YWlsIHtcbiAgYm9yZGVyLXRvcDogMS41cHggc29saWQgI2VhZWFlYTtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmJ1eWVyLWRldGFpbCBsYWJlbCNidXllci1kZXRhaWwge1xuICBmb250LXNpemU6IDI0cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIGNvbG9yOiAjMEVBOERCO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuYnV5ZXItZGV0YWlsIGxhYmVsI2J1eWVyLWRldGFpbCAuYnV5ZXItbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLnByb2R1Y3QtZ3JvdXAge1xuICBwYWRkaW5nLWJvdHRvbTogMjVweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCBibHVlO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogIzU1NTtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmF3Yi1saXN0IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBhbGlnbi1pdGVtczogZW5kO1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LWRldGFpbCAuYXdiLWxpc3QgLmF3YiB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgLmF3Yi1saXN0IC5pbnB1dC1mb3JtIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5hd2ItbGlzdCA+IC5wcm9jZXNzIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmc6IDBweDtcbiAgaGVpZ2h0OiAzMnB4O1xuICB3aWR0aDogODBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4uc2hpcHBpbmctZGF0ZSB7XG4gIGNvbG9yOiAjOTk5O1xuICBmb250LXNpemU6IDEwcHg7XG59XG5cbi5zaGlwcGluZy1zdGF0dXMge1xuICBmbG9hdDogbGVmdDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1sZWZ0OiAxLjVweCBzb2xpZCAjZWFlYWVhO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5zaGlwcGluZy1zdGF0dXMgLnJhZGlvLWJ1dHRvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxNnB4O1xuICBoZWlnaHQ6IDE2cHg7XG4gIGJvcmRlcjogMS41cHggc29saWQgI2VhZWFlYTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDE2cHg7XG4gIGZsb2F0OiBsZWZ0O1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtOXB4LCAtN3B4KTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnNoaXBwaW5nLXN0YXR1cyAucmFkaW8tYnV0dG9uICsgbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwcHggNXB4IDEwcHggMTBweDtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCAtMjJweCk7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5zaGlwcGluZy1zdGF0dXMgLnJhZGlvLWJ1dHRvbi5jaGVja2VkLXRydWUge1xuICBib3JkZXI6IG5vbmU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyZWNjNzE7XG59XG4uc2hpcHBpbmctc3RhdHVzIC5yYWRpby1idXR0b24uY2hlY2tlZC10cnVlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyZWNjNzE7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBib3JkZXItcmFkaXVzOiA5cHg7XG4gIHdpZHRoOiA4cHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiA4cHggIWltcG9ydGFudDtcbn1cbi5zaGlwcGluZy1zdGF0dXMgLnJhZGlvLWJ1dHRvbi5ob3ZlcmVkLXRydWU6YmVmb3JlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcbiAgd2lkdGg6IDZweDtcbiAgaGVpZ2h0OiA2cHg7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJlY2M3MTtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5zaGlwcGluZy1zdGF0dXNzIHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItbGVmdDogMS41cHggc29saWQgI2VhZWFlYTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uc2hpcHBpbmctc3RhdHVzcyAucmFkaW8tYnV0dG9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDE2cHg7XG4gIGhlaWdodDogMTZweDtcbiAgYm9yZGVyOiAxLjVweCBzb2xpZCAjZWFlYWVhO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTZweDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC05cHgsIC03cHgpO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuLnNoaXBwaW5nLXN0YXR1c3MgLnJhZGlvLWJ1dHRvbiArIGxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZsb2F0OiBsZWZ0O1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMHB4IDVweCAxMHB4IDEwcHg7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgLTIycHgpO1xuICBwb2ludGVyLWV2ZW50czogbm9uZTtcbn1cbi5zaGlwcGluZy1zdGF0dXNzIC5yYWRpby1idXR0b24uY2hlY2tlZC10cnVlIHtcbiAgYm9yZGVyOiBub25lO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmVjYzcxO1xuICBwb2ludGVyLWV2ZW50czogbm9uZTtcbn1cbi5zaGlwcGluZy1zdGF0dXNzIC5yYWRpby1idXR0b24uY2hlY2tlZC10cnVlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBib3JkZXItcmFkaXVzOiA5cHg7XG4gIHdpZHRoOiA4cHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiA4cHggIWltcG9ydGFudDtcbn1cbi5zaGlwcGluZy1zdGF0dXNzIC5yYWRpby1idXR0b24uaG92ZXJlZC10cnVlOmJlZm9yZSB7XG4gIHdpZHRoOiA2cHg7XG4gIGhlaWdodDogNnB4O1xuICBjb250ZW50OiBcIlwiO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4uc2hpcHBpbmctc3RhdHVzOmxhc3QtY2hpbGQge1xuICBib3JkZXItbGVmdC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5cbi5wcmludExhYmVsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4jbGluZSB7XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIGJsYWNrO1xuICBoZWlnaHQ6IDIwcHg7XG59XG5cbi5zcGFjaW5nIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG50ZXh0YXJlYSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbiNjYW5jZWwtbm90ZSB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbi5wYXllciB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nOiBpbml0aWFsO1xufVxuLnBheWVyIHRhYmxlIHRkIHtcbiAgY29sb3I6ICMwRUE4REI7XG59XG5cbiNoZWFkZXIge1xuICBtYXJnaW4tdG9wOiAwcHg7XG59XG5cbi5zYXZlLWFjdGl2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XG59XG5cbi5zYXZlIHtcbiAgYm9yZGVyOiBub25lO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDhweCA2cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbjogLTM0cHggLTdweDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5jaGFuZ2Uge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlICFpbXBvcnRhbnQ7XG59XG5cbi5lZGl0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzRDQUY1MDtcbiAgYm9yZGVyOiBub25lO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDhweCAxOHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbjogLTMxcHggM3B4O1xuICBmbG9hdDogcmlnaHQ7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLmRvbmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xuICBib3JkZXI6IG5vbmU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogOHB4IDE4cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luOiAtMzFweCAzcHg7XG4gIGZsb2F0OiByaWdodDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4ubW9kYWwuZmFsc2Uge1xuICBkaXNwbGF5OiBub25lO1xuICAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgei1pbmRleDogMTtcbiAgLyogU2l0IG9uIHRvcCAqL1xuICBwYWRkaW5nLXRvcDogMTAwcHg7XG4gIC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgLyogRnVsbCB3aWR0aCAqL1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIEZ1bGwgaGVpZ2h0ICovXG4gIG92ZXJmbG93OiBhdXRvO1xuICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG59XG5cbi5mdWxsIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbn1cblxuLmhhbGYge1xuICB3aWR0aDogNTAlO1xufVxuXG4uaGFsZi0xMCB7XG4gIHdpZHRoOiA0NyU7XG4gIGhlaWdodDogNDVweDtcbn1cblxuLmNhbmNlbC1ob2xkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1jb2xvcjogIzg4NTREMDtcbiAgY29sb3I6ICM4ODU0RDA7XG59XG5cbi5jb25maXJtLWhvbGQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjODg1NEQwO1xuICBib3JkZXItY29sb3I6ICM4ODU0RDA7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLm1vZGFsLnRydWUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cbiAgcG9zaXRpb246IGZpeGVkO1xuICAvKiBTdGF5IGluIHBsYWNlICovXG4gIHotaW5kZXg6IDE7XG4gIC8qIFNpdCBvbiB0b3AgKi9cbiAgcGFkZGluZy10b3A6IDEwMHB4O1xuICAvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXG4gIGxlZnQ6IDEwMHB4O1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICAvKiBGdWxsIHdpZHRoICovXG4gIGhlaWdodDogMTAwJTtcbiAgLyogRnVsbCBoZWlnaHQgKi9cbiAgb3ZlcmZsb3c6IGF1dG87XG4gIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICAvKiBGYWxsYmFjayBjb2xvciAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cbn1cblxuLyogTW9kYWwgSGVhZGVyICovXG4ubW9kYWwtaGVhZGVyIHtcbiAgcGFkZGluZzogMnB4IDE2cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLyogTW9kYWwgQm9keSAqL1xuLm1vZGFsLWJvZHkge1xuICBwYWRkaW5nOiAycHggMTZweDtcbn1cblxuLm1vZGFsLWJvZHkyIHtcbiAgcGFkZGluZzogMnB4IDE2cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm1vZGFsLWJ1dHRvbnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nOiAycHggMTZweDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuXG4uY29udGFpbmVyLW5vbWluYWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xufVxuXG4vKiBNb2RhbCBGb290ZXIgKi9cbi5tb2RhbC1mb290ZXIge1xuICBwYWRkaW5nOiAycHggMTZweDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4ucmVkIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLyogTW9kYWwgQ29udGVudCAqL1xuLm1vZGFsLWNvbnRlbnQge1xuICBtYXJnaW4tbGVmdDogMjU2cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiAwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xuICB3aWR0aDogNTAlO1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICBhbmltYXRpb24tbmFtZTogYW5pbWF0ZXRvcDtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjRzO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5tb2RhbC1jb250ZW50MiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiAwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xuICB3aWR0aDogMzV2dztcbiAgbWluLXdpZHRoOiAzMDBweDtcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vKiBUaGUgQ2xvc2UgQnV0dG9uICovXG4uY2xvc2Uge1xuICBjb2xvcjogIzAwMDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDI4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uY2xvc2U6aG92ZXIsXG4uY2xvc2U6Zm9jdXMge1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLyogQWRkIEFuaW1hdGlvbiAqL1xuQGtleWZyYW1lcyBhbmltYXRldG9wIHtcbiAgZnJvbSB7XG4gICAgdG9wOiAtMzAwcHg7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICB0byB7XG4gICAgdG9wOiAwO1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbn1cbnRleHRhcmVhIHtcbiAgcGFkZGluZzogMTBweDtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBib3gtc2hhZG93OiAxcHggMXB4IDFweCAjOTk5O1xufVxuXG5jYW5jZWwtb3JkZXIge1xuICBtYXJnaW4tbGVmdDogNXB4O1xufVxuXG4ubG9nbyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmJnIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDExNTBweCkge1xuICAuYXdiLWxpc3Qge1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuICAuYXdiLWxpc3QgLmF3YiB7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICB9XG5cbiAgLmF3Yi1saXN0ID4gLnByb2Nlc3Mge1xuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgIGhlaWdodDogMzJweDtcbiAgICB3aWR0aDogOTBweDtcbiAgfVxufVxuLmV4YW1wbGUtcmFkaW8tZ3JvdXAge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBtYXJnaW46IDE1cHggMDtcbn1cblxuLmV4YW1wbGUtcmFkaW8tYnV0dG9uIHtcbiAgbWFyZ2luOiA1cHg7XG59XG5cbi5pbWctdXBsb2FkIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbjogMTBweCA1cHg7XG59XG5cbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXI6IDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG5idXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcbn1cblxuLmlucHV0LWRlbGl2ZXJ5LWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LXdpZHRoOiAzMDBweDtcbn1cbi5pbnB1dC1kZWxpdmVyeS1jb250YWluZXIgbGFiZWwge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi10b3A6IDAgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogMC4ycmVtO1xufVxuLmlucHV0LWRlbGl2ZXJ5LWNvbnRhaW5lciBpbnB1dCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLmJ1dHRvbi1jb21wbGV0ZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmJ1dHRvbi1jb21wbGV0ZSAuaGFsZi1idXR0b24ge1xuICB3aWR0aDogNDglO1xufVxuLmJ1dHRvbi1jb21wbGV0ZSAuZnVsbC1idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbn1cblxubmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtaG91ciwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtbWVyaWRpYW4sIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLW1pbnV0ZSwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtc2Vjb25kIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMTBweCAwO1xufVxubmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtaG91ciBpbnB1dCwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtbWVyaWRpYW4gaW5wdXQsIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLW1pbnV0ZSBpbnB1dCwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtc2Vjb25kIGlucHV0IHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxubmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtaG91ciAuYnRuLCBuZ2ItdGltZXBpY2tlciA6Om5nLWRlZXAgLm5nYi10cC1tZXJpZGlhbiAuYnRuLCBuZ2ItdGltZXBpY2tlciA6Om5nLWRlZXAgLm5nYi10cC1taW51dGUgLmJ0biwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtc2Vjb25kIC5idG4ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiA1cHg7XG4gIHBhZGRpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAxO1xuICBmb250LXNpemU6IDEwcHg7XG4gIGNvbG9yOiBncmV5O1xufVxubmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtaG91ciAuYnRuOmZpcnN0LWNoaWxkLCBuZ2ItdGltZXBpY2tlciA6Om5nLWRlZXAgLm5nYi10cC1tZXJpZGlhbiAuYnRuOmZpcnN0LWNoaWxkLCBuZ2ItdGltZXBpY2tlciA6Om5nLWRlZXAgLm5nYi10cC1taW51dGUgLmJ0bjpmaXJzdC1jaGlsZCwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtc2Vjb25kIC5idG46Zmlyc3QtY2hpbGQge1xuICB0b3A6IDJweDtcbn1cbm5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLWhvdXIgLmJ0bjpudGgtb2YtdHlwZSgyKSwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtbWVyaWRpYW4gLmJ0bjpudGgtb2YtdHlwZSgyKSwgbmdiLXRpbWVwaWNrZXIgOjpuZy1kZWVwIC5uZ2ItdHAtbWludXRlIC5idG46bnRoLW9mLXR5cGUoMiksIG5nYi10aW1lcGlja2VyIDo6bmctZGVlcCAubmdiLXRwLXNlY29uZCAuYnRuOm50aC1vZi10eXBlKDIpIHtcbiAgYm90dG9tOiAycHg7XG59XG5cbi5kZWxpdmVyZWQtZm9ybSB7XG4gIG92ZXJmbG93OiB2aXNpYmxlICFpbXBvcnRhbnQ7XG59XG5cbi5yZWRlbGl2ZXJ5LWxhYmVsIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlICFpbXBvcnRhbnQ7XG4gIHotaW5kZXg6IDIgIWltcG9ydGFudDtcbn1cblxuLnJldHVybi1mb3JtIHtcbiAgb3ZlcmZsb3c6IHZpc2libGUgIWltcG9ydGFudDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbiAhaW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogc3RhcnQgIWltcG9ydGFudDtcbn1cbi5yZXR1cm4tZm9ybSAuaW5mb3JtYXRpb24tdGV4dCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBib3R0b206IDNweDtcbiAgdGV4dC10cmFuc2Zvcm06IGluaXRpYWw7XG59XG4ucmV0dXJuLWZvcm0gbGFiZWwge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi10b3A6IDAgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogMC4ycmVtO1xufVxuXG4ub3JkZXItY29tcGxldGUtdGV4dCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICMwRUE4REI7XG59XG5cbi5jYW5jZWwtc3RhdHVzIHtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5cbi5vdXRlciB7XG4gIG1pbi13aWR0aDogMjB2dztcbiAgZmxleDogMTtcbn1cblxuLnByb2dyZXNzIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwYWRkaW5nOiAzMHB4IDIwcHg7XG59XG4ucHJvZ3Jlc3MgPiBkaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBjb2xvcjogIzMzMztcbn1cbi5wcm9ncmVzcyA+IGRpdi5sZWZ0IHtcbiAgZmxleDogMTtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG4ucHJvZ3Jlc3MgPiBkaXYubGVmdCBkaXY6bGFzdC1vZi10eXBlOmFmdGVyIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbi5wcm9ncmVzcyA+IGRpdi5sZWZ0IGRpdjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMTI4LCAwLCAwLjEpO1xuICBib3JkZXItcmFkaXVzOiAycHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IC0yMHB4O1xuICB0b3A6IDEwcHg7XG4gIGhlaWdodDogMTAxJTtcbiAgd2lkdGg6IDFweDtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDUwJSk7XG59XG4ucHJvZ3Jlc3MgPiBkaXYubGVmdCAuY3JlYXRlZC1kYXRlIHtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBjb2xvcjogIzc2NzQ3NDtcbn1cbi5wcm9ncmVzcyA+IGRpdi5yaWdodCB7XG4gIGZsZXg6IDI7XG4gIHBhZGRpbmctbGVmdDogMjBweDtcbn1cbi5wcm9ncmVzcyA+IGRpdi5yaWdodCBkaXY6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBiYWNrZ3JvdW5kOiBncmVlbjtcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IC0yMHB4O1xuICB0b3A6IDEwcHg7XG4gIGhlaWdodDogMTAxJTtcbiAgd2lkdGg6IDJweDtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICB0cmFuc2l0aW9uOiBoZWlnaHQgMC4ycyBlYXNlO1xufVxuLnByb2dyZXNzID4gZGl2LnJpZ2h0IC5yb3dzdGF0ZS1uYW1lLnByZXY6YWZ0ZXIge1xuICB0cmFuc2l0aW9uOiBub25lO1xufVxuLnByb2dyZXNzID4gZGl2LnJpZ2h0IC5yb3dzdGF0ZS1uYW1lOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGJhY2tncm91bmQ6IGdyZWVuO1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAtMjBweDtcbiAgdG9wOiAxMHB4O1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSkgdHJhbnNsYXRlWSgtNTAlKTtcbiAgdHJhbnNpdGlvbjogcGFkZGluZyAwLjJzIGVhc2U7XG59XG4ucHJvZ3Jlc3MgPiBkaXYucmlnaHQgLmRlc2NyaXB0aW9uIHtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJvdHRvbTogMjBweDtcbiAgY29sb3I6ICM3Njc0NzQ7XG59XG4ucHJvZ3Jlc3MgPiBkaXYucmlnaHQgLmN1cnJlbnQ6YWZ0ZXIge1xuICBoZWlnaHQ6IDAlO1xuICB0cmFuc2l0aW9uOiBoZWlnaHQgMC4ycyBlYXNlLW91dDtcbn1cbi5wcm9ncmVzcyA+IGRpdi5yaWdodCAuY3VycmVudCAucm93c3RhdGUtbmFtZSB7XG4gIGNvbG9yOiAjMzMzO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5wcm9ncmVzcyA+IGRpdi5yaWdodCAuY3VycmVudCAucm93c3RhdGUtbmFtZTpiZWZvcmUge1xuICBiYWNrZ3JvdW5kOiBncmVlbjtcbiAgcGFkZGluZzogMTBweDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgMC4xNXMgY3ViaWMtYmV6aWVyKDAuMTc1LCAwLjg4NSwgMC4zMiwgMik7XG59XG4ucHJvZ3Jlc3MgPiBkaXYucmlnaHQgLmN1cnJlbnQgLnJvd3N0YXRlLW5hbWU6YWZ0ZXIge1xuICBoZWlnaHQ6IDAlO1xuICB0cmFuc2l0aW9uOiBoZWlnaHQgMC4ycyBlYXNlLW91dDtcbn1cbi5wcm9ncmVzcyA+IGRpdi5yaWdodCAuY3VycmVudCAucm93c3RhdGUtbmFtZSB+IGRpdjpiZWZvcmUge1xuICBiYWNrZ3JvdW5kOiAjYThhNWE1O1xuICBwYWRkaW5nOiAyLjVweDtcbn1cbi5wcm9ncmVzcyA+IGRpdi5yaWdodCAuY3VycmVudCAucm93c3RhdGUtbmFtZSB+IGRpdjphZnRlciB7XG4gIGhlaWdodDogMCU7XG4gIHRyYW5zaXRpb246IG5vbmU7XG59XG4ucHJvZ3Jlc3MgPiBkaXYgZGl2IHtcbiAgZmxleDogMTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgY3Vyc29yOiBkZWZhdWx0O1xuICBtaW4taGVpZ2h0OiA0MnB4O1xufVxuXG4uYnV0dG9uLXRyYWNraW5nIHtcbiAgY29sb3I6ICMwMDdiZmY7XG4gIHBhZGRpbmctdG9wOiA2cHg7XG4gIGNsZWFyOiBib3RoO1xufVxuXG4uc2hpcHBpbmctaWQge1xuICBwYWRkaW5nLWJvdHRvbTogMCAhaW1wb3J0YW50O1xuICBjb2xvcjogIzBFQThEQjtcbn1cblxuLnNoaXBwaW5nLXN0YXR1cy1yb3cge1xuICBjb2xvcjogIzAxNjkxODtcbiAgYm9yZGVyLWJvdHRvbTogMS41cHggc29saWQgI2VhZWFlYTtcbiAgYm9yZGVyLXdpZHRoOiB1bnNldCAhaW1wb3J0YW50O1xufVxuXG4uc2hpcHBpbmctZGV0YWlsIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG5cbi5jYW5jZWwtcGlja3VwIHtcbiAgbWFyZ2luOiAwIDIwcHggMjBweDtcbiAgaGVpZ2h0OiAzNXB4O1xufVxuXG46Om5nLWRlZXAgLm1vZGFsLWRpYWxvZyB7XG4gIG1heC13aWR0aDogMTAwMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5kZWZhdWx0LWxvYWRpbmcge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uZGVmYXVsdC1sb2FkaW5nIGkge1xuICBmb250LXNpemU6IDUwcHg7XG4gIGNvbG9yOiAjMDc4MGY4O1xuICBtYXJnaW46IDQwcHggMDtcbn1cblxuLmRlc3RpbmF0aW9uLWNhcmQge1xuICBib3gtc2hhZG93OiA1cHggNXB4IDEwcHggIzk5OTtcbn1cbi5kZXN0aW5hdGlvbi1jYXJkIC5maXJzdC1jb2x1bW4ge1xuICB3aWR0aDogMTA1cHggIWltcG9ydGFudDtcbn1cbi5kZXN0aW5hdGlvbi1jYXJkIC5zZWNvbmQtY29sdW1uIHtcbiAgd2lkdGg6IDEwcHg7XG59XG5cbi5zZWUtb3JkZXItYnV0dG9uIGJ1dHRvbiB7XG4gIG1hcmdpbjogMTJweCAwcHggN3B4IDAgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwN0JGRjtcbiAgY29sb3I6IHdoaXRlO1xuICBib3gtc2hhZG93OiAycHggM3B4IDRweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbn1cblxuLnNlZS1vcmRlci10ZXh0IHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBtYXJnaW4tYm90dG9tOiAyNXB4O1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5zdGFyLXJlcXVpcmVkIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuXG5idXR0b24uYnRuLWRvd25sb2FkLWltZyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiA1cHggMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm9yZGVyOiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/delivery-process/shipping/delivery-process.shipping.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/modules/delivery-process/shipping/delivery-process.shipping.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: DeliveryProcessShippingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryProcessShippingComponent", function() { return DeliveryProcessShippingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



// import { isFunction } from '../../../../object-interface/common.function';
// import { NgxBarcodeModule } from 'ngx-barcode';

// import { MemberService } from '../../../../services/member/member.service';


var DeliveryProcessShippingComponent = /** @class */ (function () {
    function DeliveryProcessShippingComponent(orderhistoryService, 
    // private route: ActivatedRoute,
    router, 
    // public memberService: MemberService
    modalService) {
        this.orderhistoryService = orderhistoryService;
        this.router = router;
        this.modalService = modalService;
        this.awb_check = false;
        this.infoDetail = [];
        this.openMoreDetail = false;
        this.loading = false;
        this.editThis = false;
        this.done = false;
        this.notesCategory = [
            {
                label: 'Product out of stock',
                value: false
            },
            {
                label: 'Color options are not available',
                value: false
            },
            {
                label: 'Product size is not available',
                value: false
            },
            {
                label: 'Other',
                value: false
            }
        ];
        this.shippingStatusList = [
            {
                label: 'on checking and processing',
                value: 'on_processing',
                id: 'on-process',
                checked: true,
                hovered: false,
                values: 'on_processing'
            },
            {
                label: 'On Delivery process',
                value: 'on_delivery',
                id: 'on-delivery',
                hovered: false,
                values: 'on_delivery'
            },
            {
                label: 'Delivered',
                value: 'delivered',
                id: 'delivered',
                hovered: false,
                values: 'delivered'
            }
        ];
        /*public redeliverList: Array<any> = [
            {
                label: 'Return Order Process',
                value: 'return',
                checked: true,
                hovered: false,
            },
            {
                label: 'Return Order Redelivery',
                value: 'redelivery',
                checked: false,
                hovered: false,
            },
            {
                label: 'Delivered',
                value: 'delivered',
                id: 'delivered',
                hovered: false,
                values: 'delivered'
            }
        ];*/
        this.redeliverList = [];
        this.packaging = false;
        this.transactionStatus = [
            { label: 'PAID', value: 'PAID' },
            { label: 'CANCEL', value: 'CANCEL' },
            { label: 'WAITING FOR CONFIRMATION', value: 'WAITING FOR CONFIRMATION' }
        ];
        this.service_courier = [
            { label: 'Drop', value: 'DROP' },
            { label: 'Pickup', value: 'PICKUP' }
        ];
        this.errorLabel = false;
        this.merchantMode = false;
        this.info = [];
        this.change = false;
        this.isClicked = false;
        this.isClicked2 = false;
        this.courier_list = [];
        this.tempstatus = '';
        this.openNotes = false;
        this.editAwb = false;
        this.othercour = false;
        this.supported = false;
        this.courier = '';
        this.delivery_service = '';
        this.delivery_method = '';
        this.courier_name = '';
        this.changeAWB = false;
        this.delivery_remarks = "";
        this.isChangeLoading = false;
        this.isChangeAWBLoading = false;
        // detailToLowerCase: string = "";
        this.isEverDelivered = false;
        this.redelivered_date = [];
        this.redelivered_time = [];
        this.redelivered_receiver_name = [];
        this.redelivered_delivery_status = [];
        this.redelivered_remark = [];
        this.completeButtonConfirmation = false;
        this.isCompleteOrder = false;
        this.disableFormShipping = [];
        this.returnType = "redeliver";
        this.isRedeliver = false;
        this.isChangeRedeliverLoading = false;
        this.redeliverButtonConfirmation = false;
        this.return_date = [];
        this.return_time = [];
        this.return_courier = [];
        this.return_awb_number = [];
        this.return_remarks = [];
        this.isProcessRedelivery = false;
        this.isChangeProcessRedeliveryLoading = false;
        this.processOrderRedeliveryButtonConfirmation = false;
        this.redelivery_date = [];
        this.redelivery_time = [];
        this.redelivery_courier = [];
        this.redelivery_awb_number = [];
        this.redelivery_remarks = [];
        this.redelivery_method = [];
        this.redelivery_services = [];
        this.isRedeliveryCompleted = false;
        this.reorderProductCheck = [];
        this.isReorder = false;
        this.isChangeReorderLoading = false;
        this.reorderButtonConfirmation = false;
        this.isPackaging = false;
        this.isSubmitPackagingLoading = false;
    }
    DeliveryProcessShippingComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.currentPermission = 'admin';
                        // this.deliveryHistoryDetail = this.detail;
                        // console.log("this.detail", this.detail)
                        this.reference_no = this.propsDetail.reference_no;
                        return [4 /*yield*/, this.firstLoad()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, billings, additionalBillings, prop, error_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.orderhistoryService.getShippingDetailByRef(this.reference_no)];
                    case 1:
                        result = _a.sent();
                        if (result && result.values) {
                            this.deliveryHistoryDetail = result.values[0];
                            this.reference_no = this.deliveryHistoryDetail.reference_no;
                        }
                        if (!this.deliveryHistoryDetail.shipping_info)
                            this.deliveryHistoryDetail.shipping_info = [];
                        this.changeAWB = false;
                        if (this.deliveryHistoryDetail.available_courier) {
                            this.deliveryHistoryDetail.available_courier.push({
                                courier: "others",
                                courier_code: "others"
                            });
                        }
                        else {
                            this.courier = 'others';
                        }
                        if (this.deliveryHistoryDetail.courier) {
                            this.courier = this.deliveryHistoryDetail.courier;
                            this.delivery_service = this.deliveryHistoryDetail.delivery_service;
                            this.delivery_method = this.deliveryHistoryDetail.delivery_method;
                            this.awb_number = this.deliveryHistoryDetail.awb_number;
                            if (this.deliveryHistoryDetail.courier == 'others')
                                this.courier_name = this.deliveryHistoryDetail.courier_name;
                        }
                        this.redeliverList = [];
                        this.redelivery_date = [];
                        this.redelivery_time = [];
                        this.redelivery_courier = [];
                        this.redelivery_awb_number = [];
                        this.redelivery_remarks = [];
                        this.redelivery_method = [];
                        this.redelivery_services = [];
                        this.return_date = [];
                        this.return_time = [];
                        this.return_courier = [];
                        this.return_awb_number = [];
                        this.return_remarks = [];
                        this.redelivered_date = [];
                        this.redelivered_time = [];
                        this.redelivered_receiver_name = [];
                        this.redelivered_delivery_status = [];
                        this.redelivered_remark = [];
                        this.disableFormShipping = [];
                        this.completeButtonConfirmation = false;
                        this.processOrderRedeliveryButtonConfirmation = false;
                        this.redeliverButtonConfirmation = false;
                        this.reorderProductCheck = [];
                        this.deliveryHistoryDetail.product_list.forEach(function (product) {
                            _this.reorderProductCheck.push(false);
                        });
                        this.deliveryHistoryDetail.shipping_info.forEach(function (element, index) {
                            if (element.label == "on_delivery") {
                                _this.delivery_remarks = element.remarks != null ? element.remarks : "";
                                _this.isPackaging = true;
                            }
                            else if (element.label == "return") {
                                _this.reorder_remarks = element.remarks;
                                if (index > 2) {
                                    var matchReturnDetail = _this.deliveryHistoryDetail.return_detail.find(function (dReturn) { return dReturn.return_id == element.id; });
                                    _this.return_remarks.push(element.remarks);
                                    _this.return_date.push(_this.convertStringToNgDate(matchReturnDetail.return_date));
                                    _this.return_time.push(_this.convertStringToNgbTimeStruct(matchReturnDetail.return_date));
                                    _this.return_courier.push(matchReturnDetail.courier);
                                    _this.return_awb_number.push(matchReturnDetail.awb_number);
                                    _this.redelivery_date.push(null);
                                    _this.redelivery_time.push(null);
                                    _this.redelivery_courier.push(null);
                                    _this.redelivery_awb_number.push(null);
                                    _this.redelivery_remarks.push(null);
                                    _this.redelivery_method.push(null);
                                    _this.redelivery_services.push(null);
                                    _this.redelivered_date.push(null);
                                    _this.redelivered_time.push(null);
                                    _this.redelivered_receiver_name.push(null);
                                    _this.redelivered_delivery_status.push(null);
                                    _this.redelivered_remark.push(null);
                                    _this.disableFormShipping.push(true);
                                    _this.redeliverList.push({
                                        label: 'Return Order Process',
                                        value: 'return',
                                        checked: true,
                                        hovered: false,
                                    });
                                }
                                ;
                                if (index == (_this.deliveryHistoryDetail.shipping_info.length - 1) && _this.deliveryHistoryDetail.status != 'REORDER') {
                                    _this.addArraysRedelivery();
                                    _this.redeliverList.push({
                                        label: 'Return Order Redelivery',
                                        value: 'redelivery',
                                        checked: true,
                                        hovered: false,
                                    });
                                }
                            }
                            else if (element.label == "redelivery") {
                                if (index > 2) {
                                    var matchRedeliveryDetail = _this.deliveryHistoryDetail.redelivery_detail.find(function (dRedelivery) { return dRedelivery.delivery_id == element.id; });
                                    _this.redelivery_date.push(_this.convertStringToNgDate(matchRedeliveryDetail.delivery_date));
                                    _this.redelivery_time.push(_this.convertStringToNgbTimeStruct(matchRedeliveryDetail.delivery_date));
                                    _this.redelivery_courier.push(matchRedeliveryDetail.courier);
                                    _this.redelivery_awb_number.push(matchRedeliveryDetail.awb_number);
                                    _this.redelivery_remarks.push(element.remarks);
                                    _this.redelivery_method.push(matchRedeliveryDetail.delivery_method);
                                    _this.redelivery_services.push(matchRedeliveryDetail.delivery_service);
                                    _this.return_date.push(null);
                                    _this.return_time.push(null);
                                    _this.return_courier.push(null);
                                    _this.return_awb_number.push(null);
                                    _this.return_remarks.push(null);
                                    _this.redelivered_date.push(null);
                                    _this.redelivered_time.push(null);
                                    _this.redelivered_receiver_name.push(null);
                                    _this.redelivered_delivery_status.push(null);
                                    _this.redelivered_remark.push(null);
                                    _this.disableFormShipping.push(true);
                                    _this.redeliverList.push({
                                        label: 'Return Order Redelivery',
                                        value: 'redelivery',
                                        checked: true,
                                        hovered: false,
                                    });
                                }
                                if (index == (_this.deliveryHistoryDetail.shipping_info.length - 1)) {
                                    _this.addArraysRedelivery();
                                    _this.redeliverList.push({
                                        label: 'Delivered',
                                        value: 'delivered',
                                        id: 'delivered',
                                        hovered: false,
                                        values: 'delivered',
                                        checked: false,
                                    });
                                }
                            }
                            else if (element.label == "delivered") {
                                _this.isEverDelivered = true;
                                _this.isCompleteOrder = true;
                                if (index > 2) {
                                    _this.redelivered_date.push(_this.convertStringToNgDate(element.created_date));
                                    _this.redelivered_time.push(_this.convertStringToNgbTimeStruct(element.created_date));
                                    _this.redelivered_receiver_name.push(element.receiver_name);
                                    _this.redelivered_delivery_status.push(element.delivery_status);
                                    _this.redelivered_remark.push(element.remarks);
                                    _this.redeliverList.push({
                                        label: 'Delivered',
                                        value: 'delivered',
                                        id: 'delivered',
                                        hovered: false,
                                        values: 'delivered',
                                        checked: true,
                                    });
                                    _this.return_date.push(null);
                                    _this.return_time.push(null);
                                    _this.return_courier.push(null);
                                    _this.return_awb_number.push(null);
                                    _this.return_remarks.push(null);
                                    _this.redelivery_date.push(null);
                                    _this.redelivery_time.push(null);
                                    _this.redelivery_courier.push(null);
                                    _this.redelivery_awb_number.push(null);
                                    _this.redelivery_remarks.push(null);
                                    _this.redelivery_method.push(null);
                                    _this.redelivery_services.push(null);
                                    _this.disableFormShipping.push(true);
                                }
                                else {
                                    _this.date = _this.convertStringToNgDate(element.delivered_date);
                                    _this.time = _this.convertStringToNgbTimeStruct(element.delivered_date);
                                    _this.receiver_name = element.receiver_name;
                                    _this.delivery_status = element.delivery_status;
                                    _this.remark = element.remarks;
                                }
                                if (index == (_this.deliveryHistoryDetail.shipping_info.length - 1)) {
                                    _this.addArraysRedelivery();
                                    _this.redeliverList.push({
                                        label: 'Return Order Process',
                                        value: 'return',
                                        checked: true,
                                        hovered: false,
                                    });
                                }
                            }
                        });
                        this.lastIndexRedelivery = this.redeliverList.length - 1;
                        // if(this.deliveryHistoryDetail.status == "RETURN" && this.deliveryHistoryDetail.return_detail) {
                        // 	this.isRedeliver = true;
                        // 	// this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
                        // 	// this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
                        // 	// this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
                        // 	// this.return_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
                        // 	// // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
                        // }
                        // if(this.deliveryHistoryDetail.status == "ACTIVE" && this.deliveryHistoryDetail.redelivery_detail && this.deliveryHistoryDetail.return_detail) {
                        // 	this.isRedeliver = true;
                        // 	// this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
                        // 	// this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
                        // 	// this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
                        // 	// this.return_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
                        // 	// // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
                        // 	this.isProcessRedelivery = true;
                        // 	// this.redeliverList[1].checked = true;
                        // 	// this.redelivery_date = this.convertStringToNgDate(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
                        // 	// this.redelivery_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
                        // 	// this.redelivery_courier = this.deliveryHistoryDetail.redelivery_detail.courier;
                        // 	// this.redelivery_awb_number = this.deliveryHistoryDetail.redelivery_detail.awb_number;
                        // 	// this.redelivery_remarks = this.deliveryHistoryDetail.redelivery_detail.remarks;
                        // 	// this.redelivery_method = this.deliveryHistoryDetail.redelivery_detail.delivery_method;
                        // 	// this.redelivery_services = this.deliveryHistoryDetail.redelivery_detail.delivery_service;
                        // }
                        // if(this.deliveryHistoryDetail.status == "DELIVERED" && this.deliveryHistoryDetail.redelivery_detail && this.deliveryHistoryDetail.return_detail) {
                        // 	// this.isRedeliver = true;
                        // 	// this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
                        // 	// this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
                        // 	// this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
                        // 	// this.return_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
                        // 	// // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
                        // 	this.isProcessRedelivery = true;
                        // 	// this.redeliverList[1].checked = true;
                        // 	// this.redeliverList[2].checked = true;
                        // 	// this.redelivery_date = this.convertStringToNgDate(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
                        // 	// this.redelivery_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
                        // 	// this.redelivery_courier = this.deliveryHistoryDetail.redelivery_detail.courier;
                        // 	// this.redelivery_awb_number = this.deliveryHistoryDetail.redelivery_detail.awb_number;
                        // 	// this.redelivery_remarks = this.deliveryHistoryDetail.redelivery_detail.remarks;
                        // 	// this.redelivery_method = this.deliveryHistoryDetail.redelivery_detail.delivery_method;
                        // 	// this.redelivery_services = this.deliveryHistoryDetail.redelivery_detail.delivery_service;
                        // 	// const completedReturn = this.deliveryHistoryDetail.shipping_info[this.deliveryHistoryDetail.shipping_info.length - 1];
                        // 	// this.date = this.convertStringToNgDate(completedReturn.created_date);
                        // 	// this.time = this.convertStringToNgbTimeStruct(completedReturn.created_date);
                        // 	// this.receiver_name = completedReturn.receiver_name;
                        // 	// this.delivery_status = completedReturn.delivery_status;
                        // 	// this.remark = completedReturn.remarks;
                        // 	// this.isRedeliveryCompleted = true;
                        // }
                        // if(this.deliveryHistoryDetail.status == "CANCEL" && this.deliveryHistoryDetail.return_detail) {
                        // 	// this.isReorder = true;
                        // 	// this.returnType = "reorder";
                        // 	// this.reorder_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
                        // 	// this.reorder_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
                        // 	// this.reorder_courier = this.deliveryHistoryDetail.return_detail.courier;
                        // 	// this.reorder_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
                        // 	// // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
                        // }
                        // STEP_3_GET_MEMBER_DETAIL_AND_SETUP_CURRENT_PERMISSION: {
                        // 	/** I need to get Member detail for getting their permission status */
                        // 	if (this.currentPermission == 'admin') {
                        // 		this.setShippingStatusList();
                        // 	} else if (this.currentPermission == 'merchant') {
                        // 		this.setShippingStatus();
                        // 	} else {
                        // 		console.log('error');
                        // 	}
                        // 	if (this.currentPermission == 'admin') {
                        // 		this.deliveryHistoryDetail.order_list.forEach((element, index) => {
                        // 			if (element.shipping_info == null) {
                        // 				// let no_history = "No History"
                        // 				this.info.push('No History');
                        // 			} else {
                        // 				let array_info = element.shipping_info.length - 1;
                        // 				console.log('array', element.shipping_info);
                        // 				this.info.push(element.shipping_info[array_info].title);
                        // 			}
                        // 		});
                        // 	}
                        // }
                        // console.log("test", this.currentPermission)
                        // try {
                        // this.deliveryHistoryDetailToLowerCase = this.deliveryHistoryDetail.status.toLowerCase();
                        if (typeof this.deliveryHistoryDetail.billings != 'undefined') {
                            billings = this.deliveryHistoryDetail.billings;
                            additionalBillings = [];
                            if (billings.discount) {
                                additionalBillings.push({
                                    product_name: billings.discount.product_name,
                                    value: billings.discount.value,
                                    discount: true
                                });
                            }
                            if (this.deliveryHistoryDetail.status == 'CHECKOUT') {
                                // if (this.deliveryHistoryDetail.payment_detail.payment_via == 'manual banking') {
                                if (billings.unique_amount) {
                                    billings.unique_amount;
                                }
                                // }
                            }
                            else {
                                delete billings.unique_amount;
                            }
                            for (prop in billings) {
                                // console.log("PROP", prop);
                                if (prop == 'shipping_fee' || prop == 'unique_amount') {
                                    additionalBillings.push({
                                        product_name: prop,
                                        value: billings[prop],
                                        discount: false
                                    });
                                }
                            }
                            this.deliveryHistoryDetail.additionalBillings = additionalBillings;
                        }
                        else if (typeof this.deliveryHistoryDetail.billings == 'undefined') {
                            // let additionalBillings = [];
                            // let shipping_services
                            // additionalBillings.push(
                            //   {
                            //     product_name: "Shipping Fee",
                            //     value: this.deliveryHistoryDetail.shipping_fee,
                            //     discount: false
                            //   }
                            // )
                            // let shipping_service = {
                            // 	shipping: {
                            // 		service: this.deliveryHistoryDetail.delivery_detail.delivery_service
                            // 	}
                            // };
                            // let sum_total = {
                            // 	sum_total: this.deliveryHistoryDetail.total_price
                            // };
                            // let detail_courier = this.deliveryHistoryDetail.courier
                            // 	? this.deliveryHistoryDetail.courier.courier
                            // 	: this.deliveryHistoryDetail.shipping_service;
                            // let courier = {
                            // 	shipping_services: detail_courier
                            // };
                            // let courier_detail = {
                            //   courier : {
                            //     courier : "",
                            //     awb_number : "",
                            //     courier_code : "",
                            //   }
                            // }
                            ASSIGN_OBJECT_TO_ORDER_HISTORY_DETAIL: {
                                // Object.assign(this.deliveryHistoryDetail, courier);
                                // Object.assign(this.deliveryHistoryDetail, courier_detail)
                                // Object.assign(this.deliveryHistoryDetail, sum_total);
                                // Object.assign(this.deliveryHistoryDetail, shipping_service);
                                // this.deliveryHistoryDetail.additionalBillings = additionalBillings;
                            }
                            // console.log('Detail', this.deliveryHistoryDetail);
                        }
                        // if (this.deliveryHistoryDetail.merchant) {
                        // 	this.merchantMode = true;
                        // }
                        // if (!this.deliveryHistoryDetail.shipping.service) {
                        // 	this.deliveryHistoryDetail.shipping.service = 'no-services';
                        // 	// console.log("Result", this.deliveryHistoryDetail.shipping)
                        // }
                        // if (this.deliveryHistoryDetail.status) {
                        // 	this.deliveryHistoryDetail.status = this.deliveryHistoryDetail.status.trim().toUpperCase();
                        // }
                        // if (this.deliveryHistoryDetail.products) {
                        // 	// console.log("disini", this.deliveryHistoryDetail.products)
                        // 	this.deliveryHistoryDetail.products.forEach((element, index) => {});
                        // }
                        if (this.deliveryHistoryDetail.shipping_info) {
                            // console.log('this.deliveryHistoryDetail.shipping_info', this.deliveryHistoryDetail.shipping_info)
                            this.shippingStatusList.forEach(function (element, index) {
                                var check = _this.isShippingValueExists(element.value);
                                if (check) {
                                    _this.shippingStatusList[index].checked = check[0];
                                    _this.shippingStatusList[index].created_date = check[1];
                                }
                                else {
                                    _this.shippingStatusList[index].checked = false;
                                }
                            });
                        }
                        // if (this.deliveryHistoryDetail.courier_list.supported){
                        // 	this.courier_list = this.deliveryHistoryDetail.courier_list.supported.concat(this.deliveryHistoryDetail.courier_list.unsupported);
                        // 	this.listService()
                        // }
                        // this.deliveryHistoryDetail.previous_status = this.deliveryHistoryDetail.status;
                        // this.transactionStatus.forEach((element, index) => {
                        // 	if (element.value == this.deliveryHistoryDetail.transaction_status) {
                        // 		this.transactionStatus[index].selected = 1;
                        // 	}
                        // });
                        // } catch (e) {
                        // 	this.errorLabel = (<Error>e).message; //conversion to Error type
                        // }
                        // console.log("type of delivery detail ", this.deliveryHistoryDetail)
                        if (this.deliveryHistoryDetail.weight && this.deliveryHistoryDetail.dimensions) {
                            this.package_height = this.deliveryHistoryDetail.dimensions.height;
                            this.package_length = this.deliveryHistoryDetail.dimensions.length;
                            this.package_width = this.deliveryHistoryDetail.dimensions.width;
                            this.package_weight = this.deliveryHistoryDetail.weight * 1000;
                            // this.isPackaging = true;
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.addArrayAttributes = function (redeliv) {
    };
    DeliveryProcessShippingComponent.prototype.addArraysRedelivery = function () {
        this.return_remarks.push(null);
        this.return_date.push(null);
        this.return_time.push(null);
        this.return_courier.push(null);
        this.return_awb_number.push(null);
        this.redelivery_date.push(null);
        this.redelivery_time.push(null);
        this.redelivery_courier.push(null);
        this.redelivery_awb_number.push(null);
        this.redelivery_remarks.push(null);
        this.redelivery_method.push(null);
        this.redelivery_services.push(null);
        this.redelivered_date.push(null);
        this.redelivered_time.push(null);
        this.redelivered_receiver_name.push(null);
        this.redelivered_delivery_status.push(null);
        this.redelivered_remark.push(null);
        this.disableFormShipping.push(false);
    };
    DeliveryProcessShippingComponent.prototype.isArray = function (obj) {
        return Array.isArray(obj);
    };
    DeliveryProcessShippingComponent.prototype.openScrollableContent = function (longContent) {
        this.modalService.open(longContent, { centered: true });
    };
    DeliveryProcessShippingComponent.prototype.openScrollableTrackingContent = function (trackingContent) {
        this.modalService.open(trackingContent, { centered: true });
    };
    DeliveryProcessShippingComponent.prototype.replaceVarian = function (value) {
        return JSON.stringify(value).replace('{', '').replace('}', '').replace(/[',]+/g, ', ').replace(/['"]+/g, '');
    };
    DeliveryProcessShippingComponent.prototype.updateComplete = function (index) {
        return __awaiter(this, void 0, void 0, function () {
            var delivered_date, payloadData, result, _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 6, , 7]);
                        this.isChangeLoading = true;
                        delivered_date = index ? this.convertDateToString(this.redelivered_date[index], this.redelivered_time[index]) : this.convertDateToString(this.date, this.time);
                        payloadData = {
                            "ref_no": this.reference_no,
                            "delivered_date": delivered_date,
                            "receiver_name": index ? this.redelivered_receiver_name[index] : this.receiver_name,
                            "delivery_status": index ? this.redelivered_delivery_status[index] : this.delivery_status,
                            "remarks": index ? this.redelivered_remark[index] : this.remark
                        };
                        return [4 /*yield*/, this.orderhistoryService.updateDeliveredProcess(payloadData)];
                    case 1:
                        result = _b.sent();
                        if (!(result.status == "success")) return [3 /*break*/, 5];
                        _a = this;
                        return [4 /*yield*/, this.orderhistoryService.getShippingDetailByRef(payloadData.ref_no)];
                    case 2:
                        _a.deliveryHistoryDetail = _b.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 3:
                        _b.sent();
                        return [4 /*yield*/, this.firstLoad()];
                    case 4:
                        _b.sent();
                        _b.label = 5;
                    case 5:
                        this.isChangeLoading = false;
                        return [3 /*break*/, 7];
                    case 6:
                        error_2 = _b.sent();
                        this.isChangeLoading = false;
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.convertDateToString = function (date, time) {
        return date.year.toString().padStart(2, "0") + '-' + date.month.toString().padStart(2, "0") + '-' + date.day.toString().padStart(2, "0") + " " + time.hour.toString().padStart(2, "0") + ':' + time.minute.toString().padStart(2, "0") + ':' + time.second.toString().padStart(2, "0");
    };
    DeliveryProcessShippingComponent.prototype.convertDateOnlyToString = function (date) {
        return date.year.toString().padStart(2, "0") + '-' + date.month.toString().padStart(2, "0") + '-' + date.day.toString().padStart(2, "0");
    };
    DeliveryProcessShippingComponent.prototype.convertStringToNgDate = function (value) {
        var _value = value.split(" ")[0].split("-");
        var result = new _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbDate"](0, 0, 0);
        result.year = parseInt(_value[0]);
        result.month = parseInt(_value[1]);
        result.day = parseInt(_value[2]);
        return result;
    };
    DeliveryProcessShippingComponent.prototype.convertStringToNgbTimeStruct = function (value) {
        var _value = value.split(" ")[1].split(":");
        var result = { hour: parseInt(_value[0]), minute: parseInt(_value[1]), second: parseInt(_value[2]) };
        return result;
    };
    DeliveryProcessShippingComponent.prototype.backToTable = function () {
        this.back[1](this.back[0]);
    };
    DeliveryProcessShippingComponent.prototype.cancelShipping = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                if (!this.reference_no)
                    return [2 /*return*/];
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                    title: 'Anda akan membatalkan proses pickup dari : ' + this.reference_no,
                    text: 'Alasan pembatalan : ',
                    icon: 'error',
                    confirmButtonText: 'Ok',
                    cancelButtonText: "cancel",
                    showCancelButton: true,
                    input: 'text',
                }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                    var payload, result_1, e_1;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!result.isConfirmed) return [3 /*break*/, 4];
                                payload = {
                                    ref_no: this.reference_no,
                                    description: result.value,
                                };
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 3, , 4]);
                                return [4 /*yield*/, this.orderhistoryService.cancelShipping(payload)];
                            case 2:
                                result_1 = _a.sent();
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                    title: "Cancel",
                                    text: 'Pickup dengan no. referensi : ' + this.reference_no + ' telah dibatalkan',
                                    icon: 'error',
                                    confirmButtonText: 'Ok',
                                    showCancelButton: false,
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        // this.backToTable();
                                        _this.firstLoad();
                                    }
                                });
                                return [3 /*break*/, 4];
                            case 3:
                                e_1 = _a.sent();
                                this.errorLabel = (e_1.message);
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                    title: "Error",
                                    text: this.errorLabel,
                                    icon: 'error',
                                    confirmButtonText: 'Ok',
                                });
                                return [3 /*break*/, 4];
                            case 4: return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.processOrder = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                if (!this.detail.order_id)
                    return [2 /*return*/];
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                    title: 'Confirmation',
                    text: 'Apakah anda yakin ingin memproses order : ' + this.detail.order_id,
                    icon: 'success',
                    confirmButtonText: 'process',
                    cancelButtonText: "cancel",
                    showCancelButton: true
                }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                    var payload, result_2, e_2;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!result.isConfirmed) return [3 /*break*/, 4];
                                payload = {
                                    order_id: this.detail.order_id
                                };
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 3, , 4]);
                                return [4 /*yield*/, this.orderhistoryService.processOrder(payload)];
                            case 2:
                                result_2 = _a.sent();
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                    title: "Success",
                                    text: 'Order : ' + this.detail.order_id + ' telah disetujui',
                                    icon: 'success',
                                    confirmButtonText: 'Ok',
                                    showCancelButton: false,
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        _this.backToTable();
                                    }
                                });
                                return [3 /*break*/, 4];
                            case 3:
                                e_2 = _a.sent();
                                this.errorLabel = (e_2.message);
                                return [3 /*break*/, 4];
                            case 4: return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.calculateSumPrice = function (index) {
        var sum_price = this.deliveryHistoryDetail.products[index].fixed_price * this.deliveryHistoryDetail.products[index].quantity;
        return sum_price;
    };
    // async getCourier() {
    // 	let couriers = await this.orderhistoryService.getCourier();
    // 	this.courier_list = couriers.result;
    // 	console.log('result', this.courier_list);
    // 	this.courier_list.forEach((element, index) => {
    // 		let values = {
    // 			value: element.courier
    // 		};
    // 		Object.assign(this.courier_list[index], values);
    // 	});
    // 	console.log(this.courier_list);
    // }
    // backToTable() {
    // 	if (this.back[0][this.back[1]] && !isFunction(this.back[0])) {
    // 		this.back[0][this.back[1]];
    // 	} else if (isFunction(this.back[0])) {
    // 		this.back[0]();
    // 	} else {
    // 		window.history.back();
    // 	}
    // }
    DeliveryProcessShippingComponent.prototype.backToList = function () {
        console.log(true);
        if (this.openMoreDetail) {
            this.openMoreDetail = false;
        }
    };
    // setShippingStatusList() {
    // 	// console.log("here", this.deliveryHistoryDetail.shipping_info);
    // 	this.deliveryHistoryDetail.order_list.forEach((element, index) => {
    // 		if (this.shippingStatusList[index].id == 'on-process') {
    // 			this.shippingStatusList[index].checked = true;
    // 		} else if (this.shippingStatusList[index].id == 'warehouse-packaging') {
    // 			this.shippingStatusList[index].checked = true;
    // 		} else if (this.shippingStatusList[index].id == 'on-delivery') {
    // 			this.shippingStatusList[index].checked = true;
    // 		} else if (this.shippingStatusList[index].id == 'delivered') {
    // 			this.shippingStatusList[index].checked = true;
    // 		}
    // 	});
    // }
    DeliveryProcessShippingComponent.prototype.setShippingStatus = function () {
        var _this = this;
        // console.log("here", this.deliveryHistoryDetail.shipping_info);
        this.deliveryHistoryDetail.shipping_info.forEach(function (element, index) {
            if (_this.shippingStatusList[index].id == 'on-process') {
                _this.shippingStatusList[index].checked = true;
            }
            else if (_this.shippingStatusList[index].id == 'warehouse-packaging') {
                _this.shippingStatusList[index].checked = true;
            }
            else if (_this.shippingStatusList[index].id == 'on-delivery') {
                _this.shippingStatusList[index].checked = true;
            }
            else if (_this.shippingStatusList[index].id == 'delivered') {
                _this.shippingStatusList[index].checked = true;
            }
        });
    };
    DeliveryProcessShippingComponent.prototype.changeshippingStatusList = function (index) {
        //  
        for (var n = 0; n <= index; n++) {
            if (this.shippingStatusList[n].id == 'on-delivery') {
                // console.log("on_delivery");
                // this.shippingStatusList[n].checked = true;
                if (this.deliveryHistoryDetail.shipping_info.at(-1).label == 'on_processing' || this.deliveryHistoryDetail.shipping_info.at(-1).label == 'on_delivery') {
                    // console.log("on_delivery 2");
                    if (this.deliveryHistoryDetail.shipping_info.at(-1).label == 'on_processing')
                        this.changeAWB = true;
                    this.shippingStatusList[n].checked = true;
                }
                // if (this.deliveryHistoryDetail.shipping_services) {
                // 	this.shippingStatusList[n].checked = true;
                // }	
                // if (!this.deliveryHistoryDetail.shipping){
                // 	this.shippingStatusList[n].checked = true;
                // }
            }
            else if (this.shippingStatusList[n].id == 'delivered') {
                console.log("delivered");
                if (this.deliveryHistoryDetail.awb_number) {
                    console.log("delivered 2");
                    this.shippingStatusList[n].checked = true;
                }
            }
            //  else if(this.shippingStatusList[n].id == 'on_processing'){
            // 	// else if (this.shippingStatusList[n].id == 'warehouse-packaging') {
            // 	//   let warehouse_packaging = (this.deliveryHistoryDetail.billings != undefined) ? this.deliveryHistoryDetail.shipping_services : this.deliveryHistoryDetail.courier.courier
            // 	//   if (warehouse_packaging) {
            // 	//     this.shippingStatusList[n].checked = true;
            // 	//   }
            // 	// }
            // 	this.shippingStatusList[n].checked = true;
            // }
            // if (this.shippingStatusList[n].id == 'warehouse-packaging') {
            // 	this.packaging = true;
            // 	// this.shippingStatusList[n].checked = true;
            // } else {
            // 	this.packaging = false;
            // }
        }
        // for (let n = index + 1; n < this.shippingStatusList.length; n++) {
        // 	this.shippingStatusList[n].checked = false;
        // }
    };
    DeliveryProcessShippingComponent.prototype.hoverShippingStatusList = function (index, objectHover) {
        for (var n = 0; n <= index; n++) {
            this.shippingStatusList[n].hovered = true;
        }
        for (var n = index + 1; n < this.shippingStatusList.length; n++) {
            this.shippingStatusList[n].hovered = false;
        }
    };
    DeliveryProcessShippingComponent.prototype.convertShippingStatusToShippingInfo = function () {
        var newData = [];
        this.shippingStatusList.forEach(function (element, index) {
            if (element.checked) {
                newData.push({ value: element.values });
            }
        });
        return newData;
    };
    DeliveryProcessShippingComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var frm, all_shipping_label, shipping_label, awb, courier, payload, newpayload, temp_payload, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.currentPermission == 'admin' && this.isClicked2)) return [3 /*break*/, 1];
                        this.deliveryHistoryDetail.status = this.tempstatus;
                        this.cancelMerchant();
                        return [3 /*break*/, 10];
                    case 1:
                        _a.trys.push([1, 9, , 10]);
                        this.loading = !this.loading;
                        this.deliveryHistoryDetail.status = this.tempstatus;
                        if (!(this.deliveryHistoryDetail.billings != undefined)) return [3 /*break*/, 3];
                        frm = JSON.parse(JSON.stringify(this.deliveryHistoryDetail));
                        frm.shipping_info = this.convertShippingStatusToShippingInfo();
                        // this.deliveryHistoryDetail.shipping_status = this.valu
                        // if (frm.status != "CANCEL") {
                        delete frm.buyer_detail;
                        delete frm.payment_expire_date;
                        delete frm.products;
                        delete frm.billings;
                        delete frm.user_id;
                        console.log('frm', frm);
                        return [4 /*yield*/, this.orderhistoryService.updateOrderHistoryAllHistory(frm)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 8];
                    case 3:
                        all_shipping_label = this.convertShippingStatusToShippingInfo();
                        shipping_label = all_shipping_label[all_shipping_label.length - 1].value;
                        awb = this.deliveryHistoryDetail.awb_receipt
                            ? this.deliveryHistoryDetail.awb_receipt
                            : this.deliveryHistoryDetail.courier ? this.deliveryHistoryDetail.courier.awb_number : null;
                        courier = this.deliveryHistoryDetail.shipping_services
                            ? this.deliveryHistoryDetail.shipping_services
                            : 'no-services';
                        console.log('courier', this.deliveryHistoryDetail.billings, this.deliveryHistoryDetail.courier);
                        if (this.deliveryHistoryDetail.billings == undefined &&
                            this.deliveryHistoryDetail.courier != undefined &&
                            this.deliveryHistoryDetail.courier.courier_code) {
                            courier = this.deliveryHistoryDetail.courier.courier_code;
                        }
                        payload = {
                            booking_id: this.deliveryHistoryDetail.booking_id,
                            shipping_label: shipping_label,
                            courier_code: courier,
                            awb_number: awb,
                            delivery_method: this.choose_service,
                            status: this.deliveryHistoryDetail.status
                        };
                        console.log('payload', payload);
                        if (!this.othercour) return [3 /*break*/, 5];
                        newpayload = {
                            booking_id: this.deliveryHistoryDetail.booking_id,
                            courier: this.otherCourValue,
                            courier_support: 0,
                            awb_number: awb,
                            shipping_label: shipping_label
                        };
                        return [4 /*yield*/, this.orderhistoryService.updateOrderHistoryAllHistoryMerchant(newpayload)];
                    case 4:
                        _a.sent();
                        return [3 /*break*/, 7];
                    case 5:
                        if (shipping_label == 'on_packaging' && payload.courier_code == 'LO-CR-GR') { // special case for INSTANT courier , add courier_support =0 and courier name 
                            temp_payload = {
                                courier_support: 0,
                                courier: 'GRAB'
                            };
                            Object.assign(payload, temp_payload);
                        }
                        return [4 /*yield*/, this.orderhistoryService.updateOrderHistoryAllHistoryMerchant(payload)];
                    case 6:
                        _a.sent();
                        console.log('choose', this.choose_service);
                        if (this.choose_service == 'PICKUP') {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                title: 'Pemberitahuan Pickup',
                                html: 'Barang anda akan segera di pickup oleh kurir minimal<br/> 2 jam setelah anda melakukan permintaan pickup <br/> <a style="color:red">Baca Term of Service pickup Courier</a>',
                                imageUrl: 'assets/images/alarm.png',
                                imageWidth: 115,
                                imageHeight: 129
                            });
                        }
                        _a.label = 7;
                    case 7:
                        console.log(this.convertShippingStatusToShippingInfo());
                        console.log(this.deliveryHistoryDetail);
                        console.log(payload);
                        _a.label = 8;
                    case 8:
                        // else{
                        //   await this.orderhistoryService.updateOrderHistoryAllHistory(frm.status);
                        // }
                        this.loading = !this.loading;
                        this.done = true;
                        this.change = false;
                        this.editThis = false;
                        alert('Edit submitted');
                        this.firstLoad();
                        return [3 /*break*/, 10];
                    case 9:
                        e_3 = _a.sent();
                        this.errorLabel = e_3.message; //conversion to Error type
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.receipt = function () {
        this.urlReceipt = this.deliveryHistoryDetail.uploaded_receipt;
        window.open(this.urlReceipt);
    };
    DeliveryProcessShippingComponent.prototype.openLabel = function () {
        this.router.navigate(['administrator/orderhistoryallhistoryadmin/receipt'], {
            queryParams: { id: this.deliveryHistoryDetail._id }
        });
    };
    DeliveryProcessShippingComponent.prototype.openNote = function () {
        this.openNotes = true;
        // this.deliveryHistoryDetail.status = 'CANCEL'
        // if(this.notesCategory[0] == true){
        // 	console.log(this.notesCategory[0].label);
        // } else if(this.notesCategory[1]){
        // 	value: true;
        // } else if(this.notesCategory[2]){
        // 	value: true;
        // }
    };
    DeliveryProcessShippingComponent.prototype.onAWBchange = function () {
        console.log(' here', this.deliveryHistoryDetail.courier.awb_number);
        if (this.deliveryHistoryDetail.courier.awb_number) {
            if (this.deliveryHistoryDetail.courier.awb_number.length == 0) {
                this.awb_check = false;
            }
            if (this.deliveryHistoryDetail.courier.awb_number.length > 0) {
                this.awb_check = true;
            }
        }
    };
    DeliveryProcessShippingComponent.prototype.openNote2 = function () {
        this.openNotes2 = true;
        // this.deliveryHistoryDetail.status = 'CANCEL'
    };
    DeliveryProcessShippingComponent.prototype.clickCategory = function (index) {
        var _this = this;
        this.notesCategory[index].value = true;
        this.notesCategory.forEach(function (element, i) {
            if ((_this.notesCategory[index].value = true)) {
                _this.notesCategory[i].value = false;
                _this.notesCategory[index].value = true;
            }
        });
        this.notes = this.notesCategory[index].label;
        console.log(this.notesCategory);
    };
    DeliveryProcessShippingComponent.prototype.noteClose = function () {
        this.openNotes = this.openNotes2 = false;
    };
    DeliveryProcessShippingComponent.prototype.hold = function () {
        return __awaiter(this, void 0, void 0, function () {
            var form, holdMerchant, e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        form = {
                            value: this.holdvalue,
                            description: this.notes,
                            remark: 'hold transaction process'
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderhistoryService.holdMerchant(this.detailPage.booking_id, form)];
                    case 2:
                        holdMerchant = _a.sent();
                        this.closeNote2();
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_4 = _a.sent();
                        this.errorLabel = e_4.message; //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.closeNote = function () {
        this.openNotes = this.openNotes2 = false;
        this.firstLoad();
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Cancelled', 'Your order has been Cancelled', 'success');
    };
    DeliveryProcessShippingComponent.prototype.closeNote2 = function () {
        this.openNotes = this.openNotes2 = false;
        this.firstLoad();
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('HOLD', 'The order has been hold', 'success');
    };
    DeliveryProcessShippingComponent.prototype.cancelOrders = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, cancel;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.currentPermission == 'merchant') {
                            payload = {
                                booking_id: this.deliveryHistoryDetail.booking_id,
                                note: this.notes
                            };
                        }
                        else {
                            payload = {
                                booking_id: this.detailPage.booking_id,
                                note: this.notes
                            };
                            console.log('payload', this.deliveryHistoryDetail);
                        }
                        return [4 /*yield*/, this.orderhistoryService.cancelOrder(payload)];
                    case 1:
                        cancel = _a.sent();
                        this.closeNote();
                        return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.cancelMerchant = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, cancel, e_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        payload = {
                            status: this.tempstatus
                        };
                        console.log('payload', payload.status);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderhistoryService.updateOrderHistoryAllHistoryCancel(this.deliveryHistoryDetail._id, payload)];
                    case 2:
                        cancel = _a.sent();
                        if (cancel) {
                            this.loading = !this.loading;
                            this.done = true;
                            this.change = false;
                            this.editThis = false;
                            alert('Edit submitted');
                            this.firstLoad();
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_5 = _a.sent();
                        this.errorLabel = e_5.message; //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.edit = function () {
        this.editThis = true;
        console.log('edit true');
    };
    DeliveryProcessShippingComponent.prototype.doneEdit = function () {
        this.editThis = false;
        console.log('done');
    };
    DeliveryProcessShippingComponent.prototype.changestatus = function () {
        this.change = true;
    };
    DeliveryProcessShippingComponent.prototype.onpaidclick = function () {
        //   this.deliveryHistoryDetail.status = "PAID";
        this.tempstatus = 'PAID';
        this.isClicked = true;
        this.isClicked2 = false;
        console.log(this.deliveryHistoryDetail);
    };
    DeliveryProcessShippingComponent.prototype.oncancelclick = function () {
        // this.deliveryHistoryDetail.status = "CANCEL";
        this.tempstatus = 'CANCEL';
        this.isClicked2 = true;
        this.isClicked = false;
        console.log(this.deliveryHistoryDetail);
    };
    DeliveryProcessShippingComponent.prototype.openDetail = function (bookingID, order) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.openMoreDetail = true;
                        return [4 /*yield*/, this.orderhistoryService.getDetailOrderhistory(bookingID)];
                    case 1:
                        result = _a.sent();
                        this.moreOrderDetail = order;
                        console.log('moreorderdetail', this.moreOrderDetail);
                        this.detailPage = result.result[0];
                        console.log('detail', this.detailPage);
                        // this.deliveryHistoryDetail.order_list.forEach((element, index) =>{
                        // })
                        if (this.detailPage.shipping_info) {
                            this.detailPage.shipping_info.forEach(function (element, index) {
                                if (element == null) {
                                    console.log('part 1', element);
                                    _this.infoDetail.push('No Service');
                                }
                                else {
                                    console.log('part 2', element);
                                    var array = _this.detailPage.shipping_info.length - 1;
                                    console.log('test', array);
                                    _this.infoDetail = _this.detailPage.shipping_info[array].title;
                                    _this.infoDetailLabel = _this.detailPage.shipping_info[array].label;
                                    console.log('this2', _this.infoDetail);
                                }
                            });
                        }
                        // if(this.detailPage.shipping_info){
                        // 	this.detailPage.shipping_info.forEach((element, index) => {
                        // 		console.log(element.shipping_info)
                        // 	})
                        // }
                        if (this.openMoreDetail &&
                            this.deliveryHistoryDetail.status == 'PAID' &&
                            this.deliveryHistoryDetail.status != 'COMPLETED' &&
                            (this.infoDetailLabel == 'on_delivery' || this.infoDetailLabel == 'delivered')) {
                            console.log('true bro');
                        }
                        else {
                            console.log('salah cok');
                        }
                        console.log('lkawdj', this.deliveryHistoryDetail.status, this.infoDetailLabel);
                        console.log('booking id', bookingID);
                        return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.other_cour = function () {
        console.log('dipilih', this.deliveryHistoryDetail.shipping_services);
        if (this.deliveryHistoryDetail.shipping_services == 'other') {
            console.log("othernih");
            this.othercour = true;
            this.supported = false;
        }
        else {
            this.supported = true;
            this.othercour = false;
        }
        console.log("courier_list", this.courier_list, "method", this.choose_service);
    };
    DeliveryProcessShippingComponent.prototype.listService = function () {
        var _this = this;
        this.courier_list.forEach(function (entry) {
            console.log(entry);
            console.log(entry.delivery_method);
            if (_this.deliveryHistoryDetail.shipping_services == entry.courier_code || _this.deliveryHistoryDetail.shipping_services == entry.courier) {
                _this.method_delivery = entry.delivery_method;
            }
        });
    };
    DeliveryProcessShippingComponent.prototype.checkStatus = function () {
        // console.log("Status", this.deliveryHistoryDetail.status)
        this.statusValue = this.deliveryHistoryDetail.status;
        this.statusLabel = this.deliveryHistoryDetail.status;
        if (this.statusValue != 'PAID' && this.statusValue != 'CANCEL') {
            this.transactionStatus.push({ label: this.statusLabel, value: this.statusValue });
        }
    };
    DeliveryProcessShippingComponent.prototype.isShippingValueExists = function (value) {
        for (var _i = 0, _a = this.deliveryHistoryDetail.shipping_info; _i < _a.length; _i++) {
            var element = _a[_i];
            console.log('element', element.label);
            if (element.label.trim().toLowerCase() == value.trim().toLowerCase()) {
                return [true, element.created_date];
            }
        }
        return false;
    };
    DeliveryProcessShippingComponent.prototype.updateAWB = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.deliveryHistoryDetail.shipping_info.at(-1).label == 'on_delivery' || this.deliveryHistoryDetail.shipping_info.at(-1).label == 'on_processing')) return [3 /*break*/, 7];
                        if (!(this.courier &&
                            this.delivery_service &&
                            this.delivery_method)) return [3 /*break*/, 6];
                        payload = {
                            ref_no: this.deliveryHistoryDetail.reference_no,
                            courier: this.courier,
                            delivery_method: this.delivery_method,
                            delivery_service: this.delivery_service,
                            remarks: this.delivery_remarks,
                            delivery_options: {},
                        };
                        if (this.courier == 'others') {
                            if (!this.courier_name) {
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "mohon masukan nama kurir terlebih dahulu", 'warning');
                                return [2 /*return*/];
                            }
                            // if(!this.awb_number){
                            // 	Swal.fire("Oops", "mohon masukan nomor resi terlebih dahulu", 'warning');
                            // 	return;
                            // }
                            payload.courier_name = this.courier_name;
                            payload.awb_number = this.awb_number ? this.awb_number : '';
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.isChangeAWBLoading = true;
                        return [4 /*yield*/, this.orderhistoryService.updatDeliveryProcess(payload)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 3:
                        _a.sent();
                        this.isChangeAWBLoading = false;
                        this.firstLoad();
                        return [3 /*break*/, 5];
                    case 4:
                        e_6 = _a.sent();
                        this.isChangeAWBLoading = false;
                        this.errorLabel = e_6.message; //conversion to Error type	
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            title: "Error",
                            text: this.errorLabel,
                            icon: 'error',
                            confirmButtonText: 'Ok',
                        });
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Oops", "Service dan metode pengiriman harus di input terlebih dahulu", 'warning');
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.updateDetailOrder = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, e_7;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.orderhistoryService.getOrderDetail(this.deliveryHistoryDetail.order_id)];
                    case 1:
                        _a.deliveryHistoryDetail = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_7 = _b.sent();
                        this.errorLabel = e_7.message; //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.updateStatusCompleted = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.deliveryHistoryDetail.last_shipping_info == 'on_delivery')) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        payload = {
                            order_id: this.deliveryHistoryDetail.order_id,
                        };
                        return [4 /*yield*/, this.orderhistoryService.updateStatusOrder(payload, 'complete')];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 3:
                        _a.sent();
                        this.firstLoad();
                        return [3 /*break*/, 5];
                    case 4:
                        e_8 = _a.sent();
                        this.errorLabel = e_8.message; //conversion to Error type
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            title: "Error",
                            text: this.errorLabel,
                            icon: 'error',
                            confirmButtonText: 'Ok',
                        });
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.processReturn = function (index, returnType) {
        if (returnType === void 0) { returnType = 'redeliver'; }
        return __awaiter(this, void 0, void 0, function () {
            var payload_1, e_9;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isChangeRedeliverLoading = true;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        payload_1 = {
                            "return_type": returnType.toUpperCase(),
                            "ref_no": this.reference_no,
                            "courier": this.return_courier[index],
                            "return_date": this.convertDateOnlyToString(this.return_date[index]),
                            "remarks": this.return_remarks[index],
                            "awb_number": this.return_awb_number[index],
                            'product_sku': []
                        };
                        if (returnType == 'reorder') {
                            console.log('checkbox', this.reorderProductCheck);
                            this.reorderProductCheck.forEach(function (check, i) {
                                if (check == true)
                                    payload_1.product_sku.push(_this.deliveryHistoryDetail.product_list[i]['sku_code']);
                            });
                        }
                        else {
                            delete payload_1.product_sku;
                        }
                        return [4 /*yield*/, this.orderhistoryService.processRedeliver(payload_1)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 3:
                        _a.sent();
                        this.isChangeRedeliverLoading = false;
                        this.firstLoad();
                        return [3 /*break*/, 5];
                    case 4:
                        e_9 = _a.sent();
                        this.isChangeRedeliverLoading = false;
                        this.errorLabel = e_9.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.processOrderRedelivery = function (index) {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.isChangeProcessRedeliveryLoading = true;
                        payload = {
                            "ref_no": this.reference_no,
                            "courier": this.redelivery_courier[index],
                            "delivery_method": this.redelivery_method[index],
                            "delivery_service": this.redelivery_services[index],
                            "delivery_date": this.convertDateOnlyToString(this.redelivery_date[index]),
                            "remarks": this.redelivery_remarks[index],
                            "awb_number": this.redelivery_awb_number[index],
                        };
                        return [4 /*yield*/, this.orderhistoryService.processOrderRedelivery(payload)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 2:
                        _a.sent();
                        this.isChangeProcessRedeliveryLoading = false;
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_10 = _a.sent();
                        this.isChangeProcessRedeliveryLoading = false;
                        this.errorLabel = e_10.message; //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.processReorder = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.isChangeReorderLoading = true;
                        payload = {
                            "return_type": "REORDER",
                            "ref_no": this.reference_no,
                            "courier": this.reorder_courier,
                            "return_date": this.convertDateOnlyToString(this.reorder_date),
                            "remarks": this.reorder_remarks,
                            "awb_number": this.reorder_awb_number,
                        };
                        return [4 /*yield*/, this.orderhistoryService.processRedeliver(payload)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 2:
                        _a.sent();
                        this.isChangeReorderLoading = false;
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_11 = _a.sent();
                        this.isChangeReorderLoading = false;
                        this.errorLabel = e_11.message; //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    // {
    // 	"order_id":"1234567890",
    // 	"courier":"JNE",
    // 	"delivery_method":"DROP"
    // 	"delivery_services":"Regular"
    // 	"delivery_date":"2021-08-30",
    // 	"remarks":"barang rusak",
    // 	"awb_number":"000000000"
    // }
    DeliveryProcessShippingComponent.prototype.submitPackaging = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        payload = {
                            "ref_no": this.deliveryHistoryDetail.reference_no,
                            "weight": this.package_weight,
                            "dimensions": {
                                "length": this.package_length,
                                "width": this.package_width,
                                "height": this.package_height
                            }
                        };
                        this.isSubmitPackagingLoading = true;
                        return [4 /*yield*/, this.orderhistoryService.processPackaging(payload)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.updateDetailOrder()];
                    case 2:
                        _a.sent();
                        // this.isChangeReorderLoading = false;
                        this.isSubmitPackagingLoading = false;
                        ;
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_12 = _a.sent();
                        this.isSubmitPackagingLoading = false;
                        // this.isChangeReorderLoading = false;
                        this.errorLabel = e_12.message; //conversion to Error type
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            title: "Error",
                            text: this.errorLabel,
                            icon: 'error',
                            confirmButtonText: 'Ok',
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.checkFileType = function (text) {
        var splitFileName = text.split('.');
        var extensFile = splitFileName[splitFileName.length - 1];
        var extens = extensFile.toLowerCase();
        console.warn("extens", extens);
        return extens == 'pdf' ? 'document' : 'image';
    };
    DeliveryProcessShippingComponent.prototype.lastPathURL = function (url) {
        var resURL = url.split("/");
        console.log("resURL", resURL);
        var lastNumber = resURL.length - 1;
        return resURL[lastNumber];
    };
    DeliveryProcessShippingComponent.prototype.downloadImage = function (url) {
        var filename;
        filename = this.lastPathURL(url);
        fetch(url).then(function (t) {
            return t.blob().then(function (b) {
                var a = document.createElement("a");
                a.href = URL.createObjectURL(b);
                a.setAttribute("download", filename);
                a.click();
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.getUrlElement = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var result, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.orderhistoryService.getHtmlElement(url)];
                    case 1:
                        result = _b.sent();
                        console.warn("result", result);
                        return [3 /*break*/, 3];
                    case 2:
                        _a = _b.sent();
                        console.warn("Error");
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DeliveryProcessShippingComponent.prototype.getUrlImg = function (url) {
        console.warn("MASUK");
        var imageTags = url.getElementsByTagName("img"); // Returns array of <img> DOM nodes
        var sources = [];
        for (var i in imageTags) {
            var src = imageTags[i].src;
            sources.push(src);
        }
        console.warn("sources img", sources);
    };
    DeliveryProcessShippingComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModal"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DeliveryProcessShippingComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DeliveryProcessShippingComponent.prototype, "back", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DeliveryProcessShippingComponent.prototype, "propsDetail", void 0);
    DeliveryProcessShippingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-delivery-process-shipping',
            template: __webpack_require__(/*! raw-loader!./delivery-process.shipping.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/delivery-process/shipping/delivery-process.shipping.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./delivery-process.shipping.component.scss */ "./src/app/layout/modules/delivery-process/shipping/delivery-process.shipping.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModal"]])
    ], DeliveryProcessShippingComponent);
    return DeliveryProcessShippingComponent;
}());



/***/ })

}]);
//# sourceMappingURL=modules-delivery-process-delivery-process-module.js.map