(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~d2607944"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryallhistory/detail/orderhistoryallhistory.detail.component.html":
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/orderhistoryallhistory/detail/orderhistoryallhistory.detail.component.html ***!
  \*************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <app-page-header *ngIf=\"orderhistoryDetail\" [heading]=\"'Order Detail for '+ orderhistoryDetail.order_id\" [icon]=\"'fa-table'\"></app-page-header>\r\n    <div *ngIf=\"orderhistoryDetail\" class=\"detail\">\r\n        <app-orderhistoryallhistory-edit [back]=\"[this, 'editThis']\" [detail]=\"orderhistoryDetail\">\r\n        </app-orderhistoryallhistory-edit>\r\n    </div>\r\n</div>\r\n    <!-- <div *ngIf=\"!edit\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button> \r\n    </div> \r\n    <div class=\"card-content\">\r\n        <div class=\"orderhistoryallhistory-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 \r\n                 \r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Shipping Info</h2> </div>\r\n                        <div class=\"card-content\">\r\n                                <label>Reciever Name</label>\r\n                                <div name=\"user\">{{detail.buyer_detail.name}}</div>\r\n                                <label>Cell Phone</label>\r\n                                <div name=\"cell_phone\">{{detail.buyer_detail.cell_phone}}</div>\r\n                                <label>Order ID</label>\r\n                                <div name=\"order_id\" >{{detail.order_id}}</div>\r\n                                <label>Address</label>\r\n                                <div name=\"Address\">{{detail.buyer_detail.address}}</div>\r\n                            <div class=\"col-md-12\" *ngIf=\"!detail.shipping_info\">\r\n                                no shipping info\r\n                            </div>\r\n                            <div class=\"col-md-12\" *ngIf=\"detail.shipping_info\">\r\n                                    <ul>\r\n                                        <li *ngFor=\"let si of detail.shipping_info\">\r\n                                                <label>{{si.label}} - {{si.created_date}}</label>\r\n                                        </li>\r\n                                        <label>Awb Receipt</label>\r\n                                        <div name=\"detail.awb_receipt\" >{{detail.awb_receipt}}</div> \r\n                                        <label>Shipping Services</label>\r\n                                        <div name=\"detail.shipping_services\" >{{detail.shipping_services}}</div>\r\n                                    </ul>\r\n                            </div>\r\n                        \r\n                        </div> \r\n                    </div>\r\n                 \r\n                </div>\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"orderhistoryallhistory-detail\">\r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> Order Summary </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                            <label>Product List</label>\r\n                            <table>\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th>Product Code</th>\r\n                                        <th>Product Name</th>\r\n                                        <th>Quantity</th>\r\n                                        <th>Price</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr *ngFor=\"let dp of detail.products\">\r\n                                            <td>{{dp.product_code}}</td>\r\n                                            <td>{{dp.product_name}} @ {{dp.sku_value}}</td>\r\n                                            <td>{{dp.quantity}}</td>\r\n                                            <td>{{dp.total_product_price}}</td>\r\n                                    </tr>\r\n                                    <tr *ngFor=\"let bd of detail.additionalBillings\">\r\n                                            <td>{{bd.product_name}}</td>\r\n                                            <td></td>\r\n                                            <td></td>\r\n                               emeb             <td>{{bd.value}}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td colspan=\"3\">Total Price</td>\r\n                                        <td><div name=\"sum_total\" >{{detail.sum_total}}</div></td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                            <br/>\r\n\r\n                            <table>\r\n                                <col style=\"width: 78%\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th>Shipping Cost</th>\r\n                                        <th>Price</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <td>Shipping Cost</td>\r\n                                        <td>{{detail.billings.shipping_fee}}</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                            <br/>\r\n\r\n                            <table>\r\n                                <col style=\"width: 78%\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th>Unique Amount</th>\r\n                                        <th>Amount</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <td>Unique Amount</td>\r\n                                        <td>{{detail.unique_amount}}</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                            <br/>\r\n\r\n                            <table>\r\n                                <col style =\"width:78%\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th> Total Price </th>\r\n                                        <th> Total </th>\r\n                                    </tr>\r\n                                </thead>\r\n\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <td>Total Price</td>\r\n                                        <td>{{detail.sum_total}}</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                            \r\n                         </div>\r\n                      </div>\r\n                 </div>\r\n                </div>\r\n            </div>\r\n         </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-orderhistoryallhistory-edit [back]=\"[this, 'editThis']\" [detail]=\"detail\"></app-orderhistoryallhistory-edit>\r\n</div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.component.html ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n  <app-page-header [heading]=\"'Order History All History'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n  <div *ngIf=\"Orderhistoryallhistory && orderhistoryallhistoryDetail==false && mci_project == false && programType!='custom_kontraktual' && !errorMessage\">\r\n        <app-form-builder-table \r\n        [table_data]=\"Orderhistoryallhistory\"\r\n        [searchCallback]=\"[service, 'searchAllreportSalesOrder',this]\" \r\n        [tableFormat]=\"tableFormat\"\r\n        [total_page]=\"totalPage\"\r\n        >\r\n        </app-form-builder-table>\r\n  </div>\r\n\r\n  <div *ngIf=\"Orderhistoryallhistory && orderhistoryallhistoryDetail==false && mci_project == true && programType!='custom_kontraktual' && !errorMessage\">\r\n      <app-form-builder-table \r\n      [table_data]=\"Orderhistoryallhistory\"\r\n      [searchCallback]=\"[service, 'searchAllreportSalesOrder',this]\" \r\n      [tableFormat]=\"tableFormat2\"\r\n      [total_page]=\"totalPage\"\r\n      >\r\n      </app-form-builder-table>\r\n</div>\r\n\r\n<div *ngIf=\"Orderhistoryallhistory && orderhistoryallhistoryDetail==false && mci_project == false && programType=='custom_kontraktual' && !errorMessage\">\r\n      <app-form-builder-table \r\n      [table_data]=\"Orderhistoryallhistory\"\r\n      [searchCallback]=\"[service, 'searchAllreportSalesOrder',this]\" \r\n      [tableFormat]=\"tableFormat3\"\r\n      [total_page]=\"totalPage\"\r\n      >\r\n      </app-form-builder-table>\r\n</div>\r\n\r\n      <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n            <div class=\"text-message\">\r\n            <i class=\"fas fa-ban\"></i>\r\n            {{errorMessage}}\r\n            </div>\r\n      </div>\r\n\r\n      <div *ngIf=\"orderhistoryallhistoryDetail\">\r\n            <app-orderhistoryallhistory-edit [back]=\"[this,backToHere]\" [detail]=\"orderhistoryallhistoryDetail\"></app-orderhistoryallhistory-edit>\r\n      </div>\r\n\r\n  </div>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryallhistory/receipt/receipt.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/orderhistoryallhistory/receipt/receipt.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- \r\n<div class=\"container\" *ngIf=\"detail\">\r\n    <div class=\"logo\">\r\n\r\n    </div>\r\n    <div class=\"awb\">\r\n    <ngx-barcode [bc-value]=\"detail.order_id\" [bc-display-value]=\"true\"></ngx-barcode>\r\n    </div>\r\n\r\n    <div class=\"layanan\">\r\n        <p> <strong>Jenis Layanan: {{detail.shipping.service}}</strong></p>\r\n    </div>\r\n\r\n    <table align=\"center\" width=\"400\">\r\n        <tr>\r\n            <th>Penerima</th>\r\n            <th>Pengirim</th>\r\n        </tr>\r\n        <tr>\r\n            <td>{{detail.buyer_detail.name}}</td>\r\n            <td>{{detail.buyer_detail.name}}</td>\r\n            \r\n        </tr>\r\n        <tr>\r\n            <td>{{detail.buyer_detail.address}}</td>\r\n            <td>{{detail.buyer_detail.address}}</td>\r\n            \r\n        </tr>\r\n        <tr>\r\n            <td>{{detail.buyer_detail.cell_phone}}</td>\r\n            <td>{{detail.buyer_detail.cell_phone}}</td>\r\n            \r\n        </tr>\r\n\r\n    </table>\r\n    <br/>\r\n\r\n    <div class=\"produk\">\r\n        <table>\r\n            <tr>\r\n                <th>Nama Produk</th>\r\n                <th>Quantity</th>\r\n            </tr>\r\n            <tr *ngFor=\"let b of detail.products\">\r\n                <td *ngIf=\"b.type =='product'\"></td>\r\n            </tr>\r\n        </table>\r\n    </div>\r\n    \r\n</div> -->\r\n\r\n<!-- \r\n<app-page-header class=\"custom-title\" [heading]=\"'Shipping Label'\" [icon]=\"'fa-table'\"></app-page-header> -->\r\n<div class=\"header-shipping\">\r\n    <h3 class=\"custom-title\">Shipping Label</h3>\r\n    <button mat-raised-button color=\"primary\" (click)=\"printPage()\">Print Label<i class=\"fa fa-print\"\r\n            aria-hidden=\"true\"></i></button>\r\n</div>\r\n<div class=\"_container\" *ngIf=\"detail\">\r\n    <div class=\"content\">\r\n        <!-- <div class=\"barcode\">\r\n            <h1>AWB :</h1>\r\n            <ngx-barcode [bc-value]=\"detail.order_id\" [bc-display-value]=\"true\"></ngx-barcode>\r\n        </div>\r\n        <div class=\"layanan\">\r\n            <p><strong>Jenis Layanan : {{detail.shipping.service}} </strong></p>\r\n        </div> -->\r\n        <br />\r\n        <div class=\"table-order\">\r\n            <table class=\"header\" align=\"center\">\r\n                <tr>\r\n                    <td class=\"locard-logo\">\r\n                        <div class=\"row logo-row\">\r\n                            <img src=\"assets/images/logo.png\" class=\"logo-avatar\" />\r\n                        </div>\r\n                        <div class=\"row logo-row\">\r\n                            <img src=\"assets/images/jne_logo.png\" class=\"logo-avatar\" />\r\n                        </div>\r\n\r\n                    </td>\r\n                    <td class=\"awb\">\r\n                        <p>AWB</p>\r\n                        <ngx-barcode [bc-value]=\"541820003372318\" [bc-display-value]=\"true\" [bc-width]=\"1\"\r\n                            [bc-font-size]=\"10\" [bc-height]=\"60\">\r\n                        </ngx-barcode>\r\n                    </td>\r\n                    <td class=\"courier-logo\">\r\n                        <p>Invoice ID:</p>\r\n                        <ngx-barcode [bc-value]=\"detail.order_id\" [bc-display-value]=\"true\" [bc-width]=\"1\" [bc-font-size]=\"10\"\r\n                            [bc-height]=\"60\">\r\n                        </ngx-barcode>\r\n                    </td>\r\n                </tr>\r\n            </table>\r\n            \r\n            <table align=\"center\" class=\"detail-header\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-9 detail-person\">\r\n                        <div class=\"row sender\">\r\n                            <div class=\"col-md-6\">\r\n                                <p><strong>Pengirim: </strong><br>\r\n                                    {{detail.products[0].merchant_username}}<br>\r\n                                    Jl. Mangga RT.007 RW.006 <br>\r\n                                    Kel. Lurahan entah dimana <br>\r\n                                    Bandung Selatan <br>\r\n                                    081282091022<br>\r\n                                </p>\r\n                            </div>\r\n                            <div class=\"col-md-6\">\r\n                                <p><strong>Penerima : </strong><br>\r\n                                    {{detail.buyer_detail.name}}<br>\r\n                                    {{detail.buyer_detail.address}}<br>\r\n                                    {{detail.buyer_detail.cell_phone}}<br>\r\n                                </p>\r\n                            </div>\r\n\r\n                        </div>\r\n                        <div class=\"receiver\"></div>\r\n                        <!-- <div class=\"row\">\r\n                            <p><strong>Penerima : </strong><br>\r\n                                {{detail.buyer_detail.name}}<br>\r\n                                {{detail.buyer_detail.address}}<br>\r\n                                {{detail.buyer_detail.cell_phone}}<br>\r\n                            </p>\r\n                        </div> -->\r\n                        <div class=\"col-md-12\">\r\n                            <strong>Daftar Barang: </strong><br>\r\n                            <p>\r\n                                <span *ngFor=\"let b of detail.products\">\r\n                                    {{b.product_name}}({{b.quantity}}) ,\r\n                                </span>\r\n                            </p>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-3 detail-order\">\r\n                        <p>\r\n                            <strong>Tanggal: </strong><br>\r\n                            {{detail.shipping_info[0].created_date}}<br><br>\r\n                            <strong>Kota Asal: </strong><br>\r\n                            Bandung<br><br>\r\n                            <strong>Kota Tujuan: </strong><br>\r\n                            {{detail.buyer_detail.region_name}}<br><br>\r\n                            <strong>Jumlah: </strong><br>\r\n                            {{detail.total_quantity}}<br><br>\r\n                            <strong>Berat Total: </strong><br>\r\n                            <span *ngFor=\"let bt of detail.products\">{{bt.weight}} Kg</span> <br>\r\n                        </p>\r\n                    </div>\r\n                </div>\r\n            </table>\r\n            <table align=\"center\" class=\"detail-barang-pengiriman\">\r\n                <div class=\"row row-eq-height\">\r\n                    <div class=\"col-md-8 detail-barang\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-6 daftar-barang\">\r\n                                <p id=\"thanks\">Terima kasih sudah berbelanja di LOCARD!</p>\r\n                            </div>\r\n                            <div class=\"col-md-6 catatan\">\r\n                                <strong>Catatan: </strong><br>\r\n                                {{detail.products[0].note_to_merchant}}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-4 detail-pengiriman\">\r\n                        <div class=\"col-md-6 jenis-pengiriman\">\r\n                            <p>Shipping Service: </p> <br>\r\n                            <p>{{detail.shipping.service}}</p>\r\n                        </div>\r\n                        <!-- <div class=\"col-md-6 total-harga\">\r\n                                <p>Total Price: </p> <br>\r\n                                <p>{{rupiah}}</p>\r\n                            </div> -->\r\n                            </div>  \r\n                    </div>\r\n                    </table>\r\n            \r\n        </div>\r\n        <p class=\"page-number\">\r\n            Page 1 of 1\r\n        </p>\r\n        </div>\r\n    </div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<!-- <div class=\"container\" *ngIf=\"detail\">\r\n        <table>\r\n            <div class=\"image-header\">\r\n            <tr>\r\n                <td><img src=\"../../../../../../img/logo-main.png\"></td>\r\n                <td><img src=\"../../../../../../img/LO+@3x-8.png\"></td>\r\n            </tr>\r\n        </div>\r\n        <tr>\r\n            <div class=\"barcode\">\r\n                <tr>\r\n                   <td colspan=\"2\"> <ngx-barcode [bc-value]=\"detail.order_id\" [bc-display-value]=\"true\"> </ngx-barcode></td>\r\n                </tr>\r\n            </div>\r\n        </tr>\r\n        <table>\r\n            <tr>\r\n                <td><b><span style=\"text-align: center;\">Jenis Layanan : {{detail.shipping.service}}</span></b></td>\r\n            </tr>\r\n        </table>\r\n        <table>\r\n            <tr>\r\n                <div class=\"barcode-2\">\r\n                <td rowspan=\"3\" style=\"text-align: center;\"><ngx-barcode [bc-value]=\"detail.order_id\" [bc-display-value]=\"true\" [bc-width]=\"1\" [bc-font-size]=\"12\"></ngx-barcode></td>\r\n            </div>\r\n            </tr>\r\n    \r\n            <tr>\r\n                <td style=\"text-align: center;\">Quantity : {{detail.total_quantity}} Pcs.</td>\r\n            </tr>\r\n            \r\n        </table>\r\n        <div class=\"table-penerima\">\r\n        <table>\r\n            <thead>\r\n                <th style=\"text-align: left;\">Penerima</th>\r\n                <th style=\"text-align: left;\">Pengirim</th>\r\n            </thead>\r\n            <tr>\r\n                <td style=\"text-align:left;\">{{detail.buyer_detail.name}}</td>\r\n                <td  style=\"text-align:left;\">E-Corp Shop</td>\r\n            </tr>\r\n            <tr>\r\n                <td  style=\"text-align:left;\">{{detail.buyer_detail.address}}</td>\r\n                <td  style=\"text-align:left;\">Jl. Rempoa</td>\r\n            </tr>\r\n            <tr>\r\n                <td  style=\"text-align:left;\">{{detail.buyer_detail.cell_phone}}</td>\r\n                <td  style=\"text-align:left;\">0813291398</td>\r\n            </tr>\r\n        </table>\r\n    </div>\r\n        <table>\r\n            <thead>\r\n                <th>Jenis Barang: </th>\r\n            </thead>\r\n            <tr *ngFor=\"let b of detail.products\">\r\n                <td *ngIf=\"b.type == 'product'\"><b> - {{b.product_name}}</b></td>\r\n            </tr>\r\n        </table>\r\n        <table>\r\n            <thead>\r\n                <th>Note To Merchant : </th>\r\n            </thead>\r\n            <tr *ngFor=\"let n of detail.products\">\r\n                <td><b>{{n.note_to_merchant}}</b></td>\r\n            </tr>\r\n        </table>\r\n    </table>\r\n    </div>\r\n    \r\n    \r\n -->"

/***/ }),

/***/ "./src/app/layout/modules/orderhistoryallhistory/detail/orderhistoryallhistory.detail.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryallhistory/detail/orderhistoryallhistory.detail.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -5px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 16px;\n}\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .orderhistoryallhistory-detail label {\n  margin-top: 10px;\n}\n.card-detail .orderhistoryallhistory-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .orderhistoryallhistory-detail .card-header {\n  background-color: #555;\n}\n.card-detail .orderhistoryallhistory-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\ntable {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n}\ntable thead {\n  background-color: #555;\n}\ntable thead th {\n  color: white;\n  padding: 10px;\n  border: 1px solid #ccc;\n}\ntable tbody td {\n  padding: 10px;\n  border: 1px solid #ccc;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvb3JkZXJoaXN0b3J5YWxsaGlzdG9yeS9kZXRhaWwvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxvcmRlcmhpc3RvcnlhbGxoaXN0b3J5XFxkZXRhaWxcXG9yZGVyaGlzdG9yeWFsbGhpc3RvcnkuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9vcmRlcmhpc3RvcnlhbGxoaXN0b3J5L2RldGFpbC9vcmRlcmhpc3RvcnlhbGxoaXN0b3J5LmRldGFpbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHSTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7QUNGUjtBREdRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQVhZO0VBWVosWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0ZaO0FER1k7RUFDSSxpQkFBQTtBQ0RoQjtBRElRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0Esc0JBMUJZO0VBMkJaLFdBQUE7RUFDQSxZQUFBO0VBRUEsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUtBLFdBQUE7QUNQWjtBRElZO0VBQ0ksaUJBQUE7QUNGaEI7QURPSTtFQUNJLGFBQUE7QUNMUjtBRE1RO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0paO0FEbUJJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0EvRGdCO0VBZ0VoQixrQkFBQTtFQUNBLGtCQUFBO0FDakJSO0FEb0JRO0VBQ0ksZ0JBQUE7QUNsQlo7QURvQlE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDbEJaO0FEcUJRO0VBQ0ksc0JBbkZZO0FDZ0V4QjtBRG9CWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDbEJoQjtBRHdCQTtFQUNJLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0FDckJKO0FEc0JJO0VBQ0ksc0JBckdnQjtBQ2lGeEI7QURxQlE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0FDbkJaO0FEeUJRO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0FDdkJaIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvb3JkZXJoaXN0b3J5YWxsaGlzdG9yeS9kZXRhaWwvb3JkZXJoaXN0b3J5YWxsaGlzdG9yeS5kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkYmFja2dyb3VuZENvbG9ySGVhZGVyOiAjNTU1O1xyXG5cclxuLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDhweDtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC01cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogJGJhY2tncm91bmRDb2xvckhlYWRlcjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuZWRpdF9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRiYWNrZ3JvdW5kQ29sb3JIZWFkZXI7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTZweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gLmltYWdle1xyXG4gICAgLy8gICAgIGgze1xyXG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAvLyAgICAgICAgIGltZ3tcclxuICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogJGJhY2tncm91bmRDb2xvckhlYWRlcjtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlse1xyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkYmFja2dyb3VuZENvbG9ySGVhZGVyO1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbnRhYmxle1xyXG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgdGhlYWR7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGJhY2tncm91bmRDb2xvckhlYWRlcjtcclxuICAgICAgICB0aHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBwYWRkaW5nOjEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcblxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0Ym9keXtcclxuICAgICAgICB0ZHtcclxuICAgICAgICAgICAgcGFkZGluZzoxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59IiwiLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiA0OHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC01cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDE2cHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5YWxsaGlzdG9yeS1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWFsbGhpc3RvcnktZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxudGFibGUge1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICB3aWR0aDogMTAwJTtcbn1cbnRhYmxlIHRoZWFkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbnRhYmxlIHRoZWFkIHRoIHtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxudGFibGUgdGJvZHkgdGQge1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/orderhistoryallhistory/detail/orderhistoryallhistory.detail.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryallhistory/detail/orderhistoryallhistory.detail.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: OrderhistoryallhistoryDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryallhistoryDetailComponent", function() { return OrderhistoryallhistoryDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var OrderhistoryallhistoryDetailComponent = /** @class */ (function () {
    function OrderhistoryallhistoryDetailComponent(orderhistoryService, route, router) {
        this.orderhistoryService = orderhistoryService;
        this.route = route;
        this.router = router;
    }
    OrderhistoryallhistoryDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderhistoryallhistoryDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log("detail sales order", this.detail);
                this.orderhistoryDetail = JSON.parse(JSON.stringify(this.detail));
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryallhistoryDetailComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderhistoryallhistoryDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderhistoryallhistoryDetailComponent.prototype, "back", void 0);
    OrderhistoryallhistoryDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-orderhistoryallhistory-detail',
            template: __webpack_require__(/*! raw-loader!./orderhistoryallhistory.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryallhistory/detail/orderhistoryallhistory.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./orderhistoryallhistory.detail.component.scss */ "./src/app/layout/modules/orderhistoryallhistory/detail/orderhistoryallhistory.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], OrderhistoryallhistoryDetailComponent);
    return OrderhistoryallhistoryDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory-routing.module.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory-routing.module.ts ***!
  \************************************************************************************************/
/*! exports provided: OrderhistoryallhistoryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryallhistoryRoutingModule", function() { return OrderhistoryallhistoryRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _orderhistoryallhistory_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orderhistoryallhistory.component */ "./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.component.ts");
/* harmony import */ var _detail_orderhistoryallhistory_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detail/orderhistoryallhistory.detail.component */ "./src/app/layout/modules/orderhistoryallhistory/detail/orderhistoryallhistory.detail.component.ts");
/* harmony import */ var _receipt_receipt_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./receipt/receipt.component */ "./src/app/layout/modules/orderhistoryallhistory/receipt/receipt.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';
var routes = [
    {
        path: '', component: _orderhistoryallhistory_component__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryallhistoryComponent"]
    },
    // {
    //   path:'detail', component: OrderhistorysummaryDetailComponent
    // },
    {
        path: 'edit', component: _detail_orderhistoryallhistory_detail_component__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryallhistoryDetailComponent"]
    },
    {
        path: 'receipt', component: _receipt_receipt_component__WEBPACK_IMPORTED_MODULE_4__["ReceiptComponent"]
    }
];
var OrderhistoryallhistoryRoutingModule = /** @class */ (function () {
    function OrderhistoryallhistoryRoutingModule() {
    }
    OrderhistoryallhistoryRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OrderhistoryallhistoryRoutingModule);
    return OrderhistoryallhistoryRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvb3JkZXJoaXN0b3J5YWxsaGlzdG9yeS9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXG9yZGVyaGlzdG9yeWFsbGhpc3RvcnlcXG9yZGVyaGlzdG9yeWFsbGhpc3RvcnkuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL29yZGVyaGlzdG9yeWFsbGhpc3Rvcnkvb3JkZXJoaXN0b3J5YWxsaGlzdG9yeS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDQ0o7QURDSTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUNDUiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL29yZGVyaGlzdG9yeWFsbGhpc3Rvcnkvb3JkZXJoaXN0b3J5YWxsaGlzdG9yeS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn0iLCIuZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: OrderhistoryallhistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryallhistoryComponent", function() { return OrderhistoryallhistoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_program_name_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/program-name.service */ "./src/app/services/program-name.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_6___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






// import { Router } from '@angular/router';

var OrderhistoryallhistoryComponent = /** @class */ (function () {
    function OrderhistoryallhistoryComponent(orderhistoryService, activatedRoute, router, programNameService) {
        this.orderhistoryService = orderhistoryService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.programNameService = programNameService;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_6__;
        this.Orderhistoryallhistory = [];
        this.tableFormat = {
            title: 'Order All History Detail Page',
            label_headers: [
                { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Toko', visible: true, type: 'string', data_row_name: 'username' },
                { label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            ],
            row_primary_key: 'order_id',
            formOptions: {
                row_id: 'order_id',
                this: this,
                result_var_name: 'Orderhistoryallhistory',
                detail_function: [this, 'callDetail'],
            },
            show_checkbox_options: true
        };
        this.tableFormat2 = {
            title: 'Order All History Detail Page',
            label_headers: [
                { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'Nomor PO', visible: true, type: 'value_po', data_row_name: 'po_no' },
                { label: 'ID Bisnis', visible: true, type: 'form_business_id', data_row_name: 'member_detail' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
                { label: 'Nama Pelanggan', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            ],
            row_primary_key: 'order_id',
            formOptions: {
                row_id: 'order_id',
                this: this,
                result_var_name: 'Orderhistoryallhistory',
                detail_function: [this, 'callDetail'],
            },
            show_checkbox_options: true
        };
        this.tableFormat3 = {
            title: 'Order All History Detail Page',
            label_headers: [
                { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
                { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
                { label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                { label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            ],
            row_primary_key: 'order_id',
            formOptions: {
                row_id: 'order_id',
                this: this,
                result_var_name: 'Orderhistoryallhistory',
                detail_function: [this, 'callDetail'],
            },
            show_checkbox_options: true
        };
        this.form_input = {};
        this.errorLabel = false;
        this.orderhistoryallhistoryDetail = false;
        this.mci_project = false;
        this.errorMessage = false;
        this.programType = "";
    }
    OrderhistoryallhistoryComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderhistoryallhistoryComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var paramId, paramAppLabel, resultDetail, program_1, _this_1, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        paramId = this.activatedRoute.snapshot.queryParamMap.get('order_id');
                        paramAppLabel = this.activatedRoute.snapshot.queryParamMap.get('app_label');
                        if (!(paramId && paramAppLabel)) return [3 /*break*/, 2];
                        // localStorage.setItem('programName', paramAppLabel);
                        // this.programNameService.programName$.next(paramAppLabel);
                        this.programNameService.setData(paramAppLabel);
                        return [4 /*yield*/, this.orderhistoryService.getOrderDetail(paramId)];
                    case 1:
                        resultDetail = _a.sent();
                        if (resultDetail && resultDetail.order_id)
                            this.orderhistoryallhistoryDetail = resultDetail;
                        _a.label = 2;
                    case 2:
                        program_1 = localStorage.getItem('programName');
                        _this_1 = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program_1) {
                                if (element.type == "reguler") {
                                    _this_1.mci_project = true;
                                    _this_1.programType = "reguler";
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this_1.mci_project = false;
                                    _this_1.programType = "custom_kontraktual";
                                }
                                else {
                                    _this_1.mci_project = false;
                                    _this_1.programType = "custom";
                                }
                            }
                        });
                        this.service = this.orderhistoryService;
                        return [4 /*yield*/, this.orderhistoryService.getAllreportSalesOrder()];
                    case 3:
                        result = _a.sent();
                        this.totalPage = result.total_page;
                        this.Orderhistoryallhistory = result.values;
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); // conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    OrderhistoryallhistoryComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        // this.orderhistoryallhistoryDetail = this.Orderhistoryallhistory.find(order => order.order_id == rowData.order_id);
                        _a = this;
                        return [4 /*yield*/, this.orderhistoryService.getOrderDetail(rowData.order_id)];
                    case 1:
                        // this.orderhistoryallhistoryDetail = this.Orderhistoryallhistory.find(order => order.order_id == rowData.order_id);
                        _a.orderhistoryallhistoryDetail = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    OrderhistoryallhistoryComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.orderhistoryallhistoryDetail = false;
                obj.router.navigate([]).then(function () {
                    obj.firstLoad();
                });
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryallhistoryComponent.prototype.processSelectedOrders = function (obj, orders) {
        return __awaiter(this, void 0, void 0, function () {
            var _this_1 = this;
            return __generator(this, function (_a) {
                if (orders.length > 0) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                        title: 'Confirmation',
                        text: 'Apakah anda yakin ingin memproses semua order yang dipilih',
                        icon: 'success',
                        confirmButtonText: 'process',
                        cancelButtonText: "cancel",
                        showCancelButton: true
                    }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
                        var _this_1 = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!result.isConfirmed) return [3 /*break*/, 2];
                                    return [4 /*yield*/, orders.forEach(function (order_id) { return __awaiter(_this_1, void 0, void 0, function () {
                                            var payload, e_2;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        payload = {
                                                            order_id: order_id
                                                        };
                                                        _a.label = 1;
                                                    case 1:
                                                        _a.trys.push([1, 3, , 4]);
                                                        return [4 /*yield*/, obj.orderhistoryService.processOrder(payload)];
                                                    case 2:
                                                        _a.sent();
                                                        return [3 /*break*/, 4];
                                                    case 3:
                                                        e_2 = _a.sent();
                                                        return [3 /*break*/, 4];
                                                    case 4:
                                                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire('Success', 'Semua order terpilih telah disetujui', 'success');
                                                        return [2 /*return*/];
                                                }
                                            });
                                        }); })];
                                case 1:
                                    _a.sent();
                                    _a.label = 2;
                                case 2: return [2 /*return*/];
                            }
                        });
                    }); });
                }
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryallhistoryComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _services_program_name_service__WEBPACK_IMPORTED_MODULE_5__["ProgramNameService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderhistoryallhistoryComponent.prototype, "back", void 0);
    OrderhistoryallhistoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-orderhistoryallhistory',
            template: __webpack_require__(/*! raw-loader!./orderhistoryallhistory.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./orderhistoryallhistory.component.scss */ "./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _services_program_name_service__WEBPACK_IMPORTED_MODULE_5__["ProgramNameService"]])
    ], OrderhistoryallhistoryComponent);
    return OrderhistoryallhistoryComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.module.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.module.ts ***!
  \****************************************************************************************/
/*! exports provided: OrderhistoryallhistoryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryallhistoryModule", function() { return OrderhistoryallhistoryModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _orderhistoryallhistory_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orderhistoryallhistory-routing.module */ "./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory-routing.module.ts");
/* harmony import */ var _orderhistoryallhistory_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./orderhistoryallhistory.component */ "./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _detail_orderhistoryallhistory_detail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./detail/orderhistoryallhistory.detail.component */ "./src/app/layout/modules/orderhistoryallhistory/detail/orderhistoryallhistory.detail.component.ts");
/* harmony import */ var _orderhistoryallhistory_edit_orderhistoryallhistory_edit_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../orderhistoryallhistory/edit/orderhistoryallhistory.edit.module */ "./src/app/layout/modules/orderhistoryallhistory/edit/orderhistoryallhistory.edit.module.ts");
/* harmony import */ var _receipt_receipt_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./receipt/receipt.component */ "./src/app/layout/modules/orderhistoryallhistory/receipt/receipt.component.ts");
/* harmony import */ var ngx_barcode__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-barcode */ "./node_modules/ngx-barcode/index.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/esm5/radio.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










// import { OrderhistoryallhistoryEditComponent } from './edit/orderhistoryallhistory.edit.component';





var OrderhistoryallhistoryModule = /** @class */ (function () {
    function OrderhistoryallhistoryModule() {
    }
    OrderhistoryallhistoryModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _orderhistoryallhistory_routing_module__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryallhistoryRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                ngx_barcode__WEBPACK_IMPORTED_MODULE_12__["NgxBarcodeModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_13__["MatButtonModule"],
                _angular_material_radio__WEBPACK_IMPORTED_MODULE_14__["MatRadioModule"],
                _orderhistoryallhistory_edit_orderhistoryallhistory_edit_module__WEBPACK_IMPORTED_MODULE_10__["OrderhistoryallhistoryEditModule"]
            ],
            declarations: [
                _orderhistoryallhistory_component__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryallhistoryComponent"],
                _detail_orderhistoryallhistory_detail_component__WEBPACK_IMPORTED_MODULE_9__["OrderhistoryallhistoryDetailComponent"],
                // OrderhistoryallhistoryEditComponent,
                _receipt_receipt_component__WEBPACK_IMPORTED_MODULE_11__["ReceiptComponent"]
            ],
            exports: [
                _orderhistoryallhistory_component__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryallhistoryComponent"],
                _detail_orderhistoryallhistory_detail_component__WEBPACK_IMPORTED_MODULE_9__["OrderhistoryallhistoryDetailComponent"],
            ]
        })
    ], OrderhistoryallhistoryModule);
    return OrderhistoryallhistoryModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistoryallhistory/receipt/receipt.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryallhistory/receipt/receipt.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media print {\n  @page {\n    size: landscape;\n  }\n  h5 {\n    display: none;\n  }\n\n  app-page-header {\n    display: none;\n  }\n\n  button {\n    display: none;\n  }\n\n  * {\n    font-size: 8px;\n  }\n\n  .detail-barang-pengiriman {\n    page-break-after: always;\n  }\n}\nbody {\n  background-color: #333;\n  font-family: \"Poppins\";\n  color: #333;\n  text-align: left;\n  font-size: 11px;\n  margin: 0;\n}\n.mat-raised-button.mat-primary {\n  background-color: #2481fb;\n  float: right;\n  padding: 5px 20px;\n  font-family: \"Poppins\";\n  margin: 10px 40px;\n  border-radius: 5px;\n}\n.mat-raised-button.mat-primary .fa {\n  margin-left: 8px;\n}\n.header-shipping {\n  margin: 20px 20px 0px;\n  display: flex;\n  flex-direction: row;\n}\n.header-shipping .custom-title {\n  color: white;\n  padding-top: 1%;\n}\n.header-shipping button {\n  margin-left: 75%;\n}\n._container {\n  background-color: #fff;\n  margin: 0px;\n  margin-top: 2vw;\n  border-radius: 5px;\n  text-align: -webkit-center;\n}\n._container .content .layanan {\n  text-align: center;\n  margin-bottom: 30px;\n}\n._container .content .page-number {\n  text-align: center;\n  padding-bottom: 25px;\n  font-size: 18px;\n}\n._container .content .table-order {\n  margin-top: 50px;\n  padding-bottom: 60px;\n  width: 80%;\n  font-size: 11px;\n}\n._container .content .table-order .header {\n  table-layout: auto;\n  border-bottom: 0;\n  width: 100%;\n}\n._container .content .table-order .detail-barang-pengiriman {\n  text-align: left;\n  width: 100%;\n}\n._container .content .table-order .detail-barang-pengiriman .row-eq-height {\n  display: flex;\n}\n._container .content .table-order .detail-barang-pengiriman .row {\n  margin-left: 0px;\n  height: 100%;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-barang {\n  border-right: solid black 1px;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-barang .daftar-barang {\n  padding-left: 0px;\n  border-right: solid black 1px;\n  padding-top: 15px;\n  padding-bottom: 20px;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-barang .catatan {\n  padding-top: 15px;\n  padding-bottom: 20px;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman .jenis-pengiriman {\n  padding-top: 15px;\n  padding-bottom: 14px;\n  padding-left: 0px;\n  font-weight: bold;\n  text-align: center;\n  border-right: solid black 1px;\n}\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman .total-harga {\n  padding-top: 15px;\n  padding-bottom: 30px;\n  text-align: center;\n  font-weight: bold;\n  padding-left: 0px;\n}\n._container .content .table-order .detail-header {\n  width: 100%;\n  text-align: left;\n  border-top: 0;\n  border-bottom: 0;\n}\n._container .content .table-order .detail-header .detail-person {\n  border-right: solid black 1px;\n  padding: 20px;\n}\n._container .content .table-order .detail-header .detail-person .receiver {\n  border-bottom: black solid 1px;\n  padding-bottom: 10px;\n  margin-bottom: 15px;\n  margin-top: 15px;\n}\n._container .content .table-order .detail-header .detail-order {\n  padding: 20px;\n}\n._container .content .table-order .detail-header .row {\n  margin-left: 0px;\n}\n._container .content .table-order .locard-logo {\n  text-align: center;\n}\n._container .content .table-order .locard-logo .logo-row {\n  margin: 5px auto;\n}\n._container .content .table-order .locard-logo .logo-row .logo-avatar {\n  border-radius: 0%;\n  width: 60px;\n  margin-left: auto;\n  margin-right: auto;\n}\n._container .content .table-order .courier-logo {\n  text-align: center;\n}\n._container .content .table-order #courier-logo {\n  text-align: center;\n}\n._container .content .table-order #courier-logo .logo-avatar {\n  width: 120px;\n}\n._container .content .table-order .awb {\n  text-align: center;\n}\n._container .content .table-order .order-id {\n  text-align: center;\n}\n._container .content .table-order .order-id p {\n  font-size: 20px;\n  font-weight: bold;\n}\ntable {\n  border: 2px solid #333;\n  border-collapse: collapse;\n}\ntd,\ntr,\nth {\n  padding: 12px;\n  border: 1px solid #333;\n  width: 0;\n}\nth {\n  background-color: #f0f0f0;\n}\nh4,\np {\n  margin: 0px;\n}\n#garis {\n  height: 200px;\n  color: black;\n}\n#thanks {\n  font-size: 12px;\n  text-align: center;\n  color: #0ea8db;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvb3JkZXJoaXN0b3J5YWxsaGlzdG9yeS9yZWNlaXB0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcb3JkZXJoaXN0b3J5YWxsaGlzdG9yeVxccmVjZWlwdFxccmVjZWlwdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvb3JkZXJoaXN0b3J5YWxsaGlzdG9yeS9yZWNlaXB0L3JlY2VpcHQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRTtJQUNFLGVBQUE7RUNDRjtFREdBO0lBQ0UsYUFBQTtFQ0RGOztFRElBO0lBQ0UsYUFBQTtFQ0RGOztFRElBO0lBQ0UsYUFBQTtFQ0RGOztFRElBO0lBQ0UsY0FBQTtFQ0RGOztFRG1CQTtJQUNFLHdCQUFBO0VDaEJGO0FBQ0Y7QUR1QkE7RUFDRSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7QUNyQkY7QUR1QkE7RUFDRSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ3BCRjtBRHNCRTtFQUNFLGdCQUFBO0FDcEJKO0FEd0JBO0VBQ0UscUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUNyQkY7QUR1QkU7RUFDRSxZQUFBO0VBQ0EsZUFBQTtBQ3JCSjtBRHdCRTtFQUNFLGdCQUFBO0FDdEJKO0FEMEJBO0VBQ0Usc0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMEJBQUE7QUN2QkY7QUQ2Qkk7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0FDM0JOO0FEOEJJO0VBQ0Usa0JBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7QUM1Qk47QUQrQkk7RUFDRSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUM3Qk47QUQrQk07RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQzdCUjtBRGdDTTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBQzlCUjtBRGdDUTtFQUlFLGFBQUE7QUM5QlY7QURpQ1E7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUMvQlY7QURrQ1E7RUFDRSw2QkFBQTtBQ2hDVjtBRGtDVTtFQUNFLGlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FDaENaO0FEbUNVO0VBQ0UsaUJBQUE7RUFDQSxvQkFBQTtBQ2pDWjtBRHNDVTtFQUNFLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFFQSw2QkFBQTtBQ3JDWjtBRHdDVTtFQUNFLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUN0Q1o7QUQyQ007RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUN6Q1I7QUQyQ1E7RUFDRSw2QkFBQTtFQUNBLGFBQUE7QUN6Q1Y7QUQyQ1U7RUFDRSw4QkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ3pDWjtBRGlEUTtFQUNFLGFBQUE7QUMvQ1Y7QURrRFE7RUFDRSxnQkFBQTtBQ2hEVjtBRG9ETTtFQUNFLGtCQUFBO0FDbERSO0FEbURRO0VBQ0UsZ0JBQUE7QUNqRFY7QURrRFU7RUFFRSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDaERaO0FEcURNO0VBQ0Usa0JBQUE7QUNuRFI7QURzRE07RUFDRSxrQkFBQTtBQ3BEUjtBRHNEUTtFQUNFLFlBQUE7QUNwRFY7QUR3RE07RUFDRSxrQkFBQTtBQ3REUjtBRHlETTtFQUNFLGtCQUFBO0FDdkRSO0FEd0RRO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FDdERWO0FENERBO0VBQ0Usc0JBQUE7RUFDQSx5QkFBQTtBQ3pERjtBRDJEQTs7O0VBR0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsUUFBQTtBQ3hERjtBRDBEQTtFQUNFLHlCQUFBO0FDdkRGO0FEeURBOztFQUVFLFdBQUE7QUN0REY7QUR5REE7RUFDRSxhQUFBO0VBQ0EsWUFBQTtBQ3RERjtBRHlEQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQ3RERiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL29yZGVyaGlzdG9yeWFsbGhpc3RvcnkvcmVjZWlwdC9yZWNlaXB0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQG1lZGlhIHByaW50IHtcclxuICBAcGFnZSB7XHJcbiAgICBzaXplOiBsYW5kc2NhcGU7XHJcbiAgICAvLyBtYXJnaW46IDVtbSA1bW0gNW1tIDVtbTtcclxuICB9XHJcblxyXG4gIGg1IHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBhcHAtcGFnZS1oZWFkZXIge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcblxyXG4gIGJ1dHRvbiB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgKiB7XHJcbiAgICBmb250LXNpemU6IDhweDtcclxuICAgIC8vIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gIH1cclxuXHJcbiAgLy8gLnRhYmxlLW9yZGVyIHtcclxuICAvLyAgIHdpZHRoOiAyMCU7XHJcbiAgLy8gfVxyXG5cclxuICAvLyB0YWJsZXtcclxuICAvLyAgIG1hcmdpbi10b3A6IDBweCAhaW1wb3J0YW50O1xyXG4gIC8vICAgdGFibGUtbGF5b3V0OiBhdXRvO1xyXG4gIC8vICAgd2lkdGg6IDYwJTtcclxuICAvLyAgIC8vIHdpZHRoOiA1NXJlbTtcclxuICAvLyB9XHJcbiAgLy8gLl9jb250YWluZXJ7XHJcbiAgLy8gICBtYXJnaW46IDA7XHJcbiAgLy8gICBwYWRkaW5nOiAwO1xyXG4gIC8vIH1cclxuICAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIHtcclxuICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGFsd2F5cztcclxuICB9XHJcblxyXG4gIC8vICN0aGFua3Mge1xyXG4gIC8vICAgZm9udC1zaXplOjVweDtcclxuICAvLyB9XHJcbn1cclxuXHJcbmJvZHkge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMzMzM7XHJcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xyXG4gIGNvbG9yOiAjMzMzO1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgZm9udC1zaXplOiAxMXB4O1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG4ubWF0LXJhaXNlZC1idXR0b24ubWF0LXByaW1hcnkge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyNDgxZmI7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG4gIHBhZGRpbmc6IDVweCAyMHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcclxuICBtYXJnaW46IDEwcHggNDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcblxyXG4gIC5mYSB7XHJcbiAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG4gIH1cclxufVxyXG5cclxuLmhlYWRlci1zaGlwcGluZyB7XHJcbiAgbWFyZ2luOiAyMHB4IDIwcHggMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuXHJcbiAgLmN1c3RvbS10aXRsZSB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLXRvcDogMSU7XHJcbiAgfVxyXG5cclxuICBidXR0b24ge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDc1JTtcclxuICB9XHJcbn1cclxuXHJcbi5fY29udGFpbmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gIG1hcmdpbjogMHB4O1xyXG4gIG1hcmdpbi10b3A6IDJ2dztcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XHJcblxyXG4gIC5jb250ZW50IHtcclxuICAgIC8vICoge1xyXG4gICAgLy8gICAvLyB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgIC8vIH1cclxuICAgIC5sYXlhbmFuIHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5wYWdlLW51bWJlciB7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDI1cHg7XHJcbiAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIH1cclxuXHJcbiAgICAudGFibGUtb3JkZXIge1xyXG4gICAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogNjBweDtcclxuICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG5cclxuICAgICAgLmhlYWRlciB7XHJcbiAgICAgICAgdGFibGUtbGF5b3V0OiBhdXRvO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDA7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4ge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgIC5yb3ctZXEtaGVpZ2h0IHtcclxuICAgICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gICAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLnJvdyB7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmRldGFpbC1iYXJhbmcge1xyXG4gICAgICAgICAgYm9yZGVyLXJpZ2h0OiBzb2xpZCBibGFjayAxcHg7XHJcblxyXG4gICAgICAgICAgLmRhZnRhci1iYXJhbmcge1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJpZ2h0OiBzb2xpZCBibGFjayAxcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAuY2F0YXRhbiB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5kZXRhaWwtcGVuZ2lyaW1hbiB7XHJcbiAgICAgICAgICAuamVuaXMtcGVuZ2lyaW1hbiB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIC8vIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgICAgIGJvcmRlci1yaWdodDogc29saWQgYmxhY2sgMXB4O1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIC50b3RhbC1oYXJnYSB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMzBweDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAuZGV0YWlsLWhlYWRlciB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICBib3JkZXItdG9wOiAwO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDA7XHJcblxyXG4gICAgICAgIC5kZXRhaWwtcGVyc29uIHtcclxuICAgICAgICAgIGJvcmRlci1yaWdodDogc29saWQgYmxhY2sgMXB4O1xyXG4gICAgICAgICAgcGFkZGluZzogMjBweDtcclxuXHJcbiAgICAgICAgICAucmVjZWl2ZXIge1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiBibGFjayBzb2xpZCAxcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIC8vIC5zZW5kZXIge1xyXG4gICAgICAgICAgLy8gICAvLyBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgICAgICAgIC8vIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5kZXRhaWwtb3JkZXIge1xyXG4gICAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5yb3cge1xyXG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5sb2NhcmQtbG9nbyB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIC5sb2dvLXJvdyB7XHJcbiAgICAgICAgICBtYXJnaW46IDVweCBhdXRvO1xyXG4gICAgICAgICAgLmxvZ28tYXZhdGFyIHtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDAlO1xyXG4gICAgICAgICAgICB3aWR0aDogNjBweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5jb3VyaWVyLWxvZ28ge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG5cclxuICAgICAgI2NvdXJpZXItbG9nbyB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgICAgICAubG9nby1hdmF0YXIge1xyXG4gICAgICAgICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLmF3YiB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAub3JkZXItaWQge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG50YWJsZSB7XHJcbiAgYm9yZGVyOiAycHggc29saWQgIzMzMztcclxuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG59XHJcbnRkLFxyXG50cixcclxudGgge1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgIzMzMztcclxuICB3aWR0aDogMDtcclxufVxyXG50aCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxufVxyXG5oNCxcclxucCB7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbn1cclxuXHJcbiNnYXJpcyB7XHJcbiAgaGVpZ2h0OiAyMDBweDtcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbiN0aGFua3Mge1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6ICMwZWE4ZGI7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuIiwiQG1lZGlhIHByaW50IHtcbiAgQHBhZ2Uge1xuICAgIHNpemU6IGxhbmRzY2FwZTtcbiAgfVxuICBoNSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIGFwcC1wYWdlLWhlYWRlciB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIGJ1dHRvbiB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gICoge1xuICAgIGZvbnQtc2l6ZTogOHB4O1xuICB9XG5cbiAgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiB7XG4gICAgcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xuICB9XG59XG5ib2R5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzMzMztcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xuICBjb2xvcjogIzMzMztcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBtYXJnaW46IDA7XG59XG5cbi5tYXQtcmFpc2VkLWJ1dHRvbi5tYXQtcHJpbWFyeSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNDgxZmI7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZzogNXB4IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcbiAgbWFyZ2luOiAxMHB4IDQwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cbi5tYXQtcmFpc2VkLWJ1dHRvbi5tYXQtcHJpbWFyeSAuZmEge1xuICBtYXJnaW4tbGVmdDogOHB4O1xufVxuXG4uaGVhZGVyLXNoaXBwaW5nIHtcbiAgbWFyZ2luOiAyMHB4IDIwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xufVxuLmhlYWRlci1zaGlwcGluZyAuY3VzdG9tLXRpdGxlIHtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nLXRvcDogMSU7XG59XG4uaGVhZGVyLXNoaXBwaW5nIGJ1dHRvbiB7XG4gIG1hcmdpbi1sZWZ0OiA3NSU7XG59XG5cbi5fY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgbWFyZ2luOiAwcHg7XG4gIG1hcmdpbi10b3A6IDJ2dztcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5sYXlhbmFuIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnBhZ2UtbnVtYmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWJvdHRvbTogMjVweDtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIHtcbiAgbWFyZ2luLXRvcDogNTBweDtcbiAgcGFkZGluZy1ib3R0b206IDYwcHg7XG4gIHdpZHRoOiA4MCU7XG4gIGZvbnQtc2l6ZTogMTFweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuaGVhZGVyIHtcbiAgdGFibGUtbGF5b3V0OiBhdXRvO1xuICBib3JkZXItYm90dG9tOiAwO1xuICB3aWR0aDogMTAwJTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgd2lkdGg6IDEwMCU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAucm93LWVxLWhlaWdodCB7XG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLnJvdyB7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5kZXRhaWwtYmFyYW5nIHtcbiAgYm9yZGVyLXJpZ2h0OiBzb2xpZCBibGFjayAxcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLWJhcmFuZyAuZGFmdGFyLWJhcmFuZyB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBib3JkZXItcmlnaHQ6IHNvbGlkIGJsYWNrIDFweDtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLmRldGFpbC1iYXJhbmcgLmNhdGF0YW4ge1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLXBlbmdpcmltYW4gLmplbmlzLXBlbmdpcmltYW4ge1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBib3JkZXItcmlnaHQ6IHNvbGlkIGJsYWNrIDFweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5kZXRhaWwtcGVuZ2lyaW1hbiAudG90YWwtaGFyZ2Ege1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDMwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGJvcmRlci10b3A6IDA7XG4gIGJvcmRlci1ib3R0b206IDA7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1oZWFkZXIgLmRldGFpbC1wZXJzb24ge1xuICBib3JkZXItcmlnaHQ6IHNvbGlkIGJsYWNrIDFweDtcbiAgcGFkZGluZzogMjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWhlYWRlciAuZGV0YWlsLXBlcnNvbiAucmVjZWl2ZXIge1xuICBib3JkZXItYm90dG9tOiBibGFjayBzb2xpZCAxcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIC5kZXRhaWwtb3JkZXIge1xuICBwYWRkaW5nOiAyMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIC5yb3cge1xuICBtYXJnaW4tbGVmdDogMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5sb2NhcmQtbG9nbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAubG9jYXJkLWxvZ28gLmxvZ28tcm93IHtcbiAgbWFyZ2luOiA1cHggYXV0bztcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAubG9jYXJkLWxvZ28gLmxvZ28tcm93IC5sb2dvLWF2YXRhciB7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3JkZXItcmFkaXVzOiAwJTtcbiAgd2lkdGg6IDYwcHg7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmNvdXJpZXItbG9nbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAjY291cmllci1sb2dvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyICNjb3VyaWVyLWxvZ28gLmxvZ28tYXZhdGFyIHtcbiAgd2lkdGg6IDEyMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5hd2Ige1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLm9yZGVyLWlkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5vcmRlci1pZCBwIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxudGFibGUge1xuICBib3JkZXI6IDJweCBzb2xpZCAjMzMzO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xufVxuXG50ZCxcbnRyLFxudGgge1xuICBwYWRkaW5nOiAxMnB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjMzMzO1xuICB3aWR0aDogMDtcbn1cblxudGgge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xufVxuXG5oNCxcbnAge1xuICBtYXJnaW46IDBweDtcbn1cblxuI2dhcmlzIHtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4jdGhhbmtzIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjMGVhOGRiO1xuICBmb250LXdlaWdodDogYm9sZDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/orderhistoryallhistory/receipt/receipt.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryallhistory/receipt/receipt.component.ts ***!
  \************************************************************************************/
/*! exports provided: ReceiptComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReceiptComponent", function() { return ReceiptComponent; });
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var ReceiptComponent = /** @class */ (function () {
    // dataList = order_id;
    function ReceiptComponent(OrderhistoryService, router, route) {
        this.OrderhistoryService = OrderhistoryService;
        this.router = router;
        this.route = route;
    }
    ReceiptComponent.prototype.ngOnInit = function () {
        this.firstLoad();
        // this.order_id();
        // this.hasil();
    };
    Object.defineProperty(ReceiptComponent.prototype, "values", {
        get: function () {
            return this.value.split('\n');
        },
        enumerable: true,
        configurable: true
    });
    ReceiptComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                    var historyID;
                    return __generator(this, function (_a) {
                        historyID = params.id;
                        this.loadDetail(historyID);
                        return [2 /*return*/];
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    ReceiptComponent.prototype.loadDetail = function (historyID) {
        return __awaiter(this, void 0, void 0, function () {
            var result, _a, e_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 3, , 4]);
                        this.service = this.OrderhistoryService;
                        return [4 /*yield*/, this.OrderhistoryService.detailOrderhistory(historyID)];
                    case 1:
                        result = _b.sent();
                        console.log("HASIL", result);
                        this.detail = result.result;
                        this.rupiah = this.detail.billings.sum_total;
                        console.log(this.rupiah);
                        _a = this;
                        return [4 /*yield*/, this.formatRupiah(this.rupiah, 'Rp ')];
                    case 2:
                        _a.rupiah = _b.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _b.sent();
                        this.errorLabel = (e_1.message);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ReceiptComponent.prototype.printPage = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                window.print();
                return [2 /*return*/];
            });
        });
    };
    /* Fungsi formatRupiah */
    ReceiptComponent.prototype.formatRupiah = function (angka, prefix) {
        return __awaiter(this, void 0, void 0, function () {
            var number_string, split, sisa, rupiah, ribuan, separator;
            return __generator(this, function (_a) {
                number_string = angka.toString().replace(/[^,\d]/g, ''), split = number_string.split(','), sisa = split[0].length % 3, rupiah = split[0].substr(0, sisa), ribuan = split[0].substr(sisa).match(/\d{3}/gi);
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if (ribuan) {
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return [2 /*return*/, prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '')];
            });
        });
    };
    ReceiptComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_0__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
    ]; };
    ReceiptComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-receipt',
            template: __webpack_require__(/*! raw-loader!./receipt.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryallhistory/receipt/receipt.component.html"),
            styles: [__webpack_require__(/*! ./receipt.component.scss */ "./src/app/layout/modules/orderhistoryallhistory/receipt/receipt.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_0__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], ReceiptComponent);
    return ReceiptComponent;
}());



/***/ })

}]);
//# sourceMappingURL=default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~d2607944.js.map