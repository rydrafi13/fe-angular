(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-notificationmessage-notificationmessage-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notificationmessage/add/notificationmessage.add.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/notificationmessage/add/notificationmessage.add.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'Add New Notification Message'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  \r\n <div>\r\n    <form #form=\"ngForm\" (ngSubmit)=\"formSubmitAddNotificationGroup(form.value)\">\r\n      <div class=\"col-md-6\">\r\n          <label>Title</label>\r\n          <form-input name=\"title\" [placeholder]=\"'Title'\"  [type]=\"'text'\" [(ngModel)]=\"title\" required></form-input>\r\n          <label>Message</label>\r\n          <form-input name=\"message\" [type]=\"'text'\" [placeholder]=\"'Message'\"  [(ngModel)]=\"message\" required></form-input>\r\n          <label>From</label>\r\n          <form-input name=\"from\" [type]=\"'text'\" [placeholder]=\"'From'\"  [(ngModel)]=\"from\" required></form-input>\r\n          <label>Url Logo</label>\r\n          <!-- <form-input name=\"url_logo\" [type]=\"'text'\" [placeholder]=\"'Url Logo'\"  [(ngModel)]=\"url_logo\" required></form-input> -->\r\n          <div class=\"col-md-3 col-sm-3\">\r\n            <div class=\"pb-framer\">\r\n                        <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n                 \r\n                        </div>\r\n                        <span *ngIf=\"progressBar<58&&progressBar>0\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                        <span class=\"clr-white\" *ngIf=\"progressBar>58\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                       \r\n                  </div>\r\n           \r\n      </div>\r\n      <div class=\"col-md-3 col-sm-3\">\r\n            <div *ngIf=\"errorFile\">Error: {{errorFile}}</div>                       \r\n      </div>\r\n      <!-- <img [src]=\"url\" height=\"200\"> <br/> -->\r\n      <input type=\"file\" (change)=\"onFileSelected($event)\" (change)=\"onSelectFile($event)\" />\r\n      <button (click)=\"onUpload()\">UPload</button>\r\n      <button (click)=\"cancelThis()\" >cancel</button>\r\n\r\n      <label>Url Web</label>\r\n          <!-- <form-input name=\"url_logo\" [type]=\"'text'\" [placeholder]=\"'Url Logo'\"  [(ngModel)]=\"url_logo\" required></form-input> -->\r\n          <div class=\"col-md-3 col-sm-3\">\r\n            <div class=\"pb-framer\">\r\n                        <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n                 \r\n                        </div>\r\n                        <span *ngIf=\"progressBar<58&&progressBar>0\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                        <span class=\"clr-white\" *ngIf=\"progressBar>58\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                       \r\n                  </div>\r\n           \r\n      </div>\r\n      <div class=\"col-md-3 col-sm-3\">\r\n            <div *ngIf=\"errorFile\">Error: {{errorFile}}</div>                       \r\n      </div>\r\n      <!-- <img [src]=\"url\" height=\"200\"> <br/> -->\r\n      <input type=\"file\" (change)=\"onFileSelected($event)\" (change)=\"onSelectFile($event)\" />\r\n      <button (click)=\"onUpload()\">UPload</button>\r\n      <button (click)=\"cancelThis()\" >cancel</button>\r\n         \r\n          <label>Status</label>\r\n          <form-select name=\"status\" [(ngModel)]=\"status\" [data]=\"notification_status\"></form-select>\r\n\r\n          <label>Icon</label>\r\n          <form-input name=\"icon\" [placeholder]=\"'Icon'\"  [type]=\"'text'\" [(ngModel)]=\"icon\" required></form-input>\r\n\r\n          <label>Message will expires in</label>\r\n          <form-input name=\"expiry_day\" [placeholder]=\"'Expiry Day'\"  [type]=\"'number'\" [(ngModel)]=\"expiry_day\" required></form-input>\r\n\r\n        </div>  \r\n      <button class=\"btn\" type=\"submit\" [disabled]=\"form.pristine\">Submit</button> \r\n      </form>\r\n </div>\r\n \r\n</div>\r\n\r\n "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notificationmessage/notificationmessage.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/notificationmessage/notificationmessage.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'Notification Message'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  \r\n  <div *ngIf=\"NotificationMessage&&notificationmessageDetail==false\">\r\n        <app-form-builder-table\r\n        [table_data]  = \"NotificationMessage\" \r\n        [searchCallback]= \"[service, 'searchNotificationMessageLint',this]\"\r\n        [tableFormat]=\"tableFormat\"\r\n        >\r\n            \r\n        </app-form-builder-table>\r\n  </div>\r\n  <!-- <div *ngIf=\"notificationmessageDetail\">\r\n    <app-notificationmessage-detail [back]=\"[this,backToHere]\" [detail]=\"notificationmessageDetail\"></app-notificationmessage-detail>\r\n</div> -->\r\n \r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/modules/notificationmessage/add/notificationmessage.add.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/modules/notificationmessage/add/notificationmessage.add.component.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n.pb-framer .clr-white {\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbm90aWZpY2F0aW9ubWVzc2FnZS9hZGQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxub3RpZmljYXRpb25tZXNzYWdlXFxhZGRcXG5vdGlmaWNhdGlvbm1lc3NhZ2UuYWRkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9ub3RpZmljYXRpb25tZXNzYWdlL2FkZC9ub3RpZmljYXRpb25tZXNzYWdlLmFkZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ0NKO0FEQUk7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ0VSO0FEQUk7RUFDSSxxQkFBQTtBQ0VSO0FEQUk7RUFDSSxZQUFBO0FDRVIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9ub3RpZmljYXRpb25tZXNzYWdlL2FkZC9ub3RpZmljYXRpb25tZXNzYWdlLmFkZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wYi1mcmFtZXJ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICBjb2xvcjogI2U2N2UyMjtcclxuICAgIC5wcm9ncmVzc2JhcntcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMzNDk4ZGI7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGhlaWdodDogMjVweDtcclxuICAgICAgICB6LWluZGV4OiAtMTtcclxuICAgIH1cclxuICAgIC50ZXJtaW5hdGVkLnByb2dyZXNzYmFye1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcclxuICAgIH1cclxuICAgIC5jbHItd2hpdGV7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG59IiwiLnBiLWZyYW1lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDI1cHg7XG4gIGNvbG9yOiAjZTY3ZTIyO1xufVxuLnBiLWZyYW1lciAucHJvZ3Jlc3NiYXIge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjVweDtcbiAgei1pbmRleDogLTE7XG59XG4ucGItZnJhbWVyIC50ZXJtaW5hdGVkLnByb2dyZXNzYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xufVxuLnBiLWZyYW1lciAuY2xyLXdoaXRlIHtcbiAgY29sb3I6IHdoaXRlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/notificationmessage/add/notificationmessage.add.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/notificationmessage/add/notificationmessage.add.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: NotificationMessageAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationMessageAddComponent", function() { return NotificationMessageAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/notification/notification.service */ "./src/app/services/notification/notification.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var NotificationMessageAddComponent = /** @class */ (function () {
    function NotificationMessageAddComponent(notificationService) {
        this.notificationService = notificationService;
        this.name = "";
        this.NotificationMessage = [];
        //   public notification_topic = [
        //     {label:"YES", value:"YES", selected:1}
        //    ,{label:"NO", value:"NO"}
        //  ]; 
        this.notification_status = [
            { label: "ACTIVE", value: "ACTIVE", selected: 1 },
            { label: "INACTIVE", value: "INACTIVE" }
        ];
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
        this.errorLabel = false;
        this.url = '';
        var form_add = [
            // { label:"notification_id",  type: "text",  value: "", data_binding: 'notification_id'  },
            { label: "title", type: "text", value: "", data_binding: 'title' },
            { label: "message", type: "text", value: "", data_binding: 'message' },
            { label: "from", type: "text", value: "", data_binding: 'from' },
            { label: "url_logo", type: "text", value: "", data_binding: 'url_logo' },
            { label: "url_web", type: "text", value: "", data_binding: 'url_web' },
            { label: "icon", type: "text", value: "", data_binding: 'icon' },
            { label: "url_logo", type: "text", value: "", data_binding: 'url_logo' },
            { label: "expiry_day", type: "number", value: "", data_binding: 'expiry_day' },
            // { label:"topic",  type: "textarea", value: [{label:"label", value:"label", selected:1},{label:"label1", value:"label2"}], data_binding: 'topic' },
            { label: "status", type: "textarea", value: [{ label: "label", value: "label", selected: 1 }, { label: "label1", value: "label2" }], data_binding: 'status' }
        ];
    }
    NotificationMessageAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    NotificationMessageAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    NotificationMessageAddComponent.prototype.formSubmitAddNotificationGroup = function (form) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.notificationService;
                        return [4 /*yield*/, this.notificationService.addNewNotification(form)];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        this.NotificationMessage = result.result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    NotificationMessageAddComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    NotificationMessageAddComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        // var reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]); //
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
    };
    NotificationMessageAddComponent.prototype.onSelectFile = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]); // read file as data url
            reader.onload = function (event) {
                var _event = event;
                _this.url = _event.target.result;
            };
        }
    };
    NotificationMessageAddComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    NotificationMessageAddComponent.ctorParameters = function () { return [
        { type: _services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__["NotificationService"] }
    ]; };
    NotificationMessageAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notificationmessage-add',
            template: __webpack_require__(/*! raw-loader!./notificationmessage.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notificationmessage/add/notificationmessage.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./notificationmessage.add.component.scss */ "./src/app/layout/modules/notificationmessage/add/notificationmessage.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__["NotificationService"]])
    ], NotificationMessageAddComponent);
    return NotificationMessageAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/notificationmessage/notificationmessage-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/layout/modules/notificationmessage/notificationmessage-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: NotificationMessageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationMessageRoutingModule", function() { return NotificationMessageRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _notificationmessage_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notificationmessage.component */ "./src/app/layout/modules/notificationmessage/notificationmessage.component.ts");
/* harmony import */ var _add_notificationmessage_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/notificationmessage.add.component */ "./src/app/layout/modules/notificationmessage/add/notificationmessage.add.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '', component: _notificationmessage_component__WEBPACK_IMPORTED_MODULE_2__["NotificationMessageComponent"],
    },
    {
        path: 'add', component: _add_notificationmessage_add_component__WEBPACK_IMPORTED_MODULE_3__["NotificationMessageAddComponent"]
    },
];
var NotificationMessageRoutingModule = /** @class */ (function () {
    function NotificationMessageRoutingModule() {
    }
    NotificationMessageRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], NotificationMessageRoutingModule);
    return NotificationMessageRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/notificationmessage/notificationmessage.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/notificationmessage/notificationmessage.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL25vdGlmaWNhdGlvbm1lc3NhZ2Uvbm90aWZpY2F0aW9ubWVzc2FnZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/modules/notificationmessage/notificationmessage.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/notificationmessage/notificationmessage.component.ts ***!
  \*************************************************************************************/
/*! exports provided: NotificationMessageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationMessageComponent", function() { return NotificationMessageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/notification/notification.service */ "./src/app/services/notification/notification.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var NotificationMessageComponent = /** @class */ (function () {
    function NotificationMessageComponent(notificationService) {
        this.notificationService = notificationService;
        this.NotificationMessage = [];
        this.tableFormat = {
            title: 'Active Notifications Message Detail',
            label_headers: [
                { label: 'Notification Message ID', visible: true, type: 'string', data_row_name: '_id' },
                { label: 'Organization ID', visible: true, type: 'string', data_row_name: 'org_id' },
                { label: 'Title', visible: true, type: 'string', data_row_name: 'title' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'NotificationMessage',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.notificationmessageDetail = false;
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
        this.url = '';
    }
    NotificationMessageComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    NotificationMessageComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.notificationService;
                        return [4 /*yield*/, this.notificationService.getNotificationMessageLint()];
                    case 1:
                        result = _a.sent();
                        this.NotificationMessage = result.result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // public async callDetail(_id) {    
    //   try {
    //     this.service    = this.notificationService;
    //     let result: any = await this.notificationService.detailNotificationGroup(_id);
    //     this.notificationgroupDetail = result.result[0];
    //   } catch (e) {
    //     this.errorLabel = ((<Error>e).message);//conversion to Error type
    //   }
    // }
    NotificationMessageComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.notificationmessageDetail = false;
                return [2 /*return*/];
            });
        });
    };
    NotificationMessageComponent.ctorParameters = function () { return [
        { type: _services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__["NotificationService"] }
    ]; };
    NotificationMessageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notificationsmessage',
            template: __webpack_require__(/*! raw-loader!./notificationmessage.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notificationmessage/notificationmessage.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./notificationmessage.component.scss */ "./src/app/layout/modules/notificationmessage/notificationmessage.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__["NotificationService"]])
    ], NotificationMessageComponent);
    return NotificationMessageComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/notificationmessage/notificationmessage.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/layout/modules/notificationmessage/notificationmessage.module.ts ***!
  \**********************************************************************************/
/*! exports provided: NotificationMessageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationMessageModule", function() { return NotificationMessageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _notificationmessage_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notificationmessage.component */ "./src/app/layout/modules/notificationmessage/notificationmessage.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _notificationmessage_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./notificationmessage-routing.module */ "./src/app/layout/modules/notificationmessage/notificationmessage-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _add_notificationmessage_add_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./add/notificationmessage.add.component */ "./src/app/layout/modules/notificationmessage/add/notificationmessage.add.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var NotificationMessageModule = /** @class */ (function () {
    function NotificationMessageModule() {
    }
    NotificationMessageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _notificationmessage_routing_module__WEBPACK_IMPORTED_MODULE_7__["NotificationMessageRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"]
            ],
            declarations: [
                _notificationmessage_component__WEBPACK_IMPORTED_MODULE_2__["NotificationMessageComponent"], _add_notificationmessage_add_component__WEBPACK_IMPORTED_MODULE_9__["NotificationMessageAddComponent"]
            ]
        })
    ], NotificationMessageModule);
    return NotificationMessageModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-notificationmessage-notificationmessage-module.js.map