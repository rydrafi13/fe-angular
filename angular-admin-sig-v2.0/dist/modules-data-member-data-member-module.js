(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-data-member-data-member-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/add/data-member.add.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/data-member/add/data-member.add.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n    <app-page-header [heading]=\"'Add New Member'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n    <div>\r\n        <div class=\"card card-detail\">\r\n            <div class=\"card-header\">\r\n                <button class=\"btn save_button\" (click)=\"formSubmitAddMember()\" type=\"submit\"\r\n                    [routerLinkActive]=\"['router-link-active']\"><i class=\"fa fa-fw fa-save\"\r\n                        style=\"transform: translate(-3px, -1px)\"></i>Submit</button>\r\n                <button class=\"btn back_button\" (click)=\"backTo()\"><i class=\"fa fa-fw fa-angle-left\"></i>Back</button>\r\n            </div>\r\n\r\n            <div class=\"card-content\">\r\n                <div class=\"member-detail\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-8\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2> Member Details </h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n\r\n                                        <label>Member ID</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.id\" [placeholder]=\"'Member ID'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Full Name</label>\r\n                                        <form-input name=\"full_name\" [(ngModel)]=\"form.full_name\" [type]=\"'text'\" [placeholder]=\"'Full Name'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <!-- <div class=\"form-group\">\r\n                                            <label for=\"dob\">Date Of Birth</label>\r\n                                            <input type=\"text\" name=\"dob\" [(ngModel)]=\"dob\" type=\"date\"\r\n                                                class=\"form-control\">\r\n                                        </div> -->\r\n\r\n                                        <label>Date of Birth</label>\r\n                                        <div class=\"input-group datepicker-input\" format=\"DD-MM-YYYY\">\r\n                                            <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\"\r\n                                                [(ngModel)]=\"form.dob\" ngbDatepicker #d=\"ngbDatepicker\">\r\n                                            <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                                <i class=\"fa fa-calendar\"></i>\r\n                                            </button>\r\n                                        </div>\r\n\r\n\r\n                                        <div class=\"form-group\">\r\n                                            <label>Gender</label>\r\n                                            <select class=\"form-control\" name=\"gender\" [(ngModel)]=\"form.gender\">\r\n                                                <option *ngFor=\"let g of gender_type\" [ngValue]=\"g.value\">{{g.label}}\r\n                                                </option>\r\n                                            </select>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group\">\r\n                                            <label>Member Status</label>\r\n                                            <select class=\"form-control\" name=\"member_status\"\r\n                                                [(ngModel)]=\"form.member_status\">\r\n                                                <option *ngFor=\"let m of member_status_type\" [ngValue]=\"m.value\">\r\n                                                    {{m.label}}</option>\r\n                                            </select>\r\n                                        </div>\r\n\r\n                                        <label>Address</label>\r\n                                        <form-input name=\"maddress1\" [(ngModel)]=\"form.maddress1\" [type]=\"'textarea'\" [placeholder]=\"'Address'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <label>City</label>\r\n                                        <form-input name=\"mcity\" [(ngModel)]=\"form.mcity\" [type]=\"'text'\" [placeholder]=\"'City'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Postcode</label>\r\n                                        <form-input name=\"mpostcode\" [(ngModel)]=\"form.mpostcode\" [type]=\"'number'\" [placeholder]=\"'Postcode'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <label>State</label>\r\n                                        <form-input name=\"mstate\" [(ngModel)]=\"form.mstate\" [type]=\"'text'\" [placeholder]=\"'State'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Country</label>\r\n                                        <form-input name=\"mcountry\" [(ngModel)]=\"form.mcountry\" [type]=\"'text'\" [placeholder]=\"'Country'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-4\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2>Member &amp; Status Member</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n\r\n                                        <label>Password</label>\r\n                                        <form-input name=\"password\" [type]=\"'password'\" [placeholder]=\"'Password'\"\r\n                                            [(ngModel)]=\"form.password\" autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <label>Email</label>\r\n                                        <form-input name=\"email\" [(ngModel)]=\"form.email\" [type]=\"'text'\" [placeholder]=\"'Email'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Cell Phone</label>\r\n                                        <form-input name=\"cell_phone\" [type]=\"'number'\" [(ngModel)]=\"form.cell_phone\" [placeholder]=\"'Cell Phone'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <!-- <form #form=\"ngForm\" (ngSubmit)=\"formSubmitAddMember(form.value)\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-6\">\r\n                        <label>Full Name</label>\r\n                        <form-input name=\"full_name\" [type]=\"'text'\" [placeholder]=\"'Full Name'\" [(ngModel)]=\"full_name\"\r\n                            required></form-input>\r\n\r\n                        <label>Email</label>\r\n                        <form-input name=\"email\" [placeholder]=\"'Email'\" [type]=\"'text'\" [(ngModel)]=\"email\" required>\r\n                        </form-input>\r\n\r\n                        <label>Password</label>\r\n                        <form-input name=\"password\" [placeholder]=\"'Password'\" [type]=\"'password'\"\r\n                            [(ngModel)]=\"password\" required></form-input>\r\n\r\n                    \r\n                        <label>Cell Phone</label>\r\n                        <form-input name=\"cell_phone\" [placeholder]=\"'Cell Phone'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"cell_phone\" required></form-input>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>City</label>\r\n                        <form-input name=\"mcity\" [placeholder]=\"'City'\" [type]=\"'text'\" [(ngModel)]=\"mcity\" required>\r\n                        </form-input>\r\n                    </div>\r\n\r\n                    <div class=\"col-md-6\">\r\n                        <label>State</label>\r\n                        <form-input name=\"mstate\" [placeholder]=\"'State'\" [type]=\"'text'\" [(ngModel)]=\"mstate\" required>\r\n                        </form-input>\r\n\r\n                        <label>Country</label>\r\n                        <form-input name=\"mcountry\" [placeholder]=\"'Country'\" [type]=\"'text'\" [(ngModel)]=\"mcountry\"\r\n                            required></form-input>\r\n\r\n                        <label>Address</label>\r\n                        <form-input name=\"maddress1\" [placeholder]=\"'Address'\" [type]=\"'textarea'\"\r\n                            [(ngModel)]=\"maddress1\" required></form-input>\r\n\r\n                        <label>Post Code</label>\r\n                        <form-input name=\"mpostcode\" [placeholder]=\"'Post Code'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"mpostcode\" required></form-input>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label>Gender</label>\r\n                            <select class=\"form-control\" name=\"gender\" [(ngModel)]=\"gender\">\r\n                                <option *ngFor=\"let d of gender_type\" value=\"{{d.value}}\">{{d.label}}</option>\r\n                            </select>\r\n                        </div>\r\n\r\n\r\n                        <label>Marital Status</label>\r\n                        <form-select name=\"marital_status\" [(ngModel)]=\"marital_status\" [data]=\"marital_status_type\">\r\n                        </form-select>\r\n\r\n                        <label>Member Type</label>\r\n                        <form-select name=\"type\" [(ngModel)]=\"type\" [data]=\"member_type\"></form-select>\r\n                    </div>\r\n                </div>\r\n\r\n            </form> -->\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/change-password/change-password.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/data-member/change-password/change-password.component.html ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!showPasswordPage\">\r\n    <div class=\"card card-detail\">\r\n        <div class=\"card-header\">\r\n            <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n        </div>\r\n        <div class=\"card-content\">\r\n            <div class=\"member-detail\">\r\n        \r\n                <div class=\"row\">\r\n                    <div class=\"col-md-4 col-sm-12\" >\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Change Password</h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\">\r\n    \r\n                                    <label>ID Pelanggan</label>\r\n                                    <form-input  name=\"id_pel\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'ID Pelanggan'\"  [(ngModel)]=\"updateSingle.id_pel\" autofocus required></form-input>\r\n    \r\n                                    <div class=\"add-more-container\">\r\n                                        <button (click)=\"saveNewPassword(updateSingle.id_pel)\" class=\"add-more-image btn-change-password\">Request Change Password</button>\r\n                                    </div>\r\n    \r\n                                </div>\r\n                            \r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <!-- CHANGE PASSWORD BULKY -->\r\n                    <!-- <div class=\"col-md-4 col-sm-12\">\r\n                        <div class=\"card mb-3\">\r\n                            <input #changePasswordBulk type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event)\"\r\n                            />\r\n\r\n                            <div class=\"card-header\">\r\n                                <h2>Change Password Bulk</h2>\r\n                            </div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\">\r\n                                    <label>Upload Your File Here</label>\r\n                                    <div class=\"member-bulk-update-container\">\r\n                                        <span *ngIf=\"selectedFile\">{{selectedFile.name}}</span>\r\n                                    </div>\r\n\r\n                                    <div class=\"form-group uploaded\" *ngIf=\"loadingKTPPemilik\">\r\n                                        <div class=\"loading\">\r\n                                          <div class=\"sk-fading-circle\">\r\n                                            <div class=\"sk-circle1 sk-circle\"></div>\r\n                                            <div class=\"sk-circle2 sk-circle\"></div>\r\n                                            <div class=\"sk-circle3 sk-circle\"></div>\r\n                                            <div class=\"sk-circle4 sk-circle\"></div>\r\n                                            <div class=\"sk-circle5 sk-circle\"></div>\r\n                                            <div class=\"sk-circle6 sk-circle\"></div>\r\n                                            <div class=\"sk-circle7 sk-circle\"></div>\r\n                                            <div class=\"sk-circle8 sk-circle\"></div>\r\n                                            <div class=\"sk-circle9 sk-circle\"></div>\r\n                                            <div class=\"sk-circle10 sk-circle\"></div>\r\n                                            <div class=\"sk-circle11 sk-circle\"></div>\r\n                                            <div class=\"sk-circle12 sk-circle\"></div>\r\n                                          </div>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <div class=\"container-two-button\">\r\n                                        <div class=\"img-upload\" *ngIf=\"!selectedFile; else upload_image\">\r\n                                            <button class=\"btn-upload\" (click)=\"changePasswordBulk.click()\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span>upload</span>\r\n                                            </button>\r\n                                        </div>\r\n\r\n                                        <div class=\"img-upload\" *ngIf=\"selectedFile\">\r\n                                            <button class=\"btn-upload\" (click)=\"updateDataBulk()\">\r\n                                                <i class=\"fa fa-fw fa-paper-plane\"></i>\r\n                                                <span>submit</span>\r\n                                            </button>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <ng-template #upload_image>\r\n                                    <button class=\"btn-upload\" (click)=\"changePasswordBulk.click()\">\r\n                                        <i class=\"fa fa-fw fa-upload\"></i>\r\n                                        <span>re-upload</span>\r\n                                    </button>\r\n                                </ng-template>\r\n\r\n                                <div class=\"col-md-12 col-sm-12 error-msg-upload\">\r\n                                    <div *ngIf=\"errorFile\"><mark>Error: {{errorFile}}</mark></div>                       \r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div> -->\r\n    \r\n                    <!-- <div class=\"col-md-8 col-sm-12\">\r\n                        <button class=\"btn-add-bulk\" (click)=\"actionShowUploadButton()\">Request Change Password Bulk</button>\r\n    \r\n                            <div *ngIf=\"prodOnUpload==false && showUploadButton\">\r\n                                <input type=\"file\" (change)=\"onFileSelected($event)\" style=\"color: white\" />\r\n                                <div class=\"file-selected\" *ngIf=\"selectedFile\">\r\n                                    {{selectedFile.name}}\r\n                                </div>\r\n                                <div class=\"col-md-12 col-sm-12 error-msg-upload\">\r\n                                    <div *ngIf=\"errorFile\"><mark>Error: {{errorFile}}</mark></div>                       \r\n                                </div>\r\n                                <button class=\"btn rounded-btn\" (click)=\"updateDataBulk()\">Upload</button>\r\n                                <button class=\"btn rounded-btn-danger\" (click)=\"cancelThis()\" >Cancel</button>\r\n                                <br><br>\r\n                            </div>\r\n                    </div> -->\r\n                    \r\n                </div>\r\n         \r\n             </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div *ngIf=\"showPasswordPage\">\r\n    <app-show-password [back]=\"[this, 'showThisPassword']\" [detail]=\"dataLogin\"></app-show-password>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/data-member.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/data-member/data-member.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'Member'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  \r\n  <!-- <button (click)=\"getDownloadFileLint()\"><i class=\"fa fa-file-pdf-o\"></i>Download</button> -->\r\n  <!-- <iframe [src]=\"srcDownload\"></iframe> -->\r\n  <div *ngIf=\"Members&&memberDetail==false&&!errorMessage\">\r\n        <app-form-builder-table \r\n        [tableFormat] = \"tableFormat\" \r\n        [table_data]  = \"Members\" \r\n        [searchCallback]= \"[service, 'searchMemberAllReport',this]\" \r\n        [total_page]=\"totalPage\"\r\n        >\r\n            \r\n        </app-form-builder-table>\r\n  </div>\r\n  <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n    <div class=\"text-message\">\r\n      <i class=\"fas fa-ban\"></i>\r\n      {{errorMessage}}\r\n    </div>\r\n  </div>\r\n  <div *ngIf=\"memberDetail\">\r\n    <app-data-member-detail [back]=\"[this,backToHere]\" [detail]=\"memberDetail\"></app-data-member-detail>\r\n</div>\r\n \r\n</div>\r\n\r\n "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/detail/data-member.detail.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/data-member/detail/data-member.detail.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit && !isChangePassword && !isShowPassword && !errorMessage\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <div class=\"edit_button\">\r\n            <!-- <button class=\"btn btn-right\" (click)=\"showPassword()\"><i class=\"fa fa-fw fa-lock\"></i> show password</button> -->\r\n            <button class=\"btn btn-right\" (click)=\"changePassword()\"><i class=\"fa fa-fw fa-key\"></i> change password</button>\r\n            <!-- <button class=\"btn btn-right\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button> -->\r\n        </div>\r\n    </div>\r\n\r\n    <ng-template #empty_image><span>\r\n        empty\r\n    </span></ng-template>\r\n  \r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\" *ngIf=\"!mci_project\"><h2>Data Pemilik dan Toko</h2></div>\r\n                        <div class=\"card-header\" *ngIf=\"mci_project\"><h2>Data Pelanggan</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>ID Pelanggan</label>\r\n                                <div name=\"member_id\">{{memberDetail.username}}</div>\r\n                                \r\n                                <label *ngIf=\"!mci_project\">Nama Toko</label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\" name=\"full_name\">{{memberDetail.input_form_data.nama_toko}}</div>\r\n                                \r\n                                <label *ngIf=\"!mci_project\">Nama Distributor</label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\" name=\"full_name\">{{memberDetail.input_form_data.nama_distributor}}</div>\r\n\r\n                                <label *ngIf=\"!mci_project\">Alamat Toko</label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\" name=\"full_name\">{{memberDetail.input_form_data.alamat_toko}}</div>\r\n\r\n                                <label *ngIf=\"!mci_project\">Nama Pemilik</label>\r\n                                <label *ngIf=\"mci_project\">Nama Pelanggan</label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data\" name=\"full_name\">{{memberDetail.input_form_data.nama_pemilik}}</div>\r\n\r\n                                <label *ngIf=\"!mci_project\">No. WA Pemilik</label>\r\n                                <label *ngIf=\"mci_project\">No. WA Pelanggan</label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data\" name=\"full_name\">{{memberDetail.input_form_data.no_wa_pemilik}}</div>\r\n                                \r\n                                <label *ngIf=\"!mci_project\">No. KTP Pemilik</label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\" name=\"full_name\">{{memberDetail.input_form_data.ktp_pemilik}}</div>\r\n\r\n                                <label *ngIf=\"!mci_project\">Foto KTP Pemilik</label>\r\n                                <div class=\"member-detail-image-container\" *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\">\r\n                                    <ng-container *ngIf=\"validURL(memberDetail.input_form_data.foto_ktp_pemilik); else empty_image\">\r\n                                        <a href={{memberDetail.input_form_data.foto_ktp_pemilik}} target=\"_blank\">\r\n                                            <img src={{memberDetail.input_form_data.foto_ktp_pemilik}} class=\"member-detail-image\" />\r\n                                        </a>\r\n                                        <div class=\"btn-download-container\">\r\n                                            <button (click)=\"downloadImage(memberDetail.input_form_data.foto_ktp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                    \r\n                                    \r\n                                </div>\r\n\r\n                                <label *ngIf=\"!mci_project\">No. NPWP Pemilik</label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\" name=\"full_name\">{{memberDetail.input_form_data.npwp_pemilik}}</div>\r\n\r\n                                <label *ngIf=\"!mci_project\">Foto NPWP Pemilik</label>\r\n                                <div class=\"member-detail-image-container\" *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\">\r\n                                    <ng-container  *ngIf=\"validURL(memberDetail.input_form_data.foto_npwp_pemilik); else empty_image\">\r\n                                        <a href={{memberDetail.input_form_data.foto_npwp_pemilik}} target=\"_blank\">\r\n                                            <img src={{memberDetail.input_form_data.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                        </a>\r\n                                        <div class=\"btn-download-container\">\r\n                                            <button (click)=\"downloadImage(memberDetail.input_form_data.foto_npwp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                </div>\r\n\r\n                            </div>\r\n                        \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Penerima </h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <label *ngIf=\"!mci_project\">Hadiah Dikuasakan</label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\" name=\"email\">{{memberDetail.input_form_data.hadiah_dikuasakan}}</div>\r\n                            \r\n                                <label>Nama Penerima</label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data\" name=\"email\">{{memberDetail.input_form_data.nama_penerima_gopay}}</div>\r\n\r\n                                <label>No. WA Penerima</label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data\" name=\"email\">{{memberDetail.input_form_data.no_wa_penerima}}</div>\r\n\r\n                                <label *ngIf=\"!mci_project\">No. Gopay Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo Gopay, isi dengan No. Gopay Penerima)</div></label>\r\n                                <div *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\" name=\"email\">{{memberDetail.input_form_data.telp_penerima_gopay}}</div>\r\n\r\n                               <label>Alamat Pengiriman</label>\r\n                               <div *ngIf=\"memberDetail && memberDetail.input_form_data\" name=\"email\">{{memberDetail.input_form_data.alamat_rumah}}</div>\r\n\r\n                               <label *ngIf=\"!mci_project\">No. KTP Penerima</label>\r\n                               <div *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\" name=\"email\">{{memberDetail.input_form_data.ktp_penerima}}</div>\r\n\r\n                               <label *ngIf=\"!mci_project\">Foto KTP Penerima</label>\r\n                                <div class=\"member-detail-image-container\" *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\">\r\n                                    <ng-container *ngIf=\"validURL(memberDetail.input_form_data.foto_ktp_penerima); else empty_image\">\r\n                                        <a href={{memberDetail.input_form_data.foto_ktp_penerima}} target=\"_blank\" >\r\n                                           <img src={{memberDetail.input_form_data.foto_ktp_penerima}} class=\"member-detail-image\" />\r\n                                        </a>\r\n                                        <div class=\"btn-download-container\">\r\n                                            <button (click)=\"downloadImage(memberDetail.input_form_data.foto_ktp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                </div>\r\n\r\n                               <label *ngIf=\"!mci_project\">No. NPWP Penerima</label>\r\n                               <div *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\" name=\"email\">{{memberDetail.input_form_data.npwp_penerima}}</div>\r\n\r\n                               <label *ngIf=\"!mci_project\">Foto NPWP Penerima</label>\r\n                               <div class=\"member-detail-image-container\" *ngIf=\"memberDetail && memberDetail.input_form_data && !mci_project\">\r\n                                   <ng-container *ngIf=\"validURL(memberDetail.input_form_data.foto_npwp_penerima); else empty_image\">\r\n                                       <a href={{memberDetail.input_form_data.foto_npwp_penerima}} target=\"_blank\">\r\n                                          <img src={{memberDetail.input_form_data.foto_npwp_penerima}} class=\"member-detail-image\" />\r\n                                       </a>\r\n                                       <div class=\"btn-download-container\">\r\n                                           <button (click)=\"downloadImage(memberDetail.input_form_data.foto_npwp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                       </div>\r\n                                   </ng-container>\r\n                                </div>\r\n                            </div>\r\n                         </div>\r\n                    </div>\r\n                    \r\n                </div>\r\n                <!-- <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Foto</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n\r\n                                <label class=\"label-bold\">Foto BAST</label>\r\n                                <ng-container *ngIf=\"memberDetail && memberDetail.input_form_data && memberDetail.input_form_data.foto_bast && isArray(memberDetail.input_form_data.foto_bast) && memberDetail.input_form_data.foto_bast.length > 0; else empty_image\">\r\n                                    <div class=\"member-detail-image-container\"  *ngFor=\"let itemImage of memberDetail.input_form_data.foto_bast; let i = index\">\r\n                                        <ng-container *ngIf=\"validURL(itemImage.url); else empty_image\">\r\n                                            <a href={{itemImage.url}} target=\"_blank\">\r\n                                                <img src={{itemImage.url}} class=\"member-detail-image\" />\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(itemImage.url)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                            <div style=\"text-align: center;\">\r\n                                                <span>\r\n                                                    <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                </span>\r\n                                            </div>\r\n\r\n                                            <br/>\r\n                                            <div>\r\n                                                <span><b>Desription :</b></span>\r\n                                                <span>\r\n                                                    {{itemImage.desc}}\r\n                                                </span>\r\n                                            </div>\r\n\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </ng-container>\r\n\r\n                                <ng-template #empty_image>\r\n                                    <div>\r\n                                        empty\r\n                                    </div>\r\n                                </ng-template>\r\n\r\n                                <hr style=\"margin:35px 0 20px 0;\"/>\r\n\r\n                                <label class=\"label-bold\">Foto Surat Kuasa</label>\r\n                                <ng-container *ngIf=\"memberDetail && memberDetail.input_form_data && memberDetail.input_form_data.foto_surat_kuasa && isArray(memberDetail.input_form_data.foto_surat_kuasa) && memberDetail.input_form_data.foto_surat_kuasa.length > 0; else empty_image\">\r\n                                    <div class=\"member-detail-image-container\"   *ngFor=\"let itemImage of memberDetail.input_form_data.foto_surat_kuasa; let i = index\">\r\n                                        <ng-container *ngIf=\"validURL(itemImage.url); else empty_image\">\r\n                                            <a href={{itemImage.url}} target=\"_blank\">\r\n                                                <img src={{itemImage.url}} class=\"member-detail-image\" />\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(itemImage.url)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                            <div style=\"text-align: center;\">\r\n                                                <span>\r\n                                                    <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                </span>\r\n                                            </div>\r\n                                            <br/>\r\n                                            <div>\r\n                                                <span><b>Desription :</b></span>\r\n                                                <span>\r\n                                                    {{itemImage.desc}}\r\n                                                </span>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </ng-container>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n            </div>\r\n            <!-- <div class=\"row\">\r\n                <div class=\"col-md-12 bg-delete\"><button class=\"btn delete-button float-right btn-danger\" (click)=\"delete()\">delete this</button></div>\r\n            </div> -->\r\n            \r\n         </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"errorMessage\" class=\"error-message\">\r\n    <div class=\"text-message\">\r\n        <i class=\"fas fa-ban\"></i>\r\n        {{errorMessage}}\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-data-member-edit [back]=\"[this, 'editThis']\" [detail]=\"memberDetail\"></app-data-member-edit>\r\n</div>\r\n\r\n<div *ngIf=\"isChangePassword\">\r\n    <app-change-password [back]=\"[this, 'changePassword']\" [detail]=\"memberDetail\"></app-change-password>\r\n</div>\r\n\r\n<div *ngIf=\"isShowPassword\">\r\n    <app-show-password [back]=\"[this, 'showPassword']\" [detail]=\"memberDetail\"></app-show-password>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/edit/data-member.edit.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/data-member/edit/data-member.edit.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <input #ktpPemilik type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'ktpPemilik')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\"/>\r\n    <input #npwpPemilik type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'npwpPemilik')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\"/>\r\n    <input #ktpPenerima type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'ktpPenerima')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\"/>\r\n    <input #npwpPenerima type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'npwpPenerima')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\"/>\r\n\r\n    <ng-container *ngIf=\"allBast.length > 0\">\r\n        <ng-container *ngFor=\"let item of allBast; let i = index\">\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'bast', i)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg\" style=\"display:none;\" [id]=\"'bast'+i\"/>\r\n        </ng-container>\r\n    </ng-container>\r\n\r\n    <ng-container *ngIf=\"allSuratKuasa.length > 0\">\r\n        <ng-container *ngFor=\"let item of allSuratKuasa; let i = index\">\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'suratKuasa', i)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg\" style=\"display:none;\" [id]=\"'suratKuasa'+i\"/>\r\n        </ng-container>\r\n    </ng-container>\r\n    \r\n    \r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span></button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Pemilik</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <label>ID Pelanggan</label>\r\n                                <form-input  name=\"id_pel\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'ID Pelanggan'\"  [(ngModel)]=\"updateSingle.id_pel\" autofocus required></form-input>\r\n                \r\n                                <label *ngIf=\"!mci_project\">Nama Toko</label>\r\n                                <form-input *ngIf=\"!mci_project\" name=\"nama_toko\"  [type]=\"'text'\" [placeholder]=\"'Nama Toko'\"  [(ngModel)]=\"updateSingle.nama_toko\" autofocus required></form-input>\r\n\r\n                                <label *ngIf=\"!mci_project\">Nama Distributor</label>\r\n                                <form-input *ngIf=\"!mci_project\" name=\"nama_distributor\"  [type]=\"'text'\" [placeholder]=\"'Nama Distributor'\"  [(ngModel)]=\"updateSingle.nama_distributor\" autofocus required></form-input>\r\n                                \r\n                                <label *ngIf=\"!mci_project\">Alamat Toko</label>\r\n                                <form-input *ngIf=\"!mci_project\" name=\"alamat_toko\"  [type]=\"'text'\" [placeholder]=\"'Alamat Toko'\"  [(ngModel)]=\"updateSingle.alamat_toko\" autofocus required></form-input>\r\n                                \r\n                                <label *ngIf=\"!mci_project\">Nama Pemilik</label>\r\n                                <label *ngIf=\"mci_project\">Nama Pelanggan</label>\r\n                                <form-input name=\"nama_pemilik\"  [type]=\"'text'\" [placeholder]=\"'Nama Pemilik'\"  [(ngModel)]=\"updateSingle.nama_pemilik\" autofocus required></form-input>\r\n\r\n                                <label *ngIf=\"!mci_project\">No. Telp Pemilik</label>\r\n                                <label *ngIf=\"mci_project\">No. Telp Pelanggan</label>\r\n                                <form-input name=\"telp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'No. Telp Pemilik'\"  [(ngModel)]=\"updateSingle.telp_pemilik\" autofocus required></form-input>\r\n\r\n                                <label *ngIf=\"!mci_project\">No WA Pemilik</label>\r\n                                <label *ngIf=\"mci_project\">No WA Pelanggan</label>\r\n                                <form-input name=\"no_wa_pemilik\"  [type]=\"'text'\" [placeholder]=\"'No WA Pemilik'\"  [(ngModel)]=\"updateSingle.no_wa_pemilik\" autofocus required></form-input>\r\n\r\n\r\n                                <label *ngIf=\"!mci_project\">No. KTP Pemilik</label>\r\n                                <form-input *ngIf=\"!mci_project\" name=\"ktp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'No. KTP Pemilik'\"  [(ngModel)]=\"updateSingle.ktp_pemilik\" autofocus required></form-input>\r\n\r\n                                <label *ngIf=\"!mci_project\">Foto KTP Pemilik</label>\r\n                                <!-- <form-input name=\"foto_ktp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'KTP Pemilik'\"  [(ngModel)]=\"updateSingle.foto_ktp_pemilik\" autofocus required></form-input> -->\r\n                                <div *ngIf=\"!mci_project\" class=\"member-detail-image-container\">\r\n                                    <div *ngIf=\"validURL(updateSingle.foto_ktp_pemilik) && !loadingKTPPemilik\" class=\"img-upload\">\r\n                                        <img src={{updateSingle.foto_ktp_pemilik}} class=\"member-detail-image\" />\r\n                                    </div>\r\n\r\n                                    <div class=\"form-group uploaded\" *ngIf=\"loadingKTPPemilik\">\r\n                                        <div class=\"loading\">\r\n                                          <div class=\"sk-fading-circle\">\r\n                                            <div class=\"sk-circle1 sk-circle\"></div>\r\n                                            <div class=\"sk-circle2 sk-circle\"></div>\r\n                                            <div class=\"sk-circle3 sk-circle\"></div>\r\n                                            <div class=\"sk-circle4 sk-circle\"></div>\r\n                                            <div class=\"sk-circle5 sk-circle\"></div>\r\n                                            <div class=\"sk-circle6 sk-circle\"></div>\r\n                                            <div class=\"sk-circle7 sk-circle\"></div>\r\n                                            <div class=\"sk-circle8 sk-circle\"></div>\r\n                                            <div class=\"sk-circle9 sk-circle\"></div>\r\n                                            <div class=\"sk-circle10 sk-circle\"></div>\r\n                                            <div class=\"sk-circle11 sk-circle\"></div>\r\n                                            <div class=\"sk-circle12 sk-circle\"></div>\r\n                                          </div>\r\n                                        </div>\r\n                                      </div>\r\n                                    \r\n                                    <!-- <ng-template #empty_image>\r\n                                        <span>\r\n                                            empty\r\n                                        </span>\r\n                                    </ng-template> -->\r\n\r\n                                    <div class=\"img-upload\"><button class=\"btn_upload\" (click)=\"ktpPemilik.click()\">\r\n                                        <i class=\"fa fa-fw fa-upload\"></i>\r\n                                        <span *ngIf=\"validURL(updateSingle.foto_ktp_pemilik); else upload_image\">Ubah</span>\r\n                                            <!-- <ng-template #upload_image>\r\n                                                <span>\r\n                                                    upload\r\n                                                </span>\r\n                                            </ng-template> -->\r\n                                    </button></div>\r\n\r\n                                </div>\r\n\r\n                                <label *ngIf=\"!mci_project\">No. NPWP Pemilik</label>\r\n                                <form-input *ngIf=\"!mci_project\" name=\"npwp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'No. NPWP Pemilik'\"  [(ngModel)]=\"updateSingle.npwp_pemilik\" autofocus required></form-input>\r\n                                \r\n                                <label *ngIf=\"!mci_project\">Foto NPWP Pemilik</label>\r\n                                <!-- <form-input name=\"foto_npwp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'NPWP Pemilik'\"  [(ngModel)]=\"updateSingle.foto_npwp_pemilik\" autofocus required></form-input> -->\r\n                                <div *ngIf=\"!mci_project\" class=\"member-detail-image-container\">\r\n                                    <div *ngIf=\"validURL(updateSingle.foto_npwp_pemilik) && !loadingNPWPPemilik\" class=\"img-upload\">\r\n                                        <img src={{updateSingle.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                    </div>\r\n\r\n                                    <div class=\"form-group uploaded\" *ngIf=\"loadingNPWPPemilik\">\r\n                                        <div class=\"loading\">\r\n                                          <div class=\"sk-fading-circle\">\r\n                                            <div class=\"sk-circle1 sk-circle\"></div>\r\n                                            <div class=\"sk-circle2 sk-circle\"></div>\r\n                                            <div class=\"sk-circle3 sk-circle\"></div>\r\n                                            <div class=\"sk-circle4 sk-circle\"></div>\r\n                                            <div class=\"sk-circle5 sk-circle\"></div>\r\n                                            <div class=\"sk-circle6 sk-circle\"></div>\r\n                                            <div class=\"sk-circle7 sk-circle\"></div>\r\n                                            <div class=\"sk-circle8 sk-circle\"></div>\r\n                                            <div class=\"sk-circle9 sk-circle\"></div>\r\n                                            <div class=\"sk-circle10 sk-circle\"></div>\r\n                                            <div class=\"sk-circle11 sk-circle\"></div>\r\n                                            <div class=\"sk-circle12 sk-circle\"></div>\r\n                                          </div>\r\n                                        </div>\r\n                                      </div>\r\n                                    \r\n                                    <!-- <ng-template #empty_image>\r\n                                        <span>\r\n                                            empty\r\n                                        </span>\r\n                                    </ng-template> -->\r\n                                    <div class=\"img-upload\">\r\n\r\n                                    <button class=\"btn_upload\" (click)=\"npwpPemilik.click()\">\r\n                                        <i class=\"fa fa-fw fa-upload\"></i>\r\n                                        <span *ngIf=\"validURL(updateSingle.foto_npwp_pemilik); else upload_image\">Ubah</span>\r\n                                            <!-- <ng-template #upload_image>\r\n                                                <span>\r\n                                                    upload\r\n                                                </span>\r\n                                            </ng-template> -->\r\n                                    </button>\r\n\r\n\r\n                                        <!-- <input [type]=\"'file'\" [id]=\"'fileUpload'\" ref=\"'file'\"/> -->\r\n                                    </div>\r\n                                </div>\r\n\r\n                            </div>\r\n                        \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Penerima</h2> </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Hadiah Dikuasakan</label>\r\n\r\n                                    <form-select [(ngModel)]=\"updateSingle.hadiah_dikuasakan\" [data]=\"optionsKuasa1\" *ngIf=\"updateSingle.hadiah_dikuasakan == 'true' || updateSingle.hadiah_dikuasakan == 'false'\"></form-select>\r\n                                    <form-select [(ngModel)]=\"updateSingle.hadiah_dikuasakan\" [data]=\"optionsKuasa2\" *ngIf=\"updateSingle.hadiah_dikuasakan == 'KUASA' || updateSingle.hadiah_dikuasakan == 'TIDAK KUASA' || updateSingle.hadiah_dikuasakan == '-'\"></form-select>\r\n                                    <form-select [(ngModel)]=\"updateSingle.hadiah_dikuasakan\" [data]=\"optionsKuasa3\" *ngIf=\"updateSingle.hadiah_dikuasakan == 'ya' || updateSingle.hadiah_dikuasakan == 'tidak'\"></form-select>\r\n                                </div>\r\n\r\n                                <label>Nama Penerima</label>\r\n                                <form-input name=\"nama_penerima_gopay\"  [type]=\"'text'\" [placeholder]=\"'Nama Penerima'\"  [(ngModel)]=\"updateSingle.nama_penerima_gopay\" autofocus required></form-input>\r\n\r\n                                <label>No. WA Penerima</label>\r\n                                <form-input name=\"no_wa_penerima\"  [type]=\"'text'\" [placeholder]=\"'No. WA Penerima'\"  [(ngModel)]=\"updateSingle.no_wa_penerima\" autofocus required></form-input>\r\n\r\n                                <label *ngIf=\"!mci_project\">No. Gopay Penerima</label>\r\n                                <form-input *ngIf=\"!mci_project\" name=\"telp_penerima_gopay\"  [type]=\"'text'\" [placeholder]=\"'No Gopay Penerima'\"  [(ngModel)]=\"updateSingle.telp_penerima_gopay\" autofocus required></form-input>\r\n\r\n                                <label>Alamat Pengiriman</label>\r\n                                <form-input name=\"alamat_rumah\"  [type]=\"'text'\" [placeholder]=\"'Alamat Pengiriman'\"  [(ngModel)]=\"updateSingle.alamat_rumah\" autofocus required></form-input>\r\n\r\n                                <label *ngIf=\"!mci_project\">No. KTP Penerima</label>\r\n                                <form-input *ngIf=\"!mci_project\" name=\"ktp_penerima\"  [type]=\"'text'\" [placeholder]=\"'No. KTP Penerima'\"  [(ngModel)]=\"updateSingle.ktp_penerima\" autofocus required></form-input>\r\n\r\n                                <label *ngIf=\"!mci_project\">Foto KTP Penerima</label>\r\n                                <!-- <form-input name=\"foto_ktp_penerima\"  [type]=\"'text'\" [placeholder]=\"'Nama Toko'\"  [(ngModel)]=\"updateSingle.foto_ktp_penerima\" autofocus required></form-input> -->\r\n\r\n                                <div *ngIf=\"!mci_project\" class=\"member-detail-image-container\">\r\n                                    <div *ngIf=\"validURL(updateSingle.foto_ktp_penerima) && !loadingKTPPenerima\" class=\"img-upload\">\r\n                                        <img src={{updateSingle.foto_ktp_penerima}} class=\"member-detail-image\" />\r\n                                    </div>\r\n\r\n                                    <div class=\"form-group uploaded\" *ngIf=\"loadingKTPPenerima\">\r\n                                        <div class=\"loading\">\r\n                                          <div class=\"sk-fading-circle\">\r\n                                            <div class=\"sk-circle1 sk-circle\"></div>\r\n                                            <div class=\"sk-circle2 sk-circle\"></div>\r\n                                            <div class=\"sk-circle3 sk-circle\"></div>\r\n                                            <div class=\"sk-circle4 sk-circle\"></div>\r\n                                            <div class=\"sk-circle5 sk-circle\"></div>\r\n                                            <div class=\"sk-circle6 sk-circle\"></div>\r\n                                            <div class=\"sk-circle7 sk-circle\"></div>\r\n                                            <div class=\"sk-circle8 sk-circle\"></div>\r\n                                            <div class=\"sk-circle9 sk-circle\"></div>\r\n                                            <div class=\"sk-circle10 sk-circle\"></div>\r\n                                            <div class=\"sk-circle11 sk-circle\"></div>\r\n                                            <div class=\"sk-circle12 sk-circle\"></div>\r\n                                          </div>\r\n                                        </div>\r\n                                      </div>\r\n\r\n                                    <!-- <ng-template #empty_image>\r\n                                        <span>\r\n                                            empty\r\n                                        </span>\r\n                                    </ng-template> -->\r\n                                    <div class=\"img-upload\">\r\n                                        <button class=\"btn_upload\" (click)=\"ktpPenerima.click()\">\r\n                                            <i class=\"fa fa-fw fa-upload\"></i>\r\n                                            <span *ngIf=\"validURL(updateSingle.foto_ktp_penerima); else upload_image\">Ubah</span>\r\n                                            <!-- <ng-template #upload_image>\r\n                                                <span>\r\n                                                    upload\r\n                                                </span>\r\n                                            </ng-template> -->\r\n                                        </button>\r\n                                    </div>\r\n                                    \r\n                                </div>\r\n\r\n                                <label *ngIf=\"!mci_project\">No. NPWP Penerima</label>\r\n                                <form-input *ngIf=\"!mci_project\" name=\"npwp_penerima\"  [type]=\"'text'\" [placeholder]=\"'No. NPWP Penerima'\"  [(ngModel)]=\"updateSingle.npwp_penerima\" autofocus required></form-input>\r\n\r\n                                <label *ngIf=\"!mci_project\">Foto NPWP Penerima</label>\r\n                                <!-- <form-input name=\"foto_npwp_penerima\"  [type]=\"'text'\" [placeholder]=\"'Nama Toko'\"  [(ngModel)]=\"updateSingle.foto_npwp_penerima\" autofocus required></form-input> -->\r\n\r\n                                <div *ngIf=\"!mci_project\" class=\"member-detail-image-container\">\r\n                                    <div *ngIf=\"validURL(updateSingle.foto_npwp_penerima) && !loadingNPWPPenerima\" class=\"img-upload\">\r\n                                        <img src={{updateSingle.foto_npwp_penerima}} class=\"member-detail-image\" />\r\n                                    </div>\r\n\r\n                                    <div class=\"form-group uploaded\" *ngIf=\"loadingNPWPPenerima\">\r\n                                        <div class=\"loading\">\r\n                                          <div class=\"sk-fading-circle\">\r\n                                            <div class=\"sk-circle1 sk-circle\"></div>\r\n                                            <div class=\"sk-circle2 sk-circle\"></div>\r\n                                            <div class=\"sk-circle3 sk-circle\"></div>\r\n                                            <div class=\"sk-circle4 sk-circle\"></div>\r\n                                            <div class=\"sk-circle5 sk-circle\"></div>\r\n                                            <div class=\"sk-circle6 sk-circle\"></div>\r\n                                            <div class=\"sk-circle7 sk-circle\"></div>\r\n                                            <div class=\"sk-circle8 sk-circle\"></div>\r\n                                            <div class=\"sk-circle9 sk-circle\"></div>\r\n                                            <div class=\"sk-circle10 sk-circle\"></div>\r\n                                            <div class=\"sk-circle11 sk-circle\"></div>\r\n                                            <div class=\"sk-circle12 sk-circle\"></div>\r\n                                          </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    \r\n                                    <!-- <ng-template #empty_image>\r\n                                        <span>\r\n                                            empty\r\n                                        </span>\r\n                                    </ng-template> -->\r\n                                    <div class=\"img-upload\">\r\n                                        <button class=\"btn_upload\" (click)=\"npwpPenerima.click()\">\r\n                                            <i class=\"fa fa-fw fa-upload\"></i>\r\n                                            <span *ngIf=\"validURL(updateSingle.foto_npwp_penerima); else upload_image\">Ubah</span>\r\n                                            <!-- <ng-template #upload_image>\r\n                                                <span>\r\n                                                    upload\r\n                                                </span>\r\n                                            </ng-template> -->\r\n                                        </button>\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <!-- <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Foto</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                \r\n                                <label class=\"label-bold\">Foto BAST</label>\r\n                                <ng-container *ngIf=\"updateSingle.foto_bast && isArray(updateSingle.foto_bast) && updateSingle.foto_bast.length > 0\">\r\n                                        <div class=\"member-detail-image-container\" *ngFor=\"let itemImage of updateSingle.foto_bast; let i = index\">\r\n                                            <div *ngIf=\"validURL(itemImage.url) && !loadingBAST[i]\" class=\"img-upload\">\r\n                                                <img src={{itemImage.url}} class=\"member-detail-image\" />\r\n                                            </div>\r\n        \r\n                                            <div class=\"form-group uploaded\" *ngIf=\"loadingBAST[i]\">\r\n                                                <div class=\"loading\">\r\n                                                  <div class=\"sk-fading-circle\">\r\n                                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                                  </div>\r\n                                                </div>\r\n                                            </div>\r\n        \r\n                                            <div class=\"img-upload\">\r\n                                                <button class=\"btn_upload\" (click)=\"openUpload('bast', i)\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span *ngIf=\"validURL(itemImage.url); else upload_image\">Ubah</span>\r\n                                                </button>\r\n                                                <button  *ngIf=\"validURL(itemImage.url)\" class=\"btn_delete\" (click)=\"deleteImage('bast', i, itemImage.url)\">\r\n                                                    <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                    <span>Hapus</span>\r\n                                                </button>\r\n                                            </div>\r\n                                            <div *ngIf=\"validURL(itemImage.url)\" style=\"text-align: center;\">\r\n                                                <span>\r\n                                                    <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                </span>\r\n                                            </div>\r\n\r\n                                            <div >\r\n                                                <label>Description</label>\r\n                                                <form-input  name=\"'descBast'+i\" [type]=\"'text'\" [placeholder]=\"'Description'\"  [(ngModel)]=\"updateSingle.foto_bast[i].desc\"></form-input>\r\n                                            </div>\r\n                                            \r\n                                        </div>\r\n                                </ng-container>\r\n\r\n                                <div class=\"add-more-container\">\r\n                                    <button (click)=\"addMoreUpload('bast')\" class=\"add-more-image\"><i class=\"fa fa-fw fa-plus-square\"></i> Tambah foto BAST</button>\r\n                                </div>\r\n                                \r\n                                <hr/>\r\n\r\n                                <label  class=\"label-bold\">Foto Surat Kuasa</label>\r\n                                <ng-container *ngIf=\"updateSingle.foto_surat_kuasa && isArray(updateSingle.foto_surat_kuasa) && updateSingle.foto_surat_kuasa.length > 0\">\r\n                                    <div class=\"member-detail-image-container\"  *ngFor=\"let itemImage of updateSingle.foto_surat_kuasa; let i = index\">\r\n                                        <div *ngIf=\"validURL(itemImage.url) && !loadingSuratKuasa[i]\" class=\"img-upload\">\r\n                                            <img src={{itemImage.url}} class=\"member-detail-image\" />\r\n                                        </div>\r\n    \r\n                                        <div class=\"form-group uploaded\" *ngIf=\"loadingSuratKuasa[i]\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                          </div>\r\n    \r\n                                        <div class=\"img-upload\">\r\n                                            <button class=\"btn_upload\" (click)=\"openUpload('suratKuasa', i)\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span *ngIf=\"validURL(itemImage.url); else upload_image\">Ubah</span>\r\n                                            </button>\r\n                                            <button  *ngIf=\"validURL(itemImage.url)\" class=\"btn_delete\" (click)=\"deleteImage('suratKuasa', i, itemImage.url)\">\r\n                                                <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                <span>Hapus</span>\r\n                                            </button>\r\n                                        </div>\r\n\r\n                                        <div *ngIf=\"validURL(itemImage.url)\" style=\"text-align: center;\">\r\n                                            <span>\r\n                                                <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                            </span>\r\n                                        </div>\r\n\r\n                                        <div >\r\n                                            <label>Description</label>\r\n                                            <form-input  name=\"'descSuratKuasa'+i\" [type]=\"'text'\" [placeholder]=\"'Description'\"  [(ngModel)]=\"updateSingle.foto_surat_kuasa[i].desc\"></form-input>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                </ng-container>\r\n\r\n                                <div class=\"add-more-container\">\r\n                                    <button (click)=\"addMoreUpload('suratKuasa')\" class=\"add-more-image\"><i class=\"fa fa-fw fa-plus-square\"></i> Tambah foto Surat Kuasa</button>\r\n                                </div>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    <ng-template #upload_image>\r\n        <span>\r\n            upload\r\n        </span>\r\n    </ng-template>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/show-password/show-password.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/data-member/show-password/show-password.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Show Password</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <label>ID Pelanggan</label>\r\n                                <form-input  name=\"id_pel\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'ID Pelanggan'\"  [(ngModel)]=\"updateSingle.username\" autofocus required></form-input>\r\n\r\n                                <label>Current Password</label>\r\n                                <form-input  name=\"password\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Current Password'\"  [(ngModel)]=\"updateSingle.password\" autofocus required></form-input>\r\n                \r\n                            </div>\r\n                        \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                \r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    <ng-template #upload_image>\r\n        <span>\r\n            upload\r\n        </span>\r\n    </ng-template>\r\n</div>"

/***/ }),

/***/ "./src/app/layout/modules/data-member/add/data-member.add.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/add/data-member.add.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.ng-pristine {\n  color: white !important;\n}\n\n.ng-pristine {\n  color: black !important;\n  width: 100%;\n  border-radius: 5px;\n  padding: 6px 0px;\n}\n\nlabel {\n  color: black;\n}\n\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 10px;\n  background-color: #3498db;\n  color: white;\n  height: 30px;\n  align-items: center;\n  display: flex;\n  right: 10px;\n  padding-top: 0px;\n  padding-right: 10px;\n  padding-bottom: 0;\n  padding-left: 10px;\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 0px;\n  border: 1px solid #f8f9fa;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.rounded-btn {\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n  margin-left: 40%;\n  margin-right: 50%;\n}\n\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGF0YS1tZW1iZXIvYWRkL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZGF0YS1tZW1iZXJcXGFkZFxcZGF0YS1tZW1iZXIuYWRkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9kYXRhLW1lbWJlci9hZGQvZGF0YS1tZW1iZXIuYWRkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVJO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQ0NSOztBREFRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0NaOztBREFZO0VBQ0ksaUJBQUE7QUNFaEI7O0FEQ1E7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNBWjs7QURHSTtFQUNJLGFBQUE7QUNEUjs7QURFUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNBWjs7QURJSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDRlI7O0FES1E7RUFDSSxrQkFBQTtBQ0haOztBREtRO0VBQ0ksZ0JBQUE7QUNIWjs7QURLUTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNIWjs7QURPUTtFQUNJLHNCQUFBO0FDTFo7O0FETVk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ0poQjs7QURVQTtFQUVRLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNKLGdCQUFBO0VBQ0EsaUJBQUE7QUNQSjs7QURTQTtFQUNRLG1CQUFBO0VBRUEsYUFBQTtBQ1BSIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGF0YS1tZW1iZXIvYWRkL2RhdGEtbWVtYmVyLmFkZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZy1kZWxldGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG5cclxuLm5nLXByaXN0aW5le1xyXG4gICAgY29sb3IgOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubmctcHJpc3RpbmV7XHJcbiAgICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcGFkZGluZzogNnB4IDBweDtcclxufVxyXG5cclxubGFiZWx7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDhweDtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICAvLyBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgICBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hbDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZjhmOWZhO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4ucm91bmRlZC1idG4ge1xyXG4gICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICM1NmE0ZmY7XHJcbiAgICBtYXJnaW4tbGVmdDogNDAlO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1MCU7XHJcbiAgICB9XHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogZGFya2VuKCM1NkE0RkYsIDE1JSk7XHJcbiAgICAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgfVxyXG4vLyAuZGF0ZXBpY2tlci1pbnB1dHtcclxuLy8gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsgICAgXHJcbi8vICAgICB9XHJcbi8vIC5mb3JtLWNvbnRyb2x7XHJcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vICAgICB0b3A6IDEwcHg7XHJcbi8vICAgICBsZWZ0OiAwcHg7XHJcbi8vIH1cclxuIiwiLmJnLWRlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG5cbi5uZy1wcmlzdGluZSB7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xufVxuXG4ubmctcHJpc3RpbmUge1xuICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZzogNnB4IDBweDtcbn1cblxubGFiZWwge1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWluLWhlaWdodDogNDhweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICByaWdodDogMTBweDtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDA7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBwYWRkaW5nOiAwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmOGY5ZmE7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5yb3VuZGVkLWJ0biB7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG4gIG1hcmdpbi1sZWZ0OiA0MCU7XG4gIG1hcmdpbi1yaWdodDogNTAlO1xufVxuXG4ucm91bmRlZC1idG46aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMGE3YmZmO1xuICBvdXRsaW5lOiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/data-member/add/data-member.add.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/add/data-member.add.component.ts ***!
  \*****************************************************************************/
/*! exports provided: DataMemberAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataMemberAddComponent", function() { return DataMemberAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var DataMemberAddComponent = /** @class */ (function () {
    function DataMemberAddComponent(memberService) {
        this.memberService = memberService;
        this.name = "";
        this.Members = [];
        this.form = {
            username: "",
            id: "",
            full_name: "",
            email: "",
            password: "",
            cell_phone: "",
            card_number: "",
            dob: "",
            mcity: "",
            mcountry: "",
            maddress1: "",
            mpostcode: "",
            gender: "",
            member_status: "",
            marital_status: "",
            address2: "",
        };
        this.errorLabel = false;
        this.member_status_type = [
            { label: 'Superuser', value: 'superuser' },
            { label: 'Member', value: 'member', selected: 1 },
            { label: 'Marketing', value: 'marketing' },
            { label: 'Admin', value: 'admin' },
            { label: 'Merchant', value: 'merchant' }
        ];
        this.gender_type = [
            { label: 'Famele', value: 'female' },
            { label: 'Male', value: 'male' }
        ];
        this.marital_status_type = [
            { label: 'Married', value: 'married' },
            { label: 'Unmarried', value: 'unmarried' }
        ];
        // let form_add_test     : any = [
        //   { label:"Member ID",  type: "text",  value: "", data_binding: 'id'  },
        //   { label:"Full Name",  type: "text",  value: "", data_binding: 'full_name'  },
        //   { label:"Email",  type: "text",  value: "", data_binding: 'email'  },
        //   { label:"Password",  type: "password",  value: "", data_binding: 'password'  },
        //   { label:"Cell Phone",  type: "text",  value: "", data_binding: 'cell_phone'  },
        //   { label:"Card Number ",  type: "text",  value: "", data_binding: 'card_number' },
        //   { label:"Date of Birth",  type: "text",  value: "", data_binding: 'dob' },
        //   { label:"City",  type: "text",  value: "", data_binding: 'mcity' },
        //   { label:"State",  type: "text",  value: "", data_binding: 'mstate' },
        //   { label:"Country",  type: "text",  value: "", data_binding: 'mcountry' },
        //   { label:"Address",  type: "textarea",  value: "", data_binding: 'maddress1' },
        //   { label:"Post Code",  type: "text",  value: "", data_binding: 'mpostcode' },
        //   { label:"Gender",  type: "text",  value: "", data_binding: 'gender' },
        //   { label:"Member Status",  type: "text",  value: "", data_binding: 'member_status' },
        //   { label:"Marital Status",  type: "text",  value: "", data_binding: 'marital_status' },
        //   // { label:"type ",  type: "textarea",  value: [{label:"label", value:"label", selected:1},{label:"label1", value:"label2"}], data_binding: 'address2' },
        //   { label:"address 2 ",  type: "textarea",  value: "", data_binding: 'address2' },
        // ];
    }
    DataMemberAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    DataMemberAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    DataMemberAddComponent.prototype.formSubmitAddMember = function () {
        // console.log(JSON.stringify(form));
        // console.log(form.password)
        var form_add = this.form;
        if (!form_add.password || form_add.password.length < 8) {
            alert("password must be longer than 8 character");
        }
        else {
            try {
                this.service = this.memberService;
                var dob = void 0;
                var dataForm = void 0;
                if (form_add.dob) {
                    var month = form_add.dob.month.toString().padStart(2, '0');
                    var day = form_add.dob.day.toString().padStart(2, '0');
                    dob = form_add.dob.year + "-" + month + "-" + day;
                    dataForm = __assign({}, form_add, { dob: dob });
                }
                else {
                    dataForm = form_add;
                }
                console.log(dataForm);
                var result = this.memberService.addNewMember(dataForm);
                this.Members = result.result;
                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire("Member Has Been Added!");
            }
            catch (e) {
                this.errorLabel = (e.message); //conversion to Error type
                var message = this.errorLabel;
                if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                    message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                }
                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    icon: 'error',
                    title: message,
                });
            }
        }
    };
    DataMemberAddComponent.prototype.backTo = function () {
        window.history.back();
    };
    DataMemberAddComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] }
    ]; };
    DataMemberAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-data-member-add',
            template: __webpack_require__(/*! raw-loader!./data-member.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/add/data-member.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./data-member.add.component.scss */ "./src/app/layout/modules/data-member/add/data-member.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"]])
    ], DataMemberAddComponent);
    return DataMemberAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/data-member/change-password/change-password.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/change-password/change-password.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn-change-password {\n  width: 100%;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n.container-two-button {\n  display: flex;\n}\n.file-selected {\n  color: red;\n}\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n.btn-upload {\n  margin-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGF0YS1tZW1iZXIvY2hhbmdlLXBhc3N3b3JkL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZGF0YS1tZW1iZXJcXGNoYW5nZS1wYXNzd29yZFxcY2hhbmdlLXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9kYXRhLW1lbWJlci9jaGFuZ2UtcGFzc3dvcmQvY2hhbmdlLXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksa0JBQUE7QUNBUjtBRENRO0VBRUksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNEWjtBREVZO0VBQ0ksaUJBQUE7QUNBaEI7QURHUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBS0EsV0FBQTtBQ05aO0FER1k7RUFDSSxpQkFBQTtBQ0RoQjtBREtRO0VBQ0kseUJBQUE7QUNIWjtBRE1JO0VBQ0ksYUFBQTtBQ0pSO0FETVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDSlo7QURRSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDTlI7QURVUTtFQUNJLGdCQUFBO0FDUlo7QURVUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNSWjtBRFVRO0VBQ0ksZ0JBQUE7QUNSWjtBRFNZO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDUGhCO0FEU1k7RUFDSSx5QkFBQTtBQ1BoQjtBRFNZO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ1BoQjtBRFNZO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ1BoQjtBRFFnQjtFQUNJLGVBQUE7RUFDQSxXQUFBO0FDTnBCO0FEWVE7RUFDSSxzQkFBQTtBQ1ZaO0FEV1k7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ1RoQjtBRGdCQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDYko7QURrQkE7RUFDSSxnQkFBQTtFQUVBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUNmSjtBRGlCQTtFQUNJLG1CQUFBO0VBRUEsYUFBQTtBQ2ZKO0FEa0JBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2ZKO0FEZ0JJO0VBQ0ksYUFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ2RSO0FEZ0JRO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ2RaO0FEa0JJO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNoQlI7QURpQlE7RUFDSSxZQUFBO0VBRUEsV0FBQTtBQ2hCWjtBRGlCWTtFQUVJLFFBQUE7QUNoQmhCO0FEdUJBO0VBQ0kseUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUNwQko7QUR1QkE7RUFDSSxXQUFBO0FDcEJKO0FEdUJBO0VBQ0ksd0JBQUE7QUNwQko7QUR1QkE7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNwQko7QUR1QkE7RUFDSSxnQkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGdCQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGFBQUE7QUNwQko7QUR1QkE7RUFDSSxVQUFBO0FDcEJKO0FEdUJBO0VBQ0ksbUJBQUE7QUNwQko7QUR1QkE7RUFDSSxrQkFBQTtBQ3BCSiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2RhdGEtbWVtYmVyL2NoYW5nZS1wYXNzd29yZC9jaGFuZ2UtcGFzc3dvcmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbi50cnVle1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgIFxyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgIFxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW1hZ2V7XHJcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgID5kaXZ7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzMuMzMzMyU7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGgze1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgICAgIGltZ3tcclxuICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnJvdW5kZWQtYnRuIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDQwJTtcclxuICAgIC8vIG1hcmdpbi1yaWdodDogNTAlO1xyXG59XHJcblxyXG4ucm91bmRlZC1idG4tZGFuZ2VyIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBjb2xvcjogIzU1NTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG59XHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2QTRGRiwgMTUlKTtcclxuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBjb2xvcjogIzY2NjtcclxuICAgIC5pbWctdXBsb2FkIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOjIwcHggMTBweDtcclxuXHJcbiAgICAgICAgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xyXG4gICAgICAgICAgICB3aWR0aDoyMDBweDtcclxuICAgICAgICAgICAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAubG9hZGluZyB7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcclxuICAgICAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICAgICAgLnNrLWZhZGluZy1jaXJjbGUge1xyXG4gICAgICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjpyZWQ7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIGJvcmRlcjowO1xyXG4gICAgY3Vyc29yOnBvaW50ZXI7XHJcbn1cclxuXHJcbmJ1dHRvbi5idG4tY2hhbmdlLXBhc3N3b3JkIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5idXR0b24uYnRuX2RlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcclxufVxyXG5cclxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5sYWJlbC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuXHJcbi5lcnJvci1tc2ctdXBsb2FkIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jb250YWluZXItdHdvLWJ1dHRvbiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4uZmlsZS1zZWxlY3RlZCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4uYnRuLWFkZC1idWxrIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5idG4tdXBsb2FkIHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxufSIsIi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2Uge1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSA+IGRpdiB7XG4gIHdpZHRoOiAzMy4zMzMzJTtcbiAgcGFkZGluZzogNXB4O1xuICBmbG9hdDogbGVmdDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgaDMge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IGltZyB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5yb3VuZGVkLWJ0biB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG59XG5cbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBjb2xvcjogIzU1NTtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi5yb3VuZGVkLWJ0bjpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMwYTdiZmY7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDIwcHggMTBweDtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCAubWVtYmVyLWRldGFpbC1pbWFnZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyAuc2stZmFkaW5nLWNpcmNsZSB7XG4gIHRvcDogNTAlO1xufVxuXG5idXR0b24uYnRuX3VwbG9hZCwgYnV0dG9uLmFkZC1tb3JlLWltYWdlLCBidXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiA1cHggMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm9yZGVyOiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbmJ1dHRvbi5idG4tY2hhbmdlLXBhc3N3b3JkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmJ1dHRvbi5idG5fZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogc2FsbW9uO1xufVxuXG4uYWRkLW1vcmUtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ubGFiZWwtYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5lcnJvci1tc2ctdXBsb2FkIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcGFkZGluZy1sZWZ0OiAwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5jb250YWluZXItdHdvLWJ1dHRvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5maWxlLXNlbGVjdGVkIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLmJ0bi1hZGQtYnVsayB7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5idG4tdXBsb2FkIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/data-member/change-password/change-password.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/change-password/change-password.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ChangePasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordComponent", function() { return ChangePasswordComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





// import { ProductService } from '../../../../services/product/product.service';
var ChangePasswordComponent = /** @class */ (function () {
    function ChangePasswordComponent(memberService, productService, zone) {
        this.memberService = memberService;
        this.productService = productService;
        this.zone = zone;
        this.errorLabel = false;
        this.prodOnUpload = false;
        this.showUploadButton = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.allBast = [];
        this.allSuratKuasa = [];
        this.optionsKuasa1 = [
            { label: "KUASA", value: "true" },
            { label: "TIDAK KUASA", value: "false" }
        ];
        this.optionsKuasa2 = [
            { label: "KUASA", value: "KUASA" },
            { label: "TIDAK KUASA", value: "TIDAK KUASA" }
        ];
        this.optionsKuasa3 = [
            { label: "KUASA", value: "ya" },
            { label: "TIDAK KUASA", value: "tidak" }
        ];
        this.loading = false;
        this.activationStatus = [
            { label: 'ACTIVATED', value: 'ACTIVATED' },
            { label: 'INACTIVED', value: 'INACTIVED' }
        ];
        this.memberStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
        this.loadingKTPPemilik = false;
        this.loadingNPWPPemilik = false;
        this.loadingKTPPenerima = false;
        this.loadingNPWPPenerima = false;
        this.loadingBAST = [];
        this.loadingSuratKuasa = [];
        this.password = "";
        this.showPasswordPage = false;
        this.dataLogin = {
            username: "",
            password: "",
        };
    }
    ChangePasswordComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ChangePasswordComponent.prototype.checkValueKuasa = function () {
        var _this = this;
        if (this.updateSingle["hadiah_dikuasakan"] == "true" || this.updateSingle["hadiah_dikuasakan"] == "false") {
            var options = this.optionsKuasa1.map(function (el) {
                if (el.value == _this.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa1 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "KUASA" || this.updateSingle["hadiah_dikuasakan"] == "TIDAK KUASA") {
            var options = this.optionsKuasa2.map(function (el) {
                if (el.value == _this.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa2 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "ya" || this.updateSingle["hadiah_dikuasakan"] == "tidak") {
            var options = this.optionsKuasa3.map(function (el) {
                if (el.value == _this.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa3 = options;
        }
    };
    ChangePasswordComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    ChangePasswordComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    ChangePasswordComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.updateSingle = JSON.parse(JSON.stringify(this.detail.input_form_data));
                this.updateSingle.foto_bast = this.isArray(this.updateSingle.foto_bast) ? this.updateSingle.foto_bast : [];
                this.allBast = this.updateSingle.foto_bast;
                this.updateSingle.foto_surat_kuasa = this.isArray(this.updateSingle.foto_surat_kuasa) ? this.updateSingle.foto_surat_kuasa : [];
                this.allSuratKuasa = this.updateSingle.foto_surat_kuasa;
                this.assignArrayBoolean(this.allBast, this.loadingBAST);
                this.assignArrayBoolean(this.allSuratKuasa, this.loadingSuratKuasa);
                // console.log("loadingBAST", this.loadingBAST);
                // this.assignArrayBoolean(this.allSuratKuasa);
                // console.log("count", this.allBast, this.allSuratKuasa);
                this.checkValueKuasa();
                this.detail.previous_member_id = this.detail.member_id;
                this.activationStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this.detail.activation_status) {
                        _this.activationStatus[index].selected = 1;
                    }
                });
                this.detail.previous_member_id = this.detail.member_id;
                this.memberStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this.detail.member_status) {
                        _this.memberStatus[index].selected = 1;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    ChangePasswordComponent.prototype.backToDetail = function () {
        // console.log("Edit member",this.back);
        this.back[0][this.back[1]]();
        // this.back[0]();
    };
    ChangePasswordComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    ChangePasswordComponent.prototype.openUpload = function (type, index) {
        var upFile = document.getElementById(type + index);
        upFile.click();
    };
    ChangePasswordComponent.prototype.saveNewPassword = function (idCustomer) {
        return __awaiter(this, void 0, void 0, function () {
            var dataLogin, result, e_1, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        console.warn("id customer", idCustomer);
                        dataLogin = {
                            username: [idCustomer]
                        };
                        return [4 /*yield*/, this.memberService.resetPasswordChangePassword(dataLogin)];
                    case 1:
                        result = _a.sent();
                        if (result && result.data) {
                            this.dataLogin.username = result.data.username;
                            this.dataLogin.password = result.data.password;
                        }
                        this.detail = JSON.parse(JSON.stringify(this.updateSingle));
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            title: 'Success',
                            text: 'Password berhasil disimpan',
                            icon: 'success',
                            confirmButtonText: 'Ok',
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                _this.showThisPassword();
                                // this.backToDetail();
                            }
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ChangePasswordComponent.prototype.showThisPassword = function () {
        this.showPasswordPage = !this.showPasswordPage;
        if (this.showPasswordPage == false) {
            this.firstLoad();
        }
        // console.log(this.edit );
    };
    ChangePasswordComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        // var reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]); //
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
        console.log('file', this.selectedFile);
    };
    ChangePasswordComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    ChangePasswordComponent.prototype.actionShowUploadButton = function () {
        this.showUploadButton = !this.showUploadButton;
    };
    ChangePasswordComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    ChangePasswordComponent.prototype.updateDataBulk = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        payload = {
                            type: 'application/form-dataLogin',
                        };
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        console.log('file', this.selectedFile, this);
                        return [4 /*yield*/, this.memberService.registerBulkMember(this.selectedFile, this, payload)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ChangePasswordComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    ChangePasswordComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ChangePasswordComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ChangePasswordComponent.prototype, "back", void 0);
    ChangePasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-change-password',
            template: __webpack_require__(/*! raw-loader!./change-password.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/change-password/change-password.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./change-password.component.scss */ "./src/app/layout/modules/data-member/change-password/change-password.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"], _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], ChangePasswordComponent);
    return ChangePasswordComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/data-member/data-member-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/data-member-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: DataMemberRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataMemberRoutingModule", function() { return DataMemberRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _data_member_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./data-member.component */ "./src/app/layout/modules/data-member/data-member.component.ts");
/* harmony import */ var _add_data_member_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/data-member.add.component */ "./src/app/layout/modules/data-member/add/data-member.add.component.ts");
/* harmony import */ var _detail_data_member_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detail/data-member.detail.component */ "./src/app/layout/modules/data-member/detail/data-member.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '', component: _data_member_component__WEBPACK_IMPORTED_MODULE_2__["DataMemberComponent"],
    },
    {
        path: 'add', component: _add_data_member_add_component__WEBPACK_IMPORTED_MODULE_3__["DataMemberAddComponent"]
    },
    {
        path: 'detail', component: _detail_data_member_detail_component__WEBPACK_IMPORTED_MODULE_4__["DataMemberDetailComponent"]
    },
];
var DataMemberRoutingModule = /** @class */ (function () {
    function DataMemberRoutingModule() {
    }
    DataMemberRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DataMemberRoutingModule);
    return DataMemberRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/data-member/data-member.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/data-member/data-member.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "iframe {\n  width: 0px;\n  height: 0px;\n  border: 0px medium none;\n  visibility: hidden;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGF0YS1tZW1iZXIvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxkYXRhLW1lbWJlclxcZGF0YS1tZW1iZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2RhdGEtbWVtYmVyL2RhdGEtbWVtYmVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksVUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0NKOztBRENJO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ0NSIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGF0YS1tZW1iZXIvZGF0YS1tZW1iZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpZnJhbWV7XHJcbiAgICB3aWR0aDowcHg7XHJcbiAgICBoZWlnaHQ6IDBweDtcclxuICAgIGJvcmRlcjowcHggbWVkaXVtIG5vbmU7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbn1cclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn0iLCJpZnJhbWUge1xuICB3aWR0aDogMHB4O1xuICBoZWlnaHQ6IDBweDtcbiAgYm9yZGVyOiAwcHggbWVkaXVtIG5vbmU7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbn1cblxuLmVycm9yLW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDI2cHg7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLmVycm9yLW1lc3NhZ2UgLnRleHQtbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6ICNFNzRDM0M7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIG1hcmdpbjogMCAxMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/data-member/data-member.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/data-member/data-member.component.ts ***!
  \*********************************************************************/
/*! exports provided: DataMemberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataMemberComponent", function() { return DataMemberComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;
var DataMemberComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function DataMemberComponent(memberService, sanitizer) {
        this.memberService = memberService;
        this.sanitizer = sanitizer;
        this.Members = [];
        this.row_id = "_id";
        this.tableFormat = {
            title: 'Member Report',
            label_headers: [
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
                { label: 'Nama Pelanggan', visible: true, type: 'string', data_row_name: 'full_name' },
                { label: 'No. WA Pelanggan', visible: true, type: 'string', data_row_name: 'cell_phone' },
                // {label: 'Penerima Hadiah',     visible: true, type: 'penerima_hadiah', data_row_name: 'member_address'},
                // {label: 'No. Telp Penerima',     visible: true, type: 'telp_penerima', data_row_name: 'member_address'},
                // {label: 'Alamat Penerima',     visible: true, type: 'alamat_penerima', data_row_name: 'member_address'},
                // {label: 'Desa/Kelurahan',     visible: true, type: 'kelurahan_penerima', data_row_name: 'member_address'},
                // {label: 'Kecamatan',     visible: true, type: 'kecamatan_penerima', data_row_name: 'member_address'},
                // {label: 'Kabupaten/Kota',     visible: true, type: 'kota_penerima', data_row_name: 'member_address'},
                // {label: 'Provinsi',     visible: true, type: 'provinsi_penerima', data_row_name: 'member_address'},
                // {label: 'Kode Pos',     visible: true, type: 'kode_pos_penerima', data_row_name: 'member_address'},
                { label: 'Point Balance', visible: true, type: 'string', data_row_name: 'point_balance' },
                // {label: 'Point Expiry Date',     visible: true, type: 'expire_date', data_row_name: 'points'},
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date' },
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
            ],
            row_primary_key: '_id',
            formOptions: {
                addForm: true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'Members',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.errorMessage = false;
        this.memberDetail = false;
        this.tableFormat.formOptions.addForm = false;
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    DataMemberComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    DataMemberComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        // let params = {
                        //   'type':'member'
                        // }
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getMemberAllReport()];
                    case 1:
                        result = _a.sent();
                        // console.warn("data member", result);
                        this.totalPage = result.total_page;
                        this.Members = result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DataMemberComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    // this.service    = this.memberService;
                    // let result: any = await this.memberService.getMemberReportByID(_id);
                    // this.memberDetail = result.values[0];
                    this.memberDetail = this.Members.find(function (member) { return member.username == rowData.username; });
                    // console.warn("Member detail",this.memberDetail);
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    DataMemberComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.memberDetail = false;
                obj.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    DataMemberComponent.prototype.onDownload = function (downloadLint) {
        var srcDownload = downloadLint;
        //let srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
        //console.log(this.srcDownload);
        this.getDownloadFileLint();
    };
    DataMemberComponent.prototype.getDownloadFileLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, downloadLint, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        result = void 0;
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getDownloadFileLint()];
                    case 1:
                        result = _a.sent();
                        downloadLint = result.result;
                        //console.log(downloadLint);
                        //To running other subfunction with together automatically
                        this.onDownload(downloadLint);
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DataMemberComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
    ]; };
    DataMemberComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-data-member',
            template: __webpack_require__(/*! raw-loader!./data-member.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/data-member.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./data-member.component.scss */ "./src/app/layout/modules/data-member/data-member.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
    ], DataMemberComponent);
    return DataMemberComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/data-member/data-member.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/modules/data-member/data-member.module.ts ***!
  \******************************************************************/
/*! exports provided: DataMemberModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataMemberModule", function() { return DataMemberModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _data_member_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./data-member.component */ "./src/app/layout/modules/data-member/data-member.component.ts");
/* harmony import */ var _add_data_member_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/data-member.add.component */ "./src/app/layout/modules/data-member/add/data-member.add.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _data_member_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./data-member-routing.module */ "./src/app/layout/modules/data-member/data-member-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _detail_data_member_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./detail/data-member.detail.component */ "./src/app/layout/modules/data-member/detail/data-member.detail.component.ts");
/* harmony import */ var _edit_data_member_edit_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./edit/data-member.edit.component */ "./src/app/layout/modules/data-member/edit/data-member.edit.component.ts");
/* harmony import */ var _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./change-password/change-password.component */ "./src/app/layout/modules/data-member/change-password/change-password.component.ts");
/* harmony import */ var _show_password_show_password_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./show-password/show-password.component */ "./src/app/layout/modules/data-member/show-password/show-password.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var DataMemberModule = /** @class */ (function () {
    function DataMemberModule() {
    }
    DataMemberModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _data_member_routing_module__WEBPACK_IMPORTED_MODULE_8__["DataMemberRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_9__["BsComponentModule"]
            ],
            declarations: [
                _data_member_component__WEBPACK_IMPORTED_MODULE_2__["DataMemberComponent"],
                _add_data_member_add_component__WEBPACK_IMPORTED_MODULE_3__["DataMemberAddComponent"],
                _detail_data_member_detail_component__WEBPACK_IMPORTED_MODULE_10__["DataMemberDetailComponent"],
                _edit_data_member_edit_component__WEBPACK_IMPORTED_MODULE_11__["DataMemberEditComponent"],
                _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_12__["ChangePasswordComponent"],
                _show_password_show_password_component__WEBPACK_IMPORTED_MODULE_13__["ShowPasswordComponent"]
            ],
        })
    ], DataMemberModule);
    return DataMemberModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/data-member/detail/data-member.detail.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/detail/data-member.detail.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.member-detail-image-container a {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.member-detail-image-container a .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.member-detail-image-container .btn-download-container {\n  margin-top: 5px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\nbutton.btn-download-img {\n  margin-top: 10px;\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGF0YS1tZW1iZXIvZGV0YWlsL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZGF0YS1tZW1iZXJcXGRldGFpbFxcZGF0YS1tZW1iZXIuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9kYXRhLW1lbWJlci9kZXRhaWwvZGF0YS1tZW1iZXIuZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FEQ0E7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDRUo7O0FEREk7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ0dSOztBREZRO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ0laOztBRERJO0VBQ0ksZUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDR1I7O0FERUE7RUFDSSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0FDQ0o7O0FER0k7RUFDSSxrQkFBQTtBQ0FSOztBRENRO0VBRUksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNEWjs7QURFWTtFQUNJLGlCQUFBO0FDQWhCOztBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBRUEsV0FBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBSUEsV0FBQTtBQ0xaOztBREVZO0VBQ0ksaUJBQUE7QUNBaEI7O0FESVE7RUFDSSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDRlo7O0FES0k7RUFDSSxhQUFBO0FDSFI7O0FESVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDRlo7O0FEaUJJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNmUjs7QURrQlE7RUFDSSxrQkFBQTtBQ2hCWjs7QURrQlE7RUFDSSxnQkFBQTtBQ2hCWjs7QURrQlE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDaEJaOztBRG1CUTtFQUNJLHNCQUFBO0FDakJaOztBRGtCWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDaEJoQjs7QUR3QkE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ3JCSjs7QUR1Qkk7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDckJSIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGF0YS1tZW1iZXIvZGV0YWlsL2RhdGEtbWVtYmVyLmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZy1kZWxldGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBjb2xvcjogIzY2NjtcclxuICAgIGEge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuYnRuLWRvd25sb2FkLWNvbnRhaW5lciB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDo1cHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuYnV0dG9uLmJ0bi1kb3dubG9hZC1pbWcge1xyXG4gICAgbWFyZ2luLXRvcDoxMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDE1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgYm9yZGVyOjA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxufVxyXG5cclxuLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5lZGl0X2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5idG4tcmlnaHQge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAvLyAuaW1hZ2V7XHJcbiAgICAvLyAgICAgaDN7XHJcbiAgICAvLyAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIC8vICAgICB9XHJcbiAgICAvLyAgICAgLmNhcmQtY29udGVudHtcclxuICAgIC8vICAgICAgICAgaW1ne1xyXG4gICAgLy8gICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgLy8gICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgLy8gICAgICAgICB9XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gfVxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgICBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5cclxuLmVycm9yLW1lc3NhZ2Uge1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBoZWlnaHQ6MTAwdmg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6MjZweDtcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG5cclxuICAgIC50ZXh0LW1lc3NhZ2Uge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNFNzRDM0M7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICBtYXJnaW46IDAgMTBweDtcclxuICAgIH1cclxufSIsIi5iZy1kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgYSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIGEgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5idG4tZG93bmxvYWQtY29udGFpbmVyIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuYnV0dG9uLmJ0bi1kb3dubG9hZC1pbWcge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogNXB4IDE1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJvcmRlcjogMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJ0bi1yaWdodCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5lcnJvci1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5lcnJvci1tZXNzYWdlIC50ZXh0LW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xuICBwYWRkaW5nOiAyMHB4O1xuICBtYXJnaW46IDAgMTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/data-member/detail/data-member.detail.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/detail/data-member.detail.component.ts ***!
  \***********************************************************************************/
/*! exports provided: DataMemberDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataMemberDetailComponent", function() { return DataMemberDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var DataMemberDetailComponent = /** @class */ (function () {
    function DataMemberDetailComponent(memberService, router, route) {
        this.memberService = memberService;
        this.router = router;
        this.route = route;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__;
        this.mci_project = false;
        this.edit = false;
        this.isChangePassword = false;
        this.isShowPassword = false;
        this.errorLabel = false;
        this.errorMessage = false;
    }
    DataMemberDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    DataMemberDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this, id_pel, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        program = localStorage.getItem('programName');
                        _this = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program) {
                                if (element.type == "reguler") {
                                    _this.mci_project = true;
                                }
                                else {
                                    _this.mci_project = false;
                                }
                            }
                        });
                        id_pel = this.detail.username;
                        this.memberPassword = this.detail.password;
                        this.memberDetail = this.detail;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.memberService.getFormMemberByID(id_pel)];
                    case 2:
                        result = _a.sent();
                        if (result.length > 0) {
                            if (result[0].username == this.detail.username) {
                                this.memberDetail = result[0];
                                this.memberDetail.password = this.memberPassword;
                                // console.warn("member detail data", this.memberDetail)
                            }
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DataMemberDetailComponent.prototype.downloadImage = function (url) {
        var filename;
        filename = this.lastPathURL(url);
        fetch(url).then(function (t) {
            return t.blob().then(function (b) {
                var a = document.createElement("a");
                a.href = URL.createObjectURL(b);
                a.setAttribute("download", filename);
                a.click();
            });
        });
    };
    DataMemberDetailComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    DataMemberDetailComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    DataMemberDetailComponent.prototype.lastPathURL = function (url) {
        var resURL = url.split("/");
        console.log("resURL", resURL);
        var lastNumber = resURL.length - 1;
        return resURL[lastNumber];
    };
    DataMemberDetailComponent.prototype.editThis = function () {
        // console.log(this.edit );
        this.edit = !this.edit;
        if (this.edit == false) {
            this.firstLoad();
        }
        // console.log(this.edit );
    };
    DataMemberDetailComponent.prototype.changePassword = function () {
        this.isChangePassword = !this.isChangePassword;
        if (this.isChangePassword == false) {
            this.firstLoad();
        }
    };
    DataMemberDetailComponent.prototype.showPassword = function () {
        this.isShowPassword = !this.isShowPassword;
        if (this.isShowPassword == false) {
            this.firstLoad();
        }
    };
    DataMemberDetailComponent.prototype.backToTable = function () {
        // console.log("back member detail",this.back);
        this.back[1](this.back[0]);
    };
    DataMemberDetailComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    DataMemberDetailComponent.prototype.delete = function () {
        return __awaiter(this, void 0, void 0, function () {
            var delResult, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.memberService.deleteMember(this.detail)];
                    case 1:
                        delResult = _a.sent();
                        // console.log(delResult);
                        if (delResult.error == false) {
                            console.log(this.back[0]);
                            this.back[0].memberDetail = false;
                            this.back[0].firstLoad();
                            // delete this.back[0].prodDetail;
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        throw new TypeError(error_1.error.error);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DataMemberDetailComponent.prototype.catch = function (e) {
        this.errorLabel = (e.message); //conversion to Error type
    };
    DataMemberDetailComponent.prototype.pointHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log('here');
                // this.router.navigate(['/member-point-history']);
                this.router.navigate(['administrator/memberadmin/point-history'], { queryParams: { id: this.detail.email } });
                return [2 /*return*/];
            });
        });
    };
    DataMemberDetailComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DataMemberDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DataMemberDetailComponent.prototype, "back", void 0);
    DataMemberDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-data-member-detail',
            template: __webpack_require__(/*! raw-loader!./data-member.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/detail/data-member.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./data-member.detail.component.scss */ "./src/app/layout/modules/data-member/detail/data-member.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], DataMemberDetailComponent);
    return DataMemberDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/data-member/edit/data-member.edit.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/edit/data-member.edit.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGF0YS1tZW1iZXIvZWRpdC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGRhdGEtbWVtYmVyXFxlZGl0XFxkYXRhLW1lbWJlci5lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9kYXRhLW1lbWJlci9lZGl0L2RhdGEtbWVtYmVyLmVkaXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtBQ0FSO0FEQ1E7RUFFSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTlo7QURHWTtFQUNJLGlCQUFBO0FDRGhCO0FES1E7RUFDSSx5QkFBQTtBQ0haO0FETUk7RUFDSSxhQUFBO0FDSlI7QURNUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNKWjtBRFFJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNOUjtBRFVRO0VBQ0ksZ0JBQUE7QUNSWjtBRFVRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ1JaO0FEVVE7RUFDSSxnQkFBQTtBQ1JaO0FEU1k7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNQaEI7QURTWTtFQUNJLHlCQUFBO0FDUGhCO0FEU1k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDUGhCO0FEU1k7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDUGhCO0FEUWdCO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNOcEI7QURZUTtFQUNJLHNCQUFBO0FDVlo7QURXWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDVGhCO0FEZ0JBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2JKO0FEY0k7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDWlI7QURjUTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0tBQUEsbUJBQUE7QUNaWjtBRGdCSTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDZFI7QURlUTtFQUNJLFlBQUE7RUFFQSxXQUFBO0FDZFo7QURlWTtFQUVJLFFBQUE7QUNkaEI7QURxQkE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ2xCSjtBRHFCQTtFQUNJLHdCQUFBO0FDbEJKO0FEcUJBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDbEJKO0FEcUJBO0VBQ0ksZ0JBQUE7QUNsQkoiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9kYXRhLW1lbWJlci9lZGl0L2RhdGEtbWVtYmVyLmVkaXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbi50cnVle1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgIFxyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgIFxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW1hZ2V7XHJcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgID5kaXZ7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzMuMzMzMyU7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGgze1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgICAgIGltZ3tcclxuICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcclxuICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWE7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgY29sb3I6ICM2NjY7XHJcbiAgICAuaW1nLXVwbG9hZCB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbjoyMHB4IDEwcHg7XHJcblxyXG4gICAgICAgIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcclxuICAgICAgICAgICAgd2lkdGg6MjAwcHg7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuZm9ybS1ncm91cC51cGxvYWRlZCB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgLmxvYWRpbmcge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XHJcbiAgICAgICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgICAgIC5zay1mYWRpbmctY2lyY2xlIHtcclxuICAgICAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6cmVkO1xyXG4gICAgICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5idXR0b24uYnRuX3VwbG9hZCwgYnV0dG9uLmFkZC1tb3JlLWltYWdlLCBidXR0b24uYnRuX2RlbGV0ZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBib3JkZXI6MDtcclxuICAgIGN1cnNvcjpwb2ludGVyO1xyXG59XHJcblxyXG5idXR0b24uYnRuX2RlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcclxufVxyXG5cclxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5sYWJlbC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuIiwiLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlID4gZGl2IHtcbiAgd2lkdGg6IDMzLjMzMzMlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGZsb2F0OiBsZWZ0O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSBoMyB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQgaW1nIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5pbWctdXBsb2FkIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbjogMjBweCAxMHB4O1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5pbWctdXBsb2FkIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcbiAgd2lkdGg6IDIwMHB4O1xuICBtYXgtaGVpZ2h0OiAyMDBweDtcbiAgb2JqZWN0LWZpdDogY29udGFpbjtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIC5zay1mYWRpbmctY2lyY2xlIHtcbiAgdG9wOiA1MCU7XG59XG5cbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXI6IDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuYnV0dG9uLmJ0bl9kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBzYWxtb247XG59XG5cbi5hZGQtbW9yZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5sYWJlbC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/data-member/edit/data-member.edit.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/edit/data-member.edit.component.ts ***!
  \*******************************************************************************/
/*! exports provided: DataMemberEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataMemberEditComponent", function() { return DataMemberEditComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





// import { ProductService } from '../../../../services/product/product.service';

var DataMemberEditComponent = /** @class */ (function () {
    function DataMemberEditComponent(memberService, productService, zone) {
        this.memberService = memberService;
        this.productService = productService;
        this.zone = zone;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__;
        this.errorLabel = false;
        this.allBast = [];
        this.allSuratKuasa = [];
        this.optionsKuasa1 = [
            { label: "KUASA", value: "true" },
            { label: "TIDAK KUASA", value: "false" }
        ];
        this.optionsKuasa2 = [
            { label: "KUASA", value: "KUASA" },
            { label: "TIDAK KUASA", value: "TIDAK KUASA" }
        ];
        this.optionsKuasa3 = [
            { label: "KUASA", value: "ya" },
            { label: "TIDAK KUASA", value: "tidak" }
        ];
        this.loading = false;
        this.activationStatus = [
            { label: 'ACTIVATED', value: 'ACTIVATED' },
            { label: 'INACTIVED', value: 'INACTIVED' }
        ];
        this.memberStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
        this.errorFile = false;
        this.loadingKTPPemilik = false;
        this.loadingNPWPPemilik = false;
        this.loadingKTPPenerima = false;
        this.loadingNPWPPenerima = false;
        this.loadingBAST = [];
        this.loadingSuratKuasa = [];
        this.mci_project = false;
    }
    DataMemberEditComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    DataMemberEditComponent.prototype.checkValueKuasa = function () {
        var _this_1 = this;
        if (this.updateSingle["hadiah_dikuasakan"] == "true" || this.updateSingle["hadiah_dikuasakan"] == "false") {
            var options = this.optionsKuasa1.map(function (el) {
                if (el.value == _this_1.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa1 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "KUASA" || this.updateSingle["hadiah_dikuasakan"] == "TIDAK KUASA") {
            var options = this.optionsKuasa2.map(function (el) {
                if (el.value == _this_1.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa2 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "ya" || this.updateSingle["hadiah_dikuasakan"] == "tidak") {
            var options = this.optionsKuasa3.map(function (el) {
                if (el.value == _this_1.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa3 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "-") {
            console.warn("im here!");
            var options = this.optionsKuasa2.map(function (el) {
                if (el.value == _this_1.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa2 = options;
        }
    };
    DataMemberEditComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    DataMemberEditComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    DataMemberEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this;
            var _this_1 = this;
            return __generator(this, function (_a) {
                program = localStorage.getItem('programName');
                _this = this;
                this.contentList.filter(function (element) {
                    if (element.appLabel == program) {
                        if (element.type == "reguler") {
                            _this.mci_project = true;
                        }
                        else {
                            _this.mci_project = false;
                        }
                    }
                });
                this.updateSingle = JSON.parse(JSON.stringify(this.detail.input_form_data));
                this.updateSingle.foto_bast = this.isArray(this.updateSingle.foto_bast) ? this.updateSingle.foto_bast : [];
                this.allBast = this.updateSingle.foto_bast;
                this.updateSingle.foto_surat_kuasa = this.isArray(this.updateSingle.foto_surat_kuasa) ? this.updateSingle.foto_surat_kuasa : [];
                this.allSuratKuasa = this.updateSingle.foto_surat_kuasa;
                this.assignArrayBoolean(this.allBast, this.loadingBAST);
                this.assignArrayBoolean(this.allSuratKuasa, this.loadingSuratKuasa);
                // console.log("loadingBAST", this.loadingBAST);
                // this.assignArrayBoolean(this.allSuratKuasa);
                // console.log("count", this.allBast, this.allSuratKuasa);
                this.checkValueKuasa();
                this.detail.previous_member_id = this.detail.member_id;
                this.activationStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this_1.detail.activation_status) {
                        _this_1.activationStatus[index].selected = 1;
                    }
                });
                this.detail.previous_member_id = this.detail.member_id;
                this.memberStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this_1.detail.member_status) {
                        _this_1.memberStatus[index].selected = 1;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    DataMemberEditComponent.prototype.backToDetail = function () {
        // console.log("Edit member",this.back);
        this.back[0][this.back[1]]();
        // this.back[0]();
    };
    DataMemberEditComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    DataMemberEditComponent.prototype.openUpload = function (type, index) {
        var upFile = document.getElementById(type + index);
        upFile.click();
    };
    DataMemberEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var new_form, currentBast, currentSuratKuasa, prop, kuasa, nonKuasa, e_1, message;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        new_form = {
                            // key:"ps-form-1",
                            id_pel: this.detail.input_form_data.id_pel,
                        };
                        if (this.updateSingle.foto_bast && this.updateSingle.foto_bast.length > 0) {
                            currentBast = this.updateSingle.foto_bast.filter(function (element) {
                                if (element.url && _this_1.validURL(element.url)) {
                                    return element;
                                }
                            });
                            this.updateSingle.foto_bast = currentBast;
                        }
                        if (this.updateSingle.foto_surat_kuasa && this.updateSingle.foto_surat_kuasa.length > 0) {
                            currentSuratKuasa = this.updateSingle.foto_surat_kuasa.filter(function (element) {
                                if (element.url && _this_1.validURL(element.url)) {
                                    return element;
                                }
                            });
                            this.updateSingle.foto_surat_kuasa = currentSuratKuasa;
                        }
                        for (prop in this.updateSingle) {
                            if (this.updateSingle[prop] != this.detail[prop] && this.updateSingle[prop] != "") {
                                if (prop == 'hadiah_dikuasakan') {
                                    kuasa = ["Ya", "true", "ya"];
                                    nonKuasa = ["Tidak", "false", "tidak"];
                                    if (kuasa.includes(this.updateSingle[prop])) {
                                        new_form[prop] = "KUASA";
                                    }
                                    else if (nonKuasa.includes(this.updateSingle[prop])) {
                                        new_form[prop] = "TIDAK KUASA";
                                    }
                                    else {
                                        new_form[prop] = this.updateSingle[prop];
                                    }
                                }
                                else {
                                    new_form[prop] = this.updateSingle[prop];
                                }
                            }
                            if (prop == 'foto_bast' || prop == 'foto_surat_kuasa') {
                                new_form[prop] = this.updateSingle[prop];
                            }
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.memberService.updateSingleFormMember(new_form)];
                    case 2:
                        _a.sent();
                        this.detail = JSON.parse(JSON.stringify(this.updateSingle));
                        //  Swal.fire('Success', 'Edit data pelanggan berhasil', 'success');
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            title: 'Success',
                            text: 'Edit data pelanggan berhasil',
                            icon: 'success',
                            confirmButtonText: 'Ok',
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                _this_1.backToDetail();
                            }
                        });
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DataMemberEditComponent.prototype.onFileSelected2 = function (event, img) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_1, valueImage_1, logo, e_2, message;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_1 = 3000000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_1) {
                                _this_1.errorFile = 'Maximum File Upload is 3MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this_1.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        valueImage_1 = this.checkLoading(img, true);
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) {
                                console.warn("RSULT IMG", result);
                                _this_1.updateSingle[valueImage_1] = result.base_url + result.pic_big_path;
                                _this_1.checkLoading(img, false);
                                // console.log("updateSingle", this.updateSingle);
                                // this.callImage(result, img);
                            })];
                    case 2:
                        logo = _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_2 = _a.sent();
                        this.errorLabel = e_2.message; //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    DataMemberEditComponent.prototype.onFileSelected = function (event, typeImg, idx) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_2, e_3, message;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (typeImg == 'bast') {
                            this.loadingBAST[idx] = true;
                        }
                        else if (typeImg == 'suratKuasa') {
                            this.loadingSuratKuasa[idx] = true;
                        }
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_2 = 3000000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_2) {
                                _this_1.errorFile = 'Maximum File Upload is 3MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this_1.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) {
                                var dataImage = {
                                    "url": result.base_url + result.pic_big_path,
                                    "updated_date": Date.now()
                                };
                                if (typeImg == 'bast') {
                                    _this_1.updateSingle.foto_bast[idx] = __assign({}, _this_1.updateSingle.foto_bast[idx], dataImage);
                                    _this_1.loadingBAST[idx] = false;
                                }
                                else if (typeImg == 'suratKuasa') {
                                    _this_1.updateSingle.foto_surat_kuasa[idx] = __assign({}, _this_1.updateSingle.foto_surat_kuasa[idx], dataImage);
                                    _this_1.loadingSuratKuasa[idx] = false;
                                }
                            })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_3 = _a.sent();
                        this.errorLabel = e_3.message; //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    DataMemberEditComponent.prototype.addMoreUpload = function (typeImg) {
        var dummyImg = {
            "url": "",
            "updated_date": Date.now(),
            "desc": ""
        };
        if (typeImg == 'bast') {
            // if(!this.isArray(this.updateSingle.foto_bast)) this.updateSingle.foto_bast = [];
            this.updateSingle.foto_bast.push(dummyImg);
            this.loadingBAST.push(false);
        }
        else if (typeImg == 'suratKuasa') {
            // if(!this.isArray(this.updateSingle.foto_surat_kuasa)) this.updateSingle.foto_surat_kuasa = [];
            this.updateSingle.foto_surat_kuasa.push(dummyImg);
            this.loadingSuratKuasa.push(false);
        }
    };
    DataMemberEditComponent.prototype.deleteImage = function (type, idx, url) {
        var _this_1 = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: "Delete",
            text: "Are you sure want to delete this ?",
            imageUrl: url,
            imageHeight: 250,
            imageAlt: type,
            showCancelButton: true,
            focusConfirm: false,
        }).then(function (res) {
            if (res.isConfirmed) {
                if (type == "bast") {
                    _this_1.updateSingle.foto_bast.splice(idx, 1);
                    _this_1.loadingBAST.splice(idx, 1);
                    // console.log("BAST DELETE", this.updateSingle.foto_bast,this.allBast);
                }
                else if (type = "suratKuasa") {
                    _this_1.updateSingle.foto_surat_kuasa.splice(idx, 1);
                    _this_1.loadingSuratKuasa.splice(idx, 1);
                    // console.log("SURAT KUASA DELETE", this.updateSingle.foto_surat_kuasa,this.allSuratKuasa);
                }
            }
        });
    };
    DataMemberEditComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    DataMemberEditComponent.prototype.checkLoading = function (fieldName, fieldValue) {
        var valueImage;
        switch (fieldName) {
            case 'ktpPemilik':
                valueImage = 'foto_ktp_pemilik';
                this.loadingKTPPemilik = fieldValue;
                break;
            case 'npwpPemilik':
                valueImage = 'foto_npwp_pemilik';
                this.loadingNPWPPemilik = fieldValue;
                break;
            case 'ktpPenerima':
                valueImage = 'foto_ktp_penerima';
                this.loadingKTPPenerima = fieldValue;
                break;
            case 'npwpPenerima':
                valueImage = 'foto_npwp_penerima';
                this.loadingNPWPPenerima = fieldValue;
                break;
            case 'bast':
                valueImage = 'foto_bast';
                this.loadingBAST = fieldValue;
                break;
            case 'suratKuasa':
                valueImage = 'foto_surat_kuasa';
                this.loadingSuratKuasa = fieldValue;
        }
        return valueImage;
    };
    DataMemberEditComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], DataMemberEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], DataMemberEditComponent.prototype, "back", void 0);
    DataMemberEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-data-member-edit',
            template: __webpack_require__(/*! raw-loader!./data-member.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/edit/data-member.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./data-member.edit.component.scss */ "./src/app/layout/modules/data-member/edit/data-member.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"], _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], DataMemberEditComponent);
    return DataMemberEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/data-member/show-password/show-password.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/show-password/show-password.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn-change-password {\n  width: 100%;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGF0YS1tZW1iZXIvc2hvdy1wYXNzd29yZC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGRhdGEtbWVtYmVyXFxzaG93LXBhc3N3b3JkXFxzaG93LXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9kYXRhLW1lbWJlci9zaG93LXBhc3N3b3JkL3Nob3ctcGFzc3dvcmQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtBQ0FSO0FEQ1E7RUFFSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTlo7QURHWTtFQUNJLGlCQUFBO0FDRGhCO0FES1E7RUFDSSx5QkFBQTtBQ0haO0FETUk7RUFDSSxhQUFBO0FDSlI7QURNUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNKWjtBRFFJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNOUjtBRFVRO0VBQ0ksZ0JBQUE7QUNSWjtBRFVRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ1JaO0FEVVE7RUFDSSxnQkFBQTtBQ1JaO0FEU1k7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNQaEI7QURTWTtFQUNJLHlCQUFBO0FDUGhCO0FEU1k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDUGhCO0FEU1k7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDUGhCO0FEUWdCO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNOcEI7QURZUTtFQUNJLHNCQUFBO0FDVlo7QURXWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDVGhCO0FEZ0JBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2JKO0FEY0k7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDWlI7QURjUTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0tBQUEsbUJBQUE7QUNaWjtBRGdCSTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDZFI7QURlUTtFQUNJLFlBQUE7RUFFQSxXQUFBO0FDZFo7QURlWTtFQUVJLFFBQUE7QUNkaEI7QURxQkE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ2xCSjtBRHFCQTtFQUNJLFdBQUE7QUNsQko7QURxQkE7RUFDSSx3QkFBQTtBQ2xCSjtBRHFCQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ2xCSjtBRHFCQTtFQUNJLGdCQUFBO0FDbEJKIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZGF0YS1tZW1iZXIvc2hvdy1wYXNzd29yZC9zaG93LXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b24udHJ1ZXtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICBcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICBcclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmltYWdle1xyXG4gICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICA+ZGl2e1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDMzLjMzMzMlO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtaGVhZGVye1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBoM3tcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgbWluLWhlaWdodDogMjdweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgLmltZy11cGxvYWQge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46MjBweCAxMHB4O1xyXG5cclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIC5sb2FkaW5nIHtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xyXG4gICAgICAgICAgICB3aWR0aDoxMDAlO1xyXG4gICAgICAgICAgICAuc2stZmFkaW5nLWNpcmNsZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOnJlZDtcclxuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuYnV0dG9uLmJ0bl91cGxvYWQsIGJ1dHRvbi5hZGQtbW9yZS1pbWFnZSwgYnV0dG9uLmJ0bl9kZWxldGUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDE1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgYm9yZGVyOjA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxufVxyXG5cclxuYnV0dG9uLmJ0bi1jaGFuZ2UtcGFzc3dvcmQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogc2FsbW9uO1xyXG59XHJcblxyXG4uYWRkLW1vcmUtY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmxhYmVsLWJvbGQge1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG4iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uLnRydWUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgPiBkaXYge1xuICB3aWR0aDogMzMuMzMzMyU7XG4gIHBhZGRpbmc6IDVweDtcbiAgZmxvYXQ6IGxlZnQ7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIGgzIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCBpbWcge1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luOiAyMHB4IDEwcHg7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcgLnNrLWZhZGluZy1jaXJjbGUge1xuICB0b3A6IDUwJTtcbn1cblxuYnV0dG9uLmJ0bl91cGxvYWQsIGJ1dHRvbi5hZGQtbW9yZS1pbWFnZSwgYnV0dG9uLmJ0bl9kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogNXB4IDE1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJvcmRlcjogMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5idXR0b24uYnRuLWNoYW5nZS1wYXNzd29yZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5idXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcbn1cblxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmxhYmVsLWJvbGQge1xuICBmb250LXdlaWdodDogNjAwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/data-member/show-password/show-password.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/data-member/show-password/show-password.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ShowPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowPasswordComponent", function() { return ShowPasswordComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { ProductService } from '../../../../services/product/product.service';
var ShowPasswordComponent = /** @class */ (function () {
    function ShowPasswordComponent(memberService, productService, zone) {
        this.memberService = memberService;
        this.productService = productService;
        this.zone = zone;
        this.errorLabel = false;
        this.loading = false;
        this.activationStatus = [
            { label: 'ACTIVATED', value: 'ACTIVATED' },
            { label: 'INACTIVED', value: 'INACTIVED' }
        ];
        this.memberStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
        this.errorFile = false;
    }
    ShowPasswordComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ShowPasswordComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    ShowPasswordComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    ShowPasswordComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.updateSingle = JSON.parse(JSON.stringify(this.detail));
                // console.warn("this detail???", this.detail)
                this.detail.previous_member_id = this.detail.member_id;
                return [2 /*return*/];
            });
        });
    };
    ShowPasswordComponent.prototype.backToDetail = function () {
        this.back[0][this.back[1]]();
    };
    ShowPasswordComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    ShowPasswordComponent.prototype.openUpload = function (type, index) {
        var upFile = document.getElementById(type + index);
        upFile.click();
    };
    ShowPasswordComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    ShowPasswordComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ShowPasswordComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ShowPasswordComponent.prototype, "back", void 0);
    ShowPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-show-password',
            template: __webpack_require__(/*! raw-loader!./show-password.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/data-member/show-password/show-password.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./show-password.component.scss */ "./src/app/layout/modules/data-member/show-password/show-password.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"], _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], ShowPasswordComponent);
    return ShowPasswordComponent;
}());



/***/ })

}]);
//# sourceMappingURL=modules-data-member-data-member-module.js.map