(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-packinglist-packinglist-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/packinglist/detail/packing-list.detail.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/packinglist/detail/packing-list.detail.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"packingListDetail &&!isGenerate\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToHere()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n    </div>\r\n\r\n    <ng-template #empty_image><span>\r\n        empty\r\n    </span></ng-template>\r\n  \r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Packing List Detail : {{packingListDetail.packing_no}}</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <table class=\"display-detail-packinglist\">\r\n                                    <tr>\r\n                                        <td>\r\n                                            Nama Kurir\r\n                                        </td>\r\n                                        <td>\r\n                                            :\r\n                                        </td>\r\n                                        <td>\r\n                                            {{packingListDetail.courier_name}}\r\n                                        </td>\r\n                                    </tr>\r\n\r\n                                    <tr>\r\n                                        <td>\r\n                                            Layanan Pengiriman\r\n                                        </td>\r\n                                        <td>\r\n                                            :\r\n                                        </td>\r\n                                        <td>\r\n                                            {{packingListDetail.delivery_service}}\r\n                                        </td>\r\n                                    </tr>\r\n\r\n                                    <tr>\r\n                                        <td>\r\n                                            Metode Pengiriman\r\n                                        </td>\r\n                                        <td>\r\n                                            :\r\n                                        </td>\r\n                                        <td>\r\n                                            {{packingListDetail.delivery_method}}\r\n                                        </td>\r\n                                    </tr>\r\n\r\n                                    <tr>\r\n                                        <td>\r\n                                            Status Packing\r\n                                        </td>\r\n                                        <td>\r\n                                            :\r\n                                        </td>\r\n                                        <td>\r\n                                            <strong>{{packingListDetail.status}}</strong>\r\n                                        </td>\r\n                                    </tr>\r\n                                </table>             \r\n                                <!-- <label>Nama Kurir : {{packingListDetail.courier_name}}</label> -->\r\n                                <!-- <div name=\"courier_name\">{{packingListDetail.courier_name}}</div> -->\r\n\r\n                                <!-- <label>Layanan Pengiriman : {{packingListDetail.delivery_service}}</label> -->\r\n                                <!-- <div name=\"service\">{{packingListDetail.delivery_service}}</div> -->\r\n\r\n                                <!-- <label>Metode Pengiriman : {{packingListDetail.delivery_method}}</label> -->\r\n                                <!-- <div name=\"method\">{{packingListDetail.delivery_method}}</div> -->\r\n\r\n                                <!-- <label>Status Packing : {{packingListDetail.status}}</label> -->\r\n                                <!-- <div name=\"method\">{{packingListDetail.status}}</div> -->\r\n                            </div>\r\n\r\n                            <div class=\"col-md-6\">\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-6\">\r\n                    <div style=\"display:flex; width:100%; height: 100%; justify-content: center; align-items: center;\">\r\n                        <div style=\"display:flex;justify-content: center; align-items: center; flex-direction: column;\">\r\n                            <div>\r\n                                <i class=\"fa fa-cube\" style=\"font-size:130px; color: #555\"></i>\r\n                            </div>\r\n                            <div style=\"margin:7px 0 12px 0;\">\r\n                                <h1 style=\"font-size: 20px; color:#000;\">\r\n                                    Process Packing List ?\r\n                                </h1>\r\n                            </div>\r\n                            <div>\r\n                                <button *ngIf=\"packingListDetail.status == 'ACTIVE'\" class=\"btn btn-process\" (click)=\"processPackingList()\">\r\n                                    PROSES SEKARANG\r\n                                </button>\r\n                                <button *ngIf=\"packingListDetail.status == 'PROCESSED'\" class=\"btn btn-process\" (click)=\"generatePackingList()\">\r\n                                    <i class=\"fa fa-download\"></i>\r\n                                    DOWNLOAD PACKING LIST\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <!-- <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Approval Status</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>Status</label>\r\n                                <div name=\"member_id\">{{packingListDetail.status}}</div>\r\n                                \r\n                                <div class=\"need-approval-text\" *ngIf=\"packingListDetail.status == 'ACTIVE' && !processNow\">\r\n                                    <span>This request needs approval</span>\r\n                                </div>\r\n\r\n                                <div class=\"need-approval-text\" *ngIf=\"packingListDetail.status == 'ACTIVE' && processNow\">\r\n                                    <span>Are you sure to process this request?</span>\r\n                                </div>\r\n\r\n                                <div class=\"btn-action-stock\" *ngIf=\"packingListDetail.status == 'ACTIVE' && !processNow\">\r\n                                    <button class=\"btn btn-approve\" (click)=\"isProcessNow()\">Process</button>\r\n                                    \r\n                                </div>\r\n\r\n                                <div class=\"btn-action-stock\" *ngIf=\"packingListDetail.status == 'ACTIVE' && processNow\">\r\n                                    <button class=\"btn btn-approve\" (click)=\"processPoint(packingListDetail.packing_no)\">Process Now</button>\r\n                                    <button class=\"btn btn-decline\" (click)=\"isProcessNow()\">Cancel</button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n               \r\n            </div>\r\n         </div>\r\n    </div>\r\n    \r\n</div>\r\n\r\n<div *ngIf=\"orders&&!errorMessage&&!isGenerate\">\r\n    <app-form-builder-table \r\n    [tableFormat] = \"tableFormat\" \r\n    [table_data]  = \"orders\" \r\n    [searchCallback]= \"[service, 'searchPackingListDetailReport',this]\" \r\n    [total_page]=\"totalPage\"\r\n    [direct_download] = \"packingListDetail.order_list\"\r\n    >\r\n    </app-form-builder-table>\r\n</div>\r\n\r\n<div *ngIf=\"isGenerate\">\r\n    <app-packing-list-generate [back]=\"[this,backHere]\" [detail]=\"packingListDetail\"></app-packing-list-generate>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/packinglist/generate/packing-list.generate.component.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/packinglist/generate/packing-list.generate.component.html ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"btn-print\">\r\n    <button class=\"btn back_button\" (click)=\"backToHere()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n    <button mat-raised-button color=\"primary\" class=\"btn btn-primary\" (click)=\"prints()\">Print Label<i\r\n            class=\"fa fa-print\" aria-hidden=\"true\"></i></button>\r\n</div>\r\n<div class=\"notif error-message\">{{errorMessage}}</div>\r\n<div class=\"loading\" *ngIf='loading'>\r\n    <div class=\"sk-fading-circle\">\r\n        <div class=\"sk-circle1 sk-circle\"></div>\r\n        <div class=\"sk-circle2 sk-circle\"></div>\r\n        <div class=\"sk-circle3 sk-circle\"></div>\r\n        <div class=\"sk-circle4 sk-circle\"></div>\r\n        <div class=\"sk-circle5 sk-circle\"></div>\r\n        <div class=\"sk-circle6 sk-circle\"></div>\r\n        <div class=\"sk-circle7 sk-circle\"></div>\r\n        <div class=\"sk-circle8 sk-circle\"></div>\r\n        <div class=\"sk-circle9 sk-circle\"></div>\r\n        <div class=\"sk-circle10 sk-circle\"></div>\r\n        <div class=\"sk-circle11 sk-circle\"></div>\r\n        <div class=\"sk-circle12 sk-circle\"></div>\r\n    </div>\r\n</div>\r\n<div class=\"page-break\" id=\"section-to-print\" *ngIf=\"!loading\">\r\n    <div class=\"_container\">\r\n        <div class=\"content\" style=\"font-family: Cambria\">\r\n            <header>\r\n                <strong>PACKING LIST PT. CLS SYSTEM</strong>\r\n            </header>\r\n            <br />\r\n            <div class=\"body-letter\">\r\n                <table cellspacing=\"0\" cellpadding=\"0\" class=\"top-table\">\r\n                    <tr>\r\n                        <td class=\"title\">Tanggal Pengiriman</td>\r\n                        <td class=\"colon\">: </td>\r\n                        <td>{{detail.delivery_date}}</td>\r\n                        <td></td>\r\n                        <td rowspan=\"4\" class=\"delivery-service\">{{detail.delivery_service}}</td>\r\n                        <td></td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"title\">Nama Kurir</td>\r\n                        <td class=\"colon\">: </td>\r\n                        <td>{{detail.courier_name}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"title\">Nama Program</td>\r\n                        <td class=\"colon\">: </td>\r\n                        <td>{{titleProgram}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"title\">Jenis Pengiriman</td>\r\n                        <td class=\"colon\">: </td>\r\n                        <td>{{detail.delivery_method}}</td>\r\n                    </tr>\r\n                </table>\r\n\r\n                <!-- <table>\r\n                    <tr>\r\n                      <th rowspan=\"2\" class=\"row_no\" style=\"width: 36px;\">No</th>\r\n                      <th rowspan=\"2\" style=\"width: 115px;\">PO</th>\r\n                      <th rowspan=\"2\" style=\"width: 115px;\">Package ID</th>\r\n                      <th rowspan=\"2\" style=\"width: 115px;\">AWB No</th>\r\n                      <th rowspan=\"2\" style=\"width: 115px;\">Nama</th>\r\n                      <th rowspan=\"2\" style=\"width: 115px;\">Alamat</th>\r\n                      <th rowspan=\"2\" style=\"width: 115px;\">No Telepon</th>\r\n                      <th rowspan=\"2\" style=\"width:75px;\">No Barang</th>\r\n                      <th rowspan=\"2\" style=\"width:115px;\">Nama Barang</th>\r\n                      <th rowspan=\"2\" style=\"width:64px;\">QTY</th>\r\n                      <th rowspan=\"2\" style=\"width:115px;\">Total</th>\r\n                      <th colspan=\"2\">TTD</th>\r\n                    </tr>\r\n                    <tr>\r\n                        <th>Kurir</th>\r\n                        <th>CLS</th>\r\n                    </tr>\r\n                    <tr *ngFor=\"let data of delivery_list; let i = index;\">\r\n                      <td class=\"row_no\">{{i + 1}}.</td>\r\n                      <td>{{data && data.po_no && data.po_no.value ? data.po_no.value : ''}}</td>\r\n                      <td>{{data.order_id}}</td>\r\n                      <td>{{data.delivery_detail.awb_number}}</td>\r\n                      <td>{{data.address_detail.name}}</td>\r\n                      <td>{{data.address_detail.address}}, {{data.address_detail.village_name}}, {{data.address_detail.district_name}}, {{data.address_detail.city_name}}, {{data.address_detail.province_name}}, {{data.address_detail.postal_code}}</td>\r\n                      <td>{{data.address_detail.cell_phone}}</td>\r\n                      <td colspan=\"4\" class=\"td-products\">\r\n                          <div>\r\n                            <tr *ngFor=\"let product of data.products; let j = index;\">\r\n                                <td style=\"width:81px; border-left: none;\">{{j + 1}}.</td>\r\n                                <td style=\"width:123px;\">\r\n                                    {{product.product_name}} \r\n                                </td>\r\n                                <td style=\"width:69px;\">\r\n                                    {{product.quantity}} \r\n                                </td>\r\n                                <td style=\"border-right: none; width:124px;\">\r\n                                    {{product.total_product_price}} \r\n                                </td>\r\n                            </tr>\r\n                          </div>\r\n                      </td>\r\n                      <td></td>\r\n                      <td></td>\r\n                    </tr>\r\n                </table> -->\r\n                <table class=\"bottom-table\">\r\n                    <tr>\r\n                        <th rowspan=\"2\" class='width-fixed-no'>No.</th>\r\n                        <th rowspan=\"2\" class='width-variable'>PO</th>\r\n                        <th rowspan=\"2\" class='width-variable'>Package ID</th>\r\n                        <th rowspan=\"2\" class='width-variable'>AWB No.</th>\r\n                        <th rowspan=\"2\" class='width-variable'>Nama</th>\r\n                        <th rowspan=\"2\" style=\"width:200px;\">Alamat</th>\r\n                        <th rowspan=\"2\" class='width-variable'>No. Telepon</th>\r\n                        <th rowspan=\"2\" class='width-fixed-no-barang'>No. Barang</th>\r\n                        <th rowspan=\"2\" class='width-fixed'>Nama Barang</th>\r\n                        <th rowspan=\"2\" class='width-fixed-no-barang'>QTY</th>\r\n                        <th rowspan=\"2\" class='width-fixed'>Total</th>\r\n                        <th colspan=\"2\" style=\"width:120px;\">TTD</th>\r\n                    </tr>\r\n                    <tr>\r\n                        <th class='width-fixed'>Kurir</th>\r\n                        <th class='width-fixed'>CLS</th>\r\n                    </tr>\r\n                    <tr *ngFor=\"let data of delivery_list; let i = index;\">\r\n                        <td class='width-fixed-no'>{{i + 1}}.</td>\r\n                        <td class='width-variable'>{{data && data.po_no && data.po_no.value ? data.po_no.value : ''}}</td>\r\n                        <td class='width-variable'>{{data.reference_no}}</td>\r\n                        <td class='width-variable'>{{data.awb_number}}</td>\r\n                        <td class='width-variable'>{{data.destination.name}}</td>\r\n                        <td style=\"width:200px;\">{{data.destination.address}}, {{data.destination.village_name}}, {{data.destination.district_name}}, {{data.destination.city_name}}, {{data.destination.province_name}}, {{data.destination.postal_code}}</td>\r\n                        <td class='width-variable'>{{data.destination.cell_phone}}</td>\r\n                        <td colspan=\"4\" class=\"td-products\">\r\n                            <table class=\"inside-loop\">\r\n                            <tr *ngFor=\"let product of data.product_list; let j = index;\" class=\"tr-looping\">\r\n                                <td class=\"width-fixed-no-barang looping\" style=\"border-left: none;\">{{j + 1}}.</td>\r\n                                <td class=\"width-fixed looping\">\r\n                                    {{product.product_name}} \r\n                                </td>\r\n                                <td class=\"width-fixed-no-barang looping\">\r\n                                    {{product.quantity}} \r\n                                </td>\r\n                                <td class=\"width-fixed looping\" style=\"border-right: none;\">\r\n                                    {{product.total_product_price | currency: 'Rp '}} \r\n                                </td>\r\n                            </tr>\r\n                            </table>\r\n                        </td>\r\n                        <td class='width-fixed'></td>\r\n                        <td class='width-fixed'></td>\r\n                    </tr>\r\n                </table>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/packinglist/packinglist.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/packinglist/packinglist.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n  <app-page-header [heading]=\"'Packing List'\" [icon]=\"'fa-table'\"></app-page-header>\r\n    <div class=\"container-swapper-option\" *ngIf=\"packingListDetail==false&&!errorMessage\">\r\n        <span style=\"margin-right: 10px;\">View</span> \r\n        <div class=\"button-container\">\r\n        <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n            <span class=\"fa fa-table\"> Active</span>\r\n        </button>\r\n        <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n            <span class=\"fa fa-table\"> Processed</span>\r\n        </button>\r\n        </div>\r\n    </div>\r\n\r\n    <div *ngIf=\"Orderhistoryallhistory && packingListDetail==false && swaper==true && !errorMessage\">\r\n        <app-form-builder-table \r\n        [table_data]=\"Orderhistoryallhistory\"\r\n        [searchCallback]=\"[service, 'searchPackingList',this]\" \r\n        [tableFormat]=\"tableFormat\"\r\n        [total_page]=\"totalPage\"\r\n        >\r\n        </app-form-builder-table>\r\n    </div>\r\n\r\n    <div *ngIf=\"Orderhistoryallhistory && packingListDetail==false && swaper==false && !errorMessage\">\r\n        <app-form-builder-table \r\n        [table_data]=\"Orderhistoryallhistory\"\r\n        [searchCallback]=\"[service, 'searchPackingList2',this]\" \r\n        [tableFormat]=\"tableFormat2\"\r\n        [total_page]=\"totalPage\"\r\n        >\r\n        </app-form-builder-table>\r\n    </div>\r\n\r\n    <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n        <div class=\"text-message\">\r\n        <i class=\"fas fa-ban\"></i>\r\n        {{errorMessage}}\r\n        </div>\r\n    </div>\r\n\r\n    <!-- <div *ngIf=\"packingListDetail\">\r\n        <app-orderhistoryallhistory-edit [back]=\"[this,backToHere]\" [detail]=\"packingListDetail\"></app-orderhistoryallhistory-edit>\r\n    </div> -->\r\n\r\n    <div *ngIf=\"packingListDetail\">\r\n    <app-packing-list-detail [back]=\"[this,backToHere]\" [detail]=\"packingListDetail\"></app-packing-list-detail>\r\n    </div>\r\n\r\n  </div>\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/modules/packinglist/detail/packing-list.detail.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/layout/modules/packinglist/detail/packing-list.detail.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.need-approval-text {\n  color: red;\n  font-size: 14px;\n  margin-bottom: 15px;\n}\n\n.btn-action-stock {\n  display: flex;\n}\n\n.btn-action-stock .btn-approve {\n  background-color: #3498db;\n  color: white;\n  margin-right: 10px;\n  font-size: 14px;\n}\n\n.btn-action-stock .btn-decline {\n  font-size: 14px;\n}\n\ntable > tr {\n  height: 50px;\n}\n\ntable > tr > td:nth-child(2) {\n  padding: 0 30px;\n}\n\n.btn-process {\n  border: 1px solid #28a745;\n  display: inline-block;\n  background-color: #2381fb;\n  transition: ease 0.3s all 0s;\n  color: white !important;\n  background-color: #28a745 !important;\n  font-size: 15px;\n}\n\n.btn-process:hover {\n  background-color: #28a715;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcGFja2luZ2xpc3QvZGV0YWlsL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xccGFja2luZ2xpc3RcXGRldGFpbFxccGFja2luZy1saXN0LmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcGFja2luZ2xpc3QvZGV0YWlsL3BhY2tpbmctbGlzdC5kZXRhaWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7QUNDSjs7QURHSTtFQUNJLGtCQUFBO0FDQVI7O0FEQ1E7RUFFSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RaOztBREVZO0VBQ0ksaUJBQUE7QUNBaEI7O0FER1E7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFFQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFJQSxXQUFBO0FDTFo7O0FERVk7RUFDSSxpQkFBQTtBQ0FoQjs7QURJUTtFQUNJLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNGWjs7QURLSTtFQUNJLGFBQUE7QUNIUjs7QURJUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNGWjs7QURpQkk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ2ZSOztBRGtCUTtFQUNJLGtCQUFBO0FDaEJaOztBRGtCUTtFQUNJLGdCQUFBO0FDaEJaOztBRGtCUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNoQlo7O0FEbUJRO0VBQ0ksc0JBQUE7QUNqQlo7O0FEa0JZO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7QUNoQmhCOztBRHVCQTtFQUNJLFVBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNwQko7O0FEdUJBO0VBQ0ksYUFBQTtBQ3BCSjs7QURxQkk7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUNuQlI7O0FEcUJJO0VBQ0ksZUFBQTtBQ25CUjs7QUR1QkE7RUFDSSxZQUFBO0FDcEJKOztBRHVCQTtFQUNJLGVBQUE7QUNwQko7O0FEdUJFO0VBQ0UseUJBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0VBQ0EsNEJBQUE7RUFDQSx1QkFBQTtFQUNBLG9DQUFBO0VBQ0EsZUFBQTtBQ3BCSjs7QUR1QkU7RUFDRSx5QkFBQTtBQ3BCSiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3BhY2tpbmdsaXN0L2RldGFpbC9wYWNraW5nLWxpc3QuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWRlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4uY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmVkaXRfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmJ0bi1yaWdodCB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIC5pbWFnZXtcclxuICAgIC8vICAgICBoM3tcclxuICAgIC8vICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgLy8gICAgIH1cclxuICAgIC8vICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgLy8gICAgICAgICBpbWd7XHJcbiAgICAvLyAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAvLyAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAvLyAgICAgICAgIH1cclxuICAgIC8vICAgICB9XHJcbiAgICAvLyB9XHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5tZW1iZXItZGV0YWlse1xyXG4gICAgICAgIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5uZWVkLWFwcHJvdmFsLXRleHQge1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbn1cclxuXHJcbi5idG4tYWN0aW9uLXN0b2NrIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAuYnRuLWFwcHJvdmUge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB9XHJcbiAgICAuYnRuLWRlY2xpbmUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIH1cclxufVxyXG5cclxudGFibGU+dHIge1xyXG4gICAgaGVpZ2h0OjUwcHg7XHJcbn1cclxuXHJcbnRhYmxlPiB0ciA+dGQ6bnRoLWNoaWxkKDIpIHtcclxuICAgIHBhZGRpbmc6IDAgMzBweDtcclxufVxyXG4gIFxyXG4gIC5idG4tcHJvY2VzcyB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMjhhNzQ1O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzIzODFmYjtcclxuICAgIHRyYW5zaXRpb246IGVhc2UgMC4zcyBhbGwgMHM7XHJcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyOGE3NDUgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICB9XHJcblxyXG4gIC5idG4tcHJvY2Vzczpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjhhNzE1O1xyXG4gIH0iLCIuYmctZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5idG4tcmlnaHQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ubmVlZC1hcHByb3ZhbC10ZXh0IHtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uYnRuLWFjdGlvbi1zdG9jayB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uYnRuLWFjdGlvbi1zdG9jayAuYnRuLWFwcHJvdmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLmJ0bi1hY3Rpb24tc3RvY2sgLmJ0bi1kZWNsaW5lIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG50YWJsZSA+IHRyIHtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuXG50YWJsZSA+IHRyID4gdGQ6bnRoLWNoaWxkKDIpIHtcbiAgcGFkZGluZzogMCAzMHB4O1xufVxuXG4uYnRuLXByb2Nlc3Mge1xuICBib3JkZXI6IDFweCBzb2xpZCAjMjhhNzQ1O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyMzgxZmI7XG4gIHRyYW5zaXRpb246IGVhc2UgMC4zcyBhbGwgMHM7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjhhNzQ1ICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuLmJ0bi1wcm9jZXNzOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI4YTcxNTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/packinglist/detail/packing-list.detail.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/layout/modules/packinglist/detail/packing-list.detail.component.ts ***!
  \************************************************************************************/
/*! exports provided: PackingListReportDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackingListReportDetailComponent", function() { return PackingListReportDetailComponent; });
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var PackingListReportDetailComponent = /** @class */ (function () {
    function PackingListReportDetailComponent(evoucherService, route, router, orderHistoryService) {
        this.evoucherService = evoucherService;
        this.route = route;
        this.router = router;
        this.orderHistoryService = orderHistoryService;
        this.processNow = false;
        this.edit = false;
        this.errorLabel = false;
        this.isGenerate = false;
        // packingList : any;
        this.orders = [];
        this.row_id = "packing_number";
        this.errorMessage = false;
        this.pointDetail = false;
        this.tableFormat = {
            title: 'Packing List Detail',
            label_headers: [
                { label: 'PO', visible: true, type: 'value_po', data_row_name: 'po_no' },
                { label: 'AWB Number', visible: true, type: 'string', data_row_name: 'awb_number' },
                { label: 'Package ID', visible: true, type: 'string', data_row_name: 'reference_no' },
                { label: 'Nama', visible: true, type: 'receiver_name', data_row_name: 'destination' },
                { label: 'Alamat', visible: true, type: 'shipping_address', data_row_name: 'destination' },
                { label: 'No Telp', visible: true, type: 'phone_number', data_row_name: 'destination' },
                { label: 'Nama Barang', visible: true, type: 'product_redeem', data_row_name: 'product_list' },
                { label: 'Qty', visible: true, type: 'product_quantity', data_row_name: 'product_list' },
                { label: 'Total Price', visible: true, type: 'product_price_redeem', data_row_name: 'product_list' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: this.row_id,
                this: this,
                result_var_name: 'orders',
                detail_function: [this, 'callDetail'],
                removePackinglist: true
            },
            show_checkbox_options: true
        };
    }
    PackingListReportDetailComponent.prototype.ngOnInit = function () {
        console.warn("DETAIL", this.detail);
        this.packingListDetail = this.detail;
        this.packingNumber = this.detail.packing_no;
        this.firstLoad();
    };
    PackingListReportDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log("this.back", this.back);
                this.service = this.orderHistoryService;
                this.processNow = false;
                this.edit = false;
                this.errorLabel = false;
                this.orders = [];
                this.errorMessage = false;
                this.pointDetail = false;
                // try {
                if (this.packingListDetail.status != 'ACTIVE') {
                    this.tableFormat.show_checkbox_options = false;
                    delete this.tableFormat.formOptions.removePackinglist;
                }
                this.orders = this.packingListDetail.delivery_list;
                return [2 /*return*/];
            });
        });
    };
    PackingListReportDetailComponent.prototype.callDetail = function (param) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    PackingListReportDetailComponent.prototype.backToHere = function () {
        // this.evoucherStockDetail = false;
        this.back[1](this.back[0]);
        // this.firstLoad();
    };
    PackingListReportDetailComponent.prototype.backHere = function (obj) {
        obj.isGenerate = false;
    };
    PackingListReportDetailComponent.prototype.isProcessNow = function () {
        this.processNow = !this.processNow;
    };
    PackingListReportDetailComponent.prototype.processPackingList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                    title: 'Confirmation',
                    text: 'Anda yakin proses packing list ?',
                    confirmButtonText: 'Process',
                    cancelButtonText: "Cancel",
                    showCancelButton: true
                }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                    var dataProcess, result_1, e_1, message;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!result.isConfirmed) return [3 /*break*/, 4];
                                if (!(this.orders.length > 0)) return [3 /*break*/, 4];
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 3, , 4]);
                                dataProcess = {
                                    "packing_no": this.packingListDetail.packing_no
                                };
                                return [4 /*yield*/, this.orderHistoryService.processPackingList(dataProcess)];
                            case 2:
                                result_1 = _a.sent();
                                console.warn("result process", result_1);
                                if (result_1) {
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                                        title: 'Success',
                                        text: 'Packing List Processed',
                                        icon: 'success',
                                        confirmButtonText: 'Ok',
                                    }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                                        var result_2, e_2;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    _a.trys.push([0, 2, , 3]);
                                                    return [4 /*yield*/, this.orderHistoryService.searchPackingListDetailReport(this.packingNumber)];
                                                case 1:
                                                    result_2 = _a.sent();
                                                    if (result_2.values.length < 1)
                                                        this.backToHere();
                                                    this.totalPage = result_2.total_page;
                                                    this.packingListDetail = result_2.values[0];
                                                    this.firstLoad();
                                                    return [3 /*break*/, 3];
                                                case 2:
                                                    e_2 = _a.sent();
                                                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire("Failed", e_2.message, "error");
                                                    return [3 /*break*/, 3];
                                                case 3: return [2 /*return*/];
                                            }
                                        });
                                    }); });
                                }
                                return [3 /*break*/, 4];
                            case 3:
                                e_1 = _a.sent();
                                console.warn("error appove", e_1);
                                // alert("Error");
                                this.errorLabel = (e_1.message);
                                message = this.errorLabel;
                                if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                                    message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                                }
                                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                                    icon: 'error',
                                    title: message,
                                });
                                return [3 /*break*/, 4];
                            case 4: return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    PackingListReportDetailComponent.prototype.generatePackingList = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.isGenerate = true;
                return [2 /*return*/];
            });
        });
    };
    // private async loadDetail(historyID: string){
    //   try {
    //     this.service = this.evoucherService;
    //     let result: any = await this.evoucherService.detailEvoucherStock(historyID);
    //     this.packingListDetail = result.result[0];
    //     console.log(this.packingListDetail);
    //   } catch (error) {
    //   }
    // }
    PackingListReportDetailComponent.prototype.processSelectedOrders = function (obj, orders) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                if (orders.length > 0) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                        title: 'Confirmation',
                        text: 'Hapus dari packing list ?',
                        confirmButtonText: 'Process',
                        cancelButtonText: "Cancel",
                        showCancelButton: true
                    }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                        var listReferenceNo_1, payload, e_3;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!result.isConfirmed) return [3 /*break*/, 4];
                                    listReferenceNo_1 = [];
                                    console.warn("orders", orders);
                                    orders.forEach(function (reference_no) {
                                        listReferenceNo_1.push(reference_no);
                                    });
                                    if (!(listReferenceNo_1.length > 0)) return [3 /*break*/, 4];
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    payload = {
                                        packing_no: this.packingListDetail.packing_no,
                                        reference_no: listReferenceNo_1
                                    };
                                    console.warn("payload remove", payload);
                                    return [4 /*yield*/, this.orderHistoryService.removePackingList(payload)];
                                case 2:
                                    _a.sent();
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                                        title: 'Success',
                                        text: 'remove from packing list success',
                                        confirmButtonText: 'ok'
                                    }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                                        var result_3, e_4;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    if (!result.isConfirmed) return [3 /*break*/, 4];
                                                    _a.label = 1;
                                                case 1:
                                                    _a.trys.push([1, 3, , 4]);
                                                    return [4 /*yield*/, this.orderHistoryService.searchPackingListDetailReport(this.packingNumber)];
                                                case 2:
                                                    result_3 = _a.sent();
                                                    if (result_3.values.length < 1)
                                                        this.backToHere();
                                                    this.totalPage = result_3.total_page;
                                                    this.packingListDetail = result_3.values[0];
                                                    this.firstLoad();
                                                    return [3 /*break*/, 4];
                                                case 3:
                                                    e_4 = _a.sent();
                                                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire("Failed", e_4.message, "error");
                                                    return [3 /*break*/, 4];
                                                case 4: return [2 /*return*/];
                                            }
                                        });
                                    }); });
                                    return [3 /*break*/, 4];
                                case 3:
                                    e_3 = _a.sent();
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire("Failed", e_3.message, "error");
                                    return [3 /*break*/, 4];
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); });
                }
                return [2 /*return*/];
            });
        });
    };
    PackingListReportDetailComponent.ctorParameters = function () { return [
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_5__["EVoucherService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_0__["OrderhistoryService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], PackingListReportDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], PackingListReportDetailComponent.prototype, "back", void 0);
    PackingListReportDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-packing-list-detail',
            template: __webpack_require__(/*! raw-loader!./packing-list.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/packinglist/detail/packing-list.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_4__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./packing-list.detail.component.scss */ "./src/app/layout/modules/packinglist/detail/packing-list.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_5__["EVoucherService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_0__["OrderhistoryService"]])
    ], PackingListReportDetailComponent);
    return PackingListReportDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/packinglist/generate/packing-list.generate.component.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/layout/modules/packinglist/generate/packing-list.generate.component.scss ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n  background-color: white;\n  font-family: \"Cambria (Headings)\";\n  color: #333;\n  text-align: left;\n  font-size: 11pt;\n  margin: 0;\n}\n\nfooter {\n  clear: both;\n  position: relative;\n  height: 200px;\n  margin-top: -200px;\n}\n\n.mat-raised-button.mat-primary {\n  background-color: #2481fb;\n  float: right;\n  padding: 5px 20px;\n  font-family: \"Cambria (Headings)\";\n  margin: 10px 40px;\n  border-radius: 5px;\n}\n\n.mat-raised-button.mat-primary .fa {\n  margin-left: 8px;\n}\n\n._container {\n  background-color: white;\n  margin: 0px 0px 30px 0px;\n  border-radius: 5px;\n  position: relative;\n  text-align: -webkit-center;\n  -webkit-column-break-before: always;\n     -moz-column-break-before: always;\n          break-before: always;\n  padding-top: 10px;\n}\n\n._container table th {\n  padding: 2px;\n}\n\n._container .watermark {\n  position: absolute;\n  font-size: 120px;\n  font-weight: bolder;\n  width: 100%;\n  left: 0;\n  top: 50%;\n  right: 0;\n  color: red;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n}\n\n._container .watermark span {\n  opacity: 0.1;\n  letter-spacing: 100px;\n}\n\n._container .content {\n  position: relative;\n  margin: 0 36px;\n  padding: 0 0 36px 0;\n}\n\n._container .content .logo-container {\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n}\n\n._container .content .logo-container img {\n  margin: 20px 0px;\n  width: 120px;\n}\n\n._container .content .body-letter {\n  text-align: left;\n}\n\n._container .content .body-letter table {\n  width: 100%;\n}\n\n._container .content .body-letter .table1 {\n  border: none;\n}\n\n._container .content .body-letter .table1 .first-col {\n  width: 20px;\n}\n\n._container .content .body-letter .table1 tr {\n  vertical-align: top;\n  padding: 0;\n  border: none;\n}\n\n._container .content .body-letter .table1 tr td {\n  padding: 0;\n  border: none;\n}\n\n._container .content .body-letter .table2 th {\n  font-size: 10pt;\n  text-align: center;\n}\n\n._container .content .body-letter .table2 .no {\n  width: 20px;\n}\n\n._container .content .body-letter .table2 td {\n  padding: 5px;\n}\n\n._container .content .body-letter .table3 th {\n  font-size: 10pt;\n}\n\n._container .content .body-letter .table3 td {\n  padding: 5px;\n}\n\n._container .content .body-letter .table3 .no {\n  width: 20px;\n}\n\n._container .content .body-letter .tabel1-1 {\n  font-style: italic;\n}\n\n._container .content .penerima {\n  display: flex;\n  justify-content: flex-end;\n  align-items: center;\n}\n\n._container .content .penerima-pemberi {\n  display: flex;\n  justify-content: space-between;\n  width: 100%;\n}\n\n._container .content .layanan {\n  text-align: center;\n  margin-bottom: 30px;\n}\n\n._container .content .page-number {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  text-align: center;\n  font-size: 18px;\n}\n\n._container .content .table-order {\n  height: 100%;\n  width: 14cmcm;\n  font-size: 11px;\n}\n\n._container .content .table-order .order-id-header {\n  width: 70%;\n  table-layout: fixed;\n  border-top: 0;\n  width: 80%;\n}\n\n._container .content .table-order .header {\n  table-layout: auto;\n  border-bottom: 0;\n  width: 20cm;\n}\n\n._container .content .table-order .detail-barang-pengiriman {\n  text-align: left;\n  width: 20cm;\n}\n\n._container .content .table-order .detail-barang-pengiriman .row-eq-height {\n  display: flex;\n}\n\n._container .content .table-order .detail-barang-pengiriman .row {\n  margin-left: 0px;\n  height: 100%;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-barang {\n  border-right: solid black 1px;\n  width: 50%;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-barang .daftar-barang {\n  padding-left: 20px;\n  padding-top: 20px;\n  padding-bottom: 20px;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-barang .catatan {\n  padding-top: 20px;\n  padding-bottom: 20px;\n  font-weight: bold;\n  text-transform: uppercase;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman {\n  width: 50%;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman .jenis-pengiriman {\n  padding-top: 20px;\n  padding-bottom: 20px;\n  padding-left: 20px;\n  font-weight: bold;\n  text-align: left;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman .total-harga {\n  padding-top: 30px;\n  padding-bottom: 30px;\n  text-align: center;\n  font-weight: bold;\n  padding-left: 0px;\n}\n\n._container .content .table-order .detail-header {\n  width: 20cm;\n  text-align: left;\n  border-top: 0;\n  border-bottom: 0;\n}\n\n._container .content .table-order .detail-header .detail-person {\n  border-right: solid black 1px;\n  padding: 20px;\n  width: 60%;\n}\n\n._container .content .table-order .detail-header .detail-person .receiver {\n  border-bottom: black solid 1px;\n  padding-bottom: 10px;\n  margin-bottom: 15px;\n  margin-top: 15px;\n}\n\n._container .content .table-order .detail-header .detail-order {\n  padding: 20px;\n}\n\n._container .content .table-order .detail-header .detail-order .tlc {\n  font-size: 20px;\n}\n\n._container .content .table-order .detail-header .row {\n  margin-left: 0px;\n}\n\n._container .content .table-order .locard-logo {\n  width: 100px;\n  text-align: center;\n}\n\n._container .content .table-order .locard-logo .logo-avatar {\n  border-radius: 0%;\n  width: 60px;\n}\n\n._container .content .table-order .courier-logo {\n  text-align: center;\n}\n\n._container .content .table-order #courier-logo {\n  text-align: center;\n}\n\n._container .content .table-order #courier-logo .logo-avatar {\n  width: 120px;\n}\n\n._container .content .table-order .awb {\n  text-align: center;\n}\n\n._container .content .table-order .order-id {\n  text-align: center;\n}\n\n._container .content .table-order .order-id p {\n  font-size: 20px;\n  font-weight: bold;\n}\n\n.btn-print {\n  text-align: center;\n  margin: 46px 0 15px 0;\n}\n\n.btn-print .back_button {\n  padding: 10px 20px;\n  vertical-align: top;\n}\n\n.btn-print .btn-primary {\n  float: none;\n  margin: 0;\n}\n\ndiv._container:last-child {\n  page-break-after: always;\n}\n\ntable {\n  border: 2px solid #333;\n  border-collapse: collapse;\n}\n\ntd,\ntr,\nth {\n  padding: 12px;\n  border: 1px solid #333;\n  width: 185px;\n}\n\nth {\n  background-color: #f0f0f0;\n}\n\nh4,\np {\n  margin: 0px;\n}\n\n#garis {\n  height: 200px;\n  color: black;\n}\n\n#thanks {\n  font-size: 15px;\n  text-align: center;\n  color: #0ea8db;\n  font-weight: bold;\n}\n\n#header {\n  text-align: center;\n  margin-top: 10px;\n}\n\n#non {\n  border: 1px solid black;\n  font-size: 20px;\n  padding: 4px;\n}\n\n#shipping {\n  font-size: 20px;\n}\n\n#order {\n  position: relative;\n  bottom: 10px;\n}\n\n.send {\n  text-align: center;\n}\n\n.sticky {\n  position: fixed;\n  top: 0;\n  width: 100%;\n}\n\n.sticky + #card-transaction {\n  padding-top: 50px;\n}\n\n.top-table {\n  border: none;\n  margin: 0 0 15px 0;\n  padding: 5px;\n}\n\n.top-table tr {\n  border: none;\n  margin: 0;\n  padding: 5px;\n}\n\n.top-table td {\n  vertical-align: top;\n  border: none;\n  margin: 0;\n  padding: 5px;\n}\n\n.top-table .title {\n  width: 100px;\n}\n\n.top-table .colon {\n  width: 0;\n}\n\n.top-table .delivery-service {\n  border: 1px solid black;\n  text-align: center;\n  vertical-align: middle;\n}\n\ntable {\n  table-layout: fixed;\n}\n\n.top-table tr {\n  width: unset;\n}\n\ntable.bottom-table {\n  border: 1px solid black;\n  table-layout: fixed;\n  width: 100%;\n  height: 100%;\n}\n\ntable.bottom-table tr, table.bottom-table td {\n  overflow-wrap: break-word;\n  text-align: center;\n}\n\ntable.bottom-table td {\n  vertical-align: top;\n}\n\ntable.bottom-table table.inside-loop {\n  height: 100%;\n  border: none;\n}\n\n.width-variable {\n  border: 1px solid black;\n  width: auto;\n}\n\n.width-fixed {\n  border: 1px solid black;\n  overflow: hidden;\n  width: 100px;\n}\n\n.width-fixed-no {\n  width: 36px;\n}\n\n.width-fixed-no-barang {\n  width: 56px;\n}\n\n.td-products {\n  padding: 0;\n  width: 0;\n}\n\n.td-products tr {\n  border-left: none;\n  border-top: none;\n  border-right: none;\n}\n\n.td-products td {\n  border-top: none;\n}\n\n.tr-looping:last-child {\n  border-bottom: none;\n}\n\n.tr-looping:last-child td {\n  border-bottom: none;\n}\n\n@media print {\n  @page {\n    size: A4 landscape;\n  }\n  body * {\n    visibility: hidden;\n  }\n\n  .content {\n    position: relative;\n    border: white solid;\n  }\n\n  .bottom-table {\n    table-layout: fixed;\n  }\n\n  .watermark span {\n    font-size: 120px;\n    opacity: 0.2;\n  }\n\n  #header {\n    display: none;\n  }\n\n  h5 {\n    display: none;\n  }\n\n  app-page-header {\n    display: none;\n  }\n\n  button {\n    display: none;\n  }\n\n  * {\n    font-size: 13px;\n  }\n\n  div.table-order {\n    -webkit-transform: scale(0.7);\n            transform: scale(0.7);\n    padding-bottom: 80px;\n    width: 20cm;\n    height: 9cm;\n  }\n\n  div._container {\n    padding: 0;\n  }\n\n  .content {\n    padding: 0 !important;\n  }\n\n  #section-to-print, #section-to-print * {\n    visibility: visible;\n  }\n\n  #section-to-print {\n    position: absolute;\n    left: 0;\n    top: 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcGFja2luZ2xpc3QvZ2VuZXJhdGUvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxwYWNraW5nbGlzdFxcZ2VuZXJhdGVcXHBhY2tpbmctbGlzdC5nZW5lcmF0ZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcGFja2luZ2xpc3QvZ2VuZXJhdGUvcGFja2luZy1saXN0LmdlbmVyYXRlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsdUJBQUE7RUFDQSxpQ0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUNFRjs7QURFQTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FEQ0U7RUFDRSxnQkFBQTtBQ0NKOztBREdBO0VBQ0UsdUJBQUE7RUFDQSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtFQUNBLG1DQUFBO0tBQUEsZ0NBQUE7VUFBQSxvQkFBQTtFQUNBLGlCQUFBO0FDQUY7O0FER0k7RUFDRSxZQUFBO0FDRE47O0FESUU7RUFFQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUNFLFFBQUE7RUFDQSxRQUFBO0VBRUYsVUFBQTtFQUNBLGlDQUFBO1VBQUEseUJBQUE7QUNKRjs7QURLSTtFQUNFLFlBQUE7RUFDQSxxQkFBQTtBQ0hOOztBRE1FO0VBQ0Usa0JBQUE7RUFFQSxjQUFBO0VBR0EsbUJBQUE7QUNQSjs7QURXSTtFQUNFLGFBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FDVE47O0FEVU07RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUNSUjs7QURZSTtFQUNFLGdCQUFBO0FDVk47O0FEV007RUFDRSxXQUFBO0FDVFI7O0FEV007RUFDRSxZQUFBO0FDVFI7O0FEV1E7RUFDRSxXQUFBO0FDVFY7O0FEV1E7RUFDRSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0FDVFY7O0FEVVU7RUFDRSxVQUFBO0VBQ0YsWUFBQTtBQ1JWOztBRGFRO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0FDWFY7O0FEYVE7RUFDRSxXQUFBO0FDWFY7O0FEYVE7RUFDRSxZQUFBO0FDWFY7O0FEZVE7RUFDRSxlQUFBO0FDYlY7O0FEZVE7RUFDRSxZQUFBO0FDYlY7O0FEZVE7RUFDRSxXQUFBO0FDYlY7O0FEZ0JNO0VBQ0Usa0JBQUE7QUNkUjs7QURtQkk7RUFDRSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtBQ2pCTjs7QURtQkk7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxXQUFBO0FDakJOOztBRG9CSTtFQUNFLGtCQUFBO0VBQ0EsbUJBQUE7QUNsQk47O0FEcUJJO0VBQ0Usa0JBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ25CTjs7QURzQkk7RUFDRSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7QUNwQk47O0FEc0JNO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7QUNwQlI7O0FEdUJNO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNyQlI7O0FEd0JNO0VBQ0UsZ0JBQUE7RUFDQSxXQUFBO0FDdEJSOztBRHdCUTtFQUlFLGFBQUE7QUN0QlY7O0FEeUJRO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0FDdkJWOztBRDBCUTtFQUNFLDZCQUFBO0VBQ0EsVUFBQTtBQ3hCVjs7QUR5QlU7RUFDRSxrQkFBQTtFQUVBLGlCQUFBO0VBQ0Esb0JBQUE7QUN4Qlo7O0FEMkJVO0VBQ0UsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7QUN6Qlo7O0FENkJRO0VBQ0UsVUFBQTtBQzNCVjs7QUQ0QlU7RUFDRSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDMUJaOztBRDZCVTtFQUNFLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUMzQlo7O0FEZ0NNO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FDOUJSOztBRGdDUTtFQUNFLDZCQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7QUM5QlY7O0FEK0JVO0VBQ0UsOEJBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUM3Qlo7O0FEaUNRO0VBQ0UsYUFBQTtBQy9CVjs7QURnQ1U7RUFDRSxlQUFBO0FDOUJaOztBRGtDUTtFQUNFLGdCQUFBO0FDaENWOztBRG9DTTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtBQ2xDUjs7QURtQ1E7RUFFRSxpQkFBQTtFQUNBLFdBQUE7QUNqQ1Y7O0FEcUNNO0VBQ0Usa0JBQUE7QUNuQ1I7O0FEc0NNO0VBQ0Usa0JBQUE7QUNwQ1I7O0FEc0NRO0VBQ0UsWUFBQTtBQ3BDVjs7QUR3Q007RUFDRSxrQkFBQTtBQ3RDUjs7QUR5Q007RUFDRSxrQkFBQTtBQ3ZDUjs7QUR3Q1E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUN0Q1Y7O0FENkNBO0VBQ0Usa0JBQUE7RUFFQSxxQkFBQTtBQzNDRjs7QUQ2Q0U7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0FDM0NKOztBRDhDRTtFQUNFLFdBQUE7RUFDQSxTQUFBO0FDNUNKOztBRGtEQTtFQUNFLHdCQUFBO0FDL0NGOztBRGtEQTtFQUNFLHNCQUFBO0VBQ0EseUJBQUE7QUMvQ0Y7O0FEaURBOzs7RUFHRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0FDOUNGOztBRGdEQTtFQUNFLHlCQUFBO0FDN0NGOztBRCtDQTs7RUFFRSxXQUFBO0FDNUNGOztBRCtDQTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FDNUNGOztBRCtDQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQzVDRjs7QUQrQ0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FDNUNGOztBRCtDQTtFQUNFLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUM1Q0Y7O0FEK0NBO0VBQ0UsZUFBQTtBQzVDRjs7QUQrQ0E7RUFDRSxrQkFBQTtFQUNBLFlBQUE7QUM1Q0Y7O0FEK0NBO0VBQ0Usa0JBQUE7QUM1Q0Y7O0FEK0NBO0VBQ0UsZUFBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0FDNUNGOztBRCtDQTtFQUNFLGlCQUFBO0FDNUNGOztBRCtDQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUM1Q0Y7O0FEOENFO0VBQ0UsWUFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0FDNUNKOztBRCtDRTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0FDN0NKOztBRGdERTtFQUNFLFlBQUE7QUM5Q0o7O0FEaURFO0VBQ0UsUUFBQTtBQy9DSjs7QURrREU7RUFDRSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUNoREo7O0FEb0RBO0VBQ0UsbUJBQUE7QUNqREY7O0FEb0RBO0VBQ0UsWUFBQTtBQ2pERjs7QUQ2RkE7RUFDRSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUMxRkY7O0FENEZFO0VBQ0UseUJBQUE7RUFDQSxrQkFBQTtBQzFGSjs7QUQ2RkU7RUFDRSxtQkFBQTtBQzNGSjs7QUQ4RkU7RUFDRSxZQUFBO0VBQ0EsWUFBQTtBQzVGSjs7QURnR0E7RUFDRSx1QkFBQTtFQUVBLFdBQUE7QUM5RkY7O0FEbUdBO0VBQ0UsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNoR0Y7O0FEbUdBO0VBQ0UsV0FBQTtBQ2hHRjs7QURtR0E7RUFDRSxXQUFBO0FDaEdGOztBRG1HQTtFQUNFLFVBQUE7RUFDQSxRQUFBO0FDaEdGOztBRGtHRTtFQUNFLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ2hHSjs7QURrR0U7RUFDRSxnQkFBQTtBQ2hHSjs7QURvR0E7RUFDRSxtQkFBQTtBQ2pHRjs7QURtR0U7RUFDRSxtQkFBQTtBQ2pHSjs7QURxR0E7RUFDRTtJQUNFLGtCQUFBO0VDbEdGO0VEb0dBO0lBQ0Usa0JBQUE7RUNsR0Y7O0VEb0dBO0lBQ0Usa0JBQUE7SUFLQSxtQkFBQTtFQ3JHRjs7RUQwR0E7SUFDRSxtQkFBQTtFQ3ZHRjs7RUQyR0U7SUFDRSxnQkFBQTtJQUNBLFlBQUE7RUN4R0o7O0VENkdBO0lBQ0UsYUFBQTtFQzFHRjs7RUQ2R0E7SUFDRSxhQUFBO0VDMUdGOztFRDZHQTtJQUNFLGFBQUE7RUMxR0Y7O0VENkdBO0lBQ0UsYUFBQTtFQzFHRjs7RUQ2R0E7SUFDRSxlQUFBO0VDMUdGOztFRDZHQTtJQUNFLDZCQUFBO1lBQUEscUJBQUE7SUFDQSxvQkFBQTtJQUNBLFdBQUE7SUFDQSxXQUFBO0VDMUdGOztFRDZHQTtJQUNFLFVBQUE7RUMxR0Y7O0VEOEdBO0lBQ0UscUJBQUE7RUMzR0Y7O0VENkdBO0lBQ0UsbUJBQUE7RUMxR0Y7O0VENEdBO0lBQ0Usa0JBQUE7SUFDQSxPQUFBO0lBQ0EsTUFBQTtFQ3pHRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcGFja2luZ2xpc3QvZ2VuZXJhdGUvcGFja2luZy1saXN0LmdlbmVyYXRlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYm9keSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgZm9udC1mYW1pbHk6IFwiQ2FtYnJpYSAoSGVhZGluZ3MpXCI7XHJcbiAgY29sb3I6ICMzMzM7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBmb250LXNpemU6IDExcHQ7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcbmZvb3RlciB7XHJcbiAgY2xlYXI6IGJvdGg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGhlaWdodDogMjAwcHg7XHJcbiAgbWFyZ2luLXRvcDogLTIwMHB4O1xyXG59XHJcblxyXG5cclxuLm1hdC1yYWlzZWQtYnV0dG9uLm1hdC1wcmltYXJ5IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjQ4MWZiO1xyXG4gIGZsb2F0OiByaWdodDtcclxuICBwYWRkaW5nOiA1cHggMjBweDtcclxuICBmb250LWZhbWlseTogXCJDYW1icmlhIChIZWFkaW5ncylcIjtcclxuICBtYXJnaW46IDEwcHggNDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcblxyXG4gIC5mYSB7XHJcbiAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG4gIH1cclxufVxyXG5cclxuLl9jb250YWluZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gIG1hcmdpbjogMHB4IDBweCAzMHB4IDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xyXG4gIGJyZWFrLWJlZm9yZTogYWx3YXlzO1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gIHRhYmxlIHtcclxuICAgXHJcbiAgICB0aCB7XHJcbiAgICAgIHBhZGRpbmc6MnB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAud2F0ZXJtYXJrXHJcbiAge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBmb250LXNpemU6IDEyMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbGVmdDogMDtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiBcclxuICBjb2xvcjpyZWQ7XHJcbiAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTtcclxuICAgIHNwYW57XHJcbiAgICAgIG9wYWNpdHk6IDAuMTtcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IDEwMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29udGVudCB7XHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIC8vIHdpZHRoOiAyMWNtO1xyXG4gICAgbWFyZ2luOiAwIDM2cHg7XHJcbiAgICAvLyBoZWlnaHQ6IDMwLjBjbTtcclxuICAgIC8vIG1hcmdpbjogMzBtbSA0NW1tIDMwbW0gNDVtbTsgXHJcbiAgICBwYWRkaW5nOiAwIDAgMzZweCAwO1xyXG4gICAgLy8gcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xyXG4gICAgLy8gcGFnZS1icmVhay1iZWZvcmU6IGFsd2F5cztcclxuIFxyXG4gICAgLmxvZ28tY29udGFpbmVyIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIG1hcmdpbjogMjBweCAwcHg7XHJcbiAgICAgICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgIFxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuYm9keS1sZXR0ZXJ7XHJcbiAgICAgIHRleHQtYWxpZ246bGVmdDtcclxuICAgICAgdGFibGV7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIH1cclxuICAgICAgLnRhYmxlMSB7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICBcclxuICAgICAgICAuZmlyc3QtY29se1xyXG4gICAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRye1xyXG4gICAgICAgICAgdmVydGljYWwtYWxpZ246dG9wO1xyXG4gICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgIHRke1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAudGFibGUye1xyXG4gICAgICAgIHRoIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTBwdDtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLm5vIHtcclxuICAgICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0ZCB7XHJcbiAgICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC50YWJsZTMge1xyXG4gICAgICAgIHRoIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTBwdDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGQge1xyXG4gICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAubm8ge1xyXG4gICAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC50YWJlbDEtMSB7XHJcbiAgICAgICAgZm9udC1zdHlsZTogaXRhbGljO1xyXG5cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICBcclxuICAgIC5wZW5lcmltYSB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAucGVuZXJpbWEtcGVtYmVyaSB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcblxyXG4gICAgLmxheWFuYW4ge1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnBhZ2UtbnVtYmVyIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBsZWZ0OiAwO1xyXG4gICAgICBib3R0b206IDA7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC50YWJsZS1vcmRlciB7XHJcbiAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgd2lkdGg6IDE0Y21jbTtcclxuICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG5cclxuICAgICAgLm9yZGVyLWlkLWhlYWRlciB7XHJcbiAgICAgICAgd2lkdGg6IDcwJTtcclxuICAgICAgICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xyXG4gICAgICAgIGJvcmRlci10b3A6IDA7XHJcbiAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmhlYWRlciB7XHJcbiAgICAgICAgdGFibGUtbGF5b3V0OiBhdXRvO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDA7XHJcbiAgICAgICAgd2lkdGg6IDIwY207XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4ge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgd2lkdGg6IDIwY207XHJcblxyXG4gICAgICAgIC5yb3ctZXEtaGVpZ2h0IHtcclxuICAgICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gICAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLnJvdyB7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmRldGFpbC1iYXJhbmcge1xyXG4gICAgICAgICAgYm9yZGVyLXJpZ2h0OiBzb2xpZCBibGFjayAxcHg7XHJcbiAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICAgLmRhZnRhci1iYXJhbmcge1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbiAgICAgICAgICAgIC8vIGJvcmRlci1yaWdodDogc29saWQgYmxhY2sgMXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgLmNhdGF0YW4ge1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmRldGFpbC1wZW5naXJpbWFuIHtcclxuICAgICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgICAuamVuaXMtcGVuZ2lyaW1hbiB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAudG90YWwtaGFyZ2Ege1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMzBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDMwcHg7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLmRldGFpbC1oZWFkZXIge1xyXG4gICAgICAgIHdpZHRoOiAyMGNtO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgYm9yZGVyLXRvcDogMDtcclxuICAgICAgICBib3JkZXItYm90dG9tOiAwO1xyXG5cclxuICAgICAgICAuZGV0YWlsLXBlcnNvbiB7XHJcbiAgICAgICAgICBib3JkZXItcmlnaHQ6IHNvbGlkIGJsYWNrIDFweDtcclxuICAgICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgICB3aWR0aDogNjAlO1xyXG4gICAgICAgICAgLnJlY2VpdmVyIHtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogYmxhY2sgc29saWQgMXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5kZXRhaWwtb3JkZXIge1xyXG4gICAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICAgIC50bGMge1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAucm93IHtcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAubG9jYXJkLWxvZ28ge1xyXG4gICAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgLmxvZ28tYXZhdGFyIHtcclxuICAgICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogMCU7XHJcbiAgICAgICAgICB3aWR0aDogNjBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5jb3VyaWVyLWxvZ28ge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG5cclxuICAgICAgI2NvdXJpZXItbG9nbyB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgICAgICAubG9nby1hdmF0YXIge1xyXG4gICAgICAgICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLmF3YiB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAub3JkZXItaWQge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmJ0bi1wcmludCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIC8vIG1hcmdpbjogMTVweCAwcHg7XHJcbiAgbWFyZ2luOiA0NnB4IDAgMTVweCAwO1xyXG5cclxuICAuYmFja19idXR0b24ge1xyXG4gICAgcGFkZGluZzogMTBweCAyMHB4O1xyXG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuICB9XHJcblxyXG4gIC5idG4tcHJpbWFyeSB7XHJcbiAgICBmbG9hdDogbm9uZTtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcblxyXG5cclxufVxyXG5cclxuZGl2Ll9jb250YWluZXI6bGFzdC1jaGlsZCB7XHJcbiAgcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xyXG59XHJcblxyXG50YWJsZSB7XHJcbiAgYm9yZGVyOiAycHggc29saWQgIzMzMztcclxuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG59XHJcbnRkLFxyXG50cixcclxudGgge1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgIzMzMztcclxuICB3aWR0aDogMTg1cHg7XHJcbn1cclxudGgge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbn1cclxuaDQsXHJcbnAge1xyXG4gIG1hcmdpbjogMHB4O1xyXG59XHJcblxyXG4jZ2FyaXMge1xyXG4gIGhlaWdodDogMjAwcHg7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4jdGhhbmtzIHtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGNvbG9yOiAjMGVhOGRiO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4jaGVhZGVyIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuI25vbiB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIHBhZGRpbmc6IDRweDtcclxufVxyXG5cclxuI3NoaXBwaW5nIHtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuXHJcbiNvcmRlciB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGJvdHRvbTogMTBweDtcclxufVxyXG5cclxuLnNlbmQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnN0aWNreSB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHRvcDogMDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnN0aWNreSArICNjYXJkLXRyYW5zYWN0aW9uIHtcclxuICBwYWRkaW5nLXRvcDogNTBweDtcclxufVxyXG5cclxuLnRvcC10YWJsZSB7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIG1hcmdpbjogMCAwIDE1cHggMDtcclxuICBwYWRkaW5nOiA1cHg7XHJcblxyXG4gIHRyIHtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICB9XHJcblxyXG4gIHRkIHtcclxuICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgfVxyXG5cclxuICAudGl0bGUge1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gIH1cclxuXHJcbiAgLmNvbG9uIHtcclxuICAgIHdpZHRoOiAwO1xyXG4gIH1cclxuXHJcbiAgLmRlbGl2ZXJ5LXNlcnZpY2Uge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gIH1cclxufVxyXG5cclxudGFibGUge1xyXG4gIHRhYmxlLWxheW91dDogZml4ZWQ7XHJcbn1cclxuXHJcbi50b3AtdGFibGUgdHIge1xyXG4gIHdpZHRoOiB1bnNldDtcclxufVxyXG5cclxuLy8gLmJvdHRvbS10YWJsZSB7XHJcbi8vICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuXHJcbi8vICAgdHIsIHRkIHtcclxuLy8gICAgIGhlaWdodDogMTAwJTtcclxuLy8gICAgIG92ZXJmbG93LXdyYXA6IGJyZWFrLXdvcmQ7XHJcbi8vICAgfVxyXG5cclxuLy8gICB0aCB7XHJcbi8vICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgICBwYWRkaW5nOiAwO1xyXG4vLyAgIH1cclxuICBcclxuLy8gICB0ZCB7XHJcbi8vICAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG5cclxuLy8gICAgIC8vIHdpZHRoOiAzMHB4O1xyXG4vLyAgICAgLy8gb3ZlcmZsb3c6IGhpZGRlbjtcclxuLy8gICAgIC8vIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuLy8gICAgIC8vIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbi8vICAgfVxyXG5cclxuLy8gICAucm93X25vIHtcclxuLy8gICAgIHdpZHRoOiAwO1xyXG4vLyAgIH1cclxuXHJcbi8vICAgLnRkLXByb2R1Y3RzIHtcclxuLy8gICAgIHBhZGRpbmc6IDA7XHJcbi8vICAgICB3aWR0aDogMDtcclxuXHJcbi8vICAgICB0ciB7XHJcbi8vICAgICAgIGJvcmRlci1sZWZ0OiBub25lO1xyXG4vLyAgICAgICBib3JkZXItdG9wOiBub25lO1xyXG4vLyAgICAgICBib3JkZXItcmlnaHQ6IG5vbmU7XHJcbi8vICAgICB9XHJcbi8vICAgICB0ZCB7XHJcbi8vICAgICAgIGJvcmRlci10b3A6IG5vbmU7XHJcbi8vICAgICB9XHJcbi8vICAgfVxyXG4vLyB9XHJcblxyXG50YWJsZS5ib3R0b20tdGFibGV7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XHJcbiAgdGFibGUtbGF5b3V0OiBmaXhlZDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gIHRyLCB0ZCB7XHJcbiAgICBvdmVyZmxvdy13cmFwOiBicmVhay13b3JkO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgdGQge1xyXG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuICB9XHJcblxyXG4gIHRhYmxlLmluc2lkZS1sb29wIHtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbn1cclxuXHJcbi53aWR0aC12YXJpYWJsZSB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XHJcbiAgLy8gb3ZlcmZsb3c6IGhpZGRlbjtcclxuICB3aWR0aDogYXV0bztcclxuICAvLyB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG5cclxufVxyXG5cclxuLndpZHRoLWZpeGVkIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIHdpZHRoOiAxMDBweDtcclxufVxyXG5cclxuLndpZHRoLWZpeGVkLW5vIHtcclxuICB3aWR0aDogMzZweDtcclxufVxyXG5cclxuLndpZHRoLWZpeGVkLW5vLWJhcmFuZyB7XHJcbiAgd2lkdGg6IDU2cHg7XHJcbn1cclxuXHJcbi50ZC1wcm9kdWN0cyB7XHJcbiAgcGFkZGluZzogMDtcclxuICB3aWR0aDogMDtcclxuXHJcbiAgdHIge1xyXG4gICAgYm9yZGVyLWxlZnQ6IG5vbmU7XHJcbiAgICBib3JkZXItdG9wOiBub25lO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiBub25lO1xyXG4gIH1cclxuICB0ZCB7XHJcbiAgICBib3JkZXItdG9wOiBub25lO1xyXG4gIH1cclxufVxyXG5cclxuLnRyLWxvb3Bpbmc6bGFzdC1jaGlsZCB7XHJcbiAgYm9yZGVyLWJvdHRvbTpub25lO1xyXG5cclxuICB0ZCB7XHJcbiAgICBib3JkZXItYm90dG9tOm5vbmU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgcHJpbnQge1xyXG4gIEBwYWdlIHtcclxuICAgIHNpemU6IEE0IGxhbmRzY2FwZTtcclxuICB9XHJcbiAgYm9keSAqIHtcclxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxuICB9XHJcbiAgLmNvbnRlbnQge1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBcclxuICAgIC8vIG1hcmdpbjogMzBtbSA0NW1tIDMwbW0gNDVtbTsgXHJcbiAgICAvLyBwYWRkaW5nOiAwY20gMWNtO1xyXG4gICAgLy8gcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xyXG4gICAgYm9yZGVyOndoaXRlIHNvbGlkO1xyXG4gICAgLy8gcGFnZS1icmVhay1iZWZvcmU6IGFsd2F5cztcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgLmJvdHRvbS10YWJsZSB7XHJcbiAgICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xyXG4gIH1cclxuICAud2F0ZXJtYXJrXHJcbiAge1xyXG4gICAgc3BhbiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTIwcHg7XHJcbiAgICAgIG9wYWNpdHk6IDAuMjtcclxuICAgIH0gICBcclxuICBcclxuIFxyXG4gIH1cclxuICAjaGVhZGVyIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBoNSB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgYXBwLXBhZ2UtaGVhZGVyIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBidXR0b24ge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcblxyXG4gICoge1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gIH1cclxuXHJcbiAgZGl2LnRhYmxlLW9yZGVyIHtcclxuICAgIHRyYW5zZm9ybTogc2NhbGUoMC43KTtcclxuICAgIHBhZGRpbmctYm90dG9tOiA4MHB4O1xyXG4gICAgd2lkdGg6IDIwY207XHJcbiAgICBoZWlnaHQ6IDljbTtcclxuICB9XHJcblxyXG4gIGRpdi5fY29udGFpbmVyIHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICAvLyBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXM7XHJcbiAgfVxyXG5cclxuICAuY29udGVudCB7XHJcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gICNzZWN0aW9uLXRvLXByaW50LCAjc2VjdGlvbi10by1wcmludCAqIHtcclxuICAgIHZpc2liaWxpdHk6IHZpc2libGU7XHJcbiAgfVxyXG4gICNzZWN0aW9uLXRvLXByaW50IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0b3A6IDA7XHJcbiAgfVxyXG5cclxufVxyXG5cclxuIiwiYm9keSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBmb250LWZhbWlseTogXCJDYW1icmlhIChIZWFkaW5ncylcIjtcbiAgY29sb3I6ICMzMzM7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtc2l6ZTogMTFwdDtcbiAgbWFyZ2luOiAwO1xufVxuXG5mb290ZXIge1xuICBjbGVhcjogYm90aDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDIwMHB4O1xuICBtYXJnaW4tdG9wOiAtMjAwcHg7XG59XG5cbi5tYXQtcmFpc2VkLWJ1dHRvbi5tYXQtcHJpbWFyeSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNDgxZmI7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZzogNXB4IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiBcIkNhbWJyaWEgKEhlYWRpbmdzKVwiO1xuICBtYXJnaW46IDEwcHggNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLm1hdC1yYWlzZWQtYnV0dG9uLm1hdC1wcmltYXJ5IC5mYSB7XG4gIG1hcmdpbi1sZWZ0OiA4cHg7XG59XG5cbi5fY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIG1hcmdpbjogMHB4IDBweCAzMHB4IDBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xuICBicmVhay1iZWZvcmU6IGFsd2F5cztcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uX2NvbnRhaW5lciB0YWJsZSB0aCB7XG4gIHBhZGRpbmc6IDJweDtcbn1cbi5fY29udGFpbmVyIC53YXRlcm1hcmsge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMTIwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIHdpZHRoOiAxMDAlO1xuICBsZWZ0OiAwO1xuICB0b3A6IDUwJTtcbiAgcmlnaHQ6IDA7XG4gIGNvbG9yOiByZWQ7XG4gIHRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7XG59XG4uX2NvbnRhaW5lciAud2F0ZXJtYXJrIHNwYW4ge1xuICBvcGFjaXR5OiAwLjE7XG4gIGxldHRlci1zcGFjaW5nOiAxMDBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW46IDAgMzZweDtcbiAgcGFkZGluZzogMCAwIDM2cHggMDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5sb2dvLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5sb2dvLWNvbnRhaW5lciBpbWcge1xuICBtYXJnaW46IDIwcHggMHB4O1xuICB3aWR0aDogMTIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAuYm9keS1sZXR0ZXIge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLmJvZHktbGV0dGVyIHRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAuYm9keS1sZXR0ZXIgLnRhYmxlMSB7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5ib2R5LWxldHRlciAudGFibGUxIC5maXJzdC1jb2wge1xuICB3aWR0aDogMjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5ib2R5LWxldHRlciAudGFibGUxIHRyIHtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgcGFkZGluZzogMDtcbiAgYm9yZGVyOiBub25lO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLmJvZHktbGV0dGVyIC50YWJsZTEgdHIgdGQge1xuICBwYWRkaW5nOiAwO1xuICBib3JkZXI6IG5vbmU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAuYm9keS1sZXR0ZXIgLnRhYmxlMiB0aCB7XG4gIGZvbnQtc2l6ZTogMTBwdDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLmJvZHktbGV0dGVyIC50YWJsZTIgLm5vIHtcbiAgd2lkdGg6IDIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAuYm9keS1sZXR0ZXIgLnRhYmxlMiB0ZCB7XG4gIHBhZGRpbmc6IDVweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5ib2R5LWxldHRlciAudGFibGUzIHRoIHtcbiAgZm9udC1zaXplOiAxMHB0O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLmJvZHktbGV0dGVyIC50YWJsZTMgdGQge1xuICBwYWRkaW5nOiA1cHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAuYm9keS1sZXR0ZXIgLnRhYmxlMyAubm8ge1xuICB3aWR0aDogMjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5ib2R5LWxldHRlciAudGFiZWwxLTEge1xuICBmb250LXN0eWxlOiBpdGFsaWM7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAucGVuZXJpbWEge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnBlbmVyaW1hLXBlbWJlcmkge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHdpZHRoOiAxMDAlO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLmxheWFuYW4ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAucGFnZS1udW1iZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE4cHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxNGNtY207XG4gIGZvbnQtc2l6ZTogMTFweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAub3JkZXItaWQtaGVhZGVyIHtcbiAgd2lkdGg6IDcwJTtcbiAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbiAgYm9yZGVyLXRvcDogMDtcbiAgd2lkdGg6IDgwJTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuaGVhZGVyIHtcbiAgdGFibGUtbGF5b3V0OiBhdXRvO1xuICBib3JkZXItYm90dG9tOiAwO1xuICB3aWR0aDogMjBjbTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgd2lkdGg6IDIwY207XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAucm93LWVxLWhlaWdodCB7XG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLnJvdyB7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5kZXRhaWwtYmFyYW5nIHtcbiAgYm9yZGVyLXJpZ2h0OiBzb2xpZCBibGFjayAxcHg7XG4gIHdpZHRoOiA1MCU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLWJhcmFuZyAuZGFmdGFyLWJhcmFuZyB7XG4gIHBhZGRpbmctbGVmdDogMjBweDtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLmRldGFpbC1iYXJhbmcgLmNhdGF0YW4ge1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLmRldGFpbC1wZW5naXJpbWFuIHtcbiAgd2lkdGg6IDUwJTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5kZXRhaWwtcGVuZ2lyaW1hbiAuamVuaXMtcGVuZ2lyaW1hbiB7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5kZXRhaWwtcGVuZ2lyaW1hbiAudG90YWwtaGFyZ2Ege1xuICBwYWRkaW5nLXRvcDogMzBweDtcbiAgcGFkZGluZy1ib3R0b206IDMwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIHtcbiAgd2lkdGg6IDIwY207XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGJvcmRlci10b3A6IDA7XG4gIGJvcmRlci1ib3R0b206IDA7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1oZWFkZXIgLmRldGFpbC1wZXJzb24ge1xuICBib3JkZXItcmlnaHQ6IHNvbGlkIGJsYWNrIDFweDtcbiAgcGFkZGluZzogMjBweDtcbiAgd2lkdGg6IDYwJTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWhlYWRlciAuZGV0YWlsLXBlcnNvbiAucmVjZWl2ZXIge1xuICBib3JkZXItYm90dG9tOiBibGFjayBzb2xpZCAxcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIC5kZXRhaWwtb3JkZXIge1xuICBwYWRkaW5nOiAyMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIC5kZXRhaWwtb3JkZXIgLnRsYyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWhlYWRlciAucm93IHtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAubG9jYXJkLWxvZ28ge1xuICB3aWR0aDogMTAwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAubG9jYXJkLWxvZ28gLmxvZ28tYXZhdGFyIHtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlci1yYWRpdXM6IDAlO1xuICB3aWR0aDogNjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuY291cmllci1sb2dvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyICNjb3VyaWVyLWxvZ28ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgI2NvdXJpZXItbG9nbyAubG9nby1hdmF0YXIge1xuICB3aWR0aDogMTIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmF3YiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAub3JkZXItaWQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLm9yZGVyLWlkIHAge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYnRuLXByaW50IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDQ2cHggMCAxNXB4IDA7XG59XG4uYnRuLXByaW50IC5iYWNrX2J1dHRvbiB7XG4gIHBhZGRpbmc6IDEwcHggMjBweDtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbn1cbi5idG4tcHJpbnQgLmJ0bi1wcmltYXJ5IHtcbiAgZmxvYXQ6IG5vbmU7XG4gIG1hcmdpbjogMDtcbn1cblxuZGl2Ll9jb250YWluZXI6bGFzdC1jaGlsZCB7XG4gIHBhZ2UtYnJlYWstYWZ0ZXI6IGFsd2F5cztcbn1cblxudGFibGUge1xuICBib3JkZXI6IDJweCBzb2xpZCAjMzMzO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xufVxuXG50ZCxcbnRyLFxudGgge1xuICBwYWRkaW5nOiAxMnB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjMzMzO1xuICB3aWR0aDogMTg1cHg7XG59XG5cbnRoIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcbn1cblxuaDQsXG5wIHtcbiAgbWFyZ2luOiAwcHg7XG59XG5cbiNnYXJpcyB7XG4gIGhlaWdodDogMjAwcHg7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuI3RoYW5rcyB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzBlYThkYjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbiNoZWFkZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbiNub24ge1xuICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nOiA0cHg7XG59XG5cbiNzaGlwcGluZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuI29yZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3R0b206IDEwcHg7XG59XG5cbi5zZW5kIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uc3RpY2t5IHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uc3RpY2t5ICsgI2NhcmQtdHJhbnNhY3Rpb24ge1xuICBwYWRkaW5nLXRvcDogNTBweDtcbn1cblxuLnRvcC10YWJsZSB7XG4gIGJvcmRlcjogbm9uZTtcbiAgbWFyZ2luOiAwIDAgMTVweCAwO1xuICBwYWRkaW5nOiA1cHg7XG59XG4udG9wLXRhYmxlIHRyIHtcbiAgYm9yZGVyOiBub25lO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDVweDtcbn1cbi50b3AtdGFibGUgdGQge1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICBib3JkZXI6IG5vbmU7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogNXB4O1xufVxuLnRvcC10YWJsZSAudGl0bGUge1xuICB3aWR0aDogMTAwcHg7XG59XG4udG9wLXRhYmxlIC5jb2xvbiB7XG4gIHdpZHRoOiAwO1xufVxuLnRvcC10YWJsZSAuZGVsaXZlcnktc2VydmljZSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbnRhYmxlIHtcbiAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbn1cblxuLnRvcC10YWJsZSB0ciB7XG4gIHdpZHRoOiB1bnNldDtcbn1cblxudGFibGUuYm90dG9tLXRhYmxlIHtcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gIHRhYmxlLWxheW91dDogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG50YWJsZS5ib3R0b20tdGFibGUgdHIsIHRhYmxlLmJvdHRvbS10YWJsZSB0ZCB7XG4gIG92ZXJmbG93LXdyYXA6IGJyZWFrLXdvcmQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbnRhYmxlLmJvdHRvbS10YWJsZSB0ZCB7XG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XG59XG50YWJsZS5ib3R0b20tdGFibGUgdGFibGUuaW5zaWRlLWxvb3Age1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuLndpZHRoLXZhcmlhYmxlIHtcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gIHdpZHRoOiBhdXRvO1xufVxuXG4ud2lkdGgtZml4ZWQge1xuICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgd2lkdGg6IDEwMHB4O1xufVxuXG4ud2lkdGgtZml4ZWQtbm8ge1xuICB3aWR0aDogMzZweDtcbn1cblxuLndpZHRoLWZpeGVkLW5vLWJhcmFuZyB7XG4gIHdpZHRoOiA1NnB4O1xufVxuXG4udGQtcHJvZHVjdHMge1xuICBwYWRkaW5nOiAwO1xuICB3aWR0aDogMDtcbn1cbi50ZC1wcm9kdWN0cyB0ciB7XG4gIGJvcmRlci1sZWZ0OiBub25lO1xuICBib3JkZXItdG9wOiBub25lO1xuICBib3JkZXItcmlnaHQ6IG5vbmU7XG59XG4udGQtcHJvZHVjdHMgdGQge1xuICBib3JkZXItdG9wOiBub25lO1xufVxuXG4udHItbG9vcGluZzpsYXN0LWNoaWxkIHtcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcbn1cbi50ci1sb29waW5nOmxhc3QtY2hpbGQgdGQge1xuICBib3JkZXItYm90dG9tOiBub25lO1xufVxuXG5AbWVkaWEgcHJpbnQge1xuICBAcGFnZSB7XG4gICAgc2l6ZTogQTQgbGFuZHNjYXBlO1xuICB9XG4gIGJvZHkgKiB7XG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICB9XG5cbiAgLmNvbnRlbnQge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBib3JkZXI6IHdoaXRlIHNvbGlkO1xuICB9XG5cbiAgLmJvdHRvbS10YWJsZSB7XG4gICAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbiAgfVxuXG4gIC53YXRlcm1hcmsgc3BhbiB7XG4gICAgZm9udC1zaXplOiAxMjBweDtcbiAgICBvcGFjaXR5OiAwLjI7XG4gIH1cblxuICAjaGVhZGVyIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgaDUge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICBhcHAtcGFnZS1oZWFkZXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICBidXR0b24ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAqIHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gIH1cblxuICBkaXYudGFibGUtb3JkZXIge1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMC43KTtcbiAgICBwYWRkaW5nLWJvdHRvbTogODBweDtcbiAgICB3aWR0aDogMjBjbTtcbiAgICBoZWlnaHQ6IDljbTtcbiAgfVxuXG4gIGRpdi5fY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG5cbiAgLmNvbnRlbnQge1xuICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgfVxuXG4gICNzZWN0aW9uLXRvLXByaW50LCAjc2VjdGlvbi10by1wcmludCAqIHtcbiAgICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xuICB9XG5cbiAgI3NlY3Rpb24tdG8tcHJpbnQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/packinglist/generate/packing-list.generate.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/layout/modules/packinglist/generate/packing-list.generate.component.ts ***!
  \****************************************************************************************/
/*! exports provided: PackingListGenerateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackingListGenerateComponent", function() { return PackingListGenerateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var PackingListGenerateComponent = /** @class */ (function () {
    function PackingListGenerateComponent(OrderhistoryService, router, route, memberService) {
        this.OrderhistoryService = OrderhistoryService;
        this.router = router;
        this.route = route;
        this.memberService = memberService;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__;
        this.titleProgram = "";
        this.getData = [];
        this.totalResult = [];
        this.totalWeight = [];
        this.perWeight = [];
        this.endTotalResult = [];
        this.special = [];
        this.loading = false;
        this.same = false;
        this.merchant_data = {};
        this.courier_image_array = [];
        this.sig_project = false;
        this.sig_kontraktual = false;
        this.delivery_list = [];
        // this.getData = route.snapshot.params['invoiceIds']
        //   .split(',');
    }
    PackingListGenerateComponent.prototype.ngOnInit = function () {
        this.firstload();
        // this.print();
    };
    PackingListGenerateComponent.prototype.firstload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this;
            var _this_1 = this;
            return __generator(this, function (_a) {
                program = localStorage.getItem('programName');
                _this = this;
                this.contentList.filter(function (element) {
                    if (element.appLabel == program) {
                        if (program && program == "retail_poin_sahabat") {
                            _this.sig_project = true;
                        }
                        else if (program && program == "sig_kontraktual") {
                            _this.sig_kontraktual = true;
                            _this.titleProgram = element.title;
                        }
                        else {
                            _this.titleProgram = element.title;
                        }
                    }
                });
                // console.log("titleProgram", this.titleProgram);
                // console.warn('this data', this.detail)
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this_1, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        this.getData = params.data;
                        this.service = this.OrderhistoryService;
                        return [2 /*return*/];
                    });
                }); });
                // this.detail.forEach(element => {
                //   console.log("data", element)
                //   if(element.member_detail.hadiah_dikuasakan == 'ya' || element.member_detail.hadiah_dikuasakan == 'KUASA' || element.member_detail.hadiah_dikuasakan == 'true' ){
                //    this.special.push(element)
                //   }
                // });
                // window.print();
                this.loading = false;
                // console.log("detail.delivery_list", this.detail.delivery_list);
                this.detail.delivery_list.forEach(function (result) {
                    console.log("result", result);
                    if (_this_1.delivery_list.filter(function (e) { return e.reference_no === result.reference_no; }).length > 0) {
                        _this_1.delivery_list.filter(function (e) { return e.reference_no === result.reference_no; })[0].product_list.push(result.product_list);
                    }
                    else {
                        _this_1.delivery_list.push({
                            awb_number: result.awb_number,
                            reference_no: result.reference_no,
                            destination: result.destination,
                            status: result.status,
                            product_list: [result.product_list],
                            po_no: result.po_no
                        });
                    }
                });
                console.log("this.delivery_list", this.delivery_list);
                return [2 /*return*/];
            });
        });
    };
    PackingListGenerateComponent.prototype.prints = function () {
        window.print();
    };
    PackingListGenerateComponent.prototype.backToHere = function () {
        // console.log("obj", obj);
        console.log("this.back", this.back);
        console.log("this.back[1]", this.back[1]);
        console.log("this.back[0]", this.back[0]);
        // this.evoucherStockDetail = false;
        this.back[1](this.back[0]);
        // this.firstLoad();
    };
    PackingListGenerateComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PackingListGenerateComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PackingListGenerateComponent.prototype, "back", void 0);
    PackingListGenerateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-packing-list-generate',
            template: __webpack_require__(/*! raw-loader!./packing-list.generate.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/packinglist/generate/packing-list.generate.component.html"),
            styles: [__webpack_require__(/*! ./packing-list.generate.component.scss */ "./src/app/layout/modules/packinglist/generate/packing-list.generate.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"]])
    ], PackingListGenerateComponent);
    return PackingListGenerateComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/packinglist/packinglist-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/modules/packinglist/packinglist-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: OrderhistoryallhistoryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryallhistoryRoutingModule", function() { return OrderhistoryallhistoryRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _packinglist_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./packinglist.component */ "./src/app/layout/modules/packinglist/packinglist.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';
var routes = [
    {
        path: '', component: _packinglist_component__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryallhistoryComponent"]
    }
];
var OrderhistoryallhistoryRoutingModule = /** @class */ (function () {
    function OrderhistoryallhistoryRoutingModule() {
    }
    OrderhistoryallhistoryRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OrderhistoryallhistoryRoutingModule);
    return OrderhistoryallhistoryRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/packinglist/packinglist.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/packinglist/packinglist.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n.container-swapper-option {\n  display: flex;\n  justify-content: center;\n  flex-direction: row;\n  align-items: center;\n}\n.container-swapper-option .button-container {\n  border: 2px solid #ddd;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button {\n  color: #ddd;\n  background: white;\n  font-size: 14px;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button.true {\n  background: #2480fb;\n  border-radius: 8px;\n  color: #ddd;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcGFja2luZ2xpc3QvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxwYWNraW5nbGlzdFxccGFja2luZ2xpc3QuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3BhY2tpbmdsaXN0L3BhY2tpbmdsaXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNDSjtBRENJO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ0NSO0FER0E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDQUo7QURDSTtFQUVFLHNCQUFBO0VBQ0Esa0JBQUE7QUNBTjtBRENNO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQ1I7QURDTTtFQUVFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDQVIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wYWNraW5nbGlzdC9wYWNraW5nbGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jb250YWluZXItc3dhcHBlci1vcHRpb24ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAuYnV0dG9uLWNvbnRhaW5lciB7XHJcblxyXG4gICAgICBib3JkZXI6IDJweCBzb2xpZCAjZGRkO1xyXG4gICAgICBib3JkZXItcmFkaXVzOjhweDtcclxuICAgICAgYnV0dG9uIHtcclxuICAgICAgICBjb2xvcjogI2RkZDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICB9XHJcbiAgICAgIGJ1dHRvbi50cnVlIHtcclxuICAgICAgIFxyXG4gICAgICAgIGJhY2tncm91bmQ6ICMyNDgwZmI7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgIGNvbG9yOiAjZGRkO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSIsIi5lcnJvci1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5lcnJvci1tZXNzYWdlIC50ZXh0LW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xuICBwYWRkaW5nOiAyMHB4O1xuICBtYXJnaW46IDAgMTBweDtcbn1cblxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiAuYnV0dG9uLWNvbnRhaW5lciB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNkZGQ7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbn1cbi5jb250YWluZXItc3dhcHBlci1vcHRpb24gLmJ1dHRvbi1jb250YWluZXIgYnV0dG9uIHtcbiAgY29sb3I6ICNkZGQ7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbn1cbi5jb250YWluZXItc3dhcHBlci1vcHRpb24gLmJ1dHRvbi1jb250YWluZXIgYnV0dG9uLnRydWUge1xuICBiYWNrZ3JvdW5kOiAjMjQ4MGZiO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGNvbG9yOiAjZGRkO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/packinglist/packinglist.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/packinglist/packinglist.component.ts ***!
  \*********************************************************************/
/*! exports provided: OrderhistoryallhistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryallhistoryComponent", function() { return OrderhistoryallhistoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { Router } from '@angular/router';

var OrderhistoryallhistoryComponent = /** @class */ (function () {
    function OrderhistoryallhistoryComponent(orderhistoryService) {
        this.orderhistoryService = orderhistoryService;
        this.Orderhistoryallhistory = [];
        this.errorMessage = false;
        this.tableFormat = {
            title: 'Packing List',
            label_headers: [
                { label: 'Packing Number', visible: true, type: 'string', data_row_name: 'packing_no' },
                { label: 'Courier Code', visible: true, type: 'date', data_row_name: 'courier_code' },
                { label: 'Courier Name', visible: true, type: 'date', data_row_name: 'courier_name' },
                { label: 'Delivery Service', visible: true, type: 'string', data_row_name: 'delivery_service' },
                { label: 'Delivery Method', visible: true, type: 'string', data_row_name: 'delivery_method' },
                { label: 'Total Order', visible: true, type: 'reference_no_length', data_row_name: 'reference_no' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            ],
            row_primary_key: 'order_id',
            formOptions: {
                row_id: 'order_id',
                this: this,
                result_var_name: 'Orderhistoryallhistory',
                detail_function: [this, 'callDetail'],
            },
            show_checkbox_options: true
        };
        this.tableFormat2 = {
            title: 'Packing List',
            label_headers: [
                { label: 'Packing Number', visible: true, type: 'string', data_row_name: 'packing_no' },
                { label: 'Courier Code', visible: true, type: 'date', data_row_name: 'courier_code' },
                { label: 'Courier Name', visible: true, type: 'date', data_row_name: 'courier_name' },
                { label: 'Delivery Service', visible: true, type: 'string', data_row_name: 'delivery_service' },
                { label: 'Delivery Method', visible: true, type: 'string', data_row_name: 'delivery_method' },
                { label: 'Total Order', visible: true, type: 'reference_no_length', data_row_name: 'reference_no' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Delivery Date', visible: true, type: 'date', data_row_name: 'delivery_date' },
            ],
            row_primary_key: 'order_id',
            formOptions: {
                row_id: 'order_id',
                this: this,
                result_var_name: 'Orderhistoryallhistory',
                detail_function: [this, 'callDetail'],
            },
            show_checkbox_options: true
        };
        this.form_input = {};
        this.errorLabel = false;
        this.swaper = true;
        this.packingListDetail = false;
        this.programType = "";
        this.mci_project = false;
        this.sig_kontraktual = false;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__;
    }
    OrderhistoryallhistoryComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderhistoryallhistoryComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program_1, _this_1, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        program_1 = localStorage.getItem('programName');
                        _this_1 = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program_1) {
                                if (element.type == "reguler") {
                                    _this_1.mci_project = true;
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this_1.programType = "custom_kontraktual";
                                    _this_1.sig_kontraktual = true;
                                }
                                else {
                                    _this_1.mci_project = false;
                                }
                            }
                        });
                        result = void 0;
                        if (!(this.swaper == true)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.orderhistoryService.getPackingListReport('ACTIVE')];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.orderhistoryService.getPackingListReport('PROCESSED')];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        this.service = this.orderhistoryService;
                        console.warn('rsult', result);
                        this.totalPage = result.total_page;
                        this.Orderhistoryallhistory = result.values;
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); // conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    OrderhistoryallhistoryComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.packingListDetail = this.Orderhistoryallhistory.find(function (packing) { return packing.packing_no == rowData.packing_no; });
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryallhistoryComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.packingListDetail = false;
                obj.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryallhistoryComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
        this.firstLoad();
    };
    OrderhistoryallhistoryComponent.prototype.processSelectedOrders = function (obj, orders) {
        return __awaiter(this, void 0, void 0, function () {
            var _this_1 = this;
            return __generator(this, function (_a) {
                if (orders.length > 0) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                        title: 'Confirmation',
                        text: 'Apakah anda yakin ingin memproses semua order yang dipilih',
                        icon: 'success',
                        confirmButtonText: 'process',
                        cancelButtonText: "cancel",
                        showCancelButton: true
                    }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
                        var _this_1 = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!result.isConfirmed) return [3 /*break*/, 2];
                                    return [4 /*yield*/, orders.forEach(function (order_id) { return __awaiter(_this_1, void 0, void 0, function () {
                                            var payload, e_2;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        payload = {
                                                            order_id: order_id
                                                        };
                                                        _a.label = 1;
                                                    case 1:
                                                        _a.trys.push([1, 3, , 4]);
                                                        return [4 /*yield*/, obj.orderhistoryService.processOrder(payload)];
                                                    case 2:
                                                        _a.sent();
                                                        return [3 /*break*/, 4];
                                                    case 3:
                                                        e_2 = _a.sent();
                                                        return [3 /*break*/, 4];
                                                    case 4:
                                                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire('Success', 'Semua order terpilih telah disetujui', 'success');
                                                        return [2 /*return*/];
                                                }
                                            });
                                        }); })];
                                case 1:
                                    _a.sent();
                                    _a.label = 2;
                                case 2: return [2 /*return*/];
                            }
                        });
                    }); });
                }
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryallhistoryComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderhistoryallhistoryComponent.prototype, "back", void 0);
    OrderhistoryallhistoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-packinglist',
            template: __webpack_require__(/*! raw-loader!./packinglist.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/packinglist/packinglist.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./packinglist.component.scss */ "./src/app/layout/modules/packinglist/packinglist.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"]])
    ], OrderhistoryallhistoryComponent);
    return OrderhistoryallhistoryComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/packinglist/packinglist.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/modules/packinglist/packinglist.module.ts ***!
  \******************************************************************/
/*! exports provided: PackingListModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackingListModule", function() { return PackingListModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _packinglist_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./packinglist-routing.module */ "./src/app/layout/modules/packinglist/packinglist-routing.module.ts");
/* harmony import */ var _packinglist_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./packinglist.component */ "./src/app/layout/modules/packinglist/packinglist.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _detail_packing_list_detail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./detail/packing-list.detail.component */ "./src/app/layout/modules/packinglist/detail/packing-list.detail.component.ts");
/* harmony import */ var _generate_packing_list_generate_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./generate/packing-list.generate.component */ "./src/app/layout/modules/packinglist/generate/packing-list.generate.component.ts");
/* harmony import */ var _orderhistoryallhistory_edit_orderhistoryallhistory_edit_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../orderhistoryallhistory/edit/orderhistoryallhistory.edit.module */ "./src/app/layout/modules/orderhistoryallhistory/edit/orderhistoryallhistory.edit.module.ts");
/* harmony import */ var ngx_barcode__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-barcode */ "./node_modules/ngx-barcode/index.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/esm5/radio.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











// import { OrderhistoryallhistoryEditComponent } from './edit/orderhistoryallhistory.edit.component';




var PackingListModule = /** @class */ (function () {
    function PackingListModule() {
    }
    PackingListModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _packinglist_routing_module__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryallhistoryRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                ngx_barcode__WEBPACK_IMPORTED_MODULE_12__["NgxBarcodeModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_13__["MatButtonModule"],
                _angular_material_radio__WEBPACK_IMPORTED_MODULE_14__["MatRadioModule"],
                _orderhistoryallhistory_edit_orderhistoryallhistory_edit_module__WEBPACK_IMPORTED_MODULE_11__["OrderhistoryallhistoryEditModule"]
            ],
            declarations: [
                _packinglist_component__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryallhistoryComponent"],
                _detail_packing_list_detail_component__WEBPACK_IMPORTED_MODULE_9__["PackingListReportDetailComponent"],
                _generate_packing_list_generate_component__WEBPACK_IMPORTED_MODULE_10__["PackingListGenerateComponent"]
            ],
            exports: [
                _packinglist_component__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryallhistoryComponent"],
            ]
        })
    ], PackingListModule);
    return PackingListModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-packinglist-packinglist-module.js.map