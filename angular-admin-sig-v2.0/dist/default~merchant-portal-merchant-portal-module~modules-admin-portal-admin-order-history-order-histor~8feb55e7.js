(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~merchant-portal-merchant-portal-module~modules-admin-portal-admin-order-history-order-histor~8feb55e7"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-per-day/order-histories-per-day.component.html":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-per-day/order-histories-per-day.component.html ***!
  \*******************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card \" *ngIf=\"analitycsData.barChartLabels.length\">\r\n    <div class=\"card-header\">\r\n        Order History Per Day\r\n    </div>\r\n    <div class=\"card-body\">\r\n        <canvas baseChart [datasets]=\"analitycsData.barChartData\"\r\n            [labels]=\"analitycsData.barChartLabels\" [options]=\"barChartOptions\"\r\n            [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n            (chartClick)=\"chartClicked($event)\">\r\n        </canvas>\r\n    </div>\r\n    <!-- <div class=\"card-footer\">\r\n            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n        </div> -->\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-total-today-order/order-histories-total-today-order.component.html":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-total-today-order/order-histories-total-today-order.component.html ***!
  \***************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card sum\" *ngIf=\"data.current_order_history\">\r\n    <div class=\"card-header\"> Today Total Order</div>\r\n    <div class=\"card-body\">\r\n        <table class=\"table table-responsive-md table-responsive-lg table-responsive-sm table-responsive-xl\" style=\"text-align: left\">\r\n            <tr>\r\n                <th>All Order</th>\r\n                <td>{{data.current_order_history.total}} </td>\r\n\r\n                <th>Pending</th>\r\n                <td>{{data.current_order_history_pending.total}} </td>\r\n            </tr>\r\n            <tr>\r\n                <th>Paid</th>\r\n                <td>{{data.current_order_history_cancel.total}} </td>\r\n\r\n                <th>Checkout</th>\r\n                <td>{{data.current_order_history_paid.total}} </td>\r\n            </tr>\r\n            <tr>\r\n                <th>Cancel</th>\r\n                <td>{{data.current_order_history_waiting.total}} </td>\r\n\r\n                <th>Waiting</th>\r\n                <td>{{data.current_order_history_checkout.total}} </td>\r\n            </tr>\r\n        </table>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component.html":
/*!****************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component.html ***!
  \****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <app-page-header *ngIf=\"topBarMenu\" [heading]=\"'All Order History (Sales)'\" [icon]=\"'fa-table'\"\r\n        [topBarMenu]=\"topBarMenu\"></app-page-header>\r\n    <div class=\"summary-report row\">\r\n        <div class=\"col-md-8\">\r\n            <div class=\"row-per-card\">\r\n                <app-order-histories-per-day *ngIf=\"analitycsData.daily.barChartLabels.length\"\r\n                    [analitycsData]=\"analitycsData.daily\" [barChartOptions]=\"barChartOptions\"\r\n                    [barChartLegend]=\"barChartLegend\"></app-order-histories-per-day>\r\n            </div>\r\n            <div class=\"row\" style=\"margin-bottom: 20px; text-align: center\">\r\n                <div class=\"col-md-6\">\r\n\r\n                    <div class=\"card \" *ngIf=\"analitycsData.monthly\">\r\n                        <div class=\"card-header\">\r\n                            monthly analytics\r\n                        </div>\r\n                        <div class=\"card-body\">\r\n                            <canvas baseChart [datasets]=\"analitycsData.monthly.barChartData\"\r\n                                [labels]=\"analitycsData.monthly.barChartLabels\" [options]=\"barChartOptions\"\r\n                                [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                                (chartClick)=\"chartClicked($event)\">\r\n                            </canvas>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"card \" *ngIf=\"analitycsData.yearly.barChartLabels\">\r\n                        <div class=\"card-header\">\r\n                            Yearly analytics\r\n                        </div>\r\n                        <div class=\"card-body\">\r\n                            <canvas baseChart [datasets]=\"analitycsData.yearly.barChartData\"\r\n                                [labels]=\"analitycsData.yearly.barChartLabels\" [options]=\"barChartOptions\"\r\n                                [legend]=\"barChartLegend\" [chartType]=\"'bar'\" (chartHover)=\"chartHovered($event)\"\r\n                                (chartClick)=\"chartClicked($event)\">\r\n                            </canvas>\r\n                        </div>\r\n                        <!-- <div class=\"card-footer\">\r\n                                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.hourly)\">Refresh</button>\r\n                                        </div> -->\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"row\" style=\"margin-bottom: 20px; text-align: center\">\r\n                <div class=\"col-md-12\">\r\n                    <div class=\"row-per-card\">\r\n                        <div class=\"card \" *ngIf=\"analitycsData.hourly.barChartLabels\">\r\n                            <div class=\"card-header\">\r\n                                Hourly analytics\r\n                            </div>\r\n                            <div class=\"card-body\">\r\n                                <canvas baseChart [datasets]=\"analitycsData.hourly.barChartData\"\r\n                                    [labels]=\"analitycsData.hourly.barChartLabels\" [options]=\"barChartOptions\"\r\n                                    [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                                    (chartClick)=\"chartClicked($event)\">\r\n                                </canvas>\r\n                            </div>\r\n                            <!-- <div class=\"card-footer\">\r\n                                                <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.hourly)\">Refresh</button>\r\n                                            </div> -->\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"col-md-4\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\" style=\"margin-bottom: 20px; text-align: center\">\r\n                    <app-order-histories-total-today-order [data]=\"data\">\r\n                    </app-order-histories-total-today-order>\r\n                </div>\r\n\r\n                <div class=\"col-lg-6 col-md-12\" style=\"margin-bottom: 20px; text-align: center\">\r\n                    <div class=\"card sum\" *ngIf=\"data.current_order_history\">\r\n                        <div class=\"card-header\"> Avg Hourly</div>\r\n                        <div class=\"card-body\">\r\n                            <table style=\"text-align: left\">\r\n                                <tr>\r\n                                    <td>All Order</td>\r\n                                    <td>{{avgData.hourly.all}} </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Pending</td>\r\n                                    <td>{{avgData.hourly.pending}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Paid</td>\r\n                                    <td>{{avgData.hourly.paid}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Checkout</td>\r\n                                    <td>{{avgData.hourly.checkout}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Cancel</td>\r\n                                    <td>{{avgData.hourly.cancel}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Waiting</td>\r\n                                    <td>{{avgData.hourly.waiting}}</td>\r\n                                </tr>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-6 col-md-12\" style=\"margin-bottom: 20px; text-align: center\">\r\n                    <div class=\"card sum\" *ngIf=\"data.current_order_history\">\r\n                        <div class=\"card-header\"> Avg Daily</div>\r\n                        <div class=\"card-body\">\r\n                            <table style=\"text-align: left\">\r\n                                <tr>\r\n                                    <td>All Order</td>\r\n                                    <td>{{avgData.daily.all}} </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Pending</td>\r\n                                    <td>{{avgData.daily.pending}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Paid</td>\r\n                                    <td>{{avgData.daily.paid}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Checkout</td>\r\n                                    <td>{{avgData.daily.checkout}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Cancel</td>\r\n                                    <td>{{avgData.daily.cancel}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Waiting</td>\r\n                                    <td>{{avgData.daily.waiting}}</td>\r\n                                </tr>\r\n\r\n                            </table>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-lg-6 col-md-12\" style=\"margin-bottom: 20px; text-align: center\">\r\n                    <div class=\"card sum\" *ngIf=\"data.current_order_history\">\r\n                        <div class=\"card-header\"> Avg Monthly</div>\r\n                        <div class=\"card-body\">\r\n                            <table style=\"text-align: left\">\r\n                                <tr>\r\n                                    <td>All Order</td>\r\n                                    <td>{{avgData.monthly.all}} </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Pending</td>\r\n                                    <td>{{avgData.monthly.pending}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Paid</td>\r\n                                    <td>{{avgData.monthly.paid}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Checkout</td>\r\n                                    <td>{{avgData.monthly.checkout}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Cancel</td>\r\n                                    <td>{{avgData.monthly.cancel}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Waiting</td>\r\n                                    <td>{{avgData.monthly.waiting}}</td>\r\n                                </tr>\r\n\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-6 col-md-12\" style=\"margin-bottom: 20px; text-align: center\">\r\n                    <div class=\"card sum\" *ngIf=\"data.current_order_history\">\r\n                        <div class=\"card-header\"> Avg Yearly</div>\r\n                        <div class=\"card-body\">\r\n                            <table style=\"text-align: left\">\r\n                                <tr>\r\n                                    <td>All Order</td>\r\n                                    <td>{{avgData.yearly.all}} </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Pending</td>\r\n                                    <td>{{avgData.yearly.pending}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Paid</td>\r\n                                    <td>{{avgData.yearly.paid}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Checkout</td>\r\n                                    <td>{{avgData.yearly.checkout}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Cancel</td>\r\n                                    <td>{{avgData.yearly.cancel}}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Waiting</td>\r\n                                    <td>{{avgData.yearly.waiting}}</td>\r\n                                </tr>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n\r\n                <div class=\"col-md-12\" style=\"float:right\">\r\n                    <div class=\"row-per-card top-20\">\r\n                        <div class=\"card \" *ngIf=\"analitycsData.yearly.barChartLabels\">\r\n                            <div class=\"card-header\">\r\n                                Top 20 Buyers (<em> by number of Transactions</em>)\r\n                            </div>\r\n                            <div class=\"card-body\">\r\n                                <table\r\n                                    class=\"table table-responsive-md table-responsive-lg table-responsive-sm table-responsive-xl\">\r\n                                    <tr>\r\n                                        <th>No.</th>\r\n                                        <th>Name</th>\r\n                                        <th>Number of Transaction</th>\r\n                                    </tr>\r\n                                    <tr *ngFor=\"let m of topMembers;let i = index;\">\r\n                                        <td>{{i+1}}</td>\r\n                                        <td>{{m.user_name}}</td>\r\n                                        <td>{{m.count}}</td>\r\n                                    </tr>\r\n                                </table>\r\n                            </div>\r\n                            <!-- <div class=\"card-footer\">\r\n                                        <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.hourly)\">Refresh</button>\r\n                                    </div> -->\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n\r\n\r\n\r\n    </div>\r\n\r\n\r\n\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-per-day/order-histories-per-day.component.scss":
/*!*****************************************************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-per-day/order-histories-per-day.component.scss ***!
  \*****************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLXBvcnRhbC9hZG1pbi1vcmRlci1oaXN0b3J5L29yZGVyLWhpc3Rvcnktc3VtbWFyeS9jb21wb25lbnRzL29yZGVyLWhpc3Rvcmllcy1wZXItZGF5L29yZGVyLWhpc3Rvcmllcy1wZXItZGF5LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-per-day/order-histories-per-day.component.ts":
/*!***************************************************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-per-day/order-histories-per-day.component.ts ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: OrderHistoriesPerDayComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderHistoriesPerDayComponent", function() { return OrderHistoriesPerDayComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OrderHistoriesPerDayComponent = /** @class */ (function () {
    function OrderHistoriesPerDayComponent() {
    }
    OrderHistoriesPerDayComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderHistoriesPerDayComponent.prototype, "analitycsData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderHistoriesPerDayComponent.prototype, "barChartOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderHistoriesPerDayComponent.prototype, "barChartLegend", void 0);
    OrderHistoriesPerDayComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-histories-per-day',
            template: __webpack_require__(/*! raw-loader!./order-histories-per-day.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-per-day/order-histories-per-day.component.html"),
            styles: [__webpack_require__(/*! ./order-histories-per-day.component.scss */ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-per-day/order-histories-per-day.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], OrderHistoriesPerDayComponent);
    return OrderHistoriesPerDayComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-total-today-order/order-histories-total-today-order.component.scss":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-total-today-order/order-histories-total-today-order.component.scss ***!
  \*************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n  text-align: center;\n}\ntable th {\n  font-size: 12px;\n  font-weight: normal;\n}\ntable tr:nth-child(odd) td {\n  background-color: #f0f0f0;\n}\ntable td, table th {\n  border: 1px solid #ccc;\n  padding: 4px 15px;\n}\ntable td {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tcG9ydGFsL2FkbWluLW9yZGVyLWhpc3Rvcnkvb3JkZXItaGlzdG9yeS1zdW1tYXJ5L2NvbXBvbmVudHMvb3JkZXItaGlzdG9yaWVzLXRvdGFsLXRvZGF5LW9yZGVyL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcYWRtaW4tcG9ydGFsXFxhZG1pbi1vcmRlci1oaXN0b3J5XFxvcmRlci1oaXN0b3J5LXN1bW1hcnlcXGNvbXBvbmVudHNcXG9yZGVyLWhpc3Rvcmllcy10b3RhbC10b2RheS1vcmRlclxcb3JkZXItaGlzdG9yaWVzLXRvdGFsLXRvZGF5LW9yZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbi1wb3J0YWwvYWRtaW4tb3JkZXItaGlzdG9yeS9vcmRlci1oaXN0b3J5LXN1bW1hcnkvY29tcG9uZW50cy9vcmRlci1oaXN0b3JpZXMtdG90YWwtdG9kYXktb3JkZXIvb3JkZXItaGlzdG9yaWVzLXRvdGFsLXRvZGF5LW9yZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQ0NKO0FEQUk7RUFHSSxlQUFBO0VBQ0EsbUJBQUE7QUNBUjtBREVJO0VBQ0kseUJBQUE7QUNBUjtBREVJO0VBQ0ksc0JBQUE7RUFDQSxpQkFBQTtBQ0FSO0FERUk7RUFDSSxlQUFBO0FDQVIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbi1wb3J0YWwvYWRtaW4tb3JkZXItaGlzdG9yeS9vcmRlci1oaXN0b3J5LXN1bW1hcnkvY29tcG9uZW50cy9vcmRlci1oaXN0b3JpZXMtdG90YWwtdG9kYXktb3JkZXIvb3JkZXItaGlzdG9yaWVzLXRvdGFsLXRvZGF5LW9yZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGV7XHJcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGh7XHJcbiAgICAgICAgLy8gYmFja2dyb3VuZDogIzMzMztcclxuICAgICAgICAvLyBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICB9XHJcbiAgICB0cjpudGgtY2hpbGQob2RkKSB0ZHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgfVxyXG4gICAgdGQsIHRoe1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgcGFkZGluZzogNHB4IDE1cHg7XHJcbiAgICB9XHJcbiAgICB0ZHtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcblxyXG4gICAgfVxyXG59IiwidGFibGUge1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxudGFibGUgdGgge1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG50YWJsZSB0cjpudGgtY2hpbGQob2RkKSB0ZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG50YWJsZSB0ZCwgdGFibGUgdGgge1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBwYWRkaW5nOiA0cHggMTVweDtcbn1cbnRhYmxlIHRkIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-total-today-order/order-histories-total-today-order.component.ts":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-total-today-order/order-histories-total-today-order.component.ts ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: OrderHistoriesTotalTodayOrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderHistoriesTotalTodayOrderComponent", function() { return OrderHistoriesTotalTodayOrderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OrderHistoriesTotalTodayOrderComponent = /** @class */ (function () {
    function OrderHistoriesTotalTodayOrderComponent() {
    }
    OrderHistoriesTotalTodayOrderComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderHistoriesTotalTodayOrderComponent.prototype, "data", void 0);
    OrderHistoriesTotalTodayOrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-histories-total-today-order',
            template: __webpack_require__(/*! raw-loader!./order-histories-total-today-order.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-total-today-order/order-histories-total-today-order.component.html"),
            styles: [__webpack_require__(/*! ./order-histories-total-today-order.component.scss */ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-total-today-order/order-histories-total-today-order.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], OrderHistoriesTotalTodayOrderComponent);
    return OrderHistoriesTotalTodayOrderComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/order-histories-object-setup.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/order-histories-object-setup.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: optionNoFill, analitycsData, generateChartData, calculateTheAverageValue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "optionNoFill", function() { return optionNoFill; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "analitycsData", function() { return analitycsData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "generateChartData", function() { return generateChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calculateTheAverageValue", function() { return calculateTheAverageValue; });
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var optionNoFill = { fill: false, borderWidth: 1 };
var analitycsData = {
    monthly: {
        barChartData: [
            __assign({ data: [], label: 'All Order' }, optionNoFill),
        ],
        barChartLabels: []
    },
    daily: {
        barChartData: [
            __assign({ data: [], label: 'All Order' }, optionNoFill),
        ],
        barChartLabels: []
    },
    hourly: {
        barChartData: [
            __assign({ data: [], label: 'All Order' }, optionNoFill),
        ],
        barChartLabels: []
    },
    yearly: {
        barChartData: [
            __assign({ data: [], label: 'All Order' }, optionNoFill),
        ],
        barChartLabels: []
    }
};
function generateChartData(dataValues) {
    var barChartLabels = [];
    var newData = [];
    // console.log("generate Chart Data", dataValues)
    var allData;
    allData = dataValues;
    var total = 0;
    for (var data in allData) {
        barChartLabels.push(data);
        total += parseInt(allData[data]);
        newData.push(allData[data]);
    }
    allData = null; //clearing memory
    return [barChartLabels, newData, total];
}
function calculateTheAverageValue(_value) {
    var howMany = Object.keys(_value).length;
    var sum = 0;
    Reflect.ownKeys(_value).forEach(function (key) {
        sum = sum + _value[key];
    });
    var avg = sum / howMany;
    return avg;
}


/***/ }),

/***/ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary-routing.module.ts":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary-routing.module.ts ***!
  \*****************************************************************************************************************************/
/*! exports provided: OrderHistorySummaryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderHistorySummaryRoutingModule", function() { return OrderHistorySummaryRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _orderhistorysummary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orderhistorysummary.component */ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _orderhistorysummary_component__WEBPACK_IMPORTED_MODULE_2__["OrderHistorySummaryComponent"],
    },
];
var OrderHistorySummaryRoutingModule = /** @class */ (function () {
    function OrderHistorySummaryRoutingModule() {
    }
    OrderHistorySummaryRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OrderHistorySummaryRoutingModule);
    return OrderHistorySummaryRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component.scss":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component.scss ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".summary-report .dark-header {\n  background: #34495e;\n  font-weight: bold;\n  color: white;\n}\n.summary-report .row-per-card {\n  margin-bottom: 20px;\n}\n.summary-report table {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n  text-align: center;\n}\n.summary-report table th {\n  font-size: 12px;\n  font-weight: normal;\n}\n.summary-report table tr:nth-child(odd) td {\n  background-color: #f0f0f0;\n}\n.summary-report table td, .summary-report table th {\n  border: 1px solid #ccc;\n  padding: 4px 5px;\n}\n.summary-report table td {\n  font-size: 12px;\n}\n@media only screen and (device-width: 768px) {\n  .summary-report {\n    /* default iPad screens */\n  }\n  .summary-report .top-20 {\n    width: 400px;\n    margin-left: -22.5vw;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tcG9ydGFsL2FkbWluLW9yZGVyLWhpc3Rvcnkvb3JkZXItaGlzdG9yeS1zdW1tYXJ5L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcYWRtaW4tcG9ydGFsXFxhZG1pbi1vcmRlci1oaXN0b3J5XFxvcmRlci1oaXN0b3J5LXN1bW1hcnlcXG9yZGVyaGlzdG9yeXN1bW1hcnkuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLXBvcnRhbC9hZG1pbi1vcmRlci1oaXN0b3J5L29yZGVyLWhpc3Rvcnktc3VtbWFyeS9vcmRlcmhpc3RvcnlzdW1tYXJ5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNBUjtBRE1JO0VBQ0ksbUJBQUE7QUNKUjtBRE1JO0VBQ0kseUJBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQ0pSO0FES1E7RUFHSSxlQUFBO0VBQ0EsbUJBQUE7QUNMWjtBRE9RO0VBQ0kseUJBQUE7QUNMWjtBRE9RO0VBQ0ksc0JBQUE7RUFDQSxnQkFBQTtBQ0xaO0FET1E7RUFDSSxlQUFBO0FDTFo7QURVSTtFQXJDSjtJQXNDUSx5QkFBQTtFQ1BOO0VEUU07SUFDSSxZQUFBO0lBQ0Esb0JBQUE7RUNOVjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tcG9ydGFsL2FkbWluLW9yZGVyLWhpc3Rvcnkvb3JkZXItaGlzdG9yeS1zdW1tYXJ5L29yZGVyaGlzdG9yeXN1bW1hcnkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3VtbWFyeS1yZXBvcnR7XHJcbiAgICAuZGFyay1oZWFkZXJ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzM0NDk1ZTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgICAuY2FyZC5zdW17XHJcbiAgICAgICAgLy8gd2lkdGg6IGNhbGMoMTAwJSAvIDMpO1xyXG5cclxuICAgIH1cclxuICAgIC5yb3ctcGVyLWNhcmR7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgIH1cclxuICAgIHRhYmxle1xyXG4gICAgICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgdGh7XHJcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQ6ICMzMzM7XHJcbiAgICAgICAgICAgIC8vIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0cjpudGgtY2hpbGQob2RkKSB0ZHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGQsIHRoe1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA0cHggNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0ZHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAoZGV2aWNlLXdpZHRoOiA3NjhweCkge1xyXG4gICAgICAgIC8qIGRlZmF1bHQgaVBhZCBzY3JlZW5zICovXHJcbiAgICAgICAgLnRvcC0yMCB7XHJcbiAgICAgICAgICAgIHdpZHRoOiA0MDBweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0yMi41dnc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiIsIi5zdW1tYXJ5LXJlcG9ydCAuZGFyay1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjMzQ0OTVlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IHdoaXRlO1xufVxuLnN1bW1hcnktcmVwb3J0IC5yb3ctcGVyLWNhcmQge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuLnN1bW1hcnktcmVwb3J0IHRhYmxlIHtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0aCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0cjpudGgtY2hpbGQob2RkKSB0ZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdGQsIC5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0aCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIHBhZGRpbmc6IDRweCA1cHg7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdGQge1xuICBmb250LXNpemU6IDEycHg7XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChkZXZpY2Utd2lkdGg6IDc2OHB4KSB7XG4gIC5zdW1tYXJ5LXJlcG9ydCB7XG4gICAgLyogZGVmYXVsdCBpUGFkIHNjcmVlbnMgKi9cbiAgfVxuICAuc3VtbWFyeS1yZXBvcnQgLnRvcC0yMCB7XG4gICAgd2lkdGg6IDQwMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAtMjIuNXZ3O1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component.ts":
/*!************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component.ts ***!
  \************************************************************************************************************************/
/*! exports provided: OrderHistorySummaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderHistorySummaryComponent", function() { return OrderHistorySummaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./order-histories-object-setup */ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/order-histories-object-setup.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var OrderHistorySummaryComponent = /** @class */ (function () {
    function OrderHistorySummaryComponent(OrderhistoryService, sanitizer) {
        this.OrderhistoryService = OrderhistoryService;
        this.sanitizer = sanitizer;
        this.data = [];
        this.topBarMenu = [
            { label: "Number of Sales", routerLink: '/administrator/order-history-summary', active: true },
            { label: "Amount of Sales", routerLink: '/administrator/order-amount-history-summary' },
            { label: "Abondonment Rate", routerLink: '/administrator/cart-abandonment-summary' }
        ];
        this.avgData = {
            monthly: {
                all: 0,
                pending: 0,
                paid: 0,
                cancel: 0,
                waiting: 0,
                checkout: 0
            },
            daily: {
                all: 0,
                pending: 0,
                paid: 0,
                cancel: 0,
                waiting: 0,
                checkout: 0
            },
            yearly: {
                all: 0,
                pending: 0,
                paid: 0,
                cancel: 0,
                waiting: 0,
                checkout: 0
            },
            hourly: {
                all: 0,
                pending: 0,
                paid: 0,
                cancel: 0,
                waiting: 0,
                checkout: 0
            }
        };
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
        };
        this.barChartLabels = [];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartData = [
            { data: [], label: 'All Order' },
        ];
        this.analitycsData = _order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["analitycsData"];
    }
    OrderHistorySummaryComponent.prototype.ngOnInit = function () {
        if (this.permissionType) {
            if (this.permissionType == 'merchant') {
                this.topBarMenu = null;
            }
        }
        var currentDate = new Date();
        this.firstLoad();
    };
    OrderHistorySummaryComponent.prototype.ngAfterViewChecked = function () {
        // console.log('hero', this.permissionType)
    };
    OrderHistorySummaryComponent.prototype.onUpdateCart = function (data) {
        var clonedData = JSON.parse(JSON.stringify(data.barChartData));
        var clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = [];
        data.barChartLabels.forEach(function () {
            clone[0].data.push(Math.round(Math.random() * 100));
        });
        data.barChartData = clone;
        setTimeout(function () {
            data.barChartData = clonedData;
        }, 500);
    };
    OrderHistorySummaryComponent.prototype.genChartData = function (dataValues) {
        return Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["generateChartData"])(dataValues);
    };
    OrderHistorySummaryComponent.prototype.graphicDataSetup = function (obj, valGroup) {
    };
    OrderHistorySummaryComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, valGroup, convertedDataMonthlyObject, clone, dataClone, prop, convertedDataDaily, convertedDataDailyPending, convertedDataDailyPaid, convertedDataDailyCancel, convertedDataDailyWaiting, convertedDataDailyCheckout, clone, dataClone, convertedDataHourly, convertedDataHourlyPending, convertedDataHourlyPaid, convertedDataHourlyCancel, convertedDataHourlyCheckout, convertedDataHourlyWaiting, clone, dataClone, convertedDataYearlyObject, convertedDataYearly, convertedDataYearlyPending, convertedDataYearlyPaid, convertedDataYearlyCancel, convertedDataYearlyCheckout, convertedDataYearlyWaiting, clone, dataClone, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.OrderhistoryService;
                        return [4 /*yield*/, this.OrderhistoryService.getOrderHistorySummaryByDate('today')];
                    case 1:
                        result = _a.sent();
                        this.data = result.result;
                        this.data.shopping_cart_abandonment_rate = this.data.current_order_history.total == 0 ? 0 :
                            (1 - (this.data.current_order_history_paid.total / this.data.current_order_history.total)) * 100;
                        if (this.data.order_history_by_month) {
                            valGroup = {
                                all: this.data.order_history_by_month.value,
                                pending: this.data.order_history_by_month_pending.value,
                                paid: this.data.order_history_by_month_paid.value,
                                cancel: this.data.order_history_by_month_cancel.value,
                                waiting: this.data.order_history_by_month_waiting.value,
                                checkout: this.data.order_history_by_month_checkout.value
                            };
                            convertedDataMonthlyObject = {};
                            clone = void 0;
                            dataClone = [];
                            for (prop in valGroup) {
                                convertedDataMonthlyObject[prop] = this.genChartData(valGroup[prop]);
                                this.avgData.monthly[prop] = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(valGroup[prop]));
                                dataClone.push({ data: convertedDataMonthlyObject[prop][1], label: prop, fill: false });
                            }
                            clone = this.dataSetup(this.analitycsData.monthly.barChartData, dataClone);
                            this.barChartLabels = convertedDataMonthlyObject.all[0];
                            this.analitycsData.monthly.barChartLabels = convertedDataMonthlyObject.all[0];
                            this.analitycsData.monthly.barChartData = clone;
                        }
                        if (this.data.order_history_by_the_day) {
                            convertedDataDaily = this.genChartData(this.data.order_history_by_the_day.value);
                            convertedDataDailyPending = this.genChartData(this.data.order_history_by_the_day_pending.value);
                            convertedDataDailyPaid = this.genChartData(this.data.order_history_by_the_day_paid.value);
                            convertedDataDailyCancel = this.genChartData(this.data.order_history_by_the_day_cancel.value);
                            convertedDataDailyWaiting = this.genChartData(this.data.order_history_by_the_day_waiting.value);
                            convertedDataDailyCheckout = this.genChartData(this.data.order_history_by_the_day_checkout.value);
                            this.avgData.daily.all = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_by_the_day.value));
                            this.avgData.daily.pending = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_by_the_day_pending.value));
                            this.avgData.daily.paid = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_by_the_day_paid.value));
                            this.avgData.daily.cancel = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_by_the_day_cancel.value));
                            this.avgData.daily.waiting = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_by_the_day_waiting.value));
                            this.avgData.daily.checkout = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_by_the_day_checkout.value));
                            clone = JSON.parse(JSON.stringify(this.analitycsData.daily.barChartData));
                            clone[0].data = convertedDataDaily[1];
                            dataClone = [
                                clone[0],
                                { data: convertedDataDailyPending[1], label: "Pending", fill: false },
                                { data: convertedDataDailyPaid[1], label: "Paid", fill: false },
                                { data: convertedDataDailyCancel[1], label: "Cancel", fill: false },
                                { data: convertedDataDailyWaiting[1], label: "Waiting", fill: false },
                                { data: convertedDataDailyCheckout[1], label: "Checkout", fill: false },
                            ];
                            clone = this.dataSetup(clone, dataClone);
                            this.analitycsData.daily.barChartLabels = convertedDataDaily[0];
                            this.barChartLabels = convertedDataDaily[0];
                            // this.barChartData   = clone;
                            this.analitycsData.daily.barChartData = clone;
                            // console.log("his.analitycsData", this.analitycsData.daily)
                        }
                        if (this.data.order_history_hourly) {
                            convertedDataHourly = this.genChartData(this.data.order_history_hourly.value);
                            convertedDataHourlyPending = this.genChartData(this.data.order_history_hourly_pending.value);
                            convertedDataHourlyPaid = this.genChartData(this.data.order_history_hourly_paid.value);
                            convertedDataHourlyCancel = this.genChartData(this.data.order_history_hourly_cancel.value);
                            convertedDataHourlyCheckout = this.genChartData(this.data.order_history_hourly_checkout.value);
                            convertedDataHourlyWaiting = this.genChartData(this.data.order_history_hourly_waiting.value);
                            this.avgData.hourly.all = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_hourly.value));
                            this.avgData.hourly.pending = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_hourly_pending.value));
                            this.avgData.hourly.paid = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_hourly_paid.value));
                            this.avgData.hourly.cancel = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_hourly_cancel.value));
                            this.avgData.hourly.waiting = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_hourly_waiting.value));
                            this.avgData.hourly.checkout = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_hourly_checkout.value));
                            clone = this.analitycsData.hourly.barChartData;
                            SETUP_DATA_FOR_HOURLY: {
                                clone[0].data = convertedDataHourly[1];
                                dataClone = [
                                    clone[0],
                                    { data: convertedDataHourlyPending[1], label: "Pending", fill: false },
                                    { data: convertedDataHourlyPaid[1], label: "Paid", fill: false },
                                    { data: convertedDataHourlyCancel[1], label: "Cancel", fill: false },
                                    { data: convertedDataHourlyWaiting[1], label: "Waiting", fill: false },
                                    { data: convertedDataHourlyCheckout[1], label: "Checkout", fill: false },
                                ];
                                clone = this.dataSetup(clone, dataClone);
                            }
                            this.analitycsData.hourly.barChartLabels = convertedDataHourly[0];
                            this.barChartLabels = convertedDataHourly[0];
                            // this.barChartData   = clone;
                            this.analitycsData.hourly.barChartData = clone;
                        }
                        if (this.data.order_history_yearly) {
                            convertedDataYearlyObject = {
                                yearly: this.genChartData(this.data.order_history_yearly.value),
                                pending: this.genChartData(this.data.order_history_yearly_pending.value),
                                paid: this.genChartData(this.data.order_history_yearly_paid.value),
                                cancel: this.genChartData(this.data.order_history_yearly_cancel.value),
                                checkout: this.genChartData(this.data.order_history_yearly_checkout.value),
                                waiting: this.genChartData(this.data.order_history_yearly_waiting.value),
                            };
                            convertedDataYearly = convertedDataYearlyObject.yearly;
                            convertedDataYearlyPending = convertedDataYearlyObject.pending;
                            convertedDataYearlyPaid = convertedDataYearlyObject.paid;
                            convertedDataYearlyCancel = convertedDataYearlyObject.cancel;
                            convertedDataYearlyCheckout = convertedDataYearlyObject.checkout;
                            convertedDataYearlyWaiting = convertedDataYearlyObject.waiting;
                            this.avgData.yearly.all = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_yearly.value));
                            this.avgData.yearly.pending = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_yearly_pending.value));
                            this.avgData.yearly.paid = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_yearly_paid.value));
                            this.avgData.yearly.cancel = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_yearly_cancel.value));
                            this.avgData.yearly.waiting = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_yearly_waiting.value));
                            this.avgData.yearly.checkout = Math.floor(Object(_order_histories_object_setup__WEBPACK_IMPORTED_MODULE_4__["calculateTheAverageValue"])(this.data.order_history_yearly_checkout.value));
                            clone = this.analitycsData.yearly.barChartData;
                            SETUP_DATA_FOR_HOURLY: {
                                clone[0].data = convertedDataYearly[1];
                                dataClone = [
                                    clone[0],
                                    { data: convertedDataYearlyPending[1], label: "Pending", fill: false },
                                    { data: convertedDataYearlyPaid[1], label: "Paid", fill: false },
                                    { data: convertedDataYearlyCancel[1], label: "Cancel", fill: false },
                                    { data: convertedDataYearlyWaiting[1], label: "Waiting", fill: false },
                                    { data: convertedDataYearlyCheckout[1], label: "Checkout", fill: false },
                                ];
                                clone = this.dataSetup(clone, dataClone);
                            }
                            this.analitycsData.yearly.barChartLabels = convertedDataYearly[0];
                            this.barChartLabels = convertedDataYearly[0];
                            // this.barChartData   = clone;
                            this.analitycsData.yearly.barChartData = clone;
                            // console.log("his.analitycsData yearly", this.analitycsData.yearly)
                            TOP_MEMBERS_SETUP: {
                                this.topMembers = this.data.top_users.value;
                            }
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log("this e result", e_1);
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderHistorySummaryComponent.prototype.dataSetup = function (clone, dataClone) {
        dataClone.forEach(function (e, i) {
            if (clone[i]) {
                clone[i] = e;
            }
            else {
                clone.push(e);
            }
        });
        return clone;
    };
    OrderHistorySummaryComponent.prototype.getCRR = function (crr) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.service = this.OrderhistoryService;
                        return [4 /*yield*/, this.OrderhistoryService.getCRR(crr)];
                    case 1:
                        result = _a.sent();
                        this.data = result.values;
                        return [2 /*return*/];
                }
            });
        });
    };
    OrderHistorySummaryComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderHistorySummaryComponent.prototype, "permissionType", void 0);
    OrderHistorySummaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-history-summary',
            template: __webpack_require__(/*! raw-loader!./orderhistorysummary.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./orderhistorysummary.component.scss */ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
    ], OrderHistorySummaryComponent);
    return OrderHistorySummaryComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.module.ts":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.module.ts ***!
  \*********************************************************************************************************************/
/*! exports provided: OrderHistorySummaryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderHistorySummaryModule", function() { return OrderHistorySummaryModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _orderhistorysummary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orderhistorysummary.component */ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _orderhistorysummary_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./orderhistorysummary-routing.module */ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_order_histories_per_day_order_histories_per_day_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/order-histories-per-day/order-histories-per-day.component */ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-per-day/order-histories-per-day.component.ts");
/* harmony import */ var _components_order_histories_total_today_order_order_histories_total_today_order_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/order-histories-total-today-order/order-histories-total-today-order.component */ "./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/components/order-histories-total-today-order/order-histories-total-today-order.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var OrderHistorySummaryModule = /** @class */ (function () {
    function OrderHistorySummaryModule() {
    }
    OrderHistorySummaryModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _orderhistorysummary_routing_module__WEBPACK_IMPORTED_MODULE_7__["OrderHistorySummaryRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_9__["ChartsModule"]
            ],
            declarations: [
                _orderhistorysummary_component__WEBPACK_IMPORTED_MODULE_2__["OrderHistorySummaryComponent"],
                _components_order_histories_per_day_order_histories_per_day_component__WEBPACK_IMPORTED_MODULE_10__["OrderHistoriesPerDayComponent"],
                _components_order_histories_total_today_order_order_histories_total_today_order_component__WEBPACK_IMPORTED_MODULE_11__["OrderHistoriesTotalTodayOrderComponent"]
            ],
            exports: [
                _orderhistorysummary_component__WEBPACK_IMPORTED_MODULE_2__["OrderHistorySummaryComponent"],
                _components_order_histories_per_day_order_histories_per_day_component__WEBPACK_IMPORTED_MODULE_10__["OrderHistoriesPerDayComponent"],
                _components_order_histories_total_today_order_order_histories_total_today_order_component__WEBPACK_IMPORTED_MODULE_11__["OrderHistoriesTotalTodayOrderComponent"]
            ]
        })
    ], OrderHistorySummaryModule);
    return OrderHistorySummaryModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~merchant-portal-merchant-portal-module~modules-admin-portal-admin-order-history-order-histor~8feb55e7.js.map