(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-merchant-login-merchant-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/login-merchant/login-merchant.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login-merchant/login-merchant.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\r\n<div class=\"title-custom\">\r\n    <img src=\"/assets/images/locard_icon_circle.png\" style=\"height: 60px;\" alt=\"Locard Icon\">\r\n    <label><strong>LOCARD\r\n        </strong></label>\r\n</div>\r\n\r\n<div class=\"register-page-bg\">\r\n    <img src=\"/assets/images/page-regis3.png\">\r\n</div>\r\n\r\n<div class=\"register-page-container\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-4 col-sm-12 register-form-container\">\r\n            <h1 class=\"form-title\"><strong>Login</strong></h1>\r\n            <span class=\"form-desc\">Please login to continue and feel more lucky</span>\r\n            <div class=\"form-container\">\r\n            <div class=\"register-form\" (keyup.enter)=\"autoLogin()\">\r\n          \r\n            <mat-list>\r\n                <mat-list-item>\r\n                    <mat-icon style=\"color: grey;\" mat-list-icon> mail_outline</mat-icon>\r\n                    <mat-form-field mat-line>\r\n                        <input matInput placeholder=\"Email\" (click)=\"errorMessage = false\" [formControl]=\"email\" required [(ngModel)]=\"form.email\"\r\n                            name=\"email\">\r\n\r\n                        <mat-error *ngIf=\"email.invalid\">{{getErrorMessage()}}</mat-error>\r\n                    </mat-form-field>\r\n                </mat-list-item>\r\n                <mat-list-item>\r\n                    <mat-icon style=\"color: grey;\" mat-list-icon>lock_open</mat-icon>\r\n                    <mat-form-field mat-line>\r\n                        <input matInput placeholder=\"Password\" (click)=\"errorMessage = false\" [type]=\"hide ? 'password' : 'text'\"\r\n                            [(ngModel)]=\"form.password\" name=\"password\">\r\n                        <button mat-icon-button matSuffix (click)=\"hide = !hide\" [attr.aria-label]=\"'Hide password'\"\r\n                            [attr.aria-pressed]=\"hide\" style=\"background:transparent; border: none;\">\r\n                            <mat-icon >{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                            \r\n                        </button>\r\n                    </mat-form-field>\r\n                </mat-list-item>\r\n            </mat-list>\r\n            <div class=\"notif error-message\" *ngIf=\"errorMessage\">\r\n                *{{errorMessage}}\r\n            </div>\r\n            \r\n            <div class=\"forgot-password\">\r\n                <label (click)=\"forgotPasswordClicked()\"><strong>Forgot\r\n                        Password ?</strong></label>\r\n            </div>\r\n            <button type=\"submit\" class=\"common-button blue-fill full\"\r\n            (click)=\"autoLogin()\"><strong>Login</strong></button>\r\n            \r\n            <div class=\"loading\" *ngIf='showLoading'>\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            \r\n            \r\n            <label class=\"register-label\"\r\n                (click)=registerClicked()>Don't Have account ?<strong style=\"color: blue; cursor:pointer;\"> Register</strong></label>\r\n            \r\n            </div>\r\n        </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/login-merchant/login-merchant-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/login-merchant/login-merchant-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: LoginMerchantRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginMerchantRoutingModule", function() { return LoginMerchantRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_merchant_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login-merchant.component */ "./src/app/login-merchant/login-merchant.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _login_merchant_component__WEBPACK_IMPORTED_MODULE_2__["LoginMerchantComponent"]
    }
];
var LoginMerchantRoutingModule = /** @class */ (function () {
    function LoginMerchantRoutingModule() {
    }
    LoginMerchantRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], LoginMerchantRoutingModule);
    return LoginMerchantRoutingModule;
}());



/***/ }),

/***/ "./src/app/login-merchant/login-merchant.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/login-merchant/login-merchant.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  font-family: \"Poppins\", sans-serif;\n}\n\n.full {\n  margin-top: 15px;\n  width: 100%;\n}\n\n.full:hover {\n  background-color: #0069d9;\n  cursor: pointer;\n}\n\n.full:active {\n  box-shadow: 0 2px #666;\n  -webkit-transform: translateY(2px);\n          transform: translateY(2px);\n}\n\n.locard-icon {\n  position: absolute;\n  background-image: url(\"/assets/images/locard_icon_circle.png\");\n  background-repeat: no-repeat;\n  background-position: center center;\n  background-size: cover;\n  height: auto;\n  width: auto;\n  z-index: -1;\n}\n\n::ng-deep .mat-list-base .mat-list-item .mat-list-item-content, .mat-list-base .mat-list-option .mat-list-item-content {\n  padding: 0px !important;\n}\n\n::ng-deep mat-list-item {\n  margin-bottom: 2vh;\n}\n\n::ng-deep mat-list-item {\n  color: grey;\n}\n\n.register-page-bg {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  z-index: -1;\n}\n\n.register-page-bg img {\n  width: 60%;\n}\n\n.title-custom {\n  padding: 20px 40px 15px 40px;\n}\n\n.title-custom label {\n  color: #096aa0;\n  font-size: 35px;\n  margin-left: 10px;\n  margin-bottom: 0px;\n  vertical-align: middle;\n}\n\n.register-page-container {\n  color: white;\n  padding-top: 0vw;\n  padding-left: 6vw;\n  padding-right: 5vw;\n  max-width: 100%;\n  overflow: hidden;\n}\n\n.text {\n  padding-right: 2vw;\n}\n\n.register-form-container {\n  position: absolute;\n  background-color: white;\n  border-radius: 5px;\n  color: black;\n  right: 0px;\n  padding-top: 1em;\n  padding-bottom: 1em;\n  padding: 1em 3em;\n}\n\n.register-form-container .form-title {\n  color: black;\n}\n\n.register-form-container .form-container {\n  margin-top: 20px;\n  padding: 7% 7% 6%;\n  box-shadow: 0px 2px 5px 3px rgba(0, 0, 0, 0.1);\n}\n\n.register-form-container .forgot-password {\n  font-size: 16px;\n  margin-top: 10px;\n  right: 0px;\n  text-align: right;\n  margin-bottom: -5px;\n}\n\n.register-form-container .forgot-password label {\n  color: blue;\n}\n\n.register-form-container .register-label {\n  margin-top: 3vh;\n  color: grey;\n  font-size: 13px;\n}\n\n.register-form {\n  padding: 0;\n  background-color: white;\n}\n\n.register-form label {\n  color: #c8c8c8;\n  font-size: 0.8em;\n}\n\n.register-form .half {\n  display: flex;\n}\n\n.register-form .half .half-space {\n  width: 2%;\n}\n\n.register-form .half .half-component {\n  width: 49%;\n}\n\n.register-form .register-btn {\n  margin-top: 10px;\n  width: 100%;\n  height: 52px;\n  background-color: #0957a2;\n  border-radius: 80px;\n}\n\n.register-form .login-btn {\n  width: 100%;\n  height: 52px;\n  color: #0957a2;\n  background-color: white;\n  border-radius: 80px;\n  border: 2px solid #0957a2;\n  box-shadow: 0 9px #999;\n}\n\n.register-form .privacy-policy-text {\n  font-size: 0.6em;\n  text-align: center;\n  padding-left: 2px;\n  padding-right: 2px;\n}\n\n.or-line {\n  display: flex;\n  align-items: center;\n}\n\n.or-line hr {\n  width: 50%;\n}\n\n.or-line label {\n  margin: 0px 10px;\n}\n\nfooter {\n  position: absolute;\n  bottom: 0;\n  right: 0px;\n  width: 50vw;\n  color: white;\n  font-size: 12px;\n  text-align: center;\n}\n\n@media screen and (max-width: 600px) {\n  .title-custom {\n    padding: 30px 40px 15px 35px;\n  }\n  .title-custom label {\n    color: white;\n    font-size: 20px;\n    margin-left: 10px;\n    margin-bottom: 0px;\n    vertical-align: middle;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4tbWVyY2hhbnQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbG9naW4tbWVyY2hhbnRcXGxvZ2luLW1lcmNoYW50LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sb2dpbi1tZXJjaGFudC9sb2dpbi1tZXJjaGFudC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtDQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxnQkFBQTtFQUNBLFdBQUE7QUNFRjs7QURBQTtFQUNFLHlCQUFBO0VBQ0EsZUFBQTtBQ0dGOztBRERBO0VBQ0Usc0JBQUE7RUFDRixrQ0FBQTtVQUFBLDBCQUFBO0FDSUE7O0FERkE7RUFDRSxrQkFBQTtFQUNBLDhEQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0FDS0Y7O0FESEE7RUFDRSx1QkFBQTtBQ01GOztBREpBO0VBQ0Usa0JBQUE7QUNPRjs7QURMQTtFQUNFLFdBQUE7QUNRRjs7QUROQTtFQUNFLGtCQUFBO0VBTUEsWUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0FDSUY7O0FESEU7RUFDRSxVQUFBO0FDS0o7O0FEREE7RUFDRSw0QkFBQTtBQ0lGOztBREZFO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUNJSjs7QURBQTtFQUNFLFlBQUE7RUFFQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNFRjs7QURDQTtFQUNFLGtCQUFBO0FDRUY7O0FEQ0E7RUFDRSxrQkFBQTtFQUNFLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ0VKOztBREFFO0VBQ0UsWUFBQTtBQ0VKOztBREFFO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDhDQUFBO0FDRUo7O0FEQUU7RUFFRSxlQUFBO0VBQWdCLGdCQUFBO0VBQ2hCLFVBQUE7RUFDQSxpQkFBQTtFQUFtQixtQkFBQTtBQ0d2Qjs7QURGSTtFQUNFLFdBQUE7QUNJTjs7QURERTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQ0dKOztBREdBO0VBQ0UsVUFBQTtFQUNBLHVCQUFBO0FDQUY7O0FERUU7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QUNBSjs7QURHRTtFQUNFLGFBQUE7QUNESjs7QURHSTtFQUNFLFNBQUE7QUNETjs7QURJSTtFQUNFLFVBQUE7QUNGTjs7QURNRTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FDSko7O0FET0U7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxzQkFBQTtBQ0xKOztBRFNFO0VBQ0UsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNQSjs7QURXQTtFQUNFLGFBQUE7RUFFQSxtQkFBQTtBQ1RGOztBRFVFO0VBQ0UsVUFBQTtBQ1JKOztBRFVFO0VBQ0UsZ0JBQUE7QUNSSjs7QURZQTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ1RGOztBRFlBO0VBQ0U7SUFDRSw0QkFBQTtFQ1RGO0VEV0U7SUFDRSxZQUFBO0lBQ0EsZUFBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7SUFDQSxzQkFBQTtFQ1RKO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi1tZXJjaGFudC9sb2dpbi1tZXJjaGFudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIioge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIiwgc2Fucy1zZXJpZjtcclxufVxyXG4uZnVsbCB7XHJcbiAgbWFyZ2luLXRvcDogMTVweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4uZnVsbDpob3ZlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwNjlkOTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuLmZ1bGw6YWN0aXZlIHtcclxuICBib3gtc2hhZG93OiAwIDJweCAjNjY2O1xyXG50cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMnB4KTtcclxufVxyXG4ubG9jYXJkLWljb24ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9sb2NhcmRfaWNvbl9jaXJjbGUucG5nXCIpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIGhlaWdodDogYXV0bztcclxuICB3aWR0aDogYXV0bztcclxuICB6LWluZGV4OiAtMTtcclxufVxyXG46Om5nLWRlZXAgLm1hdC1saXN0LWJhc2UgLm1hdC1saXN0LWl0ZW0gLm1hdC1saXN0LWl0ZW0tY29udGVudCwgLm1hdC1saXN0LWJhc2UgLm1hdC1saXN0LW9wdGlvbiAubWF0LWxpc3QtaXRlbS1jb250ZW50e1xyXG4gIHBhZGRpbmcgOiAwcHggIWltcG9ydGFudDtcclxufVxyXG46Om5nLWRlZXAgbWF0LWxpc3QtaXRlbSB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMnZoO1xyXG59XHJcbjo6bmctZGVlcCBtYXQtbGlzdC1pdGVtIHtcclxuICBjb2xvciA6IGdyZXk7XHJcbn1cclxuLnJlZ2lzdGVyLXBhZ2UtYmcge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBcclxuICAvLyBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIC8vIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XHJcbiAgLy8gYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgei1pbmRleDogLTE7XHJcbiAgaW1nIHtcclxuICAgIHdpZHRoOiA2MCU7XHJcbiAgfVxyXG59XHJcblxyXG4udGl0bGUtY3VzdG9tIHtcclxuICBwYWRkaW5nOiAyMHB4IDQwcHggMTVweCA0MHB4O1xyXG5cclxuICBsYWJlbCB7XHJcbiAgICBjb2xvcjogIzA5NmFhMDtcclxuICAgIGZvbnQtc2l6ZTogMzVweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICB9XHJcbn1cclxuXHJcbi5yZWdpc3Rlci1wYWdlLWNvbnRhaW5lciB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIC8vIG1hcmdpbi10b3A6IDEzMHB4O1xyXG4gIHBhZGRpbmctdG9wOiAwdnc7XHJcbiAgcGFkZGluZy1sZWZ0OiA2dnc7XHJcbiAgcGFkZGluZy1yaWdodDogNXZ3O1xyXG4gIG1heC13aWR0aDogMTAwJTtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4udGV4dCB7XHJcbiAgcGFkZGluZy1yaWdodDogMnZ3O1xyXG59XHJcblxyXG4ucmVnaXN0ZXItZm9ybS1jb250YWluZXIge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgcmlnaHQ6IDBweDtcclxuICAgIHBhZGRpbmctdG9wOiAxZW07XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMWVtO1xyXG4gICAgcGFkZGluZzogMWVtIDNlbTtcclxuXHJcbiAgLmZvcm0tdGl0bGUge1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gIH1cclxuICAuZm9ybS1jb250YWluZXIge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIHBhZGRpbmc6IDclIDclIDYlO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICB9XHJcbiAgLmZvcmdvdC1wYXNzd29yZCB7XHJcbiAgICBcclxuICAgIGZvbnQtc2l6ZTogMTZweDttYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgcmlnaHQ6IDBweDs7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDsgbWFyZ2luLWJvdHRvbTogLTVweDtcclxuICAgIGxhYmVsIHtcclxuICAgICAgY29sb3I6IGJsdWU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5yZWdpc3Rlci1sYWJlbCB7XHJcbiAgICBtYXJnaW4tdG9wOiAzdmg7XHJcbiAgICBjb2xvcjpncmV5O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gIH1cclxufVxyXG5cclxuXHJcblxyXG4ucmVnaXN0ZXItZm9ybSB7XHJcbiAgcGFkZGluZzogMDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuXHJcbiAgbGFiZWwge1xyXG4gICAgY29sb3I6ICNjOGM4Yzg7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gIH1cclxuXHJcbiAgLmhhbGYge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuXHJcbiAgICAuaGFsZi1zcGFjZSB7XHJcbiAgICAgIHdpZHRoOiAyJTtcclxuICAgIH1cclxuXHJcbiAgICAuaGFsZi1jb21wb25lbnQge1xyXG4gICAgICB3aWR0aDogNDklO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnJlZ2lzdGVyLWJ0biB7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDUycHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDk1N2EyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogODBweDtcclxuICB9XHJcblxyXG4gIC5sb2dpbi1idG4ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDUycHg7XHJcbiAgICBjb2xvcjogIzA5NTdhMjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogODBweDtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkICMwOTU3YTI7XHJcbiAgICBib3gtc2hhZG93OiAwIDlweCAjOTk5O1xyXG4gIH1cclxuIFxyXG5cclxuICAucHJpdmFjeS1wb2xpY3ktdGV4dCB7XHJcbiAgICBmb250LXNpemU6IDAuNmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAycHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAycHg7XHJcbiAgfVxyXG59XHJcblxyXG4ub3ItbGluZSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICAvLyBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgaHIge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcbiAgbGFiZWwge1xyXG4gICAgbWFyZ2luOiAwcHggMTBweDtcclxuICB9XHJcbn1cclxuXHJcbmZvb3RlciB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvdHRvbTogMDtcclxuICByaWdodDogMHB4O1xyXG4gIHdpZHRoOiA1MHZ3O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gIC50aXRsZS1jdXN0b20ge1xyXG4gICAgcGFkZGluZzogMzBweCA0MHB4IDE1cHggMzVweDtcclxuXHJcbiAgICBsYWJlbCB7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCIqIHtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiLCBzYW5zLXNlcmlmO1xufVxuXG4uZnVsbCB7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZnVsbDpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDY5ZDk7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmZ1bGw6YWN0aXZlIHtcbiAgYm94LXNoYWRvdzogMCAycHggIzY2NjtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDJweCk7XG59XG5cbi5sb2NhcmQtaWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvbG9jYXJkX2ljb25fY2lyY2xlLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogYXV0bztcbiAgei1pbmRleDogLTE7XG59XG5cbjo6bmctZGVlcCAubWF0LWxpc3QtYmFzZSAubWF0LWxpc3QtaXRlbSAubWF0LWxpc3QtaXRlbS1jb250ZW50LCAubWF0LWxpc3QtYmFzZSAubWF0LWxpc3Qtb3B0aW9uIC5tYXQtbGlzdC1pdGVtLWNvbnRlbnQge1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuOjpuZy1kZWVwIG1hdC1saXN0LWl0ZW0ge1xuICBtYXJnaW4tYm90dG9tOiAydmg7XG59XG5cbjo6bmctZGVlcCBtYXQtbGlzdC1pdGVtIHtcbiAgY29sb3I6IGdyZXk7XG59XG5cbi5yZWdpc3Rlci1wYWdlLWJnIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAtMTtcbn1cbi5yZWdpc3Rlci1wYWdlLWJnIGltZyB7XG4gIHdpZHRoOiA2MCU7XG59XG5cbi50aXRsZS1jdXN0b20ge1xuICBwYWRkaW5nOiAyMHB4IDQwcHggMTVweCA0MHB4O1xufVxuLnRpdGxlLWN1c3RvbSBsYWJlbCB7XG4gIGNvbG9yOiAjMDk2YWEwO1xuICBmb250LXNpemU6IDM1cHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbi5yZWdpc3Rlci1wYWdlLWNvbnRhaW5lciB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZy10b3A6IDB2dztcbiAgcGFkZGluZy1sZWZ0OiA2dnc7XG4gIHBhZGRpbmctcmlnaHQ6IDV2dztcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4udGV4dCB7XG4gIHBhZGRpbmctcmlnaHQ6IDJ2dztcbn1cblxuLnJlZ2lzdGVyLWZvcm0tY29udGFpbmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIHJpZ2h0OiAwcHg7XG4gIHBhZGRpbmctdG9wOiAxZW07XG4gIHBhZGRpbmctYm90dG9tOiAxZW07XG4gIHBhZGRpbmc6IDFlbSAzZW07XG59XG4ucmVnaXN0ZXItZm9ybS1jb250YWluZXIgLmZvcm0tdGl0bGUge1xuICBjb2xvcjogYmxhY2s7XG59XG4ucmVnaXN0ZXItZm9ybS1jb250YWluZXIgLmZvcm0tY29udGFpbmVyIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgcGFkZGluZzogNyUgNyUgNiU7XG4gIGJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG59XG4ucmVnaXN0ZXItZm9ybS1jb250YWluZXIgLmZvcmdvdC1wYXNzd29yZCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcmlnaHQ6IDBweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIG1hcmdpbi1ib3R0b206IC01cHg7XG59XG4ucmVnaXN0ZXItZm9ybS1jb250YWluZXIgLmZvcmdvdC1wYXNzd29yZCBsYWJlbCB7XG4gIGNvbG9yOiBibHVlO1xufVxuLnJlZ2lzdGVyLWZvcm0tY29udGFpbmVyIC5yZWdpc3Rlci1sYWJlbCB7XG4gIG1hcmdpbi10b3A6IDN2aDtcbiAgY29sb3I6IGdyZXk7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuLnJlZ2lzdGVyLWZvcm0ge1xuICBwYWRkaW5nOiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cbi5yZWdpc3Rlci1mb3JtIGxhYmVsIHtcbiAgY29sb3I6ICNjOGM4Yzg7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG59XG4ucmVnaXN0ZXItZm9ybSAuaGFsZiB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4ucmVnaXN0ZXItZm9ybSAuaGFsZiAuaGFsZi1zcGFjZSB7XG4gIHdpZHRoOiAyJTtcbn1cbi5yZWdpc3Rlci1mb3JtIC5oYWxmIC5oYWxmLWNvbXBvbmVudCB7XG4gIHdpZHRoOiA0OSU7XG59XG4ucmVnaXN0ZXItZm9ybSAucmVnaXN0ZXItYnRuIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogNTJweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA5NTdhMjtcbiAgYm9yZGVyLXJhZGl1czogODBweDtcbn1cbi5yZWdpc3Rlci1mb3JtIC5sb2dpbi1idG4ge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1MnB4O1xuICBjb2xvcjogIzA5NTdhMjtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDgwcHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMwOTU3YTI7XG4gIGJveC1zaGFkb3c6IDAgOXB4ICM5OTk7XG59XG4ucmVnaXN0ZXItZm9ybSAucHJpdmFjeS1wb2xpY3ktdGV4dCB7XG4gIGZvbnQtc2l6ZTogMC42ZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAycHg7XG4gIHBhZGRpbmctcmlnaHQ6IDJweDtcbn1cblxuLm9yLWxpbmUge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm9yLWxpbmUgaHIge1xuICB3aWR0aDogNTAlO1xufVxuLm9yLWxpbmUgbGFiZWwge1xuICBtYXJnaW46IDBweCAxMHB4O1xufVxuXG5mb290ZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDBweDtcbiAgd2lkdGg6IDUwdnc7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC50aXRsZS1jdXN0b20ge1xuICAgIHBhZGRpbmc6IDMwcHggNDBweCAxNXB4IDM1cHg7XG4gIH1cbiAgLnRpdGxlLWN1c3RvbSBsYWJlbCB7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/login-merchant/login-merchant.component.ts":
/*!************************************************************!*\
  !*** ./src/app/login-merchant/login-merchant.component.ts ***!
  \************************************************************/
/*! exports provided: LoginMerchantComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginMerchantComponent", function() { return LoginMerchantComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_login_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/login/login.service */ "./src/app/services/login/login.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var LoginMerchantComponent = /** @class */ (function () {
    function LoginMerchantComponent(loginService, router, route) {
        this.loginService = loginService;
        this.router = router;
        this.route = route;
        this.errorLabel = false;
        this.showLoading = false;
        this.email = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]);
        this.password = "";
        this.hide = true;
        this.checked = false;
        this.form = {
            email: "",
            password: "",
        };
    }
    LoginMerchantComponent.prototype.getErrorMessage = function () {
        return this.email.hasError('required') ? 'You must enter a value' :
            this.email.hasError('email') ? 'Not a valid email' :
                '';
    };
    LoginMerchantComponent.prototype.ngOnInit = function () {
    };
    LoginMerchantComponent.prototype.registerClicked = function () {
        this.router.navigate(['/register']);
    };
    LoginMerchantComponent.prototype.forgotPasswordClicked = function () {
        this.router.navigate(['/reset-password']);
    };
    LoginMerchantComponent.prototype.autoLogin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var login_data, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.showLoading = true;
                        console.log(this.password);
                        console.log(this.email);
                        login_data = {
                            "email": this.form.email,
                            "password": this.form.password
                        };
                        console.log(login_data);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.loginService.memberLogin(login_data)];
                    case 2:
                        result = _a.sent();
                        if (!result.error) {
                            console.log(result);
                            localStorage.setItem('isLoggedin', 'true');
                            localStorage.setItem('tokenlogin', result.result.token);
                            if (result.result.permission == 'merchant') {
                                this.router.navigateByUrl('/merchant-portal/homepage');
                            }
                            else {
                                this.router.navigateByUrl('/register/open-merchant');
                            }
                        }
                        else {
                            this.showLoading = false;
                            this.errorMessage = result.error;
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.showLoading = false;
                        this.errorMessage = (e_1.message);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LoginMerchantComponent.ctorParameters = function () { return [
        { type: _services_login_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] }
    ]; };
    LoginMerchantComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login-merchant',
            template: __webpack_require__(/*! raw-loader!./login-merchant.component.html */ "./node_modules/raw-loader/index.js!./src/app/login-merchant/login-merchant.component.html"),
            styles: [__webpack_require__(/*! ./login-merchant.component.scss */ "./src/app/login-merchant/login-merchant.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_login_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], LoginMerchantComponent);
    return LoginMerchantComponent;
}());



/***/ }),

/***/ "./src/app/login-merchant/login-merchant.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/login-merchant/login-merchant.module.ts ***!
  \*********************************************************/
/*! exports provided: LoginMerchantModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginMerchantModule", function() { return LoginMerchantModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _login_merchant_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login-merchant-routing.module */ "./src/app/login-merchant/login-merchant-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _layout_modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../layout/modules/bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _login_merchant_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login-merchant.component */ "./src/app/login-merchant/login-merchant.component.ts");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var LoginMerchantModule = /** @class */ (function () {
    function LoginMerchantModule() {
    }
    LoginMerchantModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_login_merchant_component__WEBPACK_IMPORTED_MODULE_7__["LoginMerchantComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _login_merchant_routing_module__WEBPACK_IMPORTED_MODULE_2__["LoginMerchantRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_5__["FormBuilderTableModule"],
                _layout_modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_6__["BsComponentModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__["MatFormFieldModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_9__["MatInputModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_10__["MatIconModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_11__["MatListModule"],
            ]
        })
    ], LoginMerchantModule);
    return LoginMerchantModule;
}());



/***/ })

}]);
//# sourceMappingURL=login-merchant-login-merchant-module.js.map