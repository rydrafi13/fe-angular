(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-frontpage-frontpage-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/frontpage/frontpage.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/frontpage/frontpage.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  frontpage works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/layout/modules/frontpage/frontpage.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/modules/frontpage/frontpage.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2Zyb250cGFnZS9mcm9udHBhZ2UuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/modules/frontpage/frontpage.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/modules/frontpage/frontpage.component.ts ***!
  \*****************************************************************/
/*! exports provided: FrontpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrontpageComponent", function() { return FrontpageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FrontpageComponent = /** @class */ (function () {
    function FrontpageComponent() {
    }
    FrontpageComponent.prototype.ngOnInit = function () {
    };
    FrontpageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-frontpage',
            template: __webpack_require__(/*! raw-loader!./frontpage.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/frontpage/frontpage.component.html"),
            styles: [__webpack_require__(/*! ./frontpage.component.scss */ "./src/app/layout/modules/frontpage/frontpage.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FrontpageComponent);
    return FrontpageComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/frontpage/frontpage.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/modules/frontpage/frontpage.module.ts ***!
  \**************************************************************/
/*! exports provided: FrontpageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrontpageModule", function() { return FrontpageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _frontpage_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./frontpage.component */ "./src/app/layout/modules/frontpage/frontpage.component.ts");
/* harmony import */ var _frontpage_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./frontpage.routing */ "./src/app/layout/modules/frontpage/frontpage.routing.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var FrontpageModule = /** @class */ (function () {
    function FrontpageModule() {
    }
    FrontpageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_frontpage_component__WEBPACK_IMPORTED_MODULE_2__["FrontpageComponent"]],
            // imports: [CommonModule, FormRoutingModule, PageHeaderModule],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _frontpage_routing__WEBPACK_IMPORTED_MODULE_3__["FrontpageRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"]
            ]
        })
    ], FrontpageModule);
    return FrontpageModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/frontpage/frontpage.routing.ts":
/*!***************************************************************!*\
  !*** ./src/app/layout/modules/frontpage/frontpage.routing.ts ***!
  \***************************************************************/
/*! exports provided: FrontpageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrontpageRoutingModule", function() { return FrontpageRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _frontpage_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./frontpage.component */ "./src/app/layout/modules/frontpage/frontpage.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _frontpage_component__WEBPACK_IMPORTED_MODULE_2__["FrontpageComponent"]
    }
];
var FrontpageRoutingModule = /** @class */ (function () {
    function FrontpageRoutingModule() {
    }
    FrontpageRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], FrontpageRoutingModule);
    return FrontpageRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-frontpage-frontpage-module.js.map