(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin_members-member-summary-membersummary-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin_members/member-summary/membersummary.component.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin_members/member-summary/membersummary.component.html ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  <div class=\"summary-report row\">\r\n    <div class=\"col-md-9\">\r\n        <div class=\"card \" *ngIf=\"barChartLabels.length\">\r\n            <div class=\"card-header\">\r\n               Registered Member Daily\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <canvas baseChart [datasets]=\"barChartData\" [labels]=\"barChartLabels\" \r\n                [options]=\"barChartOptions\" \r\n                [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\" (chartClick)=\"chartClicked($event)\">\r\n                </canvas>\r\n            </div>\r\n            <div class=\"card-footer\">\r\n                <button class=\"btn btn-info btn-sm\" (click)=\"onOnUpdateCart()\">Refresh</button>\r\n            </div>\r\n        </div>\r\n        <br>\r\n        <br>\r\n\r\n        <div class=\"card \" *ngIf=\"barChartLabels.length\">\r\n            <div class=\"card-header\">\r\n                Registered Member Monthly\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <canvas baseChart [datasets]=\"barChartData2\" [labels]=\"barChartLabels\" [options]=\"barChartOptions\"\r\n                    [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                    (chartClick)=\"chartClicked($event)\">\r\n                </canvas>\r\n            </div>\r\n            <div class=\"card-footer\">\r\n                <button class=\"btn btn-info btn-sm\" (click)=\"onOnUpdateCart()\">Refresh</button>\r\n            </div>\r\n        </div>\r\n        <br>\r\n        <div class=\"card \" *ngIf=\"barChartLabels.length\">\r\n            <div class=\"card-header\">\r\n                New Guest Daily\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <canvas baseChart [datasets]=\"barChartData3\" [labels]=\"barChartLabels\" [options]=\"barChartOptions\"\r\n                    [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                    (chartClick)=\"chartClicked($event)\">\r\n                </canvas>\r\n            </div>\r\n            <div class=\"card-footer\">\r\n                <button class=\"btn btn-info btn-sm\" (click)=\"onOnUpdateCart()\">Refresh</button>\r\n            </div>\r\n        </div>\r\n        <br>\r\n        <div class=\"card \" *ngIf=\"barChartLabels.length\">\r\n            <div class=\"card-header\">\r\n                New Guest Monthly\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <canvas baseChart [datasets]=\"barChartData4\" [labels]=\"barChartLabels\" [options]=\"barChartOptions\"\r\n                    [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                    (chartClick)=\"chartClicked($event)\">\r\n                </canvas>\r\n            </div>\r\n            <div class=\"card-footer\">\r\n                <button class=\"btn btn-info btn-sm\" (click)=\"onOnUpdateCart()\">Refresh</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <!-- BAGIAN TABEL -->\r\n\r\n    <div class=\"col-md-3\">\r\n        <div class=\"card sum\" *ngIf=\"data.current_registered_member\">\r\n            <div class=\"card-header\"> Registered Member Total </div>\r\n            <div class=\"card-body\">{{data.current_all_registered_guest.total}}</div>\r\n      </div>\r\n      <br>\r\n      <div class=\"card sum\" *ngIf=\"data.current_registered_member\">\r\n          <div class=\"card-header\"> Registered Member Monthly </div>\r\n          <div class=\"card-body\">{{data.current_all_registered_member.total}}</div>\r\n      </div>\r\n      <br>\r\n      <div class=\"card sum\" *ngIf=\"data.current_registered_member\">\r\n          <div class=\"card-header\"> All Guest Daily </div>\r\n          <div class=\"card-body\">{{data.current_new_guest.total}}</div>\r\n      </div>\r\n      <br>\r\n      <div class=\"card sum\" *ngIf=\"data.current_registered_member\">\r\n          <div class=\"card-header\"> New Guest Monthly </div>\r\n          <div class=\"card-body\">{{data.current_new_user.total}}</div>\r\n      </div>\r\n      <br>\r\n      <div class=\"card sum\" *ngIf=\"data.current_registered_member\">\r\n          <div class=\"card-header\"> New Member Daily </div>\r\n          <div class=\"card-body\">{{data.current_registered_member.total}}</div>\r\n      </div>\r\n    \r\n    \r\n  </div>\r\n\r\n  \r\n \r\n \r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/modules/admin_members/member-summary/membersummary-routing.module.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/member-summary/membersummary-routing.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: MemberSummaryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberSummaryRoutingModule", function() { return MemberSummaryRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _membersummary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./membersummary.component */ "./src/app/layout/modules/admin_members/member-summary/membersummary.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _membersummary_component__WEBPACK_IMPORTED_MODULE_2__["MemberSummaryComponent"],
    }
];
var MemberSummaryRoutingModule = /** @class */ (function () {
    function MemberSummaryRoutingModule() {
    }
    MemberSummaryRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], MemberSummaryRoutingModule);
    return MemberSummaryRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin_members/member-summary/membersummary.component.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/member-summary/membersummary.component.scss ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluX21lbWJlcnMvbWVtYmVyLXN1bW1hcnkvbWVtYmVyc3VtbWFyeS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/modules/admin_members/member-summary/membersummary.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/member-summary/membersummary.component.ts ***!
  \****************************************************************************************/
/*! exports provided: MemberSummaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberSummaryComponent", function() { return MemberSummaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;
var MemberSummaryComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function MemberSummaryComponent(memberService, sanitizer) {
        this.memberService = memberService;
        this.sanitizer = sanitizer;
        this.data = [];
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true
        };
        this.barChartLabels = [];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartData = [
            { data: [], label: 'Registered member' },
        ];
        this.barChartData2 = [
            { data: [], label: 'Guest' }
        ];
        this.barChartData3 = [
            { data: [], label: 'Guest Daily' }
        ];
        this.barChartData4 = [
            { data: [], label: 'Guest Monthly' }
        ];
        this.firstLoad();
        // this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    MemberSummaryComponent.prototype.onOnUpdateCart = function () {
        var _this = this;
        var clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = [];
        this.barChartLabels.forEach(function () {
            clone[0].data.push(Math.round(Math.random() * 100));
        });
        this.barChartData = clone;
        setTimeout(function () {
            _this.firstLoad();
        }, 300);
    };
    MemberSummaryComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate;
            return __generator(this, function (_a) {
                currentDate = new Date();
                console.log(this.barChartLabels);
                this.onOnUpdateCart();
                return [2 /*return*/];
            });
        });
    };
    MemberSummaryComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, barChartLabels, newData, allData, data, clone, e_1, barChartLabels, newData, allData, data, clone, barChartLabels, newData, allData, data, clone, barChartLabels, newData, allData, data, clone;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getMemberSummaryByDate('today')];
                    case 1:
                        result = _a.sent();
                        console.log("hehe", result);
                        this.data = result.result;
                        if (this.data.registered_by_month) {
                            barChartLabels = [];
                            newData = [];
                            allData = void 0;
                            allData = this.data.registered_member_daily.value;
                            for (data in allData) {
                                // console.log('allData data', allData[data])
                                barChartLabels.push(data);
                                newData.push(allData[data]);
                            }
                            allData = null; //clearing memory
                            clone = JSON.parse(JSON.stringify(this.barChartData));
                            clone[0].data = newData;
                            this.barChartLabels = barChartLabels;
                            this.barChartData = clone;
                            console.log('awd ', this.barChartData);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log("this e result", e_1);
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3:
                        if (this.data.registered_by_month) {
                            barChartLabels = [];
                            newData = [];
                            allData = void 0;
                            allData = this.data.registered_by_month.value;
                            for (data in allData) {
                                barChartLabels.push(data);
                                newData.push(allData[data]);
                            }
                            allData = null;
                            clone = JSON.parse(JSON.stringify(this.barChartData2));
                            clone[0].data = newData;
                            this.barChartLabels = barChartLabels;
                            this.barChartData2 = clone;
                        }
                        if (this.data.registered_guest_daily) {
                            barChartLabels = [];
                            newData = [];
                            allData = void 0;
                            allData = this.data.registered_guest_daily.value;
                            for (data in allData) {
                                barChartLabels.push(data);
                                newData.push(allData[data]);
                            }
                            allData = null;
                            clone = JSON.parse(JSON.stringify(this.barChartData3));
                            clone[0].data = newData;
                            this.barChartLabels = barChartLabels;
                            this.barChartData3 = clone;
                            console.log('test', this.barChartData3);
                        }
                        if (this.data.registered_guest_by_month) {
                            barChartLabels = [];
                            newData = [];
                            allData = void 0;
                            allData = this.data.registered_guest_by_month.value;
                            for (data in allData) {
                                barChartLabels.push(data);
                                newData.push(allData[data]);
                            }
                            allData = null;
                            clone = JSON.parse(JSON.stringify(this.barChartData4));
                            clone[0].data = newData;
                            this.barChartLabels = barChartLabels;
                            this.barChartData4 = clone;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MemberSummaryComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
    ]; };
    MemberSummaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-membersummary-member',
            template: __webpack_require__(/*! raw-loader!./membersummary.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin_members/member-summary/membersummary.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./membersummary.component.scss */ "./src/app/layout/modules/admin_members/member-summary/membersummary.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
    ], MemberSummaryComponent);
    return MemberSummaryComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin_members/member-summary/membersummary.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/member-summary/membersummary.module.ts ***!
  \*************************************************************************************/
/*! exports provided: MemberSummaryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberSummaryModule", function() { return MemberSummaryModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _membersummary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./membersummary.component */ "./src/app/layout/modules/admin_members/member-summary/membersummary.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _membersummary_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./membersummary-routing.module */ "./src/app/layout/modules/admin_members/member-summary/membersummary-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var MemberSummaryModule = /** @class */ (function () {
    function MemberSummaryModule() {
    }
    MemberSummaryModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _membersummary_routing_module__WEBPACK_IMPORTED_MODULE_7__["MemberSummaryRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_9__["ChartsModule"]
            ],
            declarations: [
                _membersummary_component__WEBPACK_IMPORTED_MODULE_2__["MemberSummaryComponent"]
            ]
        })
    ], MemberSummaryModule);
    return MemberSummaryModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin_members-member-summary-membersummary-module.js.map