(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin-report-product-activity-report-product-activity-report-summary-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.component.html":
/*!**********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.component.html ***!
  \**********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <app-page-header [heading]=\"'Product Activity Report'\" [icon]=\"'fa-table'\">\r\n    </app-page-header>\r\n    <div class=\"summary-report row\">\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"data.top_product_activity\">\r\n                    \r\n                    <div class=\"card-header\">\r\n                        Top 10 Most Viewed Product\r\n                    </div>\r\n                    <div class=\"card-body emirate-daily\">\r\n                        <table>\r\n                            <tr>\r\n                                <th>No.</th>\r\n                                <th>Product Code</th>\r\n                                <th>Product Name</th>\r\n                                <th>Views</th>\r\n                            </tr>\r\n                            <tr *ngFor=\"let dt of data.top_product_activity;let i=index;\">\r\n                                <td>{{i+1}}</td>\r\n                                <td>{{dt.product_code}}</td>\r\n                                <td>{{dt.product_name}}</td>\r\n                                <td>{{dt.count}}</td>\r\n\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n                <div class=\"row-per-card\">\r\n                    <div class=\"card \" *ngIf=\"data.top_product_sales\">\r\n                        \r\n                        <div class=\"card-header\">\r\n                            Top 10 Most Sales Product\r\n                        </div>\r\n                        <div class=\"card-body emirate-daily\">\r\n                            <table>\r\n                                <tr>\r\n                                    <th>No.</th>\r\n                                    <th>Product Code</th>\r\n                                    <th>Product Name</th>\r\n                                    <th>Sales</th>\r\n                                </tr>\r\n                                <tr *ngFor=\"let dt of data.top_product_sales;let i=index;\">\r\n                                    <td>{{i+1}}</td>\r\n                                    <td>{{dt.product_code}}</td>\r\n                                    <td>{{dt.product_name}}</td>\r\n                                    <td>{{dt.count}}</td>\r\n    \r\n                                </tr>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n    \r\n            </div>\r\n    </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary-routing.module.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary-routing.module.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: ProductActivityReportSummaryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductActivityReportSummaryRoutingModule", function() { return ProductActivityReportSummaryRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _product_activity_report_summary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./product-activity-report-summary.component */ "./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _product_activity_report_summary_component__WEBPACK_IMPORTED_MODULE_2__["ProductActivityReportSummaryComponent"],
    },
];
var ProductActivityReportSummaryRoutingModule = /** @class */ (function () {
    function ProductActivityReportSummaryRoutingModule() {
    }
    ProductActivityReportSummaryRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ProductActivityReportSummaryRoutingModule);
    return ProductActivityReportSummaryRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.component.scss":
/*!********************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.component.scss ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".summary-report .dark-header {\n  background: #34495e;\n  font-weight: bold;\n  color: white;\n}\n.summary-report .card-body.emirate-daily {\n  max-height: 340px;\n  overflow: auto;\n}\n.summary-report .row-per-card {\n  margin-bottom: 20px;\n}\n.summary-report table {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n  text-align: center;\n}\n.summary-report table th {\n  background: #333;\n  color: white;\n  font-size: 12px;\n  font-weight: normal;\n}\n.summary-report table tr:nth-child(odd) td {\n  background-color: #f0f0f0;\n}\n.summary-report table td, .summary-report table th {\n  border: 1px solid #ccc;\n  padding: 4px 15px;\n}\n.summary-report table td {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tcmVwb3J0L3Byb2R1Y3QtYWN0aXZpdHktcmVwb3J0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcYWRtaW4tcmVwb3J0XFxwcm9kdWN0LWFjdGl2aXR5LXJlcG9ydFxccHJvZHVjdC1hY3Rpdml0eS1yZXBvcnQtc3VtbWFyeS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tcmVwb3J0L3Byb2R1Y3QtYWN0aXZpdHktcmVwb3J0L3Byb2R1Y3QtYWN0aXZpdHktcmVwb3J0LXN1bW1hcnkuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQ0FSO0FER0k7RUFDSSxpQkFBQTtFQUNBLGNBQUE7QUNEUjtBREdJO0VBQ0ksbUJBQUE7QUNEUjtBREdJO0VBQ0kseUJBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQ0RSO0FERVE7RUFDSSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNBWjtBREVRO0VBQ0kseUJBQUE7QUNBWjtBREVRO0VBQ0ksc0JBQUE7RUFDQSxpQkFBQTtBQ0FaO0FERVE7RUFDSSxlQUFBO0FDQVoiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbi1yZXBvcnQvcHJvZHVjdC1hY3Rpdml0eS1yZXBvcnQvcHJvZHVjdC1hY3Rpdml0eS1yZXBvcnQtc3VtbWFyeS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zdW1tYXJ5LXJlcG9ydHtcclxuICAgIC5kYXJrLWhlYWRlcntcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMzQ0OTVlO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmNhcmQtYm9keS5lbWlyYXRlLWRhaWx5e1xyXG4gICAgICAgIG1heC1oZWlnaHQ6IDM0MHB4O1xyXG4gICAgICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgfVxyXG4gICAgLnJvdy1wZXItY2FyZHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgfVxyXG4gICAgdGFibGV7XHJcbiAgICAgICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB0aHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogIzMzMztcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRyOm50aC1jaGlsZChvZGQpIHRke1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0ZCwgdGh7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDRweCAxNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0ZHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG59IiwiLnN1bW1hcnktcmVwb3J0IC5kYXJrLWhlYWRlciB7XG4gIGJhY2tncm91bmQ6ICMzNDQ5NWU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogd2hpdGU7XG59XG4uc3VtbWFyeS1yZXBvcnQgLmNhcmQtYm9keS5lbWlyYXRlLWRhaWx5IHtcbiAgbWF4LWhlaWdodDogMzQwcHg7XG4gIG92ZXJmbG93OiBhdXRvO1xufVxuLnN1bW1hcnktcmVwb3J0IC5yb3ctcGVyLWNhcmQge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuLnN1bW1hcnktcmVwb3J0IHRhYmxlIHtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0aCB7XG4gIGJhY2tncm91bmQ6ICMzMzM7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuLnN1bW1hcnktcmVwb3J0IHRhYmxlIHRyOm50aC1jaGlsZChvZGQpIHRkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0ZCwgLnN1bW1hcnktcmVwb3J0IHRhYmxlIHRoIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgcGFkZGluZzogNHB4IDE1cHg7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdGQge1xuICBmb250LXNpemU6IDEycHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.component.ts":
/*!******************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.component.ts ***!
  \******************************************************************************************************************/
/*! exports provided: ProductActivityReportSummaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductActivityReportSummaryComponent", function() { return ProductActivityReportSummaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/activity/activity.service */ "./src/app/services/activity/activity.service.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ProductActivityReportSummaryComponent = /** @class */ (function () {
    function ProductActivityReportSummaryComponent(productService, sanitizer, activityService) {
        this.productService = productService;
        this.sanitizer = sanitizer;
        this.activityService = activityService;
        this.data = [];
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
            scales: {
                yAxes: [{
                        ticks: {
                            callback: this.currencyFormatter,
                        }
                    }]
            },
        };
        this.barChartLabels = [];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartData = [
            { data: [], label: 'user active each platform today' },
        ];
        this.optO = { fill: false, borderWidth: 1, };
        this.analitycsData = {
            eachPlatformToday: {
                barChartData: [__assign({ data: [], label: 'each platform' }, this.optO)],
                barChartLabels: []
            },
            eachPageToday: {
                barChartData: [__assign({ data: [], label: 'each page' }, this.optO)],
                barChartLabels: []
            },
        };
        this.service = productService;
        this.activityService.addLog({ page: this.title });
    }
    ProductActivityReportSummaryComponent.prototype.onUpdateCart = function (data) {
        var clonedData = JSON.parse(JSON.stringify(data.barChartData));
        var clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = [];
        data.barChartLabels.forEach(function () { clone[0].data.push(Math.round(Math.random() * 100)); });
        data.barChartData = clone;
        setTimeout(function () {
            data.barChartData = clonedData;
        }, 500);
    };
    ProductActivityReportSummaryComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate;
            return __generator(this, function (_a) {
                currentDate = new Date();
                console.log(this.barChartLabels);
                this.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    ProductActivityReportSummaryComponent.prototype.generateChartData = function (dataValues) {
        var barChartLabels = [];
        var newData = [];
        var allData;
        allData = dataValues;
        var total = 0;
        for (var data in allData) {
            // console.log('allData data', allData[data])
            barChartLabels.push(data);
            total += parseInt(allData[data]);
            newData.push(allData[data]);
        }
        allData = null; //clearing memory
        // console.log("new Data", newData)
        return [barChartLabels, newData, total];
    };
    ProductActivityReportSummaryComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.getProductActivityReport()];
                    case 1:
                        result = _a.sent();
                        this.data = result.result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log("this e result", e_1);
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductActivityReportSummaryComponent.prototype.calculateTheAverageValue = function (_value) {
        var howMany = Object.keys(_value).length;
        var sum = 0;
        Reflect.ownKeys(_value).forEach(function (key) {
            sum = sum + _value[key];
        });
        var avg = sum / howMany;
        return avg;
    };
    ProductActivityReportSummaryComponent.prototype.currencyFormatter = function (value, index, values) {
        // add comma as thousand separator
        return value + '%';
    };
    ProductActivityReportSummaryComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] },
        { type: _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_3__["ActivityService"] }
    ]; };
    ProductActivityReportSummaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-activity-report-summary',
            template: __webpack_require__(/*! raw-loader!./product-activity-report-summary.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./product-activity-report-summary.component.scss */ "./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"],
            _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_3__["ActivityService"]])
    ], ProductActivityReportSummaryComponent);
    return ProductActivityReportSummaryComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.module.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.module.ts ***!
  \***************************************************************************************************************/
/*! exports provided: ProductActivityReportSummaryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductActivityReportSummaryModule", function() { return ProductActivityReportSummaryModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _product_activity_report_summary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./product-activity-report-summary.component */ "./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _product_activity_report_summary_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./product-activity-report-summary-routing.module */ "./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var ProductActivityReportSummaryModule = /** @class */ (function () {
    function ProductActivityReportSummaryModule() {
    }
    ProductActivityReportSummaryModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _product_activity_report_summary_routing_module__WEBPACK_IMPORTED_MODULE_7__["ProductActivityReportSummaryRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_9__["ChartsModule"]
            ],
            declarations: [
                _product_activity_report_summary_component__WEBPACK_IMPORTED_MODULE_2__["ProductActivityReportSummaryComponent"]
            ]
        })
    ], ProductActivityReportSummaryModule);
    return ProductActivityReportSummaryModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin-report-product-activity-report-product-activity-report-summary-module.js.map