(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-points-transaction-approval-points-transaction-approval-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/points-transaction-approval/points-transaction-approval.component.html":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/points-transaction-approval/points-transaction-approval.component.html ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'Point'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n  <div class=\"container-swapper-option\" *ngIf=\"pointDetail==false&&!errorMessage\">\r\n    <span style=\"margin-right: 10px;\">View</span> \r\n    <div class=\"button-container\">\r\n    <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n        <span class=\"fa fa-table\"> Requested</span>\r\n    </button>\r\n    <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n        <span class=\"fa fa-table\"> Processed</span>\r\n    </button>\r\n    </div>\r\n  </div>\r\n\r\n  <div *ngIf=\"points&&pointDetail==false && swaper &&!errorMessage\">\r\n        <app-form-builder-table \r\n        [tableFormat] = \"tableFormat\" \r\n        [table_data]  = \"points\" \r\n        [searchCallback]= \"[service, 'searchPointstransactionApprovalProcessLint',this]\" \r\n        [total_page]=\"totalPage\"\r\n        >\r\n        </app-form-builder-table>\r\n  </div>\r\n\r\n  <div *ngIf=\"points&&pointDetail==false && !swaper &&!errorMessage\">\r\n    <app-form-builder-table \r\n    [tableFormat] = \"tableFormat\" \r\n    [table_data]  = \"points\" \r\n    [searchCallback]= \"[service, 'searchPointstransactionApprovalProcessLint2',this]\" \r\n    [total_page]=\"totalPage\"\r\n    >\r\n        \r\n    </app-form-builder-table>\r\n  </div>\r\n  <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n    <div class=\"text-message\">\r\n      <i class=\"fas fa-ban\"></i>\r\n      {{errorMessage}}\r\n    </div>\r\n  </div>\r\n  \r\n \r\n</div>\r\n\r\n<!-- <div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  <div class=\"container-swapper-option\" *ngIf=\"pointDetail==false&&!errorMessage\">\r\n      <span style=\"margin-right: 10px;\">View</span> \r\n      <div class=\"button-container\">\r\n      <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n          <span class=\"fa fa-table\"> Requested</span>\r\n      </button>\r\n      <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n          <span class=\"fa fa-table\"> Approved</span>\r\n      </button>\r\n      </div>\r\n  </div>\r\n\r\n  <div *ngIf=\"Evouchers && pointDetail==false && swaper &&!errorMessage\">\r\n        <app-form-builder-table \r\n        [tableFormat] = \"tableFormat\" \r\n        [table_data]  = \"Evouchers\" \r\n        [searchCallback]= \"[service, 'searchEvoucherStockReport',this]\" \r\n        [total_page]=\"totalPage\"\r\n        >\r\n            \r\n        </app-form-builder-table>\r\n  </div>\r\n\r\n  <div *ngIf=\"Evouchers && evoucherStockDetail==false && !swaper &&!errorMessage\">\r\n      <app-form-builder-table \r\n      [tableFormat] = \"tableFormat\" \r\n      [table_data]  = \"Evouchers\" \r\n      [searchCallback]= \"[service, 'searchEvoucherStockReport2',this]\" \r\n      [total_page]=\"totalPage\"\r\n      >\r\n          \r\n      </app-form-builder-table>\r\n  </div>\r\n\r\n  <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n      <div class=\"text-message\">\r\n          <i class=\"fas fa-ban\"></i>\r\n          {{errorMessage}}\r\n      </div>\r\n  </div>\r\n\r\n  <div *ngIf=\"evoucherStockDetail\">\r\n      <app-evoucher-stock-report-detail [back]=\"[this,backToHere]\" [detail]=\"evoucherStockDetail\"></app-evoucher-stock-report-detail>\r\n  </div>\r\n\r\n \r\n</div> -->\r\n\r\n \r\n\r\n "

/***/ }),

/***/ "./src/app/layout/modules/points-transaction-approval/points-transaction-approval-routing.module.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-approval/points-transaction-approval-routing.module.ts ***!
  \**********************************************************************************************************/
/*! exports provided: PointsTransationApprovalRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsTransationApprovalRoutingModule", function() { return PointsTransationApprovalRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _points_transaction_approval_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./points-transaction-approval.component */ "./src/app/layout/modules/points-transaction-approval/points-transaction-approval.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { BastComponent} from './bast/bast.component'
var routes = [
    {
        path: '', component: _points_transaction_approval_component__WEBPACK_IMPORTED_MODULE_2__["PointsTransationApprovalComponent"]
    },
];
var PointsTransationApprovalRoutingModule = /** @class */ (function () {
    function PointsTransationApprovalRoutingModule() {
    }
    PointsTransationApprovalRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], PointsTransationApprovalRoutingModule);
    return PointsTransationApprovalRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/points-transaction-approval/points-transaction-approval.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-approval/points-transaction-approval.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "iframe {\n  width: 0px;\n  height: 0px;\n  border: 0px medium none;\n  visibility: hidden;\n}\n\n.container-swapper-option {\n  display: flex;\n  justify-content: center;\n  flex-direction: row;\n  align-items: center;\n}\n\n.container-swapper-option .button-container {\n  border: 2px solid #ddd;\n  border-radius: 8px;\n}\n\n.container-swapper-option .button-container button {\n  color: #ddd;\n  background: white;\n  font-size: 14px;\n  border-radius: 8px;\n}\n\n.container-swapper-option .button-container button.true {\n  background: #2480fb;\n  border-radius: 8px;\n  color: #ddd;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcG9pbnRzLXRyYW5zYWN0aW9uLWFwcHJvdmFsL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xccG9pbnRzLXRyYW5zYWN0aW9uLWFwcHJvdmFsXFxwb2ludHMtdHJhbnNhY3Rpb24tYXBwcm92YWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3BvaW50cy10cmFuc2FjdGlvbi1hcHByb3ZhbC9wb2ludHMtdHJhbnNhY3Rpb24tYXBwcm92YWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURFQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNDSjs7QURBSTtFQUVFLHNCQUFBO0VBQ0Esa0JBQUE7QUNDTjs7QURBTTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0VSOztBREFNO0VBRUUsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNDUjs7QURJQTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDREo7O0FER0k7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDRFIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wb2ludHMtdHJhbnNhY3Rpb24tYXBwcm92YWwvcG9pbnRzLXRyYW5zYWN0aW9uLWFwcHJvdmFsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaWZyYW1le1xyXG4gICAgd2lkdGg6MHB4O1xyXG4gICAgaGVpZ2h0OiAwcHg7XHJcbiAgICBib3JkZXI6MHB4IG1lZGl1bSBub25lO1xyXG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG59XHJcblxyXG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgLmJ1dHRvbi1jb250YWluZXIge1xyXG5cclxuICAgICAgYm9yZGVyOiAycHggc29saWQgI2RkZDtcclxuICAgICAgYm9yZGVyLXJhZGl1czo4cHg7XHJcbiAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgY29sb3I6ICNkZGQ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgfVxyXG4gICAgICBidXR0b24udHJ1ZSB7XHJcbiAgICAgICBcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMjQ4MGZiO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICBjb2xvcjogI2RkZDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn0iLCJpZnJhbWUge1xuICB3aWR0aDogMHB4O1xuICBoZWlnaHQ6IDBweDtcbiAgYm9yZGVyOiAwcHggbWVkaXVtIG5vbmU7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbn1cblxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiAuYnV0dG9uLWNvbnRhaW5lciB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNkZGQ7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbn1cbi5jb250YWluZXItc3dhcHBlci1vcHRpb24gLmJ1dHRvbi1jb250YWluZXIgYnV0dG9uIHtcbiAgY29sb3I6ICNkZGQ7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbn1cbi5jb250YWluZXItc3dhcHBlci1vcHRpb24gLmJ1dHRvbi1jb250YWluZXIgYnV0dG9uLnRydWUge1xuICBiYWNrZ3JvdW5kOiAjMjQ4MGZiO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGNvbG9yOiAjZGRkO1xufVxuXG4uZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/points-transaction-approval/points-transaction-approval.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-approval/points-transaction-approval.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: PointsTransationApprovalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsTransationApprovalComponent", function() { return PointsTransationApprovalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/points-transaction/points-transaction.service */ "./src/app/services/points-transaction/points-transaction.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;
var PointsTransationApprovalComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function PointsTransationApprovalComponent(sanitizer, PointsTransactionService) {
        this.sanitizer = sanitizer;
        this.PointsTransactionService = PointsTransactionService;
        this.points = [];
        this.row_id = "process_number";
        this.swaper = true;
        this.tableFormat = {
            title: 'Point Transaction Report',
            label_headers: [
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
                // {label: 'Updated Date',     visible: true, type: 'date', data_row_name: 'updated_date'},
                { label: 'Process Number', visible: true, type: 'string', data_row_name: 'process_number' },
                { label: 'Process ID', visible: true, type: 'string', data_row_name: 'process_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
                { label: 'Nama Pelanggan', visible: true, type: 'string', data_row_name: 'full_name' },
                { label: 'Points', visible: true, type: 'string', data_row_name: 'points' },
                { label: 'Description', visible: true, type: 'string', data_row_name: 'description' },
                { label: 'Remarks', visible: true, type: 'string', data_row_name: 'remarks' },
                { label: 'Process Type', visible: true, type: 'string', data_row_name: 'process_type' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Transaction Month', visible: true, type: 'string', data_row_name: 'transaction_month' },
            ],
            row_primary_key: '_id',
            formOptions: {
                // addForm   : true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'points',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.errorMessage = false;
        this.pointDetail = false;
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    PointsTransationApprovalComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    PointsTransationApprovalComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        // let params = {
                        //   'type':'member'
                        // }
                        this.service = this.PointsTransactionService;
                        result = void 0;
                        if (!(this.swaper == true)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.PointsTransactionService.getPointstransactionApprovalProcessLint('REQUESTED')];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.PointsTransactionService.getPointstransactionApprovalProcessLint('PROCESSED')];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        // const result: any  = await this.PointsTransactionService.getPointstransactionReportProcessLint();
                        console.warn("form member", result);
                        this.totalPage = result.total_page;
                        this.points = result.values;
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    PointsTransationApprovalComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    PointsTransationApprovalComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.pointDetail = false;
                obj.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    PointsTransationApprovalComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
        this.firstLoad();
    };
    PointsTransationApprovalComponent.ctorParameters = function () { return [
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] },
        { type: _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_3__["PointsTransactionService"] }
    ]; };
    PointsTransationApprovalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-form-member',
            template: __webpack_require__(/*! raw-loader!./points-transaction-approval.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/points-transaction-approval/points-transaction-approval.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./points-transaction-approval.component.scss */ "./src/app/layout/modules/points-transaction-approval/points-transaction-approval.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"], _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_3__["PointsTransactionService"]])
    ], PointsTransationApprovalComponent);
    return PointsTransationApprovalComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/points-transaction-approval/points-transaction-approval.module.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/layout/modules/points-transaction-approval/points-transaction-approval.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: PointsTransactionApprovalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsTransactionApprovalModule", function() { return PointsTransactionApprovalModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _points_transaction_approval_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./points-transaction-approval.component */ "./src/app/layout/modules/points-transaction-approval/points-transaction-approval.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _points_transaction_approval_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./points-transaction-approval-routing.module */ "./src/app/layout/modules/points-transaction-approval/points-transaction-approval-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var PointsTransactionApprovalModule = /** @class */ (function () {
    function PointsTransactionApprovalModule() {
    }
    PointsTransactionApprovalModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _points_transaction_approval_routing_module__WEBPACK_IMPORTED_MODULE_7__["PointsTransationApprovalRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"]
            ],
            declarations: [
                _points_transaction_approval_component__WEBPACK_IMPORTED_MODULE_2__["PointsTransationApprovalComponent"],
            ],
        })
    ], PointsTransactionApprovalModule);
    return PointsTransactionApprovalModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-points-transaction-approval-points-transaction-approval-module.js.map