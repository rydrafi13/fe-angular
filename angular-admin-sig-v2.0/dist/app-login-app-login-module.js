(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-login-app-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/app-login/app-login.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app-login/app-login.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<div class=\"row _container\">\r\n    <div class=\"col-md-12\">\r\n        <input type=\"text\" class=\"myInput\" (keyup)=\"searchMenu(event)\" [(ngModel)]=\"searchForm\" placeholder=\"Search for names..\" title=\"Type in a name\">\r\n        <div class=\"row cl-row-list\">\r\n            <div class=\"col-md-3 col-md-6 col-lg-3\" *ngFor=\"let menuOption of menuSearch\">\r\n                <div class=\"card cl-list\" (click)=\"toMenu(menuOption.appLabel)\">\r\n                    <div class=\"card-header chocoblitz\">\r\n                        <span class=\"card-header-title\">{{menuOption.title}}</span>\r\n                        <hr />\r\n                        <div class=\"card-header-body\">\r\n                            <img src={{menuOption.imageUrl}}>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <!-- Dynamix -->\r\n            <!-- <div class=\"col-md-3 col-md-6 col-lg-3\">\r\n                <div class=\"card cl-list\" (click)=\"toProgramRetailer1()\">\r\n                    <div class=\"card-header chocoblitz\">\r\n                        <span class=\"card-header-title\">PROGRAM DYNAMIX EXTRA POIN 2020</span>\r\n                        <hr />\r\n                        <div class=\"card-header-body\">\r\n                            <img src=\"assets/images/logo-dynamix-vector.png\">\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div> -->\r\n            <!-- MCI -->\r\n            <!-- <div class=\"col-md-3 col-md-6 col-lg-3\">\r\n                <div class=\"card cl-list\" (click)=\"toProgramMCI()\">\r\n                    <div class=\"card-header chocoblitz\">\r\n                        <span class=\"card-header-title\">MCI</span>\r\n                        <hr />\r\n                        <div class=\"card-header-body\">\r\n                            <img src=\"assets/images/mci-logo-black.png\">\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div> -->\r\n            <!-- Admin Management -->\r\n            <!-- <div class=\"col-md-3 col-md-6 col-lg-3\">\r\n                <div class=\"card cl-list\" (click)=\"toAdminManagement()\">\r\n                    <div class=\"card-header tokolab\">\r\n                        <span class=\"card-header-title\">Admin Management</span>\r\n                        <hr />\r\n                        <div class=\"card-header-body\">\r\n                            <img src=\"assets/images/Member.png\">\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div> -->\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<!-- <app-header></app-header>\r\n<div class=\"app-login-page\">\r\n    <div class=\"row _container\">\r\n        <div class=\"col-md-12\">\r\n            <div class=\"row cls-row-list\">\r\n                <div class=\"col-md-3 col-md-6 col-lg-3\">\r\n                    <div class=\"card\">\r\n                        <div class=\"card-header serodja\">\r\n                            <span class=\"card-header-title\">Serodja Official</span>\r\n                            <hr />\r\n                            <div class=\"card-header-body\">\r\n                                <img src=\"assets/images/tshirt.svg\">\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"card-body\">\r\n                            <div class=\"button-container\">\r\n                                <button (click)=\"enterSerodja()\" class=\"btn btn-1 btn-primary\">\r\n                                    Login Serodja Official\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div> -->\r\n  \r\n"

/***/ }),

/***/ "./src/app/app-login/app-login-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/app-login/app-login-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: LoginRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginRoutingModule", function() { return LoginRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-login.component */ "./src/app/app-login/app-login.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared */ "./src/app/shared/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { LogoutComponent } from './app-logout.component';
// import { OrderhistoryComponent } from '../layout/modules/orderhistory/orderhistory.component';
var routes = [
    {
        path: '',
        component: _app_login_component__WEBPACK_IMPORTED_MODULE_2__["AppLoginComponent"],
        canActivate: [_shared__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    }
];
var LoginRoutingModule = /** @class */ (function () {
    function LoginRoutingModule() {
    }
    LoginRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], LoginRoutingModule);
    return LoginRoutingModule;
}());



/***/ }),

/***/ "./src/app/app-login/app-login.component.scss":
/*!****************************************************!*\
  !*** ./src/app/app-login/app-login.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  position: absolute;\n  width: 100%;\n  left: 0px;\n  top: 30px;\n  overflow: hidden;\n  display: flex;\n}\n:host .card {\n  margin-bottom: 25px;\n  cursor: pointer;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.08);\n}\ndiv,\nspan:not(.fa) {\n  font-family: \"Poppins\";\n}\n.app-login-page {\n  margin-top: 40px;\n}\n._container {\n  width: 100%;\n  padding: 25px 0vw;\n  margin-top: 40px;\n}\n._container .row.cl-row-list {\n  margin: 0px;\n}\n._container .card.cl-list {\n  border-radius: 10px;\n}\n._container .card.cl-list .serodja {\n  background: #d68060;\n}\n._container .card.cl-list .annet-sofa {\n  background: #7579e7;\n}\n._container .card.cl-list .rinjani {\n  background: #900d0d;\n}\n._container .card.cl-list .tokolab {\n  background: #f0f0f0;\n}\n._container .card.cl-list .chocoblitz {\n  background: #f0f0f0;\n}\n._container .card.cl-list .statistics {\n  background: #d61e58;\n  background: linear-gradient(50deg, #d61e58 0%, #f28e41 88%);\n}\n._container .card.cl-list .card-header {\n  border-top-left-radius: 10px;\n  border-top-right-radius: 10px;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  border-bottom-width: 0px;\n  display: flex;\n  text-align: center;\n  align-items: flex-start;\n  flex-direction: column;\n  height: 27vh;\n  color: white;\n  justify-content: flex-start;\n}\n._container .card.cl-list .card-header .card-header-title {\n  font-size: 17px;\n  color: #000000;\n}\n._container .card.cl-list .card-header hr {\n  display: block;\n  height: 1px;\n  border: 0;\n  width: 100%;\n  border-top: 1px solid #ccc;\n  margin: 1vh 0;\n  padding: 0;\n}\n._container .card.cl-list .card-header .card-header-body {\n  align-self: stretch;\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n}\n._container .card.cl-list .card-header .card-header-body img {\n  height: 15vh;\n  width: auto;\n  min-width: 100px;\n}\n._container .card.cl-list .card-header .card-header-body .title-sub {\n  font-size: 15px;\n  font-weight: bold;\n}\n._container .card.cl-list .card-header .card-header-body .body-sub {\n  font-size: 50px;\n}\n._container .card.cl-list .card-header .fa {\n  font-size: 21px;\n  font-weight: bold;\n  width: 55px;\n  display: inline-flex;\n  margin: 0px 15px 15px 0px;\n  height: 55px;\n  background: #3498db;\n  justify-content: center;\n  padding: 5px;\n  color: white;\n  align-items: center;\n  border-radius: 5px;\n}\n._container .card.cl-list .card-header .profile {\n  background: #9b59b6;\n}\n._container .card.cl-list .card-header .transactions {\n  background: #2ecc71;\n}\n._container .card.cl-list .card-header .statistics {\n  background: #e67e22;\n}\n._container .card.cl-list .card-header .store {\n  background: #1abc9c;\n}\n._container .card.cl-list .card-body {\n  display: block;\n  position: relative;\n  font-weight: bold;\n  height: 80px;\n}\n._container .card.cl-list .card-body .center {\n  text-align: center;\n}\n._container .card.cl-list .card-body .button-container button.btn.btn-2 {\n  width: calc(100% - 40px);\n  position: absolute;\n  bottom: 80px;\n  left: 20px;\n}\n._container .card.cl-list .card-body .button-container button.btn.btn-1 {\n  width: calc(100% - 40px);\n  position: absolute;\n  bottom: 20px;\n  left: 20px;\n}\n._container .card.cl-list .card-body .button-container .upgrade {\n  background: white;\n  border: 1px solid #ffb24d;\n  color: #ffb24d;\n  font-size: 14px;\n  font-weight: bold;\n  padding: 5px;\n}\n._container .card.cl-list .card-body .button-container button > img {\n  width: auto;\n  height: 30px;\n  padding-right: 5px;\n  padding-bottom: 5px;\n}\n._container .card.cl-list .with-upgrade {\n  height: 201px;\n}\n@media (max-width: 1176px) {\n  ._container .cl-row-list .col-lg-3 {\n    display: flex;\n    justify-content: center;\n    align-items: flex-start;\n    width: calc((100vw - 202px) / 2 - 5px);\n    float: left;\n    max-width: unset;\n    flex: unset;\n  }\n}\n@media (max-width: 768px) {\n  ._container .card.cl-list .card-header {\n    font-size: 20px;\n  }\n  ._container .cl-row-list .col-lg-3 {\n    display: flex;\n    justify-content: center;\n    align-items: flex-start;\n    width: calc((100vw - 202px) / 2 - 5px);\n    float: left;\n  }\n\n  div,\nspan:not(.fa) {\n    font-size: 13px;\n  }\n}\n@media (max-width: 620px) {\n  ._container .cl-row-list .col-lg-3 {\n    width: calc((100vw - 61px) / 2 - 5px);\n  }\n}\n@media (max-width: 460px) {\n  ._container .cl-row-list .col-lg-3 {\n    display: flex;\n    justify-content: center;\n    align-items: flex-start;\n    width: 80vw;\n    float: left;\n    max-width: unset;\n    flex: unset;\n  }\n}\n@media only screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) {\n  ._container .card.cl-list .card-header {\n    font-size: 15px;\n  }\n\n  div,\nspan:not(.fa) {\n    font-size: 7px;\n  }\n\n  :host .card {\n    height: 250px;\n  }\n}\n.myInput {\n  margin: 15px 15px 20px;\n  width: 100%;\n  height: 50px;\n  padding: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLWxvZ2luL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGFwcC1sb2dpblxcYXBwLWxvZ2luLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hcHAtbG9naW4vYXBwLWxvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7QUNDRjtBRENFO0VBQ0UsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsMkNBQUE7QUNDSjtBREdBOztFQUVFLHNCQUFBO0FDQUY7QURHQTtFQUNFLGdCQUFBO0FDQUY7QURHRTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDQUo7QURFSTtFQUNFLFdBQUE7QUNBTjtBREdJO0VBQ0UsbUJBQUE7QUNETjtBREVNO0VBQ0UsbUJBQUE7QUNBUjtBREdNO0VBQ0UsbUJBQUE7QUNEUjtBRElNO0VBQ0UsbUJBQUE7QUNGUjtBREtNO0VBQ0UsbUJBQUE7QUNIUjtBRE1NO0VBQ0UsbUJBQUE7QUNKUjtBRE9NO0VBQ0UsbUJBQUE7RUFDQSwyREFBQTtBQ0xSO0FET007RUFDRSw0QkFBQTtFQUNBLDZCQUFBO0VBQ0EsK0JBQUE7RUFDQSxnQ0FBQTtFQUNBLHdCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsMkJBQUE7QUNMUjtBRE1RO0VBQ0UsZUFBQTtFQUNBLGNBQUE7QUNKVjtBRE1RO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7QUNKVjtBRE1RO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtBQ0pWO0FES1U7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDSFo7QURLVTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQ0haO0FES1U7RUFDRSxlQUFBO0FDSFo7QURNUTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNKVjtBRE9RO0VBQ0UsbUJBQUE7QUNMVjtBRFFRO0VBQ0UsbUJBQUE7QUNOVjtBRFNRO0VBQ0UsbUJBQUE7QUNQVjtBRFVRO0VBQ0UsbUJBQUE7QUNSVjtBRGFNO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFJQSxZQUFBO0FDZFI7QURnQlE7RUFDRSxrQkFBQTtBQ2RWO0FEbUJVO0VBQ0Usd0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0FDakJaO0FEbUJVO0VBQ0Usd0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0FDakJaO0FEbUJVO0VBQ0UsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FDakJaO0FEb0JVO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDbEJaO0FEc0JNO0VBQ0UsYUFBQTtBQ3BCUjtBRHlCRTtFQUVJO0lBQ0UsYUFBQTtJQUNBLHVCQUFBO0lBQ0EsdUJBQUE7SUFDQSxzQ0FBQTtJQUNBLFdBQUE7SUFDQSxnQkFBQTtJQUNBLFdBQUE7RUN2Qk47QUFDRjtBRDJCRTtFQUdNO0lBQ0UsZUFBQTtFQzNCUjtFRDhCSTtJQUNFLGFBQUE7SUFDQSx1QkFBQTtJQUNBLHVCQUFBO0lBQ0Esc0NBQUE7SUFDQSxXQUFBO0VDNUJOOztFRCtCRTs7SUFFRSxlQUFBO0VDNUJKO0FBQ0Y7QUQ4QkU7RUFFSTtJQUNFLHFDQUFBO0VDN0JOO0FBQ0Y7QURpQ0U7RUFFSTtJQUNFLGFBQUE7SUFDQSx1QkFBQTtJQUNBLHVCQUFBO0lBQ0EsV0FBQTtJQUNBLFdBQUE7SUFDQSxnQkFBQTtJQUNBLFdBQUE7RUNoQ047QUFDRjtBRHFDRTtFQUdNO0lBQ0UsZUFBQTtFQ3JDUjs7RUR5Q0U7O0lBRUUsY0FBQTtFQ3RDSjs7RUR5Q0k7SUFDRSxhQUFBO0VDdENOO0FBQ0Y7QUQwQ0U7RUFDRSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBQ3hDSiIsImZpbGUiOiJzcmMvYXBwL2FwcC1sb2dpbi9hcHAtbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGxlZnQ6IDBweDtcclxuICB0b3A6IDMwcHg7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG5cclxuICAuY2FyZCB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgYm94LXNoYWRvdzogMCAzcHggNnB4IDAgcmdiKDAgMCAwIC8gOCUpO1xyXG4gIH1cclxufVxyXG4gIFxyXG5kaXYsXHJcbnNwYW46bm90KC5mYSkge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcclxufVxyXG5cclxuLmFwcC1sb2dpbi1wYWdlIHtcclxuICBtYXJnaW4tdG9wOiA0MHB4O1xyXG59XHJcblxyXG4gIC5fY29udGFpbmVyIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcGFkZGluZzogMjVweCAwdnc7XHJcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xyXG4gIFxyXG4gICAgLnJvdy5jbC1yb3ctbGlzdCB7XHJcbiAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmNhcmQuY2wtbGlzdCB7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgIC5zZXJvZGphIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZDY4MDYwO1xyXG4gICAgICAgIC8vIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg1MGRlZywgcmdiYSg4NCwgNDQsIDE3OSwgMSkgMCUsIHJnYmEoMjE4LCAxMDgsIDEzNSwgMSkgODglKTtcclxuICAgICAgfVxyXG4gICAgICAuYW5uZXQtc29mYSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzc1NzllNztcclxuICAgICAgICAvLyBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNTBkZWcsIHJnYmEoMTM3LCA0MiwgMTA2LCAxKSAwJSwgcmdiYSgyMzIsIDEwMSwgOTksIDEpIDg4JSk7XHJcbiAgICAgIH1cclxuICAgICAgLnJpbmphbmkge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICM5MDBkMGQ7XHJcbiAgICAgICAgLy8gYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDUwZGVnLCByZ2JhKDYyLCA2MCwgMTM3LCAxKSAwJSwgcmdiYSg1MiwgMTQyLCAyNDEsIDEpIDg4JSk7XHJcbiAgICAgIH1cclxuICAgICAgLnRva29sYWIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgLy8gYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDUwZGVnLCByZ2JhKDM1LCA5MSwgMTQ3LCAxKSAwJSwgcmdiYSg1MywgMTkxLCAxNTYsIDEpIDg4JSk7XHJcbiAgICAgIH1cclxuICAgICAgLmNob2NvYmxpdHoge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgLy8gYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDUwZGVnLCByZ2JhKDM1LCA5MSwgMTQ3LCAxKSAwJSwgcmdiYSg1MywgMTkxLCAxNTYsIDEpIDg4JSk7XHJcbiAgICAgIH1cclxuICAgICAgLnN0YXRpc3RpY3Mge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHJnYigyMTQsIDMwLCA4OCk7XHJcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDUwZGVnLCByZ2JhKDIxNCwgMzAsIDg4LCAxKSAwJSwgcmdiYSgyNDIsIDE0MiwgNjUsIDEpIDg4JSk7XHJcbiAgICAgIH1cclxuICAgICAgLmNhcmQtaGVhZGVyIHtcclxuICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbS13aWR0aDogMHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgaGVpZ2h0OiAyN3ZoO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgLmNhcmQtaGVhZGVyLXRpdGxlIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgICAgICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICBociB7XHJcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgIGhlaWdodDogMXB4O1xyXG4gICAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgIG1hcmdpbjogMXZoIDA7XHJcbiAgICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY2FyZC1oZWFkZXItYm9keSB7XHJcbiAgICAgICAgICBhbGlnbi1zZWxmOiBzdHJldGNoO1xyXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTV2aDtcclxuICAgICAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgICAgIG1pbi13aWR0aDogMTAwcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAudGl0bGUtc3ViIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIC5ib2R5LXN1YiB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmZhIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgd2lkdGg6IDU1cHg7XHJcbiAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICAgICAgICAgIG1hcmdpbjogMHB4IDE1cHggMTVweCAwcHg7XHJcbiAgICAgICAgICBoZWlnaHQ6IDU1cHg7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xyXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgICAgIH1cclxuICBcclxuICAgICAgICAucHJvZmlsZSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAjOWI1OWI2O1xyXG4gICAgICAgIH1cclxuICBcclxuICAgICAgICAudHJhbnNhY3Rpb25zIHtcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICMyZWNjNzE7XHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIC5zdGF0aXN0aWNzIHtcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICNlNjdlMjI7XHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIC5zdG9yZSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDI2LCAxODgsIDE1NiwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgXHJcbiAgIFxyXG4gICAgICAuY2FyZC1ib2R5IHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgLy8ganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgLy8gZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAvLyBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGhlaWdodDogODBweDtcclxuICBcclxuICAgICAgICAuY2VudGVyIHtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB9XHJcbiAgXHJcbiAgICAgICAgLmJ1dHRvbi1jb250YWluZXIge1xyXG4gICAgICAgICBcclxuICAgICAgICAgIGJ1dHRvbi5idG4uYnRuLTIge1xyXG4gICAgICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgYm90dG9tOiA4MHB4O1xyXG4gICAgICAgICAgICBsZWZ0OiAyMHB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgYnV0dG9uLmJ0bi5idG4tMSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBib3R0b206IDIwcHg7XHJcbiAgICAgICAgICAgIGxlZnQ6IDIwcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAudXBncmFkZSB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZiMjRkO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmYjI0ZDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGJ1dHRvbiA+IGltZyB7XHJcbiAgICAgICAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLndpdGgtdXBncmFkZSB7ICBcclxuICAgICAgICBoZWlnaHQ6IDIwMXB4IDtcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgIH1cclxuICB9XHJcbiAgQG1lZGlhIChtYXgtd2lkdGg6IDExNzZweCkge1xyXG4gICAgLl9jb250YWluZXIge1xyXG4gICAgICAuY2wtcm93LWxpc3QgLmNvbC1sZy0zIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIHdpZHRoOiBjYWxjKCgxMDB2dyAtIDIwMnB4KSAvIDIgLSA1cHgpO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIG1heC13aWR0aDogdW5zZXQ7XHJcbiAgICAgICAgZmxleDogdW5zZXQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XHJcbiAgICAuX2NvbnRhaW5lciB7XHJcbiAgICAgIC5jYXJkLmNsLWxpc3Qge1xyXG4gICAgICAgIC5jYXJkLWhlYWRlciB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5jbC1yb3ctbGlzdCAuY29sLWxnLTMge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgd2lkdGg6IGNhbGMoKDEwMHZ3IC0gMjAycHgpIC8gMiAtIDVweCk7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGRpdixcclxuICAgIHNwYW46bm90KC5mYSkge1xyXG4gICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA2MjBweCkge1xyXG4gICAgLl9jb250YWluZXIge1xyXG4gICAgICAuY2wtcm93LWxpc3QgLmNvbC1sZy0zIHtcclxuICAgICAgICB3aWR0aDogY2FsYygoMTAwdncgLSA2MXB4KSAvIDIgLSA1cHgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA0NjBweCkge1xyXG4gICAgLl9jb250YWluZXIge1xyXG4gICAgICAuY2wtcm93LWxpc3QgLmNvbC1sZy0zIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIHdpZHRoOiA4MHZ3O1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIG1heC13aWR0aDogdW5zZXQ7XHJcbiAgICAgICAgZmxleDogdW5zZXQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgLy8gaVBob25lIFhcclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChkZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKGRldmljZS1oZWlnaHQ6IDgxMnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAzKSB7XHJcbiAgICAuX2NvbnRhaW5lciB7XHJcbiAgICAgIC5jYXJkLmNsLWxpc3Qge1xyXG4gICAgICAgIC5jYXJkLWhlYWRlciB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBkaXYsXHJcbiAgICBzcGFuOm5vdCguZmEpIHtcclxuICAgICAgZm9udC1zaXplOiA3cHg7XHJcbiAgICB9XHJcbiAgICA6aG9zdCB7XHJcbiAgICAgIC5jYXJkIHtcclxuICAgICAgICBoZWlnaHQ6IDI1MHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIC5teUlucHV0IHtcclxuICAgIG1hcmdpbjogMTVweCAxNXB4IDIwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgfSIsIjpob3N0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMTAwJTtcbiAgbGVmdDogMHB4O1xuICB0b3A6IDMwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGRpc3BsYXk6IGZsZXg7XG59XG46aG9zdCAuY2FyZCB7XG4gIG1hcmdpbi1ib3R0b206IDI1cHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYm94LXNoYWRvdzogMCAzcHggNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTtcbn1cblxuZGl2LFxuc3Bhbjpub3QoLmZhKSB7XG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcbn1cblxuLmFwcC1sb2dpbi1wYWdlIHtcbiAgbWFyZ2luLXRvcDogNDBweDtcbn1cblxuLl9jb250YWluZXIge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMjVweCAwdnc7XG4gIG1hcmdpbi10b3A6IDQwcHg7XG59XG4uX2NvbnRhaW5lciAucm93LmNsLXJvdy1saXN0IHtcbiAgbWFyZ2luOiAwcHg7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IHtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLnNlcm9kamEge1xuICBiYWNrZ3JvdW5kOiAjZDY4MDYwO1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuYW5uZXQtc29mYSB7XG4gIGJhY2tncm91bmQ6ICM3NTc5ZTc7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5yaW5qYW5pIHtcbiAgYmFja2dyb3VuZDogIzkwMGQwZDtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLnRva29sYWIge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuY2hvY29ibGl0eiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5zdGF0aXN0aWNzIHtcbiAgYmFja2dyb3VuZDogI2Q2MWU1ODtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDUwZGVnLCAjZDYxZTU4IDAlLCAjZjI4ZTQxIDg4JSk7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciB7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcbiAgYm9yZGVyLWJvdHRvbS13aWR0aDogMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBoZWlnaHQ6IDI3dmg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuY2FyZC1oZWFkZXIgLmNhcmQtaGVhZGVyLXRpdGxlIHtcbiAgZm9udC1zaXplOiAxN3B4O1xuICBjb2xvcjogIzAwMDAwMDtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIGhyIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGhlaWdodDogMXB4O1xuICBib3JkZXI6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luOiAxdmggMDtcbiAgcGFkZGluZzogMDtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIC5jYXJkLWhlYWRlci1ib2R5IHtcbiAgYWxpZ24tc2VsZjogc3RyZXRjaDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciAuY2FyZC1oZWFkZXItYm9keSBpbWcge1xuICBoZWlnaHQ6IDE1dmg7XG4gIHdpZHRoOiBhdXRvO1xuICBtaW4td2lkdGg6IDEwMHB4O1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuY2FyZC1oZWFkZXIgLmNhcmQtaGVhZGVyLWJvZHkgLnRpdGxlLXN1YiB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciAuY2FyZC1oZWFkZXItYm9keSAuYm9keS1zdWIge1xuICBmb250LXNpemU6IDUwcHg7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciAuZmEge1xuICBmb250LXNpemU6IDIxcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB3aWR0aDogNTVweDtcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gIG1hcmdpbjogMHB4IDE1cHggMTVweCAwcHg7XG4gIGhlaWdodDogNTVweDtcbiAgYmFja2dyb3VuZDogIzM0OThkYjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IHdoaXRlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciAucHJvZmlsZSB7XG4gIGJhY2tncm91bmQ6ICM5YjU5YjY7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWhlYWRlciAudHJhbnNhY3Rpb25zIHtcbiAgYmFja2dyb3VuZDogIzJlY2M3MTtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIC5zdGF0aXN0aWNzIHtcbiAgYmFja2dyb3VuZDogI2U2N2UyMjtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIC5zdG9yZSB7XG4gIGJhY2tncm91bmQ6ICMxYWJjOWM7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWJvZHkge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgaGVpZ2h0OiA4MHB4O1xufVxuLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuY2FyZC1ib2R5IC5jZW50ZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWJvZHkgLmJ1dHRvbi1jb250YWluZXIgYnV0dG9uLmJ0bi5idG4tMiB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDgwcHg7XG4gIGxlZnQ6IDIwcHg7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWJvZHkgLmJ1dHRvbi1jb250YWluZXIgYnV0dG9uLmJ0bi5idG4tMSB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDIwcHg7XG4gIGxlZnQ6IDIwcHg7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC5jYXJkLWJvZHkgLmJ1dHRvbi1jb250YWluZXIgLnVwZ3JhZGUge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2ZmYjI0ZDtcbiAgY29sb3I6ICNmZmIyNGQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHBhZGRpbmc6IDVweDtcbn1cbi5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtYm9keSAuYnV0dG9uLWNvbnRhaW5lciBidXR0b24gPiBpbWcge1xuICB3aWR0aDogYXV0bztcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4uX2NvbnRhaW5lciAuY2FyZC5jbC1saXN0IC53aXRoLXVwZ3JhZGUge1xuICBoZWlnaHQ6IDIwMXB4O1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogMTE3NnB4KSB7XG4gIC5fY29udGFpbmVyIC5jbC1yb3ctbGlzdCAuY29sLWxnLTMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgd2lkdGg6IGNhbGMoKDEwMHZ3IC0gMjAycHgpIC8gMiAtIDVweCk7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWF4LXdpZHRoOiB1bnNldDtcbiAgICBmbGV4OiB1bnNldDtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5fY29udGFpbmVyIC5jYXJkLmNsLWxpc3QgLmNhcmQtaGVhZGVyIHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gIH1cbiAgLl9jb250YWluZXIgLmNsLXJvdy1saXN0IC5jb2wtbGctMyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICB3aWR0aDogY2FsYygoMTAwdncgLSAyMDJweCkgLyAyIC0gNXB4KTtcbiAgICBmbG9hdDogbGVmdDtcbiAgfVxuXG4gIGRpdixcbnNwYW46bm90KC5mYSkge1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDYyMHB4KSB7XG4gIC5fY29udGFpbmVyIC5jbC1yb3ctbGlzdCAuY29sLWxnLTMge1xuICAgIHdpZHRoOiBjYWxjKCgxMDB2dyAtIDYxcHgpIC8gMiAtIDVweCk7XG4gIH1cbn1cbkBtZWRpYSAobWF4LXdpZHRoOiA0NjBweCkge1xuICAuX2NvbnRhaW5lciAuY2wtcm93LWxpc3QgLmNvbC1sZy0zIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgIHdpZHRoOiA4MHZ3O1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG1heC13aWR0aDogdW5zZXQ7XG4gICAgZmxleDogdW5zZXQ7XG4gIH1cbn1cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKGRldmljZS13aWR0aDogMzc1cHgpIGFuZCAoZGV2aWNlLWhlaWdodDogODEycHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDMpIHtcbiAgLl9jb250YWluZXIgLmNhcmQuY2wtbGlzdCAuY2FyZC1oZWFkZXIge1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgfVxuXG4gIGRpdixcbnNwYW46bm90KC5mYSkge1xuICAgIGZvbnQtc2l6ZTogN3B4O1xuICB9XG5cbiAgOmhvc3QgLmNhcmQge1xuICAgIGhlaWdodDogMjUwcHg7XG4gIH1cbn1cbi5teUlucHV0IHtcbiAgbWFyZ2luOiAxNXB4IDE1cHggMjBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogNTBweDtcbiAgcGFkZGluZzogMTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/app-login/app-login.component.ts":
/*!**************************************************!*\
  !*** ./src/app/app-login/app-login.component.ts ***!
  \**************************************************/
/*! exports provided: AppLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLoginComponent", function() { return AppLoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { LoginService } from '../services/login/login.service';
// import { errorConvertToMessage } from '../../environments/environment';
// import { setTime } from 'ngx-bootstrap/chronos/utils/date-setters';
// import { PermissionObserver } from '../services/observerable/permission-observer';
var AppLoginComponent = /** @class */ (function () {
    function AppLoginComponent(
    // public loginService:LoginService, 
    router) {
        // this.permission.currentPermission.subscribe((val) => {
        // 	this.currentPermission = val;
        // });
        this.router = router;
        this.name = "";
        this.menuOptions = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_3__;
        this.Login = [];
        this.loginOpened = false;
        this.imageLoginData = [
            { img: 'url("/assets/images/malte-wingen-PDX_a_82obo-unsplash-mini.jpg")', text: "The Amazing way to start anything is Today." },
            { img: 'url("/assets/images/alexander-rotker-l8p1aWZqHvE-unsplash.jpg")', text: "Keep your eyes on the stars." },
            { img: 'url("/assets/images/curology-sR1oAhAT_Uw-unsplash.jpg")', text: "Your Journey is never ending." },
            { img: 'url("/assets/images/vincent-branciforti-mGh2rjPgUyA-unsplash.jpg")', text: "Not Impossible<br/> But I'm-possible." },
            { img: 'url("/assets/images/nick-van-den-berg-2gaNlcD75sk-unsplash.jpg")', text: "Everyhting is not as easy as they said." },
        ];
        this.errorLabel = false;
        this.searchForm = "";
        // this.choosenImage = this.imageLoginData[this.getRandomInt(0,4)];
        // console.log("CHOOSEN IMAGE", this.choosenImage);
        // let form_add     : any = [
        //     { label:"Email",  type: "text",   data_binding: 'email'  },
        //     { label:"Password",  type: "password",  data_binding: 'password'  }
        //   ];
    }
    AppLoginComponent.prototype.getRandomInt = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    AppLoginComponent.prototype.searchMenu = function () {
        var _this = this;
        this.menuSearch = this.menuOptions.filter(function (element) {
            return element.title.toLowerCase().includes(_this.searchForm.toLowerCase());
        });
    };
    AppLoginComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AppLoginComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                localStorage.removeItem('programName');
                this.menuSearch = this.menuOptions;
                return [2 /*return*/];
            });
        });
    };
    AppLoginComponent.prototype.onLoggedin = function () {
        localStorage.setItem('isLoggedin', 'true');
    };
    //   APP LOGIN
    AppLoginComponent.prototype.toMenu = function (appLabel) {
        // console.warn("enter program", this.router);
        localStorage.setItem('programName', appLabel);
        if (appLabel == "admin") {
            this.router.navigateByUrl('/admin-management/admin');
        }
        else {
            this.router.navigateByUrl('/administrator/orderhistoryallhistoryadmin');
        }
        // console.warn("after", this.router);
    };
    AppLoginComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
    ]; };
    AppLoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./app-login.component.html */ "./node_modules/raw-loader/index.js!./src/app/app-login/app-login.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./app-login.component.scss */ "./src/app/app-login/app-login.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AppLoginComponent);
    return AppLoginComponent;
}());



/***/ }),

/***/ "./src/app/app-login/app-login.module.ts":
/*!***********************************************!*\
  !*** ./src/app/app-login/app-login.module.ts ***!
  \***********************************************/
/*! exports provided: AppLoginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLoginModule", function() { return AppLoginModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _app_login_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-login-routing.module */ "./src/app/app-login/app-login-routing.module.ts");
/* harmony import */ var _app_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-login.component */ "./src/app/app-login/app-login.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _layout_modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../layout/modules/bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _app_logout_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app-logout.component */ "./src/app/app-login/app-logout.component.ts");
/* harmony import */ var _layout_components_header_header_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../layout/components/header/header.module */ "./src/app/layout/components/header/header.module.ts");
/* harmony import */ var _shared_guard_portal_guard__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../shared/guard/portal.guard */ "./src/app/shared/guard/portal.guard.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










// import { HeaderComponent } from '../layout/components/header/header.component';



var AppLoginModule = /** @class */ (function () {
    function AppLoginModule() {
    }
    AppLoginModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _app_login_routing_module__WEBPACK_IMPORTED_MODULE_2__["LoginRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _layout_components_header_header_module__WEBPACK_IMPORTED_MODULE_10__["HeaderModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__["TranslateModule"],
                _layout_modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
            ],
            declarations: [_app_login_component__WEBPACK_IMPORTED_MODULE_3__["AppLoginComponent"], _app_logout_component__WEBPACK_IMPORTED_MODULE_9__["LogoutComponent"]],
            providers: [
                _shared_guard_portal_guard__WEBPACK_IMPORTED_MODULE_11__["PortalGuard"]
            ]
        })
    ], AppLoginModule);
    return AppLoginModule;
}());



/***/ }),

/***/ "./src/app/app-login/app-logout.component.ts":
/*!***************************************************!*\
  !*** ./src/app/app-login/app-logout.component.ts ***!
  \***************************************************/
/*! exports provided: LogoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutComponent", function() { return LogoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_login_login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/login/login.service */ "./src/app/services/login/login.service.ts");
/* harmony import */ var _object_interface_common_function__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../object-interface/common.function */ "./src/app/object-interface/common.function.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var LogoutComponent = /** @class */ (function () {
    function LogoutComponent(loginService, router) {
        this.loginService = loginService;
        this.router = router;
        this.name = "";
        this.Login = [];
        this.loginOpened = false;
        this.imageLoginData = [
            { img: 'url("/assets/images/malte-wingen-PDX_a_82obo-unsplash-mini.jpg")', text: "The Amazing way to start anything is Today." },
            { img: 'url("/assets/images/alexander-rotker-l8p1aWZqHvE-unsplash.jpg")', text: "Keep your eyes on the stars." },
            { img: 'url("/assets/images/curology-sR1oAhAT_Uw-unsplash.jpg")', text: "Your Journey is never ending." },
            { img: 'url("/assets/images/vincent-branciforti-mGh2rjPgUyA-unsplash.jpg")', text: "Not Impossible<br/> But I'm-possible." },
            { img: 'url("/assets/images/nick-van-den-berg-2gaNlcD75sk-unsplash.jpg")', text: "Everyhting is not as easy as they said." },
        ];
        this.errorLabel = false;
        this.choosenImage = this.imageLoginData[this.getRandomInt(0, 4)];
        console.log("CHOOSEN IMAGE", this.choosenImage);
        var form_add = [
            { label: "Email", type: "text", data_binding: 'email' },
            { label: "Password", type: "password", data_binding: 'password' }
        ];
    }
    LogoutComponent.prototype.getRandomInt = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    LogoutComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Object(_object_interface_common_function__WEBPACK_IMPORTED_MODULE_4__["clearSession"])()];
                    case 1:
                        _a.sent();
                        this.router.navigateByUrl("/login");
                        return [2 /*return*/];
                }
            });
        });
    };
    LogoutComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    // openLogin(){
    //     this.loginOpened = !this.loginOpened;
    // }
    LogoutComponent.prototype.onLoggedin = function () {
        localStorage.setItem('isLoggedin', 'true');
    };
    LogoutComponent.prototype.formSubmitAddMember = function (form) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.errorLabel = false;
                        if (this.loginOpened == false) {
                            this.loginOpened = !this.loginOpened;
                            return [2 /*return*/, false];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.loginService.login(form)];
                    case 2:
                        result = _a.sent();
                        this.Login = result.result;
                        localStorage.setItem('isLoggedin', 'true');
                        localStorage.setItem('tokenlogin', this.Login.token);
                        if (typeof this.Login.permission == 'string' && this.Login.permission == 'merchant') {
                            this.router.navigateByUrl('/merchant-portal');
                        }
                        else if (typeof this.Login.permission == 'string' && this.Login.permission == 'admin') {
                            this.router.navigateByUrl('/dashboard');
                        }
                        else if (Array.isArray(this.Login.permission) && this.Login.permission.includes('merchant')) {
                            this.router.navigateByUrl('/merchant-portal');
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        setTimeout(function () {
                            _this.errorLabel = e_1.message;
                        }, 1000);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, false];
                }
            });
        });
    };
    LogoutComponent.ctorParameters = function () { return [
        { type: _services_login_login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
    ]; };
    LogoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./app-login.component.html */ "./node_modules/raw-loader/index.js!./src/app/app-login/app-login.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./app-login.component.scss */ "./src/app/app-login/app-login.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_login_login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], LogoutComponent);
    return LogoutComponent;
}());



/***/ })

}]);
//# sourceMappingURL=app-login-app-login-module.js.map