(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~merchant-portal-merchant-portal-module~modules-product-evoucher-evoucher-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component.html":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component.html ***!
  \****************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-form-builder-table \r\n\r\n[table_data]=\"evoucherDetail\"\r\n[searchCallback]=\"[service,'searchEvouchersLint',this]\"\r\n[tableFormat]=\"tableFormat\"\r\n[total_page]=\"totalPage\">\r\n\r\n</app-form-builder-table>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product.evoucher/evoucher.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product.evoucher/evoucher.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n      <!-- <app-page-header [heading]=\"'E-Voucher'\" [icon]=\"'fa-table'\"></app-page-header> -->\r\n      <!-- <h3 style=\"color: white\">File Upload</h3>\r\n      <div class=\"col-md-3 col-sm-3\">\r\n            <div class=\"pb-framer\">\r\n                  <div [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                  </div>\r\n                  <span *ngIf=\"progressBar < 58 && progressBar > 0\">\r\n                        {{cancel?'terminated':'Upload Progress: '+ progressBar +'%'}} </span>\r\n                  <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':'Upload Progress: '+ progressBar+'%'}} </span>\r\n            </div>\r\n\r\n      </div> -->\r\n      <div class=\"col-md-3 col-sm-3\">\r\n            <div *ngIf=\"errorFile\" class=\"errorLabel\"><mark>Error : {{errorFile}}</mark></div>\r\n      </div>\r\n      <!-- <input class=\"choose-file\" type=\"file\" (change)=\"onFileSelected($event)\" accept=\".sql,.csv,.xlsx\" style=\"color: white\"/>\r\n            <button class=\"btn rounded-btn\" (click)=\"onUpload()\" >Upload</button>\r\n            <button class=\"btn rounded-btn-danger\" (click)=\"cancelThis()\" >Cancel</button> -->\r\n      <br><br>\r\n      <div *ngIf=\"Evouchers&&evoucherDetail==false&&!errorMessage\">\r\n            <app-form-builder-table [table_data]=\"Evouchers\" [searchCallback]=\"[service,'searchEvoucherSummaryReport',this]\"\r\n                  [tableFormat]=\"tableFormat\" [total_page]=\"totalPage\">\r\n\r\n            </app-form-builder-table>\r\n      </div>\r\n      <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n            <div class=\"text-message\">\r\n                  <i class=\"fas fa-ban\"></i>\r\n                  {{errorMessage}}\r\n            </div>\r\n      </div>\r\n      <div *ngIf=\"evoucherDetail\">\r\n            <!-- <app-evoucher-detail [back]=\"[this, backToHere]\" [detail]=\"evoucherDetail\"></app-evoucher-detail> -->\r\n            <app-evoucher-detail></app-evoucher-detail>\r\n\r\n      </div>\r\n\r\n\r\n</div>\r\n\r\n<div id=\"myModal\" class=\"modal {{popUpForm}}\">\r\n      <!-- Modal content -->\r\n      <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                  <span class=\"close\" (click)=\"popUpClose()\">&times;</span>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                  <p>How many vouchers will be disabled?</p>\r\n                  <p>Inactive Stock : {{data.qty_inactive}} | Available Stock : {{data.qty_available}}</p>\r\n                  <input id=\"inputan\" type=\"number\" [(ngModel)]=\"inputedVoucher\">\r\n                  <br/>\r\n                  <button class=\"btn btn-primary\" (click)=\"approval(data)\">Active Stock</button>\r\n                  <button class=\"btn btn-secondary\" (click)=\"disabled(data)\" style=\"margin-left: 20px;\">Inactive Stock</button>\r\n                  \r\n            </div>\r\n            <br/>\r\n            <div class=\"modal-body\">\r\n                  <h3> How To Use?</h3>\r\n                  <p> Jika ingin mengcancel stock voucher, silahkan input angka dan tekan button \"<strong>Inactive\r\n                          Stock \"</strong></p>\r\n                  <p>Jika ingin mengaktifkan stock voucher yang tersisa, silahkan input angka tekan button\r\n                      \"<strong>Active Stock\"</strong></p>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                  <button class=\"btn btn-color\" (click)=\"approveAllClicked(data)\">Activate All Vouchers</button>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n\r\n            </div>\r\n      </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product.evoucher/generate/evoucher.generate.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product.evoucher/generate/evoucher.generate.component.html ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n    <app-page-header [heading]=\"'Generate E-Voucher'\" [icon]=\"'fa-table'\"></app-page-header>\r\n    \r\n   <div>\r\n      <form #form=\"ngForm\" (ngSubmit)=\"formSubmitAddEvoucher(form.value)\">\r\n        <div class=\"col-md-6\">\r\n            <br>\r\n  \r\n            <br>\r\n            <label>Product Code</label>\r\n            <br>\r\n            <input type=\"text\" name=\"product_code\" [placeholder]=\"'product code'\" [(ngModel)]=\"merchant_group\" (change)=\"selected()\" (keyup)=\"getUserIdsFirstWay($event)\" id=\"userIdFirstWay\" class=\"input-style\" list=\"dynmicUserIds\" (change)=\"selected()\">\r\n            <datalist id=\"dynmicUserIds\">\r\n               <option *ngFor=\"let item of product_code_data\" [value]=\"item.value\">{{item.label}}</option>\r\n            </datalist>\r\n  \r\n            <br>\r\n            <!-- <label>Product Code</label>\r\n            <form-select name=\"product_code\" [(ngModel)]=\"selectedLevel\" (change)=\"selected()\" [data]=\"product_code_data\" required>\r\n            </form-select> -->\r\n            <br>\r\n            <label>Expired Date</label>\r\n            <br>\r\n            <input class=\"input-style\" [type]=\"'date'\" name=\"expiry_date\" [placeholder]=\"'expired date'\" [(ngModel)]=\"expiry_date\" required>\r\n          \r\n            <br>\r\n            <br>\r\n  \r\n            <label>Quantity</label>\r\n            <br>\r\n            <input class=\"input-style\" name=\"qty\" [placeholder]=\"'Quantity'\"  [type]=\"'number'\" [(ngModel)]=\"qty\" required>\r\n            <br>\r\n            <br>\r\n            <button class=\"btn rounded-btn\" type=\"submit\" [disabled]=\"form.pristine\" [routerLinkActive]=\"['router-link-active']\">Generate</button>\r\n            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>\r\n        </div>\r\n        </form>\r\n   </div>\r\n   \r\n  </div>\r\n  \r\n   "

/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component.scss":
/*!**************************************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component.scss ***!
  \**************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form {\n  padding: 25px;\n  position: relative;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9zZXBhcmF0ZWQtbW9kdWxlcy9tZXJjaGFudC1wcm9kdWN0cy9wcm9kdWN0cy9ldm91Y2hlci1saXN0L2V2b3VjaGVyLWRldGFpbC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1lcmNoYW50LXBvcnRhbFxcc2VwYXJhdGVkLW1vZHVsZXNcXG1lcmNoYW50LXByb2R1Y3RzXFxwcm9kdWN0c1xcZXZvdWNoZXItbGlzdFxcZXZvdWNoZXItZGV0YWlsXFxldm91Y2hlci1kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvc2VwYXJhdGVkLW1vZHVsZXMvbWVyY2hhbnQtcHJvZHVjdHMvcHJvZHVjdHMvZXZvdWNoZXItbGlzdC9ldm91Y2hlci1kZXRhaWwvZXZvdWNoZXItZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3NlcGFyYXRlZC1tb2R1bGVzL21lcmNoYW50LXByb2R1Y3RzL3Byb2R1Y3RzL2V2b3VjaGVyLWxpc3QvZXZvdWNoZXItZGV0YWlsL2V2b3VjaGVyLWRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3JtIHtcclxuICAgIHBhZGRpbmc6IDI1cHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn0iLCIuZm9ybSB7XG4gIHBhZGRpbmc6IDI1cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component.ts":
/*!************************************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component.ts ***!
  \************************************************************************************************************************************************/
/*! exports provided: EvoucherDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherDetailComponent", function() { return EvoucherDetailComponent; });
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _object_interface_common_object__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../object-interface/common.object */ "./src/app/object-interface/common.object.ts");
/* harmony import */ var _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../../services/observerable/permission-observer */ "./src/app/services/observerable/permission-observer.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var EvoucherDetailComponent = /** @class */ (function () {
    // form: any = {
    //   merchant_username: '',
    //   product_code: '',
    //   expiry_date: ''
    // }
    function EvoucherDetailComponent(EVoucherService, router, route, permission) {
        var _this = this;
        this.EVoucherService = EVoucherService;
        this.router = router;
        this.route = route;
        this.permission = permission;
        this.permission.currentPermission.subscribe(function (val) {
            _this.currentPermission = val;
        });
        this.service = this.EVoucherService;
        if (this.currentPermission == 'merchant') {
            this.tableFormat = Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_3__["evoucherListDetail"])(this);
            this.tableFormat.label_headers.splice(0, 1);
            this.tableFormat.formOptions.addForm = false;
            this.tableFormat.formOptions.customButtons;
            this.tableFormat.show_checkbox_options = true;
        }
        else {
            this.tableFormat = Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_3__["evoucherListDetail"])(this);
            this.tableFormat.label_headers.splice(0, 1);
            this.tableFormat.formOptions.addForm = false;
            this.tableFormat.formOptions.customButtons;
            this.tableFormat.show_checkbox_options = true;
        }
    }
    EvoucherDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EvoucherDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                    var searchObject, result;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                searchObject = {
                                    search: {
                                        ev_product_code: params.ev_prod_code,
                                        // expiry_date : params.exp_d,
                                        merchant_username: params.m_username
                                    }
                                };
                                return [4 /*yield*/, this.EVoucherService.searchEvouchersLint(searchObject)];
                            case 1:
                                result = _a.sent();
                                this.evoucherDetail = result.result.values;
                                this.detail = result.result.values._id;
                                this.totalPage = result.result.total_page;
                                return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    EvoucherDetailComponent.prototype.evoucherStatus = function (listedData) {
        return __awaiter(this, void 0, void 0, function () {
            var listOfData;
            return __generator(this, function (_a) {
                if (listedData == undefined || listedData.length == 0) {
                    return [2 /*return*/, false];
                }
                listOfData = [];
                listedData.forEach(function (element, index) {
                    listOfData.push(element._id);
                });
                listedData = [];
                if (listOfData.length == 0)
                    return [2 /*return*/, false];
                try {
                    //Don't know the if it's needed an API or not
                }
                catch (error) {
                    alert("error");
                }
                return [2 /*return*/];
            });
        });
    };
    EvoucherDetailComponent.prototype.approval = function (listedData) {
        return __awaiter(this, void 0, void 0, function () {
            var listOfData, params, result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (listedData == undefined || listedData.length == 0) {
                            return [2 /*return*/, false];
                        }
                        listOfData = [];
                        listedData.forEach(function (element, index) {
                            listOfData.push(element._id);
                        });
                        listedData = [];
                        if (listOfData.length == 0)
                            return [2 /*return*/, false];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        params = {
                            voucher_id: listOfData,
                            method: 1
                        };
                        this.service = this.EVoucherService;
                        return [4 /*yield*/, this.EVoucherService.approvedEvoucher(params)];
                    case 2:
                        result = _a.sent();
                        this.firstLoad();
                        alert("the Current Data Below has been approved");
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        alert("error");
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    EvoucherDetailComponent.ctorParameters = function () { return [
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_0__["EVoucherService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_4__["PermissionObserver"] }
    ]; };
    EvoucherDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-evoucher-detail',
            template: __webpack_require__(/*! raw-loader!./evoucher-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component.html"),
            styles: [__webpack_require__(/*! ./evoucher-detail.component.scss */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_0__["EVoucherService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_4__["PermissionObserver"]])
    ], EvoucherDetailComponent);
    return EvoucherDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product.evoucher/evoucher-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/layout/modules/product.evoucher/evoucher-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: EvoucherRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherRoutingModule", function() { return EvoucherRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _evoucher_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./evoucher.component */ "./src/app/layout/modules/product.evoucher/evoucher.component.ts");
/* harmony import */ var _generate_evoucher_generate_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./generate/evoucher.generate.component */ "./src/app/layout/modules/product.evoucher/generate/evoucher.generate.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '', component: _evoucher_component__WEBPACK_IMPORTED_MODULE_2__["EvoucherComponent"]
    },
    {
        path: 'generate', component: _generate_evoucher_generate_component__WEBPACK_IMPORTED_MODULE_3__["EvoucherGenerateComponent"]
    },
    {
    // path:'detail', component: EvoucherDetailComponent
    }
];
var EvoucherRoutingModule = /** @class */ (function () {
    function EvoucherRoutingModule() {
    }
    EvoucherRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], EvoucherRoutingModule);
    return EvoucherRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/product.evoucher/evoucher.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/layout/modules/product.evoucher/evoucher.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n.pb-framer .clr-white {\n  color: white;\n}\nh1 {\n  color: white;\n}\nmark {\n  background-color: #dc3545;\n  color: white;\n}\n.rounded-btn {\n  border-radius: 30px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 15px;\n  background: #56a4ff;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n.rounded-btn-danger {\n  border-radius: 30px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 15px;\n  background: #dc3545;\n}\n.rounded-btn-danger:hover {\n  background: #a71d2a;\n  outline: none;\n}\n@media screen and (max-width: 992px) {\n  .rounded-btn {\n    border-radius: 30px;\n    color: white;\n    font-size: 15px;\n    line-height: 30px;\n    padding: 0 12px;\n    background: #56a4ff;\n    margin-left: 25%;\n    margin-top: 10px;\n    margin-right: 10px;\n  }\n\n  .rounded-btn:hover {\n    background: #0a7bff;\n    outline: none;\n  }\n\n  .rounded-btn-danger {\n    border-radius: 30px;\n    color: white;\n    font-size: 15px;\n    line-height: 30px;\n    padding: 0 12px;\n    background: #dc3545;\n    margin-right: 25%;\n    margin-top: 10px;\n  }\n\n  .rounded-btn-danger:hover {\n    background: #a71d2a;\n    outline: none;\n  }\n}\n.modal.false {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n.modal.true {\n  display: block;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n/* Modal Header */\n.modal-header {\n  padding: 2px 16px;\n  background-color: #5cb85c;\n  color: white;\n}\n/* Modal Body */\n.modal-body {\n  padding: 2px 16px;\n}\n/* Modal Footer */\n.modal-footer {\n  padding: 2px 16px;\n  background-color: #5cb85c;\n  color: white;\n}\n/* Modal Content */\n.modal-content {\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  padding: 0;\n  border: 1px solid #888;\n  width: 50%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n/* The Close Button */\n.close {\n  color: white;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n.close:hover,\n.close:focus {\n  color: #000;\n  text-decoration: none;\n  cursor: pointer;\n}\n/* Add Animation */\n@-webkit-keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n@keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n.btn-color {\n  color: white;\n  width: auto;\n  padding: 5px 12px;\n  height: 50px;\n  border-radius: 10px;\n  background: limegreen;\n}\n#inputan {\n  margin-bottom: 10px;\n  width: 80%;\n  position: center;\n}\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 12px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC5ldm91Y2hlci9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXHByb2R1Y3QuZXZvdWNoZXJcXGV2b3VjaGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0LmV2b3VjaGVyL2V2b3VjaGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDQ0Y7QURBRTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDRUo7QURBRTtFQUNFLHFCQUFBO0FDRUo7QURBRTtFQUNFLFlBQUE7QUNFSjtBREVBO0VBQ0UsWUFBQTtBQ0NGO0FERUE7RUFDRSx5QkFBQTtFQUNBLFlBQUE7QUNDRjtBREVBO0VBRUUsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDQ0Y7QURDQTtFQUNFLG1CQUFBO0VBRUEsYUFBQTtBQ0NGO0FERUE7RUFFRSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNDRjtBRENBO0VBQ0UsbUJBQUE7RUFFQSxhQUFBO0FDQ0Y7QURFQTtFQUNFO0lBRUUsbUJBQUE7SUFDQSxZQUFBO0lBQ0EsZUFBQTtJQUNBLGlCQUFBO0lBQ0EsZUFBQTtJQUNBLG1CQUFBO0lBQ0EsZ0JBQUE7SUFDQSxnQkFBQTtJQUNBLGtCQUFBO0VDQ0Y7O0VEQ0E7SUFDRSxtQkFBQTtJQUVBLGFBQUE7RUNDRjs7RURFQTtJQUVFLG1CQUFBO0lBQ0EsWUFBQTtJQUNBLGVBQUE7SUFDQSxpQkFBQTtJQUNBLGVBQUE7SUFDQSxtQkFBQTtJQUNBLGlCQUFBO0lBQ0EsZ0JBQUE7RUNDRjs7RURDQTtJQUNFLG1CQUFBO0lBRUEsYUFBQTtFQ0NGO0FBQ0Y7QURFQTtFQUNFLGFBQUE7RUFBZSxzQkFBQTtFQUNmLGVBQUE7RUFBaUIsa0JBQUE7RUFDakIsVUFBQTtFQUFZLGVBQUE7RUFDWixrQkFBQTtFQUFvQix3QkFBQTtFQUNwQixPQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFBYSxlQUFBO0VBQ2IsWUFBQTtFQUFjLGdCQUFBO0VBQ2QsY0FBQTtFQUFnQiw0QkFBQTtFQUNoQix1QkFBQTtFQUE4QixtQkFBQTtFQUM5QixvQ0FBQTtFQUFtQyxxQkFBQTtBQ1NyQztBRE5BO0VBQ0UsY0FBQTtFQUFnQixzQkFBQTtFQUNoQixlQUFBO0VBQWlCLGtCQUFBO0VBQ2pCLFVBQUE7RUFBWSxlQUFBO0VBQ1osa0JBQUE7RUFBb0Isd0JBQUE7RUFDcEIsT0FBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQWEsZUFBQTtFQUNiLFlBQUE7RUFBYyxnQkFBQTtFQUNkLGNBQUE7RUFBZ0IsNEJBQUE7RUFDaEIsdUJBQUE7RUFBOEIsbUJBQUE7RUFDOUIsb0NBQUE7RUFBbUMscUJBQUE7QUNrQnJDO0FEZEEsaUJBQUE7QUFDQTtFQUNFLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FDaUJGO0FEZEEsZUFBQTtBQUNBO0VBQWEsaUJBQUE7QUNrQmI7QURoQkEsaUJBQUE7QUFDQTtFQUNFLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FDbUJGO0FEaEJBLGtCQUFBO0FBQ0E7RUFDRSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxzQkFBQTtFQUNBLFVBQUE7RUFDQSw0RUFBQTtFQUNBLGtDQUFBO1VBQUEsMEJBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0Esa0JBQUE7QUNtQkY7QURoQkEscUJBQUE7QUFDQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDbUJGO0FEaEJBOztFQUVFLFdBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7QUNtQkY7QURmQSxrQkFBQTtBQUNBO0VBQ0U7SUFBTSxXQUFBO0lBQWEsVUFBQTtFQ29CbkI7RURuQkE7SUFBSSxNQUFBO0lBQVEsVUFBQTtFQ3VCWjtBQUNGO0FEMUJBO0VBQ0U7SUFBTSxXQUFBO0lBQWEsVUFBQTtFQ29CbkI7RURuQkE7SUFBSSxNQUFBO0lBQVEsVUFBQTtFQ3VCWjtBQUNGO0FEckJBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0FDdUJGO0FEcEJBO0VBQ0csbUJBQUE7RUFDQSxVQUFBO0VBQ0QsZ0JBQUE7QUN1QkY7QURwQkE7RUFDRSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ3VCRjtBRHJCRTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUN1Qk4iLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0LmV2b3VjaGVyL2V2b3VjaGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBiLWZyYW1lciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBoZWlnaHQ6IDI1cHg7XHJcbiAgY29sb3I6ICNlNjdlMjI7XHJcbiAgLnByb2dyZXNzYmFyIHtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgei1pbmRleDogLTE7XHJcbiAgfVxyXG4gIC50ZXJtaW5hdGVkLnByb2dyZXNzYmFyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcclxuICB9XHJcbiAgLmNsci13aGl0ZSB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG59XHJcblxyXG5oMSB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG5tYXJrIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGMzNTQ1O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLnJvdW5kZWQtYnRuIHtcclxuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gIHBhZGRpbmc6IDAgMTVweDtcclxuICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG59XHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgYmFja2dyb3VuZDogZGFya2VuKCM1NmE0ZmYsIDE1JSk7XHJcbiAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxuICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4ucm91bmRlZC1idG4tZGFuZ2VyIHtcclxuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gIHBhZGRpbmc6IDAgMTVweDtcclxuICBiYWNrZ3JvdW5kOiAjZGMzNTQ1O1xyXG59XHJcbi5yb3VuZGVkLWJ0bi1kYW5nZXI6aG92ZXIge1xyXG4gIGJhY2tncm91bmQ6IGRhcmtlbigjZGMzNTQ1LCAxNSUpO1xyXG4gIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAucm91bmRlZC1idG4ge1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICAgcGFkZGluZzogMCAxMnB4O1xyXG4gICAgYmFja2dyb3VuZDogIzU2YTRmZjtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNSU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gIH1cclxuICAucm91bmRlZC1idG46aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZDogZGFya2VuKCM1NmE0ZmYsIDE1JSk7XHJcbiAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICB9XHJcblxyXG4gIC5yb3VuZGVkLWJ0bi1kYW5nZXIge1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICAgcGFkZGluZzogMCAxMnB4O1xyXG4gICAgYmFja2dyb3VuZDogI2RjMzU0NTtcclxuICAgIG1hcmdpbi1yaWdodDogMjUlO1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICB9XHJcbiAgLnJvdW5kZWQtYnRuLWRhbmdlcjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oI2RjMzU0NSwgMTUlKTtcclxuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gIH1cclxufVxyXG5cclxuLm1vZGFsLmZhbHNlIHtcclxuICBkaXNwbGF5OiBub25lOyAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xyXG4gIHBvc2l0aW9uOiBmaXhlZDsgLyogU3RheSBpbiBwbGFjZSAqL1xyXG4gIHotaW5kZXg6IDE7IC8qIFNpdCBvbiB0b3AgKi9cclxuICBwYWRkaW5nLXRvcDogMTAwcHg7IC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cclxuICBsZWZ0OiAwO1xyXG4gIHRvcDogMDtcclxuICB3aWR0aDogMTAwJTsgLyogRnVsbCB3aWR0aCAqL1xyXG4gIGhlaWdodDogMTAwJTsgLyogRnVsbCBoZWlnaHQgKi9cclxuICBvdmVyZmxvdzogYXV0bzsgLyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMCwwLDApOyAvKiBGYWxsYmFjayBjb2xvciAqL1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC40KTsgLyogQmxhY2sgdy8gb3BhY2l0eSAqL1xyXG59XHJcblxyXG4ubW9kYWwudHJ1ZXtcclxuICBkaXNwbGF5OiBibG9jazsgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cclxuICBwb3NpdGlvbjogZml4ZWQ7IC8qIFN0YXkgaW4gcGxhY2UgKi9cclxuICB6LWluZGV4OiAxOyAvKiBTaXQgb24gdG9wICovXHJcbiAgcGFkZGluZy10b3A6IDEwMHB4OyAvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXHJcbiAgbGVmdDogMDtcclxuICB0b3A6IDA7XHJcbiAgd2lkdGg6IDEwMCU7IC8qIEZ1bGwgd2lkdGggKi9cclxuICBoZWlnaHQ6IDEwMCU7IC8qIEZ1bGwgaGVpZ2h0ICovXHJcbiAgb3ZlcmZsb3c6IGF1dG87IC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDAsMCwwKTsgLyogRmFsbGJhY2sgY29sb3IgKi9cclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNCk7IC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cclxufVxyXG5cclxuXHJcbi8qIE1vZGFsIEhlYWRlciAqL1xyXG4ubW9kYWwtaGVhZGVyIHtcclxuICBwYWRkaW5nOiAycHggMTZweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWNiODVjO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLyogTW9kYWwgQm9keSAqL1xyXG4ubW9kYWwtYm9keSB7cGFkZGluZzogMnB4IDE2cHg7fVxyXG5cclxuLyogTW9kYWwgRm9vdGVyICovXHJcbi5tb2RhbC1mb290ZXIge1xyXG4gIHBhZGRpbmc6IDJweCAxNnB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM1Y2I4NWM7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4vKiBNb2RhbCBDb250ZW50ICovXHJcbi5tb2RhbC1jb250ZW50IHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcclxuICBtYXJnaW46IGF1dG87XHJcbiAgcGFkZGluZzogMDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLDAsMCwwLjIpLDAgNnB4IDIwcHggMCByZ2JhKDAsMCwwLDAuMTkpO1xyXG4gIGFuaW1hdGlvbi1uYW1lOiBhbmltYXRldG9wO1xyXG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcclxuICB0ZXh0LWFsaWduOmNlbnRlcjtcclxufVxyXG5cclxuLyogVGhlIENsb3NlIEJ1dHRvbiAqL1xyXG4uY2xvc2Uge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBmbG9hdDogcmlnaHQ7XHJcbiAgZm9udC1zaXplOiAyOHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4uY2xvc2U6aG92ZXIsXHJcbi5jbG9zZTpmb2N1cyB7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuXHJcbi8qIEFkZCBBbmltYXRpb24gKi9cclxuQGtleWZyYW1lcyBhbmltYXRldG9wIHtcclxuICBmcm9tIHt0b3A6IC0zMDBweDsgb3BhY2l0eTogMH1cclxuICB0byB7dG9wOiAwOyBvcGFjaXR5OiAxfVxyXG59XHJcblxyXG4uYnRuLWNvbG9ye1xyXG4gIGNvbG9yOndoaXRlO1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIHBhZGRpbmc6IDVweCAxMnB4O1xyXG4gIGhlaWdodDogNTBweDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGJhY2tncm91bmQ6IGxpbWVncmVlbjtcclxufVxyXG5cclxuI2lucHV0YW57XHJcbiAgIG1hcmdpbi1ib3R0b206MTBweDtcclxuICAgd2lkdGg6IDgwJTtcclxuICBwb3NpdGlvbjpjZW50ZXIgXHJcbn1cclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBoZWlnaHQ6MTAwdmg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZToyNnB4O1xyXG4gIHBhZGRpbmctdG9wOiAxMnB4O1xyXG5cclxuICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgYmFja2dyb3VuZDogI0U3NEMzQztcclxuICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgfVxyXG59IiwiLnBiLWZyYW1lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDI1cHg7XG4gIGNvbG9yOiAjZTY3ZTIyO1xufVxuLnBiLWZyYW1lciAucHJvZ3Jlc3NiYXIge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjVweDtcbiAgei1pbmRleDogLTE7XG59XG4ucGItZnJhbWVyIC50ZXJtaW5hdGVkLnByb2dyZXNzYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xufVxuLnBiLWZyYW1lciAuY2xyLXdoaXRlIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG5oMSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxubWFyayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkYzM1NDU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnJvdW5kZWQtYnRuIHtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDAgMTVweDtcbiAgYmFja2dyb3VuZDogIzU2YTRmZjtcbn1cblxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzBhN2JmZjtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuLnJvdW5kZWQtYnRuLWRhbmdlciB7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDE1cHg7XG4gIGJhY2tncm91bmQ6ICNkYzM1NDU7XG59XG5cbi5yb3VuZGVkLWJ0bi1kYW5nZXI6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjYTcxZDJhO1xuICBvdXRsaW5lOiBub25lO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xuICAucm91bmRlZC1idG4ge1xuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgcGFkZGluZzogMCAxMnB4O1xuICAgIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG4gICAgbWFyZ2luLWxlZnQ6IDI1JTtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgfVxuXG4gIC5yb3VuZGVkLWJ0bjpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogIzBhN2JmZjtcbiAgICBvdXRsaW5lOiBub25lO1xuICB9XG5cbiAgLnJvdW5kZWQtYnRuLWRhbmdlciB7XG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICBwYWRkaW5nOiAwIDEycHg7XG4gICAgYmFja2dyb3VuZDogI2RjMzU0NTtcbiAgICBtYXJnaW4tcmlnaHQ6IDI1JTtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICB9XG5cbiAgLnJvdW5kZWQtYnRuLWRhbmdlcjpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogI2E3MWQyYTtcbiAgICBvdXRsaW5lOiBub25lO1xuICB9XG59XG4ubW9kYWwuZmFsc2Uge1xuICBkaXNwbGF5OiBub25lO1xuICAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgei1pbmRleDogMTtcbiAgLyogU2l0IG9uIHRvcCAqL1xuICBwYWRkaW5nLXRvcDogMTAwcHg7XG4gIC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgLyogRnVsbCB3aWR0aCAqL1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIEZ1bGwgaGVpZ2h0ICovXG4gIG92ZXJmbG93OiBhdXRvO1xuICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG59XG5cbi5tb2RhbC50cnVlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xuICB6LWluZGV4OiAxO1xuICAvKiBTaXQgb24gdG9wICovXG4gIHBhZGRpbmctdG9wOiAxMDBweDtcbiAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xuICBsZWZ0OiAwO1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICAvKiBGdWxsIHdpZHRoICovXG4gIGhlaWdodDogMTAwJTtcbiAgLyogRnVsbCBoZWlnaHQgKi9cbiAgb3ZlcmZsb3c6IGF1dG87XG4gIC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICAvKiBGYWxsYmFjayBjb2xvciAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cbn1cblxuLyogTW9kYWwgSGVhZGVyICovXG4ubW9kYWwtaGVhZGVyIHtcbiAgcGFkZGluZzogMnB4IDE2cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1Y2I4NWM7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLyogTW9kYWwgQm9keSAqL1xuLm1vZGFsLWJvZHkge1xuICBwYWRkaW5nOiAycHggMTZweDtcbn1cblxuLyogTW9kYWwgRm9vdGVyICovXG4ubW9kYWwtZm9vdGVyIHtcbiAgcGFkZGluZzogMnB4IDE2cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1Y2I4NWM7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLyogTW9kYWwgQ29udGVudCAqL1xuLm1vZGFsLWNvbnRlbnQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XG4gIG1hcmdpbjogYXV0bztcbiAgcGFkZGluZzogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzg4ODtcbiAgd2lkdGg6IDUwJTtcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vKiBUaGUgQ2xvc2UgQnV0dG9uICovXG4uY2xvc2Uge1xuICBjb2xvcjogd2hpdGU7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAyOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmNsb3NlOmhvdmVyLFxuLmNsb3NlOmZvY3VzIHtcbiAgY29sb3I6ICMwMDA7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4vKiBBZGQgQW5pbWF0aW9uICovXG5Aa2V5ZnJhbWVzIGFuaW1hdGV0b3Age1xuICBmcm9tIHtcbiAgICB0b3A6IC0zMDBweDtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG4gIHRvIHtcbiAgICB0b3A6IDA7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufVxuLmJ0bi1jb2xvciB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmc6IDVweCAxMnB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJhY2tncm91bmQ6IGxpbWVncmVlbjtcbn1cblxuI2lucHV0YW4ge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICB3aWR0aDogODAlO1xuICBwb3NpdGlvbjogY2VudGVyO1xufVxuXG4uZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEycHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/product.evoucher/evoucher.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/product.evoucher/evoucher.component.ts ***!
  \***********************************************************************/
/*! exports provided: EvoucherComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherComponent", function() { return EvoucherComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var EvoucherComponent = /** @class */ (function () {
    function EvoucherComponent(evoucherService, productService, router, route) {
        this.evoucherService = evoucherService;
        this.productService = productService;
        this.router = router;
        this.route = route;
        this.Evouchers = [];
        this.tableFormat = {
            title: 'E-Voucher Summary Report',
            label_headers: [
                // {label: 'Product Code', visible: true, type: 'string', data_row_name: 'ev_product_code'},
                { label: 'Voucher Name', visible: true, type: 'string', data_row_name: 'product_name' },
                { label: 'Voucher Code', visible: true, type: 'string', data_row_name: 'product_code' },
                { label: 'Expiry Date', visible: true, type: 'date', data_row_name: 'expiry_date' },
                { label: 'Available Stock', visible: true, type: 'string', data_row_name: 'qty_available' },
                { label: 'Bought by User', visible: true, type: 'string', data_row_name: 'qty_sold' },
                { label: 'Used by User', visible: true, type: 'string', data_row_name: 'qty_used' },
                { label: 'Inactive Stock', visible: true, type: 'string', data_row_name: 'qty_inactive' },
                { label: 'Expired Stock', visible: true, type: 'string', data_row_name: 'qty_expired' },
            ],
            row_primary_key: '_id',
            formOptions: {
                addFormGenerate: false,
                row_id: '_id',
                this: this,
                result_var_name: 'Evouchers',
                detail_function: [this, 'callDetail'],
            },
            show_checkbox_options: true
        };
        this.form_input = {};
        this.errorLabel = true;
        this.errorMessage = false;
        this.popUpForm = false;
        this.data = [];
        this.evoucherDetail = false;
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
        this.inputedVoucher = 0;
        this.approveAll = false;
    }
    EvoucherComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    //
    EvoucherComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        console.log("test");
                        this.service = this.evoucherService;
                        return [4 /*yield*/, this.evoucherService.getEvoucherSummaryReport()];
                    case 1:
                        result = _a.sent();
                        this.totalPage = result.total_page;
                        this.Evouchers = result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log("kena catch");
                        this.errorLabel = (e_1.message);
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // // Get the button that opens the modal
    // var btn = document.getElementById("myBtn");
    // // Get the <span> element that closes the modal
    // var span = document.getElementsByClassName("close")[0];
    EvoucherComponent.prototype.popUpClose = function () {
        this.popUpForm = false;
    };
    // When the user clicks anywhere outside of the modal, close it
    // window.onclick = function (event) {
    //   if (event.target == modal) {
    //     modal.style.display = "none";
    //   }
    // }
    EvoucherComponent.prototype.openDetail = function (evoucher) {
        // let ev_prod = this.Evouchers.ev_product_code
        this.data = evoucher;
        console.log("result", evoucher);
        this.popUpForm = true;
        // let expiry_date = this.expiry_date
    };
    EvoucherComponent.prototype.callDetail = function (_id, evoucherData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    EvoucherComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.evoucherDetail = false;
                return [2 /*return*/];
            });
        });
    };
    EvoucherComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000;
        // let say 3Mb
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = 'Maximum File is 3 MB';
            }
        });
        this.selectedFile = event.target.files[0];
    };
    EvoucherComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    EvoucherComponent.prototype.cancelThis = function () {
        // this.cancel = !this.cancel;
        this.cancel = true;
    };
    EvoucherComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.evoucherService.upload(this.selectedFile, this)];
                    case 1:
                        _a.sent();
                        alert("File Has Been Uploaded Successfully!");
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message);
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    EvoucherComponent.prototype.approveAllClicked = function (data) {
        this.approveAll = true;
        this.approval(data);
    };
    EvoucherComponent.prototype.approval = function (evoucherData) {
        return __awaiter(this, void 0, void 0, function () {
            var totalVoucher, expiry_date, month, day, stringDate, payloadData, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(evoucherData);
                        if (evoucherData == undefined) {
                            return [2 /*return*/, false];
                        }
                        expiry_date = new Date(evoucherData.expiry_date);
                        month = (expiry_date.getMonth() + 1).toString().padStart(2, "0");
                        day = (expiry_date.getDate() + 0).toString().padStart(2, "0");
                        stringDate = expiry_date.getFullYear() + '-' + month + '-' + day;
                        if (this.inputedVoucher != 0 && this.approveAll == false) {
                            totalVoucher = this.inputedVoucher;
                        }
                        else if (this.approveAll == true) {
                            this.approveAll = false;
                            totalVoucher = evoucherData.qty_inactive;
                        }
                        payloadData = {
                            product_code: evoucherData.ev_product_code,
                            expiry_date: stringDate,
                            qty: totalVoucher,
                            method: 2
                        };
                        this.service = this.evoucherService;
                        return [4 /*yield*/, this.evoucherService.approvedEvoucher(payloadData)];
                    case 1:
                        result = _a.sent();
                        if (result.result.status == 'success') {
                            alert("the Current Data Below has been approved");
                            this.firstLoad();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    EvoucherComponent.prototype.disabled = function (evoucherData) {
        return __awaiter(this, void 0, void 0, function () {
            var totalVoucher, expiry_date, month, day, stringDate, payloadData, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(evoucherData);
                        if (evoucherData == undefined) {
                            return [2 /*return*/, false];
                        }
                        expiry_date = new Date(evoucherData.expiry_date);
                        month = (expiry_date.getMonth() + 1).toString().padStart(2, "0");
                        day = (expiry_date.getDate() + 0).toString().padStart(2, "0");
                        stringDate = expiry_date.getFullYear() + '-' + month + '-' + day;
                        if (this.inputedVoucher != 0) {
                            totalVoucher = this.inputedVoucher;
                        }
                        else if (this.inputedVoucher == 0) {
                            totalVoucher = evoucherData.qty_inactive;
                        }
                        console.log(this.inputedVoucher);
                        payloadData = {
                            product_code: evoucherData.ev_product_code,
                            expiry_date: stringDate,
                            qty: totalVoucher,
                            method: 1
                        };
                        console.log("payload", payloadData);
                        this.service = this.evoucherService;
                        return [4 /*yield*/, this.evoucherService.approvedEvoucher(payloadData)];
                    case 1:
                        result = _a.sent();
                        if (result.result.status == 'success') {
                            alert("The stock has been inactive");
                            this.firstLoad();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    EvoucherComponent.ctorParameters = function () { return [
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_3__["EVoucherService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
    ]; };
    EvoucherComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-evoucher',
            template: __webpack_require__(/*! raw-loader!./evoucher.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product.evoucher/evoucher.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./evoucher.component.scss */ "./src/app/layout/modules/product.evoucher/evoucher.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_3__["EVoucherService"], _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], EvoucherComponent);
    return EvoucherComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product.evoucher/evoucher.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/layout/modules/product.evoucher/evoucher.module.ts ***!
  \********************************************************************/
/*! exports provided: EvoucherModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherModule", function() { return EvoucherModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _evoucher_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./evoucher-routing.module */ "./src/app/layout/modules/product.evoucher/evoucher-routing.module.ts");
/* harmony import */ var _evoucher_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./evoucher.component */ "./src/app/layout/modules/product.evoucher/evoucher.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _generate_evoucher_generate_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./generate/evoucher.generate.component */ "./src/app/layout/modules/product.evoucher/generate/evoucher.generate.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _merchant_portal_separated_modules_merchant_products_products_evoucher_list_evoucher_detail_evoucher_detail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component.ts");
/* harmony import */ var angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! angular-ng-autocomplete */ "./node_modules/angular-ng-autocomplete/fesm5/angular-ng-autocomplete.js");
/* harmony import */ var _merchant_portal_separated_modules_merchant_products_products_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../merchant-portal/separated-modules/merchant-products/products.module */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










// import { NgxEditorModule } from 'ngx-editor';


var EvoucherModule = /** @class */ (function () {
    function EvoucherModule() {
    }
    EvoucherModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _evoucher_routing_module__WEBPACK_IMPORTED_MODULE_2__["EvoucherRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_10__["AutocompleteLibModule"],
                // NgxEditorModule,
                _merchant_portal_separated_modules_merchant_products_products_module__WEBPACK_IMPORTED_MODULE_11__["ProductsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"],
            ],
            declarations: [
                _evoucher_component__WEBPACK_IMPORTED_MODULE_3__["EvoucherComponent"],
                _generate_evoucher_generate_component__WEBPACK_IMPORTED_MODULE_7__["EvoucherGenerateComponent"],
                _merchant_portal_separated_modules_merchant_products_products_evoucher_list_evoucher_detail_evoucher_detail_component__WEBPACK_IMPORTED_MODULE_9__["EvoucherDetailComponent"]
            ]
        })
    ], EvoucherModule);
    return EvoucherModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/product.evoucher/generate/evoucher.generate.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/modules/product.evoucher/generate/evoucher.generate.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n.pb-framer .clr-white {\n  color: white;\n}\n.input-style {\n  width: 15vw;\n  height: 3vw;\n  border-radius: 15px;\n}\n#userIdFirstWay.input-style.ng-valid.ng-touched.ng-dirty {\n  padding: 6px 10px;\n  border-radius: 5px;\n}\n#userIdFirstWay.input-style.ng-pristine.ng-valid.ng-touched {\n  padding: 6px 10px;\n  border-radius: 5px;\n}\n#userIdFirstWay.input-style.ng-valid.ng-touched.ng-dirty[_ngcontent-c4] {\n  padding: 6px 10px;\n  border-radius: 5px;\n}\nlabel {\n  color: white;\n}\n.ng-pristine {\n  color: white !important;\n}\n.rounded-btn {\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n  margin-left: 40%;\n  margin-right: 50%;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n.auto-name {\n  width: 152% !important;\n}\n.ng-pristine {\n  color: black !important;\n  width: 100%;\n  border-radius: 5px;\n  padding: 6px 10px;\n}\n@media screen and (max-width: 600px) {\n  .ng-pristine {\n    color: black !important;\n    border-radius: 5px;\n    padding: 15px 20px;\n  }\n\n  #userIdFirstWay.input-style.ng-valid.ng-touched.ng-dirty {\n    padding: 15px 20px;\n    border-radius: 5px;\n  }\n\n  #userIdFirstWay.input-style.ng-pristine.ng-valid.ng-touched {\n    padding: 15px 20px;\n    border-radius: 5px;\n  }\n\n  #userIdFirstWay.input-style.ng-valid.ng-touched.ng-dirty[_ngcontent-c4] {\n    padding: 15px 20px;\n    border-radius: 5px;\n  }\n\n  .input-style {\n    padding: 15px 20px;\n    width: 100%;\n    border-radius: 5px;\n  }\n}\ninput {\n  width: 40vw;\n  height: 3vw;\n  border-radius: 5px;\n}\n.input {\n  width: 40vw;\n  height: 3vw;\n  border-radius: 5px;\n}\n.input-style[_ngcontent-c4] {\n  width: 40vw;\n  height: 3vw;\n  border-radius: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC5ldm91Y2hlci9nZW5lcmF0ZS9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXHByb2R1Y3QuZXZvdWNoZXJcXGdlbmVyYXRlXFxldm91Y2hlci5nZW5lcmF0ZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC5ldm91Y2hlci9nZW5lcmF0ZS9ldm91Y2hlci5nZW5lcmF0ZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ0NKO0FEQUk7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ0VSO0FEQUk7RUFDSSxxQkFBQTtBQ0VSO0FEQUk7RUFDSSxZQUFBO0FDRVI7QURFQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUNDSjtBREVBO0VBQ0ksaUJBQUE7RUFFQSxrQkFBQTtBQ0FKO0FER0E7RUFDSSxpQkFBQTtFQUVBLGtCQUFBO0FDREo7QURJQTtFQUNJLGlCQUFBO0VBRUEsa0JBQUE7QUNGSjtBREtBO0VBQ0ksWUFBQTtBQ0ZKO0FES0E7RUFDSSx1QkFBQTtBQ0ZKO0FES0E7RUFFSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDSixnQkFBQTtFQUNBLGlCQUFBO0FDRkE7QURJQTtFQUNJLG1CQUFBO0VBRUEsYUFBQTtBQ0ZKO0FES0E7RUFDSSxzQkFBQTtBQ0ZKO0FES0E7RUFDSSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDRko7QURLQTtFQUNJO0lBQ0ksdUJBQUE7SUFHQSxrQkFBQTtJQUNBLGtCQUFBO0VDSk47O0VET0U7SUFDSSxrQkFBQTtJQUVBLGtCQUFBO0VDTE47O0VEUUU7SUFDSSxrQkFBQTtJQUVBLGtCQUFBO0VDTk47O0VEU0U7SUFDSSxrQkFBQTtJQUVBLGtCQUFBO0VDUE47O0VEVUU7SUFDSSxrQkFBQTtJQUNBLFdBQUE7SUFDQSxrQkFBQTtFQ1BOO0FBQ0Y7QURVQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNSSjtBRFdBO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQ1JKO0FEV0E7RUFDSSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDUkoiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0LmV2b3VjaGVyL2dlbmVyYXRlL2V2b3VjaGVyLmdlbmVyYXRlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBiLWZyYW1lcntcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGhlaWdodDogMjVweDtcclxuICAgIGNvbG9yOiAjZTY3ZTIyO1xyXG4gICAgLnByb2dyZXNzYmFye1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzM0OThkYjtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgIHotaW5kZXg6IC0xO1xyXG4gICAgfVxyXG4gICAgLnRlcm1pbmF0ZWQucHJvZ3Jlc3NiYXJ7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gICAgfVxyXG4gICAgLmNsci13aGl0ZXtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5pbnB1dC1zdHlsZXtcclxuICAgIHdpZHRoOiAxNXZ3O1xyXG4gICAgaGVpZ2h0OiAzdncgO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxufVxyXG5cclxuI3VzZXJJZEZpcnN0V2F5LmlucHV0LXN0eWxlLm5nLXZhbGlkLm5nLXRvdWNoZWQubmctZGlydHl7XHJcbiAgICBwYWRkaW5nOiA2cHggMTBweDtcclxuICAgIC8vIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG59XHJcblxyXG4jdXNlcklkRmlyc3RXYXkuaW5wdXQtc3R5bGUubmctcHJpc3RpbmUubmctdmFsaWQubmctdG91Y2hlZHtcclxuICAgIHBhZGRpbmc6IDZweCAxMHB4O1xyXG4gICAgLy8gd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuXHJcbiN1c2VySWRGaXJzdFdheS5pbnB1dC1zdHlsZS5uZy12YWxpZC5uZy10b3VjaGVkLm5nLWRpcnR5W19uZ2NvbnRlbnQtYzRde1xyXG4gICAgcGFkZGluZzogNnB4IDEwcHg7XHJcbiAgICAvLyB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5cclxubGFiZWx7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5uZy1wcmlzdGluZXtcclxuICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ucm91bmRlZC1idG4ge1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgYmFja2dyb3VuZDogIzU2YTRmZjtcclxubWFyZ2luLWxlZnQ6IDQwJTtcclxubWFyZ2luLXJpZ2h0OiA1MCU7XHJcbn1cclxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQ6IGRhcmtlbigjNTZBNEZGLCAxNSUpO1xyXG4gICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbn1cclxuXHJcbi5hdXRvLW5hbWV7XHJcbiAgICB3aWR0aDogMTUyJSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubmctcHJpc3RpbmV7XHJcbiAgICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcGFkZGluZzogNnB4IDEwcHg7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XHJcbiAgICAubmctcHJpc3RpbmV7XHJcbiAgICAgICAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgLy8gd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgLy8gd2lkdGg6IDQwdnc7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHggMjBweDtcclxuICAgIH1cclxuXHJcbiAgICAjdXNlcklkRmlyc3RXYXkuaW5wdXQtc3R5bGUubmctdmFsaWQubmctdG91Y2hlZC5uZy1kaXJ0eXtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4IDIwcHg7XHJcbiAgICAgICAgLy8gd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAjdXNlcklkRmlyc3RXYXkuaW5wdXQtc3R5bGUubmctcHJpc3RpbmUubmctdmFsaWQubmctdG91Y2hlZHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4IDIwcHg7XHJcbiAgICAgICAgLy8gd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAjdXNlcklkRmlyc3RXYXkuaW5wdXQtc3R5bGUubmctdmFsaWQubmctdG91Y2hlZC5uZy1kaXJ0eVtfbmdjb250ZW50LWM0XXtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4IDIwcHg7XHJcbiAgICAgICAgLy8gd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5pbnB1dC1zdHlsZXtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4IDIwcHg7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG5pbnB1dHtcclxuICAgIHdpZHRoOiA0MHZ3O1xyXG4gICAgaGVpZ2h0OiAzdnc7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuXHJcbi5pbnB1dHtcclxuICAgIHdpZHRoOiA0MHZ3O1xyXG4gICAgaGVpZ2h0OiAzdnc7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuXHJcbi5pbnB1dC1zdHlsZVtfbmdjb250ZW50LWM0XSB7XHJcbiAgICB3aWR0aDogNDB2dztcclxuICAgIGhlaWdodDogM3Z3O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG59IiwiLnBiLWZyYW1lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDI1cHg7XG4gIGNvbG9yOiAjZTY3ZTIyO1xufVxuLnBiLWZyYW1lciAucHJvZ3Jlc3NiYXIge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjVweDtcbiAgei1pbmRleDogLTE7XG59XG4ucGItZnJhbWVyIC50ZXJtaW5hdGVkLnByb2dyZXNzYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xufVxuLnBiLWZyYW1lciAuY2xyLXdoaXRlIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uaW5wdXQtc3R5bGUge1xuICB3aWR0aDogMTV2dztcbiAgaGVpZ2h0OiAzdnc7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG59XG5cbiN1c2VySWRGaXJzdFdheS5pbnB1dC1zdHlsZS5uZy12YWxpZC5uZy10b3VjaGVkLm5nLWRpcnR5IHtcbiAgcGFkZGluZzogNnB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuI3VzZXJJZEZpcnN0V2F5LmlucHV0LXN0eWxlLm5nLXByaXN0aW5lLm5nLXZhbGlkLm5nLXRvdWNoZWQge1xuICBwYWRkaW5nOiA2cHggMTBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4jdXNlcklkRmlyc3RXYXkuaW5wdXQtc3R5bGUubmctdmFsaWQubmctdG91Y2hlZC5uZy1kaXJ0eVtfbmdjb250ZW50LWM0XSB7XG4gIHBhZGRpbmc6IDZweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbmxhYmVsIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4ubmctcHJpc3RpbmUge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuLnJvdW5kZWQtYnRuIHtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiA3MHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDAgNDBweDtcbiAgYmFja2dyb3VuZDogIzU2YTRmZjtcbiAgbWFyZ2luLWxlZnQ6IDQwJTtcbiAgbWFyZ2luLXJpZ2h0OiA1MCU7XG59XG5cbi5yb3VuZGVkLWJ0bjpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMwYTdiZmY7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbi5hdXRvLW5hbWUge1xuICB3aWR0aDogMTUyJSAhaW1wb3J0YW50O1xufVxuXG4ubmctcHJpc3RpbmUge1xuICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZzogNnB4IDEwcHg7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC5uZy1wcmlzdGluZSB7XG4gICAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBhZGRpbmc6IDE1cHggMjBweDtcbiAgfVxuXG4gICN1c2VySWRGaXJzdFdheS5pbnB1dC1zdHlsZS5uZy12YWxpZC5uZy10b3VjaGVkLm5nLWRpcnR5IHtcbiAgICBwYWRkaW5nOiAxNXB4IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICB9XG5cbiAgI3VzZXJJZEZpcnN0V2F5LmlucHV0LXN0eWxlLm5nLXByaXN0aW5lLm5nLXZhbGlkLm5nLXRvdWNoZWQge1xuICAgIHBhZGRpbmc6IDE1cHggMjBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIH1cblxuICAjdXNlcklkRmlyc3RXYXkuaW5wdXQtc3R5bGUubmctdmFsaWQubmctdG91Y2hlZC5uZy1kaXJ0eVtfbmdjb250ZW50LWM0XSB7XG4gICAgcGFkZGluZzogMTVweCAyMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgfVxuXG4gIC5pbnB1dC1zdHlsZSB7XG4gICAgcGFkZGluZzogMTVweCAyMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgfVxufVxuaW5wdXQge1xuICB3aWR0aDogNDB2dztcbiAgaGVpZ2h0OiAzdnc7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLmlucHV0IHtcbiAgd2lkdGg6IDQwdnc7XG4gIGhlaWdodDogM3Z3O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5pbnB1dC1zdHlsZVtfbmdjb250ZW50LWM0XSB7XG4gIHdpZHRoOiA0MHZ3O1xuICBoZWlnaHQ6IDN2dztcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/product.evoucher/generate/evoucher.generate.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/product.evoucher/generate/evoucher.generate.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: EvoucherGenerateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherGenerateComponent", function() { return EvoucherGenerateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





// import {Observable} from 'rxjs/Observable'
var EvoucherGenerateComponent = /** @class */ (function () {
    function EvoucherGenerateComponent(evoucherService) {
        this.evoucherService = evoucherService;
        this.name = "";
        this.categoryProduct = [];
        this.Evouchers = [];
        this.merchant_group = [];
        this.keyname = [];
        this.product_code = [];
        this.product_name = [];
        this.merch_username = [];
        this.userList1 = [];
        this.lastkeydown1 = 0;
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
        this.errorLabel = false;
        this.category = false;
    }
    EvoucherGenerateComponent.prototype.selected = function () {
        console.log(this.selectedLevel);
    };
    EvoucherGenerateComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EvoucherGenerateComponent.prototype.getKeyname = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.evoucherService.config()];
                    case 1:
                        _a.keyname = _b.sent();
                        this.keyname.result.merchant_group.forEach(function (element) {
                            _this.merchant_group.push({ label: element, value: element });
                        });
                        this.merchant_group = this.merchant_group[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    EvoucherGenerateComponent.prototype.getCategory = function () {
        var _this = this;
        this.category.result.forEach(function (element) {
            _this.categoryProduct.push({ label: element.name, value: element.name });
        });
    };
    EvoucherGenerateComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var getProductsData, productStringListOfName, products, dataDropdown, _a, params, result;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.evoucherService.getDropdown()];
                    case 1:
                        getProductsData = _b.sent();
                        this.rowOfTable = getProductsData.result.values;
                        productStringListOfName = getProductsData.result;
                        products = getProductsData.result;
                        this.currentValue = productStringListOfName[0].product_name;
                        productStringListOfName.forEach(function (obj, index) {
                            _this.product_code.push(obj.product_code);
                            // this.product_name.push(obj.product_name)
                            // this.merch_username.push(obj.merchant_username)
                        });
                        dataDropdown = [];
                        products.forEach(function (data, index) {
                            dataDropdown.push({
                                label: data['product_code'] + " - " + data['product_name'] + " - " + data['merchant_username'],
                                value: data['product_code'],
                                selected: 0
                            });
                        });
                        this.product_code_data = dataDropdown;
                        this.product_code_data[0].selected = 1;
                        _a = this;
                        return [4 /*yield*/, this.evoucherService.getCategoryLint()];
                    case 2:
                        _a.category = _b.sent();
                        this.getCategory();
                        params = {
                            'usage': 'generate'
                        };
                        return [2 /*return*/];
                }
            });
        });
    };
    EvoucherGenerateComponent.prototype.formSubmitAddEvoucher = function (form) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        // form.expiry_date = '2019-01-01';
                        form.qty = Number(form.qty);
                        this.service = this.evoucherService;
                        return [4 /*yield*/, this.evoucherService.generateEVoucherLint(form)];
                    case 1:
                        result = _a.sent();
                        // console.log(form.expiry_date);
                        this.Evouchers = result.result;
                        alert("Generate Success");
                        window.location.href = "/evoucheradmin";
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EvoucherGenerateComponent.prototype.getUserIdsFirstWay = function ($event) {
        var userId = document.getElementById('userIdFirstWay').value;
        this.userList1 = [];
        this.otherData = this.product_code + ' - ' + this.product_name + ' - ' + this.merch_username;
        // console.log("here", this.otherData);
        if (userId.length > 0 || userId.length == 0) {
            if ($event.timeStamp - this.lastkeydown1 > 200) {
                this.userList1 = this.searchFromArray(this.otherData, userId);
            }
        }
    };
    EvoucherGenerateComponent.prototype.searchFromArray = function (arr, regex) {
        var matches = [], i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i].match(regex)) {
                matches.push(arr[i]);
            }
        }
        return matches;
    };
    ;
    EvoucherGenerateComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        try {
            this.errorFile = false;
            var fileMaxSize_1 = 3000000; // let say 3Mb
            Array.from(event.target.files).forEach(function (file) {
                if (file.size > fileMaxSize_1) {
                    _this.errorFile = "Maximum File Upload is 3 MB";
                }
            });
            this.selectedFile = event.target.files[0];
        }
        catch (e) {
            this.errorLabel = (e.message); //conversion to Error type
            var message = this.errorLabel;
            if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
            }
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                icon: 'error',
                title: message,
            });
        }
    };
    EvoucherGenerateComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    EvoucherGenerateComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    EvoucherGenerateComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.evoucherService.upload(this.selectedFile, this)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    EvoucherGenerateComponent.ctorParameters = function () { return [
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__["EVoucherService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], EvoucherGenerateComponent.prototype, "products", void 0);
    EvoucherGenerateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-evoucher-add',
            template: __webpack_require__(/*! raw-loader!./evoucher.generate.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product.evoucher/generate/evoucher.generate.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./evoucher.generate.component.scss */ "./src/app/layout/modules/product.evoucher/generate/evoucher.generate.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__["EVoucherService"]])
    ], EvoucherGenerateComponent);
    return EvoucherGenerateComponent;
}());



/***/ })

}]);
//# sourceMappingURL=default~merchant-portal-merchant-portal-module~modules-product-evoucher-evoucher-module.js.map