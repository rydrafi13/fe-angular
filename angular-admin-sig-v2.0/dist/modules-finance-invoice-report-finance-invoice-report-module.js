(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-finance-invoice-report-finance-invoice-report-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/finance-invoice-report/detail/finance-invoice-report.detail.component.html":
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/finance-invoice-report/detail/finance-invoice-report.detail.component.html ***!
  \*************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit && !isAddDocument\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <!-- <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button> -->\r\n        <div *ngIf=\"!mci_project\" class=\"edit_button\">\r\n            <button class=\"btn btn-right\" (click)=\"addDocument()\"><i class=\"fa fa-fw fa-file-image\"></i>Other Document Photos</button>\r\n            <!-- <button class=\"btn btn-right\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button> -->\r\n        </div>\r\n    </div>\r\n\r\n    <ng-template #empty_image><span>\r\n        empty\r\n    </span></ng-template>\r\n  \r\n    <div class=\"card-content\" *ngIf=\"!errorMessage\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\" *ngIf=\"!mci_project && sig_kontraktual\"><h2>Data Pelanggan</h2></div>\r\n                        <div class=\"card-header\" *ngIf=\"!mci_project && !sig_kontraktual\"><h2>Data Pemilik dan Toko</h2></div>\r\n                        <div class=\"card-header\" *ngIf=\"mci_project\"><h2>Data Pelanggan</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <label>Order ID</label>\r\n                                <div name=\"order_id\">{{detail.order_id}}</div>\r\n                                          \r\n                                <label>ID Pelanggan</label>\r\n                                <div name=\"member_id\">{{invoiceDetail.id_toko}}</div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                    <label>Nama Program</label>\r\n                                    <div name=\"full_name\">{{invoiceDetail.nama_toko}}</div>\r\n                                </div>\r\n                                \r\n                                <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                    <label>Nama Toko</label>\r\n                                    <div name=\"full_name\">{{invoiceDetail.nama_toko}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                    <label>Nama Entitas</label>\r\n                                    <div name=\"full_name\">{{invoiceDetail.nama_toko}}</div>\r\n                                </div>\r\n                                \r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Nama Distributor</label>\r\n                                    <div name=\"full_name\">{{invoiceDetail.member_detail.nama_distributor}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                    <label>Alamat Toko</label>\r\n                                    <div name=\"full_name\">{{invoiceDetail.member_detail.alamat_toko}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                    <label>Alamat Toko</label>\r\n                                    <div name=\"full_name\">{{invoiceDetail.member_detail.alamat_toko}}</div>\r\n                                </div>\r\n\r\n                                <label *ngIf=\"!mci_project\">Nama Pemilik</label>\r\n                                <label *ngIf=\"mci_project\">Nama Pelanggan</label>\r\n                                <div name=\"full_name\">{{invoiceDetail.member_detail.nama_pemilik}}</div>\r\n\r\n                                <label *ngIf=\"!mci_project\">No. WA Pemilik</label>\r\n                                <label *ngIf=\"mci_project\">No. WA Pelanggan</label>\r\n                                <div name=\"full_name\">{{invoiceDetail.member_detail.no_wa_pemilik}}</div>\r\n                                \r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>No. KTP Pemilik</label>\r\n                                    <div name=\"full_name\">{{invoiceDetail.member_detail.ktp_pemilik}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Foto KTP Pemilik</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container  *ngIf=\"validURL(invoiceDetail.member_detail.foto_ktp_pemilik); else empty_image\">\r\n                                            <a href={{invoiceDetail.member_detail.foto_ktp_pemilik}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(invoiceDetail.member_detail.foto_ktp_pemilik) == 'image'\" src={{invoiceDetail.member_detail.foto_ktp_pemilik}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(invoiceDetail.member_detail.foto_ktp_pemilik) == 'document'\"\r\n                                                    [src]=\"invoiceDetail.member_detail.foto_ktp_pemilik\"\r\n                                                    [render-text]=\"true\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                                <!-- <img src={{invoiceDetail.member_detail.foto_ktp_pemilik}} class=\"member-detail-image\" /> -->\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(invoiceDetail.member_detail.foto_ktp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                        \r\n                                        \r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>No. NPWP Pemilik</label>\r\n                                    <div name=\"full_name\">{{invoiceDetail.member_detail.npwp_pemilik}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Foto NPWP Pemilik</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container  *ngIf=\"validURL(invoiceDetail.member_detail.foto_npwp_pemilik); else empty_image\">\r\n                                            <a href={{invoiceDetail.member_detail.foto_npwp_pemilik}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(invoiceDetail.member_detail.foto_npwp_pemilik) == 'image'\" src={{invoiceDetail.member_detail.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(invoiceDetail.member_detail.foto_npwp_pemilik) == 'document'\"\r\n                                                    [src]=\"invoiceDetail.member_detail.foto_npwp_pemilik\"\r\n                                                    [render-text]=\"true\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                                <!-- <img src={{invoiceDetail.member_detail.foto_npwp_pemilik}} class=\"member-detail-image\" /> -->\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(invoiceDetail.member_detail.foto_npwp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n\r\n                            </div>\r\n                        \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Penerima </h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Hadiah Dikuasakan</label>\r\n                                    <div name=\"email\">{{invoiceDetail.member_detail.hadiah_dikuasakan}}</div>\r\n                                </div>\r\n\r\n                                <label>Nama Penerima</label>\r\n                                <div name=\"email\">{{invoiceDetail.member_detail.nama_penerima_gopay}}</div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                    <label>Jabatan</label>\r\n                                    <div name=\"jabatan\">{{invoiceDetail.member_detail.cluster}}</div>\r\n                                </div>\r\n\r\n                                <label>No. WA Penerima</label>\r\n                                <div name=\"email\">{{invoiceDetail.member_detail.no_wa_penerima}}</div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                    <label>No. Gopay Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo Gopay, isi dengan No. Gopay Penerima)</div></label>\r\n                                    <div name=\"email\">{{invoiceDetail.member_detail.telp_penerima_gopay}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                    <label>No. E-Wallet Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo E-Wallet, isi dengan No. E-Wallet Penerima)</div></label>\r\n                                    <div name=\"email\">{{invoiceDetail.member_detail.telp_penerima_gopay}}</div>\r\n                                </div>\r\n\r\n                               <label>Alamat Pengiriman</label>\r\n                               <div name=\"email\">{{invoiceDetail.member_detail.alamat_rumah}}</div>\r\n\r\n                               <div *ngIf=\"!mci_project\">\r\n                                    <label>No. KTP Penerima</label>\r\n                                    <div name=\"email\">{{invoiceDetail.member_detail.ktp_penerima}}</div>\r\n                               </div>\r\n\r\n                               <div *ngIf=\"!mci_project\">\r\n                                    <label>Foto KTP Penerima</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container *ngIf=\"validURL(invoiceDetail.member_detail.foto_ktp_penerima); else empty_image\">\r\n                                            <a href={{invoiceDetail.member_detail.foto_ktp_penerima}} target=\"_blank\" >\r\n                                                <img *ngIf=\"checkFileType(invoiceDetail.member_detail.foto_ktp_penerima) == 'image'\" src={{invoiceDetail.member_detail.foto_ktp_penerima}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(invoiceDetail.member_detail.foto_ktp_penerima) == 'document'\"\r\n                                                    [src]=\"invoiceDetail.member_detail.foto_ktp_penerima\"\r\n                                                    [render-text]=\"true\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                            <!-- <img src={{invoiceDetail.member_detail.foto_ktp_penerima}} class=\"member-detail-image\" /> -->\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(invoiceDetail.member_detail.foto_ktp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>No. NPWP Penerima</label>\r\n                                    <div name=\"email\">{{invoiceDetail.member_detail.npwp_penerima}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Foto NPWP Penerima</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container *ngIf=\"validURL(invoiceDetail.member_detail.foto_npwp_penerima); else empty_image\">\r\n                                            <a href={{invoiceDetail.member_detail.foto_npwp_penerima}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(invoiceDetail.member_detail.foto_npwp_penerima) == 'image'\" src={{invoiceDetail.member_detail.foto_npwp_penerima}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(invoiceDetail.member_detail.foto_npwp_penerima) == 'document'\"\r\n                                                    [src]=\"invoiceDetail.member_detail.foto_npwp_penerima\"\r\n                                                    [render-text]=\"true\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                                <!-- <img src={{invoiceDetail.member_detail.foto_npwp_penerima}} class=\"member-detail-image\" /> -->\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(invoiceDetail.member_detail.foto_npwp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                         </div>\r\n                    </div>\r\n                    \r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Dokumen Lainnya </h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <label>AWB Number</label>\r\n                                <div class=\"member-detail-image-container\">\r\n                                    <ng-container *ngIf=\"invoiceDetail.delivery_detail && invoiceDetail.delivery_detail; else empty_image\">\r\n                                        <span>{{invoiceDetail.delivery_detail.awb_number}}</span>\r\n                                    </ng-container>\r\n                                </div>\r\n\r\n                                <label>No. Invoice</label>\r\n                                <div class=\"member-detail-image-container\">\r\n                                    <ng-container *ngIf=\"invoiceDetail.invoice_no && invoiceDetail.invoice_no.value; else empty_image\">\r\n                                        <span>{{invoiceDetail.invoice_no.value}}</span>\r\n                                    </ng-container>\r\n                                </div>\r\n\r\n                                <label>Tanggal Invoice</label>\r\n                                <div class=\"member-detail-image-container\">\r\n                                    <ng-container *ngIf=\"invoiceDetail.invoice_no && invoiceDetail.invoice_no.created_date; else empty_image\">\r\n                                        <span>{{invoiceDetail.invoice_no.created_date}}</span>\r\n                                    </ng-container>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Foto BAST</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container *ngIf=\"validURL(invoiceDetail.additional_info && invoiceDetail.additional_info.bast && invoiceDetail.additional_info.bast.pic_file_name); else empty_image\">\r\n                                            <a href={{invoiceDetail.additional_info.bast.pic_file_name}} target=\"_blank\" >\r\n                                                <img *ngIf=\"checkFileType(invoiceDetail.additional_info.bast.pic_file_name) == 'image'\" src={{invoiceDetail.additional_info.bast.pic_file_name}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(invoiceDetail.additional_info.bast.pic_file_name) == 'document'\"\r\n                                                    [src]=\"invoiceDetail.additional_info.bast.pic_file_name\"\r\n                                                    [render-text]=\"true\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                            \r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(invoiceDetail.additional_info.bast.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Foto Surat Kuasa</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container *ngIf=\"validURL(invoiceDetail.additional_info && invoiceDetail.additional_info.surat_kuasa && invoiceDetail.additional_info.surat_kuasa.pic_file_name); else empty_image\">\r\n                                            <a href={{invoiceDetail.additional_info.surat_kuasa.pic_file_name}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(invoiceDetail.additional_info.surat_kuasa.pic_file_name) == 'image'\" src={{invoiceDetail.additional_info.surat_kuasa.pic_file_name}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(invoiceDetail.additional_info.surat_kuasa.pic_file_name) == 'document'\"\r\n                                                    [src]=\"invoiceDetail.additional_info.surat_kuasa.pic_file_name\"\r\n                                                    [render-text]=\"true\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                                <!-- <img src={{invoiceDetail.additional_info.surat_kuasa.pic_file_name}} class=\"member-detail-image\" /> -->\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(invoiceDetail.additional_info.surat_kuasa.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                         </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n          \r\n            \r\n         </div>\r\n    </div>\r\n    <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n        <div class=\"text-message\">\r\n            <i class=\"fas fa-ban\"></i>\r\n            {{errorMessage}}\r\n        </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-finance-invoice-report-edit [back]=\"[this, 'editThis']\" [detail]=\"invoiceDetail\"></app-finance-invoice-report-edit>\r\n</div>\r\n\r\n<div *ngIf=\"isAddDocument\">\r\n    <app-list-order [back]=\"[this, 'addDocument']\" [detail]=\"invoiceDetail\"></app-list-order>\r\n</div>\r\n\r\n<!-- <div *ngIf=\"isAddDocument\">\r\n    <app-additional-document [back]=\"[this, 'addDocument']\" [detail]=\"invoiceDetail\"></app-additional-document>\r\n</div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/finance-invoice-report/edit/finance-invoice-report.edit.component.html":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/finance-invoice-report/edit/finance-invoice-report.edit.component.html ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <input #ktpPemilik type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'ktpPemilik')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\"/>\r\n    <input #npwpPemilik type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'npwpPemilik')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\"/>\r\n    <input #ktpPenerima type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'ktpPenerima')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\"/>\r\n    <input #npwpPenerima type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'npwpPenerima')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\"/>\r\n\r\n    <ng-container *ngIf=\"allBast.length > 0\">\r\n        <ng-container *ngFor=\"let item of allBast; let i = index\">\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'bast', i)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg\" style=\"display:none;\" [id]=\"'bast'+i\"/>\r\n        </ng-container>\r\n    </ng-container>\r\n\r\n    <ng-container *ngIf=\"allSuratKuasa.length > 0\">\r\n        <ng-container *ngFor=\"let item of allSuratKuasa; let i = index\">\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'suratKuasa', i)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg\" style=\"display:none;\" [id]=\"'suratKuasa'+i\"/>\r\n        </ng-container>\r\n    </ng-container>\r\n    \r\n    \r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"updateInvoiceNumber()\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span></button>\r\n    </div>\r\n\r\n    <ng-template #empty_image><span>\r\n        empty\r\n    </span></ng-template>\r\n\r\n    <ng-template #empty_invoice>\r\n        <form-input name=\"invoice_no\" [type]=\"'text'\" [placeholder]=\"'No. invoice'\"  [(ngModel)]=\"invoiceNumber\" autofocus required></form-input>\r\n    </ng-template>\r\n\r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Pemilik</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <label>ID Pelanggan</label>\r\n                                <form-input  name=\"id_pel\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'ID Pelanggan'\"  [(ngModel)]=\"updateSingle.id_pel\" autofocus required></form-input>\r\n                \r\n                                <label>Nama Toko</label>\r\n                                <form-input name=\"nama_toko\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Nama Toko'\"  [(ngModel)]=\"updateSingle.nama_toko\" autofocus required></form-input>\r\n\r\n                                <label>Nama Distributor</label>\r\n                                <form-input name=\"nama_distributor\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Nama Distributor'\"  [(ngModel)]=\"updateSingle.nama_distributor\" autofocus required></form-input>\r\n                                \r\n                                <label>Alamat Toko</label>\r\n                                <form-input name=\"alamat_toko\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Alamat Toko'\"  [(ngModel)]=\"updateSingle.alamat_toko\" autofocus required></form-input>\r\n                                \r\n                                <label>Nama Pemilik</label>\r\n                                <form-input name=\"nama_pemilik\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Nama Pemilik'\"  [(ngModel)]=\"updateSingle.nama_pemilik\" autofocus required></form-input>\r\n\r\n                                <label>No. WA Pemilik</label>\r\n                                <form-input name=\"no_wa_pemilik\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'No WA Pemilik'\"  [(ngModel)]=\"updateSingle.no_wa_pemilik\" autofocus required></form-input>\r\n\r\n                                <label>No. KTP Pemilik</label>\r\n                                <form-input name=\"ktp_pemilik\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'No. KTP Pemilik'\"  [(ngModel)]=\"updateSingle.ktp_pemilik\" autofocus required></form-input>\r\n\r\n                                <label>Foto KTP Pemilik</label>\r\n                                <!-- <form-input name=\"foto_ktp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'KTP Pemilik'\"  [(ngModel)]=\"updateSingle.foto_ktp_pemilik\" autofocus required></form-input> -->\r\n                                <div class=\"member-detail-image-container\">\r\n                                    <ng-container  *ngIf=\"validURL(updateSingle.foto_ktp_pemilik); else empty_image\">\r\n                                        <a href={{updateSingle.foto_ktp_pemilik}} target=\"_blank\">\r\n                                            <img src={{updateSingle.foto_ktp_pemilik}} class=\"member-detail-image\" />\r\n                                        </a>\r\n                                        <div class=\"btn-download-container\">\r\n                                            <button (click)=\"downloadImage(updateSingle.foto_ktp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                </div>\r\n\r\n                                <label>No. NPWP Pemilik</label>\r\n                                <form-input name=\"npwp_pemilik\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'No. NPWP Pemilik'\"  [(ngModel)]=\"updateSingle.npwp_pemilik\" autofocus required></form-input>\r\n                                \r\n                                <label>Foto NPWP Pemilik</label>\r\n                                <!-- <form-input name=\"foto_npwp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'NPWP Pemilik'\"  [(ngModel)]=\"updateSingle.foto_npwp_pemilik\" autofocus required></form-input> -->\r\n                                <div class=\"member-detail-image-container\">\r\n                                    <ng-container  *ngIf=\"validURL(updateSingle.foto_npwp_pemilik); else empty_image\">\r\n                                        <a href={{updateSingle.foto_npwp_pemilik}} target=\"_blank\">\r\n                                            <img src={{updateSingle.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                        </a>\r\n                                        <div class=\"btn-download-container\">\r\n                                            <button (click)=\"downloadImage(updateSingle.foto_npwp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                </div>\r\n\r\n                            </div>\r\n                        \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Penerima</h2> </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <label>Hadiah Dikuasakan</label>\r\n                                <form-input name=\"hadiah_dikuasakan\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Hadiah Dikuasakan'\"  [(ngModel)]=\"updateSingle.hadiah_dikuasakan\" autofocus required></form-input>\r\n\r\n                                <label>Nama Penerima</label>\r\n                                <form-input name=\"nama_penerima_gopay\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Nama Penerima'\"  [(ngModel)]=\"updateSingle.nama_penerima_gopay\" autofocus required></form-input>\r\n\r\n                                <label>No. WA Penerima</label>\r\n                                <form-input name=\"no_wa_penerima\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'No. WA Penerima'\"  [(ngModel)]=\"updateSingle.no_wa_penerima\" autofocus required></form-input>\r\n\r\n                                <label>No. Gopay Penerima</label>\r\n                                <form-input name=\"telp_penerima_gopay\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'No Gopay Penerima'\"  [(ngModel)]=\"updateSingle.telp_penerima_gopay\" autofocus required></form-input>\r\n\r\n                                <label>Alamat Pengiriman</label>\r\n                                <form-input name=\"alamat_rumah\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Alamat Pengiriman'\"  [(ngModel)]=\"updateSingle.alamat_rumah\" autofocus required></form-input>\r\n\r\n                                <label>No. KTP Penerima</label>\r\n                                <form-input name=\"ktp_penerima\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'No. KTP Penerima'\"  [(ngModel)]=\"updateSingle.ktp_penerima\" autofocus required></form-input>\r\n\r\n                                <label>Foto KTP Penerima</label>\r\n                                <!-- <form-input name=\"foto_ktp_penerima\"  [type]=\"'text'\" [placeholder]=\"'Nama Toko'\"  [(ngModel)]=\"updateSingle.foto_ktp_penerima\" autofocus required></form-input> -->\r\n\r\n                                <div class=\"member-detail-image-container\">\r\n                                    <ng-container *ngIf=\"validURL(updateSingle.foto_ktp_penerima); else empty_image\">\r\n                                        <a href={{updateSingle.foto_ktp_penerima}} target=\"_blank\" >\r\n                                           <img src={{updateSingle.foto_ktp_penerima}} class=\"member-detail-image\" />\r\n                                        </a>\r\n                                        <div class=\"btn-download-container\">\r\n                                            <button (click)=\"downloadImage(updateSingle.foto_ktp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                </div>\r\n\r\n                                <label>No. NPWP Penerima</label>\r\n                                <form-input name=\"npwp_penerima\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'No. NPWP Penerima'\"  [(ngModel)]=\"updateSingle.npwp_penerima\" autofocus required></form-input>\r\n\r\n                                <label>Foto NPWP Penerima</label>\r\n                                <!-- <form-input name=\"foto_npwp_penerima\"  [type]=\"'text'\" [placeholder]=\"'Nama Toko'\"  [(ngModel)]=\"updateSingle.foto_npwp_penerima\" autofocus required></form-input> -->\r\n\r\n                                <div class=\"member-detail-image-container\">\r\n                                    <ng-container *ngIf=\"validURL(updateSingle.foto_npwp_penerima); else empty_image\">\r\n                                        <a href={{updateSingle.foto_npwp_penerima}} target=\"_blank\">\r\n                                           <img src={{updateSingle.foto_npwp_penerima}} class=\"member-detail-image\" />\r\n                                        </a>\r\n                                        <div class=\"btn-download-container\">\r\n                                            <button (click)=\"downloadImage(updateSingle.foto_npwp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                 </div>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Dokumen Lainnya </h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <label>No. Invoice</label>\r\n                                <ng-container>\r\n                                    <form-input *ngIf=\"detail.additional_info && detail.additional_info.invoice_no; else empty_invoice\" name=\"invoice_no\" [type]=\"'text'\" [placeholder]=\"'No. invoice'\"  [(ngModel)]=\"invoiceNumber\" autofocus required></form-input>\r\n                                </ng-container>\r\n\r\n                               <label>Foto BAST</label>\r\n                                <div class=\"member-detail-image-container\">\r\n                                    <ng-container *ngIf=\"validURL(detail.additional_info && detail.additional_info.bast && detail.additional_info.bast.pic_file_name); else empty_image\">\r\n                                        <a href={{detail.additional_info.bast.pic_file_name}} target=\"_blank\" >\r\n                                           <img src={{detail.additional_info.bast.pic_file_name}} class=\"member-detail-image\" />\r\n                                        </a>\r\n                                        <div class=\"btn-download-container\">\r\n                                            <button (click)=\"downloadImage(detail.additional_info.bast.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                </div>\r\n\r\n                               <label>Foto Surat Kuasa</label>\r\n                               <div class=\"member-detail-image-container\">\r\n                                   <ng-container *ngIf=\"validURL(detail.additional_info && detail.additional_info.surat_kuasa && detail.additional_info.surat_kuasa.pic_file_name); else empty_image\">\r\n                                       <a href={{detail.additional_info.surat_kuasa.pic_file_name}} target=\"_blank\">\r\n                                          <img src={{detail.additional_info.surat_kuasa.pic_file_name}} class=\"member-detail-image\" />\r\n                                       </a>\r\n                                       <div class=\"btn-download-container\">\r\n                                           <button (click)=\"downloadImage(detail.additional_info.surat_kuasa.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                       </div>\r\n                                   </ng-container>\r\n                                </div>\r\n                            </div>\r\n                         </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    <ng-template #upload_image>\r\n        <span>\r\n            upload\r\n        </span>\r\n    </ng-template>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/finance-invoice-report/finance-invoice-report.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/finance-invoice-report/finance-invoice-report.component.html ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n      <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n      <div *ngIf=\"SalesRedemption && invoiceDetail==false && !errorMessage && mci_project && programType!='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, 'searchCompleteFinanceInvoiceReport',this]\"\r\n            [tableFormat]=\"tableFormat2\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n      \r\n            </app-form-builder-table>\r\n      </div>\r\n      <div *ngIf=\"SalesRedemption && invoiceDetail==false && !errorMessage && !mci_project && programType!='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, 'searchFinanceInvoiceReport',this]\"\r\n            [tableFormat]=\"tableFormat\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n      \r\n            </app-form-builder-table>\r\n      </div>\r\n      <div *ngIf=\"SalesRedemption && invoiceDetail==false && !errorMessage && !mci_project && programType=='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, 'searchFinanceInvoiceReport',this]\"\r\n            [tableFormat]=\"tableFormat3\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n      \r\n            </app-form-builder-table>\r\n      </div>\r\n      <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n            <div class=\"text-message\">\r\n            <i class=\"fas fa-ban\"></i>\r\n            {{errorMessage}}\r\n            </div>\r\n      </div>\r\n      <div *ngIf=\"invoiceDetail\">\r\n            <app-finance-invoice-report-detail [back]=\"[this,backToHere]\" [detail]=\"invoiceDetail\"></app-finance-invoice-report-detail>\r\n      </div>\r\n  \r\n    </div>\r\n  "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/finance-invoice-report/list-order/list-order.component.html":
/*!**********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/finance-invoice-report/list-order/list-order.component.html ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit\">\r\n    <div class=\"card card-detail\">\r\n        <div class=\"card-header\">\r\n            <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n            <!-- <h1>ID Pelanggan : {{memberDetail.username}}</h1> -->\r\n            <!-- <div class=\"edit_button\">\r\n                <button class=\"btn btn-right\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i>edit</button>\r\n            </div> -->\r\n        </div>\r\n        <div class=\"card-content\" *ngIf=\"!errorMessage\">\r\n            <div class=\"member-detail\">\r\n                <div class=\"row\">\r\n                <div class=\"col-md-8 col-sm-12\">\r\n                    <div class=\"row\">\r\n\r\n                    <div class=\"col-sm-12\" *ngFor=\"let order of listOrder\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Order ID : {{order.order_id}}</h2>\r\n                                <!-- <div class=\"edit_button\">\r\n                                    <button class=\"btn btn-right\" (click)=\"editThis(order)\"><i class=\"fa fa-fw fa-pencil-alt\"></i>edit</button>\r\n                                </div> -->\r\n                            </div>\r\n                            <div class=\"card-content\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-6 col-sm-12\">             \r\n                                    <label class=\"label-bold\">Foto BAST</label>\r\n                                    <ng-container *ngIf=\"order.additional_info; else empty_image\">\r\n\r\n                                        <div class=\"member-detail-image-container\" *ngIf=\"order.additional_info.bast && order.additional_info.bast.pic_file_name; else empty_image\">\r\n                                            <ng-container>\r\n                                                <a href={{order.additional_info.bast.pic_file_name}} target=\"_blank\">\r\n                                                    <img *ngIf=\"checkFileType(order.additional_info.bast.pic_file_name) == 'image'\" src={{order.additional_info.bast.pic_file_name}} class=\"member-detail-image\" />\r\n                                                    <pdf-viewer \r\n                                                        *ngIf=\"checkFileType(order.additional_info.bast.pic_file_name) == 'document'\"\r\n                                                        [src]=\"order.additional_info.bast.pic_file_name\"\r\n                                                        [render-text]=\"true\"\r\n                                                        [original-size]=\"false\"\r\n                                                        [page] = \"1\"\r\n                                                        [show-all]=\"false\"\r\n                                                        style=\"width: 100%; height: 100%\">\r\n                                                    </pdf-viewer>\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(order.additional_info.bast.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{order.additional_info.bast.created_date}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n    \r\n                                                <br/>\r\n                                                <!-- <div>\r\n                                                    <span><b>Description :</b></span>\r\n                                                    <span>\r\n                                                        {{itemImage.desc}}\r\n                                                    </span>\r\n                                                </div> -->\r\n    \r\n                                            </ng-container>\r\n                                        </div>\r\n\r\n                                        <!-- <div class=\"member-detail-image-container\" *ngIf=\"order.additional_info.surat_kuasa && order.additional_info.surat_kuasa.pic_file_name\">\r\n                                            <ng-container>\r\n                                                <a href={{order.additional_info.surat_kuasa.pic_file_name}} target=\"_blank\">\r\n                                                    <img src={{order.additional_info.surat_kuasa.pic_file_name}} class=\"member-detail-image\" />\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(order.additional_info.surat_kuasa.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{order.additional_info.surat_kuasa.created_date}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n    \r\n                                                <br/>\r\n    \r\n                                            </ng-container>\r\n                                        </div> -->\r\n\r\n                                        \r\n                                    </ng-container>\r\n    \r\n                                    \r\n                                </div>\r\n\r\n\r\n                                <div class=\"col-md-6 col-sm-12\">             \r\n                                    <label class=\"label-bold\">Foto Surat Kuasa</label>\r\n                                    <ng-container *ngIf=\"order.additional_info; else empty_image\">\r\n\r\n                                        <!-- <div class=\"member-detail-image-container\" *ngIf=\"order.additional_info.bast && order.additional_info.bast.pic_file_name\">\r\n                                            <ng-container>\r\n                                                <a href={{order.additional_info.bast.pic_file_name}} target=\"_blank\">\r\n                                                    <img src={{order.additional_info.bast.pic_file_name}} class=\"member-detail-image\" />\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(order.additional_info.bast.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{order.additional_info.bast.created_date}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n    \r\n                                                <br/>\r\n    \r\n                                            </ng-container>\r\n                                        </div> -->\r\n\r\n                                        <div class=\"member-detail-image-container\" *ngIf=\"order.additional_info.surat_kuasa && order.additional_info.surat_kuasa.pic_file_name; else empty_image\">\r\n                                            <ng-container>\r\n                                                <a href={{order.additional_info.surat_kuasa.pic_file_name}} target=\"_blank\">\r\n                                                    <img *ngIf=\"checkFileType(order.additional_info.surat_kuasa.pic_file_name) == 'image'\" src={{order.additional_info.surat_kuasa.pic_file_name}} class=\"member-detail-image\" />\r\n                                                    <pdf-viewer \r\n                                                        *ngIf=\"checkFileType(order.additional_info.surat_kuasa.pic_file_name) == 'document'\"\r\n                                                        [src]=\"order.additional_info.surat_kuasa.pic_file_name\"\r\n                                                        [render-text]=\"true\"\r\n                                                        [original-size]=\"false\"\r\n                                                        [page] = \"1\"\r\n                                                        [show-all]=\"false\"\r\n                                                        style=\"width: 100%; height: 100%\">\r\n                                                    </pdf-viewer>\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(order.additional_info.surat_kuasa.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{order.additional_info.surat_kuasa.created_date}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n    \r\n                                                <br/>\r\n    \r\n                                            </ng-container>\r\n                                        </div>\r\n\r\n                                        \r\n                                    </ng-container>\r\n                                </div>\r\n                            </div>\r\n                            </div>\r\n\r\n                            <div class=\"card-content\" *ngIf=\"order.additional_info && order.additional_info.others && order.additional_info.others.length > 0; else empty_others\">\r\n                                <label class=\"label-bold\">Others</label>\r\n                                <div class=\"row\">\r\n                                    <ng-container>\r\n                                        <div class=\"col-md-6 col-sm-12\" *ngFor=\"let other of order.additional_info.others\">             \r\n                                                <div class=\"member-detail-image-container\">\r\n                                                    <!-- <div class=\"img-upload\">\r\n                                                        <img src={{other.pic_file_name}} class=\"member-detail-image\" />\r\n                                                    </div>\r\n                \r\n                                                    <div style=\"text-align: center;\">\r\n                                                        <span>\r\n                                                            <b>Uploaded date : {{other.created_date}}</b>\r\n                                                        </span>\r\n                                                    </div> -->\r\n\r\n                                                    <a href={{other.pic_file_name}} target=\"_blank\">\r\n                                                        <img *ngIf=\"checkFileType(other.pic_file_name) == 'image'\" src={{other.pic_file_name}} class=\"member-detail-image\" />\r\n                                                        <pdf-viewer \r\n                                                            *ngIf=\"checkFileType(other.pic_file_name) == 'document'\"\r\n                                                            [src]=\"other.pic_file_name\"\r\n                                                            [render-text]=\"true\"\r\n                                                            [original-size]=\"false\"\r\n                                                            [page] = \"1\"\r\n                                                            [show-all]=\"false\"\r\n                                                            style=\"width: 100%; height: 100%\">\r\n                                                        </pdf-viewer>\r\n                                                    </a>\r\n                                                    <div class=\"btn-download-container\">\r\n                                                        <button (click)=\"downloadImage(other.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                    </div>\r\n                                                    <div style=\"text-align: center;\">\r\n                                                        <span>\r\n                                                            <b>Uploaded date : {{other.created_date}}</b>\r\n                                                        </span>\r\n                                                    </div>\r\n        \r\n                                                    <br/>\r\n                           \r\n                                                </div>\r\n                                            \r\n                                            </div>\r\n                                    </ng-container>\r\n                                </div>\r\n                            </div>\r\n                            <ng-template  #empty_others>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"row\">\r\n                                            <div class=\"col-md-6 col-sm-12\">             \r\n                                                <label class=\"label-bold\">Others</label>\r\n                                                    <div class=\"member-detail-image-container\">\r\n                                                        <div style=\"width:100%; text-align: center;\">\r\n                                                            empty\r\n                                                        </div>\r\n                                                    </div>\r\n                                                \r\n                                            </div>\r\n                                    </div>\r\n                                </div>\r\n                            </ng-template>\r\n                            \r\n\r\n                        </div>\r\n                    </div>\r\n\r\n                    <!-- <div class=\"col-md-4 col-sm-12\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Dokumen Tambahan</h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\"> \r\n    \r\n    \r\n                                    <label class=\"label-bold\">Foto Surat Kuasa</label>\r\n                                    <ng-container *ngIf=\"detail.additional_info && detail.additional_info.surat_kuasa && isArray(detail.additional_info.surat_kuasa) && detail.additional_info.surat_kuasa.length > 0; else empty_image\">\r\n                                        <div class=\"member-detail-image-container\"   *ngFor=\"let itemImage of detail.additional_info.surat_kuasa; let i = index\">\r\n                                            <ng-container>\r\n                                                <a href={{itemImage.pic_file_name}} target=\"_blank\">\r\n                                                    <img src={{itemImage.pic_file_name}} class=\"member-detail-image\" />\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(itemImage.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n                                                <br/>\r\n                                            </ng-container>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    \r\n                </div>\r\n            </div>\r\n\r\n\r\n            <div class=\"col-md-4 col-sm-12\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Data Pelanggan</h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\">             \r\n                                    <label>ID Pelanggan</label>\r\n                                    <div name=\"member_id\">{{memberDetail.id_toko}}</div>\r\n    \r\n                                    <label>Data Complete</label>\r\n                                    <div name=\"member_id\">{{memberDetail.additional_info && memberDetail.additional_info.data_complete ? memberDetail.additional_info.data_complete : '-'}}</div>\r\n                                    \r\n                                    <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                        <label>Nama Toko</label>\r\n                                        <div name=\"full_name\">{{memberDetail.member_detail.nama_toko}}</div>\r\n                                    </div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>Nama Program</label>\r\n                                        <div name=\"full_name\">{{memberDetail.member_detail.cluster}}</div>\r\n                                    </div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>Nama Entitas</label>\r\n                                        <div name=\"full_name\">{{memberDetail.member_detail.nama_toko}}</div>\r\n                                    </div>\r\n                                    \r\n                                    <label>Nama Distributor</label>\r\n                                    <div name=\"full_name\">{{sig_kontraktual? 'Semen Indonesia' : memberDetail.member_detail.nama_distributor}}</div>\r\n    \r\n                                    <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                        <label>Alamat Toko</label>\r\n                                        <div name=\"full_name\">{{memberDetail.member_detail.alamat_toko}}</div>\r\n                                    </div>\r\n    \r\n                                    <label>Nama Pemilik</label>\r\n                                    <div name=\"full_name\">{{memberDetail.member_detail.nama_pemilik}}</div>\r\n    \r\n                                    <label>No. WA Pemilik</label>\r\n                                    <div name=\"full_name\">{{memberDetail.member_detail.no_wa_pemilik}}</div>\r\n                                    \r\n                                    <label>No. KTP Pemilik</label>\r\n                                    <div name=\"full_name\">{{memberDetail.member_detail.ktp_pemilik}}</div>\r\n    \r\n                                    <label>Foto KTP Pemilik</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container  *ngIf=\"validURL(memberDetail.member_detail.foto_ktp_pemilik); else empty_image\">\r\n                                            <a href={{memberDetail.member_detail.foto_ktp_pemilik}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(memberDetail.member_detail.foto_ktp_pemilik) == 'image'\" src={{memberDetail.member_detail.foto_ktp_pemilik}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.member_detail.foto_ktp_pemilik) == 'document'\"\r\n                                                    [src]=\"memberDetail.member_detail.foto_ktp_pemilik\"\r\n                                                    [render-text]=\"true\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>                                                \r\n                                                <!-- <img src={{memberDetail.member_detail.foto_ktp_pemilik}} class=\"member-detail-image\" /> -->\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(memberDetail.member_detail.foto_ktp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                        \r\n                                        \r\n                                    </div>\r\n    \r\n                                    <label>No. NPWP Pemilik</label>\r\n                                    <div name=\"full_name\">{{memberDetail.member_detail.npwp_pemilik}}</div>\r\n    \r\n                                    <label>Foto NPWP Pemilik</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container  *ngIf=\"validURL(memberDetail.member_detail.foto_npwp_pemilik); else empty_image\">\r\n                                            <a href={{memberDetail.member_detail.foto_npwp_pemilik}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(memberDetail.member_detail.foto_npwp_pemilik) == 'image'\" src={{memberDetail.member_detail.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.member_detail.foto_npwp_pemilik) == 'document'\"\r\n                                                    [src]=\"memberDetail.member_detail.foto_npwp_pemilik\"\r\n                                                    [render-text]=\"true\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                                <img src={{memberDetail.member_detail.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(memberDetail.member_detail.foto_npwp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n    \r\n                                </div>\r\n                            \r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Data Penerima </h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\">\r\n    \r\n                                    <label>Hadiah Dikuasakan</label>\r\n                                    <div name=\"email\">{{memberDetail.member_detail.hadiah_dikuasakan}}</div>\r\n                                \r\n                                    <label>Nama Penerima</label>\r\n                                    <div name=\"email\">{{memberDetail.member_detail.nama_penerima_gopay}}</div>\r\n    \r\n                                    <label>No. WA Penerima</label>\r\n                                    <div name=\"email\">{{memberDetail.member_detail.no_wa_penerima}}</div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>Jabatan</label>\r\n                                        <div name=\"jabatan\">{{memberDetail.member_detail.description}}</div>\r\n                                    </div>\r\n    \r\n                                    <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                        <label>No. Gopay Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo Gopay, isi dengan No. Gopay Penerima)</div></label>\r\n                                        <div name=\"email\">{{memberDetail.member_detail.telp_penerima_gopay}}</div>\r\n                                    </div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>No. E-Wallet Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo E-Wallet, isi dengan No. E-Wallet Penerima)</div></label>\r\n                                        <div name=\"email\">{{memberDetail.member_detail.telp_penerima_gopay}}</div>\r\n                                    </div>\r\n    \r\n                                   <label>Alamat Pengiriman</label>\r\n                                   <div name=\"email\">{{memberDetail.member_detail.alamat_rumah}}</div>\r\n    \r\n                                   <label>No. KTP Penerima</label>\r\n                                   <div name=\"email\">{{memberDetail.member_detail.ktp_penerima}}</div>\r\n    \r\n                                   <label>Foto KTP Penerima</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container *ngIf=\"validURL(memberDetail.member_detail.foto_ktp_penerima); else empty_image\">\r\n                                            <a href={{memberDetail.member_detail.foto_ktp_penerima}} target=\"_blank\" >\r\n                                                <img *ngIf=\"checkFileType(memberDetail.member_detail.foto_ktp_penerima) == 'image'\" src={{memberDetail.member_detail.foto_ktp_penerima}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.member_detail.foto_ktp_penerima) == 'document'\"\r\n                                                    [src]=\"memberDetail.member_detail.foto_ktp_penerima\"\r\n                                                    [render-text]=\"true\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>    \r\n                                               <!-- <img src={{memberDetail.member_detail.foto_ktp_penerima}} class=\"member-detail-image\" /> -->\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(memberDetail.member_detail.foto_ktp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n    \r\n                                   <label>No. NPWP Penerima</label>\r\n                                   <div name=\"email\">{{memberDetail.member_detail.npwp_penerima}}</div>\r\n    \r\n                                   <label>Foto NPWP Penerima</label>\r\n                                   <div class=\"member-detail-image-container\">\r\n                                       <ng-container *ngIf=\"validURL(memberDetail.member_detail.foto_npwp_penerima); else empty_image\">\r\n                                           <a href={{memberDetail.member_detail.foto_npwp_penerima}} target=\"_blank\">\r\n                                            <img *ngIf=\"checkFileType(memberDetail.member_detail.foto_npwp_penerima) == 'image'\" src={{memberDetail.member_detail.foto_npwp_penerima}} class=\"member-detail-image\" />\r\n                                            <pdf-viewer \r\n                                                *ngIf=\"checkFileType(memberDetail.member_detail.foto_npwp_penerima) == 'document'\"\r\n                                                [src]=\"memberDetail.member_detail.foto_npwp_penerima\"\r\n                                                [render-text]=\"true\"\r\n                                                [original-size]=\"false\"\r\n                                                [page] = \"1\"\r\n                                                [show-all]=\"false\"\r\n                                                style=\"width: 100%; height: 100%\">\r\n                                            </pdf-viewer>\r\n                                              <!-- <img src={{memberDetail.member_detail.foto_npwp_penerima}} class=\"member-detail-image\" /> -->\r\n                                           </a>\r\n                                           <div class=\"btn-download-container\">\r\n                                               <button (click)=\"downloadImage(memberDetail.member_detail.foto_npwp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                           </div>\r\n                                       </ng-container>\r\n                                    </div>\r\n                                </div>\r\n                             </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n         \r\n            </div>\r\n        </div>\r\n    </div>\r\n        <ng-template #empty_image>\r\n            <div style=\"width:100%; text-align: center;\">\r\n                empty\r\n            </div>\r\n        </ng-template>\r\n\r\n        <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n            <div class=\"text-message\">\r\n                <i class=\"fas fa-ban\"></i>\r\n                {{errorMessage}}\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/finance-invoice-report/update-invoice/finance-invoice-report.update-invoice.component.html":
/*!*****************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/finance-invoice-report/update-invoice/finance-invoice-report.update-invoice.component.html ***!
  \*****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n    <app-page-header [heading]=\"'Update No. Invoice'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n    <div>\r\n        <div class=\"card card-detail\">\r\n            <div class=\"card-header\">\r\n                <!-- <button class=\"btn save_button\" (click)=\"formSubmitAddMember()\" type=\"submit\"\r\n                    [routerLinkActive]=\"['router-link-active']\"><i class=\"fa fa-fw fa-save\"\r\n                        style=\"transform: translate(-3px, -1px)\"></i>Submit</button> -->\r\n                <button class=\"btn back_button\" (click)=\"backTo()\"><i class=\"fa fa-fw fa-angle-left\"></i>Back</button>\r\n            </div>\r\n\r\n            <div class=\"card-content\">\r\n                <div class=\"member-detail\">\r\n                    <div class=\"row\">\r\n                      \r\n                        <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <input #updateBulk type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event)\"\r\n                                />\r\n\r\n                                <div class=\"card-header\">\r\n                                    <h2>Update No. Invoice</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n                                        <label>Upload Your File Here</label>\r\n                                        <div class=\"member-bulk-update-container\">\r\n                                            <span *ngIf=\"selectedFile && prodOnUpload == false\">{{selectedFile.name}}</span>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group uploaded\" *ngIf=\"prodOnUpload == false && startUploading == true && cancel == false\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <div class=\"container-two-button\">\r\n                                            <div class=\"img-upload\" *ngIf=\"!selectedFile; else upload_image\">\r\n                                                <button class=\"btn btn-upload\" (click)=\"updateBulk.click()\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span>upload</span>\r\n                                                </button>\r\n                                            </div>\r\n    \r\n                                            <div class=\"img-upload\" *ngIf=\"selectedFile\">\r\n                                                <button class=\"btn btn-submit\" (click)=\"updateDataBulk()\">\r\n                                                    <i class=\"fa fa-fw fa-save\"></i>\r\n                                                    <span>submit</span>\r\n                                                </button>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <ng-template #upload_image>\r\n                                        <button class=\"btn btn-upload\" (click)=\"updateBulk.click()\">\r\n                                            <i class=\"fa fa-fw fa-upload\"></i>\r\n                                            <span>re-upload</span>\r\n                                        </button>\r\n                                    </ng-template>\r\n\r\n                                    <div class=\"col-md-12 col-sm-12 error-msg-upload\">\r\n                                        <div *ngIf=\"errorFile\"><mark>Error: {{errorFile}}</mark></div>                       \r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n                        \r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/detail/finance-invoice-report.detail.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/detail/finance-invoice-report.detail.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.member-detail-image-container a {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.member-detail-image-container a .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.member-detail-image-container .btn-download-container {\n  margin-top: 5px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\nbutton.btn-download-img {\n  margin-top: 10px;\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n\n.card-detail > .card-header .btn-right-redeem {\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZmluYW5jZS1pbnZvaWNlLXJlcG9ydC9kZXRhaWwvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxmaW5hbmNlLWludm9pY2UtcmVwb3J0XFxkZXRhaWxcXGZpbmFuY2UtaW52b2ljZS1yZXBvcnQuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9maW5hbmNlLWludm9pY2UtcmVwb3J0L2RldGFpbC9maW5hbmNlLWludm9pY2UtcmVwb3J0LmRldGFpbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0EsYUFBQTtBQ0NKOztBRENBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ0VKOztBRERJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNHUjs7QURGUTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0tBQUEsbUJBQUE7QUNJWjs7QURESTtFQUNJLGVBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ0dSOztBREVBO0VBQ0ksZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ0NKOztBREdJO0VBQ0ksa0JBQUE7QUNBUjs7QURDUTtFQUVJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDRFo7O0FERVk7RUFDSSxpQkFBQTtBQ0FoQjs7QURHUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUVBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUlBLFdBQUE7QUNMWjs7QURFWTtFQUNJLGlCQUFBO0FDQWhCOztBRElRO0VBQ0ksc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ0ZaOztBRElRO0VBQ0kseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ0ZaOztBREtJO0VBQ0ksYUFBQTtBQ0hSOztBRElRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0ZaOztBRGlCSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDZlI7O0FEa0JRO0VBQ0ksa0JBQUE7QUNoQlo7O0FEa0JRO0VBQ0ksZ0JBQUE7QUNoQlo7O0FEa0JRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2hCWjs7QURtQlE7RUFDSSxzQkFBQTtBQ2pCWjs7QURrQlk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ2hCaEI7O0FEdUJBO0VBQ0ksaUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNwQko7O0FEc0JJO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ3BCUiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2ZpbmFuY2UtaW52b2ljZS1yZXBvcnQvZGV0YWlsL2ZpbmFuY2UtaW52b2ljZS1yZXBvcnQuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWRlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgbWluLWhlaWdodDogMjdweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgYSB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcclxuICAgICAgICAgICAgd2lkdGg6MjAwcHg7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5idG4tZG93bmxvYWQtY29udGFpbmVyIHtcclxuICAgICAgICBtYXJnaW4tdG9wOjVweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5idXR0b24uYnRuLWRvd25sb2FkLWltZyB7XHJcbiAgICBtYXJnaW4tdG9wOjEwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBib3JkZXI6MDtcclxuICAgIGN1cnNvcjpwb2ludGVyO1xyXG59XHJcblxyXG4uY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmVkaXRfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmJ0bi1yaWdodCB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5idG4tcmlnaHQtcmVkZWVtIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gLmltYWdle1xyXG4gICAgLy8gICAgIGgze1xyXG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAvLyAgICAgICAgIGltZ3tcclxuICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICAgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmVycm9yLW1lc3NhZ2Uge1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBoZWlnaHQ6MTAwdmg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6MjZweDtcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG5cclxuICAgIC50ZXh0LW1lc3NhZ2Uge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNFNzRDM0M7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICBtYXJnaW46IDAgMTBweDtcclxuICAgIH1cclxufSIsIi5iZy1kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgYSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIGEgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5idG4tZG93bmxvYWQtY29udGFpbmVyIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuYnV0dG9uLmJ0bi1kb3dubG9hZC1pbWcge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogNXB4IDE1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJvcmRlcjogMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJ0bi1yaWdodCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJ0bi1yaWdodC1yZWRlZW0ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4uZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/detail/finance-invoice-report.detail.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/detail/finance-invoice-report.detail.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: FinanceInvoiceReportDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceInvoiceReportDetailComponent", function() { return FinanceInvoiceReportDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var FinanceInvoiceReportDetailComponent = /** @class */ (function () {
    function FinanceInvoiceReportDetailComponent(OrderhistoryService, router, route) {
        this.OrderhistoryService = OrderhistoryService;
        this.router = router;
        this.route = route;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__;
        this.edit = false;
        this.errorLabel = false;
        this.errorMessage = false;
        this.isAddDocument = false;
        this.mci_project = false;
        this.programType = "";
        this.sig_kontraktual = false;
    }
    FinanceInvoiceReportDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    FinanceInvoiceReportDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this, order_id, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        program = localStorage.getItem('programName');
                        _this = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program) {
                                if (element.type == "reguler") {
                                    _this.mci_project = true;
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this.programType = "custom_kontraktual";
                                    _this.sig_kontraktual = true;
                                }
                                else {
                                    _this.mci_project = false;
                                }
                            }
                        });
                        order_id = this.detail.order_id;
                        this.invoiceDetail = this.detail;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.OrderhistoryService.getFinanceInvoiceReportByID(order_id)];
                    case 2:
                        result = _a.sent();
                        if (result.length > 0) {
                            if (result[0].order_id == this.detail.order_id) {
                                this.invoiceDetail = result[0];
                            }
                        }
                        console.warn("invoice detail lagi", this.invoiceDetail);
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    FinanceInvoiceReportDetailComponent.prototype.addDocument = function () {
        this.isAddDocument = !this.isAddDocument;
        if (this.isAddDocument == false) {
            this.firstLoad();
        }
    };
    FinanceInvoiceReportDetailComponent.prototype.downloadImage = function (url) {
        var filename;
        filename = this.lastPathURL(url);
        fetch(url).then(function (t) {
            return t.blob().then(function (b) {
                var a = document.createElement("a");
                a.href = URL.createObjectURL(b);
                a.setAttribute("download", filename);
                a.click();
            });
        });
    };
    FinanceInvoiceReportDetailComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    FinanceInvoiceReportDetailComponent.prototype.checkFileType = function (text) {
        var splitFileName = text.split('.');
        var extensFile = splitFileName[splitFileName.length - 1];
        var extens = extensFile.toLowerCase();
        return extens == 'pdf' ? 'document' : 'image';
    };
    FinanceInvoiceReportDetailComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    FinanceInvoiceReportDetailComponent.prototype.lastPathURL = function (url) {
        var resURL = url.split("/");
        console.log("resURL", resURL);
        var lastNumber = resURL.length - 1;
        return resURL[lastNumber];
    };
    FinanceInvoiceReportDetailComponent.prototype.editThis = function () {
        // console.log(this.edit );
        this.edit = !this.edit;
        if (this.edit == false) {
            console.warn("firstLoad lagi");
            this.firstLoad();
        }
        // console.log(this.edit );
    };
    FinanceInvoiceReportDetailComponent.prototype.backToTable = function () {
        console.warn("back invoice detail", this.back);
        this.back[1](this.back[0]);
    };
    FinanceInvoiceReportDetailComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    FinanceInvoiceReportDetailComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FinanceInvoiceReportDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FinanceInvoiceReportDetailComponent.prototype, "back", void 0);
    FinanceInvoiceReportDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-finance-invoice-report-detail',
            template: __webpack_require__(/*! raw-loader!./finance-invoice-report.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/finance-invoice-report/detail/finance-invoice-report.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./finance-invoice-report.detail.component.scss */ "./src/app/layout/modules/finance-invoice-report/detail/finance-invoice-report.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], FinanceInvoiceReportDetailComponent);
    return FinanceInvoiceReportDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/edit/finance-invoice-report.edit.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/edit/finance-invoice-report.edit.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container a {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.member-detail-image-container a .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .btn-download-container {\n  margin-top: 5px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\nbutton.btn-download-img {\n  margin-top: 10px;\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZmluYW5jZS1pbnZvaWNlLXJlcG9ydC9lZGl0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZmluYW5jZS1pbnZvaWNlLXJlcG9ydFxcZWRpdFxcZmluYW5jZS1pbnZvaWNlLXJlcG9ydC5lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9maW5hbmNlLWludm9pY2UtcmVwb3J0L2VkaXQvZmluYW5jZS1pbnZvaWNlLXJlcG9ydC5lZGl0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksa0JBQUE7QUNBUjtBRENRO0VBRUksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNEWjtBREVZO0VBQ0ksaUJBQUE7QUNBaEI7QURHUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBS0EsV0FBQTtBQ05aO0FER1k7RUFDSSxpQkFBQTtBQ0RoQjtBREtRO0VBQ0kseUJBQUE7QUNIWjtBRE1JO0VBQ0ksYUFBQTtBQ0pSO0FETVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDSlo7QURRSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDTlI7QURVUTtFQUNJLGdCQUFBO0FDUlo7QURVUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNSWjtBRFVRO0VBQ0ksZ0JBQUE7QUNSWjtBRFNZO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDUGhCO0FEU1k7RUFDSSx5QkFBQTtBQ1BoQjtBRFNZO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ1BoQjtBRFNZO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ1BoQjtBRFFnQjtFQUNJLGVBQUE7RUFDQSxXQUFBO0FDTnBCO0FEWVE7RUFDSSxzQkFBQTtBQ1ZaO0FEV1k7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ1RoQjtBRGdCQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNiSjtBRGNJO0VBQ0ksYUFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ1pSO0FEY1E7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtLQUFBLG1CQUFBO0FDWlo7QURnQkk7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ2RSO0FEZVE7RUFDSSxZQUFBO0VBRUEsV0FBQTtBQ2RaO0FEZVk7RUFFSSxRQUFBO0FDZGhCO0FEcUJBO0VBQ0kseUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUNsQko7QURxQkE7RUFDSSx3QkFBQTtBQ2xCSjtBRHFCQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ2xCSjtBRHFCQTtFQUNJLGdCQUFBO0FDbEJKO0FEcUJBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2xCSjtBRG1CSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDakJSO0FEa0JRO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ2hCWjtBRG1CSTtFQUNJLGVBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ2pCUjtBRHFCQTtFQUNJLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUNsQkoiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9maW5hbmNlLWludm9pY2UtcmVwb3J0L2VkaXQvZmluYW5jZS1pbnZvaWNlLXJlcG9ydC5lZGl0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b24udHJ1ZXtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICBcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICBcclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmltYWdle1xyXG4gICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICA+ZGl2e1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDMzLjMzMzMlO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtaGVhZGVye1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBoM3tcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgbWluLWhlaWdodDogMjdweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgLmltZy11cGxvYWQge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46MjBweCAxMHB4O1xyXG5cclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIC5sb2FkaW5nIHtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xyXG4gICAgICAgICAgICB3aWR0aDoxMDAlO1xyXG4gICAgICAgICAgICAuc2stZmFkaW5nLWNpcmNsZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOnJlZDtcclxuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuYnV0dG9uLmJ0bl91cGxvYWQsIGJ1dHRvbi5hZGQtbW9yZS1pbWFnZSwgYnV0dG9uLmJ0bl9kZWxldGUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDE1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgYm9yZGVyOjA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxufVxyXG5cclxuYnV0dG9uLmJ0bl9kZWxldGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBzYWxtb247XHJcbn1cclxuXHJcbi5hZGQtbW9yZS1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTpmbGV4O1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4ubGFiZWwtYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG59XHJcblxyXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBjb2xvcjogIzY2NjtcclxuICAgIGEge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuYnRuLWRvd25sb2FkLWNvbnRhaW5lciB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDo1cHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG59XHJcblxyXG5idXR0b24uYnRuLWRvd25sb2FkLWltZyB7XHJcbiAgICBtYXJnaW4tdG9wOjEwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBib3JkZXI6MDtcclxuICAgIGN1cnNvcjpwb2ludGVyO1xyXG59XHJcbiIsIi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2Uge1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSA+IGRpdiB7XG4gIHdpZHRoOiAzMy4zMzMzJTtcbiAgcGFkZGluZzogNXB4O1xuICBmbG9hdDogbGVmdDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgaDMge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IGltZyB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDIwcHggMTBweDtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCAubWVtYmVyLWRldGFpbC1pbWFnZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyAuc2stZmFkaW5nLWNpcmNsZSB7XG4gIHRvcDogNTAlO1xufVxuXG5idXR0b24uYnRuX3VwbG9hZCwgYnV0dG9uLmFkZC1tb3JlLWltYWdlLCBidXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiA1cHggMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm9yZGVyOiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbmJ1dHRvbi5idG5fZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogc2FsbW9uO1xufVxuXG4uYWRkLW1vcmUtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ubGFiZWwtYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciBhIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgYSAubWVtYmVyLWRldGFpbC1pbWFnZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmJ0bi1kb3dubG9hZC1jb250YWluZXIge1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG5idXR0b24uYnRuLWRvd25sb2FkLWltZyB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiA1cHggMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm9yZGVyOiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/edit/finance-invoice-report.edit.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/edit/finance-invoice-report.edit.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: FinanceInvoiceReportEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceInvoiceReportEditComponent", function() { return FinanceInvoiceReportEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { ProductService } from '../../../../services/product/product.service';
var FinanceInvoiceReportEditComponent = /** @class */ (function () {
    function FinanceInvoiceReportEditComponent(OrderhistoryService, zone) {
        this.OrderhistoryService = OrderhistoryService;
        this.zone = zone;
        this.errorLabel = false;
        this.allBast = [];
        this.allSuratKuasa = [];
        this.invoiceNumber = "";
        this.optionsKuasa1 = [
            { label: "KUASA", value: "true" },
            { label: "TIDAK KUASA", value: "false" }
        ];
        this.optionsKuasa2 = [
            { label: "KUASA", value: "KUASA" },
            { label: "TIDAK KUASA", value: "TIDAK KUASA" }
        ];
        this.optionsKuasa3 = [
            { label: "KUASA", value: "ya" },
            { label: "TIDAK KUASA", value: "tidak" }
        ];
        this.loading = false;
        this.activationStatus = [
            { label: 'ACTIVATED', value: 'ACTIVATED' },
            { label: 'INACTIVED', value: 'INACTIVED' }
        ];
        this.memberStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
        this.startUploading = false;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.loadingKTPPemilik = false;
        this.loadingNPWPPemilik = false;
        this.loadingKTPPenerima = false;
        this.loadingNPWPPenerima = false;
        this.loadingBAST = [];
        this.loadingSuratKuasa = [];
    }
    FinanceInvoiceReportEditComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    FinanceInvoiceReportEditComponent.prototype.checkValueKuasa = function () {
        var _this = this;
        if (this.updateSingle["hadiah_dikuasakan"] == "true" || this.updateSingle["hadiah_dikuasakan"] == "false") {
            var options = this.optionsKuasa1.map(function (el) {
                if (el.value == _this.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa1 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "KUASA" || this.updateSingle["hadiah_dikuasakan"] == "TIDAK KUASA") {
            var options = this.optionsKuasa2.map(function (el) {
                if (el.value == _this.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa2 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "ya" || this.updateSingle["hadiah_dikuasakan"] == "tidak") {
            var options = this.optionsKuasa3.map(function (el) {
                if (el.value == _this.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa3 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "-") {
            console.warn("im here!");
            var options = this.optionsKuasa2.map(function (el) {
                if (el.value == _this.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa2 = options;
        }
    };
    FinanceInvoiceReportEditComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    FinanceInvoiceReportEditComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    FinanceInvoiceReportEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                // console.warn("detail",this.detail);
                this.updateSingle = JSON.parse(JSON.stringify(this.detail.member_detail));
                this.orderID = this.detail.order_id;
                if (this.invoiceNumber == "") {
                    this.invoiceNumber = this.detail.additional_info.invoice_no;
                }
                else {
                    this.invoiceNumber = this.invoiceNumber;
                    this.detail.additional_info.invoice_no = this.invoiceNumber;
                }
                this.checkValueKuasa();
                this.detail.previous_member_id = this.detail.member_id;
                this.activationStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this.detail.activation_status) {
                        _this.activationStatus[index].selected = 1;
                    }
                });
                this.detail.previous_member_id = this.detail.member_id;
                this.memberStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this.detail.member_status) {
                        _this.memberStatus[index].selected = 1;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    FinanceInvoiceReportEditComponent.prototype.updateInvoiceNumber = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, result, e_1, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.startUploading = true;
                        this.cancel = false;
                        payload = {
                            type: 'application/form-data',
                        };
                        console.warn("order id", this.orderID);
                        console.warn("this.invoiceNumber", this.invoiceNumber);
                        if (!(this.invoiceNumber && this.orderID)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.OrderhistoryService.uploadInvoiceNumber(this.invoiceNumber, this, payload, this.orderID)];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.startUploading = false;
                        this.errorLabel = (e_1.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    FinanceInvoiceReportEditComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    FinanceInvoiceReportEditComponent.prototype.backToDetail = function () {
        // console.log("Edit member",this.back);
        this.back[0][this.back[1]]();
        // this.back[0]();
    };
    FinanceInvoiceReportEditComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    FinanceInvoiceReportEditComponent.prototype.openUpload = function (type, index) {
        var upFile = document.getElementById(type + index);
        upFile.click();
    };
    FinanceInvoiceReportEditComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    FinanceInvoiceReportEditComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FinanceInvoiceReportEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FinanceInvoiceReportEditComponent.prototype, "back", void 0);
    FinanceInvoiceReportEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-finance-invoice-report-edit',
            template: __webpack_require__(/*! raw-loader!./finance-invoice-report.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/finance-invoice-report/edit/finance-invoice-report.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./finance-invoice-report.edit.component.scss */ "./src/app/layout/modules/finance-invoice-report/edit/finance-invoice-report.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]])
    ], FinanceInvoiceReportEditComponent);
    return FinanceInvoiceReportEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/finance-invoice-report-routing.module.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/finance-invoice-report-routing.module.ts ***!
  \************************************************************************************************/
/*! exports provided: FinanceInvoiceReportRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceInvoiceReportRoutingModule", function() { return FinanceInvoiceReportRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _finance_invoice_report_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./finance-invoice-report.component */ "./src/app/layout/modules/finance-invoice-report/finance-invoice-report.component.ts");
/* harmony import */ var _update_invoice_finance_invoice_report_update_invoice_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update-invoice/finance-invoice-report.update-invoice.component */ "./src/app/layout/modules/finance-invoice-report/update-invoice/finance-invoice-report.update-invoice.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '', component: _finance_invoice_report_component__WEBPACK_IMPORTED_MODULE_2__["FinanceInvoiceReportComponent"]
    },
    {
        path: 'update-invoice', component: _update_invoice_finance_invoice_report_update_invoice_component__WEBPACK_IMPORTED_MODULE_3__["UpdateInvoiceComponent"]
    },
];
var FinanceInvoiceReportRoutingModule = /** @class */ (function () {
    function FinanceInvoiceReportRoutingModule() {
    }
    FinanceInvoiceReportRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], FinanceInvoiceReportRoutingModule);
    return FinanceInvoiceReportRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/finance-invoice-report.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/finance-invoice-report.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZmluYW5jZS1pbnZvaWNlLXJlcG9ydC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGZpbmFuY2UtaW52b2ljZS1yZXBvcnRcXGZpbmFuY2UtaW52b2ljZS1yZXBvcnQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2ZpbmFuY2UtaW52b2ljZS1yZXBvcnQvZmluYW5jZS1pbnZvaWNlLXJlcG9ydC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDQ0o7QURDSTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUNDUiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2ZpbmFuY2UtaW52b2ljZS1yZXBvcnQvZmluYW5jZS1pbnZvaWNlLXJlcG9ydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn0iLCIuZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/finance-invoice-report.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/finance-invoice-report.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: FinanceInvoiceReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceInvoiceReportComponent", function() { return FinanceInvoiceReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var FinanceInvoiceReportComponent = /** @class */ (function () {
    function FinanceInvoiceReportComponent(OrderhistoryService) {
        this.OrderhistoryService = OrderhistoryService;
        this.SalesRedemption = [];
        this.tableFormat = {
            title: 'Finance Invoice report',
            label_headers: [
                { label: 'Tanggal Proses', visible: true, type: 'date', data_row_name: 'approve_date' },
                { label: 'Tanggal Upload BAST', visible: true, type: 'bast_date', data_row_name: 'additional_info' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'AWB Number', visible: true, type: 'awb_number', data_row_name: 'delivery_detail' },
                { label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                { label: 'Alamat Toko', visible: true, type: 'form_alamat_toko', data_row_name: 'member_detail' },
                { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'No Telp Pemilik', visible: true, type: 'string', data_row_name: 'no_wa_pemilik' },
                { label: 'KTP Pemilik', visible: true, type: 'form_ktp_pemilik', data_row_name: 'member_detail' },
                { label: 'Foto KTP Pemilik', visible: true, type: 'form_foto_ktp_pemilik', data_row_name: 'member_detail' },
                { label: 'NPWP Pemilik', visible: true, type: 'form_npwp_pemilik', data_row_name: 'member_detail' },
                { label: 'Foto NPWP Pemilik', visible: true, type: 'form_foto_npwp_pemilik', data_row_name: 'member_detail' },
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'Alamat Penerima', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'No Telp Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'KTP Penerima', visible: true, type: 'form_ktp_penerima', data_row_name: 'member_detail' },
                { label: 'Foto KTP Penerima', visible: true, type: 'form_foto_ktp_penerima', data_row_name: 'member_detail' },
                { label: 'NPWP Penerima', visible: true, type: 'form_npwp_penerima', data_row_name: 'member_detail' },
                { label: 'Foto NPWP Penerima', visible: true, type: 'form_foto_npwp_penerima', data_row_name: 'member_detail' },
                { label: 'Hadiah', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Price', visible: true, type: 'product_price_redeem', data_row_name: 'products' },
                { label: 'Total Order Price', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Group', visible: true, type: 'form_group', data_row_name: 'member_detail' },
                { label: 'BAST', visible: true, type: 'bast', data_row_name: 'additional_info' },
                { label: 'Surat Kuasa', visible: true, type: 'surat_kuasa', data_row_name: 'additional_info' },
                { label: 'No Invoice', visible: true, type: 'value_invoice', data_row_name: 'invoice_no' },
                { label: 'Tanggal Invoice', visible: true, type: 'date_invoice', data_row_name: 'invoice_no' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
                updateInvoice: true,
            },
            show_checkbox_options: true
        };
        this.tableFormat2 = {
            title: 'Finance Invoice report',
            label_headers: [
                { label: 'Tanggal Proses', visible: true, type: 'date', data_row_name: 'approve_date' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'PO Number', visible: true, type: 'value_po', data_row_name: 'po_no' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'AWB Number', visible: true, type: 'awb_number', data_row_name: 'delivery_detail' },
                { label: 'Nama Pelanggan', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'No Telp Pelanggan', visible: true, type: 'string', data_row_name: 'no_wa_pemilik' },
                { label: 'Alamat Pengiriman', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'Hadiah', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Price', visible: true, type: 'product_price_redeem', data_row_name: 'products' },
                { label: 'Total Order Price', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Program', visible: true, type: 'form_group', data_row_name: 'member_detail' },
                { label: 'No Invoice', visible: true, type: 'value_invoice', data_row_name: 'invoice_no' },
                { label: 'Tanggal Invoice', visible: true, type: 'date_invoice', data_row_name: 'invoice_no' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
                updateInvoice: true,
            },
            show_checkbox_options: true
        };
        this.tableFormat3 = {
            title: 'Finance Invoice report',
            label_headers: [
                { label: 'Tanggal Proses', visible: true, type: 'date', data_row_name: 'approve_date' },
                { label: 'Tanggal Upload BAST', visible: true, type: 'bast_date', data_row_name: 'additional_info' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'AWB Number', visible: true, type: 'awb_number', data_row_name: 'delivery_detail' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
                { label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                // {label: 'Alamat', visible: true, type: 'form_alamat_toko', data_row_name: 'member_detail'},
                { label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'No Telp PIC', visible: true, type: 'string', data_row_name: 'no_wa_pemilik' },
                { label: 'KTP PIC', visible: true, type: 'form_ktp_pemilik', data_row_name: 'member_detail' },
                { label: 'Foto KTP PIC', visible: true, type: 'form_foto_ktp_pemilik', data_row_name: 'member_detail' },
                { label: 'NPWP PIC', visible: true, type: 'form_npwp_pemilik', data_row_name: 'member_detail' },
                { label: 'Foto NPWP PIC', visible: true, type: 'form_foto_npwp_pemilik', data_row_name: 'member_detail' },
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'Jabatan', visible: true, type: 'form_group', data_row_name: 'member_detail' },
                { label: 'Alamat Penerima', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'No Telp Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'KTP Penerima', visible: true, type: 'form_ktp_penerima', data_row_name: 'member_detail' },
                { label: 'Foto KTP Penerima', visible: true, type: 'form_foto_ktp_penerima', data_row_name: 'member_detail' },
                { label: 'NPWP Penerima', visible: true, type: 'form_npwp_penerima', data_row_name: 'member_detail' },
                { label: 'Foto NPWP Penerima', visible: true, type: 'form_foto_npwp_penerima', data_row_name: 'member_detail' },
                { label: 'Hadiah', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Price', visible: true, type: 'product_price_redeem', data_row_name: 'products' },
                { label: 'Total Order Price', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'BAST', visible: true, type: 'bast', data_row_name: 'additional_info' },
                { label: 'Surat Kuasa', visible: true, type: 'surat_kuasa', data_row_name: 'additional_info' },
                { label: 'No Invoice', visible: true, type: 'value_invoice', data_row_name: 'invoice_no' },
                { label: 'Tanggal Invoice', visible: true, type: 'date_invoice', data_row_name: 'invoice_no' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
                updateInvoice: true,
            },
            show_checkbox_options: true
        };
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2__;
        this.form_input = {};
        this.errorLabel = false;
        this.errorMessage = false;
        this.totalPage = 0;
        this.invoiceDetail = false;
        this.mci_project = false;
        this.programType = "";
    }
    FinanceInvoiceReportComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    FinanceInvoiceReportComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program_1, _this_1, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        program_1 = localStorage.getItem('programName');
                        _this_1 = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program_1) {
                                if (element.type == "reguler") {
                                    _this_1.mci_project = true;
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this_1.programType = "custom_kontraktual";
                                }
                                else {
                                    _this_1.mci_project = false;
                                }
                            }
                        });
                        this.service = this.OrderhistoryService;
                        result = void 0;
                        if (!(this.mci_project == true)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.OrderhistoryService.getCompleteFinanceInvoiceReport()];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.OrderhistoryService.getFinanceInvoiceReport()];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        this.totalPage = result.total_page;
                        this.SalesRedemption = result.values;
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    FinanceInvoiceReportComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    this.invoiceDetail = this.SalesRedemption.find(function (order) { return order.order_id == rowData.order_id; });
                    // console.warn("invoice detail",this.invoiceDetail);
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    FinanceInvoiceReportComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.invoiceDetail = false;
                obj.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    FinanceInvoiceReportComponent.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    FinanceInvoiceReportComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"] }
    ]; };
    FinanceInvoiceReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-finance-invoice-report',
            template: __webpack_require__(/*! raw-loader!./finance-invoice-report.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/finance-invoice-report/finance-invoice-report.component.html"),
            styles: [__webpack_require__(/*! ./finance-invoice-report.component.scss */ "./src/app/layout/modules/finance-invoice-report/finance-invoice-report.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"]])
    ], FinanceInvoiceReportComponent);
    return FinanceInvoiceReportComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/finance-invoice-report.module.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/finance-invoice-report.module.ts ***!
  \****************************************************************************************/
/*! exports provided: FinanceInvoiceReportModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceInvoiceReportModule", function() { return FinanceInvoiceReportModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _finance_invoice_report_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./finance-invoice-report-routing.module */ "./src/app/layout/modules/finance-invoice-report/finance-invoice-report-routing.module.ts");
/* harmony import */ var _finance_invoice_report_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./finance-invoice-report.component */ "./src/app/layout/modules/finance-invoice-report/finance-invoice-report.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _detail_finance_invoice_report_detail_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./detail/finance-invoice-report.detail.component */ "./src/app/layout/modules/finance-invoice-report/detail/finance-invoice-report.detail.component.ts");
/* harmony import */ var _edit_finance_invoice_report_edit_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./edit/finance-invoice-report.edit.component */ "./src/app/layout/modules/finance-invoice-report/edit/finance-invoice-report.edit.component.ts");
/* harmony import */ var _update_invoice_finance_invoice_report_update_invoice_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./update-invoice/finance-invoice-report.update-invoice.component */ "./src/app/layout/modules/finance-invoice-report/update-invoice/finance-invoice-report.update-invoice.component.ts");
/* harmony import */ var _list_order_list_order_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./list-order/list-order.component */ "./src/app/layout/modules/finance-invoice-report/list-order/list-order.component.ts");
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ng2-pdf-viewer */ "./node_modules/ng2-pdf-viewer/fesm5/ng2-pdf-viewer.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













// import { BastComponent } from './bast/bast.component';

var FinanceInvoiceReportModule = /** @class */ (function () {
    function FinanceInvoiceReportModule() {
    }
    FinanceInvoiceReportModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _finance_invoice_report_routing_module__WEBPACK_IMPORTED_MODULE_2__["FinanceInvoiceReportRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_13__["BsComponentModule"],
                ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_12__["PdfViewerModule"]
            ],
            declarations: [
                _finance_invoice_report_component__WEBPACK_IMPORTED_MODULE_3__["FinanceInvoiceReportComponent"],
                _detail_finance_invoice_report_detail_component__WEBPACK_IMPORTED_MODULE_8__["FinanceInvoiceReportDetailComponent"],
                _edit_finance_invoice_report_edit_component__WEBPACK_IMPORTED_MODULE_9__["FinanceInvoiceReportEditComponent"],
                _update_invoice_finance_invoice_report_update_invoice_component__WEBPACK_IMPORTED_MODULE_10__["UpdateInvoiceComponent"],
                _list_order_list_order_component__WEBPACK_IMPORTED_MODULE_11__["ListOrderComponent"]
            ],
        })
    ], FinanceInvoiceReportModule);
    return FinanceInvoiceReportModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/list-order/list-order.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/list-order/list-order.component.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn-change-password {\n  width: 100%;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n.container-two-button {\n  display: flex;\n}\n.file-selected {\n  color: red;\n}\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n.btn-upload {\n  margin-right: 10px;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container img {\n  width: 100%;\n}\n.member-detail-image-container a {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.member-detail-image-container a .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .btn-download-container {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\nbutton.btn-download-img {\n  margin-top: 10px;\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\n.edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.edit_button i {\n  font-weight: bold;\n}\n.btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZmluYW5jZS1pbnZvaWNlLXJlcG9ydC9saXN0LW9yZGVyL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZmluYW5jZS1pbnZvaWNlLXJlcG9ydFxcbGlzdC1vcmRlclxcbGlzdC1vcmRlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZmluYW5jZS1pbnZvaWNlLXJlcG9ydC9saXN0LW9yZGVyL2xpc3Qtb3JkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtBQ0FSO0FEQ1E7RUFFSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTlo7QURHWTtFQUNJLGlCQUFBO0FDRGhCO0FES1E7RUFDSSx5QkFBQTtBQ0haO0FETUk7RUFDSSxhQUFBO0FDSlI7QURNUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNKWjtBRFFJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNOUjtBRFVRO0VBQ0ksZ0JBQUE7QUNSWjtBRFVRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ1JaO0FEVVE7RUFDSSxnQkFBQTtBQ1JaO0FEU1k7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNQaEI7QURTWTtFQUNJLHlCQUFBO0FDUGhCO0FEU1k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDUGhCO0FEU1k7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDUGhCO0FEUWdCO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNOcEI7QURZUTtFQUNJLHNCQUFBO0FDVlo7QURXWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDVGhCO0FEZ0JBO0VBQ0ksZ0JBQUE7RUFFQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNiSjtBRGtCQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQ2ZKO0FEaUJBO0VBQ0ksbUJBQUE7RUFFQSxhQUFBO0FDZko7QURrQkE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDZko7QURnQkk7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDZFI7QURnQlE7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtLQUFBLG1CQUFBO0FDZFo7QURrQkk7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ2hCUjtBRGlCUTtFQUNJLFlBQUE7RUFFQSxXQUFBO0FDaEJaO0FEaUJZO0VBRUksUUFBQTtBQ2hCaEI7QUR1QkE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ3BCSjtBRHVCQTtFQUNJLFdBQUE7QUNwQko7QUR1QkE7RUFDSSx3QkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGdCQUFBO0FDcEJKO0FEdUJBO0VBQ0ksZ0JBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FDcEJKO0FEdUJBO0VBQ0ksYUFBQTtBQ3BCSjtBRHVCQTtFQUNJLFVBQUE7QUNwQko7QUR1QkE7RUFDSSxtQkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGtCQUFBO0FDcEJKO0FEdUJBO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ3BCSjtBRHFCSTtFQUNJLFdBQUE7QUNuQlI7QURxQkk7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ25CUjtBRG9CUTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0tBQUEsbUJBQUE7QUNsQlo7QURxQkk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ25CUjtBRHFCSTtFQUNJLGFBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNuQlI7QURxQlE7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtLQUFBLG1CQUFBO0FDbkJaO0FEd0JBO0VBQ0ksZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ3JCSjtBRHdCQTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUVBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUlBLFdBQUE7QUN6Qko7QURzQkk7RUFDSSxpQkFBQTtBQ3BCUjtBRHdCQTtFQUNJLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNyQko7QUR3QkE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ3JCSjtBRHVCSTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUNyQlIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9maW5hbmNlLWludm9pY2UtcmVwb3J0L2xpc3Qtb3JkZXIvbGlzdC1vcmRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgLy8gcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9uLnRydWV7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgXHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5tZW1iZXItZGV0YWlse1xyXG4gICAgICAgXHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5pbWFnZXtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgPmRpdntcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzMy4zMzMzJTtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jYXJkLWhlYWRlcntcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaDN7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4ucm91bmRlZC1idG4ge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBsaW5lLWhlaWdodDogNDBweDtcclxuICAgIHBhZGRpbmc6IDAgNDBweDtcclxuICAgIGJhY2tncm91bmQ6ICM1NmE0ZmY7XHJcbiAgICAvLyBtYXJnaW4tbGVmdDogNDAlO1xyXG4gICAgLy8gbWFyZ2luLXJpZ2h0OiA1MCU7XHJcbn1cclxuXHJcbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBsaW5lLWhlaWdodDogNDBweDtcclxuICAgIHBhZGRpbmc6IDAgNDBweDtcclxuICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbn1cclxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQ6IGRhcmtlbigjNTZBNEZGLCAxNSUpO1xyXG4gICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbn1cclxuXHJcbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgbWluLWhlaWdodDogMjdweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgLmltZy11cGxvYWQge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46MjBweCAxMHB4O1xyXG5cclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIC5sb2FkaW5nIHtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xyXG4gICAgICAgICAgICB3aWR0aDoxMDAlO1xyXG4gICAgICAgICAgICAuc2stZmFkaW5nLWNpcmNsZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOnJlZDtcclxuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuYnV0dG9uLmJ0bl91cGxvYWQsIGJ1dHRvbi5hZGQtbW9yZS1pbWFnZSwgYnV0dG9uLmJ0bl9kZWxldGUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDE1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgYm9yZGVyOjA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxufVxyXG5cclxuYnV0dG9uLmJ0bi1jaGFuZ2UtcGFzc3dvcmQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogc2FsbW9uO1xyXG59XHJcblxyXG4uYWRkLW1vcmUtY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmxhYmVsLWJvbGQge1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG5cclxuLmVycm9yLW1zZy11cGxvYWQge1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIHBhZGRpbmctbGVmdDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNvbnRhaW5lci10d28tYnV0dG9uIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5maWxlLXNlbGVjdGVkIHtcclxuICAgIGNvbG9yOiByZWQ7XHJcbn1cclxuXHJcbi5idG4tYWRkLWJ1bGsge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLmJ0bi11cGxvYWQge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgY29sb3I6ICM2NjY7XHJcbiAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gICAgYSB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcclxuICAgICAgICAgICAgd2lkdGg6MjAwcHg7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5idG4tZG93bmxvYWQtY29udGFpbmVyIHtcclxuICAgICAgICBtYXJnaW4tdG9wOjVweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOjVweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAuaW1nLXVwbG9hZCB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbjoyMHB4IDEwcHg7XHJcblxyXG4gICAgICAgIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcclxuICAgICAgICAgICAgd2lkdGg6MjAwcHg7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuYnV0dG9uLmJ0bi1kb3dubG9hZC1pbWcge1xyXG4gICAgbWFyZ2luLXRvcDoxMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDE1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgYm9yZGVyOjA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxufVxyXG5cclxuLmVkaXRfYnV0dG9ue1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA3cHg7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBpe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG4gICAgcmlnaHQ6IDEwcHg7XHJcbn1cclxuLmJ0bi1yaWdodCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxufVxyXG5cclxuLmVycm9yLW1lc3NhZ2Uge1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBoZWlnaHQ6MTAwdmg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6MjZweDtcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG5cclxuICAgIC50ZXh0LW1lc3NhZ2Uge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNFNzRDM0M7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICBtYXJnaW46IDAgMTBweDtcclxuICAgIH1cclxufSIsIi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2Uge1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSA+IGRpdiB7XG4gIHdpZHRoOiAzMy4zMzMzJTtcbiAgcGFkZGluZzogNXB4O1xuICBmbG9hdDogbGVmdDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgaDMge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IGltZyB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5yb3VuZGVkLWJ0biB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG59XG5cbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBjb2xvcjogIzU1NTtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi5yb3VuZGVkLWJ0bjpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMwYTdiZmY7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDIwcHggMTBweDtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCAubWVtYmVyLWRldGFpbC1pbWFnZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyAuc2stZmFkaW5nLWNpcmNsZSB7XG4gIHRvcDogNTAlO1xufVxuXG5idXR0b24uYnRuX3VwbG9hZCwgYnV0dG9uLmFkZC1tb3JlLWltYWdlLCBidXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiA1cHggMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm9yZGVyOiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbmJ1dHRvbi5idG4tY2hhbmdlLXBhc3N3b3JkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmJ1dHRvbi5idG5fZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogc2FsbW9uO1xufVxuXG4uYWRkLW1vcmUtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ubGFiZWwtYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5lcnJvci1tc2ctdXBsb2FkIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcGFkZGluZy1sZWZ0OiAwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5jb250YWluZXItdHdvLWJ1dHRvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5maWxlLXNlbGVjdGVkIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLmJ0bi1hZGQtYnVsayB7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5idG4tdXBsb2FkIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIGEge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciBhIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcbiAgd2lkdGg6IDIwMHB4O1xuICBtYXgtaGVpZ2h0OiAyMDBweDtcbiAgb2JqZWN0LWZpdDogY29udGFpbjtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuYnRuLWRvd25sb2FkLWNvbnRhaW5lciB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDIwcHggMTBweDtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCAubWVtYmVyLWRldGFpbC1pbWFnZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG59XG5cbmJ1dHRvbi5idG4tZG93bmxvYWQtaW1nIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXI6IDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmVkaXRfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5lZGl0X2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5idG4tcmlnaHQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4uZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/list-order/list-order.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/list-order/list-order.component.ts ***!
  \******************************************************************************************/
/*! exports provided: ListOrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderComponent", function() { return ListOrderComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






// import Swal from 'sweetalert2';
// import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';
var ListOrderComponent = /** @class */ (function () {
    function ListOrderComponent(memberService, productService, zone, orderHistoryService) {
        this.memberService = memberService;
        this.productService = productService;
        this.zone = zone;
        this.orderHistoryService = orderHistoryService;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__;
        this.errorLabel = false;
        this.errorMessage = false;
        this.edit = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.mci_project = false;
        this.programType = "";
        this.allBast = [];
        this.allSuratKuasa = [];
        this.loading = false;
        this.loadingBAST = [];
        this.loadingSuratKuasa = [];
        this.password = "";
        this.sig_kontraktual = false;
    }
    ListOrderComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ListOrderComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    ListOrderComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    ListOrderComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this, query, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.warn("this detail", this.detail);
                        program = localStorage.getItem('programName');
                        _this = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program) {
                                if (element.type == "reguler") {
                                    _this.mci_project = true;
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this.programType = "custom_kontraktual";
                                    _this.sig_kontraktual = true;
                                }
                                else {
                                    _this.mci_project = false;
                                }
                            }
                        });
                        this.memberDetail = this.detail;
                        query = {
                            search: {
                                username: this.memberDetail.id_toko,
                                status: {
                                    in: ["PROCESSED", "COMPLETED"]
                                }
                            },
                            limit_per_page: 0
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderHistoryService.searchAllReportSalesOrder(query)];
                    case 2:
                        result = _a.sent();
                        this.listOrder = result.values;
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ListOrderComponent.prototype.backToDetail = function () {
        // console.log("Edit member",this.back);
        this.back[0][this.back[1]]();
        // this.back[0]();
    };
    ListOrderComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return pattern.test(str);
    };
    ListOrderComponent.prototype.downloadImage = function (url) {
        var filename;
        filename = this.lastPathURL(url);
        fetch(url).then(function (t) {
            return t.blob().then(function (b) {
                var a = document.createElement("a");
                a.href = URL.createObjectURL(b);
                a.setAttribute("download", filename);
                a.click();
            });
        });
    };
    ListOrderComponent.prototype.lastPathURL = function (url) {
        var resURL = url.split("/");
        console.log("resURL", resURL);
        var lastNumber = resURL.length - 1;
        return resURL[lastNumber];
    };
    ListOrderComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    ListOrderComponent.prototype.checkFileType = function (text) {
        var splitFileName = text.split('.');
        var extensFile = splitFileName[splitFileName.length - 1];
        var extens = extensFile.toLowerCase();
        return extens == 'pdf' ? 'document' : 'image';
    };
    ListOrderComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__["OrderhistoryService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ListOrderComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ListOrderComponent.prototype, "back", void 0);
    ListOrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-order',
            template: __webpack_require__(/*! raw-loader!./list-order.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/finance-invoice-report/list-order/list-order.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./list-order.component.scss */ "./src/app/layout/modules/finance-invoice-report/list-order/list-order.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"],
            _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__["OrderhistoryService"]])
    ], ListOrderComponent);
    return ListOrderComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/update-invoice/finance-invoice-report.update-invoice.component.scss":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/update-invoice/finance-invoice-report.update-invoice.component.scss ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n.pb-framer .clr-white {\n  color: white;\n}\n.file-selected {\n  color: red;\n}\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n.container-two-button {\n  display: flex;\n}\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n.btn-upload, .btn-submit {\n  margin-right: 10px;\n}\n.btn-upload {\n  border-color: black;\n}\n.btn-submit {\n  background-color: #3498db;\n  color: white;\n}\n.member-bulk-update-container {\n  padding: 10px !important;\n}\n.bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n.ng-pristine {\n  color: white !important;\n}\n.ng-pristine {\n  color: black !important;\n  width: 100%;\n  border-radius: 5px;\n  padding: 6px 0px;\n}\nlabel {\n  color: black;\n}\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 10px;\n  background-color: #3498db;\n  color: white;\n  height: 30px;\n  align-items: center;\n  display: flex;\n  right: 10px;\n  padding-top: 0px;\n  padding-right: 10px;\n  padding-bottom: 0;\n  padding-left: 10px;\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #000;\n  margin-bottom: 10px;\n  margin-top: 20px;\n}\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 0px;\n  border: 1px solid #f8f9fa;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZmluYW5jZS1pbnZvaWNlLXJlcG9ydC91cGRhdGUtaW52b2ljZS9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGZpbmFuY2UtaW52b2ljZS1yZXBvcnRcXHVwZGF0ZS1pbnZvaWNlXFxmaW5hbmNlLWludm9pY2UtcmVwb3J0LnVwZGF0ZS1pbnZvaWNlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9maW5hbmNlLWludm9pY2UtcmVwb3J0L3VwZGF0ZS1pbnZvaWNlL2ZpbmFuY2UtaW52b2ljZS1yZXBvcnQudXBkYXRlLWludm9pY2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUNDSjtBREFJO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNFUjtBREFJO0VBQ0kscUJBQUE7QUNFUjtBREFJO0VBQ0ksWUFBQTtBQ0VSO0FERUE7RUFDSSxVQUFBO0FDQ0o7QURFQTtFQUNJLGdCQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQ0NKO0FERUE7RUFDSSxhQUFBO0FDQ0o7QURFQTtFQUNJLG1CQUFBO0FDQ0o7QURFQTtFQUNJLGtCQUFBO0FDQ0o7QURFQTtFQUNJLG1CQUFBO0FDQ0o7QURFQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtBQ0NKO0FERUE7RUFDSSx3QkFBQTtBQ0NKO0FERUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7QUNDSjtBREVBO0VBQ0ksdUJBQUE7QUNDSjtBREVBO0VBQ0ksdUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0NKO0FERUE7RUFDSSxZQUFBO0FDQ0o7QURFSTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7QUNDUjtBREFRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0NaO0FEQVk7RUFDSSxpQkFBQTtBQ0VoQjtBRENRO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFFQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDQVo7QURHSTtFQUNJLGFBQUE7QUNEUjtBREVRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0FaO0FESUk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBRUEsbUJBQUE7RUFDQSxnQkFBQTtBQ0hSO0FETVE7RUFDSSxrQkFBQTtBQ0paO0FETVE7RUFDSSxnQkFBQTtBQ0paO0FETVE7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDSlo7QURRUTtFQUNJLHNCQUFBO0FDTlo7QURPWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDTGhCO0FEV0E7RUFDSSxnQkFBQTtFQUVBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ1JKO0FEYUE7RUFDSSxnQkFBQTtFQUVBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUNWSjtBRFlBO0VBQ1EsbUJBQUE7RUFFQSxhQUFBO0FDVlIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9maW5hbmNlLWludm9pY2UtcmVwb3J0L3VwZGF0ZS1pbnZvaWNlL2ZpbmFuY2UtaW52b2ljZS1yZXBvcnQudXBkYXRlLWludm9pY2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGItZnJhbWVye1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgY29sb3I6ICNlNjdlMjI7XHJcbiAgICAucHJvZ3Jlc3NiYXJ7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgei1pbmRleDogLTE7XHJcbiAgICB9XHJcbiAgICAudGVybWluYXRlZC5wcm9ncmVzc2JhcntcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbiAgICB9XHJcbiAgICAuY2xyLXdoaXRle1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxufVxyXG5cclxuLmZpbGUtc2VsZWN0ZWQge1xyXG4gICAgY29sb3I6IHJlZDtcclxufVxyXG5cclxuLmVycm9yLW1zZy11cGxvYWQge1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIHBhZGRpbmctbGVmdDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNvbnRhaW5lci10d28tYnV0dG9uIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5idG4tYWRkLWJ1bGsge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLmJ0bi11cGxvYWQsIC5idG4tc3VibWl0IHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuLmJ0bi11cGxvYWQge1xyXG4gICAgYm9yZGVyLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLmJ0bi1zdWJtaXQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLm1lbWJlci1idWxrLXVwZGF0ZS1jb250YWluZXIge1xyXG4gICAgcGFkZGluZzogMTBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYmctZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5uZy1wcmlzdGluZXtcclxuICAgIGNvbG9yIDogd2hpdGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm5nLXByaXN0aW5le1xyXG4gICAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIHBhZGRpbmc6IDZweCAwcHg7XHJcbn1cclxuXHJcbmxhYmVse1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcbi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDQ4cHg7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICAgICAgLy8gbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzAwMDtcclxuICAgICAgICAvLyB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICAgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYWw7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2Y4ZjlmYTtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuLnJvdW5kZWQtYnRuIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDQwJTtcclxuICAgIC8vIG1hcmdpbi1yaWdodDogNTAlO1xyXG4gICAgfVxyXG5cclxuLnJvdW5kZWQtYnRuLWRhbmdlciB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgY29sb3I6ICM1NTU7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIH1cclxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2QTRGRiwgMTUlKTtcclxuICAgICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4gICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB9XHJcbi8vIC5kYXRlcGlja2VyLWlucHV0e1xyXG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlOyAgICBcclxuLy8gICAgIH1cclxuLy8gLmZvcm0tY29udHJvbHtcclxuLy8gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuLy8gICAgIHRvcDogMTBweDtcclxuLy8gICAgIGxlZnQ6IDBweDtcclxuLy8gfVxyXG4iLCIucGItZnJhbWVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGhlaWdodDogMjVweDtcbiAgY29sb3I6ICNlNjdlMjI7XG59XG4ucGItZnJhbWVyIC5wcm9ncmVzc2JhciB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJhY2tncm91bmQ6ICMzNDk4ZGI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAyNXB4O1xuICB6LWluZGV4OiAtMTtcbn1cbi5wYi1mcmFtZXIgLnRlcm1pbmF0ZWQucHJvZ3Jlc3NiYXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG59XG4ucGItZnJhbWVyIC5jbHItd2hpdGUge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5maWxlLXNlbGVjdGVkIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLmVycm9yLW1zZy11cGxvYWQge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcbn1cblxuLmNvbnRhaW5lci10d28tYnV0dG9uIHtcbiAgZGlzcGxheTogZmxleDtcbn1cblxuLmJ0bi1hZGQtYnVsayB7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5idG4tdXBsb2FkLCAuYnRuLXN1Ym1pdCB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLmJ0bi11cGxvYWQge1xuICBib3JkZXItY29sb3I6IGJsYWNrO1xufVxuXG4uYnRuLXN1Ym1pdCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLm1lbWJlci1idWxrLXVwZGF0ZS1jb250YWluZXIge1xuICBwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5iZy1kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubmctcHJpc3RpbmUge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuLm5nLXByaXN0aW5lIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDZweCAwcHg7XG59XG5cbmxhYmVsIHtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1pbi1oZWlnaHQ6IDQ4cHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG4gIGhlaWdodDogMzBweDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZGlzcGxheTogZmxleDtcbiAgcmlnaHQ6IDEwcHg7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMDAwO1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBwYWRkaW5nOiAwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmOGY5ZmE7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5yb3VuZGVkLWJ0biB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG59XG5cbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBjb2xvcjogIzU1NTtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi5yb3VuZGVkLWJ0bjpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMwYTdiZmY7XG4gIG91dGxpbmU6IG5vbmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/finance-invoice-report/update-invoice/finance-invoice-report.update-invoice.component.ts":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/finance-invoice-report/update-invoice/finance-invoice-report.update-invoice.component.ts ***!
  \*************************************************************************************************************************/
/*! exports provided: UpdateInvoiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateInvoiceComponent", function() { return UpdateInvoiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var UpdateInvoiceComponent = /** @class */ (function () {
    function UpdateInvoiceComponent(OrderhistoryService) {
        this.OrderhistoryService = OrderhistoryService;
        this.name = "";
        this.Members = [];
        this.form = {
            id_pel: "",
            nama_toko: "",
            nama_distributor: "",
            alamat_toko: "",
            kelurahan_toko: "-",
            kecamatan_toko: "-",
            kota_toko: "-",
            provinsi_toko: "-",
            kode_pos_toko: "-",
            landmark_toko: "-",
            nama_pemilik: "",
            telp_pemilik: "",
            no_wa_pemilik: "",
            ktp_pemilik: "",
            foto_ktp_pemilik: "-",
            npwp_pemilik: "",
            foto_npwp_pemilik: "-",
            alamat_rumah: "",
            kelurahan_rumah: "",
            kecamatan_rumah: "",
            kota_rumah: "",
            provinsi_rumah: "",
            kode_pos_rumah: "",
            landmark_rumah: "-",
            description: "nonupfront",
            hadiah_dikuasakan: "TIDAK KUASA",
            nama_penerima_gopay: "-",
            ktp_penerima: "-",
            foto_ktp_penerima: "-",
            npwp_penerima: "-",
            foto_npwp_penerima: "-",
            telp_penerima_gopay: "-",
            no_wa_penerima: "-",
            nama_id_gopay: "-",
            jenis_akun: "-",
            saldo_terakhir: "-",
        };
        this.errorLabel = false;
        this.member_status_type = [
            { label: 'Superuser', value: 'superuser' },
            { label: 'Member', value: 'member', selected: 1 },
            { label: 'Marketing', value: 'marketing' },
            { label: 'Admin', value: 'admin' },
            { label: 'Merchant', value: 'merchant' }
        ];
        this.group_desc = [
            { label: 'Nonupfront', value: 'nonupfront' },
            { label: 'Upfront', value: 'upfront' }
        ];
        this.kuasa_option = [
            { label: 'TIDAK KUASA', value: 'TIDAK KUASA' },
            { label: 'KUASA', value: 'KUASA' }
        ];
        this.isBulkUpdate = false;
        this.showUploadButton = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.prodOnUpload = false;
        this.startUploading = false;
    }
    UpdateInvoiceComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    UpdateInvoiceComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.selectedFile = null;
                this.prodOnUpload = false;
                return [2 /*return*/];
            });
        });
    };
    UpdateInvoiceComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        // var reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]); //
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
        this.updateBulk.nativeElement.value = '';
    };
    UpdateInvoiceComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    UpdateInvoiceComponent.prototype.updateDataBulk = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, result, e_1, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.startUploading = true;
                        this.cancel = false;
                        payload = {
                            type: 'application/form-data',
                        };
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        console.log('file', this.selectedFile, this);
                        return [4 /*yield*/, this.OrderhistoryService.updateBulkInvoice(this.selectedFile, this, payload, "invoice_no")];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            this.firstLoad();
                        }
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.startUploading = false;
                        this.errorLabel = (e_1.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    UpdateInvoiceComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    UpdateInvoiceComponent.prototype.backTo = function () {
        window.history.back();
    };
    UpdateInvoiceComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('updateBulk', { static: false }),
        __metadata("design:type", Object)
    ], UpdateInvoiceComponent.prototype, "updateBulk", void 0);
    UpdateInvoiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-update-invoice',
            template: __webpack_require__(/*! raw-loader!./finance-invoice-report.update-invoice.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/finance-invoice-report/update-invoice/finance-invoice-report.update-invoice.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./finance-invoice-report.update-invoice.component.scss */ "./src/app/layout/modules/finance-invoice-report/update-invoice/finance-invoice-report.update-invoice.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"]])
    ], UpdateInvoiceComponent);
    return UpdateInvoiceComponent;
}());



/***/ })

}]);
//# sourceMappingURL=modules-finance-invoice-report-finance-invoice-report-module.js.map