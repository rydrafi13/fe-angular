(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin-group-admin-group-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-group/add/admin-group.add.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-group/add/admin-group.add.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n    <app-page-header [heading]=\"'Add New Member'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n    <div>\r\n        <div class=\"card card-detail\">\r\n            <div class=\"card-header\">\r\n                <button class=\"btn save_button\" (click)=\"formSubmitAddMember()\" type=\"submit\"\r\n                    [routerLinkActive]=\"['router-link-active']\" *ngIf=\"!errorMessage\"><i class=\"fa fa-fw fa-save\"\r\n                        style=\"transform: translate(-3px, -1px)\"></i>Submit</button>\r\n                <button class=\"btn back_button\" (click)=\"backTo()\"><i class=\"fa fa-fw fa-angle-left\"></i>Back</button>\r\n            </div>\r\n\r\n            <div class=\"card-content\">\r\n                <div class=\"member-detail\">\r\n                    <div class=\"row\" *ngIf=\"!errorMessage\">\r\n                        <div class=\"col-md-8\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2> Admin Group Details </h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n                                        <label>Group Name</label>\r\n                                        <form-input name=\"group_name\" [(ngModel)]=\"form.group_name\" [type]=\"'text'\" [placeholder]=\"'Group Name'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n                                        <div *ngIf=\"alertGroupname\" class=\"empty-alert\">Group Name is Required</div>\r\n\r\n                                        <label>Group Description</label>\r\n                                        <form-input name=\"description\" [(ngModel)]=\"form.description\" [type]=\"'text'\" [placeholder]=\"'Group Description'\">\r\n                                            autofocus required>\r\n                                        </form-input>\r\n                                        <div *ngIf=\"alertDescription\" class=\"empty-alert\">Group Description is Required</div>\r\n\r\n                                        <div class=\"header-access-list\">Access List</div>\r\n                                        <div *ngFor=\"let value of usrMngtData; let index = index;\">\r\n                                            <input\r\n                                                type=\"checkbox\" id=\"{{value.name}}\" value={{value.name}}\r\n                                                (change)=\"checkList(index)\"\r\n                                            />\r\n                                            <label class=\"label-access-list\" for=\"{{value.name}}\">{{value.name}}</label>\r\n                                        </div>\r\n                                        <div *ngIf=\"alertAccessList\" class=\"empty-alert\">Access List is Required</div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n                        <div class=\"text-message\">\r\n                            <i class=\"fas fa-ban\"></i>\r\n                            {{errorMessage}}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <!-- <form #form=\"ngForm\" (ngSubmit)=\"formSubmitAddMember(form.value)\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-6\">\r\n                        <label>Full Name</label>\r\n                        <form-input name=\"full_name\" [type]=\"'text'\" [placeholder]=\"'Full Name'\" [(ngModel)]=\"full_name\"\r\n                            required></form-input>\r\n\r\n                        <label>Email</label>\r\n                        <form-input name=\"email\" [placeholder]=\"'Email'\" [type]=\"'text'\" [(ngModel)]=\"email\" required>\r\n                        </form-input>\r\n\r\n                        <label>Password</label>\r\n                        <form-input name=\"password\" [placeholder]=\"'Password'\" [type]=\"'password'\"\r\n                            [(ngModel)]=\"password\" required></form-input>\r\n\r\n                    \r\n                        <label>Cell Phone</label>\r\n                        <form-input name=\"cell_phone\" [placeholder]=\"'Cell Phone'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"cell_phone\" required></form-input>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>City</label>\r\n                        <form-input name=\"mcity\" [placeholder]=\"'City'\" [type]=\"'text'\" [(ngModel)]=\"mcity\" required>\r\n                        </form-input>\r\n                    </div>\r\n\r\n                    <div class=\"col-md-6\">\r\n                        <label>State</label>\r\n                        <form-input name=\"mstate\" [placeholder]=\"'State'\" [type]=\"'text'\" [(ngModel)]=\"mstate\" required>\r\n                        </form-input>\r\n\r\n                        <label>Country</label>\r\n                        <form-input name=\"mcountry\" [placeholder]=\"'Country'\" [type]=\"'text'\" [(ngModel)]=\"mcountry\"\r\n                            required></form-input>\r\n\r\n                        <label>Address</label>\r\n                        <form-input name=\"maddress1\" [placeholder]=\"'Address'\" [type]=\"'textarea'\"\r\n                            [(ngModel)]=\"maddress1\" required></form-input>\r\n\r\n                        <label>Post Code</label>\r\n                        <form-input name=\"mpostcode\" [placeholder]=\"'Post Code'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"mpostcode\" required></form-input>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label>Gender</label>\r\n                            <select class=\"form-control\" name=\"gender\" [(ngModel)]=\"gender\">\r\n                                <option *ngFor=\"let d of gender_type\" value=\"{{d.value}}\">{{d.label}}</option>\r\n                            </select>\r\n                        </div>\r\n\r\n\r\n                        <label>Marital Status</label>\r\n                        <form-select name=\"marital_status\" [(ngModel)]=\"marital_status\" [data]=\"marital_status_type\">\r\n                        </form-select>\r\n\r\n                        <label>Member Type</label>\r\n                        <form-select name=\"type\" [(ngModel)]=\"type\" [data]=\"member_type\"></form-select>\r\n                    </div>\r\n                </div>\r\n\r\n            </form> -->\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-group/admin-group.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-group/admin-group.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"' '\" [icon]=\"'fa-table'\"></app-page-header>\r\n  \r\n  <!-- <button (click)=\"getDownloadFileLint()\"><i class=\"fa fa-file-pdf-o\"></i>Download</button> -->\r\n  <!-- <iframe [src]=\"srcDownload\"></iframe> -->\r\n  <div *ngIf=\"Members&&memberDetail==false&&!errorMessage\">\r\n        <app-form-builder-table \r\n        [tableFormat] = \"tableFormat\" \r\n        [table_data]  = \"Members\" \r\n        [searchCallback]= \"[adminService, 'searchAdminGroup',this]\" \r\n        [total_page]=\"totalPage\"\r\n        >\r\n            \r\n        </app-form-builder-table>\r\n  </div>\r\n  <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n    <div class=\"text-message\">\r\n      <i class=\"fas fa-ban\"></i>\r\n      {{errorMessage}}\r\n    </div>\r\n  </div>\r\n  <div *ngIf=\"memberDetail\">\r\n    <app-admin-group-detail [back]=\"[this,backToHere]\" [detail]=\"memberDetail\"></app-admin-group-detail>\r\n</div>\r\n \r\n</div>\r\n\r\n "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-group/detail/admin-group.detail.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-group/detail/admin-group.detail.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button>\r\n    </div>\r\n  \r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\" *ngIf=\"adminDetail\">\r\n                                <label>Group Name</label>\r\n                                <div name=\"group_name\">{{adminDetail.group_name}}</div>\r\n\r\n                                <label>Description</label>\r\n                                <div name=\"description\">{{adminDetail.description}}</div>\r\n\r\n                                <label>Status</label>\r\n                                <div name=\"status\">{{adminDetail.status}}</div>\r\n\r\n                                <label>Access List</label>\r\n                                <!-- <div name=\"full_name\">{{pipeToComma(adminDetail.access_list.usr_mngt)}}</div> -->\r\n                                <div name=\"full_name\">\r\n                                    <div *ngFor=\"let item of adminDetail.access_list | keyvalue\">\r\n                                    <!-- <div name=\"full_name\" > -->\r\n                                        <b>{{item.key}}</b> : {{pipeToComma(item.value)}}\r\n                                        <br><br>\r\n                                    <!-- </div> -->\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            \r\n            \r\n         </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-admin-group-edit [back]=\"[this, 'editThis']\" [detail]=\"adminDetail\"></app-admin-group-edit>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-group/edit/admin-group.edit.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-group/edit/admin-group.edit.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\" *ngIf=\"!errorMessage\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span></button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n            <div class=\"row\" *ngIf=\"!errorMessage\">\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Edit Data Admin Group</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <label>Description</label>\r\n                                <form-input  name=\"description\" [type]=\"'text'\" [placeholder]=\"'Description'\"  [(ngModel)]=\"detail.description\" autofocus required></form-input>\r\n                                <div *ngIf=\"alertDescription\" class=\"empty-alert\">Description is Required</div>\r\n\r\n                                <label>Status</label>\r\n                                <form-select [(ngModel)]=\"detail.status\" [data]=\"memberStatus\"></form-select>\r\n                                <div *ngIf=\"alertStatus\" class=\"empty-alert\">Status is Required</div>\r\n                                <div *ngFor=\"let _accessList of accessList | keyvalue\">\r\n                                    <div class=\"header-access-list\">{{_accessList.key}}</div>\r\n                                    <div *ngFor=\"let value of _accessList.value; let index = index;\">\r\n                                        <input\r\n                                            type=\"checkbox\" id={{value.name}} value={{value.name}}\r\n                                            (change)=\"checkList(_accessList.key , index)\"\r\n                                            [checked]=\"value.checked == 1\"\r\n                                        />\r\n                                        <label class=\"label-access-list\" for=\"{{value.name}}\" class=\"label-access-list\">{{value.name}}</label>\r\n                                    </div>\r\n                                </div>\r\n                                <div *ngIf=\"alertAccessList\" class=\"empty-alert\">Access List is Required</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n                <div class=\"text-message\">\r\n                    <i class=\"fas fa-ban\"></i>\r\n                    {{errorMessage}}\r\n                </div>\r\n            </div>\r\n         </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/layout/modules/admin-group/add/admin-group.add.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/admin-group/add/admin-group.add.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.ng-pristine {\n  color: white !important;\n}\n\n.ng-pristine {\n  color: black !important;\n  width: 100%;\n  border-radius: 5px;\n  padding: 6px 0px;\n}\n\nlabel {\n  color: black;\n}\n\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 10px;\n  background-color: #3498db;\n  color: white;\n  height: 30px;\n  align-items: center;\n  display: flex;\n  right: 10px;\n  padding-top: 0px;\n  padding-right: 10px;\n  padding-bottom: 0;\n  padding-left: 10px;\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 0px;\n  border: 1px solid #f8f9fa;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.rounded-btn {\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n  margin-left: 40%;\n  margin-right: 50%;\n}\n\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n\n.empty-alert {\n  color: red;\n  font-size: 10px;\n  position: relative;\n  bottom: 10px;\n}\n\n.header-access-list {\n  margin-top: 28px;\n}\n\n.label-access-list {\n  font-size: 15px;\n  position: relative;\n  bottom: 1px;\n  left: 6px;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 19px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #EF7C8E;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tZ3JvdXAvYWRkL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcYWRtaW4tZ3JvdXBcXGFkZFxcYWRtaW4tZ3JvdXAuYWRkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbi1ncm91cC9hZGQvYWRtaW4tZ3JvdXAuYWRkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVJO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQ0NSOztBREFRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0NaOztBREFZO0VBQ0ksaUJBQUE7QUNFaEI7O0FEQ1E7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNBWjs7QURHSTtFQUNJLGFBQUE7QUNEUjs7QURFUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNBWjs7QURJSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDRlI7O0FES1E7RUFDSSxrQkFBQTtBQ0haOztBREtRO0VBQ0ksZ0JBQUE7QUNIWjs7QURLUTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNIWjs7QURPUTtFQUNJLHNCQUFBO0FDTFo7O0FETVk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ0poQjs7QURVQTtFQUVRLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNKLGdCQUFBO0VBQ0EsaUJBQUE7QUNQSjs7QURTQTtFQUNRLG1CQUFBO0VBRUEsYUFBQTtBQ1BSOztBRGtCQTtFQUNJLFVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDZko7O0FEa0JBO0VBQ0ksZ0JBQUE7QUNmSjs7QURrQkE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtBQ2ZKOztBRGtCQTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDZko7O0FEaUJJO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ2ZSIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tZ3JvdXAvYWRkL2FkbWluLWdyb3VwLmFkZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZy1kZWxldGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG5cclxuLm5nLXByaXN0aW5le1xyXG4gICAgY29sb3IgOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubmctcHJpc3RpbmV7XHJcbiAgICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcGFkZGluZzogNnB4IDBweDtcclxufVxyXG5cclxubGFiZWx7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDhweDtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICAvLyBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgICBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hbDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZjhmOWZhO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4ucm91bmRlZC1idG4ge1xyXG4gICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICM1NmE0ZmY7XHJcbiAgICBtYXJnaW4tbGVmdDogNDAlO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1MCU7XHJcbiAgICB9XHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogZGFya2VuKCM1NkE0RkYsIDE1JSk7XHJcbiAgICAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgfVxyXG4vLyAuZGF0ZXBpY2tlci1pbnB1dHtcclxuLy8gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsgICAgXHJcbi8vICAgICB9XHJcbi8vIC5mb3JtLWNvbnRyb2x7XHJcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vICAgICB0b3A6IDEwcHg7XHJcbi8vICAgICBsZWZ0OiAwcHg7XHJcbi8vIH1cclxuXHJcbi5lbXB0eS1hbGVydCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYm90dG9tOiAxMHB4O1xyXG59XHJcblxyXG4uaGVhZGVyLWFjY2Vzcy1saXN0IHtcclxuICAgIG1hcmdpbi10b3A6MjhweDtcclxufVxyXG5cclxuLmxhYmVsLWFjY2Vzcy1saXN0IHtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJvdHRvbTogMXB4O1xyXG4gICAgbGVmdDogNnB4O1xyXG59XHJcblxyXG4uZXJyb3ItbWVzc2FnZSB7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGhlaWdodDoxMDB2aDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZToxOXB4O1xyXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICBcclxuICAgIC50ZXh0LW1lc3NhZ2Uge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNFRjdDOEU7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICBtYXJnaW46IDAgMTBweDtcclxuICAgIH1cclxufSIsIi5iZy1kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubmctcHJpc3RpbmUge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuLm5nLXByaXN0aW5lIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDZweCAwcHg7XG59XG5cbmxhYmVsIHtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1pbi1oZWlnaHQ6IDQ4cHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG4gIGhlaWdodDogMzBweDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZGlzcGxheTogZmxleDtcbiAgcmlnaHQ6IDEwcHg7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgcGFkZGluZzogMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZjhmOWZhO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ucm91bmRlZC1idG4ge1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xuICBtYXJnaW4tbGVmdDogNDAlO1xuICBtYXJnaW4tcmlnaHQ6IDUwJTtcbn1cblxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzBhN2JmZjtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuLmVtcHR5LWFsZXJ0IHtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJvdHRvbTogMTBweDtcbn1cblxuLmhlYWRlci1hY2Nlc3MtbGlzdCB7XG4gIG1hcmdpbi10b3A6IDI4cHg7XG59XG5cbi5sYWJlbC1hY2Nlc3MtbGlzdCB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3R0b206IDFweDtcbiAgbGVmdDogNnB4O1xufVxuXG4uZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTlweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0VGN0M4RTtcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/admin-group/add/admin-group.add.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/layout/modules/admin-group/add/admin-group.add.component.ts ***!
  \*****************************************************************************/
/*! exports provided: AdminGroupAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminGroupAddComponent", function() { return AdminGroupAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/admin/admin.service */ "./src/app/services/admin/admin.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var AdminGroupAddComponent = /** @class */ (function () {
    function AdminGroupAddComponent(adminService) {
        this.adminService = adminService;
        this.name = "";
        this.Members = [];
        this.form = {
            group_name: "",
            description: "",
            access_list: {
                "usr_mngt": []
            },
        };
        this.alertGroupname = false;
        this.alertDescription = false;
        this.alertAccessList = false;
        this.errorLabel = false;
        this.errorMessage = false;
        this.usrMngtData = [];
    }
    AdminGroupAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AdminGroupAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.adminService.getAccessListApp().then(function (result) {
                                result.usr_mngt.forEach(function (element) {
                                    _this.usrMngtData.push({
                                        name: element.name,
                                        checked: 0
                                    });
                                });
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        this.errorLabel = (error_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminGroupAddComponent.prototype.formSubmitAddMember = function () {
        return __awaiter(this, void 0, void 0, function () {
            var usrMngtForm;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.form.group_name || this.form.group_name.length == 0) {
                            this.alertGroupname = true;
                        }
                        else {
                            this.alertGroupname = false;
                        }
                        if (!this.form.description || this.form.description.length == 0) {
                            this.alertDescription = true;
                        }
                        else {
                            this.alertDescription = false;
                        }
                        usrMngtForm = [];
                        this.usrMngtData.forEach(function (element) {
                            if (element.checked == 1)
                                usrMngtForm.push(element.name);
                        });
                        if (usrMngtForm.length == 0) {
                            this.alertAccessList = true;
                        }
                        else {
                            this.alertAccessList = false;
                        }
                        if (!(!this.alertGroupname && !this.alertDescription && !this.alertAccessList)) return [3 /*break*/, 2];
                        this.form.access_list.usr_mngt = usrMngtForm;
                        return [4 /*yield*/, this.adminService.addAdminGroup(this.form).then(function () {
                                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire("Group Admin Has Been Added!")
                                    .then(function () {
                                    _this.backTo();
                                });
                            }).catch(function (error) {
                                _this.errorLabel = (error.message);
                                var message = _this.errorLabel;
                                if (_this.errorLabel.includes("Unauthorized") || _this.errorLabel.includes("unauthorized") || _this.errorLabel.includes("access not permitted")) {
                                    message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                                }
                                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                                    icon: 'error',
                                    title: message,
                                });
                            })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    AdminGroupAddComponent.prototype.backTo = function () {
        window.history.back();
    };
    AdminGroupAddComponent.prototype.checkList = function (index) {
        if (this.usrMngtData[index].checked == 1) {
            this.usrMngtData[index].checked = 0;
        }
        else if (this.usrMngtData[index].checked == 0) {
            this.usrMngtData[index].checked = 1;
        }
    };
    AdminGroupAddComponent.ctorParameters = function () { return [
        { type: _services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"] }
    ]; };
    AdminGroupAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-group-add',
            template: __webpack_require__(/*! raw-loader!./admin-group.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-group/add/admin-group.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./admin-group.add.component.scss */ "./src/app/layout/modules/admin-group/add/admin-group.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"]])
    ], AdminGroupAddComponent);
    return AdminGroupAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-group/admin-group-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/modules/admin-group/admin-group-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: AdminRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutingModule", function() { return AdminRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_group_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-group.component */ "./src/app/layout/modules/admin-group/admin-group.component.ts");
/* harmony import */ var _add_admin_group_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/admin-group.add.component */ "./src/app/layout/modules/admin-group/add/admin-group.add.component.ts");
/* harmony import */ var _detail_admin_group_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detail/admin-group.detail.component */ "./src/app/layout/modules/admin-group/detail/admin-group.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// import { UserPointHistoryComponent } from './detail/member-point-history/member-point-history.component';
var routes = [
    {
        path: '', component: _admin_group_component__WEBPACK_IMPORTED_MODULE_2__["AdminGroupComponent"],
    },
    {
        path: 'add', component: _add_admin_group_add_component__WEBPACK_IMPORTED_MODULE_3__["AdminGroupAddComponent"]
    },
    {
        path: 'detail', component: _detail_admin_group_detail_component__WEBPACK_IMPORTED_MODULE_4__["AdminGroupDetailComponent"]
    },
];
var AdminRoutingModule = /** @class */ (function () {
    function AdminRoutingModule() {
    }
    AdminRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AdminRoutingModule);
    return AdminRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-group/admin-group.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/admin-group/admin-group.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "iframe {\n  width: 0px;\n  height: 0px;\n  border: 0px medium none;\n  visibility: hidden;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 19px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tZ3JvdXAvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxhZG1pbi1ncm91cFxcYWRtaW4tZ3JvdXAuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLWdyb3VwL2FkbWluLWdyb3VwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksVUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0NKOztBRENJO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ0NSIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tZ3JvdXAvYWRtaW4tZ3JvdXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpZnJhbWV7XHJcbiAgICB3aWR0aDowcHg7XHJcbiAgICBoZWlnaHQ6IDBweDtcclxuICAgIGJvcmRlcjowcHggbWVkaXVtIG5vbmU7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbn1cclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjE5cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgIFxyXG4gICAgLnRleHQtbWVzc2FnZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0U3NEMzQztcclxuICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgIG1hcmdpbjogMCAxMHB4O1xyXG4gICAgfVxyXG59IiwiaWZyYW1lIHtcbiAgd2lkdGg6IDBweDtcbiAgaGVpZ2h0OiAwcHg7XG4gIGJvcmRlcjogMHB4IG1lZGl1bSBub25lO1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG59XG5cbi5lcnJvci1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxOXB4O1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5lcnJvci1tZXNzYWdlIC50ZXh0LW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xuICBwYWRkaW5nOiAyMHB4O1xuICBtYXJnaW46IDAgMTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/admin-group/admin-group.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/admin-group/admin-group.component.ts ***!
  \*********************************************************************/
/*! exports provided: AdminGroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminGroupComponent", function() { return AdminGroupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/admin/admin.service */ "./src/app/services/admin/admin.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var AdminGroupComponent = /** @class */ (function () {
    function AdminGroupComponent(adminService, sanitizer) {
        this.adminService = adminService;
        this.sanitizer = sanitizer;
        this.Members = [];
        this.row_id = "_id";
        this.tableFormat = {
            title: 'Admin  Group',
            label_headers: [
                { label: 'group name', visible: true, type: 'string', data_row_name: 'group_name' },
                { label: 'description', visible: true, type: 'string', data_row_name: 'description' },
                { label: 'status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'access list', visible: true, type: 'usr_mngt', data_row_name: 'access_list' },
            ],
            row_primary_key: '_id',
            formOptions: {
                addForm: true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'Members',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.errorMessage = false;
        this.memberDetail = false;
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    AdminGroupComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AdminGroupComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.adminService.getAdminGroup()];
                    case 1:
                        result = _a.sent();
                        this.Members = result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminGroupComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    this.memberDetail = this.Members.find(function (member) { return member._id == rowData._id; });
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    AdminGroupComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.memberDetail = false;
                obj.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    AdminGroupComponent.prototype.onDownload = function (downloadLint) {
        var srcDownload = downloadLint;
        //let srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
        //console.log(this.srcDownload);
        this.getDownloadFileLint();
    };
    AdminGroupComponent.prototype.getDownloadFileLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, downloadLint, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        result = void 0;
                        this.service = this.adminService;
                        return [4 /*yield*/, this.adminService.getDownloadFileLint()];
                    case 1:
                        result = _a.sent();
                        downloadLint = result.result;
                        //console.log(downloadLint);
                        //To running other subfunction with together automatically
                        this.onDownload(downloadLint);
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminGroupComponent.ctorParameters = function () { return [
        { type: _services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
    ]; };
    AdminGroupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-group',
            template: __webpack_require__(/*! raw-loader!./admin-group.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-group/admin-group.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./admin-group.component.scss */ "./src/app/layout/modules/admin-group/admin-group.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
    ], AdminGroupComponent);
    return AdminGroupComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-group/admin-group.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/modules/admin-group/admin-group.module.ts ***!
  \******************************************************************/
/*! exports provided: AdminGroupModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminGroupModule", function() { return AdminGroupModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _admin_group_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-group.component */ "./src/app/layout/modules/admin-group/admin-group.component.ts");
/* harmony import */ var _add_admin_group_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/admin-group.add.component */ "./src/app/layout/modules/admin-group/add/admin-group.add.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _admin_group_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./admin-group-routing.module */ "./src/app/layout/modules/admin-group/admin-group-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _detail_admin_group_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./detail/admin-group.detail.component */ "./src/app/layout/modules/admin-group/detail/admin-group.detail.component.ts");
/* harmony import */ var _edit_admin_group_edit_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./edit/admin-group.edit.component */ "./src/app/layout/modules/admin-group/edit/admin-group.edit.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












// import { UserPointHistoryComponent } from './detail/user-point-history/member-point-history.component';
var AdminGroupModule = /** @class */ (function () {
    function AdminGroupModule() {
    }
    AdminGroupModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _admin_group_routing_module__WEBPACK_IMPORTED_MODULE_8__["AdminRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_9__["BsComponentModule"]
            ],
            declarations: [
                _admin_group_component__WEBPACK_IMPORTED_MODULE_2__["AdminGroupComponent"], _add_admin_group_add_component__WEBPACK_IMPORTED_MODULE_3__["AdminGroupAddComponent"], _detail_admin_group_detail_component__WEBPACK_IMPORTED_MODULE_10__["AdminGroupDetailComponent"], _edit_admin_group_edit_component__WEBPACK_IMPORTED_MODULE_11__["AdminGroupEditComponent"],
            ],
        })
    ], AdminGroupModule);
    return AdminGroupModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-group/detail/admin-group.detail.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-group/detail/admin-group.detail.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n  height: 48px;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.empty-alert {\n  color: red;\n  font-size: 10px;\n  position: relative;\n  bottom: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tZ3JvdXAvZGV0YWlsL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcYWRtaW4tZ3JvdXBcXGRldGFpbFxcYWRtaW4tZ3JvdXAuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbi1ncm91cC9kZXRhaWwvYWRtaW4tZ3JvdXAuZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FERUk7RUFDSSxrQkFBQTtFQUNBLFlBQUE7QUNDUjs7QURBUTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNDWjs7QURBWTtFQUNJLGlCQUFBO0FDRWhCOztBRENRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDSlo7O0FEQ1k7RUFDSSxpQkFBQTtBQ0NoQjs7QURJSTtFQUNJLGFBQUE7QUNGUjs7QURHUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNEWjs7QURnQkk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ2RSOztBRGlCUTtFQUNJLGtCQUFBO0FDZlo7O0FEaUJRO0VBQ0ksZ0JBQUE7QUNmWjs7QURpQlE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDZlo7O0FEa0JRO0VBQ0ksc0JBQUE7QUNoQlo7O0FEaUJZO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7QUNmaEI7O0FEc0JBO0VBQ0ksVUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNuQkoiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbi1ncm91cC9kZXRhaWwvYWRtaW4tZ3JvdXAuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWRlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcbi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGhlaWdodDogNDhweDtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmVkaXRfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIC5pbWFnZXtcclxuICAgIC8vICAgICBoM3tcclxuICAgIC8vICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgLy8gICAgIH1cclxuICAgIC8vICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgLy8gICAgICAgICBpbWd7XHJcbiAgICAvLyAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAvLyAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAvLyAgICAgICAgIH1cclxuICAgIC8vICAgICB9XHJcbiAgICAvLyB9XHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5tZW1iZXItZGV0YWlse1xyXG4gICAgICAgIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5lbXB0eS1hbGVydCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYm90dG9tOiAxMHB4O1xyXG59IiwiLmJnLWRlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG5cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiA0OHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5lbXB0eS1hbGVydCB7XG4gIGNvbG9yOiByZWQ7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3R0b206IDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/admin-group/detail/admin-group.detail.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/modules/admin-group/detail/admin-group.detail.component.ts ***!
  \***********************************************************************************/
/*! exports provided: AdminGroupDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminGroupDetailComponent", function() { return AdminGroupDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/admin/admin.service */ "./src/app/services/admin/admin.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var AdminGroupDetailComponent = /** @class */ (function () {
    function AdminGroupDetailComponent(adminService, router, route) {
        this.adminService = adminService;
        this.router = router;
        this.route = route;
        this.edit = false;
        this.errorLabel = false;
    }
    AdminGroupDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AdminGroupDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _id, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _id = this.detail._id;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.adminService.getAdminGroupByID(_id)];
                    case 2:
                        result = _a.sent();
                        if (result.length > 0) {
                            if (result[0]._id == this.detail._id) {
                                this.adminDetail = result[0];
                                // console.log("data member from firstload detail", this.memberDetail);
                            }
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AdminGroupDetailComponent.prototype.editThis = function () {
        // console.log(this.edit );
        this.edit = !this.edit;
        if (this.edit == false) {
            this.firstLoad();
        }
        // console.log(this.edit );
    };
    AdminGroupDetailComponent.prototype.backToTable = function () {
        // console.log(this.back);
        this.back[1](this.back[0]);
    };
    // async delete(){
    //   try {
    //     const delResult: any = await this.memberService.deleteMember(this.detail);
    //     console.log(delResult);
    //     if (delResult.error == false) {
    //       console.log(this.back[0]);
    //       this.back[0].memberDetail = false;
    //       this.back[0].firstLoad();
    //     // delete this.back[0].prodDetail;
    //     }
    //   } catch (error) {
    //     throw new TypeError(error.error.error);
    //   }
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
    // async pointHistory(){
    //   console.log('here')
    //   // this.router.navigate(['/member-point-history']);
    //   this.router.navigate(['administrator/memberadmin/point-history'],  {queryParams: {id: this.detail.email }})
    // }
    AdminGroupDetailComponent.prototype.pipeToComma = function (data) {
        return data.replace(/\|/g, ', ');
    };
    AdminGroupDetailComponent.ctorParameters = function () { return [
        { type: _services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AdminGroupDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AdminGroupDetailComponent.prototype, "back", void 0);
    AdminGroupDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-group-detail',
            template: __webpack_require__(/*! raw-loader!./admin-group.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-group/detail/admin-group.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./admin-group.detail.component.scss */ "./src/app/layout/modules/admin-group/detail/admin-group.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], AdminGroupDetailComponent);
    return AdminGroupDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-group/edit/admin-group.edit.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/admin-group/edit/admin-group.edit.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n  height: 48px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.card-detail .empty-alert {\n  color: red;\n  font-size: 10px;\n  position: relative;\n  bottom: 10px;\n}\n.card-detail .header-access-list {\n  margin-top: 28px;\n}\n.card-detail .label-access-list {\n  font-size: 15px;\n  position: relative;\n  bottom: 1px;\n  left: 6px;\n}\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n.error-message .text-message {\n  background: #EF7C8E;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tZ3JvdXAvZWRpdC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGFkbWluLWdyb3VwXFxlZGl0XFxhZG1pbi1ncm91cC5lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbi1ncm91cC9lZGl0L2FkbWluLWdyb3VwLmVkaXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtFQUNBLFlBQUE7QUNBUjtBRENRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0FaO0FEQ1k7RUFDSSxpQkFBQTtBQ0NoQjtBREVRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTFo7QURFWTtFQUNJLGlCQUFBO0FDQWhCO0FESVE7RUFDSSx5QkFBQTtBQ0ZaO0FES0k7RUFDSSxhQUFBO0FDSFI7QURLUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNIWjtBRE9JO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNMUjtBRFNRO0VBQ0ksZ0JBQUE7QUNQWjtBRFNRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ1BaO0FEU1E7RUFDSSxnQkFBQTtBQ1BaO0FEUVk7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNOaEI7QURRWTtFQUNJLHlCQUFBO0FDTmhCO0FEUVk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDTmhCO0FEUVk7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDTmhCO0FET2dCO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNMcEI7QURXUTtFQUNJLHNCQUFBO0FDVFo7QURVWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDUmhCO0FEY0k7RUFDSSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ1pSO0FEZUk7RUFDSSxnQkFBQTtBQ2JSO0FEZ0JJO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7QUNkUjtBRG1CQTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDaEJKO0FEa0JJO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ2hCUiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLWdyb3VwL2VkaXQvYWRtaW4tZ3JvdXAuZWRpdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGhlaWdodDogNDhweDtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbi50cnVle1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgIFxyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgIFxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW1hZ2V7XHJcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgID5kaXZ7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzMuMzMzMyU7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGgze1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgICAgIGltZ3tcclxuICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuZW1wdHktYWxlcnQge1xyXG4gICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBib3R0b206IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmhlYWRlci1hY2Nlc3MtbGlzdCB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDoyOHB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAubGFiZWwtYWNjZXNzLWxpc3Qge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgYm90dG9tOiAxcHg7XHJcbiAgICAgICAgbGVmdDogNnB4O1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuLmVycm9yLW1lc3NhZ2Uge1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBoZWlnaHQ6MTAwdmg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6MjZweDtcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRUY3QzhFO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn0iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGhlaWdodDogNDhweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlID4gZGl2IHtcbiAgd2lkdGg6IDMzLjMzMzMlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGZsb2F0OiBsZWZ0O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSBoMyB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQgaW1nIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cbi5jYXJkLWRldGFpbCAuZW1wdHktYWxlcnQge1xuICBjb2xvcjogcmVkO1xuICBmb250LXNpemU6IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYm90dG9tOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5oZWFkZXItYWNjZXNzLWxpc3Qge1xuICBtYXJnaW4tdG9wOiAyOHB4O1xufVxuLmNhcmQtZGV0YWlsIC5sYWJlbC1hY2Nlc3MtbGlzdCB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3R0b206IDFweDtcbiAgbGVmdDogNnB4O1xufVxuXG4uZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0VGN0M4RTtcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/admin-group/edit/admin-group.edit.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/admin-group/edit/admin-group.edit.component.ts ***!
  \*******************************************************************************/
/*! exports provided: AdminGroupEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminGroupEditComponent", function() { return AdminGroupEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/admin/admin.service */ "./src/app/services/admin/admin.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var AdminGroupEditComponent = /** @class */ (function () {
    // public usrMngtForm: any = [];
    function AdminGroupEditComponent(adminService) {
        this.adminService = adminService;
        this.errorLabel = false;
        this.errorMessage = false;
        this.loading = false;
        this.memberStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
        this.groupNameData = [];
        this.alertGroupname = false;
        this.alertDescription = false;
        this.alertStatus = false;
        this.alertAccessList = false;
        this.usrMngtData = [];
    }
    AdminGroupEditComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AdminGroupEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _access_list_1, err_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.memberStatus.forEach(function (element, index) {
                            if (element.value == _this.detail.status) {
                                _this.memberStatus[index].selected = 1;
                            }
                        });
                        _access_list_1 = this.detail.access_list;
                        return [4 /*yield*/, this.adminService.getAccessListApp().then(function (result) {
                                _this.accessList = result;
                                var _loop_1 = function (key) {
                                    _this.accessList[key].forEach(function (element) {
                                        if (key in _access_list_1 && _access_list_1[key].includes(element.name)) {
                                            element.checked = true;
                                        }
                                        // this.usrMngtData.push(
                                        //   {
                                        //     name : element.name,
                                        //     checked : _usr_mngt.includes(element.name),
                                        //   }
                                        // );
                                    });
                                };
                                for (var key in _this.accessList) {
                                    _loop_1(key);
                                }
                                // result.usr_mngt.forEach((element) => {
                                //   this.usrMngtData.push(
                                //     {
                                //       name : element.name,
                                //       checked : _usr_mngt.includes(element.name),
                                //     }
                                //   );
                                // });
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        console.log("err", err_1);
                        this.errorLabel = (err_1.message);
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminGroupEditComponent.prototype.backToDetail = function () {
        // console.log(this.back);
        this.back[0][this.back[1]]();
    };
    AdminGroupEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var usrMngtForm, _loop_2, this_1, key, new_form, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.detail.group_name || this.detail.group_name.length == 0) {
                            this.alertGroupname = true;
                        }
                        else {
                            this.alertGroupname = false;
                        }
                        if (!this.detail.description || this.detail.description.length == 0) {
                            this.alertDescription = true;
                        }
                        else {
                            this.alertDescription = false;
                        }
                        if (!this.detail.status || this.detail.status.length == 0) {
                            this.alertStatus = true;
                        }
                        else {
                            this.alertStatus = false;
                        }
                        usrMngtForm = {};
                        _loop_2 = function (key) {
                            // this.accessList[key].forEach((element) => {
                            // if(key in _access_list && _access_list[key].includes(element.name)) {
                            //   element.checked = true;
                            // }
                            // this.usrMngtData.push(
                            //   {
                            //     name : element.name,
                            //     checked : _usr_mngt.includes(element.name),
                            //   }
                            // );
                            // });
                            this_1.accessList[key].forEach(function (element) {
                                if (element.checked == true) {
                                    if (!(key in usrMngtForm))
                                        usrMngtForm[key] = [];
                                    usrMngtForm[key].push(element.name);
                                }
                            });
                        };
                        this_1 = this;
                        for (key in this.accessList) {
                            _loop_2(key);
                        }
                        // this.usrMngtData.forEach((element) => {
                        //   if(element.checked == 1) usrMngtForm.push(element.name);
                        // });
                        if (Object.keys(usrMngtForm).length === 0 && usrMngtForm.constructor === Object) {
                            this.alertAccessList = true;
                        }
                        else {
                            this.alertAccessList = false;
                        }
                        if (!(!this.alertGroupname && !this.alertDescription && !this.alertStatus && !this.alertAccessList)) return [3 /*break*/, 4];
                        new_form = {
                            "description": this.detail.description,
                            "status": this.detail.status,
                            "access_list": usrMngtForm
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.adminService.updateAdminGroup(this.detail.group_name, new_form).then(function () {
                                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire("Group Admin Has Been Edited!")
                                    .then(function () {
                                    _this.backToDetail();
                                });
                            }).catch(function (error) {
                                _this.errorLabel = (error.message);
                                var message = _this.errorLabel;
                                if (_this.errorLabel.includes("Unauthorized") || _this.errorLabel.includes("unauthorized") || _this.errorLabel.includes("access not permitted")) {
                                    message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                                }
                                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                                    icon: 'error',
                                    title: message,
                                });
                            })];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        alert("Error");
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AdminGroupEditComponent.prototype.checkList = function (key, index) {
        if (this.accessList[key][index].checked == true) {
            this.accessList[key][index].checked = false;
        }
        else if (this.accessList[key][index].checked != true) {
            this.accessList[key][index].checked = true;
        }
    };
    AdminGroupEditComponent.ctorParameters = function () { return [
        { type: _services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AdminGroupEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AdminGroupEditComponent.prototype, "back", void 0);
    AdminGroupEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-group-edit',
            template: __webpack_require__(/*! raw-loader!./admin-group.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-group/edit/admin-group.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./admin-group.edit.component.scss */ "./src/app/layout/modules/admin-group/edit/admin-group.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"]])
    ], AdminGroupEditComponent);
    return AdminGroupEditComponent;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin-group-admin-group-module.js.map