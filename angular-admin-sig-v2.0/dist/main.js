(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [ngClass]=\"developmentMode\">\r\n<div class=\"dev-mode\">\r\n    <h5>Development mode is active!</h5>\r\n</div>\r\n<router-outlet></router-outlet>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/component-libs/form-builder-table/form-builder-table.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/component-libs/form-builder-table/form-builder-table.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"fbt\" *ngIf=\"!bast_mode\">\r\n    <div class=\"col col-xl-12\">\r\n        <div class=\"layout-card\">\r\n            <div class=\"header-custom\">\r\n                <label class=\"label-custom\">{{table_title}}</label>\r\n                <div class=\"dropdown-header\">\r\n                    <a class=\"btn right\" (click)=\"toggleColoumnCheck()\" [ngClass]=\"'toggle-'+columnCheck\">\r\n                        <i class=\"fa fa-check-square\"></i>\r\n                        Column\r\n                    </a>\r\n                    <a *ngIf=\"options.addMultipleLable\" routerLink=\"/administrator/orderhistoryallhistoryadmin/receipt\"\r\n                        [routerLinkActive]=\"['router-link-active']\" class=\"btn right\">\r\n                        <i class=\"fa fa-print\"></i>\r\n                        <span>{{ 'Print Label' }}</span>\r\n                    </a>\r\n                    <a *ngIf=\"options.addForm\" routerLink=\"./add\" [routerLinkActive]=\"['router-link-active']\"\r\n                        class=\"btn right\">\r\n                        <i class=\"fa fa-plus\"></i>\r\n                        <span>{{ 'Add New' }}</span>\r\n                    </a>\r\n                    <a *ngIf=\"options.addBulk\" routerLink=\"./add-bulk\" [routerLinkActive]=\"['router-link-active']\"\r\n                        class=\"btn right\">\r\n                        <i class=\"fa fa-plus\"></i>\r\n                        <span>{{ 'Add Bulk' }}</span>\r\n                    </a>\r\n                    <a *ngIf=\"options.bulkUpdate\" routerLink=\"./add-bulk\" [routerLinkActive]=\"['router-link-active']\"\r\n                        class=\"btn right\">\r\n                        <i class=\"fa fa-plus\"></i>\r\n                        <span>{{ 'Bulk Update' }}</span>\r\n                    </a>\r\n                    <a *ngIf=\"options.updateInvoice\" routerLink=\"./update-invoice\" [routerLinkActive]=\"['router-link-active']\"\r\n                        class=\"btn right\">\r\n                        <i class=\"fa fa-plus\"></i>\r\n                        <span>{{ 'Update No. Invoice' }}</span>\r\n                    </a>\r\n                    <a *ngIf=\"options.addFormGenerate\" routerLink=\"./generate\"\r\n                        [routerLinkActive]=\"['router-link-active']\" class=\"btn right\">\r\n                        <i class=\"fa fa-plus\"></i>\r\n                        <span>{{ 'Generate' }}</span>\r\n                    </a>\r\n\r\n                    <a *ngIf=\"options.filterForm\" routerLink=\"./add\" [routerLinkActive]=\"['router-link-active']\"\r\n                        class=\"btn\">\r\n                        <i id=\"search\" class=\"fa fa-search\"></i>\r\n                        <span>{{ 'Search Filter' }}</span>\r\n                    </a>\r\n                    <!-- <a *ngIf=\"options.salesRedemption\" (click)=\"autoBast()\" class=\"btn right\"><i class=\"fa fa-download\"></i>BAST</a> -->\r\n                    <a *ngIf=\"options.bastReport\" (click)=\"autoBast()\" class=\"btn right\"><i class=\"fa fa-download\"></i>BAST</a>\r\n                    <a (click)=\"onClickDownload($event)\" class=\"btn right\"><i class=\"fa fa-download\"></i>Download Excel</a>\r\n                    <a *ngIf=\"options.setCompleteMember\" (click)=\"setCompleteMember()\" class=\"btn right\"><i class=\"fa fa-address-card\"></i>Set Complete Member</a>\r\n                    \r\n                    <!-- Approve Button -->\r\n                    <button *ngIf=\"options.approveButton\" (click)=\"approveOrder()\"\r\n                        class=\"btn btn-approve right\">\r\n                        <i class=\"fa fa-check\"></i>\r\n                        <span>{{ 'Approve Selected' }}</span>\r\n                    </button>\r\n\r\n                    <!-- Remove fron Packing List Button -->\r\n                    <button *ngIf=\"options.removePackinglist\" (click)=\"removeFromPackingList()\"\r\n                        class=\"btn btn-remove right\">\r\n                        <i class=\"fa fa-times\"></i>\r\n                        <span>{{ 'Remove from Packing List' }}</span>\r\n                    </button>\r\n\r\n                    <!-- Remove fron Packing List Button -->\r\n                    <button *ngIf=\"options.cancelProduct\" (click)=\"cancelSelectedProduct()\"\r\n                        class=\"btn btn-remove right\">\r\n                        <i class=\"fa fa-times\"></i>\r\n                        <span>{{ 'Cancel Selected Product' }}</span>\r\n                    </button>\r\n                </div>\r\n            </div>\r\n            <div class=\"card mb-3\">\r\n                <!-- INI COLUMN CHECKBOX -->\r\n                <div class=\"card-header\" *ngIf=\"columnCheck\">\r\n                    <span class=\"cb-coloumn\" *ngFor=\"let cb of table_header;let c = index;\">\r\n                        <mat-checkbox color=\"primary\" (click)=\"checkedColumn()\" [(ngModel)]=\"cb.visible\" value=\"{{cb.label}}\"\r\n                            id=\"{{cb.label}}\">\r\n                            {{cb.label}}\r\n                        </mat-checkbox>\r\n                    </span>\r\n                </div>\r\n                <div *ngIf=\"options.filterForm\" class=\"card-header\">\r\n                    <div *ngFor=\"let filForm of options.filterForm\">\r\n\r\n                        <div class=\"col-md-3 float\">\r\n\r\n                            <!-- <app-date-picker [label]=\"filForm.label\"></app-date-picker> -->\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!-- <div class=\"card-body table-responsive\" style=\"background-color:#2F3545;\r\n                        color: #627190;\"> -->\r\n                <div class=\"card-body table-responsive\">\r\n                    <table class=\"table table-sm table-bordered\">\r\n                        <thead>\r\n                            <div *ngIf=\"options.check_box\">\r\n                                <div *ngFor=\"let data of list_data\" class=\"btn-group\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\"\r\n                                        (click)=\"removeFromList(data)\">{{data}}</button>\r\n                                </div>\r\n                            </div>\r\n                            <tr>\r\n                                <th *ngIf=\"tableFormat.show_checkbox_options\">\r\n                                    <mat-checkbox (change)=\"checkedAll()\" [(ngModel)]=\"activeCheckbox\" type=\"checkbox\"\r\n                                        name=\"cbox\"></mat-checkbox>\r\n                                </th>\r\n                                <th class=\"custom-table-border\">No.</th>\r\n                                <th *ngFor=\"let row of table_head;let i = index;\">\r\n                                    <div class=\"head\" (click)=\"orderChange($event, row.data_row_name)\">{{row.label}}\r\n                                        <i *ngIf=\"orderBy[row.data_row_name] && asc==true\"\r\n                                            class=\"fa fa-caret-down\"></i>\r\n                                        <i *ngIf=\"orderBy[row.data_row_name] && asc==false\"\r\n                                            class=\"fa fa-caret-up\"></i>\r\n                                    </div>\r\n                                </th>\r\n                            </tr>\r\n                            <tr>\r\n                                <th *ngIf=\"tableFormat.show_checkbox_options\"> </th>\r\n\r\n                                <ng-container *ngIf=\"showSearch\">\r\n                                <th></th>\r\n                                <th *ngFor=\"let row of table_head;let i = index;\">\r\n                                    <div *ngIf=\"row.type == 'number'\">\r\n                                        <div class=\"input-group mb-3\">\r\n                                            <div class=\"input-group-prepend\">\r\n                                                <select (change)=\"valuechange($event, row)\"\r\n                                                    [(ngModel)]=\"form_input[row.data_row_name].option\">\r\n                                                    <option [ngValue]=\"'equal'\">=</option>\r\n                                                    <option [ngValue]=\"'greater_than'\">&gt;</option>\r\n                                                    <option [ngValue]=\"'less_than'\">&lt;</option>\r\n                                                    <option [ngValue]=\"'greater_than_or_equal'\">&ge;</option>\r\n                                                    <option [ngValue]=\"'less_than_or_equal'\">&le;</option>\r\n                                                    <option [ngValue]=\"'range'\">range</option>\r\n                                                </select>\r\n                                            </div>\r\n                                            <input *ngIf=\"form_input[row.data_row_name].option != 'range'\"\r\n                                                class=\"form-control\" type=\"text\"\r\n                                                [(ngModel)]=\"form_input[row.data_row_name].value\" placeholder=\"find\"\r\n                                                (keyup)=\"valuechange($event, row)\">\r\n                                            <div class=\"form-control\"\r\n                                                *ngIf=\"form_input[row.data_row_name].option == 'range'\">\r\n                                                <input type=\"text\" [(ngModel)]=\"form_input[row.data_row_name].from\"\r\n                                                    placeholder=\"range from\" (keyup)=\"valuechange($event, row)\"> -\r\n                                                <input type=\"text\" [(ngModel)]=\"form_input[row.data_row_name].to\"\r\n                                                    placeholder=\"range to\" (keyup)=\"valuechange($event, row)\">\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'string' \" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'value_po' \" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'form_nama_penerima'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'image' \" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <!-- <div *ngIf=\"row.type == 'penerima'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'pemilik'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div> -->\r\n                                    <div *ngIf=\"row.type == 'product'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'productcode' \" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'productdesc'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'productname'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\" row.type == 'productcat'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'productmerc'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\" row.type == 'productqty'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'productunit'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'producttotal'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'productweight'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'productdim'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <div *ngIf=\"row.type == 'escrow-d' || row.type == 'escrow-c'\" class=\"search-string\">\r\n                                        <i class=\"fa fa-search search-field\"></i>\r\n                                        <input class=\"form-control\" type=\"text\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\" placeholder=\"find\"\r\n                                            (keyup)=\"valuechange($event, row)\">\r\n                                    </div>\r\n                                    <span *ngIf=\"row.type == 'list'\">\r\n                                        <select (change)=\"valuechange($event, row.data_row_name)\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\">\r\n                                            <option [ngValue]=\"''\" selected=\"selected\">-empty-</option>\r\n                                            <option *ngFor=\"let h of row.options\" [ngValue]=\"h.value\">{{h.label}}\r\n                                            </option>\r\n                                        </select>\r\n                                    </span>\r\n                                    <span *ngIf=\"row.type == 'list-escrow'\">\r\n                                        <select (change)=\"valuechange($event, row.data_row_name)\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\">\r\n                                            <option [ngValue]=\"''\" selected=\"selected\">-empty-</option>\r\n                                            <option *ngFor=\"let h of row.options\" [ngValue]=\"h.value\">{{h.label}}\r\n                                            </option>\r\n                                        </select>\r\n                                    </span>\r\n                                    <span *ngIf=\"row.type == 'list-escrow-status'\">\r\n                                        <select (change)=\"valuechange($event, row.data_row_name)\"\r\n                                            [(ngModel)]=\"form_input[row.data_row_name]\">\r\n                                            <option [ngValue]=\"''\" selected=\"selected\">-empty-</option>\r\n                                            <option *ngFor=\"let h of row.options\" [ngValue]=\"h.value\">{{h.label}}\r\n                                            </option>\r\n                                        </select>\r\n                                    </span>\r\n                                    <div *ngIf=\"row.type == 'date'\" class=\"date-picker-wrapper\">\r\n                                        <!-- <ngb-datepicker #d></ngb-datepicker> -->\r\n                                        <div class=\"date-picker-overlay\" *ngIf=\"toggler && toggler[row.data_row_name]\">\r\n                                            <div class=\"dpicker\">\r\n                                                <ngb-datepicker #d\r\n                                                    (select)=\"datePickerOnDateSelection($event, row.data_row_name)\"\r\n                                                    [displayMonths]=\"1\" [dayTemplate]=\"t\" outsideDays=\"hidden\">\r\n                                                </ngb-datepicker>\r\n                                                <div style=\"float: right\">\r\n                                                    <a class=\"btn ok\" (click)=\"dateToggler(row.data_row_name)\">OK</a>\r\n                                                    <a class=\"btn clear\"\r\n                                                        (click)=\"dateClear($event, row.data_row_name, row)\"> <span\r\n                                                            class=\"fa fa-trash\"></span> clear</a>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <ng-template #t let-date=\"date\" let-focused=\"focused\">\r\n                                            <span class=\"custom-day\" [class.focused]=\"focused\"\r\n                                                [class.range]=\"datePickerOnDateIsRange(date, form_input[row.data_row_name])\"\r\n                                                [class.faded]=\"datePickerOnDateIsHovered(date, form_input[row.data_row_name]) || datePickerOnDateIsInside(date, form_input[row.data_row_name])\"\r\n                                                (mouseenter)=\"hoveredDate = date\" (mouseleave)=\"hoveredDate = null\">\r\n                                                {{ date.day }}\r\n                                            </span>\r\n                                        </ng-template>\r\n                                        <!-- <input (select)=\"onDateSelection($event)\" class=\"form-control\" (click)=\"d.toggle()\" placeholder=\"yyyy-mm-dd\"\r\n                                                        name=\"dp\"\r\n                                                        /> -->\r\n                                        <!-- <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                                            <span class=\"fa fa-calendar\"></span>\r\n                                                        </button> -->\r\n                                        <em\r\n                                            *ngIf=\"typeof(form_input[row.data_row_name]) == 'string' || !form_input[row.data_row_name]\">\r\n                                            <input class=\"form-control\" type=\"text\"\r\n                                                [(ngModel)]=\"form_input[row.data_row_name]\"\r\n                                                placeholder=\"search date here\" (click)=\"dateToggler(row.data_row_name)\">\r\n                                        </em>\r\n\r\n                                        <a class=\"btn\"\r\n                                            *ngIf=\"typeof(form_input[row.data_row_name]) == 'object' && form_input[row.data_row_name].from \"\r\n                                            (click)=\"dateToggler(row.data_row_name)\">\r\n                                            <span *ngIf=\"form_input[row.data_row_name].from\">\r\n                                                {{form_input[row.data_row_name].from}} </span>\r\n                                            <span *ngIf=\"form_input[row.data_row_name].to\"> -\r\n                                                {{form_input[row.data_row_name].to}} </span>\r\n\r\n                                        </a>\r\n                                    </div>\r\n                                </th>\r\n                            </ng-container>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody *ngIf=\"showLoading\">\r\n                            <tr>\r\n                                <td [attr.colspan]=\"table_head.length+2\">\r\n                                    <div class=\"sk-fading-circle\">\r\n                                        <div class=\"sk-circle1 sk-circle\"></div>\r\n                                        <div class=\"sk-circle2 sk-circle\"></div>\r\n                                        <div class=\"sk-circle3 sk-circle\"></div>\r\n                                        <div class=\"sk-circle4 sk-circle\"></div>\r\n                                        <div class=\"sk-circle5 sk-circle\"></div>\r\n                                        <div class=\"sk-circle6 sk-circle\"></div>\r\n                                        <div class=\"sk-circle7 sk-circle\"></div>\r\n                                        <div class=\"sk-circle8 sk-circle\"></div>\r\n                                        <div class=\"sk-circle9 sk-circle\"></div>\r\n                                        <div class=\"sk-circle10 sk-circle\"></div>\r\n                                        <div class=\"sk-circle11 sk-circle\"></div>\r\n                                        <div class=\"sk-circle12 sk-circle\"></div>\r\n                                    </div>\r\n                                </td>\r\n                            </tr>\r\n                        </tbody>\r\n                        <tbody *ngIf=\"options.type=='row-text' && !showLoading\">\r\n                            <tr *ngIf='!table_data || table_data.length < 1 '>\r\n                                <td [attr.colspan]=\"table_head.length+2\">\r\n                                    <div>Data Empty</div>\r\n                                </td>\r\n                            </tr>\r\n                            <tr *ngFor=\"let data of table_data; let i = index;\">\r\n                                <!-- <div>\r\n                                    {{data.id_pel}}\r\n                                </div> -->\r\n                                <td *ngIf=\"tableFormat.show_checkbox_options\">\r\n                                    <mat-checkbox name=\"cbox\" (change)=\"getValueofCheckbox(this)\"\r\n                                        [(ngModel)]=\"checkBox[i]\" value=\"{{data[options.row_id]}}\">\r\n                                    </mat-checkbox>\r\n                                </td>\r\n\r\n                                <td (click)=\"clickedRowTable(data[options.row_id], data)\">\r\n                                    {{i + 1 + ((currentPage -1) * pageLimits )}}</td>\r\n                                <td (click)=\"clickedRowTable(data[options.row_id], data)\"\r\n                                    *ngFor=\"let row of table_head\" [ngClass]=\"row.data_row_name\"> \r\n                                    <div *ngIf=\"!options.check_box\">\r\n                                        <span\r\n                                            *ngIf=\"isString(data[row])\">{{stripHtml(data[row.data_row_name]) | slice:0:150}}</span>\r\n                                        <span\r\n                                            *ngIf=\"!isString(data[row])\">\r\n                                         <!-- heru's note \r\n                                            So in this part i change the logic to match the escrow transaction logic\r\n                                            + and - will be replaced by some string , also for debit & credit \r\n\r\n                                            **the data will be presented in the tableas normal string if the row.type != list-escrow -->\r\n                                        <ng-container [ngSwitch]=\"row.type\">\r\n                                            <ng-container *ngSwitchCase=\"'list-escrow'\">\r\n                                                <ng-container [ngSwitch]=\"stripHtml(data[row.data_row_name])\">\r\n                                                    <ng-container *ngSwitchCase=\"'+'\">Debit</ng-container>\r\n                                                    <ng-container *ngSwitchCase=\"'-'\">Kredit</ng-container>\r\n                                                    <ng-container *ngSwitchCase=\"'withdrawal'\">Withdrawal</ng-container>\r\n                                                    <ng-container *ngSwitchDefault></ng-container>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'list-escrow-status'\">\r\n                                                <ng-container [ngSwitch]=\"stripHtml(data[row.data_row_name])\">\r\n                                                    <ng-container *ngSwitchCase=\"'PENDING'\"><button class=\"pending\">{{stripHtml(data[row.data_row_name]) }}</button></ng-container>\r\n                                                    <ng-container *ngSwitchCase=\"'SUCCESS'\"><button class=\"success\">{{stripHtml(data[row.data_row_name]) }}</button></ng-container>\r\n                                                    <ng-container *ngSwitchCase=\"'HOLD'\"><button class=\"hold\">{{stripHtml(data[row.data_row_name]) }}</button></ng-container>\r\n                                                    <ng-container *ngSwitchCase=\"'REFUND'\"><button class=\"refunded\">{{stripHtml(data[row.data_row_name]) }}</button></ng-container>\r\n                                                    <ng-container *ngSwitchCase=\"'REJECT'\"><button class=\"rejected\">{{stripHtml(data[row.data_row_name]) }}</button></ng-container>\r\n                                                    <ng-container *ngSwitchDefault>-</ng-container>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'escrow-d'\">\r\n                                                <ng-container *ngIf=\"!include(stripHtml(data[row.data_row_name]))\">\r\n                                                    <span>{{stripHtml(data[row.data_row_name]) }}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'escrow-c'\">\r\n                                                <ng-container *ngIf=\"include(stripHtml(data[row.data_row_name]))\">\r\n                                                    <span style=\"color:red\">{{stripHtml(data[row.data_row_name]) * -1 }}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'pemilik'\">\r\n                                                <ng-container>\r\n                                                    <span>{{pemilik(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'nama_toko'\">\r\n                                                <ng-container>\r\n                                                    <span>{{nama_toko(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'id_toko'\">\r\n                                                <ng-container>\r\n                                                    <span>{{id_toko(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'member_desc'\">\r\n                                                <ng-container>\r\n                                                    <span>{{member_desc(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'dikuasakan'\">\r\n                                                <ng-container>\r\n                                                    <span>{{dikuasakan(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'no_wa'\">\r\n                                                <ng-container>\r\n                                                    <span>{{no_wa(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'alamat_kirim'\">\r\n                                                <ng-container>\r\n                                                    <span>{{alamat_kirim(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'shipping_date'\">\r\n                                                <ng-container>\r\n                                                    <span>{{shipping_date(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'product'\">\r\n                                                <ng-container>\r\n                                                    <span>{{product(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'productcode'\">\r\n                                                <ng-container>\r\n                                                    <span>{{productcode(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'productdesc'\">\r\n                                                <ng-container>\r\n                                                    <span>{{productdesc(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'productname'\">\r\n                                                <ng-container>\r\n                                                    <span>{{productname(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'productcat'\">\r\n                                                <ng-container>\r\n                                                    <span>{{productcat(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'productmerc'\">\r\n                                                <ng-container>\r\n                                                    <span>{{productmerc(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'productqty'\">\r\n                                                <ng-container>\r\n                                                    <span>{{productqty(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'productunit'\">\r\n                                                <ng-container>\r\n                                                    <span>{{productunit(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'producttotal'\">\r\n                                                <ng-container>\r\n                                                    <span>{{producttotal(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'productdim'\">\r\n                                                <ng-container>\r\n                                                    <span>{{productdim(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'productweight'\">\r\n                                                <ng-container>\r\n                                                    <span>{{productweight(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'total_quantity'\">\r\n                                                <ng-container>\r\n                                                    <span>{{total_quantity(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'quantity'\">\r\n                                                <ng-container>\r\n                                                    <span>{{quantity(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'product_code'\">\r\n                                                <ng-container>\r\n                                                    <span>{{product_code(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'product_name'\">\r\n                                                <ng-container>\r\n                                                    <span>{{product_name(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!-- shipping info -->\r\n                                            <ng-container *ngSwitchCase=\"'on_process_date'\">\r\n                                                <ng-container>\r\n                                                    <span>{{on_process_date(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'on_delivery_date'\">\r\n                                                <ng-container>\r\n                                                    <span>{{on_delivery_date(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!-- <ng-container *ngSwitchCase=\"'input_awb_date'\">\r\n                                                <ng-container>\r\n                                                    <span>{{input_awb_date(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container> -->\r\n                                            <!-- points -->\r\n                                            <ng-container *ngSwitchCase=\"'point_balance'\">\r\n                                                <ng-container>\r\n                                                    <span>{{point_balance(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'expire_date'\">\r\n                                                <ng-container>\r\n                                                    <span>{{expire_date(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!-- member address -->\r\n                                            <ng-container *ngSwitchCase=\"'penerima_hadiah'\">\r\n                                                <ng-container>\r\n                                                    <span>{{penerima_hadiah(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'telp_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{telp_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'alamat_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{alamat_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'kelurahan_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{kelurahan_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'kecamatan_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{kecamatan_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'kota_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{kota_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'provinsi_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{provinsi_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'kode_pos_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{kode_pos_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!-- end of member address -->\r\n\r\n                                            <!-- start of input form data -->\r\n                                            <ng-container *ngSwitchCase=\"'form_id_pel'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_id_pel(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_business_id'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_business_id(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_nama_toko'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_nama_toko(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'reference_no_length'\">\r\n                                                <ng-container>\r\n                                                    <span>{{reference_no_length(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'data_complete'\">\r\n                                                <ng-container>\r\n                                                    <span>{{data_complete(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_nama_distributor'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_nama_distributor(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_alamat_toko'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_alamat_toko(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_nama_pemilik'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_nama_pemilik(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_ktp_pemilik'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_ktp_pemilik(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_foto_ktp_pemilik'\">\r\n                                                <ng-container *ngIf=\"validURL(form_foto_ktp_pemilik(data[row.data_row_name])); else empty_image\">\r\n                                                    <div style=\"display:flex; align-items: center; justify-content: center; width: 100%; height: 100%;\">\r\n                                                        <div style=\"display:flex; align-items: center; justify-content: center; width: 150px;\">\r\n                                                            <pdf-viewer \r\n                                                                *ngIf=\"checkFileType(form_foto_ktp_pemilik(data[row.data_row_name])) == 'document'\"\r\n                                                                [src]=\"form_foto_ktp_pemilik(data[row.data_row_name])\"\r\n                                                                [render-text]=\"false\"\r\n                                                                [original-size]=\"false\"\r\n                                                                [page] = \"1\"\r\n                                                                [show-all]=\"false\"\r\n                                                                style=\"width: 100%; height: 100%\">\r\n                                                            </pdf-viewer>\r\n                                                        </div>\r\n                                                    </div>\r\n                                                    <img *ngIf=\"checkFileType(form_foto_ktp_pemilik(data[row.data_row_name])) == 'image'\" class=\"image-autoform\" src={{form_foto_ktp_pemilik(data[row.data_row_name])}} />\r\n                                                </ng-container>\r\n                                                <ng-template #empty_image>\r\n                                                    <ng-container>\r\n                                                        <span>empty</span>\r\n                                                    </ng-container>\r\n                                                </ng-template>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_npwp_pemilik'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_npwp_pemilik(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_foto_npwp_pemilik'\">\r\n                                                <ng-container *ngIf=\"validURL(form_foto_npwp_pemilik(data[row.data_row_name])); else empty_image\">\r\n                                                    <div style=\"display:flex; align-items: center; justify-content: center; width: 100%; height: 100%;\">\r\n                                                        <div style=\"display:flex; align-items: center; justify-content: center; width: 150px;\">\r\n                                                            <pdf-viewer \r\n                                                                *ngIf=\"checkFileType(form_foto_npwp_pemilik(data[row.data_row_name])) == 'document'\"\r\n                                                                [src]=\"form_foto_npwp_pemilik(data[row.data_row_name])\"\r\n                                                                [render-text]=\"false\"\r\n                                                                [original-size]=\"false\"\r\n                                                                [page] = \"1\"\r\n                                                                [show-all]=\"false\"\r\n                                                                style=\"width: 100%; height: 100%\">\r\n                                                            </pdf-viewer>\r\n                                                            <img *ngIf=\"checkFileType(form_foto_npwp_pemilik(data[row.data_row_name])) == 'image'\" class=\"image-autoform\" src={{form_foto_npwp_pemilik(data[row.data_row_name])}} />\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </ng-container>\r\n                                                <ng-template #empty_image>\r\n                                                    <ng-container>\r\n                                                        <span>empty</span>\r\n                                                    </ng-container>\r\n                                                </ng-template>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_telp_pemilik'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_telp_pemilik(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_wa_pemilik'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_wa_pemilik(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_email_pelanggan'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_email_pelanggan(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_alamat_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_alamat_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_hadiah_dikuasakan'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_hadiah_dikuasakan(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_nama_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_nama_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_group'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_group(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_cluster'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_cluster(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_group_mci'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_group_mci(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_ktp_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_ktp_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_foto_ktp_penerima'\">\r\n                                                <ng-container *ngIf=\"validURL(form_foto_ktp_penerima(data[row.data_row_name])); else empty_image\">\r\n                                                    <div style=\"display:flex; align-items: center; justify-content: center; width: 100%; height: 100%;\">\r\n                                                        <div style=\"display:flex; align-items: center; justify-content: center; width: 150px;\">\r\n                                                            <pdf-viewer \r\n                                                                *ngIf=\"checkFileType(form_foto_ktp_penerima(data[row.data_row_name])) == 'document'\"\r\n                                                                [src]=\"form_foto_ktp_penerima(data[row.data_row_name])\"\r\n                                                                [render-text]=\"false\"\r\n                                                                [original-size]=\"false\"\r\n                                                                [page] = \"1\"\r\n                                                                [show-all]=\"false\"\r\n                                                                style=\"width: 100%; height: 100%\">\r\n                                                            </pdf-viewer>\r\n                                                            <img *ngIf=\"checkFileType(form_foto_ktp_penerima(data[row.data_row_name])) == 'image'\" class=\"image-autoform\" src={{form_foto_ktp_penerima(data[row.data_row_name])}} />\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </ng-container>\r\n                                                <ng-template #empty_image>\r\n                                                    <ng-container>\r\n                                                        <span>empty</span>\r\n                                                    </ng-container>\r\n                                                </ng-template>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_npwp_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_npwp_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_foto_npwp_penerima'\">\r\n                                                <ng-container *ngIf=\"validURL(form_foto_npwp_penerima(data[row.data_row_name])); else empty_image\">\r\n                                                    <div style=\"display:flex; align-items: center; justify-content: center; width: 100%; height: 100%;\">\r\n                                                        <div style=\"display:flex; align-items: center; justify-content: center; width: 150px;\">\r\n                                                            <pdf-viewer \r\n                                                                *ngIf=\"checkFileType(form_foto_npwp_penerima(data[row.data_row_name])) == 'document'\"\r\n                                                                [src]=\"form_foto_npwp_penerima(data[row.data_row_name])\"\r\n                                                                [render-text]=\"false\"\r\n                                                                [original-size]=\"false\"\r\n                                                                [page] = \"1\"\r\n                                                                [show-all]=\"false\"\r\n                                                                style=\"width: 100%; height: 100%\">\r\n                                                            </pdf-viewer>\r\n                                                            <img *ngIf=\"checkFileType(form_foto_npwp_penerima(data[row.data_row_name])) == 'image'\" class=\"image-autoform\" src={{form_foto_npwp_penerima(data[row.data_row_name])}} />\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </ng-container>\r\n                                                <ng-template #empty_image>\r\n                                                    <ng-container>\r\n                                                        <span>empty</span>\r\n                                                    </ng-container>\r\n                                                </ng-template>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_wa_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_wa_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'form_gopay_penerima'\">\r\n                                                <ng-container>\r\n                                                    <span>{{form_gopay_penerima(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!-- end of input form data -->\r\n                                            <!-- start of additional info -->\r\n                                            <ng-container *ngSwitchCase=\"'bast'\">\r\n                                                <ng-container>\r\n                                                    <span>{{bast(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'surat_kuasa'\">\r\n                                                <ng-container>\r\n                                                    <span>{{surat_kuasa(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'bast_date'\">\r\n                                                <ng-container>\r\n                                                    <span>{{bast_date(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'invoice_no'\">\r\n                                                <ng-container>\r\n                                                    <span>{{invoice_no(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!-- Delivery Detail -->\r\n                                            <ng-container *ngSwitchCase=\"'awb_number'\">\r\n                                                <ng-container>\r\n                                                    <span>{{awb_number(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'delivered_date'\">\r\n                                                <ng-container>\r\n                                                    <span>{{delivered_date(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'delivery_date'\">\r\n                                                <ng-container>\r\n                                                    <span>{{delivery_date(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'last_shipping_info'\">\r\n                                                <ng-container>\r\n                                                    <span>{{last_shipping_info(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'receiver_name_sap'\">\r\n                                                <ng-container>\r\n                                                    <span>{{receiver_name_sap(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'relation_name'\">\r\n                                                <ng-container>\r\n                                                    <span>{{relation_name(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!-- Cancel Detail -->\r\n                                            <ng-container *ngSwitchCase=\"'cancel_detail'\">\r\n                                                <ng-container>\r\n                                                    <span>{{relation_name(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!-- New Invoice Finance -->\r\n                                            <ng-container *ngSwitchCase=\"'value_invoice'\">\r\n                                                <ng-container>\r\n                                                    <span>{{value_invoice(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'value_po'\">\r\n                                                <ng-container>\r\n                                                    <span>{{value_po(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'date_invoice'\">\r\n                                                <ng-container>\r\n                                                    <span>{{date_invoice(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!--  -->\r\n                                            <!-- product sales order new -->\r\n                                            <ng-container *ngSwitchCase=\"'product_quantity'\">\r\n                                                <ng-container>\r\n                                                    <span>{{product_quantity(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'product_redeem'\">\r\n                                                <ng-container>\r\n                                                    <span>{{product_redeem(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'product_code_redeem'\">\r\n                                                <ng-container>\r\n                                                    <span>{{product_code_redeem(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'product_price_redeem'\">\r\n                                                <ng-container>\r\n                                                    <span>{{product_price_redeem(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!--  -->\r\n\r\n                                            <!-- address detail -->\r\n                                            <ng-container *ngSwitchCase=\"'receiver_name'\">\r\n                                                <ng-container>\r\n                                                    <span>{{receiver_name(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'shipping_address'\">\r\n                                                <ng-container>\r\n                                                    <span>{{shipping_address(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'phone_number'\">\r\n                                                <ng-container>\r\n                                                    <span>{{phone_number(stripHtml(data[row.data_row_name]))}}</span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <!-- -->\r\n\r\n                                            <ng-container *ngSwitchCase=\"'usr_mngt'\">\r\n                                                <ng-container>\r\n                                                    <div *ngFor=\"let item of data[row.data_row_name] | keyvalue\">\r\n                                                        <b>{{item.key}}</b> : {{usr_mngt(stripHtml(item.value))}}\r\n                                                        <br><br>\r\n                                                    </div>\r\n\r\n\r\n                                                    <!-- <div *ngIf=\"data[row.data_row_name]['usr_mngt']\"><b>User Management : </b>{{usr_mngt(stripHtml(data[row.data_row_name]['usr_mngt']))}}<br><br></div>\r\n                                                    <div *ngIf=\"data[row.data_row_name]['dynamix_sbi']\"><b>Dynamix SBI : </b>{{dynamix_sbi(stripHtml(data[row.data_row_name]['dynamix_sbi']))}}</div> -->\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'variation'\">\r\n                                                <ng-container>\r\n                                                    <span [innerHTML]=\"variation(stripHtml(data[row.data_row_name]))\"></span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchCase=\"'dimensions'\">\r\n                                                <ng-container>\r\n                                                    <span [innerHTML]=\"dimensions(stripHtml(data[row.data_row_name]))\"></span>\r\n                                                </ng-container>\r\n                                            </ng-container>\r\n\r\n                                            <ng-container *ngSwitchCase=\"'images_gallery'\">\r\n                                                <ng-container *ngIf=\"validURL(images_gallery(data[row.data_row_name])); else empty_image\">\r\n                                                    <img class=\"image-autoform\" src={{images_gallery(data[row.data_row_name])}} />\r\n                                                </ng-container>\r\n                                                <ng-template #empty_image>\r\n                                                    <ng-container>\r\n                                                        <span>empty</span>\r\n                                                    </ng-container>\r\n                                                </ng-template>\r\n                                            </ng-container>\r\n\r\n\r\n\r\n                                            <ng-container *ngSwitchCase=\"'image'\">\r\n                                                <ng-container *ngIf=\"validURL(data[row.data_row_name]); else empty_image\">\r\n                                                    <img class=\"image-autoform\" src={{data[row.data_row_name]}} />\r\n                                                </ng-container>\r\n                                                <ng-template #empty_image>\r\n                                                    <ng-container>\r\n                                                        <span>empty</span>\r\n                                                    </ng-container>\r\n                                                </ng-template>\r\n                                            </ng-container>\r\n                                            <ng-container *ngSwitchDefault>\r\n                                                {{stripHtml(data[row.data_row_name]) }}\r\n                                            </ng-container>\r\n                                        </ng-container>\r\n                                        </span>\r\n                                    </div>\r\n                                </td>\r\n                            </tr>\r\n                            <button *ngIf=\"options.check_box\" class=\"btn submit_button\" (click)=\"submit()\"><i\r\n                                    class=\"fa fa-fw fa-save\"></i> submit</button>\r\n                            <tr *ngIf=\"errorLabel\">\r\n                                {{errorLabel}}\r\n                            </tr>\r\n\r\n                        </tbody>\r\n                        <tbody *ngIf=\"options.type=='image'\">\r\n                            <tr class=\"{{options.type}}\">\r\n                                <td [colSpan]=\"table_head.length\">\r\n                                    <div class=\"img_dt_prp\" *ngFor=\"let data of table_data\"\r\n                                        (click)=\"clickedRowTable(data[options.row_id], data)\">\r\n                                        <div>\r\n                                            <div class=\"image_wrapper\">\r\n                                                <img [src]=\"data[options.image_data_property]\" />\r\n                                            </div>\r\n                                            <div>{{data['name']}}</div>\r\n                                        </div>\r\n                                    </div>\r\n                                </td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n\r\n                </div>\r\n\r\n            </div>\r\n            <nav class=\"pagination-head\" aria-label=\"Page navigation example\">\r\n                <label class=\"show-entries\">Showing\r\n                    <select [(ngModel)]=\"pageLimits\" (change)=\"pageLimitChanges($event)\"\r\n                        id=\"pagesLimiter\" class=\"btn btn-outline-primary order-btn\">\r\n                        <option *ngFor=\"let order of pagesLimiter; let i = index\" [ngValue]=\"order.id\">\r\n                            {{order.name}}\r\n                        </option>\r\n                    </select> data</label>\r\n                <ul class=\"pagination justify-content-end example\">\r\n\r\n                    <li class=\"page-item start\" *ngIf=\"currentPage != 1\">\r\n                        <a class=\"page-link next\" (click)=\"onChangePage(currentPage = 1)\"><<\r\n                        </a>\r\n                    </li>\r\n\r\n                    <li [class]=\"currentPage == 1 ? 'page-item start':'page-item'\">\r\n                        <a class=\"page-link next\" (click)=\"onChangePage(currentPage - 1)\">&lt;</a>\r\n                    </li>\r\n\r\n                    <li *ngFor=\"let tp of pageNumbering; let i = index;\" class=\"page-item\">\r\n                        <a *ngIf=\"tp=='...'\" [ngClass]=\"'dotter'\" class=\"page-link\">{{tp}}</a>\r\n                        <a *ngIf=\"isCurrentPage(tp) == 'current-page'\" [ngClass]=\"'page-link '+isCurrentPage(tp)\"\r\n                            class=\"page-link\">{{tp}}</a>\r\n                        <a *ngIf=\"isCurrentPage(tp) != 'current-page' && tp!='...'\" class=\"page-link\"\r\n                            (click)=\"onChangePage(tp)\">{{tp}}</a>\r\n\r\n                    </li>\r\n                    <!-- <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\r\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li> -->\r\n                    <li [class]=\"currentPage == pageNumbering[pageNumbering.length - 1] ? 'page-item end':'page-item'\">\r\n                        <a class=\"page-link next\" (click)=\"onChangePage(currentPage + 1)\">&gt;</a>\r\n                    </li>\r\n                    <li class=\"page-item end\" *ngIf=\"currentPage != pageNumbering[pageNumbering.length - 1]\">\r\n                        <a class=\"page-link next\" (click)=\"onChangePage(currentPage = total_page)\"> >> </a>\r\n                    </li>\r\n                </ul>\r\n                <ul class=\"pagination justify-content-end\">\r\n                    <li></li>\r\n                    <!-- <li>Data 1-100 </!-->\r\n                </ul>\r\n            </nav>\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n\r\n<table class=\"undisplayed\" border=\"1\" id=\"reportTable\" #reportTable>\r\n    <thead>\r\n        <div *ngIf=\"options.check_box\">\r\n            <div *ngFor=\"let data of list_data\" class=\"btn-group\">\r\n                <button type=\"button\" class=\"btn btn-primary\" (click)=\"removeFromList(data)\">{{data}}</button>\r\n            </div>\r\n        </div>\r\n        <tr>\r\n            <th>NO.</th>\r\n            <th *ngFor=\"let row of table_head;let i = index;\">\r\n                <div class=\"head\" (click)=\"orderChange($event, row.data_row_name)\">{{row.label}}\r\n                    <i *ngIf=\"orderBy[row.data_row_name] && asc==true\"\r\n                        class=\"fa fa-caret-down\"></i>\r\n                    <i *ngIf=\"orderBy[row.data_row_name] && asc==false\"\r\n                        class=\"fa fa-caret-up\"></i>\r\n                </div>\r\n            </th>\r\n        </tr>\r\n\r\n    </thead>\r\n    <tbody *ngIf=\"options.type=='row-text'\">\r\n\r\n        <tr *ngFor=\"let data of table_data; let i = index;\" >\r\n\r\n            <!-- <span *ngIf=\"options.check_box\">\r\n                            <input type=\"checkbox\" [(ngModel)]=\"data.selected\"\r\n                                (change)=\"toggleCheck($event, data.member_id)\">\r\n                            {{stripHtml(data.member_id) }}\r\n                        </span> -->\r\n            <td (click)=\"clickedRowTable(data[options.row_id], data)\">{{i + 1 + ((currentPage -1) * pageLimits )}}</td>\r\n            <td (click)=\"clickedRowTable(data[options.row_id], data)\" *ngFor=\"let row of table_head\">\r\n                <div *ngIf=\"!options.check_box\">\r\n                    <span *ngIf=\"isString(data[row])\">{{stripHtml(data[row.data_row_name]) | slice:0:150}}</span>\r\n                    <span *ngIf=\"!isString(data[row])\">{{stripHtml(data[row.data_row_name]) }}</span>\r\n                </div>\r\n            </td>\r\n        </tr>\r\n        <button *ngIf=\"options.check_box\" class=\"btn submit_button\" (click)=\"submit()\"><i class=\"fa fa-fw fa-save\"></i>\r\n            submit</button>\r\n        <tr *ngIf=\"errorLabel\">\r\n            {{errorLabel}}\r\n        </tr>\r\n\r\n    </tbody>\r\n    <tbody *ngIf=\"options.type=='image'\">\r\n        <tr class=\"{{options.type}}\">\r\n            <td [colSpan]=\"table_head.length\">\r\n                <div class=\"img_dt_prp\" *ngFor=\"let data of table_data\"\r\n                    (click)=\"clickedRowTable(data[options.row_id], data)\">\r\n                    <div>\r\n                        <div class=\"image_wrapper\">\r\n                            <img [src]=\"data[options.image_data_property]\" />\r\n                        </div>\r\n                        <div>{{data['name']}}</div>\r\n                    </div>\r\n                </div>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n\r\n<div *ngIf=\"bast_mode\" class=\"print_container\">\r\n    <app-bast [data]=\"salesRedemp\" [thisParent]=\"this\"></app-bast>\r\n</div>\r\n<iframe [src]=\"srcDownload\"></iframe>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/component-libs/form-detail/form-detail.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/component-libs/form-detail/form-detail.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form_detail\"></div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/component-libs/form-input/form-input.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/component-libs/form-input/form-input.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div  *ngIf=\"type=='text'\"  class=\"input-group mb-3\">\r\n  <input class=\"form-control\" [disabled]=\"disabled\" placeholder=\"{{placeholder}}\" type=\"{{type}}\" [(ngModel)]=\"text_value\" (keyup)=\"onKeyupMy($event)\" [ngClass]=\"alert && alert.alert ? 'red-alert':''\" />\r\n</div>\r\n\r\n<div *ngIf=\"type=='password'\" class=\"input-group mb-3\">\r\n  \r\n  <input type=\"text\" class=\"form-control\"placeholder=\"{{placeholder}}\" type=\"{{inputType}}\" [(ngModel)]=\"text_value\" (keyup)=\"onKeyupMy(event)\" [ngClass]=\"alert && alert.alert ? 'red-alert':''\" />\r\n  <div class=\"input-group-append\">\r\n    <span class=\"input-group-text\" *ngIf=\"inputType=='password'\" id=\"basic-addon1\" (click)=\"changePasswordType()\">\r\n      <i   class=\"fa fa-fw fa-eye-slash\"></i>\r\n    </span>\r\n    <span class=\"input-group-text\" *ngIf=\"inputType=='text'\" id=\"basic-addon1\" (click)=\"changePasswordType()\">\r\n      <i class=\"fa fa-fw fa-eye\"></i>\r\n    </span>\r\n    \r\n  </div>\r\n</div>\r\n\r\n<div  *ngIf=\"type=='number'\"  class=\"input-group mb-3\">\r\n  <input class=\"form-control\" placeholder=\"{{placeholder}}\" type=\"{{type}}\" [(ngModel)]=\"text_value\" (keyup)=\"onKeyupMy(event)\" min=\"0\"  [ngClass]=\"alert && alert.alert ? 'red-alert':''\" />\r\n</div>\r\n\r\n<!-- <div  *ngIf=\"type=='select'\"  class=\"input-group mb-3\">\r\n  <select name=\"singleSelect\" [(ngModel)]=\"text_value\">\r\n    <option value=\"product\" [(ngModel)]=\"text_value\">Product</option>\r\n    <option value=\"deals\"   [(ngModel)]=\"text_value\" (keyup)=\"onKeyupMy(event)\">Deals</option>\r\n  </select><br>\r\n  \r\n</div> -->\r\n<div  *ngIf=\"type=='textarea'\"  class=\"input-group mb-3\">\r\n  <textarea class=\"form-control\" placeholder=\"{{placeholder}}\" [(ngModel)]=\"text_value\" (keyup)=\"onKeyupMy(event)\" [ngClass]=\"alert && alert.alert ? 'red-alert':''\"></textarea>\r\n</div>\r\n\r\n<div class=\"red-error-alert\" *ngIf=\"alert && alert.alert\">{{alert.message}}</div>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/component-libs/form-select/form-select.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/component-libs/form-select/form-select.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div  class=\"mb-3\">\r\n    <div class=\"form-group\">\r\n        <select class=\"form-control\"  [(ngModel)]=\"select_value\"  (change)=\"whenItChange()\">\r\n            <option *ngFor=\"let d of data\" value=\"{{d.value}}\"  >{{d.label}}</option>\r\n        </select>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-bast/bast/bast.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-bast/bast/bast.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 id=\"header\"><strong>AUTO BAST</strong></h1>\r\n<div class=\"card-header\" id=\"header\">\r\n    <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n    \r\n</div>\r\n<div class=\"btn-print\">\r\n    <button mat-raised-button color=\"primary\" class=\"btn btn-primary\" (click)=\"prints()\">Print Label<i\r\n            class=\"fa fa-print\" aria-hidden=\"true\"></i></button>\r\n</div>\r\n<div class=\"notif error-message\">{{errorMessage}}</div>\r\n<div class=\"loading\" *ngIf='loading'>\r\n    <div class=\"sk-fading-circle\">\r\n        <div class=\"sk-circle1 sk-circle\"></div>\r\n        <div class=\"sk-circle2 sk-circle\"></div>\r\n        <div class=\"sk-circle3 sk-circle\"></div>\r\n        <div class=\"sk-circle4 sk-circle\"></div>\r\n        <div class=\"sk-circle5 sk-circle\"></div>\r\n        <div class=\"sk-circle6 sk-circle\"></div>\r\n        <div class=\"sk-circle7 sk-circle\"></div>\r\n        <div class=\"sk-circle8 sk-circle\"></div>\r\n        <div class=\"sk-circle9 sk-circle\"></div>\r\n        <div class=\"sk-circle10 sk-circle\"></div>\r\n        <div class=\"sk-circle11 sk-circle\"></div>\r\n        <div class=\"sk-circle12 sk-circle\"></div>\r\n    </div>\r\n</div>\r\n<div class=\"page-break\" id=\"section-to-print\" *ngIf=\"!loading\">\r\n    <div class=\"_container\" *ngFor=\"let datas of data; let i = index\">\r\n        <div class=\"content\" style=\"font-family: Cambria\">\r\n            <div class=\"logo-container\" *ngIf=\"!sig_project && !sig_kontraktual && !sig_promotion_2\">\r\n                <img src=\"assets/images/logo-dynamix-vector.png\" class=\"logo\" />\r\n            </div>\r\n            <div class=\"logo-container\" *ngIf=\"sig_project || sig_kontraktual || sig_promotion_2\">\r\n                <img src=\"assets/images/semengrup.png\" class=\"logo\" />\r\n            </div>\r\n            <div class=\"watermark\">\r\n                <span style=\"color:red;opacity: 0.1;\">ASLI</span>\r\n            </div>\r\n            <header *ngIf=\"!sig_project && !sig_kontraktual && !sig_promotion_2\">\r\n                <strong>BERITA ACARA SERAH TERIMA HADIAH <br />\r\n                    PROGRAM DYNAMIX EXTRA POIN 2020</strong>\r\n                <br />\r\n                Nomor : {{datas.member_detail.id_pel}}/DYNAMIX-SBI/VII/{{datas.approve_date.substr(0,4)}}\r\n            </header>\r\n            <header *ngIf=\"sig_project\">\r\n                <strong>BERITA ACARA SERAH TERIMA HADIAH <br />\r\n                    PROGRAM TACTICAL SEMEN GRESIK</strong>\r\n                <br />\r\n                Nomor : {{datas.member_detail.id_pel}}/{{datas.member_detail.description == 'nonupfront'? \"NONUPFRONT-SG\": \"UPFRONT-SG\"}}/I/{{datas.approve_date.substr(0,4)}}\r\n            </header>\r\n            <header *ngIf=\"sig_kontraktual || sig_promotion_2\">\r\n                <strong>BERITA ACARA SERAH TERIMA HADIAH <br />\r\n                    {{titleProgram}}</strong>\r\n                <br />\r\n                Nomor : {{datas.member_detail.id_pel}}/{{datas.member_detail.cluster + '-SIG'}}/I/{{datas.approve_date.substr(0,4)}}\r\n            </header>\r\n            <br />\r\n            <div class=\"body-letter\">\r\n                Yang bertanda tangan dibawah ini: <br />\r\n                <ol type=\"I\">\r\n                    <li>\r\n                        Nama Distributor <span style=\"margin-left: 20px\"> : {{sig_kontraktual || sig_promotion_2? 'Semen Indonesia' : datas.member_detail.nama_distributor}} </span>\r\n                    </li>\r\n                    <li>Informasi Penerima Hadiah\r\n                        <table class=\"table1\">\r\n                            <tr>\r\n                                <td class=\"first-col\">a.</td>\r\n                                <td class=\"second-col\">ID Pelanggan</td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.id_pel}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td class=\"first-col\">b.</td>\r\n                                <td class=\"second-col\">{{sig_kontraktual || sig_promotion_2? 'Nama Entitas' : 'Nama Toko'}}</td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.nama_toko}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td class=\"first-col\">c.</td>\r\n                                <td class=\"second-col\">Nama Penerima</td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.nama_penerima_gopay}}</td>\r\n                            </tr>\r\n                            <tr *ngIf=\"sig_kontraktual || sig_promotion_2\">\r\n                                <td class=\"first-col\">d.</td>\r\n                                <td class=\"second-col\">Jabatan</td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.description}}</td>\r\n                            </tr>\r\n                            <tr *ngIf=\"!sig_kontraktual && !sig_promotion_2\">\r\n                                <td class=\"first-col\">d.</td>\r\n                                <td class=\"second-col\">Alamat</td>\r\n                                <td class=\"third-col\">:  {{datas.member_detail.alamat_toko}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td class=\"first-col\">e.</td>\r\n                                <td class=\"second-col\">Nomor KTP</td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.ktp_penerima}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td class=\"first-col\">f.</td>\r\n                                <td class=\"second-col\">Nomor NPWP</td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.npwp_penerima}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td class=\"first-col\">g.</td>\r\n                                <td class=\"second-col\">Alamat Pengiriman Hadiah</td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.alamat_rumah}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td class=\"first-col\">h.</td>\r\n                                <td class=\"second-col\">No HP/ WA</td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.no_wa_penerima}}</td>\r\n                            </tr>\r\n                        </table>\r\n                    </li>\r\n                </ol>\r\n                <p>\r\n                    Untuk selanjutnya disebut sebagai \"<strong>PENERIMA</strong>\"\r\n                </p>\r\n        \r\n                    Bahwa dengan ini PENERIMA telah melakukan serah terima hadiah berupa:<br />\r\n                    <span class=\"tabel1-1\">*Tabel 1.1</span><br />\r\n                <table class=\"table2\">\r\n                    <tr>\r\n                        <th class=\"no\">No</th>\r\n                        <th>KODE HADIAH</th>\r\n                        <th>DESKRIPSI HADIAH</th>\r\n                        <th style=\"width: 20px;\">JUMLAH</th>\r\n                        <th>TOTAL NILAI HADIAH (RP)</th>\r\n                        <!-- <th>POIN DITUKAR</th> -->\r\n                    </tr>\r\n                    <tr *ngFor=\"let product of datas.products; let i = index\">\r\n                        <td class=\"no\">{{i+1}}</td>\r\n                        <td>{{product.product_code}}</td>\r\n                        <td style=\"font-size: 8pt;\">{{product.product_name}}</td>\r\n                        <td style=\"width: 20px;\">{{product.quantity}}</td>\r\n                        <!-- <td>{{product.quantity * 100000 | currency: 'Rp'}}</td> -->\r\n                        <td>{{product.total_product_price | currency: 'Rp '}}</td>\r\n                    </tr>\r\n                </table>\r\n               \r\n              <br/>\r\n                <table class=\"table3\">\r\n                    <tr *ngIf=\"datas.member_detail && datas.member_detail.telp_penerima_gopay != '-'\">\r\n                        <th class=\"no\">No</th>\r\n                        <th>{{sig_kontraktual || sig_promotion_2?'No. Akun E-Wallet':'No. Handphone Akun Gopay'}}</th>\r\n                        <th *ngIf=\"!sig_kontraktual && !sig_promotion_2\">Email untuk pengiriman e-voucher Tokopedia</th>\r\n                    </tr>\r\n                    <!-- <tr *ngFor=\"let product of datas.products; let i = index\"> -->\r\n                    <tr *ngIf=\"datas.member_detail && datas.member_detail.telp_penerima_gopay != '-'\">\r\n                        <td class=\"no\">{{i+1}}</td>\r\n                        <!-- <td>{{product.product_code == 'EVC-GPY-0011-DYX' ? datas.member_detail.telp_penerima_gopay : '-'}}</td> -->\r\n                        <td>{{datas.member_detail.telp_penerima_gopay != '-'? datas.member_detail.telp_penerima_gopay : '-'}}</td>\r\n                        <td *ngIf=\"!sig_kontraktual && !sig_promotion_2\">-</td>\r\n                    </tr>\r\n                </table><br />\r\n        \r\n                <p>\r\n                    Untuk Selanjutnya, berdasarakan tabel 1.1 diatas maka hal-hal yang berkaitan dengan hak dan\r\n                    kewajiban namun tidak terbatas pada tanggung jawab atas Hadiah tersebut diatas, sepenuhnya beralih\r\n                    kepada PENERIMA. <br>\r\n                    Demikian Berita Acara Serah Terima ini dibuat untuk digunakan sebagaimana mestinya.\r\n                </p>\r\n                <div class=\"penerima\">\r\n                    <p>\r\n                        Penerima, <br />      <br />  <br />\r\n                        <span style=\"color:grey;opacity: 0.5;\">WAJIB CAP & TTD</span>\r\n                        <br />\r\n                    \r\n                        ( {{datas.member_detail.nama_penerima_gopay}} )\r\n                    </p>\r\n                </div>\r\n            </div>\r\n          \r\n            <p class=\"page-number\" style=\"color:grey;text-align:left; font-size: 10pt;font-family: Calibri; opacity: 0.5;\">\r\n                BAST WAJIB di tanda tangani & Cap Toko setelah menerima Hadiah, dan kirimkan ke :<br/>\r\n                CS {{titleProgram}}<br/>\r\n                Jl. Kebun Jeruk VII No.2E<br/>\r\n                Hayam Wuruk, Tamansari<br/>\r\n                JAKARTA BARAT 11160<br/>\r\n                Hubungi CS di nomor 081197105100/081197105200\r\n            </p>\r\n        </div>\r\n    </div>\r\n    <div class=\"_container\" *ngFor=\"let datas of special; let i = index\">\r\n        <div class=\"content\" style=\"font-family: Cambria\">\r\n            <div class=\"logo-container\" *ngIf=\"!sig_project\">\r\n                <img src=\"assets/images/logo-dynamix-vector.png\" class=\"logo\" />\r\n            </div>\r\n            <div class=\"logo-container\" *ngIf=\"sig_project\">\r\n                <img src=\"assets/images/semengrup.png\" class=\"logo\" />\r\n            </div>\r\n            <div class=\"watermark\">\r\n                <span style=\"color:red;opacity: 0.1;\">ASLI</span>\r\n            </div>\r\n            <header *ngIf=\"!sig_project && !sig_kontraktual && !sig_promotion_2\">\r\n                <strong>SURAT KUASA KHUSUS</strong>\r\n                <br />\r\n                Nomor : {{datas.member_detail.id_pel}}/DYNAMIX-SBI/VII/{{datas.approve_date.substr(0,4)}}\r\n            </header>\r\n            <header *ngIf=\"sig_project\">\r\n                <strong>SURAT KUASA KHUSUS</strong>\r\n                <br />\r\n                Nomor : {{datas.member_detail.id_pel}}/{{datas.member_detail.description == 'nonupfront'? \"NONUPFRONT-SG\": \"UPFRONT-SG\"}}/I/{{datas.approve_date.substr(0,4)}}\r\n            </header>\r\n            <header *ngIf=\"sig_kontraktual || sig_promotion_2\">\r\n                <strong>BERITA ACARA SERAH TERIMA HADIAH <br />\r\n                    {{titleProgram}}</strong>\r\n                <br />\r\n                Nomor : {{datas.member_detail.id_pel}}/{{datas.member_detail.cluster + '-SIG'}}/I/{{datas.approve_date.substr(0,4)}}\r\n            </header>\r\n            <br />\r\n            <div class=\"body-letter\">\r\n                Kami yang bertanda tangan dibawah ini: <br /><br />\r\n              \r\n                  \r\n                        <table class=\"table1\">\r\n                            <tr>\r\n                                <!-- <td class=\"first-col\"></td> -->\r\n                                <td class=\"second-col\"> Nama Distributor </td>\r\n                                <td class=\"third-col\">: {{sig_kontraktual || sig_promotion_2? 'Semen Indonesia' : datas.member_detail.nama_distributor}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <!-- <td class=\"first-col\">b.</td> -->\r\n                                <td class=\"second-col\">{{sig_kontraktual || sig_promotion_2? 'Nama Entitas' : 'Nama Toko'}}</td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.nama_toko}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <!-- <td class=\"first-col\">c.</td> -->\r\n                                <td class=\"second-col\"> {{sig_kontraktual || sig_promotion_2? 'Nama PIC' : 'Nama Pemilik Toko'}}</td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.nama_pemilik}} </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <!-- <td class=\"first-col\">d.</td> -->\r\n                                <td class=\"second-col\"> Nomor KTP </td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.ktp_pemilik}} </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <!-- <td class=\"first-col\">e.</td> -->\r\n                                <td class=\"second-col\">Alamat </td>\r\n                                <td class=\"third-col\">: {{datas.member_detail.alamat_toko}} </td>\r\n                            </tr>\r\n                           \r\n                        </table>\r\n               \r\n              \r\n                <p>\r\n                   <i> Untuk selanjutnya disebut sebagai \"<strong>Pemberi Kuasa</strong>\" </i>\r\n                </p><br />\r\n             \r\n              \r\n                  \r\n                <table class=\"table1\">\r\n                    <tr>\r\n                        <!-- <td class=\"first-col\"></td> -->\r\n                        <td class=\"second-col\"> Nama</td>\r\n                        <td class=\"third-col\">: {{datas.member_detail.nama_penerima_gopay}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <!-- <td class=\"first-col\">b.</td> -->\r\n                        <td class=\"second-col\">Alamat</td>\r\n                        <td class=\"third-col\">: {{datas.member_detail.alamat_rumah}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <!-- <td class=\"first-col\">c.</td> -->\r\n                        <td class=\"second-col\"> Nomor KTP</td>\r\n                        <td class=\"third-col\">: {{datas.member_detail.ktp_penerima}} </td>\r\n                    </tr>\r\n                    <tr>\r\n                        <!-- <td class=\"first-col\">d.</td> -->\r\n                        <td class=\"second-col\"> Nomor NPWP</td>\r\n                        <td class=\"third-col\">: {{datas.member_detail.npwp_penerima}} </td>\r\n                    </tr>\r\n                    <tr>\r\n                        <!-- <td class=\"first-col\">e.</td> -->\r\n                        <td class=\"second-col\">Nomor HP / WA</td>\r\n                        <td class=\"third-col\">: {{datas.member_detail.no_wa_penerima}} </td>\r\n                    </tr>\r\n                   \r\n                </table>\r\n       \r\n      \r\n        <p>\r\n           <i> Untuk selanjutnya disebut sebagai \"<strong>Penerima Kuasa</strong>\"</i>\r\n        </p><br/>\r\n        <div style=\"display:flex; justify-content: center;\"><strong>----------------------------------------------------------KHUSUS-------------------------------------------------------------</strong></div>\r\n        <br/>\r\n        Pemberi Kuasa merupakan pemenang Program Hadiah <strong *ngIf=\"!sig_project && !sig_kontraktual && !sig_promotion_2\">DYNAMIX EXTRA POIN 2020</strong> <strong *ngIf=\"sig_project\">TACTICAL {{datas.member_detail.description == 'nonupfront'? \"NON UPFRONT\": \"UPFRONT\"}} SEMEN GRESIK</strong> <strong *ngIf=\"sig_kontraktual || sig_promotion_2\">{{datas.member_detail.cluster}}</strong> dan atas nama serta kepentingan Pemberi Kuasa, Penerima Kuasa berhak bertindak untuk dan atas nama serta mewakili Pemberi Kuasa untuk melakukan pengambilan dan/atau Penerimaan hadiah berupa:<br />\r\n                    <!-- <span class=\"tabel1-1\">tabel 1.1</span><br /> -->\r\n                <br>\r\n                <table class=\"table2\">\r\n                    <tr>\r\n                        <th class=\"no\">No</th>\r\n                        <th>KODE HADIAH</th>\r\n                        <th>DESKRIPSI HADIAH</th>\r\n                        <th style=\"width: 20px;\">JUMLAH</th>\r\n                        <th>TOTAL NILAI HADIAH (RP)</th>\r\n                        <!-- <th>POIN DITUKAR</th> -->\r\n                    </tr>\r\n                    <tr *ngFor=\"let product of datas.products; let i = index\">\r\n                        <td class=\"no\">{{i+1}}</td>\r\n                        <td>{{product.product_code}}</td>\r\n                        <td style=\"font-size: 8pt;\">{{product.product_name}}</td>\r\n                        <td style=\"width: 20px;\">{{product.quantity}}</td>\r\n                        <td>{{product.quantity * 100000 | currency: 'Rp'}}</td>\r\n                        <!-- <td>{{product.total_product_price}}</td> -->\r\n                    </tr>\r\n                </table>\r\n               \r\n             \r\n        \r\n                <br/>\r\n                    Untuk maksud dan kepentingan tersebut diatas, Penerima Kuasa berhak dan berwenang untuk melakukan segala tindakan yang diperlukan, termasuk namun tidak terbatas pada:<br/>\r\n                    <ol>\r\n                        <li>Menerima Barang dan membuat serta menyerahkan BAST.</li>\r\n                        <li>Membuat dan menandatangani serta menyerahkan dan/atau menerima segala surat dan/atau dokumen yang diperlukan.</li>\r\n                        <li>Melakukan segala tindakan yang diperlukan dan dianggap baik oleh Penerima Kuasa guna terlaksananya dengan baik pemberian kuasa ini.</li>\r\n                    </ol>\r\n                    Demikian Surat Kuasa ini dibuat untuk dipergunakan sebagaimana mestinya\r\n               \r\n                <div class=\"penerima\">\r\n                   ________,__ _______________ _________\r\n                </div>\r\n                <div class=\"penerima-pemberi\">\r\n                    <p style=\"text-align: center;\">\r\n                        Penerima Kuasa <br />      <br />  <br />\r\n                       \r\n                        <br />\r\n                    \r\n                        ( {{datas.member_detail.nama_penerima_gopay}} )\r\n                    </p>\r\n                    <p style=\"text-align: center;\">\r\n                        Pemberi Kuasa <br />      <br />  <br />\r\n                        <span style=\"color:grey;opacity: 0.5;\">Materai Tempel Rp. 6000</span>\r\n                        <br />\r\n                    \r\n                        ( {{datas.member_detail.nama_pemilik}} )\r\n                    </p>\r\n\r\n                </div>\r\n            </div>\r\n          \r\n            <p class=\"page-number\" style=\"color:grey;text-align:left; font-size: 10pt;font-family: Calibri;opacity: 0.5;\">\r\n                BAST WAJIB di tanda tangani & Cap Toko setelah menerima Hadiah, dan kirimkan ke :<br/>\r\n                CS {{titleProgram}}<br/>\r\n                Jl. Kebun Jeruk VII No.2E<br/>\r\n                Hayam Wuruk, Tamansari<br/>\r\n                JAKARTA BARAT 11160<br/>\r\n                Hubungi CS di nomor 081197105100/081197105200\r\n            </p>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin_members/allmember/add/allmember.add.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin_members/allmember/add/allmember.add.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'Add New Member'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  \r\n <div>\r\n    <form #form=\"ngForm\" (ngSubmit)=\"formSubmitAddMember(form.value)\">\r\n      <div class=\"col-md-6\">\r\n          <label>Full Name</label>\r\n          <form-input name=\"full_name\" [type]=\"'text'\" [placeholder]=\"'Full Name'\"  [(ngModel)]=\"full_name\" required></form-input>\r\n\r\n          <label>Email</label>\r\n          <form-input name=\"email\" [placeholder]=\"'Email'\"  [type]=\"'text'\" [(ngModel)]=\"email\" required></form-input>\r\n\r\n          <label>Password</label>\r\n          <form-input name=\"password\" [placeholder]=\"'Password'\"  [type]=\"'password'\" [(ngModel)]=\"password\" pattern=\".{8,}\"  required title=\"8 characters minimum\"></form-input>\r\n\r\n          <label>PIN</label>\r\n          <form-input name=\"pin\" [placeholder]=\"'PIN'\"  [type]=\"'text'\" [(ngModel)]=\"pin\" required></form-input>\r\n\r\n          <label>Cell Phone</label>\r\n          <form-input name=\"cell_phone\" [placeholder]=\"'Cell Phone'\"  [type]=\"'text'\" [(ngModel)]=\"cell_phone\" required></form-input>\r\n\r\n          <label>Card Number</label>\r\n          <form-input name=\"card_number\" [placeholder]=\"'Card Number'\"  [type]=\"'text'\" [(ngModel)]=\"card_number\" required></form-input>\r\n\r\n          <label>Date of Birth</label>\r\n          <!-- <form-input name=\"dob\" [placeholder]=\"'DOB'\"  [type]=\"'text'\" [(ngModel)]=\"dob\" required></form-input> -->\r\n          <app-date-picker [label]=\"dob\"></app-date-picker>\r\n\r\n          <label>City</label>\r\n          <form-input name=\"mcity\" [placeholder]=\"'City'\"  [type]=\"'text'\" [(ngModel)]=\"mcity\" required></form-input>\r\n\r\n          <label>State</label>\r\n          <form-input name=\"mstate\" [placeholder]=\"'State'\"  [type]=\"'text'\" [(ngModel)]=\"mstate\" required></form-input>\r\n\r\n          <label>Country</label>\r\n          <form-input name=\"mcountry\" [placeholder]=\"'Country'\"  [type]=\"'text'\" [(ngModel)]=\"mcountry\" required></form-input>\r\n\r\n          <label>Address</label>\r\n          <form-input name=\"maddress1\" [placeholder]=\"'Address'\"  [type]=\"'textarea'\" [(ngModel)]=\"maddress1\" required></form-input>\r\n\r\n          <label>Post Code</label>\r\n          <form-input name=\"mpostcode\" [placeholder]=\"'Post Code'\"  [type]=\"'text'\" [(ngModel)]=\"mpostcode\" required></form-input>\r\n\r\n          <label>Gender</label>\r\n          <form-input name=\"gender\" [placeholder]=\"'Gender'\"  [type]=\"'text'\" [(ngModel)]=\"gender\" required></form-input>\r\n\r\n          <label>Marital Status</label>\r\n          <form-input name=\"marital_status\" [placeholder]=\"'Marital Status'\"  [type]=\"'text'\" [(ngModel)]=\"maritalStatus\" required></form-input>\r\n\r\n          <label>Member Type</label>\r\n          <form-select name=\"type\" [(ngModel)]=\"type\" [data]=\"member_type\"></form-select>\r\n         <!-- {{form.value | json}}  -->\r\n      </div>\r\n        \r\n        <button class=\"btn\" type=\"submit\" [disabled]=\"form.pristine\">Submit</button> \r\n      </form>\r\n </div>\r\n \r\n</div>\r\n\r\n "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin_members/allmember/allmember.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin_members/allmember/allmember.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"''\" [icon]=\"'fa-table'\"></app-page-header>\r\n  \r\n  <!-- <button (click)=\"onDownload()\" (click)=\"getDownloadFileLint()\"><i class=\"fa fa-file-pdf-o\"></i>Download</button>\r\n  <iframe [src]=\"srcDownload\"></iframe> -->\r\n  <div *ngIf=\"AllMember&&allMemberDetail==false\">\r\n        <app-form-builder-table\r\n        [table_data]  = \"AllMember\" \r\n        [searchCallback]= \"[service,'searchAllMemberLint',this]\" \r\n        [tableFormat]=\"tableFormat\"\r\n        [total_page]=\"totalPage\"\r\n        >\r\n            \r\n        </app-form-builder-table>\r\n  </div>\r\n  <div *ngIf=\"allMemberDetail\">\r\n    <app-all-member-detail [back]=\"[this,backToHere]\" [detail]=\"allMemberDetail\"></app-all-member-detail>\r\n</div>\r\n \r\n</div>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin_members/allmember/detail/allmember.detail.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin_members/allmember/detail/allmember.detail.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <h1>\r\n                {{detail._id}}\r\n        </h1>\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"all-member-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> All Member Details </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                                     \r\n                             <label>Full Name</label>\r\n                             <div name=\"full_name\">{{detail.full_name}}</div>\r\n\r\n                             <label>Member Status</label>\r\n                             <div name=\"member_status\">{{detail.member_status}}</div>\r\n\r\n                             <label>Address</label>\r\n                             <div name=\"maddress1\">{{detail.maddress1}}</div>\r\n\r\n                             <label>City </label>\r\n                             <div name=\"mcity\">{{detail.mcity}}</div>\r\n\r\n                             <label>PostCode </label>\r\n                             <div name=\"mpostcode\">{{detail.mpostcode}}</div>\r\n\r\n                             <label>State </label>\r\n                             <div name=\"mstate\">{{detail.mstate}}</div>\r\n\r\n                             <label>Country </label>\r\n                             <div name=\"mcountry\">{{detail.mcountry}}</div>\r\n\r\n\r\n\r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Email </label>\r\n                            <div name=\"email\">{{detail.email}}</div>\r\n\r\n                            <label>Cell Phone </label>\r\n                            <div name=\"cell_phone\">{{detail.cell_phone}}</div>\r\n\r\n                            <label>Registration Date </label>\r\n                            <div name=\"registration_date\">{{detail.registration_date}}</div>\r\n\r\n                            <label>Point Balance </label>\r\n                            <div name=\"point_balance\">{{detail.point_balance}}</div>\r\n\r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                 \r\n                </div>\r\n            </div>\r\n            \r\n            <div class=\"row\">\r\n                <div class=\"col-md-12 bg-delete\"><button class=\"btn delete-button float-right btn-danger\" (click)=\"deleteThis()\">delete this</button></div>\r\n            </div>\r\n            \r\n         </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-all-member-edit [back]=\"[this,'editThis']\" [detail]=\"detail\"></app-all-member-edit>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin_members/allmember/edit/allmember.edit.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin_members/allmember/edit/allmember.edit.component.html ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span></button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"all-member-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> All Member Details </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             <label>Organization ID</label>\r\n                             <form-input  name=\"org_id\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Organization ID'\"  [(ngModel)]=\"detail.org_id\" autofocus required></form-input>\r\n             \r\n                             <label>Merchant ID</label>\r\n                             <form-input  name=\"merchant_id\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Merchant ID'\"  [(ngModel)]=\"detail.merchant_id\" autofocus required></form-input>\r\n             \r\n                             <label>Member ID</label>\r\n                             <form-input  name=\"member_id\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Member ID'\"  [(ngModel)]=\"detail.member_id\" autofocus required></form-input>\r\n             \r\n                             <label>Card Number</label>\r\n                             <form-input name=\"card_number\" [type]=\"'text'\" [placeholder]=\"'Card Number'\"  [(ngModel)]=\"detail.card_number\" autofocus required></form-input>\r\n             \r\n                             <label>Full Name</label>\r\n                             <form-input name=\"full_name\" [type]=\"'text'\" [placeholder]=\"'Full Name'\"  [(ngModel)]=\"detail.full_name\" autofocus required></form-input>\r\n\r\n                             <label>Member Status</label>\r\n                             <div name=\"member_status\"> {{memberStatus}}\r\n                                <form-select name=\"member_status\" [(ngModel)]=\"detail.member_status\" [data]=\"memberStatus\" required></form-select> \r\n                             </div> \r\n\r\n                             <label>Address</label>\r\n                             <form-input name=\"maddress1\" [type]=\"'textarea'\" [placeholder]=\"'Address'\"  [(ngModel)]=\"detail.maddress1\" autofocus required></form-input>\r\n\r\n                             <label>City </label>\r\n                             <form-input name=\"mcity\" [type]=\"'text'\" [placeholder]=\"'City'\"  [(ngModel)]=\"detail.mcity\" autofocus required></form-input>\r\n\r\n                             <label>Post Code </label>\r\n                             <form-input name=\"mpostcode\" [type]=\"'text'\" [placeholder]=\"'Post Code'\"  [(ngModel)]=\"detail.post_code\" autofocus required></form-input>\r\n\r\n                             <label>State </label>\r\n                             <form-input name=\"mstate\" [type]=\"'text'\" [placeholder]=\"'State'\"  [(ngModel)]=\"detail.mstate\" autofocus required></form-input>\r\n\r\n                             <label>Country </label>\r\n                             <form-input name=\"mcountry\" [type]=\"'text'\" [placeholder]=\"'Country'\"  [(ngModel)]=\"detail.mcountry\" autofocus required></form-input>\r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Email </label>\r\n                            <form-input name=\"email\" [type]=\"'text'\" [placeholder]=\"'Email'\"  [(ngModel)]=\"detail.email\" autofocus required></form-input>\r\n\r\n                            <label>Cell Phone </label>\r\n                            <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Cell Phone'\"  [(ngModel)]=\"detail.cell_phone\" autofocus required></form-input>\r\n                            \r\n                            <label>Registration Date </label>\r\n                            <form-input  name=\"registration_date\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Registration Date'\"  [(ngModel)]=\"detail.registration_date\" autofocus required></form-input>\r\n\r\n                            <label>Point Balance </label>\r\n                            <form-input  name=\"point_balance\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Point Balance'\"  [(ngModel)]=\"detail.point_balance\" autofocus required></form-input>\r\n\r\n                            <label>Activation Code </label>\r\n                            <form-input  name=\"activation_code\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Activation Code'\"  [(ngModel)]=\"detail.activation_code\" autofocus required></form-input>\r\n\r\n                            <label>Activation Status </label>\r\n                            <div name=\"activation_status\"> {{activationStatus}}\r\n                                <form-select name=\"activation_status\" [(ngModel)]=\"detail.activation_status\" [data]=\"activationStatus\" required></form-select> \r\n                             </div> \r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                 \r\n                </div>\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    \r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/bs-component.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/bs-component.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <app-page-header [heading]=\"'Bootstrap Component'\" [icon]=\"'fa-th-list'\"></app-page-header>\r\n    <app-modal></app-modal>\r\n    <app-alert></app-alert>\r\n    <app-buttons></app-buttons>\r\n    <app-collapse></app-collapse>\r\n    <app-date-picker></app-date-picker>\r\n    <app-dropdown></app-dropdown>\r\n    <app-pagination></app-pagination>\r\n    <app-pop-over></app-pop-over>\r\n    <app-progressbar></app-progressbar>\r\n    <app-tabs></app-tabs>\r\n    <app-tooltip></app-tooltip>\r\n    <app-timepicker></app-timepicker>\r\n    <app-rating></app-rating>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/alert/alert.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/alert/alert.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-lg-12\">\r\n        <div class=\"card mb-3\">\r\n            <div class=\"card-header\">Buttons</div>\r\n            <div class=\"card-body\">\r\n                <p *ngFor=\"let alert of alerts\">\r\n                    <ngb-alert [type]=\"alert.type\" (close)=\"closeAlert(alert)\">{{ alert.message }}</ngb-alert>\r\n                </p>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/buttons/buttons.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/buttons/buttons.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row mb-3\">\r\n    <div class=\"col col-sm-6\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">\r\n                Radio Button group (<strong>Using ngModel</strong>)\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <div class=\"btn-group btn-group-toggle mb-3\" ngbRadioGroup name=\"radioBasic\" [(ngModel)]=\"model\">\r\n                    <label ngbButtonLabel class=\"btn-primary\">\r\n                        <input ngbButton type=\"radio\" [value]=\"1\"> Left (pre-checked)\r\n                    </label>\r\n                    <label ngbButtonLabel class=\"btn-primary\">\r\n                        <input ngbButton type=\"radio\" value=\"middle\"> Middle\r\n                    </label>\r\n                    <label ngbButtonLabel class=\"btn-primary\">\r\n                        <input ngbButton type=\"radio\" [value]=\"false\"> Right\r\n                    </label>\r\n                </div>\r\n                <div class=\"alert alert-info mb-0\">\r\n                    <strong>Selected Value: </strong>{{model}}\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"col col-sm-6\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">\r\n                Radio Button group (<strong>Ractive Forms</strong>)\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <form [formGroup]=\"radioGroupForm\" class=\"mb-3\">\r\n                    <div class=\"btn-group btn-group-toggle\" ngbRadioGroup name=\"radioBasic\" formControlName=\"model\">\r\n                        <label ngbButtonLabel class=\"btn-primary\">\r\n                            <input ngbButton type=\"radio\" [value]=\"1\"> Left (pre-checked)\r\n                        </label>\r\n                        <label ngbButtonLabel class=\"btn-primary\">\r\n                            <input ngbButton type=\"radio\" value=\"middle\"> Middle\r\n                        </label>\r\n                        <label ngbButtonLabel class=\"btn-primary\">\r\n                            <input ngbButton type=\"radio\" [value]=\"false\"> Right\r\n                        </label>\r\n                    </div>\r\n                </form>\r\n                <div class=\"alert alert-info mb-0\">\r\n                    <strong>Selected Value: </strong>{{radioGroupForm.value.model}}\r\n                </div>\r\n                <!-- <form [formGroup]=\"radioGroupForm\">\r\n                    <div ngbRadioGroup name=\"radioBasic\" formControlName=\"model\">\r\n                        <label class=\"btn btn-primary\">\r\n                            <input type=\"radio\" [value]=\"1\"> Left (pre-checked)\r\n                        </label>\r\n                        <label class=\"btn btn-primary\">\r\n                            <input type=\"radio\" value=\"middle\"> Middle\r\n                        </label>\r\n                        <label class=\"btn btn-primary\">\r\n                            <input type=\"radio\" [value]=\"false\"> Right\r\n                        </label>\r\n                    </div>\r\n                </form> -->\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/collapse/collapse.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/collapse/collapse.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-lg-12\">\r\n        <div class=\"card mb-3\">\r\n            <div class=\"card-header\">Collapse</div>\r\n            <div class=\"card-body\">\r\n                <p>\r\n                    <button type=\"button\" class=\"btn btn-outline-primary\" (click)=\"isCollapsed = !isCollapsed\"\r\n                        [attr.aria-expanded]=\"!isCollapsed\" aria-controls=\"collapseExample\">\r\n                        Toggle\r\n                    </button>\r\n                </p>\r\n                <div id=\"collapseExample\" [ngbCollapse]=\"isCollapsed\">\r\n                    <div class=\"card\">\r\n                        <div class=\"card-body\">\r\n                            You can collapse this card by clicking Toggle\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/date-picker/date-picker.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/date-picker/date-picker.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-lg-12\">\r\n        <div class=\"card mb-3\">\r\n            <div class=\"card-header\">{{label}}</div>\r\n            <div class=\"card-body\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"input-group datepicker-input\">\r\n                        <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                            name=\"dp\" [(ngModel)]=\"model\" ngbDatepicker #d=\"ngbDatepicker\">\r\n                        <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                            <!-- <img src=\"img/calendar-icon.svg\" style=\"width: 1.2rem; height: 1rem; cursor: pointer;\"/> -->\r\n                            <span class=\"fa fa-calendar\"></span>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n                <!-- <div class=\"alert alert-info mb-0\">\r\n                    <strong>Model: </strong> {{ model | json }}\r\n                </div> -->\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/dropdown/dropdown.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/dropdown/dropdown.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n    <div class=\"card-header\">Dropdown</div>\r\n    <div class=\"card-body\">\r\n        <div class=\"row\">\r\n            <div class=\"col\">\r\n                <div ngbDropdown class=\"d-inline-block dropdown\">\r\n                    <button class=\"btn btn-outline-primary\" ngbDropdownToggle>Toggle dropdown</button>\r\n                    <div ngbDropdownMenu>\r\n                        <button class=\"dropdown-item\">Action</button>\r\n                        <button class=\"dropdown-item\">Another Action</button>\r\n                        <button class=\"dropdown-item\">Something else is here</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col text-right\">\r\n                <div ngbDropdown placement=\"top-right\" class=\"d-inline-block dropdown\">\r\n                    <button class=\"btn btn-outline-primary\" ngbDropdownToggle>Toggle dropup</button>\r\n                    <div ngbDropdownMenu>\r\n                        <button class=\"dropdown-item\">Action</button>\r\n                        <button class=\"dropdown-item\">Another Action</button>\r\n                        <button class=\"dropdown-item\">Something else is here</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <hr>\r\n        <p class=\"mb-2\">You can easily control dropdowns programmatically using the exported dropdown instance.</p>\r\n        <div class=\"d-inline-block dropdown\" ngbDropdown #myDrop=\"ngbDropdown\">\r\n            <button class=\"btn btn-outline-primary\" ngbDropdownToggle>Toggle dropdown</button>\r\n            <div ngbDropdownMenu>\r\n                <button class=\"dropdown-item\">Action</button>\r\n                <button class=\"dropdown-item\">Another Action</button>\r\n                <button class=\"dropdown-item\">Something else is here</button>\r\n            </div>\r\n        </div>\r\n        <div class=\"d-inline-block dropdown\" ngbDropdown #myDrop=\"ngbDropdown\">\r\n            <button class=\"btn btn-outline-success\" (click)=\"$event.stopPropagation(); myDrop.open();\">Open from outside</button>\r\n        </div>\r\n        <div class=\"d-inline-block dropdown\" ngbDropdown #myDrop=\"ngbDropdown\">\r\n            <button class=\"btn btn-outline-danger\" (click)=\"$event.stopPropagation(); myDrop.close();\">Close from outside</button>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/modal/modal.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/modal/modal.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <div class=\"card mb-3\">\r\n            <div class=\"card-header\">Modal</div>\r\n            <!-- Large modal -->\r\n            <div class=\"card-body\">\r\n                <button class=\"btn btn-primary\" (click)=\"open(content)\">Large modal</button>\r\n                <ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n                    <div class=\"modal-header\">\r\n                        <h4 class=\"modal-title\">Modal title</h4>\r\n                        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\r\n                            <span aria-hidden=\"true\">&times;</span>\r\n                        </button>\r\n                    </div>\r\n                    <div class=\"modal-body\">\r\n                        <p>One fine body&hellip;</p>\r\n                    </div>\r\n                    <div class=\"modal-footer\">\r\n                        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c('Close click')\">Close</button>\r\n                    </div>\r\n                </ng-template>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/pagination/pagination.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/pagination/pagination.component.html ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"row\">\r\n    <div class=\"col col-sm-6\">\r\n        <div class=\"card mt-3\">\r\n            <div class=\"card-header\">Basic Pagination</div>\r\n            <div class=\"card-body\">\r\n                <div class=\"text-uppercase text-muted fs-12\">Default pagination</div>\r\n                <ngb-pagination [collectionSize]=\"70\" [(page)]=\"defaultPagination\"></ngb-pagination>\r\n                <div class=\"text-uppercase text-muted fs-12\">directionLinks = false</div>\r\n                <ngb-pagination [collectionSize]=\"70\" [(page)]=\"defaultPagination\" [directionLinks]=\"false\"></ngb-pagination>\r\n                <div class=\"text-uppercase text-muted fs-12\">boundaryLinks = true</div>\r\n                <ngb-pagination [collectionSize]=\"70\" [(page)]=\"defaultPagination\" [boundaryLinks]=\"true\"></ngb-pagination>\r\n                <div class=\"alert alert-info\">\r\n                    <b>Current page: </b>{{defaultPagination}}\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"col col-sm-6\">\r\n        <div class=\"card mt-3\">\r\n            <div class=\"card-header\">Advanced Pagination</div>\r\n            <div class=\"card-body\">\r\n                <div class=\"text-uppercase text-muted fs-12\">maxSize = 5, rotate = false</div>\r\n                <ngb-pagination [collectionSize]=\"120\" [(page)]=\"advancedPagination\" [maxSize]=\"5\" [boundaryLinks]=\"true\"></ngb-pagination>\r\n\r\n                <div class=\"text-uppercase text-muted fs-12\">maxSize = 5, rotate = true</div>\r\n                <ngb-pagination [collectionSize]=\"120\" [(page)]=\"advancedPagination\" [maxSize]=\"5\" [rotate]=\"true\" [boundaryLinks]=\"true\"></ngb-pagination>\r\n\r\n                <div class=\"text-uppercase text-muted fs-12\">maxSize = 5, rotate = true, ellipses = false</div>\r\n                <ngb-pagination [collectionSize]=\"120\" [(page)]=\"advancedPagination\" [maxSize]=\"5\" [rotate]=\"true\" [ellipses]=\"false\" [boundaryLinks]=\"true\"></ngb-pagination>\r\n                <div class=\"alert alert-info\">\r\n                    <b>Current page: </b>{{advancedPagination}}\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col col-sm-6\">\r\n        <div class=\"card mt-3\">\r\n            <div class=\"card-header\">Pagination size</div>\r\n            <div class=\"card-body\">\r\n                <ngb-pagination [collectionSize]=\"50\" [(page)]=\"paginationSize\" size=\"lg\"></ngb-pagination>\r\n                <ngb-pagination [collectionSize]=\"50\" [(page)]=\"paginationSize\"></ngb-pagination>\r\n                <ngb-pagination [collectionSize]=\"50\" [(page)]=\"paginationSize\" size=\"sm\"></ngb-pagination>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"col col-sm-6\">\r\n        <div class=\"card mt-3\">\r\n            <div class=\"card-header\">Disabled pagination</div>\r\n            <div class=\"card-body\">\r\n                <p>Pagination control can be disabled:</p>\r\n                <ngb-pagination [collectionSize]=\"70\" [(page)]=\"disabledPagination\" [disabled]='isDisabled'></ngb-pagination>\r\n                <hr>\r\n                <button class=\"btn btn-outline-primary\" (click)=\"toggleDisabled()\">\r\n                    Toggle disabled\r\n                </button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div> -->\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/pop-over/pop-over.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/pop-over/pop-over.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card mt-3\">\r\n    <div class=\"card-header\">\r\n        Pop over\r\n    </div>\r\n    <div class=\"card-body\">\r\n        <button type=\"button\" class=\"btn btn-secondary\" placement=\"top\"\r\n                ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on top\">\r\n          Popover on top\r\n        </button>\r\n\r\n        <button type=\"button\" class=\"btn btn-secondary\" placement=\"right\"\r\n                ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on right\">\r\n          Popover on right\r\n        </button>\r\n\r\n        <button type=\"button\" class=\"btn btn-secondary\" placement=\"bottom\"\r\n                ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on bottom\">\r\n          Popover on bottom\r\n        </button>\r\n\r\n        <button type=\"button\" class=\"btn btn-secondary\" placement=\"left\"\r\n                ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on left\">\r\n          Popover on left\r\n        </button>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/progressbar/progressbar.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/progressbar/progressbar.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card mt-3\">\r\n    <div class=\"card-header\">Progressbar</div>\r\n    <div class=\"card-body\">\r\n        <p><ngb-progressbar type=\"info\" [value]=\"50\"></ngb-progressbar></p>\r\n        <p><ngb-progressbar showValue=\"true\" type=\"warning\" [value]=\"150\" [max]=\"200\"></ngb-progressbar></p>\r\n        <p><ngb-progressbar type=\"danger\" [value]=\"100\" [striped]=\"true\"></ngb-progressbar></p>\r\n        <p><ngb-progressbar type=\"success\" [value]=\"25\">25</ngb-progressbar></p>\r\n        <p><ngb-progressbar type=\"info\" [value]=\"50\">Copying file <b>2 of 4</b>...</ngb-progressbar></p>\r\n        <p><ngb-progressbar type=\"warning\" [value]=\"75\" [striped]=\"true\" [animated]=\"true\"><i>50%</i></ngb-progressbar></p>\r\n        <p class=\"mb-0\"><ngb-progressbar type=\"danger\" [value]=\"100\" [striped]=\"true\">Completed!</ngb-progressbar></p>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/rating/rating.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/rating/rating.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n    <div class=\"card-header\">\r\n        Rating (<strong>Basic demo</strong>)\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <ngb-rating [(rate)]=\"currentRate\"></ngb-rating>\r\n      <hr>\r\n      <pre>Rate: <b>{{currentRate}}</b></pre>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/tabs/tabs.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/tabs/tabs.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row mt-3\">\r\n    <div class=\"col-sm-6\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">Tabset</div>\r\n            <div class=\"card-body\">\r\n                <ngb-tabset>\r\n                    <ngb-tab title=\"Simple\">\r\n                        <ng-template ngbTabContent>\r\n                            <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth\r\n                            master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh\r\n                            dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum\r\n                            iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>\r\n                        </ng-template>\r\n                    </ngb-tab>\r\n                    <ngb-tab>\r\n                        <ng-template ngbTabTitle><b>Fancy</b> title</ng-template>\r\n                        <ng-template ngbTabContent>\r\n                            <p>Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table\r\n                            craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl\r\n                            cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia\r\n                            yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean\r\n                            shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero\r\n                            sint qui sapiente accusamus tattooed echo park.</p>\r\n                        </ng-template>\r\n                    </ngb-tab>\r\n                    <ngb-tab title=\"Disabled\" [disabled]=\"true\">\r\n                        <ng-template ngbTabContent>\r\n                            <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth\r\n                            master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh\r\n                            dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum\r\n                            iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>\r\n                        </ng-template>\r\n                    </ngb-tab>\r\n                </ngb-tabset>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"col-sm-6\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">Pills</div>\r\n            <div class=\"card-body\">\r\n                <ngb-tabset type=\"pills\">\r\n                    <ngb-tab title=\"Simple\">\r\n                        <ng-template ngbTabContent>\r\n                            <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth\r\n                            master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh\r\n                            dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum\r\n                            iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>\r\n                        </ng-template>\r\n                    </ngb-tab>\r\n                    <ngb-tab>\r\n                        <ng-template ngbTabTitle><b>Fancy</b> title</ng-template>\r\n                        <ng-template ngbTabContent>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.\r\n                            <p>Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table\r\n                            craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl\r\n                            cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia\r\n                            yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean\r\n                            shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero\r\n                            sint qui sapiente accusamus tattooed echo park.</p>\r\n                        </ng-template>\r\n                    </ngb-tab>\r\n                    <ngb-tab title=\"Disabled\" [disabled]=\"true\">\r\n                        <ng-template ngbTabContent>\r\n                            <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth\r\n                            master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh\r\n                            dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum\r\n                            iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>\r\n                        </ng-template>\r\n                    </ngb-tab>\r\n                </ngb-tabset>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/timepicker/timepicker.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/timepicker/timepicker.component.html ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row mt-3\">\r\n    <div class=\"col-sm-6\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">Timepicker (<b>Default</b>)</div>\r\n            <div class=\"card-body\">\r\n                <ngb-timepicker [(ngModel)]=\"defaultTime\"></ngb-timepicker>\r\n                <div class=\"alert alert-info\">\r\n                    <b>Selected time: </b>{{defaultTime | json}}\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"col-sm-6\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">Timepicker (<b>Default</b>)</div>\r\n            <div class=\"card-body\">\r\n                <ngb-timepicker [(ngModel)]=\"meridianTime\" [meridian]=\"meridian\"></ngb-timepicker>\r\n                <button class=\"btn btn-sm btn-outline-{{meridian ? 'success' : 'danger'}}\" (click)=\"toggleMeridian()\">\r\n                    Meridian - {{meridian ? \"ON\" : \"OFF\"}}\r\n                </button>\r\n                <div class=\"alert alert-info mt-3 mb-0\">\r\n                    <b>Selected time: </b>{{meridianTime | json}}\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"col-sm-6\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">Timepicker (<b>Seconds</b>)</div>\r\n            <div class=\"card-body\">\r\n                <ngb-timepicker [(ngModel)]=\"SecondsTime\" [seconds]=\"seconds\"></ngb-timepicker>\r\n                <button class=\"btn btn-sm btn-outline-{{seconds ? 'success' : 'danger'}}\" (click)=\"toggleSeconds()\">\r\n                    Seconds - {{seconds ? \"ON\" : \"OFF\"}}\r\n                </button>\r\n                <div class=\"alert alert-info mt-3 mb-0\">\r\n                    <b>Selected time: </b>{{SecondsTime | json}}\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"col-sm-6 mt-3\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">Timepicker (<b>Custom Step</b>)</div>\r\n            <div class=\"card-body\">\r\n                <ngb-timepicker [(ngModel)]=\"customTime\" [seconds]=\"true\"\r\n                [hourStep]=\"hourStep\" [minuteStep]=\"minuteStep\" [secondStep]=\"secondStep\"></ngb-timepicker>\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-4\">\r\n                        <label for=\"changeHourStep\">Hour Step</label>\r\n                        <input type=\"number\" class=\"form-control\" [(ngModel)]=\"hourStep\" />\r\n                    </div>\r\n                    <div class=\"col-sm-4\">\r\n                        <label for=\"changeMinuteStep\">Minute Step</label>\r\n                        <input type=\"number\" class=\"form-control\" [(ngModel)]=\"minuteStep\" />\r\n                    </div>\r\n                    <div class=\"col-sm-4\">\r\n                        <label for=\"changeSecondStep\">Second Step</label>\r\n                        <input type=\"number\" class=\"form-control\" [(ngModel)]=\"secondStep\" />\r\n                    </div>\r\n                </div>\r\n                <div class=\"alert alert-info mt-3 mb-0\">\r\n                    <b>Selected time: </b>{{customTime | json}}\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/tooltip/tooltip.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/bs-component/components/tooltip/tooltip.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n    <div class=\"card-header\">\r\n        Tooltip\r\n    </div>\r\n    <div class=\"card-body\">\r\n        <button type=\"button\" class=\"btn btn-secondary\" placement=\"top\" ngbTooltip=\"Tooltip on top\">\r\n          Tooltip on top\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary\" placement=\"right\" ngbTooltip=\"Tooltip on right\">\r\n          Tooltip on right\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary\" placement=\"bottom\" ngbTooltip=\"Tooltip on bottom\">\r\n          Tooltip on bottom\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary\" placement=\"left\" ngbTooltip=\"Tooltip on left\">\r\n          Tooltip on left\r\n        </button>\r\n        <ng-template #StaticTipContent><em>Tooltip</em> <u>with</u> <b>HTML</b></ng-template>\r\n        <button type=\"button\" class=\"btn btn-secondary\" data-html=\"true\" [ngbTooltip]=\"StaticTipContent\">\r\n          Tooltip with HTML\r\n        </button>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notificationmessage/detail/notificationgroup.detail.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/notificationmessage/detail/notificationgroup.detail.component.html ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.name}}</h1>\r\n        <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button>\r\n    </div> \r\n    <div class=\"card-content\">\r\n        <div class=\"notificationgroup-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> Notification Group Details </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             <label>Notification Group ID</label>\r\n                             <div name=\"_id\" >{{detail._id}}</div>\r\n                             <label>ID</label>\r\n                             <div name=\"id\" >{{detail.id}}</div>\r\n                             <label>Group Name</label>\r\n                             <div name=\"name\" >{{detail.name}}</div>\r\n                             <label>Description</label>\r\n                             <div name=\"description\" >{{detail.description}}</div>\r\n                             <label>Topic</label>\r\n                             <div name=\"topic\" >{{detail.topic}}</div>\r\n                             <label>Status</label>\r\n                             <div name=\"status\" >{{detail.status}}</div>\r\n                             <label>Token List</label>\r\n                             <div name=\"token_list\" >{{detail.token_list}}</div>\r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2>Notification Group &amp; Status Notification Group</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                            <label>Created Date</label>\r\n                            <div name=\"created_date\" >{{detail.created_date}}</div>\r\n                            <label>Created By</label>\r\n                            <div name=\"created_by\" >{{detail.created_by}}</div>\r\n                            <label>Created At</label>\r\n                            <div name=\"created_at\" >{{detail.created_at}}</div>\r\n                            <label>Updated Date</label>\r\n                            <div name=\"updated_date\" >{{detail.updated_date}}</div>\r\n                            <label>Updated By</label>\r\n                            <div name=\"updated_by\" >{{detail.updated_by}}</div>\r\n                            <label>Updated At</label>\r\n                            <div name=\"updated_at\" >{{detail.updated_at}}</div>\r\n                            \r\n                             \r\n                         </div>\r\n                       \r\n                      </div> \r\n                 </div>\r\n                 \r\n                </div>\r\n            </div>\r\n            <!-- <div class=\"row\">\r\n                <div class=\"col-md-12 bg-delete\"><button class=\"btn delete-button float-right btn-danger\" (click)=\"deleteThis()\">delete this</button></div>\r\n            </div> -->\r\n         </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-notificationgroup-edit [back]=\"[this, 'editThis']\" [detail]=\"detail\"></app-notificationgroup-edit>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notificationmessage/edit/notificationgroup.edit.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/notificationmessage/edit/notificationgroup.edit.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail._id}}</h1>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span></button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"notificationgroup-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> Edit Notification Group Details </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             <label>Notification Group ID</label>\r\n                             <form-input  name=\"_id\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'ID'\"  [(ngModel)]=\"detail._id\" autofocus required></form-input>\r\n                             <label>Group Name</label>\r\n                             <form-input name=\"name\" [type]=\"'text'\" [placeholder]=\"'Name'\"  [(ngModel)]=\"detail.name\" autofocus required></form-input>\r\n                             <label>Description</label>\r\n                             <form-input name=\"name\" [type]=\"'text'\" [placeholder]=\"'Description'\"  [(ngModel)]=\"detail.description\" autofocus required></form-input>\r\n                            <label>Notification Topic</label>\r\n                            <div name=\"topic\"> {{topic}}\r\n                            <form-select name=\"topic\" [(ngModel)]=\"detail.topic\" [data]=\"notificationTopic\" required></form-select> \r\n                            </div>\r\n                            <label>Status</label>\r\n                            <div name=\"status\"> {{status}}\r\n                            <form-select name=\"status\" [(ngModel)]=\"detail.status\" [data]=\"notificationStatus\" required></form-select> \r\n                            </div>\r\n                            <label>Token List</label>\r\n                             <form-input  name=\"token_list\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Token List'\"  [(ngModel)]=\"detail.token_list\" autofocus required></form-input>\r\n                            \r\n                            \r\n                             <!-- <label>User ID</label>\r\n                             <form-input name=\"user_id\" [type]=\"'text'\" [placeholder]=\"'User ID'\"  [(ngModel)]=\"detail.user_id\" autofocus required></form-input> -->\r\n                         </div>\r\n                         <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                        </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                 <div class=\"card mb-3\">\r\n                    <div class=\"card-header\"><h2>Notification Group &amp; Status Notification Group</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Created Date</label>\r\n                            <form-input  name=\"created_date\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Created Date'\"  [(ngModel)]=\"detail.created_date\" autofocus required></form-input>\r\n\r\n                            <label>Created By</label>\r\n                            <form-input  name=\"created_by\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Created By'\"  [(ngModel)]=\"detail.created_by\" autofocus required></form-input>\r\n                             \r\n                            <label>Created At</label>\r\n                            <form-input  name=\"created_at\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Created At'\"  [(ngModel)]=\"detail.created_at\" autofocus required></form-input>\r\n\r\n                            <label>Updated Date</label>\r\n                            <form-input  name=\"updated_date\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Updated Date'\"  [(ngModel)]=\"detail.updated_date\" autofocus required></form-input>\r\n\r\n                            <label>Updated By</label>\r\n                            <form-input  name=\"updated_by\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Updated By'\"  [(ngModel)]=\"detail.updated_by\" autofocus required></form-input>\r\n\r\n                            <label>Updated At</label>\r\n                            <form-input  name=\"updated_at\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Updated At'\"  [(ngModel)]=\"detail.updated_at\" autofocus required></form-input>\r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div> \r\n                 \r\n                </div>\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    \r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/modules/page-header/page-header.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/modules/page-header/page-header.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-xl-12\">\r\n        <div *ngIf=\"topBarMenu\" class=\"top-bar-menu\">\r\n                <a mat-raised-button color=\"primary\" *ngFor=\"let tbm of topBarMenu \" routerLink=\"{{tbm.routerLink}}\" [ngClass]=\"isActiveClass(tbm.active)\" [routerLinkActive]=\"\">\r\n                    {{tbm.label}}\r\n                </a>\r\n        </div>\r\n        <h2 class=\"page-header\">\r\n            {{heading}}\r\n        </h2>\r\n        <!-- <ol class=\"breadcrumb\">\r\n            <li class=\"breadcrumb-item\">\r\n                <i class=\"fa fa-dashboard\"></i> <a id=\"dash\" href=\"Javascript:void(0)\" [routerLink]=\"['/dashboard']\">Dashboard</a>\r\n            </li>\r\n            <li class=\"breadcrumb-item active\"> {{heading}}</li>\r\n        </ol> -->\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/modules/stat/stat.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/modules/stat/stat.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card text-white bg-{{bgClass}}\">\r\n    <div class=\"card-header\">\r\n        <div class=\"row\">\r\n            <div class=\"col col-xs-3\">\r\n                <i class=\"fa {{icon}} fa-5x\"></i>\r\n            </div>\r\n            <div class=\"col col-xs-9 text-right\">\r\n                <div class=\"d-block huge\">{{count}}</div>\r\n                <div class=\"d-block\">{{label}}</div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"card-footer\">\r\n        <span class=\"float-left\">View Details {{data}}</span>\r\n        <a href=\"javascript:void(0)\" class=\"float-right card-inverse\">\r\n            <span ><i class=\"fa fa-arrow-circle-right\"></i></span>\r\n        </a>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../merchant-portal/order-histories/order-histories.component": [
		"./src/app/layout/merchant-portal/order-histories/order-histories.component.ts",
		"default~merchant-portal-merchant-portal-module~merchant-portal-order-histories-order-histories-compo~0cfe36a4"
	],
	"../modules/admin-all-balance/admin-all-balance.module": [
		"./src/app/layout/modules/admin-all-balance/admin-all-balance.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-all-balance-admin-all-balance-module~mo~e0a0e125",
		"modules-admin-all-balance-admin-all-balance-module"
	],
	"../modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary.module": [
		"./src/app/layout/modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-amount-order-history-order-amount-histo~c36c7d64",
		"modules-admin-amount-order-history-order-amount-history-summary-orderamounthistorysummary-module"
	],
	"../modules/admin-bast/admin-bast.module": [
		"./src/app/layout/modules/admin-bast/admin-bast.module.ts",
		"modules-admin-bast-admin-bast-module"
	],
	"../modules/admin-cnr/cnr.module": [
		"./src/app/layout/modules/admin-cnr/cnr.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-amount-order-history-order-amount-histo~c36c7d64",
		"modules-admin-cnr-cnr-module"
	],
	"../modules/admin-crr/crr.module": [
		"./src/app/layout/modules/admin-crr/crr.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-amount-order-history-order-amount-histo~c36c7d64",
		"modules-admin-crr-crr-module"
	],
	"../modules/admin-escrow-by-merchant/admin-escrow-by-merchant.module": [
		"./src/app/layout/modules/admin-escrow-by-merchant/admin-escrow-by-merchant.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-all-balance-admin-all-balance-module~mo~e0a0e125",
		"modules-admin-escrow-by-merchant-admin-escrow-by-merchant-module"
	],
	"../modules/admin-escrow-report/admin-escrow-report.module": [
		"./src/app/layout/modules/admin-escrow-report/admin-escrow-report.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-all-balance-admin-all-balance-module~mo~e0a0e125",
		"modules-admin-escrow-report-admin-escrow-report-module"
	],
	"../modules/admin-group/admin-group.module": [
		"./src/app/layout/modules/admin-group/admin-group.module.ts",
		"default~modules-admin-admin-module~modules-admin-group-admin-group-module",
		"modules-admin-group-admin-group-module"
	],
	"../modules/admin-merchant-sales-order/admin-merchant-sales-order.module": [
		"./src/app/layout/modules/admin-merchant-sales-order/admin-merchant-sales-order.module.ts",
		"modules-admin-merchant-sales-order-admin-merchant-sales-order-module"
	],
	"../modules/admin-payment/admin-payment.module": [
		"./src/app/layout/modules/admin-payment/admin-payment.module.ts",
		"modules-admin-payment-admin-payment-module"
	],
	"../modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.module": [
		"./src/app/layout/modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-amount-order-history-order-amount-histo~c36c7d64",
		"default~merchant-portal-merchant-portal-module~modules-admin-portal-admin-order-history-order-histor~8feb55e7"
	],
	"../modules/admin-portal/form/form.module": [
		"./src/app/layout/modules/admin-portal/form/form.module.ts",
		"modules-admin-portal-form-form-module"
	],
	"../modules/admin-promo-statistic/admin-promo-statistic.module": [
		"./src/app/layout/modules/admin-promo-statistic/admin-promo-statistic.module.ts",
		"default~modules-admin-promo-statistic-admin-promo-statistic-module~modules-admin-promo-usage-admin-p~114e8250",
		"modules-admin-promo-statistic-admin-promo-statistic-module"
	],
	"../modules/admin-promo-usage/admin-promo-usage.module": [
		"./src/app/layout/modules/admin-promo-usage/admin-promo-usage.module.ts",
		"default~modules-admin-promo-statistic-admin-promo-statistic-module~modules-admin-promo-usage-admin-p~114e8250",
		"modules-admin-promo-usage-admin-promo-usage-module"
	],
	"../modules/admin-report/activity-report/activity-report-summary.module": [
		"./src/app/layout/modules/admin-report/activity-report/activity-report-summary.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-amount-order-history-order-amount-histo~c36c7d64",
		"common",
		"modules-admin-report-activity-report-activity-report-summary-module"
	],
	"../modules/admin-report/cartabandonment/cartabandonmentsummary.module": [
		"./src/app/layout/modules/admin-report/cartabandonment/cartabandonmentsummary.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-amount-order-history-order-amount-histo~c36c7d64",
		"common",
		"modules-admin-report-cartabandonment-cartabandonmentsummary-module"
	],
	"../modules/admin-report/member-demography-report/member-demography-report.module": [
		"./src/app/layout/modules/admin-report/member-demography-report/member-demography-report.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-amount-order-history-order-amount-histo~c36c7d64",
		"common",
		"modules-admin-report-member-demography-report-member-demography-report-module"
	],
	"../modules/admin-report/product-activity-report/product-activity-report-summary.module": [
		"./src/app/layout/modules/admin-report/product-activity-report/product-activity-report-summary.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"default~merchant-portal-merchant-portal-module~modules-admin-amount-order-history-order-amount-histo~c36c7d64",
		"common",
		"modules-admin-report-product-activity-report-product-activity-report-summary-module"
	],
	"../modules/admin-salesorder/admin-salesorder.module": [
		"./src/app/layout/modules/admin-salesorder/admin-salesorder.module.ts",
		"modules-admin-salesorder-admin-salesorder-module"
	],
	"../modules/admin-shipping/admin-shipping.module": [
		"./src/app/layout/modules/admin-shipping/admin-shipping.module.ts",
		"modules-admin-shipping-admin-shipping-module"
	],
	"../modules/admin-stock/admin-stock.module": [
		"./src/app/layout/modules/admin-stock/admin-stock.module.ts",
		"modules-admin-stock-admin-stock-module"
	],
	"../modules/admin/admin.module": [
		"./src/app/layout/modules/admin/admin.module.ts",
		"default~modules-admin-admin-module~modules-admin-group-admin-group-module",
		"modules-admin-admin-module"
	],
	"../modules/admin_members/allmember/allmember.module": [
		"./src/app/layout/modules/admin_members/allmember/allmember.module.ts"
	],
	"../modules/admin_members/member-summary/membersummary.module": [
		"./src/app/layout/modules/admin_members/member-summary/membersummary.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-amount-order-history-order-amount-histo~c36c7d64",
		"modules-admin_members-member-summary-membersummary-module"
	],
	"../modules/article/article.module": [
		"./src/app/layout/modules/article/article.module.ts",
		"modules-article-article-module"
	],
	"../modules/banner/banner.module": [
		"./src/app/layout/modules/banner/banner.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-banner-banner-module~mod~a01ff4b1",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-merchant-merchan~731400bb",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-evouchersalesrep~4891dc5f",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-product-evoucher~39635b8a",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~register-register-module",
		"common",
		"modules-banner-banner-module"
	],
	"../modules/bs-component/bs-component.module": [
		"./src/app/layout/modules/bs-component/bs-component.module.ts"
	],
	"../modules/bs-element/bs-element.module": [
		"./src/app/layout/modules/bs-element/bs-element.module.ts",
		"modules-bs-element-bs-element-module"
	],
	"../modules/category/category.module": [
		"./src/app/layout/modules/category/category.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-category-category-module~modules-product-evou~6b1c0ead",
		"modules-category-category-module"
	],
	"../modules/code-page/code-page.module": [
		"./src/app/layout/modules/code-page/code-page.module.ts",
		"default~modules-code-page-code-page-module~modules-packagebundle-packagebundle-module~modules-page-b~0c19901b",
		"default~modules-admin-promo-statistic-admin-promo-statistic-module~modules-admin-promo-usage-admin-p~114e8250",
		"modules-code-page-code-page-module"
	],
	"../modules/courier-service-page/courier-service-page.module": [
		"./src/app/layout/modules/courier-service-page/courier-service-page.module.ts",
		"modules-courier-service-page-courier-service-page-module"
	],
	"../modules/dashboard/dashboard.module": [
		"./src/app/layout/modules/dashboard/dashboard.module.ts",
		"modules-dashboard-dashboard-module"
	],
	"../modules/data-member/data-member.module": [
		"./src/app/layout/modules/data-member/data-member.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"modules-data-member-data-member-module"
	],
	"../modules/delivery-process/delivery-process.module": [
		"./src/app/layout/modules/delivery-process/delivery-process.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-delivery-process-deliver~bba343e9",
		"modules-delivery-process-delivery-process-module"
	],
	"../modules/email/email.module": [
		"./src/app/layout/modules/email/email.module.ts",
		"modules-email-email-module"
	],
	"../modules/escrow-transaction/escrow-transaction.module": [
		"./src/app/layout/modules/escrow-transaction/escrow-transaction.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-all-balance-admin-all-balance-module~mo~e0a0e125",
		"modules-escrow-transaction-escrow-transaction-module"
	],
	"../modules/evouchersales/evoucher-sales.module": [
		"./src/app/layout/modules/evouchersales/evoucher-sales.module.ts",
		"default~modules-evouchersales-evoucher-sales-module~modules-ewalletsalesreport-ewallet-sales-report-~3f725935",
		"modules-evouchersales-evoucher-sales-module"
	],
	"../modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.module": [
		"./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-evouchersalesrep~4891dc5f",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~f087e61d",
		"modules-evouchersalesreport-evoucher-sales-report-evoucher-sales-report-module"
	],
	"../modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.module": [
		"./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304",
		"modules-evoucherstockreport-evoucher-stock-report-evoucher-stock-report-module"
	],
	"../modules/ewalletsalesreport/ewallet-sales-report.module": [
		"./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.module.ts",
		"default~modules-evouchersales-evoucher-sales-module~modules-ewalletsalesreport-ewallet-sales-report-~3f725935",
		"modules-ewalletsalesreport-ewallet-sales-report-module"
	],
	"../modules/finance-invoice-report/finance-invoice-report.module": [
		"./src/app/layout/modules/finance-invoice-report/finance-invoice-report.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"modules-finance-invoice-report-finance-invoice-report-module"
	],
	"../modules/form-member/form-member.module": [
		"./src/app/layout/modules/form-member/form-member.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"modules-form-member-form-member-module"
	],
	"../modules/frontpage/frontpage.module": [
		"./src/app/layout/modules/frontpage/frontpage.module.ts",
		"modules-frontpage-frontpage-module"
	],
	"../modules/grid/grid.module": [
		"./src/app/layout/modules/grid/grid.module.ts",
		"modules-grid-grid-module"
	],
	"../modules/guest/guest.module": [
		"./src/app/layout/modules/guest/guest.module.ts",
		"modules-guest-guest-module"
	],
	"../modules/media/media.module": [
		"./src/app/layout/modules/media/media.module.ts",
		"common",
		"modules-media-media-module"
	],
	"../modules/member/member.module": [
		"./src/app/layout/modules/member/member.module.ts",
		"default~modules-member-member-module~modules-point-movement-order-point-movement-order-module~module~22dd5777",
		"modules-member-member-module"
	],
	"../modules/membership/membership.module": [
		"./src/app/layout/modules/membership/membership.module.ts",
		"modules-membership-membership-module"
	],
	"../modules/merchant/merchant.module": [
		"./src/app/layout/modules/merchant/merchant.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-banner-banner-module~mod~a01ff4b1",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-merchant-merchan~731400bb",
		"default~merchant-portal-merchant-portal-module~modules-merchant-merchant-module~modules-product-evou~dbcd8c47",
		"modules-merchant-merchant-module"
	],
	"../modules/notification.broadcast/broadcast.module": [
		"./src/app/layout/modules/notification.broadcast/broadcast.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"default~modules-notification-broadcast-broadcast-module~modules-promo-campaign-promo-campaign-module",
		"modules-notification-broadcast-broadcast-module"
	],
	"../modules/notification.setting/notificationsetting.module": [
		"./src/app/layout/modules/notification.setting/notificationsetting.module.ts",
		"modules-notification-setting-notificationsetting-module"
	],
	"../modules/notification/notification.module": [
		"./src/app/layout/modules/notification/notification.module.ts",
		"modules-notification-notification-module"
	],
	"../modules/notificationgroup/notificationgroup.module": [
		"./src/app/layout/modules/notificationgroup/notificationgroup.module.ts",
		"modules-notificationgroup-notificationgroup-module"
	],
	"../modules/notificationmessage/notificationmessage.module": [
		"./src/app/layout/modules/notificationmessage/notificationmessage.module.ts",
		"modules-notificationmessage-notificationmessage-module"
	],
	"../modules/order-bulk/order-bulk.module": [
		"./src/app/layout/modules/order-bulk/order-bulk.module.ts",
		"modules-order-bulk-order-bulk-module"
	],
	"../modules/orderhistoryallhistory/orderhistoryallhistory.module": [
		"./src/app/layout/modules/orderhistoryallhistory/orderhistoryallhistory.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-delivery-process-deliver~bba343e9",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~da8d91ef",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~efc57dfe",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~d2607944"
	],
	"../modules/orderhistorycancel/orderhistorycancel.module": [
		"./src/app/layout/modules/orderhistorycancel/orderhistorycancel.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-delivery-process-deliver~bba343e9",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~da8d91ef",
		"modules-orderhistorycancel-orderhistorycancel-module"
	],
	"../modules/orderhistorycomplete/orderhistorycomplete.module": [
		"./src/app/layout/modules/orderhistorycomplete/orderhistorycomplete.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-delivery-process-deliver~bba343e9",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~da8d91ef",
		"modules-orderhistorycomplete-orderhistorycomplete-module"
	],
	"../modules/orderhistoryfailed/orderhistoryfailed.module": [
		"./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.module.ts",
		"common",
		"modules-orderhistoryfailed-orderhistoryfailed-module"
	],
	"../modules/orderhistorypending/orderhistorypending.module": [
		"./src/app/layout/modules/orderhistorypending/orderhistorypending.module.ts",
		"common",
		"modules-orderhistorypending-orderhistorypending-module"
	],
	"../modules/orderhistoryreturn/orderhistoryreturn.module": [
		"./src/app/layout/modules/orderhistoryreturn/orderhistoryreturn.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-delivery-process-deliver~bba343e9",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~da8d91ef",
		"modules-orderhistoryreturn-orderhistoryreturn-module"
	],
	"../modules/orderhistorysuccess/orderhistorysuccess.module": [
		"./src/app/layout/modules/orderhistorysuccess/orderhistorysuccess.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-delivery-process-deliver~bba343e9",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~da8d91ef",
		"modules-orderhistorysuccess-orderhistorysuccess-module"
	],
	"../modules/orderhistorysummary/orderhistorysummary.module": [
		"./src/app/layout/modules/orderhistorysummary/orderhistorysummary.module.ts",
		"modules-orderhistorysummary-orderhistorysummary-module"
	],
	"../modules/orderhistorywaiting/orderhistorywaiting.module": [
		"./src/app/layout/modules/orderhistorywaiting/orderhistorywaiting.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-delivery-process-deliver~bba343e9",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~da8d91ef",
		"default~modules-evouchersales-evoucher-sales-module~modules-ewalletsalesreport-ewallet-sales-report-~3f725935",
		"modules-orderhistorywaiting-orderhistorywaiting-module"
	],
	"../modules/outlets/outlets.module": [
		"./src/app/layout/modules/outlets/outlets.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-banner-banner-module~mod~a01ff4b1",
		"modules-outlets-outlets-module"
	],
	"../modules/packagebundle/packagebundle.module": [
		"./src/app/layout/modules/packagebundle/packagebundle.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"default~modules-code-page-code-page-module~modules-packagebundle-packagebundle-module~modules-page-b~0c19901b",
		"modules-packagebundle-packagebundle-module"
	],
	"../modules/packinglist/packinglist.module": [
		"./src/app/layout/modules/packinglist/packinglist.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-delivery-process-deliver~bba343e9",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~da8d91ef",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~efc57dfe",
		"modules-packinglist-packinglist-module"
	],
	"../modules/page-baru/pagebaru.module": [
		"./src/app/layout/modules/page-baru/pagebaru.module.ts",
		"default~modules-code-page-code-page-module~modules-packagebundle-packagebundle-module~modules-page-b~0c19901b",
		"default~merchant-portal-merchant-portal-module~modules-page-baru-pagebaru-module~modules-paymentgate~1252908c",
		"modules-page-baru-pagebaru-module"
	],
	"../modules/paymentgateway/paymentgateway.module": [
		"./src/app/layout/modules/paymentgateway/paymentgateway.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-page-baru-pagebaru-module~modules-paymentgate~1252908c",
		"modules-paymentgateway-paymentgateway-module"
	],
	"../modules/po-bulk-update/po-bulk-update.module": [
		"./src/app/layout/modules/po-bulk-update/po-bulk-update.module.ts",
		"modules-po-bulk-update-po-bulk-update-module"
	],
	"../modules/po-report/po-report.module": [
		"./src/app/layout/modules/po-report/po-report.module.ts",
		"modules-po-report-po-report-module"
	],
	"../modules/point-movement-order/point-movement-order.module": [
		"./src/app/layout/modules/point-movement-order/point-movement-order.module.ts",
		"default~modules-member-member-module~modules-point-movement-order-point-movement-order-module~module~22dd5777",
		"modules-point-movement-order-point-movement-order-module"
	],
	"../modules/point-movement-product/point-movement-product.module": [
		"./src/app/layout/modules/point-movement-product/point-movement-product.module.ts",
		"default~modules-member-member-module~modules-point-movement-order-point-movement-order-module~module~22dd5777",
		"modules-point-movement-product-point-movement-product-module"
	],
	"../modules/points-transaction-approval/points-transaction-approval.module": [
		"./src/app/layout/modules/points-transaction-approval/points-transaction-approval.module.ts",
		"default~modules-member-member-module~modules-point-movement-order-point-movement-order-module~module~22dd5777",
		"modules-points-transaction-approval-points-transaction-approval-module"
	],
	"../modules/points-transaction-report/points-transaction-report.module": [
		"./src/app/layout/modules/points-transaction-report/points-transaction-report.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304",
		"default~modules-member-member-module~modules-point-movement-order-point-movement-order-module~module~22dd5777",
		"modules-points-transaction-report-points-transaction-report-module"
	],
	"../modules/points-transaction/points-transaction.module": [
		"./src/app/layout/modules/points-transaction/points-transaction.module.ts",
		"default~modules-member-member-module~modules-point-movement-order-point-movement-order-module~module~22dd5777",
		"modules-points-transaction-points-transaction-module"
	],
	"../modules/points.campaign/pointscampaign.module": [
		"./src/app/layout/modules/points.campaign/pointscampaign.module.ts",
		"modules-points-campaign-pointscampaign-module"
	],
	"../modules/points.group/points-group.module": [
		"./src/app/layout/modules/points.group/points-group.module.ts",
		"modules-points-group-points-group-module"
	],
	"../modules/points.models/pointsmodels.module": [
		"./src/app/layout/modules/points.models/pointsmodels.module.ts",
		"default~modules-member-member-module~modules-point-movement-order-point-movement-order-module~module~22dd5777",
		"modules-points-models-pointsmodels-module"
	],
	"../modules/product-voucher/voucher.module": [
		"./src/app/layout/modules/product-voucher/voucher.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-banner-banner-module~mod~a01ff4b1",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-merchant-merchan~731400bb",
		"default~merchant-portal-merchant-portal-module~modules-merchant-merchant-module~modules-product-evou~dbcd8c47",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-evouchersalesrep~4891dc5f",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-product-evoucher~39635b8a",
		"default~merchant-portal-merchant-portal-module~modules-category-category-module~modules-product-evou~6b1c0ead",
		"default~merchant-portal-merchant-portal-module~modules-product-evoucher-evoucher-module~modules-prod~65e48bf7",
		"modules-product-voucher-voucher-module"
	],
	"../modules/product.evoucher/evoucher.module": [
		"./src/app/layout/modules/product.evoucher/evoucher.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-banner-banner-module~mod~a01ff4b1",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-merchant-merchan~731400bb",
		"default~merchant-portal-merchant-portal-module~modules-merchant-merchant-module~modules-product-evou~dbcd8c47",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-evouchersalesrep~4891dc5f",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-product-evoucher~39635b8a",
		"default~merchant-portal-merchant-portal-module~modules-category-category-module~modules-product-evou~6b1c0ead",
		"default~merchant-portal-merchant-portal-module~modules-product-evoucher-evoucher-module~modules-prod~65e48bf7",
		"default~merchant-portal-merchant-portal-module~modules-product-evoucher-evoucher-module"
	],
	"../modules/product/product.module": [
		"./src/app/layout/modules/product/product.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-banner-banner-module~mod~a01ff4b1",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-merchant-merchan~731400bb",
		"default~merchant-portal-merchant-portal-module~modules-merchant-merchant-module~modules-product-evou~dbcd8c47",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-evouchersalesrep~4891dc5f",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-product-evoucher~39635b8a",
		"default~merchant-portal-merchant-portal-module~modules-category-category-module~modules-product-evou~6b1c0ead",
		"default~merchant-portal-merchant-portal-module~modules-product-evoucher-evoucher-module~modules-prod~65e48bf7",
		"modules-product-product-module"
	],
	"../modules/promo-campaign/promo-campaign.module": [
		"./src/app/layout/modules/promo-campaign/promo-campaign.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"default~modules-admin-promo-statistic-admin-promo-statistic-module~modules-admin-promo-usage-admin-p~114e8250",
		"default~modules-notification-broadcast-broadcast-module~modules-promo-campaign-promo-campaign-module",
		"modules-promo-campaign-promo-campaign-module"
	],
	"../modules/promodeals/promodeals.module": [
		"./src/app/layout/modules/promodeals/promodeals.module.ts",
		"default~modules-code-page-code-page-module~modules-packagebundle-packagebundle-module~modules-page-b~0c19901b",
		"modules-promodeals-promodeals-module"
	],
	"../modules/promotion/promotion.module": [
		"./src/app/layout/modules/promotion/promotion.module.ts",
		"default~modules-code-page-code-page-module~modules-packagebundle-packagebundle-module~modules-page-b~0c19901b",
		"default~modules-admin-promo-statistic-admin-promo-statistic-module~modules-admin-promo-usage-admin-p~114e8250",
		"modules-promotion-promotion-module"
	],
	"../modules/report/report.module": [
		"./src/app/layout/modules/report/report.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-report-report-module",
		"modules-report-report-module"
	],
	"../modules/reporterror/reporterror.module": [
		"./src/app/layout/modules/reporterror/reporterror.module.ts",
		"modules-reporterror-reporterror-module"
	],
	"../modules/salesorder/salesorder.module": [
		"./src/app/layout/modules/salesorder/salesorder.module.ts",
		"modules-salesorder-salesorder-module"
	],
	"../modules/shipping-bulk-process/shipping-bulk-process.module": [
		"./src/app/layout/modules/shipping-bulk-process/shipping-bulk-process.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304",
		"modules-shipping-bulk-process-shipping-bulk-process-module"
	],
	"../modules/shoppingcart/shoppingcart.module": [
		"./src/app/layout/modules/shoppingcart/shoppingcart.module.ts",
		"modules-shoppingcart-shoppingcart-module"
	],
	"../modules/versioning/versioning.module": [
		"./src/app/layout/modules/versioning/versioning.module.ts",
		"modules-versioning-versioning-module"
	],
	"./access-denied/access-denied.module": [
		"./src/app/access-denied/access-denied.module.ts",
		"access-denied-access-denied-module"
	],
	"./admin-management/admin-management.module": [
		"./src/app/layout/admin-management/admin-management.module.ts",
		"default~admin-management-admin-management-module~administrator-administrator-module~app-login-app-lo~7f3d8a02",
		"admin-management-admin-management-module"
	],
	"./administrator/administrator.module": [
		"./src/app/layout/administrator/administrator.module.ts",
		"default~admin-management-admin-management-module~administrator-administrator-module~app-login-app-lo~7f3d8a02",
		"administrator-administrator-module"
	],
	"./app-login/app-login.module": [
		"./src/app/app-login/app-login.module.ts",
		"default~admin-management-admin-management-module~administrator-administrator-module~app-login-app-lo~7f3d8a02",
		"common",
		"app-login-app-login-module"
	],
	"./devmode/devmode.module": [
		"./src/app/devmode/devmode.module.ts",
		"devmode-devmode-module"
	],
	"./layout/layout.module": [
		"./src/app/layout/layout.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-delivery-process-deliver~bba343e9",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~da8d91ef",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-banner-banner-module~mod~a01ff4b1",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~efc57dfe",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~d2607944",
		"common",
		"layout-layout-module"
	],
	"./login-merchant/login-merchant.module": [
		"./src/app/login-merchant/login-merchant.module.ts",
		"default~login-merchant-login-merchant-module~merchant-portal-merchant-portal-module~register-registe~01607f54",
		"common",
		"login-merchant-login-merchant-module"
	],
	"./login/login.module": [
		"./src/app/login/login.module.ts",
		"common",
		"login-login-module"
	],
	"./merchant-portal/merchant-portal.module": [
		"./src/app/layout/merchant-portal/merchant-portal.module.ts",
		"default~merchant-portal-merchant-portal-module~modules-admin-report-product-activity-report-product-~2089eef6",
		"default~merchant-portal-merchant-portal-module~modules-admin-amount-order-history-order-amount-histo~c36c7d64",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-delivery-process-deliver~bba343e9",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~da8d91ef",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-banner-banner-module~mod~a01ff4b1",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-merchant-merchan~731400bb",
		"default~merchant-portal-merchant-portal-module~modules-merchant-merchant-module~modules-product-evou~dbcd8c47",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-evouchersalesrep~4891dc5f",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-product-evoucher~39635b8a",
		"default~merchant-portal-merchant-portal-module~modules-admin-all-balance-admin-all-balance-module~mo~e0a0e125",
		"default~merchant-portal-merchant-portal-module~modules-category-category-module~modules-product-evou~6b1c0ead",
		"default~merchant-portal-merchant-portal-module~modules-product-evoucher-evoucher-module~modules-prod~65e48bf7",
		"default~login-merchant-login-merchant-module~merchant-portal-merchant-portal-module~register-registe~01607f54",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~efc57dfe",
		"default~merchant-portal-merchant-portal-module~modules-page-baru-pagebaru-module~modules-paymentgate~1252908c",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~register-register-module",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~d2607944",
		"default~merchant-portal-merchant-portal-module~register-register-module",
		"default~merchant-portal-merchant-portal-module~merchant-portal-order-histories-order-histories-compo~0cfe36a4",
		"default~merchant-portal-merchant-portal-module~modules-report-report-module",
		"default~merchant-portal-merchant-portal-module~modules-product-evoucher-evoucher-module",
		"default~merchant-portal-merchant-portal-module~modules-admin-portal-admin-order-history-order-histor~8feb55e7",
		"default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~f087e61d",
		"common",
		"merchant-portal-merchant-portal-module"
	],
	"./not-found/not-found.module": [
		"./src/app/not-found/not-found.module.ts",
		"not-found-not-found-module"
	],
	"./register/register.module": [
		"./src/app/register/register.module.ts",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-orderhistoryallhistory-o~da8d91ef",
		"default~layout-layout-module~merchant-portal-merchant-portal-module~modules-banner-banner-module~mod~a01ff4b1",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-merchant-merchan~731400bb",
		"default~merchant-portal-merchant-portal-module~modules-merchant-merchant-module~modules-product-evou~dbcd8c47",
		"default~login-merchant-login-merchant-module~merchant-portal-merchant-portal-module~register-registe~01607f54",
		"default~merchant-portal-merchant-portal-module~modules-page-baru-pagebaru-module~modules-paymentgate~1252908c",
		"default~merchant-portal-merchant-portal-module~modules-banner-banner-module~register-register-module",
		"default~merchant-portal-merchant-portal-module~register-register-module",
		"common",
		"register-register-module"
	],
	"./reset-password/reset-password.module": [
		"./src/app/reset-password/reset-password.module.ts",
		"default~login-merchant-login-merchant-module~merchant-portal-merchant-portal-module~register-registe~01607f54",
		"reset-password-reset-password-module"
	],
	"./server-error/server-error.module": [
		"./src/app/server-error/server-error.module.ts",
		"server-error-server-error-module"
	],
	"./signup/signup.module": [
		"./src/app/signup/signup.module.ts",
		"common",
		"signup-signup-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared */ "./src/app/shared/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', loadChildren: './layout/layout.module#LayoutModule', canActivate: [_shared__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
    { path: 'login', loadChildren: './login/login.module#LoginModule' },
    { path: 'app-login', loadChildren: './app-login/app-login.module#AppLoginModule' },
    { path: 'reset-password', loadChildren: './reset-password/reset-password.module#ResetPasswordModule' },
    { path: 'login-merchant', loadChildren: './login-merchant/login-merchant.module#LoginMerchantModule' },
    { path: 'register', loadChildren: './register/register.module#RegisterModule' },
    { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
    { path: 'devmode', loadChildren: './devmode/devmode.module#DevmodeModule' },
    { path: 'error', loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    // { path: 'merchant-portal',  loadChildren: './layout/merchant-portal/merchant-portal.module#MerchantPortalModule',  data: { preload: true }, canActivate: [AuthGuard] },
    { path: '**', redirectTo: 'not-found' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\r\n    font-family: 'League Spartan';\r\n    src: url('leaguespartan-bold.eot');\r\n    src: url('leaguespartan-bold.eot?#iefix') format('embedded-opentype'),\r\n         url('leaguespartan-bold.woff2') format('woff2'),\r\n         url('leaguespartan-bold.woff') format('woff'),\r\n         url('leaguespartan-bold.ttf') format('truetype'),\r\n         url('leaguespartan-bold.svg#league_spartanbold') format('svg');\r\n    font-weight: bold;\r\n    font-style: normal;\r\n\r\n}\n@media print {\n  .dev-mode {\n    display: none;\n  }\n}\n.development-mode .dev-mode {\n  background-color: darkcyan;\n  width: 100%;\n  top: 0px;\n  position: fixed;\n  text-align: center;\n  color: white;\n  z-index: 99;\n}\n.development-mode div.dev-mode h5 {\n  font-size: 12px;\n  font-weight: bold;\n  height: 23px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  vertical-align: middle;\n  margin-bottom: 0px;\n}\n.production .dev-mode {\n  display: none;\n}\n.multiselect-dropdown .dropdown-btn {\n  background-color: white;\n}\n.dev-mode-merchant-portal {\n  margin-top: 25px;\n}\n.dev-mode-merchant-portal #merchant-portal {\n  position: absolute;\n  z-index: 1;\n  width: 100%;\n  height: calc(100vh - 25px);\n  left: 0px;\n  top: 25px;\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hc3NldHMvZm9udHMvbGVhZ3Vlc3BhcnRhbi9zdHlsZXNoZWV0LmNzcyIsInNyYy9hcHAvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcYXBwLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSw2QkFBNkI7SUFDN0Isa0NBQWtDO0lBQ2xDOzs7O3VFQUltRTtJQUNuRSxpQkFBaUI7SUFDakIsa0JBQWtCOztBQUV0QjtBQ1RBO0VBQ0k7SUFDSSxhQUFBO0VDQU47QUFDRjtBRElJO0VBQ0ksMEJBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDRlI7QURNSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUNKUjtBRFVJO0VBQ0ksYUFBQTtBQ1BSO0FEV0E7RUFDSSx1QkFBQTtBQ1JKO0FEV0E7RUFDSSxnQkFBQTtBQ1JKO0FEVUk7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtBQ1JSIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGZvbnQtZmFjZSB7XHJcbiAgICBmb250LWZhbWlseTogJ0xlYWd1ZSBTcGFydGFuJztcclxuICAgIHNyYzogdXJsKCdsZWFndWVzcGFydGFuLWJvbGQuZW90Jyk7XHJcbiAgICBzcmM6IHVybCgnbGVhZ3Vlc3BhcnRhbi1ib2xkLmVvdD8jaWVmaXgnKSBmb3JtYXQoJ2VtYmVkZGVkLW9wZW50eXBlJyksXHJcbiAgICAgICAgIHVybCgnbGVhZ3Vlc3BhcnRhbi1ib2xkLndvZmYyJykgZm9ybWF0KCd3b2ZmMicpLFxyXG4gICAgICAgICB1cmwoJ2xlYWd1ZXNwYXJ0YW4tYm9sZC53b2ZmJykgZm9ybWF0KCd3b2ZmJyksXHJcbiAgICAgICAgIHVybCgnbGVhZ3Vlc3BhcnRhbi1ib2xkLnR0ZicpIGZvcm1hdCgndHJ1ZXR5cGUnKSxcclxuICAgICAgICAgdXJsKCdsZWFndWVzcGFydGFuLWJvbGQuc3ZnI2xlYWd1ZV9zcGFydGFuYm9sZCcpIGZvcm1hdCgnc3ZnJyk7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuXHJcbn0iLCJAaW1wb3J0IHVybCgnLi8uLi9hc3NldHMvZm9udHMvbGVhZ3Vlc3BhcnRhbi9zdHlsZXNoZWV0LmNzcycpO1xyXG5cclxuQG1lZGlhIHByaW50e1xyXG4gICAgLmRldi1tb2Rle1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5kZXZlbG9wbWVudC1tb2RlIHtcclxuICAgIC5kZXYtbW9kZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogZGFya2N5YW47XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgdG9wOiAwcHg7XHJcbiAgICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgei1pbmRleDogOTk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGRpdi5kZXYtbW9kZSBoNSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGhlaWdodDogMjNweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG4ucHJvZHVjdGlvbiB7XHJcbiAgICAuZGV2LW1vZGUge1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5tdWx0aXNlbGVjdC1kcm9wZG93biAuZHJvcGRvd24tYnRuIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uZGV2LW1vZGUtbWVyY2hhbnQtcG9ydGFsIHtcclxuICAgIG1hcmdpbi10b3A6IDI1cHg7XHJcblxyXG4gICAgI21lcmNoYW50LXBvcnRhbCB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiBjYWxjKDEwMHZoIC0gMjVweCk7XHJcbiAgICAgICAgbGVmdDogMHB4O1xyXG4gICAgICAgIHRvcDogMjVweDtcclxuICAgIH1cclxufVxyXG4iLCJAaW1wb3J0IHVybChcIi4vLi4vYXNzZXRzL2ZvbnRzL2xlYWd1ZXNwYXJ0YW4vc3R5bGVzaGVldC5jc3NcIik7XG5AbWVkaWEgcHJpbnQge1xuICAuZGV2LW1vZGUge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbn1cbi5kZXZlbG9wbWVudC1tb2RlIC5kZXYtbW9kZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IGRhcmtjeWFuO1xuICB3aWR0aDogMTAwJTtcbiAgdG9wOiAwcHg7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHotaW5kZXg6IDk5O1xufVxuLmRldmVsb3BtZW50LW1vZGUgZGl2LmRldi1tb2RlIGg1IHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgaGVpZ2h0OiAyM3B4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuXG4ucHJvZHVjdGlvbiAuZGV2LW1vZGUge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ubXVsdGlzZWxlY3QtZHJvcGRvd24gLmRyb3Bkb3duLWJ0biB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4uZGV2LW1vZGUtbWVyY2hhbnQtcG9ydGFsIHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbn1cbi5kZXYtbW9kZS1tZXJjaGFudC1wb3J0YWwgI21lcmNoYW50LXBvcnRhbCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogMTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogY2FsYygxMDB2aCAtIDI1cHgpO1xuICBsZWZ0OiAwcHg7XG4gIHRvcDogMjVweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.developmentMode = "development-mode";
    }
    AppComponent.prototype.ngOnInit = function () {
        // console.log("ngoninit", localStorage.getItem('devmode'));
    };
    AppComponent.prototype.ngDoCheck = function () {
        // console.log("ITS CHANGED");  
        if (localStorage.getItem('devmode') == 'active') {
            this.developmentMode = 'development-mode';
        }
        else {
            this.developmentMode = 'production';
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class'),
        __metadata("design:type", Object)
    ], AppComponent.prototype, "developmentMode", void 0);
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            host: { 'class': '' },
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: createTranslateLoader, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createTranslateLoader", function() { return createTranslateLoader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/esm5/ngx-translate-http-loader.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared */ "./src/app/shared/index.ts");
/* harmony import */ var _component_libs_form_detail_form_detail_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./component-libs/form-detail/form-detail.component */ "./src/app/component-libs/form-detail/form-detail.component.ts");
/* harmony import */ var _layout_modules_notificationmessage_detail_notificationgroup_detail_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./layout/modules/notificationmessage/detail/notificationgroup.detail.component */ "./src/app/layout/modules/notificationmessage/detail/notificationgroup.detail.component.ts");
/* harmony import */ var _layout_modules_notificationmessage_edit_notificationgroup_edit_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./layout/modules/notificationmessage/edit/notificationgroup.edit.component */ "./src/app/layout/modules/notificationmessage/edit/notificationgroup.edit.component.ts");
/* harmony import */ var _layout_modules_admin_members_allmember_allmember_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./layout/modules/admin_members/allmember/allmember.module */ "./src/app/layout/modules/admin_members/allmember/allmember.module.ts");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var ngx_webstorage_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-webstorage-service */ "./node_modules/ngx-webstorage-service/fesm5/ngx-webstorage-service.js");
/* harmony import */ var _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./services/storage-service-module/storage-service-module.service */ "./src/app/services/storage-service-module/storage-service-module.service.ts");
/* harmony import */ var ngx_toggle_switch__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ngx-toggle-switch */ "./node_modules/ngx-toggle-switch/ui-switch.es5.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/fesm2015/agm-core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! angular-ng-autocomplete */ "./node_modules/angular-ng-autocomplete/fesm5/angular-ng-autocomplete.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _layout_modules_admin_stock_module__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./layout/modules/admin-stock.module */ "./src/app/layout/modules/admin-stock.module.ts");
/* harmony import */ var _services_program_name_service__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./services/program-name.service */ "./src/app/services/program-name.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























// import { Member } from './layout/modules/member/detail/member.detail.module';
// AoT requires an exported function for factories
var createTranslateLoader = function (http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_6__["TranslateHttpLoader"](http, './assets/i18n/', '.json');
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                // Ng2CompleterModule,
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_22__["MatMenuModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_20__["NgbModule"],
                ngx_toggle_switch__WEBPACK_IMPORTED_MODULE_18__["UiSwitchModule"],
                ngx_webstorage_service__WEBPACK_IMPORTED_MODULE_16__["StorageServiceModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
                angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_21__["AutocompleteLibModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateLoader"],
                        useFactory: createTranslateLoader,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]]
                    }
                }),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"],
                _layout_modules_admin_members_allmember_allmember_module__WEBPACK_IMPORTED_MODULE_14__["AllMemberModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_15__["FormBuilderTableModule"],
                _layout_modules_admin_stock_module__WEBPACK_IMPORTED_MODULE_24__["AdminStockModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_19__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyChHtMJJ6RE3Bg-SFeTKQvOsks7MzGQ5VA',
                    libraries: ["places"]
                })
                // Member.DetailModule
            ],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _component_libs_form_detail_form_detail_component__WEBPACK_IMPORTED_MODULE_11__["FormDetailComponent"],
                // ProductDetailComponent,
                // AllMemberComponent, 
                // AllMemberDetailComponent,
                // MemberPointHistoryComponent,
                _layout_modules_notificationmessage_detail_notificationgroup_detail_component__WEBPACK_IMPORTED_MODULE_12__["NotificationGroupDetailComponent"],
                _layout_modules_notificationmessage_edit_notificationgroup_edit_component__WEBPACK_IMPORTED_MODULE_13__["NotificationGroupEditComponent"],
            ],
            providers: [_shared__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"], _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_17__["LocalStorageService"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HTTP_INTERCEPTORS"],
                    useClass: _shared__WEBPACK_IMPORTED_MODULE_10__["AuthInterceptor"],
                    multi: true
                },
                _services_program_name_service__WEBPACK_IMPORTED_MODULE_25__["ProgramNameService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/component-libs/form-builder-table/form-builder-table.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/component-libs/form-builder-table/form-builder-table.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".undisplayed {\n  display: none;\n}\n\n.image-autoform {\n  width: 200px;\n  height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.date-picker-wrapper {\n  position: relative;\n}\n\n.date-picker-wrapper .date-picker-overlay {\n  position: absolute;\n  z-index: 99;\n  right: 0;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker {\n  background: white;\n  overflow: hidden;\n  border: 1px solid #dfdfdf;\n  position: absolute;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker .btn.ok {\n  clear: both;\n  background-color: #3498db;\n  color: white;\n  margin: 0px 5px 10px;\n  border-color: transparent;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker .btn.clear {\n  clear: both;\n  background-color: #ac0a0a;\n  color: white;\n  margin: 0px 10px 10px;\n  border-color: transparent;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker ngb-datepicker {\n  float: left;\n  clear: both;\n  margin: 10px;\n  right: 0%;\n}\n\n.layout-card {\n  border-radius: 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  margin-top: 30px;\n}\n\nselect {\n  border: 0px solid #ddd;\n  background: transparent;\n  outline: none;\n}\n\n.btn {\n  border: 1px solid black;\n  display: inline-block;\n  margin-right: 15px;\n  margin-top: 5px;\n  margin-left: 0px;\n  background-color: #2381fb;\n  transition: ease 0.3s all 0s;\n  color: white !important;\n}\n\n.btn .fa {\n  margin-right: 5px;\n}\n\n.btn-approve {\n  background-color: #28a745 !important;\n}\n\n.btn-remove {\n  background-color: #e74c3c;\n  border-color: #db3700;\n}\n\n.btn-remove:hover {\n  background-color: #db3700 !important;\n  border-color: #db3700 !important;\n}\n\n.btn:hover {\n  background-color: #3d91ff;\n  color: white !important;\n}\n\n.custom-day {\n  text-align: center;\n  padding: 0.185rem 0.25rem;\n  display: inline-block;\n  height: 2rem;\n  width: 2rem;\n}\n\niframe {\n  visibility: hidden;\n  height: 0px;\n  width: 0px;\n  position: absolute;\n  border-width: 0px;\n}\n\n.custom-day.focused {\n  background-color: #e6e6e6;\n}\n\n.custom-day.range,\n.custom-day:hover {\n  background-color: #0275d8;\n  border-radius: 30px;\n  color: white;\n}\n\n.custom-day.faded {\n  background-color: rgba(2, 117, 216, 0.5);\n}\n\nli.page-item {\n  font-size: 18px;\n  color: #869abb;\n  background-color: white;\n}\n\n.end {\n  border-top-right-radius: 5px;\n  border-bottom-right-radius: 5px;\n}\n\n.start {\n  border-top-left-radius: 5px;\n  border-bottom-left-radius: 5px;\n}\n\n.pagination-head {\n  width: -webkit-fill-available;\n}\n\n.pagination-head .pagination {\n  border-radius: 10px;\n  background-color: white;\n  float: right;\n}\n\n.pagination-head .show-entries {\n  color: #869abb;\n}\n\n.pagination-head .show-entries .order-btn {\n  margin-left: 15px;\n  background-color: #394458;\n  color: #869abb;\n}\n\na.current-page {\n  font-weight: bold;\n  border-radius: 10px;\n  color: white !important;\n  background-color: #ccc !important;\n  margin: 5px 5px;\n}\n\na.current-page:hover {\n  color: #869abb !important;\n}\n\na.dotter {\n  cursor: unset;\n}\n\na.dotter:hover {\n  background-color: unset;\n}\n\na.page-link {\n  border-collapse: collapse;\n  border: none;\n  padding: 8px 12px;\n  font-size: 12px;\n  color: black;\n  background-color: white;\n  margin-top: 5px;\n}\n\na.page-link:hover {\n  color: #869abb;\n}\n\na.page-link.next {\n  color: #869abb;\n  font-weight: bold;\n  font-size: 12px;\n}\n\na {\n  cursor: pointer;\n}\n\n.fbt .form-control {\n  font-size: 12px;\n  border-radius: 15px;\n}\n\n.fbt .header-custom {\n  flex-direction: row;\n  display: flex;\n  width: 100%;\n}\n\n.fbt .header-custom .label-custom {\n  height: 37.6px;\n  padding: 6px;\n  display: flex;\n  font-size: 25px;\n  text-align: left;\n  margin-bottom: 1cm;\n  width: 100%;\n  color: white;\n}\n\n@media (max-width: 500px) {\n  .fbt .header-custom .label-custom {\n    font-size: 22px;\n  }\n}\n\n.fbt .header-custom .btn:hover {\n  border-color: #3498db;\n}\n\n.fbt .header-custom .btn.toggle-true {\n  color: #3498db;\n  border-color: #3498db;\n}\n\n.fbt .header-custom .btn.right {\n  float: right;\n  margin-right: 0px;\n  margin-left: 15px;\n  border-radius: 8px;\n}\n\n@media (max-width: 500px) {\n  .fbt .header-custom .btn.right {\n    float: left;\n    margin-right: 0px;\n    margin-left: 0px;\n  }\n}\n\n.fbt .header-custom .head {\n  cursor: pointer;\n}\n\n.fbt .header-custom .cb-coloumn {\n  padding: 5px 5px;\n  float: left;\n  height: 29px;\n  margin-right: 15px;\n  border-radius: 20px;\n}\n\n.fbt .dropdown-header {\n  justify-content: flex-end;\n  align-items: flex-end;\n  padding: 0px;\n  width: 100%;\n  margin-bottom: 10px;\n}\n\n.fbt ::ng-deep .mat-checkbox .cdk-visually-hidden {\n  visibility: hidden;\n}\n\n.fbt ::ng-deep .mat-checkbox .mat-checkbox-frame {\n  border-color: #ccc;\n  border-width: 1px;\n}\n\n.fbt ::ng-deep .mat-checkbox .mat-checkbox-checkmark-path {\n  stroke: black !important;\n}\n\n.fbt .custom-table-border {\n  border-bottom: none !important;\n}\n\n.fbt ::ng-deep .mat-checkbox-checked.mat-accent .mat-checkbox-ripple .mat-ripple-element {\n  opacity: 0.03 !important;\n  background-color: #56a4ff !important;\n}\n\n.fbt ::ng-deep .mat-checkbox-checked.mat-accent .mat-checkbox-background,\n.fbt .mat-checkbox-indeterminate.mat-accent .mat-checkbox-background {\n  background-color: #56a4ff;\n}\n\n.fbt .card-body {\n  margin: 0;\n  padding: 0px;\n  min-height: 450px;\n}\n\n.fbt .card-body thead th {\n  color: #555;\n  text-align: center;\n  vertical-align: top !important;\n  background-color: #f2f2f2;\n  opacity: 1;\n  padding: 10px;\n  text-transform: capitalize;\n}\n\n.fbt .card-body thead th .search-string {\n  position: relative;\n}\n\n.fbt .card-body thead th .search-string .search-field {\n  position: absolute;\n  right: 7px;\n  top: 7px;\n  opacity: 0.3;\n}\n\n.fbt .card-body tbody tr:hover td {\n  background: #3b9fe2;\n  cursor: pointer;\n}\n\n.fbt .card-body tbody tr.image:hover td {\n  background: transparent;\n  cursor: default;\n  color: unset;\n}\n\n.fbt .card-body tbody tr td {\n  font-size: 12px;\n  vertical-align: top !important;\n  text-align: center;\n  padding: 12px;\n}\n\n.fbt .card-body tbody .pending {\n  border-radius: 30px;\n  background-color: #ffe6d5;\n  color: #ee680c;\n  width: 80px;\n  height: 35px;\n  font-size: 12px;\n  border: none;\n}\n\n.fbt .card-body tbody .success {\n  background-color: #daf9f1;\n  color: #2ea56e;\n  border-radius: 30px;\n  width: 80px;\n  height: 35px;\n  font-size: 12px;\n  border: none;\n}\n\n.fbt .card-body tbody .hold {\n  background-color: #f2e8ff;\n  color: #8b5acf;\n  border-radius: 30px;\n  width: 80px;\n  height: 35px;\n  font-size: 12px;\n  border: none;\n}\n\n.fbt .card-body tbody .refunded {\n  border-radius: 30px;\n  background-color: #e4ecff;\n  color: #4271df;\n  width: 80px;\n  height: 35px;\n  font-size: 12px;\n  border: none;\n}\n\n.fbt .card-body tbody .rejected {\n  border-radius: 30px;\n  background-color: #fcecf0;\n  color: #ec3b60;\n  width: 80px;\n  height: 35px;\n  font-size: 12px;\n  border: none;\n}\n\n.fbt .card-body tbody .img_dt_prp {\n  width: 120px;\n  float: left;\n  padding: 5px;\n  height: 120px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.fbt .card-body tbody .img_dt_prp .image_wrapper {\n  height: 120px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-bottom: 5px;\n  overflow: hidden;\n}\n\n.fbt .card-body tbody .img_dt_prp img {\n  max-width: 100%;\n}\n\n.fbt .sk-fading-circle {\n  margin: 10px auto;\n  width: 40px;\n  height: 40px;\n  position: relative;\n}\n\n.fbt .sk-fading-circle .sk-circle {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  left: 0;\n  top: 0;\n}\n\n.fbt .sk-fading-circle .sk-circle:before {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 15%;\n  height: 15%;\n  background-color: #333;\n  border-radius: 100%;\n  -webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;\n  animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;\n}\n\n.fbt .sk-fading-circle .sk-circle2 {\n  -webkit-transform: rotate(30deg);\n  transform: rotate(30deg);\n}\n\n.fbt .sk-fading-circle .sk-circle3 {\n  -webkit-transform: rotate(60deg);\n  transform: rotate(60deg);\n}\n\n.fbt .sk-fading-circle .sk-circle4 {\n  -webkit-transform: rotate(90deg);\n  transform: rotate(90deg);\n}\n\n.fbt .sk-fading-circle .sk-circle5 {\n  -webkit-transform: rotate(120deg);\n  transform: rotate(120deg);\n}\n\n.fbt .sk-fading-circle .sk-circle6 {\n  -webkit-transform: rotate(150deg);\n  transform: rotate(150deg);\n}\n\n.fbt .sk-fading-circle .sk-circle7 {\n  -webkit-transform: rotate(180deg);\n  transform: rotate(180deg);\n}\n\n.fbt .sk-fading-circle .sk-circle8 {\n  -webkit-transform: rotate(210deg);\n  transform: rotate(210deg);\n}\n\n.fbt .sk-fading-circle .sk-circle9 {\n  -webkit-transform: rotate(240deg);\n  transform: rotate(240deg);\n}\n\n.fbt .sk-fading-circle .sk-circle10 {\n  -webkit-transform: rotate(270deg);\n  transform: rotate(270deg);\n}\n\n.fbt .sk-fading-circle .sk-circle11 {\n  -webkit-transform: rotate(300deg);\n  transform: rotate(300deg);\n}\n\n.fbt .sk-fading-circle .sk-circle12 {\n  -webkit-transform: rotate(330deg);\n  transform: rotate(330deg);\n}\n\n.fbt .sk-fading-circle .sk-circle2:before {\n  -webkit-animation-delay: -1.1s;\n  animation-delay: -1.1s;\n}\n\n.fbt .sk-fading-circle .sk-circle3:before {\n  -webkit-animation-delay: -1s;\n  animation-delay: -1s;\n}\n\n.fbt .sk-fading-circle .sk-circle4:before {\n  -webkit-animation-delay: -0.9s;\n  animation-delay: -0.9s;\n}\n\n.fbt .sk-fading-circle .sk-circle5:before {\n  -webkit-animation-delay: -0.8s;\n  animation-delay: -0.8s;\n}\n\n.fbt .sk-fading-circle .sk-circle6:before {\n  -webkit-animation-delay: -0.7s;\n  animation-delay: -0.7s;\n}\n\n.fbt .sk-fading-circle .sk-circle7:before {\n  -webkit-animation-delay: -0.6s;\n  animation-delay: -0.6s;\n}\n\n.fbt .sk-fading-circle .sk-circle8:before {\n  -webkit-animation-delay: -0.5s;\n  animation-delay: -0.5s;\n}\n\n.fbt .sk-fading-circle .sk-circle9:before {\n  -webkit-animation-delay: -0.4s;\n  animation-delay: -0.4s;\n}\n\n.fbt .sk-fading-circle .sk-circle10:before {\n  -webkit-animation-delay: -0.3s;\n  animation-delay: -0.3s;\n}\n\n.fbt .sk-fading-circle .sk-circle11:before {\n  -webkit-animation-delay: -0.2s;\n  animation-delay: -0.2s;\n}\n\n.fbt .sk-fading-circle .sk-circle12:before {\n  -webkit-animation-delay: -0.1s;\n  animation-delay: -0.1s;\n}\n\n@-webkit-keyframes sk-circleFadeDelay {\n  0%, 39%, 100% {\n    opacity: 0;\n  }\n  40% {\n    opacity: 1;\n  }\n}\n\n@keyframes sk-circleFadeDelay {\n  0%, 39%, 100% {\n    opacity: 0;\n  }\n  40% {\n    opacity: 1;\n  }\n}\n\n.col-xl-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\nem {\n  font-style: normal;\n}\n\n@media print {\n  .print_container {\n    width: 21cm;\n    height: 29.7cm;\n    margin: 30mm 45mm 30mm 45mm;\n    background-color: white;\n    /* change the margins as you want them to be. */\n  }\n\n  #header {\n    display: none;\n  }\n\n  h5 {\n    display: none;\n  }\n\n  app-page-header {\n    display: none;\n  }\n\n  button {\n    display: none;\n  }\n\n  * {\n    font-size: 13px;\n  }\n\n  div.table-order {\n    -webkit-transform: scale(0.7);\n            transform: scale(0.7);\n    padding-bottom: 80px;\n    width: 20cm;\n    height: 9cm;\n  }\n\n  div._container {\n    padding-top: 0px;\n    page-break-after: always;\n  }\n\n  .page-number {\n    display: none;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50LWxpYnMvZm9ybS1idWlsZGVyLXRhYmxlL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGNvbXBvbmVudC1saWJzXFxmb3JtLWJ1aWxkZXItdGFibGVcXGZvcm0tYnVpbGRlci10YWJsZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tcG9uZW50LWxpYnMvZm9ybS1idWlsZGVyLXRhYmxlL2Zvcm0tYnVpbGRlci10YWJsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7QUNDRjs7QURFQTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ0NGOztBREVBO0VBQ0Usa0JBQUE7QUNDRjs7QURDRTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7QUNDSjs7QURBSTtFQUNFLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0FDRU47O0FEQU07RUFFRSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7RUFDQSx5QkFBQTtBQ0NSOztBRENNO0VBRUUsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7QUNBUjs7QURFTTtFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7QUNBUjs7QURNQTtFQUVFLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0FDSkY7O0FET0E7RUFDRSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsYUFBQTtBQ0pGOztBRE9BO0VBQ0UsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSw0QkFBQTtFQUNBLHVCQUFBO0FDSkY7O0FES0U7RUFDRSxpQkFBQTtBQ0hKOztBRFFBO0VBQ0Usb0NBQUE7QUNMRjs7QURPQTtFQUNFLHlCQUFBO0VBQ0EscUJBQUE7QUNKRjs7QURNQTtFQUNFLG9DQUFBO0VBQ0EsZ0NBQUE7QUNIRjs7QURNQTtFQUNFLHlCQUFBO0VBQ0EsdUJBQUE7QUNIRjs7QURRQTtFQUNFLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDTEY7O0FET0E7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0pGOztBRE1BO0VBQ0UseUJBQUE7QUNIRjs7QURLQTs7RUFFRSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0ZGOztBRElBO0VBQ0Usd0NBQUE7QUNERjs7QURJQTtFQUNFLGVBQUE7RUFDQSxjQUFBO0VBQ0EsdUJBQUE7QUNERjs7QURJQTtFQUNFLDRCQUFBO0VBQ0EsK0JBQUE7QUNERjs7QURJQTtFQUNFLDJCQUFBO0VBQ0EsOEJBQUE7QUNERjs7QURJQTtFQUNFLDZCQUFBO0FDREY7O0FERUU7RUFFRSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtBQ0RKOztBRElFO0VBQ0UsY0FBQTtBQ0ZKOztBRElJO0VBQ0UsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7QUNGTjs7QURPQTtFQUNFLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGlDQUFBO0VBQ0EsZUFBQTtBQ0pGOztBRE1BO0VBQ0UseUJBQUE7QUNIRjs7QURLQTtFQUNFLGFBQUE7QUNGRjs7QURJQTtFQUNFLHVCQUFBO0FDREY7O0FESUE7RUFDRSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FDREY7O0FESUE7RUFDRSxjQUFBO0FDREY7O0FESUE7RUFFRSxjQUFBO0VBRUEsaUJBQUE7RUFDQSxlQUFBO0FDSEY7O0FETUE7RUFDRSxlQUFBO0FDSEY7O0FET0U7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7QUNKSjs7QURNRTtFQUNFLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7QUNKSjs7QURNSTtFQUNFLGNBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBRUEsV0FBQTtFQUNBLFlBQUE7QUNMTjs7QURPSTtFQUNFO0lBQ0UsZUFBQTtFQ0xOO0FBQ0Y7O0FEUUk7RUFDRSxxQkFBQTtBQ05OOztBRFNJO0VBQ0UsY0FBQTtFQUNBLHFCQUFBO0FDUE47O0FEVUk7RUFDRSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDUk47O0FEVUk7RUFDRTtJQUNFLFdBQUE7SUFDQSxpQkFBQTtJQUNBLGdCQUFBO0VDUk47QUFDRjs7QURVSTtFQUNFLGVBQUE7QUNSTjs7QURXSTtFQUNFLGdCQUFBO0VBRUEsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDVk47O0FEYUU7RUFDRSx5QkFBQTtFQUNBLHFCQUFBO0VBRUEsWUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQ1pKOztBRGdCSTtFQUNFLGtCQUFBO0FDZE47O0FEZ0JJO0VBQ0Usa0JBQUE7RUFDQSxpQkFBQTtBQ2ROOztBRGdCSTtFQUNFLHdCQUFBO0FDZE47O0FEa0JFO0VBQ0UsOEJBQUE7QUNoQko7O0FEbUJFO0VBQ0Usd0JBQUE7RUFDQSxvQ0FBQTtBQ2pCSjs7QURvQkU7O0VBRUUseUJBQUE7QUNsQko7O0FEcUJFO0VBQ0UsU0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtBQ25CSjs7QURzQkk7RUFFRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSw4QkFBQTtFQUNBLHlCQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSwwQkFBQTtBQ3JCTjs7QURzQk07RUFDRSxrQkFBQTtBQ3BCUjs7QURxQlE7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtBQ25CVjs7QUR1Qkk7RUFDRSxtQkFBQTtFQUVBLGVBQUE7QUN0Qk47O0FEd0JJO0VBQ0UsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ3RCTjs7QUR3Qkk7RUFDRSxlQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUN0Qk47O0FEMkJNO0VBQ0UsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDekJSOztBRDRCSTtFQUNJLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQzFCUjs7QUQ2Qkk7RUFDSSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUMzQlI7O0FEOEJJO0VBQ0ksbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDNUJSOztBRCtCSTtFQUNJLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQzdCUjs7QUQrQk07RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUM3QlI7O0FEOEJRO0VBQ0UsYUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQzVCVjs7QUQ4QlE7RUFDRSxlQUFBO0FDNUJWOztBRGtDRTtFQUNFLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ2hDSjs7QURtQ0U7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7QUNqQ0o7O0FEb0NFO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0VBQUE7RUFDQSw0REFBQTtBQ2xDSjs7QURvQ0U7RUFDRSxnQ0FBQTtFQUVBLHdCQUFBO0FDbENKOztBRG9DRTtFQUNFLGdDQUFBO0VBRUEsd0JBQUE7QUNsQ0o7O0FEb0NFO0VBQ0UsZ0NBQUE7RUFFQSx3QkFBQTtBQ2xDSjs7QURvQ0U7RUFDRSxpQ0FBQTtFQUVBLHlCQUFBO0FDbENKOztBRG9DRTtFQUNFLGlDQUFBO0VBRUEseUJBQUE7QUNsQ0o7O0FEb0NFO0VBQ0UsaUNBQUE7RUFFQSx5QkFBQTtBQ2xDSjs7QURvQ0U7RUFDRSxpQ0FBQTtFQUVBLHlCQUFBO0FDbENKOztBRG9DRTtFQUNFLGlDQUFBO0VBRUEseUJBQUE7QUNsQ0o7O0FEb0NFO0VBQ0UsaUNBQUE7RUFFQSx5QkFBQTtBQ2xDSjs7QURvQ0U7RUFDRSxpQ0FBQTtFQUVBLHlCQUFBO0FDbENKOztBRG9DRTtFQUNFLGlDQUFBO0VBRUEseUJBQUE7QUNsQ0o7O0FEb0NFO0VBQ0UsOEJBQUE7RUFDQSxzQkFBQTtBQ2xDSjs7QURvQ0U7RUFDRSw0QkFBQTtFQUNBLG9CQUFBO0FDbENKOztBRG9DRTtFQUNFLDhCQUFBO0VBQ0Esc0JBQUE7QUNsQ0o7O0FEb0NFO0VBQ0UsOEJBQUE7RUFDQSxzQkFBQTtBQ2xDSjs7QURvQ0U7RUFDRSw4QkFBQTtFQUNBLHNCQUFBO0FDbENKOztBRG9DRTtFQUNFLDhCQUFBO0VBQ0Esc0JBQUE7QUNsQ0o7O0FEb0NFO0VBQ0UsOEJBQUE7RUFDQSxzQkFBQTtBQ2xDSjs7QURvQ0U7RUFDRSw4QkFBQTtFQUNBLHNCQUFBO0FDbENKOztBRG9DRTtFQUNFLDhCQUFBO0VBQ0Esc0JBQUE7QUNsQ0o7O0FEb0NFO0VBQ0UsOEJBQUE7RUFDQSxzQkFBQTtBQ2xDSjs7QURvQ0U7RUFDRSw4QkFBQTtFQUNBLHNCQUFBO0FDbENKOztBRHFDRTtFQUNFO0lBR0UsVUFBQTtFQ3JDSjtFRHVDRTtJQUNFLFVBQUE7RUNyQ0o7QUFDRjs7QUR3Q0U7RUFDRTtJQUdFLFVBQUE7RUN4Q0o7RUQwQ0U7SUFDRSxVQUFBO0VDeENKO0FBQ0Y7O0FENENBO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtBQ3pDRjs7QUQ0Q0E7RUFDRSxrQkFBQTtBQ3pDRjs7QUQ2Q0E7RUFDRTtJQUNFLFdBQUE7SUFDQSxjQUFBO0lBQ0EsMkJBQUE7SUFDQSx1QkFBQTtJQUNBLCtDQUFBO0VDMUNGOztFRDZDQTtJQUNFLGFBQUE7RUMxQ0Y7O0VENkNBO0lBQ0UsYUFBQTtFQzFDRjs7RUQ2Q0E7SUFDRSxhQUFBO0VDMUNGOztFRDZDQTtJQUNFLGFBQUE7RUMxQ0Y7O0VENkNBO0lBQ0UsZUFBQTtFQzFDRjs7RUQ2Q0E7SUFDRSw2QkFBQTtZQUFBLHFCQUFBO0lBQ0Esb0JBQUE7SUFDQSxXQUFBO0lBQ0EsV0FBQTtFQzFDRjs7RUQ2Q0E7SUFDRSxnQkFBQTtJQUNBLHdCQUFBO0VDMUNGOztFRDZDQTtJQUNFLGFBQUE7RUMxQ0Y7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudC1saWJzL2Zvcm0tYnVpbGRlci10YWJsZS9mb3JtLWJ1aWxkZXItdGFibGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudW5kaXNwbGF5ZWQge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi5pbWFnZS1hdXRvZm9ybSB7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG4gIGhlaWdodDogMjAwcHg7XHJcbiAgb2JqZWN0LWZpdDogY29udGFpbjtcclxufVxyXG5cclxuLmRhdGUtcGlja2VyLXdyYXBwZXIge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgLmRhdGUtcGlja2VyLW92ZXJsYXkge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogOTk7XHJcbiAgICByaWdodDogMDtcclxuICAgIC5kcGlja2VyIHtcclxuICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkZmRmZGY7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHJcbiAgICAgIC5idG4ub2sge1xyXG4gICAgICAgIC8vIGZsb2F0OiByaWdodDtcclxuICAgICAgICBjbGVhcjogYm90aDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBtYXJnaW46IDBweCA1cHggMTBweDtcclxuICAgICAgICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICB9XHJcbiAgICAgIC5idG4uY2xlYXIge1xyXG4gICAgICAgIC8vIGZsb2F0OiByaWdodDtcclxuICAgICAgICBjbGVhcjogYm90aDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTcyLCAxMCwgMTApO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBtYXJnaW46IDBweCAxMHB4IDEwcHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgfVxyXG4gICAgICBuZ2ItZGF0ZXBpY2tlciB7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgY2xlYXI6IGJvdGg7XHJcbiAgICAgICAgbWFyZ2luOiAxMHB4O1xyXG4gICAgICAgIHJpZ2h0OiAwJTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmxheW91dC1jYXJkIHtcclxuICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMkUzNTQ1O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgbWFyZ2luLXRvcDozMHB4O1xyXG59XHJcblxyXG5zZWxlY3Qge1xyXG4gIGJvcmRlcjogMHB4IHNvbGlkICNkZGQ7XHJcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLmJ0biB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIG1hcmdpbi1yaWdodDogMTVweDtcclxuICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjM4MWZiO1xyXG4gIHRyYW5zaXRpb246IGVhc2UgMC4zcyBhbGwgMHM7XHJcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgLmZhIHtcclxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG4gICAgLy8gLy9jb2xvcjogd2hpdGU7XHJcbiAgfVxyXG59XHJcblxyXG4uYnRuLWFwcHJvdmUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyOGE3NDUgIWltcG9ydGFudDtcclxufVxyXG4uYnRuLXJlbW92ZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U3NGMzYztcclxuICBib3JkZXItY29sb3I6ICNkYjM3MDA7XHJcbn1cclxuLmJ0bi1yZW1vdmU6aG92ZXJ7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2RiMzcwMCAhaW1wb3J0YW50O1xyXG4gIGJvcmRlci1jb2xvcjogI2RiMzcwMCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYnRuOmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoNjEsIDE0NSwgMjU1KTtcclxuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxufVxyXG4vLyAuYnRuLnJpZ2h0e1xyXG4vLyAgIC8vIC8vY29sb3I6IHdoaXRlO1xyXG4vLyB9XHJcbi5jdXN0b20tZGF5IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZzogMC4xODVyZW0gMC4yNXJlbTtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgaGVpZ2h0OiAycmVtO1xyXG4gIHdpZHRoOiAycmVtO1xyXG59XHJcbmlmcmFtZSB7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gIGhlaWdodDogMHB4O1xyXG4gIHdpZHRoOiAwcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvcmRlci13aWR0aDogMHB4O1xyXG59XHJcbi5jdXN0b20tZGF5LmZvY3VzZWQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlNmU2ZTY7XHJcbn1cclxuLmN1c3RvbS1kYXkucmFuZ2UsXHJcbi5jdXN0b20tZGF5OmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMiwgMTE3LCAyMTYpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcbi5jdXN0b20tZGF5LmZhZGVkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIsIDExNywgMjE2LCAwLjUpO1xyXG59XHJcblxyXG5saS5wYWdlLWl0ZW0ge1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBjb2xvcjogIzg2OWFiYjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmVuZCB7XHJcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDVweDtcclxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNXB4O1xyXG59XHJcblxyXG4uc3RhcnQge1xyXG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDVweDtcclxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA1cHg7XHJcbn1cclxuXHJcbi5wYWdpbmF0aW9uLWhlYWQge1xyXG4gIHdpZHRoOiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xyXG4gIC5wYWdpbmF0aW9uIHtcclxuICAgIC8vIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICB9XHJcblxyXG4gIC5zaG93LWVudHJpZXMge1xyXG4gICAgY29sb3I6ICM4NjlhYmI7XHJcblxyXG4gICAgLm9yZGVyLWJ0biB7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzk0NDU4O1xyXG4gICAgICBjb2xvcjogIzg2OWFiYjtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmEuY3VycmVudC1wYWdlIHtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjY2MgIWltcG9ydGFudDtcclxuICBtYXJnaW46IDVweCA1cHg7XHJcbn1cclxuYS5jdXJyZW50LXBhZ2U6aG92ZXIge1xyXG4gIGNvbG9yOiAjODY5YWJiICFpbXBvcnRhbnQ7XHJcbn1cclxuYS5kb3R0ZXIge1xyXG4gIGN1cnNvcjogdW5zZXQ7XHJcbn1cclxuYS5kb3R0ZXI6aG92ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHVuc2V0O1xyXG59XHJcblxyXG5hLnBhZ2UtbGluayB7XHJcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgcGFkZGluZzogOHB4IDEycHg7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBtYXJnaW4tdG9wOiA1cHg7XHJcbn1cclxuXHJcbmEucGFnZS1saW5rOmhvdmVyIHtcclxuICBjb2xvcjogIzg2OWFiYjtcclxufVxyXG5cclxuYS5wYWdlLWxpbmsubmV4dCB7XHJcbiAgLy8gYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjODY5YWJiO1xyXG4gIGNvbG9yOiAjODY5YWJiO1xyXG4gIC8vIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICM4NjlhYmI7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcblxyXG5hIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5mYnQge1xyXG4gIC5mb3JtLWNvbnRyb2wge1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICB9XHJcbiAgLmhlYWRlci1jdXN0b20ge1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAubGFiZWwtY3VzdG9tIHtcclxuICAgICAgaGVpZ2h0OiAzNy42cHg7XHJcbiAgICAgIHBhZGRpbmc6IDZweDtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxY207XHJcbiAgICAgIC8vIGNvbG9yOiAjN2U3ZTdlO1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG4gICAgQG1lZGlhIChtYXgtd2lkdGg6IDUwMHB4KSB7XHJcbiAgICAgIC5sYWJlbC1jdXN0b20ge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5idG46aG92ZXIge1xyXG4gICAgICBib3JkZXItY29sb3I6ICMzNDk4ZGI7XHJcbiAgICB9XHJcblxyXG4gICAgLmJ0bi50b2dnbGUtdHJ1ZSB7XHJcbiAgICAgIGNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICBib3JkZXItY29sb3I6ICMzNDk4ZGI7XHJcbiAgICB9XHJcblxyXG4gICAgLmJ0bi5yaWdodCB7XHJcbiAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICB9XHJcbiAgICBAbWVkaWEgKG1heC13aWR0aDogNTAwcHgpIHtcclxuICAgICAgLmJ0bi5yaWdodCB7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmhlYWQge1xyXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLmNiLWNvbG91bW4ge1xyXG4gICAgICBwYWRkaW5nOiA1cHggNXB4O1xyXG4gICAgICAvLyB3aWR0aDogMjQwcHg7XHJcbiAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICBoZWlnaHQ6IDI5cHg7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMTVweDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmRyb3Bkb3duLWhlYWRlciB7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDUwJTtcclxuICAgIHBhZGRpbmc6IDBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICB9XHJcblxyXG4gIDo6bmctZGVlcCAubWF0LWNoZWNrYm94IHtcclxuICAgIC5jZGstdmlzdWFsbHktaGlkZGVuIHtcclxuICAgICAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgfVxyXG4gICAgLm1hdC1jaGVja2JveC1mcmFtZSB7XHJcbiAgICAgIGJvcmRlci1jb2xvcjogI2NjYztcclxuICAgICAgYm9yZGVyLXdpZHRoOiAxcHg7XHJcbiAgICB9XHJcbiAgICAubWF0LWNoZWNrYm94LWNoZWNrbWFyay1wYXRoIHtcclxuICAgICAgc3Ryb2tlOiBibGFjayAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmN1c3RvbS10YWJsZS1ib3JkZXIge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogbm9uZSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgOjpuZy1kZWVwIC5tYXQtY2hlY2tib3gtY2hlY2tlZC5tYXQtYWNjZW50IC5tYXQtY2hlY2tib3gtcmlwcGxlIC5tYXQtcmlwcGxlLWVsZW1lbnQge1xyXG4gICAgb3BhY2l0eTogMC4wMyAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzU2YTRmZiAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgOjpuZy1kZWVwIC5tYXQtY2hlY2tib3gtY2hlY2tlZC5tYXQtYWNjZW50IC5tYXQtY2hlY2tib3gtYmFja2dyb3VuZCxcclxuICAubWF0LWNoZWNrYm94LWluZGV0ZXJtaW5hdGUubWF0LWFjY2VudCAubWF0LWNoZWNrYm94LWJhY2tncm91bmQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzU2YTRmZjtcclxuICB9XHJcblxyXG4gIC5jYXJkLWJvZHkge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcGFkZGluZzogMHB4O1xyXG4gICAgbWluLWhlaWdodDogNDUwcHg7XHJcblxyXG4gICAgLy8gYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIHRoZWFkIHRoIHtcclxuICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjojM0E0NDU4O1xyXG4gICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wICFpbXBvcnRhbnQ7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMmYyZjI7XHJcbiAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAuc2VhcmNoLXN0cmluZyB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5zZWFyY2gtZmllbGQge1xyXG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgcmlnaHQ6IDdweDtcclxuICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgb3BhY2l0eTogMC4zO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgdGJvZHkgdHI6aG92ZXIgdGQge1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjM2I5ZmUyO1xyXG4gICAgICAvL2NvbG9yOiB3aGl0ZTtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgfVxyXG4gICAgdGJvZHkgdHIuaW1hZ2U6aG92ZXIgdGQge1xyXG4gICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgY3Vyc29yOiBkZWZhdWx0O1xyXG4gICAgICBjb2xvcjogdW5zZXQ7XHJcbiAgICB9XHJcbiAgICB0Ym9keSB0ciB0ZCB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgdmVydGljYWwtYWxpZ246IHRvcCAhaW1wb3J0YW50O1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIHBhZGRpbmc6IDEycHg7XHJcbiAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6IzJGMzU0NTtcclxuICAgICAgLy8gY29sb3I6ICM4OTlCQzE7XHJcbiAgICB9XHJcbiAgICB0Ym9keSB7XHJcbiAgICAgIC5wZW5kaW5nIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmU2ZDU7XHJcbiAgICAgICAgY29sb3I6ICNlZTY4MGM7XHJcbiAgICAgICAgd2lkdGg6IDgwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICB9XHJcblxyXG4gICAgLnN1Y2Nlc3Mge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkYWY5ZjE7XHJcbiAgICAgICAgY29sb3I6ICMyZWE1NmU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgICAgICB3aWR0aDogODBweDtcclxuICAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgIH1cclxuXHJcbiAgICAuaG9sZCB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YyZThmZjtcclxuICAgICAgICBjb2xvcjogIzhiNWFjZjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIC5yZWZ1bmRlZCB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTRlY2ZmO1xyXG4gICAgICAgIGNvbG9yOiAjNDI3MWRmO1xyXG4gICAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIC5yZWplY3RlZCB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmNlY2YwO1xyXG4gICAgICAgIGNvbG9yOiAjZWMzYjYwO1xyXG4gICAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgfVxyXG4gICAgICAuaW1nX2R0X3BycCB7XHJcbiAgICAgICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICBoZWlnaHQ6IDEyMHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAuaW1hZ2Vfd3JhcHBlciB7XHJcbiAgICAgICAgICBoZWlnaHQ6IDEyMHB4O1xyXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuc2stZmFkaW5nLWNpcmNsZSB7XHJcbiAgICBtYXJnaW46IDEwcHggYXV0bztcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxuXHJcbiAgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0b3A6IDA7XHJcbiAgfVxyXG5cclxuICAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlOmJlZm9yZSB7XHJcbiAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHdpZHRoOiAxNSU7XHJcbiAgICBoZWlnaHQ6IDE1JTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzMzM7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgLXdlYmtpdC1hbmltYXRpb246IHNrLWNpcmNsZUZhZGVEZWxheSAxLjJzIGluZmluaXRlIGVhc2UtaW4tb3V0IGJvdGg7XHJcbiAgICBhbmltYXRpb246IHNrLWNpcmNsZUZhZGVEZWxheSAxLjJzIGluZmluaXRlIGVhc2UtaW4tb3V0IGJvdGg7XHJcbiAgfVxyXG4gIC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUyIHtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzBkZWcpO1xyXG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDMwZGVnKTtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDMwZGVnKTtcclxuICB9XHJcbiAgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTMge1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg2MGRlZyk7XHJcbiAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNjBkZWcpO1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoNjBkZWcpO1xyXG4gIH1cclxuICAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNCB7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcclxuICAgIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XHJcbiAgfVxyXG4gIC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU1IHtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTIwZGVnKTtcclxuICAgIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgxMjBkZWcpO1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMTIwZGVnKTtcclxuICB9XHJcbiAgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTYge1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxNTBkZWcpO1xyXG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDE1MGRlZyk7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxNTBkZWcpO1xyXG4gIH1cclxuICAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNyB7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XHJcbiAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XHJcbiAgfVxyXG4gIC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU4IHtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMjEwZGVnKTtcclxuICAgIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgyMTBkZWcpO1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMjEwZGVnKTtcclxuICB9XHJcbiAgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTkge1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgyNDBkZWcpO1xyXG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDI0MGRlZyk7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgyNDBkZWcpO1xyXG4gIH1cclxuICAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTAge1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgyNzBkZWcpO1xyXG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDI3MGRlZyk7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgyNzBkZWcpO1xyXG4gIH1cclxuICAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTEge1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzMDBkZWcpO1xyXG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDMwMGRlZyk7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgzMDBkZWcpO1xyXG4gIH1cclxuICAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTIge1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzMzBkZWcpO1xyXG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDMzMGRlZyk7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgzMzBkZWcpO1xyXG4gIH1cclxuICAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMjpiZWZvcmUge1xyXG4gICAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0xLjFzO1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAtMS4xcztcclxuICB9XHJcbiAgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTM6YmVmb3JlIHtcclxuICAgIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMXM7XHJcbiAgICBhbmltYXRpb24tZGVsYXk6IC0xcztcclxuICB9XHJcbiAgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTQ6YmVmb3JlIHtcclxuICAgIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC45cztcclxuICAgIGFuaW1hdGlvbi1kZWxheTogLTAuOXM7XHJcbiAgfVxyXG4gIC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU1OmJlZm9yZSB7XHJcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuOHM7XHJcbiAgICBhbmltYXRpb24tZGVsYXk6IC0wLjhzO1xyXG4gIH1cclxuICAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNjpiZWZvcmUge1xyXG4gICAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjdzO1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAtMC43cztcclxuICB9XHJcbiAgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTc6YmVmb3JlIHtcclxuICAgIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC42cztcclxuICAgIGFuaW1hdGlvbi1kZWxheTogLTAuNnM7XHJcbiAgfVxyXG4gIC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU4OmJlZm9yZSB7XHJcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuNXM7XHJcbiAgICBhbmltYXRpb24tZGVsYXk6IC0wLjVzO1xyXG4gIH1cclxuICAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlOTpiZWZvcmUge1xyXG4gICAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjRzO1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAtMC40cztcclxuICB9XHJcbiAgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTEwOmJlZm9yZSB7XHJcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuM3M7XHJcbiAgICBhbmltYXRpb24tZGVsYXk6IC0wLjNzO1xyXG4gIH1cclxuICAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTE6YmVmb3JlIHtcclxuICAgIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC4ycztcclxuICAgIGFuaW1hdGlvbi1kZWxheTogLTAuMnM7XHJcbiAgfVxyXG4gIC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMjpiZWZvcmUge1xyXG4gICAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjFzO1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAtMC4xcztcclxuICB9XHJcblxyXG4gIEAtd2Via2l0LWtleWZyYW1lcyBzay1jaXJjbGVGYWRlRGVsYXkge1xyXG4gICAgMCUsXHJcbiAgICAzOSUsXHJcbiAgICAxMDAlIHtcclxuICAgICAgb3BhY2l0eTogMDtcclxuICAgIH1cclxuICAgIDQwJSB7XHJcbiAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBAa2V5ZnJhbWVzIHNrLWNpcmNsZUZhZGVEZWxheSB7XHJcbiAgICAwJSxcclxuICAgIDM5JSxcclxuICAgIDEwMCUge1xyXG4gICAgICBvcGFjaXR5OiAwO1xyXG4gICAgfVxyXG4gICAgNDAlIHtcclxuICAgICAgb3BhY2l0eTogMTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5jb2wteGwtMTIge1xyXG4gIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcclxufVxyXG5cclxuZW0ge1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxufVxyXG5cclxuXHJcbkBtZWRpYSBwcmludCB7XHJcbiAgLnByaW50X2NvbnRhaW5lcntcclxuICAgIHdpZHRoOiAyMWNtO1xyXG4gICAgaGVpZ2h0OiAyOS43Y207XHJcbiAgICBtYXJnaW46IDMwbW0gNDVtbSAzMG1tIDQ1bW07IFxyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICAvKiBjaGFuZ2UgdGhlIG1hcmdpbnMgYXMgeW91IHdhbnQgdGhlbSB0byBiZS4gKi9cclxufSBcclxuXHJcbiAgI2hlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgaDUge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcblxyXG4gIGFwcC1wYWdlLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgYnV0dG9uIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAqIHtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICB9XHJcblxyXG4gIGRpdi50YWJsZS1vcmRlciB7XHJcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogODBweDtcclxuICAgIHdpZHRoOiAyMGNtO1xyXG4gICAgaGVpZ2h0OiA5Y207XHJcbiAgfVxyXG5cclxuICBkaXYuX2NvbnRhaW5lciB7XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xyXG4gIH1cclxuXHJcbiAgLnBhZ2UtbnVtYmVyIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG59IiwiLnVuZGlzcGxheWVkIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLmltYWdlLWF1dG9mb3JtIHtcbiAgd2lkdGg6IDIwMHB4O1xuICBoZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuXG4uZGF0ZS1waWNrZXItd3JhcHBlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5kYXRlLXBpY2tlci13cmFwcGVyIC5kYXRlLXBpY2tlci1vdmVybGF5IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA5OTtcbiAgcmlnaHQ6IDA7XG59XG4uZGF0ZS1waWNrZXItd3JhcHBlciAuZGF0ZS1waWNrZXItb3ZlcmxheSAuZHBpY2tlciB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZGZkZmRmO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4uZGF0ZS1waWNrZXItd3JhcHBlciAuZGF0ZS1waWNrZXItb3ZlcmxheSAuZHBpY2tlciAuYnRuLm9rIHtcbiAgY2xlYXI6IGJvdGg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luOiAwcHggNXB4IDEwcHg7XG4gIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG4uZGF0ZS1waWNrZXItd3JhcHBlciAuZGF0ZS1waWNrZXItb3ZlcmxheSAuZHBpY2tlciAuYnRuLmNsZWFyIHtcbiAgY2xlYXI6IGJvdGg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhYzBhMGE7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luOiAwcHggMTBweCAxMHB4O1xuICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xufVxuLmRhdGUtcGlja2VyLXdyYXBwZXIgLmRhdGUtcGlja2VyLW92ZXJsYXkgLmRwaWNrZXIgbmdiLWRhdGVwaWNrZXIge1xuICBmbG9hdDogbGVmdDtcbiAgY2xlYXI6IGJvdGg7XG4gIG1hcmdpbjogMTBweDtcbiAgcmlnaHQ6IDAlO1xufVxuXG4ubGF5b3V0LWNhcmQge1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgbWFyZ2luLXRvcDogMzBweDtcbn1cblxuc2VsZWN0IHtcbiAgYm9yZGVyOiAwcHggc29saWQgI2RkZDtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbi5idG4ge1xuICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzIzODFmYjtcbiAgdHJhbnNpdGlvbjogZWFzZSAwLjNzIGFsbCAwcztcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG59XG4uYnRuIC5mYSB7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4uYnRuLWFwcHJvdmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjhhNzQ1ICFpbXBvcnRhbnQ7XG59XG5cbi5idG4tcmVtb3ZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U3NGMzYztcbiAgYm9yZGVyLWNvbG9yOiAjZGIzNzAwO1xufVxuXG4uYnRuLXJlbW92ZTpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkYjM3MDAgIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjZGIzNzAwICFpbXBvcnRhbnQ7XG59XG5cbi5idG46aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2Q5MWZmO1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuLmN1c3RvbS1kYXkge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDAuMTg1cmVtIDAuMjVyZW07XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgaGVpZ2h0OiAycmVtO1xuICB3aWR0aDogMnJlbTtcbn1cblxuaWZyYW1lIHtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICBoZWlnaHQ6IDBweDtcbiAgd2lkdGg6IDBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3JkZXItd2lkdGg6IDBweDtcbn1cblxuLmN1c3RvbS1kYXkuZm9jdXNlZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNmU2ZTY7XG59XG5cbi5jdXN0b20tZGF5LnJhbmdlLFxuLmN1c3RvbS1kYXk6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDI3NWQ4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5jdXN0b20tZGF5LmZhZGVkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyLCAxMTcsIDIxNiwgMC41KTtcbn1cblxubGkucGFnZS1pdGVtIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBjb2xvcjogIzg2OWFiYjtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5lbmQge1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNXB4O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNXB4O1xufVxuXG4uc3RhcnQge1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1cHg7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDVweDtcbn1cblxuLnBhZ2luYXRpb24taGVhZCB7XG4gIHdpZHRoOiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xufVxuLnBhZ2luYXRpb24taGVhZCAucGFnaW5hdGlvbiB7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBmbG9hdDogcmlnaHQ7XG59XG4ucGFnaW5hdGlvbi1oZWFkIC5zaG93LWVudHJpZXMge1xuICBjb2xvcjogIzg2OWFiYjtcbn1cbi5wYWdpbmF0aW9uLWhlYWQgLnNob3ctZW50cmllcyAub3JkZXItYnRuIHtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzOTQ0NTg7XG4gIGNvbG9yOiAjODY5YWJiO1xufVxuXG5hLmN1cnJlbnQtcGFnZSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2NjYyAhaW1wb3J0YW50O1xuICBtYXJnaW46IDVweCA1cHg7XG59XG5cbmEuY3VycmVudC1wYWdlOmhvdmVyIHtcbiAgY29sb3I6ICM4NjlhYmIgIWltcG9ydGFudDtcbn1cblxuYS5kb3R0ZXIge1xuICBjdXJzb3I6IHVuc2V0O1xufVxuXG5hLmRvdHRlcjpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHVuc2V0O1xufVxuXG5hLnBhZ2UtbGluayB7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIGJvcmRlcjogbm9uZTtcbiAgcGFkZGluZzogOHB4IDEycHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6IGJsYWNrO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG5hLnBhZ2UtbGluazpob3ZlciB7XG4gIGNvbG9yOiAjODY5YWJiO1xufVxuXG5hLnBhZ2UtbGluay5uZXh0IHtcbiAgY29sb3I6ICM4NjlhYmI7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbmEge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5mYnQgLmZvcm0tY29udHJvbCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbn1cbi5mYnQgLmhlYWRlci1jdXN0b20ge1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbn1cbi5mYnQgLmhlYWRlci1jdXN0b20gLmxhYmVsLWN1c3RvbSB7XG4gIGhlaWdodDogMzcuNnB4O1xuICBwYWRkaW5nOiA2cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgbWFyZ2luLWJvdHRvbTogMWNtO1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IHdoaXRlO1xufVxuQG1lZGlhIChtYXgtd2lkdGg6IDUwMHB4KSB7XG4gIC5mYnQgLmhlYWRlci1jdXN0b20gLmxhYmVsLWN1c3RvbSB7XG4gICAgZm9udC1zaXplOiAyMnB4O1xuICB9XG59XG4uZmJ0IC5oZWFkZXItY3VzdG9tIC5idG46aG92ZXIge1xuICBib3JkZXItY29sb3I6ICMzNDk4ZGI7XG59XG4uZmJ0IC5oZWFkZXItY3VzdG9tIC5idG4udG9nZ2xlLXRydWUge1xuICBjb2xvcjogIzM0OThkYjtcbiAgYm9yZGVyLWNvbG9yOiAjMzQ5OGRiO1xufVxuLmZidCAuaGVhZGVyLWN1c3RvbSAuYnRuLnJpZ2h0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbn1cbkBtZWRpYSAobWF4LXdpZHRoOiA1MDBweCkge1xuICAuZmJ0IC5oZWFkZXItY3VzdG9tIC5idG4ucmlnaHQge1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIH1cbn1cbi5mYnQgLmhlYWRlci1jdXN0b20gLmhlYWQge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uZmJ0IC5oZWFkZXItY3VzdG9tIC5jYi1jb2xvdW1uIHtcbiAgcGFkZGluZzogNXB4IDVweDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIGhlaWdodDogMjlweDtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xufVxuLmZidCAuZHJvcGRvd24taGVhZGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICBwYWRkaW5nOiAwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLmZidCA6Om5nLWRlZXAgLm1hdC1jaGVja2JveCAuY2RrLXZpc3VhbGx5LWhpZGRlbiB7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbn1cbi5mYnQgOjpuZy1kZWVwIC5tYXQtY2hlY2tib3ggLm1hdC1jaGVja2JveC1mcmFtZSB7XG4gIGJvcmRlci1jb2xvcjogI2NjYztcbiAgYm9yZGVyLXdpZHRoOiAxcHg7XG59XG4uZmJ0IDo6bmctZGVlcCAubWF0LWNoZWNrYm94IC5tYXQtY2hlY2tib3gtY2hlY2ttYXJrLXBhdGgge1xuICBzdHJva2U6IGJsYWNrICFpbXBvcnRhbnQ7XG59XG4uZmJ0IC5jdXN0b20tdGFibGUtYm9yZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogbm9uZSAhaW1wb3J0YW50O1xufVxuLmZidCA6Om5nLWRlZXAgLm1hdC1jaGVja2JveC1jaGVja2VkLm1hdC1hY2NlbnQgLm1hdC1jaGVja2JveC1yaXBwbGUgLm1hdC1yaXBwbGUtZWxlbWVudCB7XG4gIG9wYWNpdHk6IDAuMDMgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU2YTRmZiAhaW1wb3J0YW50O1xufVxuLmZidCA6Om5nLWRlZXAgLm1hdC1jaGVja2JveC1jaGVja2VkLm1hdC1hY2NlbnQgLm1hdC1jaGVja2JveC1iYWNrZ3JvdW5kLFxuLmZidCAubWF0LWNoZWNrYm94LWluZGV0ZXJtaW5hdGUubWF0LWFjY2VudCAubWF0LWNoZWNrYm94LWJhY2tncm91bmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTZhNGZmO1xufVxuLmZidCAuY2FyZC1ib2R5IHtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwcHg7XG4gIG1pbi1oZWlnaHQ6IDQ1MHB4O1xufVxuLmZidCAuY2FyZC1ib2R5IHRoZWFkIHRoIHtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdmVydGljYWwtYWxpZ246IHRvcCAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJmMmYyO1xuICBvcGFjaXR5OiAxO1xuICBwYWRkaW5nOiAxMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cbi5mYnQgLmNhcmQtYm9keSB0aGVhZCB0aCAuc2VhcmNoLXN0cmluZyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5mYnQgLmNhcmQtYm9keSB0aGVhZCB0aCAuc2VhcmNoLXN0cmluZyAuc2VhcmNoLWZpZWxkIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogN3B4O1xuICB0b3A6IDdweDtcbiAgb3BhY2l0eTogMC4zO1xufVxuLmZidCAuY2FyZC1ib2R5IHRib2R5IHRyOmhvdmVyIHRkIHtcbiAgYmFja2dyb3VuZDogIzNiOWZlMjtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmZidCAuY2FyZC1ib2R5IHRib2R5IHRyLmltYWdlOmhvdmVyIHRkIHtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIGN1cnNvcjogZGVmYXVsdDtcbiAgY29sb3I6IHVuc2V0O1xufVxuLmZidCAuY2FyZC1ib2R5IHRib2R5IHRyIHRkIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZzogMTJweDtcbn1cbi5mYnQgLmNhcmQtYm9keSB0Ym9keSAucGVuZGluZyB7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmU2ZDU7XG4gIGNvbG9yOiAjZWU2ODBjO1xuICB3aWR0aDogODBweDtcbiAgaGVpZ2h0OiAzNXB4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5mYnQgLmNhcmQtYm9keSB0Ym9keSAuc3VjY2VzcyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkYWY5ZjE7XG4gIGNvbG9yOiAjMmVhNTZlO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICB3aWR0aDogODBweDtcbiAgaGVpZ2h0OiAzNXB4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5mYnQgLmNhcmQtYm9keSB0Ym9keSAuaG9sZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMmU4ZmY7XG4gIGNvbG9yOiAjOGI1YWNmO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICB3aWR0aDogODBweDtcbiAgaGVpZ2h0OiAzNXB4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5mYnQgLmNhcmQtYm9keSB0Ym9keSAucmVmdW5kZWQge1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTRlY2ZmO1xuICBjb2xvcjogIzQyNzFkZjtcbiAgd2lkdGg6IDgwcHg7XG4gIGhlaWdodDogMzVweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBib3JkZXI6IG5vbmU7XG59XG4uZmJ0IC5jYXJkLWJvZHkgdGJvZHkgLnJlamVjdGVkIHtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZjZWNmMDtcbiAgY29sb3I6ICNlYzNiNjA7XG4gIHdpZHRoOiA4MHB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgYm9yZGVyOiBub25lO1xufVxuLmZidCAuY2FyZC1ib2R5IHRib2R5IC5pbWdfZHRfcHJwIHtcbiAgd2lkdGg6IDEyMHB4O1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZzogNXB4O1xuICBoZWlnaHQ6IDEyMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mYnQgLmNhcmQtYm9keSB0Ym9keSAuaW1nX2R0X3BycCAuaW1hZ2Vfd3JhcHBlciB7XG4gIGhlaWdodDogMTIwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uZmJ0IC5jYXJkLWJvZHkgdGJvZHkgLmltZ19kdF9wcnAgaW1nIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xufVxuLmZidCAuc2stZmFkaW5nLWNpcmNsZSB7XG4gIG1hcmdpbjogMTBweCBhdXRvO1xuICB3aWR0aDogNDBweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZmJ0IC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHdpZHRoOiAxNSU7XG4gIGhlaWdodDogMTUlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzMzO1xuICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAtd2Via2l0LWFuaW1hdGlvbjogc2stY2lyY2xlRmFkZURlbGF5IDEuMnMgaW5maW5pdGUgZWFzZS1pbi1vdXQgYm90aDtcbiAgYW5pbWF0aW9uOiBzay1jaXJjbGVGYWRlRGVsYXkgMS4ycyBpbmZpbml0ZSBlYXNlLWluLW91dCBib3RoO1xufVxuLmZidCAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMiB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMzBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgzMGRlZyk7XG59XG4uZmJ0IC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUzIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg2MGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSg2MGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDYwZGVnKTtcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTQge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xufVxuLmZidCAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNSB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTIwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDEyMGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDEyMGRlZyk7XG59XG4uZmJ0IC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU2IHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxNTBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMTUwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTUwZGVnKTtcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTcge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xufVxuLmZidCAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlOCB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMjEwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDIxMGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDIxMGRlZyk7XG59XG4uZmJ0IC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU5IHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgyNDBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMjQwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMjQwZGVnKTtcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTEwIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgyNzBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMjcwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMjcwZGVnKTtcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTExIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzMDBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMzAwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMzAwZGVnKTtcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTEyIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzMzBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMzMwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMzMwZGVnKTtcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTI6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0xLjFzO1xuICBhbmltYXRpb24tZGVsYXk6IC0xLjFzO1xufVxuLmZidCAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMzpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTFzO1xuICBhbmltYXRpb24tZGVsYXk6IC0xcztcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTQ6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjlzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjlzO1xufVxuLmZidCAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNTpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuOHM7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuOHM7XG59XG4uZmJ0IC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU2OmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC43cztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC43cztcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTc6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjZzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjZzO1xufVxuLmZidCAuc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlODpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuNXM7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuNXM7XG59XG4uZmJ0IC5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU5OmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC40cztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC40cztcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTEwOmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC4zcztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4zcztcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTExOmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC4ycztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4ycztcbn1cbi5mYnQgLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTEyOmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC4xcztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4xcztcbn1cbkAtd2Via2l0LWtleWZyYW1lcyBzay1jaXJjbGVGYWRlRGVsYXkge1xuICAwJSwgMzklLCAxMDAlIHtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG4gIDQwJSB7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufVxuQGtleWZyYW1lcyBzay1jaXJjbGVGYWRlRGVsYXkge1xuICAwJSwgMzklLCAxMDAlIHtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG4gIDQwJSB7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufVxuXG4uY29sLXhsLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cblxuZW0ge1xuICBmb250LXN0eWxlOiBub3JtYWw7XG59XG5cbkBtZWRpYSBwcmludCB7XG4gIC5wcmludF9jb250YWluZXIge1xuICAgIHdpZHRoOiAyMWNtO1xuICAgIGhlaWdodDogMjkuN2NtO1xuICAgIG1hcmdpbjogMzBtbSA0NW1tIDMwbW0gNDVtbTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAvKiBjaGFuZ2UgdGhlIG1hcmdpbnMgYXMgeW91IHdhbnQgdGhlbSB0byBiZS4gKi9cbiAgfVxuXG4gICNoZWFkZXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICBoNSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIGFwcC1wYWdlLWhlYWRlciB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIGJ1dHRvbiB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gICoge1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgfVxuXG4gIGRpdi50YWJsZS1vcmRlciB7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwLjcpO1xuICAgIHBhZGRpbmctYm90dG9tOiA4MHB4O1xuICAgIHdpZHRoOiAyMGNtO1xuICAgIGhlaWdodDogOWNtO1xuICB9XG5cbiAgZGl2Ll9jb250YWluZXIge1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xuICB9XG5cbiAgLnBhZ2UtbnVtYmVyIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/component-libs/form-builder-table/form-builder-table.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/component-libs/form-builder-table/form-builder-table.component.ts ***!
  \***********************************************************************************/
/*! exports provided: FormBuilderTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormBuilderTableComponent", function() { return FormBuilderTableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_download_download_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/download/download.service */ "./src/app/services/download/download.service.ts");
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! xlsx */ "./node_modules/xlsx/xlsx.js");
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_6__);
// Dear Future Developer, please consider leaving notes on your code, good habits will reduce development time ~ H
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







// import { ConsoleReporter } from 'jasmine';
var FormBuilderTableComponent = /** @class */ (function () {
    function FormBuilderTableComponent(calendar, sanitizer, router, downloadService) {
        this.sanitizer = sanitizer;
        this.router = router;
        this.downloadService = downloadService;
        this.onPageChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.orderBy = [];
        this.form_input = {};
        this.onSearchActive = false;
        this.showLoading = false;
        this.form_detail = [];
        this.edit = false;
        this.list_data = [];
        this.columnCheck = false;
        this.salesRedemp = [];
        this.completeMember = [];
        this.pagesLimiter = [];
        this.pageLimits = 50;
        this.pageNumbering = [];
        this.setupSetPage = false;
        this.currentPage = 1;
        this.checkBox = [];
        this.activeCheckbox = false;
        this.asc = true;
        this.download_temp = {};
        this.showSearch = true;
        this.bast_mode = false;
        this.toggler = {};
        this.order = {};
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
        // this.fromDate = calendar.getToday();
        // this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
        // console.log('HadfI', this.table_row);
        // console.log('HsdI', this.table_row);
        this.pagesLimiter = this.pageLimiter();
    }
    // checkboxFunction(cButton:any){
    //   if(this.checkBox.length ==0) return false;
    //   let getData = [];
    //   this.checkBox.forEach((el,i)=>{
    //       getData.push(this.table_data[i]);
    //   })
    //   cButton.func(getData);
    // }
    FormBuilderTableComponent.prototype.ngOnInit = function () {
        var _this_1 = this;
        if (this.direct_download)
            this.showSearch = false;
        // console.log("hasil tabel", this.table_data)
        if (this.tableFormat != undefined) {
            this.table_header = this.tableFormat.label_headers;
            this.table_title = this.tableFormat.title;
            this.row_id = this.tableFormat.row_primary_key;
            this.options = this.tableFormat.formOptions;
            // console.log('callback',this.searchCallback)
            if (this.total_page) {
                this.buildPagesNumbers(this.total_page);
            }
        }
        if (!this.options) {
            this.options = null;
        }
        if (!this.options.type) {
            this.options.type = 'row-text';
        }
        if (!this.options.image_data_property) {
            this.options.image_data_property = 'url';
        }
        this.table_header.forEach(function (a, index) {
            if (typeof _this_1.table_header[index] == 'string') {
                _this_1.table_header[index] = {
                    label: _this_1.table_header[index],
                    type: 'string'
                };
            }
            else {
                if (_this_1.table_header[index].type == 'list' || _this_1.table_header[index].type == 'list-escrow' || _this_1.table_header[index].type == 'list-escrow-status') {
                    _this_1.table_header[index].options.forEach(function (b, index_options) {
                        if (typeof _this_1.table_header[index].options[index_options] == 'string') {
                            _this_1.table_header[index].options[index_options] = {
                                label: _this_1.table_header[index].options[index_options],
                                value: _this_1.table_header[index].options[index_options],
                            };
                        }
                    });
                }
            }
            // this.form_input[a] = '';
        });
        this.showTableHead();
    };
    FormBuilderTableComponent.prototype.showTableHead = function () {
        var _this_1 = this;
        this.table_head = [];
        this.table_header.forEach(function (a, index) {
            if (a.visible != false) {
                if (a.type == 'number' && _this_1.form_input[a.data_row_name] == undefined) {
                    var ob = { option: 'equal' };
                    _this_1.form_input[a.data_row_name] = ob;
                }
                else {
                    _this_1.form_input[a.data_row_name] = '';
                }
                _this_1.table_head.push(a);
            }
        });
        // console.warn("table head", this.table_head)
    };
    FormBuilderTableComponent.prototype.checkFileType = function (text) {
        var splitFileName = text.split('.');
        var extensFile = splitFileName[splitFileName.length - 1];
        var extens = extensFile.toLowerCase();
        return extens == 'pdf' ? 'document' : 'image';
    };
    FormBuilderTableComponent.prototype.openLabel = function () {
        this.router.navigate(['administrator/orderhistoryallhistoryadmin/receipt'], { queryParams: { id: this.detail._id } });
    };
    FormBuilderTableComponent.prototype.getValueofCheckbox = function () {
        var _this_1 = this;
        // console.warn("active checkbox", this.activeCheckbox)
        var getData = [];
        this.checkBox.forEach(function (el, i) {
            var data = _this_1.table_data[i];
            if (!_this_1.activeCheckbox) {
                if (_this_1.options.salesRedemption || _this_1.options.bastReport) {
                }
                else {
                    getData.push(data._id);
                }
                // console.log(this.salesRedemp, this.checkBox)
            }
            if (_this_1.activeCheckbox) {
                console.log("HERE");
            }
            // else{
            //   var index = getData.indexOf(data._id);
            //   if (index !== -1) {
            //     getData.splice(index, 1);
            //     this.salesRedemp.splice(index, 1)
            //   console.log(this.salesRedemp)
            //   }
            // }
        });
        // this.salesRedemp.push(data) 
    };
    FormBuilderTableComponent.prototype.checkedColumn = function () {
        var _this_1 = this;
        setTimeout(function () {
            _this_1.showTableHead();
        }, 500);
    };
    FormBuilderTableComponent.prototype.checkedAll = function () {
        var _this_1 = this;
        setTimeout(function () {
            _this_1.table_data.forEach(function (e, i) {
                if (_this_1.activeCheckbox) {
                    _this_1.checkBox[i] = _this_1.activeCheckbox;
                    console.log('checkbox all', _this_1.checkBox[i]);
                }
                else {
                    _this_1.checkBox = [];
                }
            });
        }, 100);
    };
    /* Date Picker Part */
    FormBuilderTableComponent.prototype.datePickerOnDateSelection = function (date, formName) {
        var _this = this.make_This(formName);
        if (!_this.fromDate && !_this.toDate) {
            _this.fromDate = date;
        }
        else if (_this.fromDate && !_this.toDate && this.datePickerDateAfter(date, _this.fromDate)) {
            _this.toDate = date;
        }
        else {
            _this.toDate = null;
            _this.fromDate = date;
        }
        _this.from = (_this.fromDate != undefined && _this.fromDate != null) ? this.convertToDateString(_this.fromDate) : '';
        _this.to = (_this.toDate != undefined && _this.toDate != null) ? this.convertToDateString(_this.toDate) : '';
    };
    FormBuilderTableComponent.prototype.typeof = function (object) {
        return typeof object;
    };
    FormBuilderTableComponent.prototype.make_This = function (formName) {
        if (typeof formName == 'object') {
            return formName;
        }
        if (this.form_input[formName] == undefined || this.form_input[formName] == '') {
            this.form_input[formName] = {};
        }
        return this.form_input[formName];
    };
    FormBuilderTableComponent.prototype.dateToggler = function (formName) {
        this.toggler[formName] = (typeof this.toggler == undefined) ? true : !this.toggler[formName];
        if (this.toggler[formName] == false && this.form_input[formName] !== '') {
            this.valuechange({}, false, false);
        }
        // console.log(this.form_input);
    };
    FormBuilderTableComponent.prototype.dateClear = function (event, dataInput, deleteInput) {
        this.form_input[dataInput] = '';
        this.valuechange(event, deleteInput);
    };
    FormBuilderTableComponent.prototype.datePickerOnDateIsInside = function (date, formName) {
        var _this = this.make_This(formName);
        return this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.toDate);
    };
    FormBuilderTableComponent.prototype.datePickerOnDateIsRange = function (date, formName) {
        var _this = this.make_This(formName);
        return this.datePickerDateEquals(date, _this.fromDate) || this.datePickerDateEquals(date, _this.toDate) || this.datePickerOnDateIsInside(date, _this) || this.datePickerOnDateIsHovered(date, _this);
    };
    FormBuilderTableComponent.prototype.datePickerOnDateIsHovered = function (date, formName) {
        var _this = this.make_This(formName);
        return _this.fromDate && !_this.toDate && _this.hoveredDate && this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.hoveredDate);
    };
    FormBuilderTableComponent.prototype.datePickerDateAfter = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day);
        if (iToDate.getTime() < iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    FormBuilderTableComponent.prototype.convertToDateString = function (datePrev) {
        return datePrev.year + '-' + datePrev.month + '-' + datePrev.day;
    };
    FormBuilderTableComponent.prototype.datePickerDateEquals = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day);
        if (iToDate.getTime() == iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    FormBuilderTableComponent.prototype.datePickerDateBefore = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day);
        if (iToDate.getTime() > iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    /* end of datepicker component */
    FormBuilderTableComponent.prototype.ngOnChanges = function (changes) {
        // console.log("table data", this.table_data)
        if (this.setupSetPage == false) {
            if (this.total_page > 0) {
                this.buildPagesNumbers(this.total_page);
                // console.log(this.total_page)
                this.setupSetPage = true;
            }
        }
        // console.log('HI', this.total_page);
        // console.log('changes', this.table_row);
    };
    FormBuilderTableComponent.prototype.toggleColoumnCheck = function () {
        this.columnCheck = !this.columnCheck;
    };
    FormBuilderTableComponent.prototype.onChangePage = function (pageNumber) {
        if (pageNumber <= 0) {
            pageNumber = 1;
        }
        if (pageNumber >= this.total_page) {
            pageNumber = this.total_page;
        }
        this.currentPage = pageNumber;
        // console.log(pageNumber, this.currentPage, this.total_page)
        this.onPageChange.emit(this.currentPage);
        this.valuechange({}, false, false);
    };
    FormBuilderTableComponent.prototype.pageLimitChanges = function (event) {
        // console.log('Test limiter', this.pageLimits);
        this.valuechange({}, false, false);
    };
    FormBuilderTableComponent.prototype.pageLimiter = function () {
        return [
            { id: 10, name: '10' },
            { id: 20, name: '20' },
            { id: 50, name: '50' },
            { id: 100, name: '100' }
        ];
    };
    FormBuilderTableComponent.prototype.isCurrentPage = function (tp) {
        if (tp == this.currentPage) {
            return 'current-page';
        }
        else
            return '';
    };
    FormBuilderTableComponent.prototype.pagination = function (c, m) {
        var current = c, last = m, delta = 2, left = current - delta, right = current + delta + 1, range = [], rangeWithDots = [], l;
        for (var i = 1; i <= last; i++) {
            if (i == 1 || i == last || i >= left && i < right) {
                range.push(i);
            }
        }
        for (var _i = 0, range_1 = range; _i < range_1.length; _i++) {
            var i = range_1[_i];
            if (l) {
                if (i - l === 2) {
                    rangeWithDots.push(l + 1);
                }
                else if (i - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(i);
            l = i;
        }
        return rangeWithDots;
    };
    FormBuilderTableComponent.prototype.buildPagesNumbers = function (totalPage) {
        this.pageNumbering = [];
        // let maxNumber = 8;
        this.total_page = totalPage;
        this.pageNumbering = this.pagination(this.currentPage, totalPage);
    };
    FormBuilderTableComponent.prototype.orderChange = function (event, orderBy) {
        // var row_name = orderBy
        if (this.orderBy[orderBy] && this.asc == false) {
            this.orderBy[orderBy].asc = !this.orderBy[orderBy].asc;
            this.asc = true;
            this.order[orderBy] = 1;
        }
        else {
            this.asc = false;
            this.order = [];
            this.orderBy = [];
            this.orderBy[orderBy] = { asc: false };
            this.order[orderBy] = -1;
        }
        this.valuechange(event, orderBy);
    };
    FormBuilderTableComponent.prototype.stringToNumber = function (str) {
        if (typeof str == 'number') {
            str = str + '';
        }
        var s = str.toLowerCase().replace(/[^0-9]/g, '');
        return parseInt(s);
    };
    FormBuilderTableComponent.prototype.valuechange = function (event, input_name, download) {
        var _this_1 = this;
        var searchCallback = this.searchCallback;
        // console.log(searchCallback)
        if (this.onSearchActive == false) {
            this.onSearchActive = true;
            this.showLoading = true;
            var myVar = setTimeout(function () { return __awaiter(_this_1, void 0, void 0, function () {
                var clearFormInput, filter, property, result, srcDownload;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            clearFormInput = JSON.stringify(this.form_input);
                            console.warn("clearFormInput", clearFormInput);
                            clearFormInput = JSON.parse(clearFormInput);
                            console.warn("clearFormInput parsed", clearFormInput);
                            if (clearFormInput.unique_amount) {
                                clearFormInput.unique_amount = this.stringToNumber(clearFormInput.unique_amount);
                            }
                            if (clearFormInput.total_price) {
                                clearFormInput.total_price = this.stringToNumber(clearFormInput.total_price);
                            }
                            if (clearFormInput.po_no) {
                                Object.defineProperty(clearFormInput, 'po_no.value', Object.getOwnPropertyDescriptor(clearFormInput, 'po_no'));
                                delete clearFormInput['po_no'];
                            }
                            if (clearFormInput.member_detail) {
                                Object.defineProperty(clearFormInput, 'member_detail.nama_penerima_gopay', Object.getOwnPropertyDescriptor(clearFormInput, 'member_detail'));
                                delete clearFormInput['member_detail'];
                            }
                            if (!searchCallback) return [3 /*break*/, 2];
                            filter = {
                                search: clearFormInput,
                                order_by: this.order,
                                limit_per_page: this.pageLimits,
                                current_page: this.currentPage,
                                download: false
                            };
                            //  console.log(filter)
                            if (download != undefined && download == true) {
                                // console.log('here1')
                                filter.limit_per_page = 0;
                                filter = __assign({}, filter, { download: false });
                            }
                            for (property in filter.search) {
                                /** check for type string */
                                if (typeof filter.search[property] == 'string' && filter.search[property].trim() == '') {
                                    delete filter.search[property];
                                }
                                /** check for number */
                                if (typeof filter.search[property] == 'object' && filter.search[property].option) {
                                    if (filter.search[property].value == undefined && filter.search[property].from == undefined) {
                                        delete filter.search[property];
                                    }
                                    else if (filter.search[property].value != undefined) {
                                        if (filter.search[property].value.trim() == '') {
                                            delete filter.search[property];
                                        }
                                    }
                                    else if (filter.search[property].from && filter.search[property].from.trim() == '') {
                                        delete filter.search[property];
                                    }
                                }
                            }
                            if (this.direct_download) {
                                if (download != undefined && download == true) {
                                    this.downloadFile(this.direct_download);
                                }
                                this.onSearchActive = false;
                                this.showLoading = false;
                                return [2 /*return*/];
                            }
                            return [4 /*yield*/, searchCallback[0][searchCallback[1]](filter)];
                        case 1:
                            result = _a.sent();
                            if (download != undefined && download == true) {
                                if (result && result.values) {
                                    this.download_temp = result.values;
                                    this.downloadFile(this.download_temp);
                                }
                                else if (!result.values) {
                                    this.download_temp = result;
                                    this.downloadFile(this.download_temp);
                                }
                                else {
                                    this.download_temp = result;
                                    this.downloadFile(this.download_temp);
                                }
                                // console.log('here',this.download_temp)
                            }
                            if (result && filter.download != true && download != true) {
                                if (result && result.total_page) {
                                    this.buildPagesNumbers(result.total_page);
                                }
                                else {
                                    this.buildPagesNumbers(1);
                                }
                                if (searchCallback[2]) {
                                    // console.warn("result", result);
                                    if (result && result.values) {
                                        searchCallback[2][this.options.result_var_name] = result.values;
                                    }
                                    else if (!result.values) {
                                        searchCallback[2][this.options.result_var_name] = result;
                                    }
                                    else {
                                        searchCallback[2][this.options.result_var_name] = result;
                                    }
                                    /* for(let data of this.table_data) {
                                      if(this.list_data.includes(data.member_id)) {
                                        data.selected = true;
                                      }
                                    } */
                                }
                                else {
                                    if (result.values) {
                                        this.table_data = result.values;
                                    }
                                    else {
                                        this.table_data = result;
                                    }
                                    /* for(let data of this.table_data) {
                                      if(this.list_data.includes(data.member_id)) {
                                        data.selected = true;
                                      }
                                    } */
                                }
                            }
                            else if (result && filter.download == true) {
                                if (typeof result.result == 'string') {
                                    srcDownload = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getHost"])() + '.' + '/' + result.result;
                                    window.open(srcDownload, "_blank");
                                    // this.srcDownload = '';
                                    // setTimeout(() => {
                                    //   this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
                                    // }, 200);
                                }
                            }
                            _a.label = 2;
                        case 2:
                            this.onSearchActive = false;
                            this.showLoading = false;
                            return [2 /*return*/];
                    }
                });
            }); }, 2000);
        }
    };
    FormBuilderTableComponent.prototype.submit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.options.submit_function[0][this.options.submit_function[1]](this.list_data)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    FormBuilderTableComponent.prototype.toggleCheck = function (event, member_id) {
        if (event.target.checked) {
            if (!this.list_data.includes(member_id)) {
                this.list_data.push(member_id);
            }
        }
        else if (!event.target.checked) {
            if (this.list_data.includes(member_id)) {
                this.list_data.splice(this.list_data.indexOf(member_id), 1);
            }
        }
    };
    FormBuilderTableComponent.prototype.removeFromList = function (member_id) {
        this.list_data.splice(this.list_data.indexOf(member_id), 1);
        var index = this.table_data.findIndex(function (obj) { return obj.member_id == member_id; });
        if (index > -1) {
            this.table_data[index].selected = false;
        }
    };
    FormBuilderTableComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    FormBuilderTableComponent.prototype.clickedRowTable = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.options.check_box) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.options.detail_function[0][this.options.detail_function[1]](data, rowData)];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    FormBuilderTableComponent.prototype.isString = function (data) {
        return typeof data == 'string';
    };
    FormBuilderTableComponent.prototype.include = function (data) {
        if (data != null) {
            var x = data.toString();
            return x.includes('-');
        }
        else {
            var y = '0';
            return y.includes('-');
        }
    };
    FormBuilderTableComponent.prototype.stripHtml = function (value) {
        if (typeof value == 'string')
            return value.replace(/<.*?>/g, ''); // replace tags
        return value;
    };
    FormBuilderTableComponent.prototype.onClickDownload = function ($event) {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.valuechange($event, false, true)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    FormBuilderTableComponent.prototype.downloadFile = function (data, filename) {
        var _this_1 = this;
        if (filename === void 0) { filename = 'data'; }
        var label = [];
        var key = [];
        this.table_head.forEach(function (a, index) {
            label.push(_this_1.table_head[index].label);
            key.push(_this_1.table_head[index].data_row_name);
        });
        var csvData = this.ConvertToCSV(data, label, key);
        var newCSV = csvData.replace(/"+/g, '');
        var arrayCSV = newCSV.split('\r\n');
        var aoa = [];
        for (var i = 0; i < arrayCSV.length; i++) {
            aoa.push(arrayCSV[i].split(';'));
        }
        var ws = xlsx__WEBPACK_IMPORTED_MODULE_6__["utils"].aoa_to_sheet(aoa);
        var wb = xlsx__WEBPACK_IMPORTED_MODULE_6__["utils"].book_new();
        xlsx__WEBPACK_IMPORTED_MODULE_6__["utils"].book_append_sheet(wb, ws, 'Sheet1');
        var fileName = this.table_title.replace(/\s+/g, '-');
        var date = Date().split(' ');
        var fileDate = '-' + date[2] + '-' + date[1] + '-' + date[3] + '-' + date[4];
        /* save to file */
        xlsx__WEBPACK_IMPORTED_MODULE_6__["writeFile"](wb, fileName + fileDate + '.xlsx');
        // let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
        // let dwldLink = document.createElement("a");
        // let url = URL.createObjectURL(blob);
        // let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        // if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
        //     dwldLink.setAttribute("target", "_blank");
        // }
        // dwldLink.setAttribute("href", url);
        // dwldLink.setAttribute("download", filename + ".csv");
        // dwldLink.style.visibility = "hidden";
        // document.body.appendChild(dwldLink);
        // dwldLink.click();
        // document.body.removeChild(dwldLink);
    };
    FormBuilderTableComponent.prototype.ConvertToCSV = function (objArray, labelHeader, headerList) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
        var row = 'No;';
        for (var index in labelHeader) {
            row += labelHeader[index] + ';';
        }
        row = row.slice(0, -1);
        str += row + '\r\n';
        // 
        for (var i = 0; i < array.length; i++) {
            var line = (i + 1) + '';
            for (var index in headerList) {
                var head = headerList[index];
                // console.warn("HEAD", head)
                var headtype = this.table_head[index].type;
                // console.warn('HEADTYPE',headtype, 'HEAD', head)
                if ((array[i][head]) instanceof Array && (array[i][head]) != null && head == 'products') {
                    var line2 = '';
                    if (headtype == 'productcode') {
                        if ((array[i][head])[0].product_code) {
                            if ((array[i][head]).length == 1) {
                                line2 += (array[i][head])[0].product_code + ' ';
                            }
                            else {
                                for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                    line2 += (array[i][head])[j].product_code + ' , ';
                                }
                            }
                        }
                        if ((array[i][head])[0].item_code) {
                            if ((array[i][head]).length == 1) {
                                line2 += (array[i][head])[0].item_code + ' ';
                            }
                            else {
                                for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                    line2 += (array[i][head])[j].item_code + ' , ';
                                }
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                    }
                    else if (headtype == 'product_code') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_code + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length; j++) {
                                line2 += (array[i][head])[j].product_code + ', ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productweight') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].weight + '(' + (array[i][head])[0].weight * 1000 + 'gram )' + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].weight + '(' + (array[i][head])[0].weight * 1000 + 'gram )' + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productdesc') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].description + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].description + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productname') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].product_name + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'product_name') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + '(' + (array[i][head])[0].quantity + ') ' + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length; j++) {
                                line2 += (array[i][head])[j].product_name + '(' + (array[i][head])[j].quantity + ') ' + ', ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productcat') {
                        if ((array[i][head])[0].categories) {
                            if ((array[i][head]).length == 1) {
                                line2 += (array[i][head])[0].categories + ' ';
                            }
                            else {
                                for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                    line2 += (array[i][head])[j].categories + ' , ';
                                }
                            }
                        }
                        if ((array[i][head])[0].category) {
                            if ((array[i][head]).length == 1) {
                                line2 += (array[i][head])[0].category + ' ';
                            }
                            else {
                                for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                    line2 += (array[i][head])[j].category + ' , ';
                                }
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productmerc') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].merchant_username + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].merchant_username + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productqty') {
                        var qty = 0;
                        if ((array[i][head]).length == 1) {
                            qty += 1;
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                qty += this.stringToNumber((array[i][head])[j].quantity);
                            }
                        }
                        line2 = qty.toString();
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'quantity') {
                        var qty = 0;
                        if ((array[i][head]).length == 1) {
                            qty += (array[i][head])[0].quantity;
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length; j++) {
                                qty += this.stringToNumber((array[i][head])[j].quantity);
                            }
                        }
                        line2 = qty.toString();
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productdim') {
                        // for (let j = 0; j < (array[i][head]).length-1; j++) {      
                        line2 += 'height:' + (array[i][head])[0].dimensions.height + ' length :' + (array[i][head])[0].dimensions.length + ' width:' + (array[i][head])[0].dimensions.width + ', ';
                        // line2 += (array[i][head])[j].product_name +' '+ '(' + (array[i][head])[j].fixed_price + ' ) ' +','
                        // }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productunit') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ' + '(' + (array[i][head])[0].fixed_price + ' )';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].product_name + ' ' + '(' + (array[i][head])[j].fixed_price + ' ) ' + ',';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'producttotal') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ' + '(' + (array[i][head])[0].total_product_price + ' )';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[0].product_name + ' ' + '(' + (array[i][head])[j].total_product_price + ') ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                    }
                    else if (headtype == 'product') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ' + '(' + ' ' + (array[i][head])[0].total_product_price + ' ' + ')' + ' ' + (array[i][head])[0].quantity + ' ' + 'X';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].product_name + ' ' + '(' + ' ' + (array[i][head])[j].total_product_price + ' ' + ')' + ' ' + (array[i][head])[j].quantity + ' ' + 'X';
                            }
                        }
                        line += ';' + ' ' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                    }
                }
                //  Shipping Info
                else if ((array[i][head]) instanceof Array && (array[i][head]) != null && head == 'shipping_info') {
                    var line2 = '';
                    if (headtype == 'on_process_date') {
                        if ((array[i][head])[0].created_date) {
                            if ((array[i][head]).length == 1) {
                                line2 += (array[i][head])[0].created_date + ' ';
                            }
                            else {
                                for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                    line2 += (array[i][head])[j].created_date + ' , ';
                                }
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                    }
                    else if (headtype == 'on_delivery_date') {
                        if ((array[i][head]).length == 1) {
                            // line2 += (array[i][head])[0].product_code + ' '
                            line2 = "-";
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length; j++) {
                                if ((array[i][head])[j].label == "on_delivery") {
                                    line2 += (array[i][head])[j].created_date;
                                }
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        console.warn("line", line);
                        line2 = null;
                    }
                    //  else if(headtype == 'input_awb_date'){
                    //   if ((array[i][head]).length == 1){
                    //     // line2 += (array[i][head])[0].product_code + ' '
                    //     line2 = "-";
                    //   }else {
                    //     for (let j = 0; j < (array[i][head]).length; j++) {   
                    //       if ((array[i][head])[j].label == "on_delivery" && (array[i][head])[j].updated_date) {
                    //         line2 += (array[i][head])[j].updated_date
                    //       } else {
                    //         line2 = "-"
                    //       }         
                    //     }
                    //   }  
                    //   line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                    //   console.warn("line", line)
                    //   line2 = null
                    //  }
                }
                //  start array of member address
                else if ((array[i][head]) instanceof Array && (array[i][head]) != null && head == 'member_address') {
                    var line2 = '';
                    if (headtype == 'penerima_hadiah') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].name + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length; j++) {
                                line2 += (array[i][head])[j].name + ', ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'telp_penerima') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].cell_phone + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length; j++) {
                                line2 += (array[i][head])[j].cell_phone + ', ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productweight') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].weight + '(' + (array[i][head])[0].weight * 1000 + 'gram )' + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].weight + '(' + (array[i][head])[0].weight * 1000 + 'gram )' + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productdesc') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].description + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].description + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productname') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].product_name + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'product_name') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + '(' + (array[i][head])[0].quantity + ') ' + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length; j++) {
                                line2 += (array[i][head])[j].product_name + '(' + (array[i][head])[j].quantity + ') ' + ', ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productcat') {
                        if ((array[i][head])[0].categories) {
                            if ((array[i][head]).length == 1) {
                                line2 += (array[i][head])[0].categories + ' ';
                            }
                            else {
                                for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                    line2 += (array[i][head])[j].categories + ' , ';
                                }
                            }
                        }
                        if ((array[i][head])[0].category) {
                            if ((array[i][head]).length == 1) {
                                line2 += (array[i][head])[0].category + ' ';
                            }
                            else {
                                for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                    line2 += (array[i][head])[j].category + ' , ';
                                }
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productmerc') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].merchant_username + ' ';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].merchant_username + ' , ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productqty') {
                        var qty = 0;
                        if ((array[i][head]).length == 1) {
                            qty += 1;
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                qty += this.stringToNumber((array[i][head])[j].quantity);
                            }
                        }
                        line2 = qty.toString();
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'quantity') {
                        var qty = 0;
                        if ((array[i][head]).length == 1) {
                            qty += (array[i][head])[0].quantity;
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length; j++) {
                                qty += this.stringToNumber((array[i][head])[j].quantity);
                            }
                        }
                        line2 = qty.toString();
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productdim') {
                        // for (let j = 0; j < (array[i][head]).length-1; j++) {      
                        line2 += 'height:' + (array[i][head])[0].dimensions.height + ' length :' + (array[i][head])[0].dimensions.length + ' width:' + (array[i][head])[0].dimensions.width + ', ';
                        // line2 += (array[i][head])[j].product_name +' '+ '(' + (array[i][head])[j].fixed_price + ' ) ' +','
                        // }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'productunit') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ' + '(' + (array[i][head])[0].fixed_price + ' )';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].product_name + ' ' + '(' + (array[i][head])[j].fixed_price + ' ) ' + ',';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                        line2 = null;
                    }
                    else if (headtype == 'producttotal') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ' + '(' + (array[i][head])[0].total_product_price + ' )';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[0].product_name + ' ' + '(' + (array[i][head])[j].total_product_price + ') ';
                            }
                        }
                        line += ';' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                    }
                    else if (headtype == 'product') {
                        if ((array[i][head]).length == 1) {
                            line2 += (array[i][head])[0].product_name + ' ' + '(' + ' ' + (array[i][head])[0].total_product_price + ' ' + ')' + ' ' + (array[i][head])[0].quantity + ' ' + 'X';
                        }
                        else {
                            for (var j = 0; j < (array[i][head]).length - 1; j++) {
                                line2 += (array[i][head])[j].product_name + ' ' + '(' + ' ' + (array[i][head])[j].total_product_price + ' ' + ')' + ' ' + (array[i][head])[j].quantity + ' ' + 'X';
                            }
                        }
                        line += ';' + ' ' + JSON.stringify(line2).replace(/;/g, ' ') + ' ';
                    }
                }
                // end of member address
                else if (head == 'description') {
                    var line2 = void 0;
                    if ((array[i][head])) {
                        line2 = JSON.stringify(array[i][head]).replace(/;/g, ' ');
                    }
                    line += ';' + line2;
                    //start of input form data
                }
                else if (head == 'input_form_data' || head == 'member_detail') {
                    var line2 = void 0;
                    if (headtype == 'form_nama_penerima') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).nama_penerima_gopay;
                        }
                    }
                    else if (headtype == 'form_group') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).description;
                        }
                    }
                    else if (headtype == 'form_cluster') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).cluster;
                        }
                    }
                    else if (headtype == 'form_group_mci') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).description_mci;
                        }
                    }
                    else if (headtype == 'form_nama_toko') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).nama_toko;
                        }
                    }
                    else if (headtype == 'form_nama_distributor') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).nama_distributor;
                        }
                    }
                    else if (headtype == 'form_alamat_toko') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).alamat_toko;
                        }
                    }
                    else if (headtype == 'form_nama_pemilik') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).nama_pemilik;
                        }
                    }
                    else if (headtype == 'form_business_id') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).business_id;
                        }
                    }
                    else if (headtype == 'form_id_pel') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).id_pel;
                        }
                    }
                    else if (headtype == 'form_ktp_pemilik') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).ktp_pemilik;
                        }
                    }
                    else if (headtype == 'form_foto_ktp_pemilik') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).foto_ktp_pemilik;
                        }
                    }
                    else if (headtype == 'form_npwp_pemilik') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).npwp_pemilik;
                        }
                    }
                    else if (headtype == 'form_foto_npwp_pemilik') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).foto_npwp_pemilik;
                        }
                    }
                    else if (headtype == 'form_telp_pemilik') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).telp_pemilik;
                        }
                    }
                    else if (headtype == 'form_wa_pemilik') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).no_wa_pemilik;
                        }
                    }
                    else if (headtype == 'form_email_pelanggan') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).email;
                        }
                    }
                    else if (headtype == 'form_alamat_penerima') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).alamat_rumah;
                        }
                    }
                    else if (headtype == 'form_hadiah_dikuasakan') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).hadiah_dikuasakan;
                        }
                    }
                    else if (headtype == 'form_ktp_penerima') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).ktp_penerima;
                        }
                    }
                    else if (headtype == 'form_foto_ktp_penerima') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).foto_ktp_penerima;
                        }
                    }
                    else if (headtype == 'form_npwp_penerima') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).npwp_penerima;
                        }
                    }
                    else if (headtype == 'form_foto_npwp_penerima') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).foto_npwp_penerima;
                        }
                    }
                    else if (headtype == 'form_wa_penerima') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).no_wa_penerima;
                        }
                    }
                    else if (headtype == 'form_gopay_penerima') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).telp_penerima_gopay;
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                else if (head == 'additional_info') {
                    var line2 = void 0;
                    if (headtype == 'bast') {
                        if ((array[i][head]) && (array[i][head]).bast && (array[i][head]).bast.pic_file_name) {
                            line2 = (array[i][head]).bast.pic_file_name;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'surat_kuasa') {
                        if ((array[i][head]) && (array[i][head]).surat_kuasa && (array[i][head]).surat_kuasa.pic_file_name) {
                            line2 = (array[i][head]).surat_kuasa.pic_file_name;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'bast_date') {
                        if ((array[i][head]) && (array[i][head]).bast && (array[i][head]).bast.created_date) {
                            line2 = (array[i][head]).bast.created_date;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'invoice_no') {
                        if ((array[i][head]) && (array[i][head]).invoice_no) {
                            line2 = (array[i][head]).invoice_no;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'data_complete') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).data_complete;
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                // delivery_detail
                else if (head == 'delivery_detail') {
                    var line2 = void 0;
                    if (headtype == 'awb_number') {
                        if ((array[i][head]) && (array[i][head]).awb_number) {
                            line2 = (array[i][head]).awb_number;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'delivered_date') {
                        if ((array[i][head]) && (array[i][head]).delivered_date) {
                            line2 = (array[i][head]).delivered_date;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'delivery_date') {
                        if ((array[i][head]) && (array[i][head]).delivery_date) {
                            line2 = (array[i][head]).delivery_date;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'last_shipping_info') {
                        if ((array[i][head]) && (array[i][head]).last_shipping_info) {
                            line2 = (array[i][head]).last_shipping_info;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'receiver_name_sap') {
                        if ((array[i][head]) && (array[i][head]).receiver_name) {
                            line2 = (array[i][head]).receiver_name;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'relation_name') {
                        if ((array[i][head]) && (array[i][head]).relation_name) {
                            line2 = (array[i][head]).relation_name;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                //address detail
                else if (head == 'destination') {
                    var line2 = void 0;
                    if (headtype == 'receiver_name') {
                        if ((array[i][head]) && (array[i][head]).name) {
                            line2 = (array[i][head]).name;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'shipping_address') {
                        if ((array[i][head]) && (array[i][head]).address) {
                            line2 = (array[i][head]).address;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'phone_number') {
                        if ((array[i][head]) && (array[i][head]).cell_phone) {
                            line2 = (array[i][head]).cell_phone;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                //  Invoice Number
                else if (head == 'invoice_no') {
                    var line2 = void 0;
                    if (headtype == 'value_invoice') {
                        if ((array[i][head]) && (array[i][head]).value) {
                            line2 = (array[i][head]).value;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    else if (headtype == 'date_invoice') {
                        if ((array[i][head]) && (array[i][head]).created_date) {
                            line2 = (array[i][head]).created_date;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                else if (head == 'reference_no') {
                    var line2 = void 0;
                    if (headtype == 'reference_no_length') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).constructor === Array ? (array[i][head]).length : 0;
                        }
                        else {
                            line2 = 0;
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                else if (head == 'po_no') {
                    var line2 = void 0;
                    if (headtype == 'value_po') {
                        if ((array[i][head]) && (array[i][head]).value) {
                            line2 = (array[i][head]).value;
                        }
                        else {
                            line2 = "-";
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                else if (head == 'autoform') {
                    var line2 = void 0;
                    if (headtype == 'penerima') {
                        if ((array[i][head]) && (array[i][head]).values) {
                            line2 = (array[i][head]).values.nama_penerima_gopay;
                        }
                    }
                    else if (headtype == 'pemilik') {
                        if ((array[i][head]) && (array[i][head]).values) {
                            line2 = (array[i][head]).values.nama_pemilik;
                        }
                    }
                    else if (headtype == 'nama_toko') {
                        if ((array[i][head]) && (array[i][head]).values) {
                            line2 = (array[i][head]).values.nama_toko;
                        }
                    }
                    else if (headtype == 'alamat_kirim') {
                        if ((array[i][head]) && (array[i][head]).values) {
                            line2 = (array[i][head]).values.alamat_rumah;
                        }
                    }
                    else if (headtype == 'id_toko') {
                        if ((array[i][head]) && (array[i][head]).values) {
                            line2 = (array[i][head]).values.id_pel;
                        }
                    }
                    else if (headtype == 'dikuasakan') {
                        if ((array[i][head]) && (array[i][head]).values) {
                            line2 = (array[i][head]).values.hadiah_dikuasakan;
                        }
                    }
                    else if (headtype == 'no_wa') {
                        if ((array[i][head]) && (array[i][head]).values) {
                            line2 = "'" + (array[i][head]).values.no_wa_penerima;
                        }
                    }
                    else if (headtype == 'member_desc') {
                        if ((array[i][head]) && (array[i][head]).values) {
                            line2 = (array[i][head]).values.description;
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                //  end of input form data
                //Points
                else if (head == 'points') {
                    var line2 = void 0;
                    if (headtype == 'point_balance') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).point_balance;
                        }
                    }
                    else if (headtype == 'expire_date') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).expire_date;
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                else if (head == "access_list") {
                    var line2 = "";
                    if (headtype == 'usr_mngt') {
                        for (var key in (array[i][head])) {
                            line2 += key + " : " + (array[i][head])[key] + "  ";
                        }
                        //   if((array[i][head]) && (array[i][head]).usr_mngt){
                        //      line2 += "User Management : " + (array[i][head]).usr_mngt + "  ";
                        //   } 
                        //   if((array[i][head]) && (array[i][head]).dynamix_sbi){
                        //     line2 += "Dynamix SBI : " + (array[i][head]).dynamix_sbi;
                        //  } 
                    }
                    line2 = line2 === undefined ? '-' : line2.replace(/\|/g, ', ');
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                else if (head == "variation") {
                    var line2 = "";
                    if (headtype == 'variation') {
                        if ((array[i][head])) {
                            for (var property in (array[i][head])) {
                                line2 += property + ": " + (array[i][head])[property] + " | ";
                            }
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                else if (head == "dimensions") {
                    var line2 = "";
                    if (headtype == 'dimensions') {
                        if ((array[i][head])) {
                            for (var property in (array[i][head])) {
                                line2 += property + ": " + (array[i][head])[property] + " | ";
                            }
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                else if (head == "images_gallery") {
                    var line2 = void 0;
                    if (headtype == 'images_gallery') {
                        if ((array[i][head])) {
                            line2 = array[i][head][0].base_url + array[i][head][0].pic_file_name;
                        }
                    }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                // IF ROW IS PRODUCTS
                else if (head == 'products' || head == 'product_list') {
                    var line2 = void 0;
                    if (headtype == 'quantity') {
                        if ((array[i][head]) && (array[i][head]).values) {
                            line2 = (array[i][head]).values.quantity;
                        }
                    }
                    else if (headtype == 'product_quantity') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).quantity;
                        }
                    }
                    else if (headtype == 'product_redeem') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).product_name;
                        }
                    }
                    else if (headtype == 'product_code_redeem') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).product_code;
                        }
                    }
                    else if (headtype == 'product_price_redeem') {
                        if ((array[i][head])) {
                            line2 = (array[i][head]).total_product_price;
                        }
                    }
                    // else if(headtype == 'product_code'){
                    //  if((array[i][head]) && (array[i][head]).values){
                    //     line2 = (array[i][head]).values.product_code
                    //  }         
                    // }
                    line2 = line2 === undefined ? '-' : line2;
                    line += ';' + JSON.stringify(line2).replace(/;/g, ' ');
                }
                else {
                    line += ';' + JSON.stringify(array[i][head]);
                }
            }
            str += line + '\r\n';
        }
        return str;
    };
    //  start of input form data
    FormBuilderTableComponent.prototype.form_id_pel = function (params) {
        var form_id_pel = '';
        if (params)
            form_id_pel = params.id_pel;
        return form_id_pel;
    };
    FormBuilderTableComponent.prototype.form_business_id = function (params) {
        var form_business_id = '';
        if (params)
            form_business_id = params.business_id;
        return form_business_id;
    };
    FormBuilderTableComponent.prototype.form_nama_penerima = function (params) {
        var form_nama_penerima = '';
        if (params)
            form_nama_penerima = params.nama_penerima_gopay;
        return form_nama_penerima;
    };
    FormBuilderTableComponent.prototype.form_group = function (params) {
        var form_group = '';
        if (params)
            form_group = params.description;
        return form_group;
    };
    FormBuilderTableComponent.prototype.form_cluster = function (params) {
        var form_cluster = '';
        if (params)
            form_cluster = params.cluster;
        return form_cluster;
    };
    FormBuilderTableComponent.prototype.form_group_mci = function (params) {
        var form_group_mci = '';
        if (params)
            form_group_mci = params.description_mci;
        return form_group_mci;
    };
    FormBuilderTableComponent.prototype.data_complete = function (params) {
        var data_complete = '';
        if (params)
            data_complete = params.data_complete;
        return data_complete;
    };
    FormBuilderTableComponent.prototype.form_nama_toko = function (params) {
        var form_nama_toko = '';
        if (params)
            form_nama_toko = params.nama_toko;
        return form_nama_toko;
    };
    FormBuilderTableComponent.prototype.reference_no_length = function (params) {
        if (params && params.constructor === Array && params.length > 0) {
            var reference_no_length = void 0;
            reference_no_length = params.length;
            return reference_no_length;
        }
        else {
            return 0;
        }
    };
    FormBuilderTableComponent.prototype.form_alamat_toko = function (params) {
        var form_alamat_toko = '';
        if (params)
            form_alamat_toko = params.alamat_toko;
        return form_alamat_toko;
    };
    FormBuilderTableComponent.prototype.form_nama_distributor = function (params) {
        var form_nama_distributor = '';
        if (params)
            form_nama_distributor = params.nama_distributor;
        return form_nama_distributor;
    };
    FormBuilderTableComponent.prototype.form_nama_pemilik = function (params) {
        var form_nama_pemilik = '';
        if (params)
            form_nama_pemilik = params.nama_pemilik;
        return form_nama_pemilik;
    };
    FormBuilderTableComponent.prototype.form_ktp_pemilik = function (params) {
        var form_ktp_pemilik = '';
        if (params)
            form_ktp_pemilik = params.ktp_pemilik;
        return form_ktp_pemilik;
    };
    FormBuilderTableComponent.prototype.form_npwp_pemilik = function (params) {
        var form_npwp_pemilik = '';
        if (params)
            form_npwp_pemilik = params.npwp_pemilik;
        return form_npwp_pemilik;
    };
    FormBuilderTableComponent.prototype.form_foto_ktp_pemilik = function (params) {
        var form_foto_ktp_pemilik = '';
        if (params)
            form_foto_ktp_pemilik = params.foto_ktp_pemilik;
        return form_foto_ktp_pemilik;
    };
    FormBuilderTableComponent.prototype.form_foto_npwp_pemilik = function (params) {
        var form_foto_npwp_pemilik = '';
        if (params)
            form_foto_npwp_pemilik = params.foto_npwp_pemilik;
        return form_foto_npwp_pemilik;
    };
    FormBuilderTableComponent.prototype.form_foto_npwp_penerima = function (params) {
        var form_foto_npwp_penerima = '';
        if (params)
            form_foto_npwp_penerima = params.foto_npwp_penerima;
        return form_foto_npwp_penerima;
    };
    FormBuilderTableComponent.prototype.form_telp_pemilik = function (params) {
        var form_telp_pemilik = '';
        if (params)
            form_telp_pemilik = params.telp_pemilik;
        return form_telp_pemilik;
    };
    FormBuilderTableComponent.prototype.form_wa_pemilik = function (params) {
        var form_wa_pemilik = '';
        if (params)
            form_wa_pemilik = params.no_wa_pemilik;
        return form_wa_pemilik;
    };
    FormBuilderTableComponent.prototype.form_email_pelanggan = function (params) {
        var form_email_pelanggan = '';
        if (params)
            form_email_pelanggan = params.email;
        return form_email_pelanggan;
    };
    FormBuilderTableComponent.prototype.form_alamat_penerima = function (params) {
        var form_alamat_penerima = '';
        if (params)
            form_alamat_penerima = params.alamat_rumah;
        return form_alamat_penerima;
    };
    FormBuilderTableComponent.prototype.form_hadiah_dikuasakan = function (params) {
        var form_hadiah_dikuasakan = '';
        if (params)
            form_hadiah_dikuasakan = params.hadiah_dikuasakan;
        return form_hadiah_dikuasakan;
    };
    FormBuilderTableComponent.prototype.form_ktp_penerima = function (params) {
        var form_ktp_penerima = '';
        if (params)
            form_ktp_penerima = params.ktp_penerima;
        return form_ktp_penerima;
    };
    FormBuilderTableComponent.prototype.form_foto_ktp_penerima = function (params) {
        var form_foto_ktp_penerima = '';
        if (params)
            form_foto_ktp_penerima = params.foto_ktp_penerima;
        return form_foto_ktp_penerima;
    };
    FormBuilderTableComponent.prototype.form_npwp_penerima = function (params) {
        var form_npwp_penerima = '';
        if (params)
            form_npwp_penerima = params.npwp_penerima;
        return form_npwp_penerima;
    };
    FormBuilderTableComponent.prototype.form_wa_penerima = function (params) {
        var form_wa_penerima = '';
        if (params)
            form_wa_penerima = params.no_wa_penerima;
        return form_wa_penerima;
    };
    FormBuilderTableComponent.prototype.form_gopay_penerima = function (params) {
        var form_gopay_penerima = '';
        if (params)
            form_gopay_penerima = params.telp_penerima_gopay;
        return form_gopay_penerima;
    };
    // additional info
    FormBuilderTableComponent.prototype.bast = function (params) {
        var bast = '';
        if (params && params.bast && params.bast.pic_file_name) {
            bast = params.bast.pic_file_name;
        }
        else {
            bast = '-';
        }
        return bast;
    };
    FormBuilderTableComponent.prototype.surat_kuasa = function (params) {
        var surat_kuasa = '';
        if (params && params.surat_kuasa && params.surat_kuasa.pic_file_name) {
            surat_kuasa = params.surat_kuasa.pic_file_name;
        }
        else {
            surat_kuasa = '-';
        }
        return surat_kuasa;
    };
    FormBuilderTableComponent.prototype.bast_date = function (params) {
        var bast_date = '';
        if (params && params.bast && params.bast.created_date) {
            bast_date = params.bast.created_date;
        }
        else {
            bast_date = '-';
        }
        return bast_date;
    };
    FormBuilderTableComponent.prototype.invoice_no = function (params) {
        var invoice_no = '';
        if (params && params.invoice_no) {
            invoice_no = params.invoice_no;
        }
        else {
            invoice_no = '-';
        }
        return invoice_no;
    };
    // delivery detail
    FormBuilderTableComponent.prototype.awb_number = function (params) {
        var awb_number = '';
        if (params && params.awb_number) {
            awb_number = params.awb_number;
        }
        else {
            awb_number = '-';
        }
        return awb_number;
    };
    FormBuilderTableComponent.prototype.delivered_date = function (params) {
        var delivered_date = '';
        if (params && params.delivered_date) {
            delivered_date = params.delivered_date;
        }
        else {
            delivered_date = '-';
        }
        return delivered_date;
    };
    FormBuilderTableComponent.prototype.delivery_date = function (params) {
        var delivery_date = '';
        if (params && params.delivery_date) {
            delivery_date = params.delivery_date;
        }
        else {
            delivery_date = '-';
        }
        return delivery_date;
    };
    FormBuilderTableComponent.prototype.last_shipping_info = function (params) {
        var last_shipping_info = '';
        if (params && params.last_shipping_info) {
            last_shipping_info = params.last_shipping_info;
        }
        else {
            last_shipping_info = '-';
        }
        return last_shipping_info;
    };
    FormBuilderTableComponent.prototype.receiver_name_sap = function (params) {
        var receiver_name_sap = '';
        if (params && params.receiver_name) {
            receiver_name_sap = params.receiver_name;
        }
        else {
            receiver_name_sap = '-';
        }
        return receiver_name_sap;
    };
    FormBuilderTableComponent.prototype.relation_name = function (params) {
        var relation_name = '';
        if (params && params.relation_name) {
            relation_name = params.relation_name;
        }
        else {
            relation_name = '-';
        }
        return relation_name;
    };
    // address detail
    FormBuilderTableComponent.prototype.receiver_name = function (params) {
        var receiver_name = '';
        if (params && params.name) {
            receiver_name = params.name;
        }
        else {
            receiver_name = '-';
        }
        return receiver_name;
    };
    FormBuilderTableComponent.prototype.shipping_address = function (params) {
        var shipping_address = '';
        if (params && params.address) {
            shipping_address = params.address;
        }
        else {
            shipping_address = '-';
        }
        return shipping_address;
    };
    FormBuilderTableComponent.prototype.phone_number = function (params) {
        var phone_number = '';
        if (params && params.cell_phone) {
            phone_number = params.cell_phone;
        }
        else {
            phone_number = '-';
        }
        return phone_number;
    };
    // Inovice Finance New
    FormBuilderTableComponent.prototype.value_invoice = function (params) {
        var value_invoice = '';
        if (params)
            value_invoice = params.value;
        return value_invoice;
    };
    FormBuilderTableComponent.prototype.value_po = function (params) {
        var value_po = '';
        if (params)
            value_po = params.value;
        return value_po;
    };
    FormBuilderTableComponent.prototype.date_invoice = function (params) {
        var date_invoice = '';
        if (params)
            date_invoice = params.created_date;
        return date_invoice;
    };
    //product sales order new 
    FormBuilderTableComponent.prototype.product_quantity = function (params) {
        var product_quantity = '';
        if (params)
            product_quantity = params.quantity;
        return product_quantity;
    };
    FormBuilderTableComponent.prototype.product_redeem = function (params) {
        var product_redeem = '';
        if (params)
            product_redeem = params.product_name;
        return product_redeem;
    };
    FormBuilderTableComponent.prototype.product_code_redeem = function (params) {
        var product_code_redeem = '';
        if (params)
            product_code_redeem = params.product_code;
        return product_code_redeem;
    };
    FormBuilderTableComponent.prototype.product_price_redeem = function (params) {
        var product_price_redeem = '';
        if (params)
            product_price_redeem = params.total_product_price;
        return product_price_redeem;
    };
    FormBuilderTableComponent.prototype.penerima = function (params) {
        //  console.log( " HREEE")
        var penerima = '';
        if (params.values)
            penerima = params.values.nama_penerima_gopay;
        return penerima;
    };
    FormBuilderTableComponent.prototype.pemilik = function (params) {
        //  console.log( " HREEE")
        var pemilik = '';
        if (params.values)
            pemilik = params.values.nama_pemilik;
        return pemilik;
    };
    FormBuilderTableComponent.prototype.nama_toko = function (params) {
        // console.log( " HREEE")
        var penerima = '';
        if (params.values)
            penerima = params.values.nama_toko;
        //  console.log(' pe', penerima)
        return penerima;
    };
    FormBuilderTableComponent.prototype.member_desc = function (params) {
        // console.log( " HREEE")
        var penerima = '';
        if (params.values)
            penerima = params.values.description;
        //  console.log(' pe', penerima)
        return penerima;
    };
    FormBuilderTableComponent.prototype.id_toko = function (params) {
        // console.log( " HREEE")
        var penerima = '';
        if (params.values)
            penerima = params.values.id_pel;
        return penerima;
    };
    FormBuilderTableComponent.prototype.dikuasakan = function (params) {
        var penerima = '';
        if (params.values)
            penerima = params.values.hadiah_dikuasakan;
        return penerima;
    };
    FormBuilderTableComponent.prototype.no_wa = function (params) {
        // console.log( " HREEE")
        var penerima = '';
        if (params.values)
            penerima = params.values.no_wa_penerima;
        return penerima;
    };
    FormBuilderTableComponent.prototype.alamat_kirim = function (params) {
        // console.log( " HREEE")
        var penerima = '';
        if (params.values)
            penerima = params.values.alamat_rumah;
        return penerima;
    };
    FormBuilderTableComponent.prototype.shipping_date = function (params) {
        // console.log( " HREEE")
        var penerima = '';
        if (params.values)
            penerima = params.values.order_date;
        return penerima;
    };
    FormBuilderTableComponent.prototype.product = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product = product + params[i].product_name + ' ' + '(' + ' ' + params[i].total_product_price + ' ' + ')' + ' ' + params[i].quantity + ' ' + 'X' + ',';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productcode = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].product_code) {
                product += params[i].product_code + ',';
            }
            if (params[i].item_code) {
                product += params[i].item_code + ',';
            }
        }
        return product;
    };
    FormBuilderTableComponent.prototype.product_code = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].product_code) {
                product += params[i].product_code + ',';
            }
            if (params[i].item_code) {
                product += params[i].item_code + ',';
            }
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productweight = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += params[i].weight + '(' + params[i].weight * 1000 + 'gram )' + ',';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productdesc = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += params[i].description + ', ';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productname = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += params[i].product_name + ', ';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.product_name = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += params[i].product_name + '(' + params[i].quantity + ') ' + ', ';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productdim = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += 'height:' + params[i].dimensions.height + ' length :' + params[i].dimensions.length + ' width:' + params[i].dimensions.width + ', ';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productcat = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].categories) {
                product += params[i].categories + ', ';
            }
            if (params[i].category) {
                product += params[i].category + ', ';
            }
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productmerc = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += params[i].merchant_username + ',';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productqty = function (params) {
        var product = 0;
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += this.stringToNumber(params[i].quantity);
        }
        return product;
    };
    FormBuilderTableComponent.prototype.quantity = function (params) {
        var product = 0;
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += this.stringToNumber(params[i].quantity);
        }
        return product;
    };
    FormBuilderTableComponent.prototype.productunit = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product += params[i].product_name + '(' + params[i].fixed_price + ')' + ',';
        }
        return product;
    };
    FormBuilderTableComponent.prototype.producttotal = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            product = product + params[i].product_name + ' ' + '(' + ' ' + params[i].total_product_price + ' ' + ')' + ',';
        }
        return product;
    };
    //  points
    FormBuilderTableComponent.prototype.point_balance = function (params) {
        var point_balance = '';
        if (params)
            point_balance = params.point_balance;
        return point_balance;
    };
    FormBuilderTableComponent.prototype.expire_date = function (params) {
        var expire_date = '';
        if (params)
            expire_date = params.expire_date;
        return expire_date;
    };
    // member address
    FormBuilderTableComponent.prototype.penerima_hadiah = function (params) {
        var penerima_hadiah = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].name) {
                penerima_hadiah += params[i].name;
            }
        }
        return penerima_hadiah;
    };
    FormBuilderTableComponent.prototype.telp_penerima = function (params) {
        var telp_penerima = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].name) {
                telp_penerima += params[i].cell_phone;
            }
        }
        return telp_penerima;
    };
    FormBuilderTableComponent.prototype.alamat_penerima = function (params) {
        var alamat_penerima = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].address) {
                alamat_penerima += params[i].address;
            }
        }
        return alamat_penerima;
    };
    FormBuilderTableComponent.prototype.kelurahan_penerima = function (params) {
        var keluarahan_penerima = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].village_name) {
                keluarahan_penerima += params[i].village_name;
            }
        }
        return keluarahan_penerima;
    };
    FormBuilderTableComponent.prototype.kecamatan_penerima = function (params) {
        var kecamatan_penerima = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].district_name) {
                kecamatan_penerima += params[i].district_name;
            }
        }
        return kecamatan_penerima;
    };
    FormBuilderTableComponent.prototype.kota_penerima = function (params) {
        var kota_penerima = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].city_name) {
                kota_penerima += params[i].city_name;
            }
        }
        return kota_penerima;
    };
    FormBuilderTableComponent.prototype.provinsi_penerima = function (params) {
        var provinsi_penerima = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].province_name) {
                provinsi_penerima += params[i].province_name;
            }
        }
        return provinsi_penerima;
    };
    FormBuilderTableComponent.prototype.kode_pos_penerima = function (params) {
        var kode_pos_penerima = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params[i].postal_code) {
                kode_pos_penerima += params[i].postal_code;
            }
        }
        return kode_pos_penerima;
    };
    // end of member address
    // shipping_info
    // on_process_date(params){
    //   let product = ''
    //   for (let i = 0; i < params.length; i++) {
    //     // console.log(' here',params[i]);
    //     if (params[i].label == "on_processing"){
    //       product += params[i].created_date
    //     }    
    //   }
    //   return product
    //  }
    FormBuilderTableComponent.prototype.on_delivery_date = function (params) {
        var product = '';
        for (var i = 0; i < params.length; i++) {
            // console.log(' here',params[i]);
            if (params.length > 1) {
                if (params[i].label == "on_delivery") {
                    product += params[i].created_date;
                }
            }
            else if (params.length == 1) {
                product = "-";
            }
        }
        return product;
    };
    //  input_awb_date(params){
    //   let product = ''
    //   for (let i = 0; i < params.length; i++) {
    //     // console.log(' here',params[i]);
    //     if (params.length > 1) {
    //       if (params[i].label == "on_delivery" && params[i].updated_date){
    //         product += params[i].updated_date
    //       }
    //     } else if (params.length == 1) {
    //       product = "-"
    //     }
    //   }
    //   return product
    //  }
    FormBuilderTableComponent.prototype.autoBast = function () {
        var _this_1 = this;
        // console.log('print BAST',this.checkBox)
        if (this.checkBox.length == 0)
            return;
        this.checkBox.forEach(function (el, i) {
            if (el == true) {
                _this_1.salesRedemp.push(_this_1.table_data[i]);
                console.log('el', _this_1.salesRedemp);
            }
        });
        this.bast_mode = true;
    };
    FormBuilderTableComponent.prototype.setCompleteMember = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.checkBox.forEach(function (el, i) {
                            if (el == true) {
                                _this_1.completeMember.push(_this_1.table_data[i]);
                                console.log('el', _this_1.completeMember);
                            }
                        });
                        if (!(this.completeMember.length > 0)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.options.detail_function[0]['completeSelectedMembers'](this.options.detail_function[0], this.completeMember)];
                    case 1:
                        _a.sent();
                        this.checkBox = [];
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    FormBuilderTableComponent.prototype.approveOrder = function () {
        return __awaiter(this, void 0, void 0, function () {
            var selectedOrder;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        selectedOrder = [];
                        this.checkBox.forEach(function (el, i) {
                            if (el == true) {
                                selectedOrder.push(_this_1.table_data[i].order_id);
                            }
                        });
                        if (!(selectedOrder.length > 0)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.options.detail_function[0]['processSelectedOrders'](this.options.detail_function[0], selectedOrder)];
                    case 1:
                        _a.sent();
                        this.checkBox = [];
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    FormBuilderTableComponent.prototype.removeFromPackingList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var selectedOrder;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        selectedOrder = [];
                        this.checkBox.forEach(function (el, i) {
                            if (el == true) {
                                selectedOrder.push(_this_1.table_data[i].reference_no);
                            }
                        });
                        if (!(selectedOrder.length > 0)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.options.detail_function[0]['processSelectedOrders'](this.options.detail_function[0], selectedOrder)];
                    case 1:
                        _a.sent();
                        this.checkBox = [];
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    FormBuilderTableComponent.prototype.cancelSelectedProduct = function () {
        return __awaiter(this, void 0, void 0, function () {
            var selectedOrder;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        selectedOrder = [];
                        this.checkBox.forEach(function (el, i) {
                            if (el == true) {
                                selectedOrder.push(_this_1.table_data[i].products.sku_code);
                            }
                        });
                        if (!(selectedOrder.length > 0)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.options.detail_function[0]['processSelectedProducts'](this.options.detail_function[0], selectedOrder)];
                    case 1:
                        _a.sent();
                        this.checkBox = [];
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    FormBuilderTableComponent.prototype.usr_mngt = function (params) {
        var usr_mngt = '';
        if (params)
            usr_mngt = params;
        var tableHTML = usr_mngt.replace(/\|/g, ', ');
        return tableHTML;
    };
    FormBuilderTableComponent.prototype.clickToDownloadReport = function () {
        //reportTable ID
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById('reportTable');
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        // Specify file name
        var filename = 'excel_data.xls';
        var downloadLink = document.createElement("a");
        document.body.appendChild(downloadLink);
        if (navigator.msSaveOrOpenBlob) {
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob(blob, filename);
        }
        else {
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
            // Setting the file name
            downloadLink.download = filename;
            //triggering the function
            downloadLink.click();
        }
    };
    FormBuilderTableComponent.prototype.variation = function (params) {
        var color = "";
        if (params) {
            for (var property in params) {
                color += property + ": " + params[property] + "<br>";
            }
        }
        return (color);
    };
    FormBuilderTableComponent.prototype.dimensions = function (params) {
        var color = "";
        if (params) {
            for (var property in params) {
                color += property + ": " + params[property] + "<br>";
            }
        }
        return (color);
    };
    FormBuilderTableComponent.prototype.images_gallery = function (params) {
        var image = "";
        if (params && params[0] && params[0].base_url && params[0].pic_file_name) {
            image = params[0].base_url + params[0].pic_file_name;
        }
        return image;
    };
    FormBuilderTableComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCalendar"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _services_download_download_service__WEBPACK_IMPORTED_MODULE_5__["DownloadService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], FormBuilderTableComponent.prototype, "table_data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "total_page", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "table_row", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "table_header", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "table_footer", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "table_title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "searchCallback", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "row_id", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "options", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "table_detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "image_data_property", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "tableFormat", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "direct_download", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], FormBuilderTableComponent.prototype, "onPageChange", void 0);
    FormBuilderTableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-form-builder-table',
            template: __webpack_require__(/*! raw-loader!./form-builder-table.component.html */ "./node_modules/raw-loader/index.js!./src/app/component-libs/form-builder-table/form-builder-table.component.html"),
            styles: [__webpack_require__(/*! ./form-builder-table.component.scss */ "./src/app/component-libs/form-builder-table/form-builder-table.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCalendar"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _services_download_download_service__WEBPACK_IMPORTED_MODULE_5__["DownloadService"]])
    ], FormBuilderTableComponent);
    return FormBuilderTableComponent;
}());



/***/ }),

/***/ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/component-libs/form-builder-table/form-builder-table.module.ts ***!
  \********************************************************************************/
/*! exports provided: FormBuilderTableModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormBuilderTableModule", function() { return FormBuilderTableModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _form_builder_table_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./form-builder-table.component */ "./src/app/component-libs/form-builder-table/form-builder-table.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _form_input_form_input_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../form-input/form-input.component */ "./src/app/component-libs/form-input/form-input.component.ts");
/* harmony import */ var _form_select_form_select_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../form-select/form-select.component */ "./src/app/component-libs/form-select/form-select.component.ts");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _layout_modules_admin_bast_bast_bast_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../layout/modules/admin-bast/bast/bast.component */ "./src/app/layout/modules/admin-bast/bast/bast.component.ts");
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-pdf-viewer */ "./node_modules/ng2-pdf-viewer/fesm5/ng2-pdf-viewer.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









// import { BastComponent } from '../../layout/modules/admin-stock/bast/bast.component';


var FormBuilderTableModule = /** @class */ (function () {
    function FormBuilderTableModule() {
    }
    FormBuilderTableModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_8__["MatCheckboxModule"],
                ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_10__["PdfViewerModule"],
            ],
            declarations: [
                _form_builder_table_component__WEBPACK_IMPORTED_MODULE_2__["FormBuilderTableComponent"],
                _form_input_form_input_component__WEBPACK_IMPORTED_MODULE_6__["FormInputComponent"],
                _form_select_form_select_component__WEBPACK_IMPORTED_MODULE_7__["FormSelectComponent"], _layout_modules_admin_bast_bast_bast_component__WEBPACK_IMPORTED_MODULE_9__["BastComponent"]
            ],
            exports: [_form_builder_table_component__WEBPACK_IMPORTED_MODULE_2__["FormBuilderTableComponent"], _form_input_form_input_component__WEBPACK_IMPORTED_MODULE_6__["FormInputComponent"], _form_select_form_select_component__WEBPACK_IMPORTED_MODULE_7__["FormSelectComponent"]]
        })
    ], FormBuilderTableModule);
    return FormBuilderTableModule;
}());



/***/ }),

/***/ "./src/app/component-libs/form-detail/form-detail.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/component-libs/form-detail/form-detail.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudC1saWJzL2Zvcm0tZGV0YWlsL2Zvcm0tZGV0YWlsLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/component-libs/form-detail/form-detail.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/component-libs/form-detail/form-detail.component.ts ***!
  \*********************************************************************/
/*! exports provided: FormDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormDetailComponent", function() { return FormDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FormDetailComponent = /** @class */ (function () {
    function FormDetailComponent() {
    }
    FormDetailComponent_1 = FormDetailComponent;
    FormDetailComponent.prototype.whenItChange = function () {
        this.onChange(this.select_value);
    };
    FormDetailComponent.prototype.writeValue = function (value) {
        var _this = this;
        this.data.forEach(function (key, index) {
            if (key.selected) {
                _this.select_value = key.value;
                return true;
            }
        });
        // this.select_value = value;
    };
    FormDetailComponent.prototype.registerOnChange = function (onChange) {
        this.onChange = onChange;
        // todo
    };
    FormDetailComponent.prototype.registerOnTouched = function () { };
    FormDetailComponent.prototype.changePasswordType = function () {
        console.log(this.inputType);
    };
    var FormDetailComponent_1;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormDetailComponent.prototype, "data", void 0);
    FormDetailComponent = FormDetailComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'form-detail',
            template: __webpack_require__(/*! raw-loader!./form-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/component-libs/form-detail/form-detail.component.html"),
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
                    useExisting: FormDetailComponent_1,
                    multi: true }
            ],
            styles: [__webpack_require__(/*! ./form-detail.component.scss */ "./src/app/component-libs/form-detail/form-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FormDetailComponent);
    return FormDetailComponent;
}());



/***/ }),

/***/ "./src/app/component-libs/form-input/form-input.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/component-libs/form-input/form-input.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".red-alert {\n  border: solid 1px red;\n}\n\n.red-error-alert {\n  color: red;\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50LWxpYnMvZm9ybS1pbnB1dC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxjb21wb25lbnQtbGlic1xcZm9ybS1pbnB1dFxcZm9ybS1pbnB1dC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tcG9uZW50LWxpYnMvZm9ybS1pbnB1dC9mb3JtLWlucHV0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtJO0VBQ0kscUJBQUE7QUNKUjs7QURPSTtFQUNJLFVBQUE7RUFDQSxlQUFBO0FDSlIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnQtbGlicy9mb3JtLWlucHV0L2Zvcm0taW5wdXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuICAgIHB7XHJcbiAgICAgICAgLy8gYmFja2dyb3VuZDpibGFjaztcclxuICAgIH1cclxuXHJcbiAgICAucmVkLWFsZXJ0IHtcclxuICAgICAgICBib3JkZXI6IHNvbGlkIDFweCByZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgLnJlZC1lcnJvci1hbGVydCB7XHJcbiAgICAgICAgY29sb3I6IHJlZDtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB9XHJcbiIsIi5yZWQtYWxlcnQge1xuICBib3JkZXI6IHNvbGlkIDFweCByZWQ7XG59XG5cbi5yZWQtZXJyb3ItYWxlcnQge1xuICBjb2xvcjogcmVkO1xuICBmb250LXNpemU6IDEycHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/component-libs/form-input/form-input.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/component-libs/form-input/form-input.component.ts ***!
  \*******************************************************************/
/*! exports provided: FormInputComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormInputComponent", function() { return FormInputComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FormInputComponent = /** @class */ (function () {
    function FormInputComponent() {
    }
    FormInputComponent_1 = FormInputComponent;
    FormInputComponent.prototype.onKeyupMy = function (event) {
        this.onChange(this.text_value);
        if (this.onKeyup)
            this.onKeyup(event);
    };
    FormInputComponent.prototype.writeValue = function (value) {
        if (value)
            this.text_value = value;
        if (this.type == 'password') {
            this.inputType = 'password';
        }
        if (!this.disabled) {
            this.disabled = false;
        }
    };
    FormInputComponent.prototype.registerOnChange = function (onChange) {
        this.onChange = onChange;
    };
    FormInputComponent.prototype.registerOnTouched = function () { };
    FormInputComponent.prototype.changePasswordType = function () {
        if (this.inputType == 'password')
            this.inputType = 'text';
        else
            this.inputType = 'password';
    };
    var FormInputComponent_1;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], FormInputComponent.prototype, "text_value", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], FormInputComponent.prototype, "placeholder", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], FormInputComponent.prototype, "type", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], FormInputComponent.prototype, "disabled", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], FormInputComponent.prototype, "addClass", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormInputComponent.prototype, "alert", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormInputComponent.prototype, "onKeyup", void 0);
    FormInputComponent = FormInputComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'form-input',
            template: __webpack_require__(/*! raw-loader!./form-input.component.html */ "./node_modules/raw-loader/index.js!./src/app/component-libs/form-input/form-input.component.html"),
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
                    useExisting: FormInputComponent_1,
                    multi: true }
            ],
            styles: [__webpack_require__(/*! ./form-input.component.scss */ "./src/app/component-libs/form-input/form-input.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FormInputComponent);
    return FormInputComponent;
}());



/***/ }),

/***/ "./src/app/component-libs/form-select/form-select.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/component-libs/form-select/form-select.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudC1saWJzL2Zvcm0tc2VsZWN0L2Zvcm0tc2VsZWN0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/component-libs/form-select/form-select.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/component-libs/form-select/form-select.component.ts ***!
  \*********************************************************************/
/*! exports provided: FormSelectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormSelectComponent", function() { return FormSelectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FormSelectComponent = /** @class */ (function () {
    function FormSelectComponent() {
    }
    FormSelectComponent_1 = FormSelectComponent;
    FormSelectComponent.prototype.whenItChange = function () {
        this.onChange(this.select_value);
    };
    FormSelectComponent.prototype.writeValue = function (value) {
        var _this = this;
        console.log(this.data);
        if (!this.data) {
            this.data = [];
            return false;
        }
        this.data.forEach(function (key, index) {
            if (key.selected) {
                _this.select_value = key.value;
                return true;
            }
        });
        // this.select_value = value;
    };
    FormSelectComponent.prototype.registerOnChange = function (onChange) {
        this.onChange = onChange;
        // todo
    };
    FormSelectComponent.prototype.registerOnTouched = function () { };
    FormSelectComponent.prototype.changePasswordType = function () {
        console.log(this.inputType);
    };
    var FormSelectComponent_1;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormSelectComponent.prototype, "data", void 0);
    FormSelectComponent = FormSelectComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'form-select',
            template: __webpack_require__(/*! raw-loader!./form-select.component.html */ "./node_modules/raw-loader/index.js!./src/app/component-libs/form-select/form-select.component.html"),
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
                    useExisting: FormSelectComponent_1,
                    multi: true }
            ],
            styles: [__webpack_require__(/*! ./form-select.component.scss */ "./src/app/component-libs/form-select/form-select.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FormSelectComponent);
    return FormSelectComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-bast/bast/bast.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/layout/modules/admin-bast/bast/bast.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n  background-color: white;\n  font-family: \"Cambria (Headings)\";\n  color: #333;\n  text-align: left;\n  font-size: 11pt;\n  margin: 0;\n}\n\nfooter {\n  clear: both;\n  position: relative;\n  height: 200px;\n  margin-top: -200px;\n}\n\n.mat-raised-button.mat-primary {\n  background-color: #2481fb;\n  float: right;\n  padding: 5px 20px;\n  font-family: \"Cambria (Headings)\";\n  margin: 10px 40px;\n  border-radius: 5px;\n}\n\n.mat-raised-button.mat-primary .fa {\n  margin-left: 8px;\n}\n\n._container {\n  background-color: white;\n  margin: 0px 0px 30px 0px;\n  border-radius: 5px;\n  position: relative;\n  text-align: -webkit-center;\n  -webkit-column-break-before: always;\n     -moz-column-break-before: always;\n          break-before: always;\n  padding-top: 10px;\n}\n\n._container table th {\n  padding: 2px;\n}\n\n._container .watermark {\n  position: absolute;\n  font-size: 120px;\n  font-weight: bolder;\n  width: 100%;\n  left: 0;\n  top: 50%;\n  right: 0;\n  color: red;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n}\n\n._container .watermark span {\n  opacity: 0.1;\n  letter-spacing: 100px;\n}\n\n._container .content {\n  position: relative;\n  width: 21cm;\n  height: 30cm;\n  padding: 0;\n  page-break-after: always;\n  page-break-before: always;\n}\n\n._container .content .logo-container {\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n}\n\n._container .content .logo-container img {\n  margin: 20px 0px;\n  width: 120px;\n}\n\n._container .content .body-letter {\n  text-align: left;\n}\n\n._container .content .body-letter table {\n  width: 100%;\n}\n\n._container .content .body-letter .table1 {\n  border: none;\n}\n\n._container .content .body-letter .table1 .first-col {\n  width: 20px;\n}\n\n._container .content .body-letter .table1 tr {\n  vertical-align: top;\n  padding: 0;\n  border: none;\n}\n\n._container .content .body-letter .table1 tr td {\n  padding: 0;\n  border: none;\n}\n\n._container .content .body-letter .table2 th {\n  font-size: 10pt;\n  text-align: center;\n}\n\n._container .content .body-letter .table2 .no {\n  width: 20px;\n}\n\n._container .content .body-letter .table2 td {\n  padding: 5px;\n}\n\n._container .content .body-letter .table3 th {\n  font-size: 10pt;\n}\n\n._container .content .body-letter .table3 td {\n  padding: 5px;\n}\n\n._container .content .body-letter .table3 .no {\n  width: 20px;\n}\n\n._container .content .body-letter .tabel1-1 {\n  font-style: italic;\n}\n\n._container .content .penerima {\n  display: flex;\n  justify-content: flex-end;\n  align-items: center;\n}\n\n._container .content .penerima-pemberi {\n  display: flex;\n  justify-content: space-between;\n  width: 100%;\n}\n\n._container .content .layanan {\n  text-align: center;\n  margin-bottom: 30px;\n}\n\n._container .content .page-number {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  text-align: center;\n  font-size: 18px;\n}\n\n._container .content .table-order {\n  height: 100%;\n  width: 14cmcm;\n  font-size: 11px;\n}\n\n._container .content .table-order .order-id-header {\n  width: 70%;\n  table-layout: fixed;\n  border-top: 0;\n  width: 80%;\n}\n\n._container .content .table-order .header {\n  table-layout: auto;\n  border-bottom: 0;\n  width: 20cm;\n}\n\n._container .content .table-order .detail-barang-pengiriman {\n  text-align: left;\n  width: 20cm;\n}\n\n._container .content .table-order .detail-barang-pengiriman .row-eq-height {\n  display: flex;\n}\n\n._container .content .table-order .detail-barang-pengiriman .row {\n  margin-left: 0px;\n  height: 100%;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-barang {\n  border-right: solid black 1px;\n  width: 50%;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-barang .daftar-barang {\n  padding-left: 20px;\n  padding-top: 20px;\n  padding-bottom: 20px;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-barang .catatan {\n  padding-top: 20px;\n  padding-bottom: 20px;\n  font-weight: bold;\n  text-transform: uppercase;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman {\n  width: 50%;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman .jenis-pengiriman {\n  padding-top: 20px;\n  padding-bottom: 20px;\n  padding-left: 20px;\n  font-weight: bold;\n  text-align: left;\n}\n\n._container .content .table-order .detail-barang-pengiriman .detail-pengiriman .total-harga {\n  padding-top: 30px;\n  padding-bottom: 30px;\n  text-align: center;\n  font-weight: bold;\n  padding-left: 0px;\n}\n\n._container .content .table-order .detail-header {\n  width: 20cm;\n  text-align: left;\n  border-top: 0;\n  border-bottom: 0;\n}\n\n._container .content .table-order .detail-header .detail-person {\n  border-right: solid black 1px;\n  padding: 20px;\n  width: 60%;\n}\n\n._container .content .table-order .detail-header .detail-person .receiver {\n  border-bottom: black solid 1px;\n  padding-bottom: 10px;\n  margin-bottom: 15px;\n  margin-top: 15px;\n}\n\n._container .content .table-order .detail-header .detail-order {\n  padding: 20px;\n}\n\n._container .content .table-order .detail-header .detail-order .tlc {\n  font-size: 20px;\n}\n\n._container .content .table-order .detail-header .row {\n  margin-left: 0px;\n}\n\n._container .content .table-order .locard-logo {\n  width: 100px;\n  text-align: center;\n}\n\n._container .content .table-order .locard-logo .logo-avatar {\n  border-radius: 0%;\n  width: 60px;\n}\n\n._container .content .table-order .courier-logo {\n  text-align: center;\n}\n\n._container .content .table-order #courier-logo {\n  text-align: center;\n}\n\n._container .content .table-order #courier-logo .logo-avatar {\n  width: 120px;\n}\n\n._container .content .table-order .awb {\n  text-align: center;\n}\n\n._container .content .table-order .order-id {\n  text-align: center;\n}\n\n._container .content .table-order .order-id p {\n  font-size: 20px;\n  font-weight: bold;\n}\n\n.btn-print {\n  text-align: center;\n  margin: 15px 0px;\n}\n\ndiv._container:last-child {\n  page-break-after: always;\n}\n\ntable {\n  border: 2px solid #333;\n  border-collapse: collapse;\n}\n\ntd,\ntr,\nth {\n  padding: 12px;\n  border: 1px solid #333;\n  width: 185px;\n}\n\nth {\n  background-color: #f0f0f0;\n}\n\nh4,\np {\n  margin: 0px;\n}\n\n#garis {\n  height: 200px;\n  color: black;\n}\n\n#thanks {\n  font-size: 15px;\n  text-align: center;\n  color: #0ea8db;\n  font-weight: bold;\n}\n\n#header {\n  text-align: center;\n  margin-top: 10px;\n}\n\n#non {\n  border: 1px solid black;\n  font-size: 20px;\n  padding: 4px;\n}\n\n#shipping {\n  font-size: 20px;\n}\n\n#order {\n  position: relative;\n  bottom: 10px;\n}\n\n.send {\n  text-align: center;\n}\n\n.sticky {\n  position: fixed;\n  top: 0;\n  width: 100%;\n}\n\n.sticky + #card-transaction {\n  padding-top: 50px;\n}\n\n@media print {\n  body * {\n    visibility: hidden;\n  }\n\n  .content {\n    position: relative;\n    page-break-after: always;\n    border: white solid;\n    page-break-before: always;\n  }\n\n  .watermark span {\n    font-size: 120px;\n    opacity: 0.2;\n  }\n\n  #header {\n    display: none;\n  }\n\n  h5 {\n    display: none;\n  }\n\n  app-page-header {\n    display: none;\n  }\n\n  button {\n    display: none;\n  }\n\n  * {\n    font-size: 13px;\n  }\n\n  div.table-order {\n    -webkit-transform: scale(0.7);\n            transform: scale(0.7);\n    padding-bottom: 80px;\n    width: 20cm;\n    height: 9cm;\n  }\n\n  div._container {\n    padding-top: 0px;\n    page-break-after: always;\n  }\n\n  #section-to-print, #section-to-print * {\n    visibility: visible;\n  }\n\n  #section-to-print {\n    position: absolute;\n    left: 0;\n    top: 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tYmFzdC9iYXN0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcYWRtaW4tYmFzdFxcYmFzdFxcYmFzdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tYmFzdC9iYXN0L2Jhc3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx1QkFBQTtFQUNBLGlDQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7QUNDRjs7QURDQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtBQ0VGOztBREVBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQ0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNDRjs7QURDRTtFQUNFLGdCQUFBO0FDQ0o7O0FER0E7RUFDRSx1QkFBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLDBCQUFBO0VBQ0EsbUNBQUE7S0FBQSxnQ0FBQTtVQUFBLG9CQUFBO0VBQ0EsaUJBQUE7QUNBRjs7QURHSTtFQUNFLFlBQUE7QUNETjs7QURJRTtFQUVBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxPQUFBO0VBQ0UsUUFBQTtFQUNBLFFBQUE7RUFFRixVQUFBO0VBQ0EsaUNBQUE7VUFBQSx5QkFBQTtBQ0pGOztBREtJO0VBQ0UsWUFBQTtFQUNBLHFCQUFBO0FDSE47O0FETUU7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsVUFBQTtFQUNBLHdCQUFBO0VBQ0EseUJBQUE7QUNMSjs7QURPSTtFQUNFLGFBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FDTE47O0FETU07RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUNKUjs7QURRSTtFQUNFLGdCQUFBO0FDTk47O0FET007RUFDRSxXQUFBO0FDTFI7O0FET007RUFDRSxZQUFBO0FDTFI7O0FET1E7RUFDRSxXQUFBO0FDTFY7O0FET1E7RUFDRSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0FDTFY7O0FETVU7RUFDRSxVQUFBO0VBQ0YsWUFBQTtBQ0pWOztBRFNRO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0FDUFY7O0FEU1E7RUFDRSxXQUFBO0FDUFY7O0FEU1E7RUFDRSxZQUFBO0FDUFY7O0FEV1E7RUFDRSxlQUFBO0FDVFY7O0FEV1E7RUFDRSxZQUFBO0FDVFY7O0FEV1E7RUFDRSxXQUFBO0FDVFY7O0FEWU07RUFDRSxrQkFBQTtBQ1ZSOztBRGVJO0VBQ0UsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUNiTjs7QURlSTtFQUNFLGFBQUE7RUFDQSw4QkFBQTtFQUNBLFdBQUE7QUNiTjs7QURnQkk7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0FDZE47O0FEaUJJO0VBQ0Usa0JBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ2ZOOztBRGtCSTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtBQ2hCTjs7QURrQk07RUFDRSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtBQ2hCUjs7QURtQk07RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQ2pCUjs7QURvQk07RUFDRSxnQkFBQTtFQUNBLFdBQUE7QUNsQlI7O0FEb0JRO0VBSUUsYUFBQTtBQ2xCVjs7QURxQlE7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUNuQlY7O0FEc0JRO0VBQ0UsNkJBQUE7RUFDQSxVQUFBO0FDcEJWOztBRHFCVTtFQUNFLGtCQUFBO0VBRUEsaUJBQUE7RUFDQSxvQkFBQTtBQ3BCWjs7QUR1QlU7RUFDRSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtBQ3JCWjs7QUR5QlE7RUFDRSxVQUFBO0FDdkJWOztBRHdCVTtFQUNFLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUN0Qlo7O0FEeUJVO0VBQ0UsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQ3ZCWjs7QUQ0Qk07RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUMxQlI7O0FENEJRO0VBQ0UsNkJBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtBQzFCVjs7QUQyQlU7RUFDRSw4QkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ3pCWjs7QUQ2QlE7RUFDRSxhQUFBO0FDM0JWOztBRDRCVTtFQUNFLGVBQUE7QUMxQlo7O0FEOEJRO0VBQ0UsZ0JBQUE7QUM1QlY7O0FEZ0NNO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0FDOUJSOztBRCtCUTtFQUVFLGlCQUFBO0VBQ0EsV0FBQTtBQzdCVjs7QURpQ007RUFDRSxrQkFBQTtBQy9CUjs7QURrQ007RUFDRSxrQkFBQTtBQ2hDUjs7QURrQ1E7RUFDRSxZQUFBO0FDaENWOztBRG9DTTtFQUNFLGtCQUFBO0FDbENSOztBRHFDTTtFQUNFLGtCQUFBO0FDbkNSOztBRG9DUTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQ2xDVjs7QUR5Q0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FDdENGOztBRHlDQTtFQUNFLHdCQUFBO0FDdENGOztBRHdDQTtFQUNFLHNCQUFBO0VBQ0EseUJBQUE7QUNyQ0Y7O0FEdUNBOzs7RUFHRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0FDcENGOztBRHNDQTtFQUNFLHlCQUFBO0FDbkNGOztBRHFDQTs7RUFFRSxXQUFBO0FDbENGOztBRHFDQTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FDbENGOztBRHFDQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQ2xDRjs7QURxQ0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FDbENGOztBRHFDQTtFQUNFLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNsQ0Y7O0FEcUNBO0VBQ0UsZUFBQTtBQ2xDRjs7QURxQ0E7RUFDRSxrQkFBQTtFQUNBLFlBQUE7QUNsQ0Y7O0FEcUNBO0VBQ0Usa0JBQUE7QUNsQ0Y7O0FEcUNBO0VBQ0UsZUFBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0FDbENGOztBRHFDQTtFQUNFLGlCQUFBO0FDbENGOztBRDBKQTtFQUNFO0lBQ0Usa0JBQUE7RUN2SkY7O0VEeUpBO0lBQ0Usa0JBQUE7SUFJQSx3QkFBQTtJQUNBLG1CQUFBO0lBQ0EseUJBQUE7RUN6SkY7O0VEOEpFO0lBQ0UsZ0JBQUE7SUFDQSxZQUFBO0VDM0pKOztFRGdLQTtJQUNFLGFBQUE7RUM3SkY7O0VEZ0tBO0lBQ0UsYUFBQTtFQzdKRjs7RURnS0E7SUFDRSxhQUFBO0VDN0pGOztFRGdLQTtJQUNFLGFBQUE7RUM3SkY7O0VEZ0tBO0lBQ0UsZUFBQTtFQzdKRjs7RURnS0E7SUFDRSw2QkFBQTtZQUFBLHFCQUFBO0lBQ0Esb0JBQUE7SUFDQSxXQUFBO0lBQ0EsV0FBQTtFQzdKRjs7RURnS0E7SUFDRSxnQkFBQTtJQUNBLHdCQUFBO0VDN0pGOztFRCtKQTtJQUNFLG1CQUFBO0VDNUpGOztFRDhKQTtJQUNFLGtCQUFBO0lBQ0EsT0FBQTtJQUNBLE1BQUE7RUMzSkY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLWJhc3QvYmFzdC9iYXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYm9keSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgZm9udC1mYW1pbHk6IFwiQ2FtYnJpYSAoSGVhZGluZ3MpXCI7XHJcbiAgY29sb3I6ICMzMzM7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBmb250LXNpemU6IDExcHQ7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcbmZvb3RlciB7XHJcbiAgY2xlYXI6IGJvdGg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGhlaWdodDogMjAwcHg7XHJcbiAgbWFyZ2luLXRvcDogLTIwMHB4O1xyXG59XHJcblxyXG5cclxuLm1hdC1yYWlzZWQtYnV0dG9uLm1hdC1wcmltYXJ5IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjQ4MWZiO1xyXG4gIGZsb2F0OiByaWdodDtcclxuICBwYWRkaW5nOiA1cHggMjBweDtcclxuICBmb250LWZhbWlseTogXCJDYW1icmlhIChIZWFkaW5ncylcIjtcclxuICBtYXJnaW46IDEwcHggNDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcblxyXG4gIC5mYSB7XHJcbiAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG4gIH1cclxufVxyXG5cclxuLl9jb250YWluZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gIG1hcmdpbjogMHB4IDBweCAzMHB4IDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xyXG4gIGJyZWFrLWJlZm9yZTogYWx3YXlzO1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gIHRhYmxlIHtcclxuICAgXHJcbiAgICB0aCB7XHJcbiAgICAgIHBhZGRpbmc6MnB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAud2F0ZXJtYXJrXHJcbiAge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBmb250LXNpemU6IDEyMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbGVmdDogMDtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiBcclxuICBjb2xvcjpyZWQ7XHJcbiAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTtcclxuICAgIHNwYW57XHJcbiAgICAgIG9wYWNpdHk6IDAuMTtcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IDEwMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29udGVudCB7XHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIHdpZHRoOiAyMWNtO1xyXG4gICAgaGVpZ2h0OiAzMC4wY207XHJcbiAgICAvLyBtYXJnaW46IDMwbW0gNDVtbSAzMG1tIDQ1bW07IFxyXG4gICAgcGFkZGluZzogMDtcclxuICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGFsd2F5cztcclxuICAgIHBhZ2UtYnJlYWstYmVmb3JlOiBhbHdheXM7XHJcbiBcclxuICAgIC5sb2dvLWNvbnRhaW5lciB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgaW1nIHtcclxuICAgICAgICBtYXJnaW46IDIwcHggMHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMjBweDtcclxuICAgICBcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmJvZHktbGV0dGVye1xyXG4gICAgICB0ZXh0LWFsaWduOmxlZnQ7XHJcbiAgICAgIHRhYmxle1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB9XHJcbiAgICAgIC50YWJsZTEge1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgXHJcbiAgICAgICAgLmZpcnN0LWNvbHtcclxuICAgICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0cntcclxuICAgICAgICAgIHZlcnRpY2FsLWFsaWduOnRvcDtcclxuICAgICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICB0ZHtcclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLnRhYmxlMntcclxuICAgICAgICB0aCB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDEwcHQ7XHJcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5ubyB7XHJcbiAgICAgICAgICB3aWR0aDogMjBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGQge1xyXG4gICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAudGFibGUzIHtcclxuICAgICAgICB0aCB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDEwcHQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRkIHtcclxuICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLm5vIHtcclxuICAgICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAudGFiZWwxLTEge1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuXHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgXHJcbiAgICAucGVuZXJpbWEge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgLnBlbmVyaW1hLXBlbWJlcmkge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG5cclxuICAgIC5sYXlhbmFuIHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5wYWdlLW51bWJlciB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgbGVmdDogMDtcclxuICAgICAgYm90dG9tOiAwO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIH1cclxuXHJcbiAgICAudGFibGUtb3JkZXIge1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIHdpZHRoOiAxNGNtY207XHJcbiAgICAgIGZvbnQtc2l6ZTogMTFweDtcclxuXHJcbiAgICAgIC5vcmRlci1pZC1oZWFkZXIge1xyXG4gICAgICAgIHdpZHRoOiA3MCU7XHJcbiAgICAgICAgdGFibGUtbGF5b3V0OiBmaXhlZDtcclxuICAgICAgICBib3JkZXItdG9wOiAwO1xyXG4gICAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5oZWFkZXIge1xyXG4gICAgICAgIHRhYmxlLWxheW91dDogYXV0bztcclxuICAgICAgICBib3JkZXItYm90dG9tOiAwO1xyXG4gICAgICAgIHdpZHRoOiAyMGNtO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgIHdpZHRoOiAyMGNtO1xyXG5cclxuICAgICAgICAucm93LWVxLWhlaWdodCB7XHJcbiAgICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgICAgIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICAgICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5yb3cge1xyXG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5kZXRhaWwtYmFyYW5nIHtcclxuICAgICAgICAgIGJvcmRlci1yaWdodDogc29saWQgYmxhY2sgMXB4O1xyXG4gICAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICAgIC5kYWZ0YXItYmFyYW5nIHtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG4gICAgICAgICAgICAvLyBib3JkZXItcmlnaHQ6IHNvbGlkIGJsYWNrIDFweDtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIC5jYXRhdGFuIHtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5kZXRhaWwtcGVuZ2lyaW1hbiB7XHJcbiAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICAgLmplbmlzLXBlbmdpcmltYW4ge1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMjBweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgLnRvdGFsLWhhcmdhIHtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDMwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAzMHB4O1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5kZXRhaWwtaGVhZGVyIHtcclxuICAgICAgICB3aWR0aDogMjBjbTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgIGJvcmRlci10b3A6IDA7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMDtcclxuXHJcbiAgICAgICAgLmRldGFpbC1wZXJzb24ge1xyXG4gICAgICAgICAgYm9yZGVyLXJpZ2h0OiBzb2xpZCBibGFjayAxcHg7XHJcbiAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgICAgd2lkdGg6IDYwJTtcclxuICAgICAgICAgIC5yZWNlaXZlciB7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IGJsYWNrIHNvbGlkIDFweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuZGV0YWlsLW9yZGVyIHtcclxuICAgICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgICAudGxjIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLnJvdyB7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLmxvY2FyZC1sb2dvIHtcclxuICAgICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIC5sb2dvLWF2YXRhciB7XHJcbiAgICAgICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDAlO1xyXG4gICAgICAgICAgd2lkdGg6IDYwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAuY291cmllci1sb2dvIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgICNjb3VyaWVyLWxvZ28ge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICAgICAgLmxvZ28tYXZhdGFyIHtcclxuICAgICAgICAgIHdpZHRoOiAxMjBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5hd2Ige1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG5cclxuICAgICAgLm9yZGVyLWlkIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgcCB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5idG4tcHJpbnQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW46IDE1cHggMHB4O1xyXG59XHJcblxyXG5kaXYuX2NvbnRhaW5lcjpsYXN0LWNoaWxkIHtcclxuICBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXM7XHJcbn1cclxudGFibGUge1xyXG4gIGJvcmRlcjogMnB4IHNvbGlkICMzMzM7XHJcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxufVxyXG50ZCxcclxudHIsXHJcbnRoIHtcclxuICBwYWRkaW5nOiAxMnB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XHJcbiAgd2lkdGg6IDE4NXB4O1xyXG59XHJcbnRoIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG59XHJcbmg0LFxyXG5wIHtcclxuICBtYXJnaW46IDBweDtcclxufVxyXG5cclxuI2dhcmlzIHtcclxuICBoZWlnaHQ6IDIwMHB4O1xyXG4gIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuI3RoYW5rcyB7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBjb2xvcjogIzBlYThkYjtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuI2hlYWRlciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbiNub24ge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBwYWRkaW5nOiA0cHg7XHJcbn1cclxuXHJcbiNzaGlwcGluZyB7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG59XHJcblxyXG4jb3JkZXIge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5zZW5kIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zdGlja3kge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB0b3A6IDA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5zdGlja3kgKyAjY2FyZC10cmFuc2FjdGlvbiB7XHJcbiAgcGFkZGluZy10b3A6IDUwcHg7XHJcbn1cclxuXHJcbi8vIEBtZWRpYSBwcmludHtcclxuLy8gICBAcGFnZSB7XHJcbi8vICAgICBzaXplOiBBNCBsYW5kc2NhcGU7XHJcbi8vICAgfVxyXG5cclxuLy8gICAjaGVhZGVye1xyXG4vLyAgICAgZGlzcGxheTogbm9uZTtcclxuLy8gICB9XHJcblxyXG4vLyAgIGg1e1xyXG4vLyAgICAgZGlzcGxheTogbm9uZTtcclxuLy8gICB9XHJcblxyXG4vLyAgIGFwcC1wYWdlLWhlYWRlcntcclxuLy8gICAgIGRpc3BsYXk6IG5vbmU7XHJcbi8vICAgfVxyXG5cclxuLy8gICBidXR0b257XHJcbi8vICAgICBkaXNwbGF5OiBub25lO1xyXG4vLyAgIH1cclxuXHJcbi8vICAgZGl2Ll9jb250YWluZXJ7XHJcbi8vICAgICBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXM7XHJcbi8vICAgfVxyXG4vLyB9XHJcblxyXG4vLyBsYWJlbHtcclxuLy8gICBmb250LXNpemU6IDEwcHg7XHJcbi8vIH1cclxuXHJcbi8vIC5fY29udGFpbmVye1xyXG4vLyAgIGJvcmRlcjogc29saWQgYmxhY2sgMXB4O1xyXG4vLyAgIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xyXG4vLyAgIC50YWJsZS1sYWJlbHtcclxuLy8gICAgIHdpZHRoOiAxNGNtO1xyXG4vLyAgICAgaGVpZ2h0OiA5Y207XHJcbi8vICAgICAuc2hpcHBpbmctaW5mb3tcclxuLy8gICAgICAgZGlzcGxheTogZmxleDtcclxuLy8gICAgICAgcGFkZGluZzogNXB4O1xyXG4vLyAgICAgICAubG9nb3tcclxuLy8gICAgICAgICAubG9nby1hdmF0YXJ7XHJcbi8vICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgICAgICB9XHJcbi8vICAgICAgIH1cclxuLy8gICAgICAgLmF3YntcclxuLy8gICAgICAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuLy8gICAgICAgfVxyXG4vLyAgICAgICAub3JkZXItaWR7XHJcbi8vICAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XHJcbi8vICAgICAgIH1cclxuLy8gICAgIH1cclxuLy8gICAgIC5zaGlwcGluZy1kZXRhaWx7XHJcbi8vICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbi8vICAgICAgIHBhZGRpbmc6IDVweDtcclxuLy8gICAgICAgLmRldGFpbHtcclxuLy8gICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbi8vICAgICAgICAgLnNlbmRlci1yZWNlaXZlcntcclxuLy8gICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgICAgICAgICAgLnNlbmRlcntcclxuLy8gICAgICAgICAgICAgbGFiZWx7XHJcbi8vICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgICAgICAgICAgIH1cclxuLy8gICAgICAgICB9XHJcbi8vICAgICAgICAgICAucmVjZWl2ZXJ7XHJcbi8vICAgICAgICAgICAgIGxhYmVse1xyXG4vLyAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4vLyAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgICB9XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgICAgIC5kYWZ0YXItYmFyYW5ne1xyXG5cclxuLy8gICAgICAgICB9XHJcbi8vICAgICAgIH1cclxuLy8gICAgICAgLmRlc3RpbmF0aW9ue1xyXG4vLyAgICAgICAgIC5kZXN0aW5hdGlvbi1kZXRhaWx7XHJcbi8vICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4vLyAgICAgICAgICAgLnRhbmdnYWx7XHJcblxyXG4vLyAgICAgICAgICAgfVxyXG4vLyAgICAgICAgICAgLmtvdGEtYXNhbHtcclxuXHJcbi8vICAgICAgICAgICB9XHJcbi8vICAgICAgICAgICAua290YS10dWp1YW57XHJcblxyXG4vLyAgICAgICAgICAgfVxyXG4vLyAgICAgICAgICAgLmp1bWxhaHtcclxuXHJcbi8vICAgICAgICAgICB9XHJcbi8vICAgICAgICAgICAuYmVyYXQtdG90YWx7XHJcblxyXG4vLyAgICAgICAgICAgfVxyXG4vLyAgICAgICAgIH1cclxuLy8gICAgICAgfVxyXG4vLyAgICAgfVxyXG4vLyAgICAgLnNoaXBwaW5nLW5vdGVze1xyXG4vLyAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgICAgICBwYWRkaW5nOiA1cHg7XHJcbi8vICAgICAgIC50aGFua3lvdS1ub3Rle1xyXG5cclxuLy8gICAgICAgfVxyXG4vLyAgICAgICAuY2F0YXRhbntcclxuXHJcbi8vICAgICAgIH1cclxuLy8gICAgICAgLmRlbGl2ZXJ5LXNlcnZpY2V7XHJcblxyXG4vLyAgICAgICB9XHJcbi8vICAgICB9XHJcbi8vICAgfVxyXG4vLyB9XHJcblxyXG4vLyAjYXdie1xyXG4vLyAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuLy8gfVxyXG4vLyAjb3JkZXItaWR7XHJcbi8vICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4vLyB9XHJcbkBtZWRpYSBwcmludCB7XHJcbiAgYm9keSAqIHtcclxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxuICB9XHJcbiAgLmNvbnRlbnQge1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBcclxuICAgIC8vIG1hcmdpbjogMzBtbSA0NW1tIDMwbW0gNDVtbTsgXHJcbiAgICAvLyBwYWRkaW5nOiAwY20gMWNtO1xyXG4gICAgcGFnZS1icmVhay1hZnRlcjogYWx3YXlzO1xyXG4gICAgYm9yZGVyOndoaXRlIHNvbGlkO1xyXG4gICAgcGFnZS1icmVhay1iZWZvcmU6IGFsd2F5cztcclxuICAgIFxyXG4gIH1cclxuICAud2F0ZXJtYXJrXHJcbiAge1xyXG4gICAgc3BhbiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTIwcHg7XHJcbiAgICAgIG9wYWNpdHk6IDAuMjtcclxuICAgIH0gICBcclxuICBcclxuIFxyXG4gIH1cclxuICAjaGVhZGVyIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBoNSB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgYXBwLXBhZ2UtaGVhZGVyIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBidXR0b24ge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcblxyXG4gICoge1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gIH1cclxuXHJcbiAgZGl2LnRhYmxlLW9yZGVyIHtcclxuICAgIHRyYW5zZm9ybTogc2NhbGUoMC43KTtcclxuICAgIHBhZGRpbmctYm90dG9tOiA4MHB4O1xyXG4gICAgd2lkdGg6IDIwY207XHJcbiAgICBoZWlnaHQ6IDljbTtcclxuICB9XHJcblxyXG4gIGRpdi5fY29udGFpbmVyIHtcclxuICAgIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgICBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXM7XHJcbiAgfVxyXG4gICNzZWN0aW9uLXRvLXByaW50LCAjc2VjdGlvbi10by1wcmludCAqIHtcclxuICAgIHZpc2liaWxpdHk6IHZpc2libGU7XHJcbiAgfVxyXG4gICNzZWN0aW9uLXRvLXByaW50IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0b3A6IDA7XHJcbiAgfVxyXG5cclxufVxyXG5cclxuIiwiYm9keSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBmb250LWZhbWlseTogXCJDYW1icmlhIChIZWFkaW5ncylcIjtcbiAgY29sb3I6ICMzMzM7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtc2l6ZTogMTFwdDtcbiAgbWFyZ2luOiAwO1xufVxuXG5mb290ZXIge1xuICBjbGVhcjogYm90aDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDIwMHB4O1xuICBtYXJnaW4tdG9wOiAtMjAwcHg7XG59XG5cbi5tYXQtcmFpc2VkLWJ1dHRvbi5tYXQtcHJpbWFyeSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNDgxZmI7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZzogNXB4IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiBcIkNhbWJyaWEgKEhlYWRpbmdzKVwiO1xuICBtYXJnaW46IDEwcHggNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLm1hdC1yYWlzZWQtYnV0dG9uLm1hdC1wcmltYXJ5IC5mYSB7XG4gIG1hcmdpbi1sZWZ0OiA4cHg7XG59XG5cbi5fY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIG1hcmdpbjogMHB4IDBweCAzMHB4IDBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xuICBicmVhay1iZWZvcmU6IGFsd2F5cztcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uX2NvbnRhaW5lciB0YWJsZSB0aCB7XG4gIHBhZGRpbmc6IDJweDtcbn1cbi5fY29udGFpbmVyIC53YXRlcm1hcmsge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMTIwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIHdpZHRoOiAxMDAlO1xuICBsZWZ0OiAwO1xuICB0b3A6IDUwJTtcbiAgcmlnaHQ6IDA7XG4gIGNvbG9yOiByZWQ7XG4gIHRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7XG59XG4uX2NvbnRhaW5lciAud2F0ZXJtYXJrIHNwYW4ge1xuICBvcGFjaXR5OiAwLjE7XG4gIGxldHRlci1zcGFjaW5nOiAxMDBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMjFjbTtcbiAgaGVpZ2h0OiAzMGNtO1xuICBwYWRkaW5nOiAwO1xuICBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXM7XG4gIHBhZ2UtYnJlYWstYmVmb3JlOiBhbHdheXM7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAubG9nby1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAubG9nby1jb250YWluZXIgaW1nIHtcbiAgbWFyZ2luOiAyMHB4IDBweDtcbiAgd2lkdGg6IDEyMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLmJvZHktbGV0dGVyIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5ib2R5LWxldHRlciB0YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLmJvZHktbGV0dGVyIC50YWJsZTEge1xuICBib3JkZXI6IG5vbmU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAuYm9keS1sZXR0ZXIgLnRhYmxlMSAuZmlyc3QtY29sIHtcbiAgd2lkdGg6IDIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAuYm9keS1sZXR0ZXIgLnRhYmxlMSB0ciB7XG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gIHBhZGRpbmc6IDA7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5ib2R5LWxldHRlciAudGFibGUxIHRyIHRkIHtcbiAgcGFkZGluZzogMDtcbiAgYm9yZGVyOiBub25lO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLmJvZHktbGV0dGVyIC50YWJsZTIgdGgge1xuICBmb250LXNpemU6IDEwcHQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5ib2R5LWxldHRlciAudGFibGUyIC5ubyB7XG4gIHdpZHRoOiAyMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLmJvZHktbGV0dGVyIC50YWJsZTIgdGQge1xuICBwYWRkaW5nOiA1cHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAuYm9keS1sZXR0ZXIgLnRhYmxlMyB0aCB7XG4gIGZvbnQtc2l6ZTogMTBwdDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5ib2R5LWxldHRlciAudGFibGUzIHRkIHtcbiAgcGFkZGluZzogNXB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLmJvZHktbGV0dGVyIC50YWJsZTMgLm5vIHtcbiAgd2lkdGg6IDIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAuYm9keS1sZXR0ZXIgLnRhYmVsMS0xIHtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnBlbmVyaW1hIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5wZW5lcmltYS1wZW1iZXJpIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB3aWR0aDogMTAwJTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC5sYXlhbmFuIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnBhZ2UtbnVtYmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICBib3R0b206IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTRjbWNtO1xuICBmb250LXNpemU6IDExcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLm9yZGVyLWlkLWhlYWRlciB7XG4gIHdpZHRoOiA3MCU7XG4gIHRhYmxlLWxheW91dDogZml4ZWQ7XG4gIGJvcmRlci10b3A6IDA7XG4gIHdpZHRoOiA4MCU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmhlYWRlciB7XG4gIHRhYmxlLWxheW91dDogYXV0bztcbiAgYm9yZGVyLWJvdHRvbTogMDtcbiAgd2lkdGg6IDIwY207XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIHdpZHRoOiAyMGNtO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLnJvdy1lcS1oZWlnaHQge1xuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5yb3cge1xuICBtYXJnaW4tbGVmdDogMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLWJhcmFuZyB7XG4gIGJvcmRlci1yaWdodDogc29saWQgYmxhY2sgMXB4O1xuICB3aWR0aDogNTAlO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtYmFyYW5nLXBlbmdpcmltYW4gLmRldGFpbC1iYXJhbmcgLmRhZnRhci1iYXJhbmcge1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5kZXRhaWwtYmFyYW5nIC5jYXRhdGFuIHtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWJhcmFuZy1wZW5naXJpbWFuIC5kZXRhaWwtcGVuZ2lyaW1hbiB7XG4gIHdpZHRoOiA1MCU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLXBlbmdpcmltYW4gLmplbmlzLXBlbmdpcmltYW4ge1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gIHBhZGRpbmctbGVmdDogMjBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1iYXJhbmctcGVuZ2lyaW1hbiAuZGV0YWlsLXBlbmdpcmltYW4gLnRvdGFsLWhhcmdhIHtcbiAgcGFkZGluZy10b3A6IDMwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAzMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWhlYWRlciB7XG4gIHdpZHRoOiAyMGNtO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBib3JkZXItdG9wOiAwO1xuICBib3JkZXItYm90dG9tOiAwO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5kZXRhaWwtaGVhZGVyIC5kZXRhaWwtcGVyc29uIHtcbiAgYm9yZGVyLXJpZ2h0OiBzb2xpZCBibGFjayAxcHg7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIHdpZHRoOiA2MCU7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1oZWFkZXIgLmRldGFpbC1wZXJzb24gLnJlY2VpdmVyIHtcbiAgYm9yZGVyLWJvdHRvbTogYmxhY2sgc29saWQgMXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWhlYWRlciAuZGV0YWlsLW9yZGVyIHtcbiAgcGFkZGluZzogMjBweDtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAuZGV0YWlsLWhlYWRlciAuZGV0YWlsLW9yZGVyIC50bGMge1xuICBmb250LXNpemU6IDIwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmRldGFpbC1oZWFkZXIgLnJvdyB7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmxvY2FyZC1sb2dvIHtcbiAgd2lkdGg6IDEwMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmxvY2FyZC1sb2dvIC5sb2dvLWF2YXRhciB7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3JkZXItcmFkaXVzOiAwJTtcbiAgd2lkdGg6IDYwcHg7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLmNvdXJpZXItbG9nbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5fY29udGFpbmVyIC5jb250ZW50IC50YWJsZS1vcmRlciAjY291cmllci1sb2dvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyICNjb3VyaWVyLWxvZ28gLmxvZ28tYXZhdGFyIHtcbiAgd2lkdGg6IDEyMHB4O1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5hd2Ige1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uX2NvbnRhaW5lciAuY29udGVudCAudGFibGUtb3JkZXIgLm9yZGVyLWlkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLl9jb250YWluZXIgLmNvbnRlbnQgLnRhYmxlLW9yZGVyIC5vcmRlci1pZCBwIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmJ0bi1wcmludCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAxNXB4IDBweDtcbn1cblxuZGl2Ll9jb250YWluZXI6bGFzdC1jaGlsZCB7XG4gIHBhZ2UtYnJlYWstYWZ0ZXI6IGFsd2F5cztcbn1cblxudGFibGUge1xuICBib3JkZXI6IDJweCBzb2xpZCAjMzMzO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xufVxuXG50ZCxcbnRyLFxudGgge1xuICBwYWRkaW5nOiAxMnB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjMzMzO1xuICB3aWR0aDogMTg1cHg7XG59XG5cbnRoIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcbn1cblxuaDQsXG5wIHtcbiAgbWFyZ2luOiAwcHg7XG59XG5cbiNnYXJpcyB7XG4gIGhlaWdodDogMjAwcHg7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuI3RoYW5rcyB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzBlYThkYjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbiNoZWFkZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbiNub24ge1xuICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nOiA0cHg7XG59XG5cbiNzaGlwcGluZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuI29yZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3R0b206IDEwcHg7XG59XG5cbi5zZW5kIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uc3RpY2t5IHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uc3RpY2t5ICsgI2NhcmQtdHJhbnNhY3Rpb24ge1xuICBwYWRkaW5nLXRvcDogNTBweDtcbn1cblxuQG1lZGlhIHByaW50IHtcbiAgYm9keSAqIHtcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gIH1cblxuICAuY29udGVudCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGFsd2F5cztcbiAgICBib3JkZXI6IHdoaXRlIHNvbGlkO1xuICAgIHBhZ2UtYnJlYWstYmVmb3JlOiBhbHdheXM7XG4gIH1cblxuICAud2F0ZXJtYXJrIHNwYW4ge1xuICAgIGZvbnQtc2l6ZTogMTIwcHg7XG4gICAgb3BhY2l0eTogMC4yO1xuICB9XG5cbiAgI2hlYWRlciB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIGg1IHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgYXBwLXBhZ2UtaGVhZGVyIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgYnV0dG9uIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgKiB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICB9XG5cbiAgZGl2LnRhYmxlLW9yZGVyIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDAuNyk7XG4gICAgcGFkZGluZy1ib3R0b206IDgwcHg7XG4gICAgd2lkdGg6IDIwY207XG4gICAgaGVpZ2h0OiA5Y207XG4gIH1cblxuICBkaXYuX2NvbnRhaW5lciB7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXM7XG4gIH1cblxuICAjc2VjdGlvbi10by1wcmludCwgI3NlY3Rpb24tdG8tcHJpbnQgKiB7XG4gICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcbiAgfVxuXG4gICNzZWN0aW9uLXRvLXByaW50IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/admin-bast/bast/bast.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/modules/admin-bast/bast/bast.component.ts ***!
  \******************************************************************/
/*! exports provided: BastComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BastComponent", function() { return BastComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var BastComponent = /** @class */ (function () {
    function BastComponent(OrderhistoryService, router, route, memberService) {
        this.OrderhistoryService = OrderhistoryService;
        this.router = router;
        this.route = route;
        this.memberService = memberService;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__;
        this.titleProgram = "";
        this.getData = [];
        this.totalResult = [];
        this.totalWeight = [];
        this.perWeight = [];
        this.endTotalResult = [];
        this.special = [];
        this.loading = false;
        this.same = false;
        this.merchant_data = {};
        this.courier_image_array = [];
        this.sig_project = false;
        this.sig_kontraktual = false;
        this.sig_promotion_2 = false;
        // this.getData = route.snapshot.params['invoiceIds']
        //   .split(',');
    }
    BastComponent.prototype.ngOnInit = function () {
        this.firstload();
        // this.print();
    };
    BastComponent.prototype.firstload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this;
            var _this_1 = this;
            return __generator(this, function (_a) {
                program = localStorage.getItem('programName');
                _this = this;
                this.contentList.filter(function (element) {
                    if (element.appLabel == program) {
                        _this.titleProgram = element.title;
                        if (program && program == "retail_poin_sahabat") {
                            _this.sig_project = true;
                        }
                        else if (program && program == "sig_kontraktual") {
                            _this.sig_kontraktual = true;
                        }
                        else if (program && program == "sig_promotion_2") {
                            _this.sig_promotion_2 = true;
                        }
                    }
                });
                console.warn('this data', this.data);
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this_1, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        this.getData = params.data;
                        this.service = this.OrderhistoryService;
                        return [2 /*return*/];
                    });
                }); });
                this.data.forEach(function (element) {
                    console.log("data", element);
                    if (element.member_detail.hadiah_dikuasakan == 'ya' || element.member_detail.hadiah_dikuasakan == 'KUASA' || element.member_detail.hadiah_dikuasakan == 'true') {
                        _this_1.special.push(element);
                    }
                });
                // window.print();
                this.loading = false;
                return [2 /*return*/];
            });
        });
    };
    BastComponent.prototype.prints = function () {
        window.print();
    };
    BastComponent.prototype.backToTable = function () {
        this.thisParent.checkBox = [];
        this.thisParent.bast_mode = false;
        this.thisParent.salesRedemp = [];
    };
    BastComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BastComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BastComponent.prototype, "thisParent", void 0);
    BastComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bast',
            template: __webpack_require__(/*! raw-loader!./bast.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-bast/bast/bast.component.html"),
            styles: [__webpack_require__(/*! ./bast.component.scss */ "./src/app/layout/modules/admin-bast/bast/bast.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"]])
    ], BastComponent);
    return BastComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-stock.module.ts":
/*!******************************************************!*\
  !*** ./src/app/layout/modules/admin-stock.module.ts ***!
  \******************************************************/
/*! exports provided: AdminStockModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminStockModule", function() { return AdminStockModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// import { AdminPromoUsageComponent } from './admin-promo-usage/admin-promo-usage.component';
// import { AdminEscrowReportComponent } from './admin-escrow-report/admin-escrow-report.component';
var AdminStockModule = /** @class */ (function () {
    function AdminStockModule() {
    }
    AdminStockModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
            ]
        })
    ], AdminStockModule);
    return AdminStockModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin_members/allmember/add/allmember.add.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/allmember/add/allmember.add.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .all-member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .all-member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .all-member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .all-member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .all-member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW5fbWVtYmVycy9hbGxtZW1iZXIvYWRkL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcYWRtaW5fbWVtYmVyc1xcYWxsbWVtYmVyXFxhZGRcXGFsbG1lbWJlci5hZGQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluX21lbWJlcnMvYWxsbWVtYmVyL2FkZC9hbGxtZW1iZXIuYWRkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FERUk7RUFDSSxrQkFBQTtBQ0NSOztBREFRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0NaOztBREFZO0VBQ0ksaUJBQUE7QUNFaEI7O0FEQ1E7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUtBLFdBQUE7QUNKWjs7QURDWTtFQUNJLGlCQUFBO0FDQ2hCOztBRElJO0VBQ0ksYUFBQTtBQ0ZSOztBREdRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0RaOztBRGdCSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDZFI7O0FEaUJRO0VBQ0ksa0JBQUE7QUNmWjs7QURpQlE7RUFDSSxnQkFBQTtBQ2ZaOztBRGlCUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNmWjs7QURrQlE7RUFDSSxzQkFBQTtBQ2hCWjs7QURpQlk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ2ZoQiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluX21lbWJlcnMvYWxsbWVtYmVyL2FkZC9hbGxtZW1iZXIuYWRkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWRlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcbi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuZWRpdF9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gLmltYWdle1xyXG4gICAgLy8gICAgIGgze1xyXG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAvLyAgICAgICAgIGltZ3tcclxuICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLmFsbC1tZW1iZXItZGV0YWlse1xyXG4gICAgICAgIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIuYmctZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5hbGwtbWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLmFsbC1tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAuYWxsLW1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLmFsbC1tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLmFsbC1tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/admin_members/allmember/add/allmember.add.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/allmember/add/allmember.add.component.ts ***!
  \***************************************************************************************/
/*! exports provided: AllMemberAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllMemberAddComponent", function() { return AllMemberAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var AllMemberAddComponent = /** @class */ (function () {
    function AllMemberAddComponent(memberService) {
        this.memberService = memberService;
        this.name = "";
        this.AllMember = [];
        this.errorLabel = false;
        this.member_type = [
            { label: "superuser", value: "superuser" },
            { label: "member", value: "member" },
            { label: "marketing", value: "marketing" },
            { label: "admin", value: "admin" },
            { label: "merchant", value: "merchant" }
        ];
        var form_add = [
            { label: "Full Name", type: "text", value: "", data_binding: 'full_name' },
            { label: "Email", type: "text", value: "", data_binding: 'email' },
            { label: "Password", type: "password", value: "", data_binding: 'password' },
            { label: "PIN", type: "password", value: "", data_binding: 'pin' },
            { label: "Cell Phone", type: "password", value: "", data_binding: 'cell_phone' },
            { label: "Card Number ", type: "text", value: "", data_binding: 'card_number' },
            { label: "Date of Birth", type: "text", value: "", data_binding: 'dob' },
            { label: "City", type: "text", value: "", data_binding: 'mcity' },
            { label: "State", type: "text", value: "", data_binding: 'mstate' },
            { label: "Country", type: "text", value: "", data_binding: 'mcountry' },
            { label: "Address", type: "textarea", value: "", data_binding: 'maddress1' },
            { label: "Post Code", type: "text", value: "", data_binding: 'mpostcode' },
            { label: "Gender", type: "text", value: "", data_binding: 'gender' },
            { label: "Marital Status", type: "text", value: "", data_binding: 'marital_status' },
            { label: "type ", type: "textarea", value: [{ label: "label", value: "label", selected: 1 }, { label: "label1", value: "label2" }], data_binding: 'address2' },
            { label: "address 2 ", type: "textarea", value: "", data_binding: 'address2' },
        ];
    }
    AllMemberAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AllMemberAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    AllMemberAddComponent.prototype.formSubmitAddMember = function (form) {
        // console.log(form);
        // console.log(JSON.stringify(form));
        console.log("pass : ", form.password);
        try {
            this.service = this.memberService;
            var result = this.memberService.addNewMember(form);
            this.AllMember = result.result;
        }
        catch (e) {
            this.errorLabel = (e.message); //conversion to Error type
        }
    };
    AllMemberAddComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] }
    ]; };
    AllMemberAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-all-member-add',
            template: __webpack_require__(/*! raw-loader!./allmember.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin_members/allmember/add/allmember.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./allmember.add.component.scss */ "./src/app/layout/modules/admin_members/allmember/add/allmember.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"]])
    ], AllMemberAddComponent);
    return AllMemberAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin_members/allmember/allmember-routing.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/allmember/allmember-routing.module.ts ***!
  \************************************************************************************/
/*! exports provided: AllMemberRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllMemberRoutingModule", function() { return AllMemberRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _allmember_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./allmember.component */ "./src/app/layout/modules/admin_members/allmember/allmember.component.ts");
/* harmony import */ var _add_allmember_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/allmember.add.component */ "./src/app/layout/modules/admin_members/allmember/add/allmember.add.component.ts");
/* harmony import */ var _detail_allmember_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detail/allmember.detail.component */ "./src/app/layout/modules/admin_members/allmember/detail/allmember.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '', component: _allmember_component__WEBPACK_IMPORTED_MODULE_2__["AllMemberComponent"],
    },
    {
        path: 'add', component: _add_allmember_add_component__WEBPACK_IMPORTED_MODULE_3__["AllMemberAddComponent"]
    },
    {
        path: 'detail', component: _detail_allmember_detail_component__WEBPACK_IMPORTED_MODULE_4__["AllMemberDetailComponent"]
    }
];
var AllMemberRoutingModule = /** @class */ (function () {
    function AllMemberRoutingModule() {
    }
    AllMemberRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AllMemberRoutingModule);
    return AllMemberRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin_members/allmember/allmember.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/allmember/allmember.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "iframe {\n  width: 0px;\n  height: 0px;\n  border: 0px medium none;\n  visibility: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW5fbWVtYmVycy9hbGxtZW1iZXIvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxhZG1pbl9tZW1iZXJzXFxhbGxtZW1iZXJcXGFsbG1lbWJlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW5fbWVtYmVycy9hbGxtZW1iZXIvYWxsbWVtYmVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksVUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbl9tZW1iZXJzL2FsbG1lbWJlci9hbGxtZW1iZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpZnJhbWV7XHJcbiAgICB3aWR0aDowcHg7XHJcbiAgICBoZWlnaHQ6IDBweDtcclxuICAgIGJvcmRlcjowcHggbWVkaXVtIG5vbmU7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbn0iLCJpZnJhbWUge1xuICB3aWR0aDogMHB4O1xuICBoZWlnaHQ6IDBweDtcbiAgYm9yZGVyOiAwcHggbWVkaXVtIG5vbmU7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/admin_members/allmember/allmember.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/allmember/allmember.component.ts ***!
  \*******************************************************************************/
/*! exports provided: AllMemberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllMemberComponent", function() { return AllMemberComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;
var AllMemberComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function AllMemberComponent(memberService, sanitizer) {
        this.memberService = memberService;
        this.sanitizer = sanitizer;
        this.AllMember = [];
        this.tableFormat = {
            title: 'Active All Members Detail',
            label_headers: [
                { label: 'Join At', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'Full Name', visible: true, type: 'string', data_row_name: 'full_name' },
                { label: 'Username', visible: true, type: 'string', data_row_name: 'username' },
                // {label: 'Owner',     visible: true, type: 'string', data_row_name: 'owner'},
                // {label: 'Merchant Code', visible: false, type: 'string', data_row_name: 'merchant_code'},
                { label: 'Email', visible: true, type: 'string', data_row_name: 'email' },
                { label: 'Phone', visible: true, type: 'string', data_row_name: 'cell_phone' },
                // {label: 'State', visible: false, type: 'string', data_row_name: 'state'},
                // {label: 'User ID', visible: false, type: 'string', data_row_name: 'user_id'},
                { label: 'Type', visible: true, type: 'string', data_row_name: 'type' },
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'AllMember',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.allMemberDetail = false;
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    AllMemberComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AllMemberComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getAllMembersLint()];
                    case 1:
                        result = _a.sent();
                        console.log("TEST", result);
                        this.totalPage = result.result.total_page;
                        this.AllMember = result.result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AllMemberComponent.prototype.callDetail = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.detailMember(_id)];
                    case 1:
                        result = _a.sent();
                        this.allMemberDetail = result.result[0];
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AllMemberComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.allMemberDetail = false;
                return [2 /*return*/];
            });
        });
    };
    AllMemberComponent.prototype.onDownload = function () {
        var srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
        console.log(this.srcDownload);
    };
    AllMemberComponent.prototype.getDownloadFileLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        result = void 0;
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getDownloadFileLint()];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        this.AllMember = result.result;
                        console.log(this.AllMember);
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        this.errorLabel = (e_3.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AllMemberComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
    ]; };
    AllMemberComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-all-member',
            template: __webpack_require__(/*! raw-loader!./allmember.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin_members/allmember/allmember.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./allmember.component.scss */ "./src/app/layout/modules/admin_members/allmember/allmember.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
    ], AllMemberComponent);
    return AllMemberComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin_members/allmember/allmember.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/allmember/allmember.module.ts ***!
  \****************************************************************************/
/*! exports provided: AllMemberModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllMemberModule", function() { return AllMemberModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _allmember_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./allmember.component */ "./src/app/layout/modules/admin_members/allmember/allmember.component.ts");
/* harmony import */ var _add_allmember_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/allmember.add.component */ "./src/app/layout/modules/admin_members/allmember/add/allmember.add.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _detail_allmember_detail_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./detail/allmember.detail.component */ "./src/app/layout/modules/admin_members/allmember/detail/allmember.detail.component.ts");
/* harmony import */ var _allmember_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./allmember-routing.module */ "./src/app/layout/modules/admin_members/allmember/allmember-routing.module.ts");
/* harmony import */ var _edit_allmember_edit_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./edit/allmember.edit.component */ "./src/app/layout/modules/admin_members/allmember/edit/allmember.edit.component.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AllMemberModule = /** @class */ (function () {
    function AllMemberModule() {
    }
    AllMemberModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _allmember_routing_module__WEBPACK_IMPORTED_MODULE_9__["AllMemberRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_11__["BsComponentModule"],
            ],
            declarations: [
                _allmember_component__WEBPACK_IMPORTED_MODULE_2__["AllMemberComponent"], _add_allmember_add_component__WEBPACK_IMPORTED_MODULE_3__["AllMemberAddComponent"], _detail_allmember_detail_component__WEBPACK_IMPORTED_MODULE_8__["AllMemberDetailComponent"], _edit_allmember_edit_component__WEBPACK_IMPORTED_MODULE_10__["AllMemberEditComponent"]
            ],
        })
    ], AllMemberModule);
    return AllMemberModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin_members/allmember/detail/allmember.detail.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/allmember/detail/allmember.detail.component.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -25px;\n  margin-left: -5px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 16px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .all-member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .all-member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .all-member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .all-member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .all-member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW5fbWVtYmVycy9hbGxtZW1iZXIvZGV0YWlsL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcYWRtaW5fbWVtYmVyc1xcYWxsbWVtYmVyXFxkZXRhaWxcXGFsbG1lbWJlci5kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluX21lbWJlcnMvYWxsbWVtYmVyL2RldGFpbC9hbGxtZW1iZXIuZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksYUFBQTtBQ0FKOztBREdJO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQ0FSOztBRENRO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0FaOztBRENZO0VBQ0ksaUJBQUE7QUNDaEI7O0FERVE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUtBLFdBQUE7QUNMWjs7QURFWTtFQUNJLGlCQUFBO0FDQWhCOztBREtJO0VBQ0ksYUFBQTtBQ0hSOztBRElRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0ZaOztBRGlCSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDZlI7O0FEa0JRO0VBQ0ksa0JBQUE7QUNoQlo7O0FEa0JRO0VBQ0ksZ0JBQUE7QUNoQlo7O0FEa0JRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2hCWjs7QURtQlE7RUFDSSxzQkFBQTtBQ2pCWjs7QURrQlk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ2hCaEIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbl9tZW1iZXJzL2FsbG1lbWJlci9kZXRhaWwvYWxsbWVtYmVyLmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZy1kZWxldGV7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG4uY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtaW4taGVpZ2h0OiA0OHB4O1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtMjVweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC01cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuZWRpdF9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTZweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gLmltYWdle1xyXG4gICAgLy8gICAgIGgze1xyXG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAvLyAgICAgICAgIGltZ3tcclxuICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLmFsbC1tZW1iZXItZGV0YWlse1xyXG4gICAgICAgIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIuYmctZGVsZXRlIHtcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiA0OHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTI1cHg7XG4gIG1hcmdpbi1sZWZ0OiAtNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxNnB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5hbGwtbWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLmFsbC1tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAuYWxsLW1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLmFsbC1tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLmFsbC1tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/admin_members/allmember/detail/allmember.detail.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/allmember/detail/allmember.detail.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: AllMemberDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllMemberDetailComponent", function() { return AllMemberDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var AllMemberDetailComponent = /** @class */ (function () {
    function AllMemberDetailComponent(memberService) {
        this.memberService = memberService;
        this.edit = false;
        this.errorLabel = false;
    }
    AllMemberDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AllMemberDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    AllMemberDetailComponent.prototype.editThis = function () {
        // console.log(this.edit );
        this.edit = !this.edit;
        // console.log(this.edit );
    };
    AllMemberDetailComponent.prototype.backToTable = function () {
        // console.log(this.back);
        this.back[1](this.back[0]);
    };
    AllMemberDetailComponent.prototype.deleteThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var delResult, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.memberService.deleteMember(this.detail)];
                    case 1:
                        delResult = _a.sent();
                        console.log(delResult);
                        if (delResult.error == false) {
                            console.log(this.back[0]);
                            this.back[0].AllMemberDetail = false;
                            this.back[0].firstLoad();
                            // delete this.back[0].prodDetail;
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        throw new TypeError(error_1.error.error);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AllMemberDetailComponent.prototype.catch = function (e) {
        this.errorLabel = (e.message); //conversion to Error type
    };
    AllMemberDetailComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AllMemberDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AllMemberDetailComponent.prototype, "back", void 0);
    AllMemberDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-all-member-detail',
            template: __webpack_require__(/*! raw-loader!./allmember.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin_members/allmember/detail/allmember.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./allmember.detail.component.scss */ "./src/app/layout/modules/admin_members/allmember/detail/allmember.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"]])
    ], AllMemberDetailComponent);
    return AllMemberDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin_members/allmember/edit/allmember.edit.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/allmember/edit/allmember.edit.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -5px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 16px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .all-member-detail label {\n  margin-top: 10px;\n}\n.card-detail .all-member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .all-member-detail .image {\n  overflow: hidden;\n}\n.card-detail .all-member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .all-member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .all-member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .all-member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .all-member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .all-member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .all-member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW5fbWVtYmVycy9hbGxtZW1iZXIvZWRpdC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGFkbWluX21lbWJlcnNcXGFsbG1lbWJlclxcZWRpdFxcYWxsbWVtYmVyLmVkaXQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluX21lbWJlcnMvYWxsbWVtYmVyL2VkaXQvYWxsbWVtYmVyLmVkaXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FDQVI7QURDUTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNBWjtBRENZO0VBQ0ksaUJBQUE7QUNDaEI7QURFUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBS0EsV0FBQTtBQ0xaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBRElRO0VBQ0kseUJBQUE7QUNGWjtBREtJO0VBQ0ksYUFBQTtBQ0hSO0FES1E7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDSFo7QURPSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDTFI7QURTUTtFQUNJLGdCQUFBO0FDUFo7QURTUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNQWjtBRFNRO0VBQ0ksZ0JBQUE7QUNQWjtBRFFZO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDTmhCO0FEUVk7RUFDSSx5QkFBQTtBQ05oQjtBRFFZO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ05oQjtBRFFZO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ05oQjtBRE9nQjtFQUNJLGVBQUE7RUFDQSxXQUFBO0FDTHBCO0FEV1E7RUFDSSxzQkFBQTtBQ1RaO0FEVVk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ1JoQiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluX21lbWJlcnMvYWxsbWVtYmVyL2VkaXQvYWxsbWVtYmVyLmVkaXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtaW4taGVpZ2h0OiA0OHB4O1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTVweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxNnB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b24udHJ1ZXtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICBcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLmFsbC1tZW1iZXItZGV0YWlse1xyXG4gICAgICAgXHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5pbWFnZXtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgPmRpdntcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzMy4zMzMzJTtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jYXJkLWhlYWRlcntcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaDN7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiA0OHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC01cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDE2cHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5hbGwtbWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLmFsbC1tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5hbGwtbWVtYmVyLWRldGFpbCAuaW1hZ2Uge1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmNhcmQtZGV0YWlsIC5hbGwtbWVtYmVyLWRldGFpbCAuaW1hZ2UgPiBkaXYge1xuICB3aWR0aDogMzMuMzMzMyU7XG4gIHBhZGRpbmc6IDVweDtcbiAgZmxvYXQ6IGxlZnQ7XG59XG4uY2FyZC1kZXRhaWwgLmFsbC1tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xufVxuLmNhcmQtZGV0YWlsIC5hbGwtbWVtYmVyLWRldGFpbCAuaW1hZ2UgaDMge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLmFsbC1tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbn1cbi5jYXJkLWRldGFpbCAuYWxsLW1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQgaW1nIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5jYXJkLWRldGFpbCAuYWxsLW1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAuYWxsLW1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/admin_members/allmember/edit/allmember.edit.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/admin_members/allmember/edit/allmember.edit.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: AllMemberEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllMemberEditComponent", function() { return AllMemberEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var AllMemberEditComponent = /** @class */ (function () {
    function AllMemberEditComponent(memberService) {
        this.memberService = memberService;
        this.errorLabel = false;
        this.loading = false;
        this.activationStatus = [
            { label: 'ACTIVATED', value: 'ACTIVATED' },
            { label: 'INACTIVED', value: 'INACTIVED' }
        ];
        this.memberStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
    }
    AllMemberEditComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AllMemberEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.detail.previous_member_id = this.detail.member_id;
                this.activationStatus.forEach(function (element, index) {
                    if (element.value == _this.detail.activation_status) {
                        _this.activationStatus[index].selected = 1;
                    }
                });
                this.detail.previous_member_id = this.detail.member_id;
                this.memberStatus.forEach(function (element, index) {
                    if (element.value == _this.detail.member_status) {
                        _this.memberStatus[index].selected = 1;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    AllMemberEditComponent.prototype.backToDetail = function () {
        // console.log(this.back);
        this.back[0][this.back[1]]();
    };
    AllMemberEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.loading = !this.loading;
                        return [4 /*yield*/, this.memberService.updateMemberID(this.detail)];
                    case 1:
                        _a.sent();
                        this.loading = !this.loading;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AllMemberEditComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AllMemberEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AllMemberEditComponent.prototype, "back", void 0);
    AllMemberEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-all-member-edit',
            template: __webpack_require__(/*! raw-loader!./allmember.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin_members/allmember/edit/allmember.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./allmember.edit.component.scss */ "./src/app/layout/modules/admin_members/allmember/edit/allmember.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"]])
    ], AllMemberEditComponent);
    return AllMemberEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/bs-component-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/bs-component-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: BsComponentRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BsComponentRoutingModule", function() { return BsComponentRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _bs_component_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bs-component.component */ "./src/app/layout/modules/bs-component/bs-component.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _bs_component_component__WEBPACK_IMPORTED_MODULE_2__["BsComponentComponent"]
    }
];
var BsComponentRoutingModule = /** @class */ (function () {
    function BsComponentRoutingModule() {
    }
    BsComponentRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], BsComponentRoutingModule);
    return BsComponentRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/bs-component.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/bs-component.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9icy1jb21wb25lbnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/bs-component.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/bs-component.component.ts ***!
  \***********************************************************************/
/*! exports provided: BsComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BsComponentComponent", function() { return BsComponentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BsComponentComponent = /** @class */ (function () {
    function BsComponentComponent() {
    }
    BsComponentComponent.prototype.ngOnInit = function () { };
    BsComponentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bs-component',
            template: __webpack_require__(/*! raw-loader!./bs-component.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/bs-component.component.html"),
            styles: [__webpack_require__(/*! ./bs-component.component.scss */ "./src/app/layout/modules/bs-component/bs-component.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BsComponentComponent);
    return BsComponentComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/bs-component.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/bs-component.module.ts ***!
  \********************************************************************/
/*! exports provided: BsComponentModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BsComponentModule", function() { return BsComponentModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _bs_component_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./bs-component-routing.module */ "./src/app/layout/modules/bs-component/bs-component-routing.module.ts");
/* harmony import */ var _bs_component_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bs-component.component */ "./src/app/layout/modules/bs-component/bs-component.component.ts");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components */ "./src/app/layout/modules/bs-component/components/index.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var BsComponentModule = /** @class */ (function () {
    function BsComponentModule() {
    }
    BsComponentModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _bs_component_routing_module__WEBPACK_IMPORTED_MODULE_4__["BsComponentRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModule"],
                _shared__WEBPACK_IMPORTED_MODULE_7__["PageHeaderModule"]
            ],
            declarations: [
                _bs_component_component__WEBPACK_IMPORTED_MODULE_5__["BsComponentComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["ButtonsComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["AlertComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["ModalComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["CollapseComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["DatePickerComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["DropdownComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["PaginationComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["PopOverComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["ProgressbarComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["TabsComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["RatingComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["TooltipComponent"],
                _components__WEBPACK_IMPORTED_MODULE_6__["TimepickerComponent"]
            ],
            exports: [_components__WEBPACK_IMPORTED_MODULE_6__["DatePickerComponent"], _components__WEBPACK_IMPORTED_MODULE_6__["ModalComponent"]]
        })
    ], BsComponentModule);
    return BsComponentModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/alert/alert.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/alert/alert.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL2FsZXJ0L2FsZXJ0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/alert/alert.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/alert/alert.component.ts ***!
  \*********************************************************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return AlertComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AlertComponent = /** @class */ (function () {
    function AlertComponent() {
        this.alerts = [];
        this.alerts.push({
            id: 1,
            type: 'success',
            message: 'This is an success alert',
        }, {
            id: 2,
            type: 'info',
            message: 'This is an info alert',
        }, {
            id: 3,
            type: 'warning',
            message: 'This is a warning alert',
        }, {
            id: 4,
            type: 'danger',
            message: 'This is a danger alert',
        });
    }
    AlertComponent.prototype.ngOnInit = function () { };
    AlertComponent.prototype.closeAlert = function (alert) {
        var index = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    };
    AlertComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-alert',
            template: __webpack_require__(/*! raw-loader!./alert.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/alert/alert.component.html"),
            styles: [__webpack_require__(/*! ./alert.component.scss */ "./src/app/layout/modules/bs-component/components/alert/alert.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/buttons/buttons.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/buttons/buttons.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL2J1dHRvbnMvYnV0dG9ucy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/buttons/buttons.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/buttons/buttons.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ButtonsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonsComponent", function() { return ButtonsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ButtonsComponent = /** @class */ (function () {
    function ButtonsComponent(formBuilder) {
        this.formBuilder = formBuilder;
        this.model = 1;
    }
    ButtonsComponent.prototype.ngOnInit = function () {
        this.radioGroupForm = this.formBuilder.group({
            model: 'middle'
        });
    };
    ButtonsComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }
    ]; };
    ButtonsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-buttons',
            template: __webpack_require__(/*! raw-loader!./buttons.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/buttons/buttons.component.html"),
            styles: [__webpack_require__(/*! ./buttons.component.scss */ "./src/app/layout/modules/bs-component/components/buttons/buttons.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], ButtonsComponent);
    return ButtonsComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/collapse/collapse.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/collapse/collapse.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL2NvbGxhcHNlL2NvbGxhcHNlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/collapse/collapse.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/collapse/collapse.component.ts ***!
  \***************************************************************************************/
/*! exports provided: CollapseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollapseComponent", function() { return CollapseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var CollapseComponent = /** @class */ (function () {
    function CollapseComponent() {
        this.isCollapsed = false;
    }
    CollapseComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-collapse',
            template: __webpack_require__(/*! raw-loader!./collapse.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/collapse/collapse.component.html"),
            styles: [__webpack_require__(/*! ./collapse.component.scss */ "./src/app/layout/modules/bs-component/components/collapse/collapse.component.scss")]
        })
    ], CollapseComponent);
    return CollapseComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/date-picker/date-picker.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/date-picker/date-picker.component.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .datepicker-input .custom-select {\n  width: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYnMtY29tcG9uZW50L2NvbXBvbmVudHMvZGF0ZS1waWNrZXIvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxicy1jb21wb25lbnRcXGNvbXBvbmVudHNcXGRhdGUtcGlja2VyXFxkYXRlLXBpY2tlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYnMtY29tcG9uZW50L2NvbXBvbmVudHMvZGF0ZS1waWNrZXIvZGF0ZS1waWNrZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxVQUFBO0FDQVIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9icy1jb21wb25lbnQvY29tcG9uZW50cy9kYXRlLXBpY2tlci9kYXRlLXBpY2tlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IC5kYXRlcGlja2VyLWlucHV0IHtcclxuICAgIC5jdXN0b20tc2VsZWN0IHtcclxuICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgfVxyXG59IiwiOmhvc3QgLmRhdGVwaWNrZXItaW5wdXQgLmN1c3RvbS1zZWxlY3Qge1xuICB3aWR0aDogNTAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/date-picker/date-picker.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/date-picker/date-picker.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: DatePickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatePickerComponent", function() { return DatePickerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DatePickerComponent = /** @class */ (function () {
    function DatePickerComponent() {
    }
    DatePickerComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], DatePickerComponent.prototype, "label", void 0);
    DatePickerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-date-picker',
            template: __webpack_require__(/*! raw-loader!./date-picker.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/date-picker/date-picker.component.html"),
            styles: [__webpack_require__(/*! ./date-picker.component.scss */ "./src/app/layout/modules/bs-component/components/date-picker/date-picker.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DatePickerComponent);
    return DatePickerComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/dropdown/dropdown.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/dropdown/dropdown.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL2Ryb3Bkb3duL2Ryb3Bkb3duLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/dropdown/dropdown.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/dropdown/dropdown.component.ts ***!
  \***************************************************************************************/
/*! exports provided: DropdownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropdownComponent", function() { return DropdownComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DropdownComponent = /** @class */ (function () {
    function DropdownComponent() {
    }
    DropdownComponent.prototype.ngOnInit = function () {
    };
    DropdownComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dropdown',
            template: __webpack_require__(/*! raw-loader!./dropdown.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/dropdown/dropdown.component.html"),
            styles: [__webpack_require__(/*! ./dropdown.component.scss */ "./src/app/layout/modules/bs-component/components/dropdown/dropdown.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DropdownComponent);
    return DropdownComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/index.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/index.ts ***!
  \*****************************************************************/
/*! exports provided: ButtonsComponent, AlertComponent, ModalComponent, CollapseComponent, DatePickerComponent, DropdownComponent, PaginationComponent, PopOverComponent, ProgressbarComponent, TabsComponent, RatingComponent, TooltipComponent, TimepickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _buttons_buttons_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./buttons/buttons.component */ "./src/app/layout/modules/bs-component/components/buttons/buttons.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ButtonsComponent", function() { return _buttons_buttons_component__WEBPACK_IMPORTED_MODULE_0__["ButtonsComponent"]; });

/* harmony import */ var _alert_alert_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./alert/alert.component */ "./src/app/layout/modules/bs-component/components/alert/alert.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return _alert_alert_component__WEBPACK_IMPORTED_MODULE_1__["AlertComponent"]; });

/* harmony import */ var _modal_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal/modal.component */ "./src/app/layout/modules/bs-component/components/modal/modal.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return _modal_modal_component__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"]; });

/* harmony import */ var _collapse_collapse_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./collapse/collapse.component */ "./src/app/layout/modules/bs-component/components/collapse/collapse.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "CollapseComponent", function() { return _collapse_collapse_component__WEBPACK_IMPORTED_MODULE_3__["CollapseComponent"]; });

/* harmony import */ var _date_picker_date_picker_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./date-picker/date-picker.component */ "./src/app/layout/modules/bs-component/components/date-picker/date-picker.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DatePickerComponent", function() { return _date_picker_date_picker_component__WEBPACK_IMPORTED_MODULE_4__["DatePickerComponent"]; });

/* harmony import */ var _dropdown_dropdown_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dropdown/dropdown.component */ "./src/app/layout/modules/bs-component/components/dropdown/dropdown.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DropdownComponent", function() { return _dropdown_dropdown_component__WEBPACK_IMPORTED_MODULE_5__["DropdownComponent"]; });

/* harmony import */ var _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pagination/pagination.component */ "./src/app/layout/modules/bs-component/components/pagination/pagination.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PaginationComponent", function() { return _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_6__["PaginationComponent"]; });

/* harmony import */ var _pop_over_pop_over_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pop-over/pop-over.component */ "./src/app/layout/modules/bs-component/components/pop-over/pop-over.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PopOverComponent", function() { return _pop_over_pop_over_component__WEBPACK_IMPORTED_MODULE_7__["PopOverComponent"]; });

/* harmony import */ var _progressbar_progressbar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./progressbar/progressbar.component */ "./src/app/layout/modules/bs-component/components/progressbar/progressbar.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ProgressbarComponent", function() { return _progressbar_progressbar_component__WEBPACK_IMPORTED_MODULE_8__["ProgressbarComponent"]; });

/* harmony import */ var _tabs_tabs_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./tabs/tabs.component */ "./src/app/layout/modules/bs-component/components/tabs/tabs.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TabsComponent", function() { return _tabs_tabs_component__WEBPACK_IMPORTED_MODULE_9__["TabsComponent"]; });

/* harmony import */ var _rating_rating_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./rating/rating.component */ "./src/app/layout/modules/bs-component/components/rating/rating.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "RatingComponent", function() { return _rating_rating_component__WEBPACK_IMPORTED_MODULE_10__["RatingComponent"]; });

/* harmony import */ var _tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./tooltip/tooltip.component */ "./src/app/layout/modules/bs-component/components/tooltip/tooltip.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TooltipComponent", function() { return _tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_11__["TooltipComponent"]; });

/* harmony import */ var _timepicker_timepicker_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./timepicker/timepicker.component */ "./src/app/layout/modules/bs-component/components/timepicker/timepicker.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TimepickerComponent", function() { return _timepicker_timepicker_component__WEBPACK_IMPORTED_MODULE_12__["TimepickerComponent"]; });
















/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/modal/modal.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/modal/modal.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL21vZGFsL21vZGFsLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/modal/modal.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/modal/modal.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalComponent = /** @class */ (function () {
    function ModalComponent(modalService) {
        this.modalService = modalService;
    }
    ModalComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    ModalComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    ModalComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModal"] }
    ]; };
    ModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal',
            template: __webpack_require__(/*! raw-loader!./modal.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/modal/modal.component.html"),
            styles: [__webpack_require__(/*! ./modal.component.scss */ "./src/app/layout/modules/bs-component/components/modal/modal.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModal"]])
    ], ModalComponent);
    return ModalComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/pagination/pagination.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/pagination/pagination.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL3BhZ2luYXRpb24vcGFnaW5hdGlvbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/pagination/pagination.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/pagination/pagination.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: PaginationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationComponent", function() { return PaginationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PaginationComponent = /** @class */ (function () {
    function PaginationComponent() {
        this.defaultPagination = 1;
        this.advancedPagination = 1;
        this.paginationSize = 1;
        this.disabledPagination = 1;
        this.isDisabled = true;
    }
    PaginationComponent.prototype.toggleDisabled = function () {
        this.isDisabled = !this.isDisabled;
    };
    PaginationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pagination',
            template: __webpack_require__(/*! raw-loader!./pagination.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/pagination/pagination.component.html"),
            styles: [__webpack_require__(/*! ./pagination.component.scss */ "./src/app/layout/modules/bs-component/components/pagination/pagination.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PaginationComponent);
    return PaginationComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/pop-over/pop-over.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/pop-over/pop-over.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL3BvcC1vdmVyL3BvcC1vdmVyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/pop-over/pop-over.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/pop-over/pop-over.component.ts ***!
  \***************************************************************************************/
/*! exports provided: PopOverComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopOverComponent", function() { return PopOverComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PopOverComponent = /** @class */ (function () {
    function PopOverComponent() {
    }
    PopOverComponent.prototype.ngOnInit = function () {
    };
    PopOverComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pop-over',
            template: __webpack_require__(/*! raw-loader!./pop-over.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/pop-over/pop-over.component.html"),
            styles: [__webpack_require__(/*! ./pop-over.component.scss */ "./src/app/layout/modules/bs-component/components/pop-over/pop-over.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PopOverComponent);
    return PopOverComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/progressbar/progressbar.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/progressbar/progressbar.component.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL3Byb2dyZXNzYmFyL3Byb2dyZXNzYmFyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/progressbar/progressbar.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/progressbar/progressbar.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: ProgressbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgressbarComponent", function() { return ProgressbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProgressbarComponent = /** @class */ (function () {
    function ProgressbarComponent() {
    }
    ProgressbarComponent.prototype.ngOnInit = function () {
    };
    ProgressbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-progressbar',
            template: __webpack_require__(/*! raw-loader!./progressbar.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/progressbar/progressbar.component.html"),
            styles: [__webpack_require__(/*! ./progressbar.component.scss */ "./src/app/layout/modules/bs-component/components/progressbar/progressbar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProgressbarComponent);
    return ProgressbarComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/rating/rating.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/rating/rating.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL3JhdGluZy9yYXRpbmcuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/rating/rating.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/rating/rating.component.ts ***!
  \***********************************************************************************/
/*! exports provided: RatingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingComponent", function() { return RatingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RatingComponent = /** @class */ (function () {
    function RatingComponent() {
        this.currentRate = 8;
    }
    RatingComponent.prototype.ngOnInit = function () {
    };
    RatingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-rating',
            template: __webpack_require__(/*! raw-loader!./rating.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/rating/rating.component.html"),
            styles: [__webpack_require__(/*! ./rating.component.scss */ "./src/app/layout/modules/bs-component/components/rating/rating.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], RatingComponent);
    return RatingComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/tabs/tabs.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/tabs/tabs.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL3RhYnMvdGFicy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/tabs/tabs.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/tabs/tabs.component.ts ***!
  \*******************************************************************************/
/*! exports provided: TabsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsComponent", function() { return TabsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TabsComponent = /** @class */ (function () {
    function TabsComponent() {
    }
    TabsComponent.prototype.ngOnInit = function () {
    };
    TabsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tabs',
            template: __webpack_require__(/*! raw-loader!./tabs.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/tabs/tabs.component.html"),
            styles: [__webpack_require__(/*! ./tabs.component.scss */ "./src/app/layout/modules/bs-component/components/tabs/tabs.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TabsComponent);
    return TabsComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/timepicker/timepicker.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/timepicker/timepicker.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL3RpbWVwaWNrZXIvdGltZXBpY2tlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/timepicker/timepicker.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/timepicker/timepicker.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: TimepickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimepickerComponent", function() { return TimepickerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TimepickerComponent = /** @class */ (function () {
    function TimepickerComponent() {
        this.defaultTime = { hour: 13, minute: 30 };
        this.meridianTime = { hour: 13, minute: 30 };
        this.meridian = true;
        this.SecondsTime = { hour: 13, minute: 30, second: 30 };
        this.seconds = true;
        this.customTime = { hour: 13, minute: 30, second: 0 };
        this.hourStep = 1;
        this.minuteStep = 15;
        this.secondStep = 30;
    }
    TimepickerComponent.prototype.toggleSeconds = function () {
        this.seconds = !this.seconds;
    };
    TimepickerComponent.prototype.toggleMeridian = function () {
        this.meridian = !this.meridian;
    };
    TimepickerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-timepicker',
            template: __webpack_require__(/*! raw-loader!./timepicker.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/timepicker/timepicker.component.html"),
            styles: [__webpack_require__(/*! ./timepicker.component.scss */ "./src/app/layout/modules/bs-component/components/timepicker/timepicker.component.scss")]
        })
    ], TimepickerComponent);
    return TimepickerComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/tooltip/tooltip.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/tooltip/tooltip.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2JzLWNvbXBvbmVudC9jb21wb25lbnRzL3Rvb2x0aXAvdG9vbHRpcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/modules/bs-component/components/tooltip/tooltip.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/bs-component/components/tooltip/tooltip.component.ts ***!
  \*************************************************************************************/
/*! exports provided: TooltipComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TooltipComponent", function() { return TooltipComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TooltipComponent = /** @class */ (function () {
    function TooltipComponent() {
    }
    TooltipComponent.prototype.ngOnInit = function () {
    };
    TooltipComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tooltip',
            template: __webpack_require__(/*! raw-loader!./tooltip.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/bs-component/components/tooltip/tooltip.component.html"),
            styles: [__webpack_require__(/*! ./tooltip.component.scss */ "./src/app/layout/modules/bs-component/components/tooltip/tooltip.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TooltipComponent);
    return TooltipComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/notificationmessage/detail/notificationgroup.detail.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/modules/notificationmessage/detail/notificationgroup.detail.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .notificationgroup-detail label {\n  margin-top: 10px;\n}\n.card-detail .notificationgroup-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .notificationgroup-detail .card-header {\n  background-color: #555;\n}\n.card-detail .notificationgroup-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbm90aWZpY2F0aW9ubWVzc2FnZS9kZXRhaWwvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxub3RpZmljYXRpb25tZXNzYWdlXFxkZXRhaWxcXG5vdGlmaWNhdGlvbmdyb3VwLmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbm90aWZpY2F0aW9ubWVzc2FnZS9kZXRhaWwvbm90aWZpY2F0aW9uZ3JvdXAuZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksa0JBQUE7QUNBUjtBRENRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0FaO0FEQ1k7RUFDSSxpQkFBQTtBQ0NoQjtBREVRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTFo7QURFWTtFQUNJLGlCQUFBO0FDQWhCO0FES0k7RUFDSSxhQUFBO0FDSFI7QURJUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNGWjtBRGlCSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDZlI7QURrQlE7RUFDSSxnQkFBQTtBQ2hCWjtBRGtCUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNoQlo7QURtQlE7RUFDSSxzQkFBQTtBQ2pCWjtBRGtCWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDaEJoQiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL25vdGlmaWNhdGlvbm1lc3NhZ2UvZGV0YWlsL25vdGlmaWNhdGlvbmdyb3VwLmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuZWRpdF9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gLmltYWdle1xyXG4gICAgLy8gICAgIGgze1xyXG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAvLyAgICAgICAgIGltZ3tcclxuICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm5vdGlmaWNhdGlvbmdyb3VwLWRldGFpbHtcclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufSIsIi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubm90aWZpY2F0aW9uZ3JvdXAtZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAubm90aWZpY2F0aW9uZ3JvdXAtZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5ub3RpZmljYXRpb25ncm91cC1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubm90aWZpY2F0aW9uZ3JvdXAtZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/notificationmessage/detail/notificationgroup.detail.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/modules/notificationmessage/detail/notificationgroup.detail.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: NotificationGroupDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationGroupDetailComponent", function() { return NotificationGroupDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/notification/notification.service */ "./src/app/services/notification/notification.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var NotificationGroupDetailComponent = /** @class */ (function () {
    function NotificationGroupDetailComponent(notificationService) {
        this.notificationService = notificationService;
        this.errorLabel = false;
        this.edit = false;
    }
    NotificationGroupDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    NotificationGroupDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    NotificationGroupDetailComponent.prototype.editThis = function () {
        console.log(this.edit);
        this.edit = !this.edit;
        console.log(this.edit);
    };
    NotificationGroupDetailComponent.prototype.backToTable = function () {
        console.log(this.back);
        this.back[1](this.back[0]);
    };
    NotificationGroupDetailComponent.ctorParameters = function () { return [
        { type: _services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__["NotificationService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NotificationGroupDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NotificationGroupDetailComponent.prototype, "back", void 0);
    NotificationGroupDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notificationgroup-detail',
            template: __webpack_require__(/*! raw-loader!./notificationgroup.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notificationmessage/detail/notificationgroup.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./notificationgroup.detail.component.scss */ "./src/app/layout/modules/notificationmessage/detail/notificationgroup.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__["NotificationService"]])
    ], NotificationGroupDetailComponent);
    return NotificationGroupDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/notificationmessage/edit/notificationgroup.edit.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/modules/notificationmessage/edit/notificationgroup.edit.component.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .notificationgroup-detail label {\n  margin-top: 10px;\n}\n.card-detail .notificationgroup-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .notificationgroup-detail .card-header {\n  background-color: #555;\n}\n.card-detail .notificationgroup-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbm90aWZpY2F0aW9ubWVzc2FnZS9lZGl0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcbm90aWZpY2F0aW9ubWVzc2FnZVxcZWRpdFxcbm90aWZpY2F0aW9uZ3JvdXAuZWRpdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbm90aWZpY2F0aW9ubWVzc2FnZS9lZGl0L25vdGlmaWNhdGlvbmdyb3VwLmVkaXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtBQ0FSO0FEQ1E7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDQVo7QURDWTtFQUNJLGlCQUFBO0FDQ2hCO0FERVE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUtBLFdBQUE7QUNMWjtBREVZO0VBQ0ksaUJBQUE7QUNBaEI7QURJUTtFQUNJLHlCQUFBO0FDRlo7QURLSTtFQUNJLGFBQUE7QUNIUjtBREtRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0haO0FET0k7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0xSO0FEU1E7RUFDSSxnQkFBQTtBQ1BaO0FEU1E7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDUFo7QURvQ1E7RUFDSSxzQkFBQTtBQ2xDWjtBRG1DWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDakNoQiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL25vdGlmaWNhdGlvbm1lc3NhZ2UvZWRpdC9ub3RpZmljYXRpb25ncm91cC5lZGl0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b24udHJ1ZXtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICBcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm5vdGlmaWNhdGlvbmdyb3VwLWRldGFpbHtcclxuICAgICAgIFxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyAuaW1hZ2V7XHJcbiAgICAgICAgLy8gICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgLy8gICAgID5kaXZ7XHJcbiAgICAgICAgLy8gICAgICAgICB3aWR0aDogMzMuMzMzMyU7XHJcbiAgICAgICAgLy8gICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgLy8gICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vICAgICAuY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgLy8gICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gICAgIGgze1xyXG4gICAgICAgIC8vICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIC8vICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIC8vICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICAvLyAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgLy8gICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAvLyAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAvLyAgICAgICAgIGltZ3tcclxuICAgICAgICAvLyAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vIH1cclxuXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufSIsIi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm5vdGlmaWNhdGlvbmdyb3VwLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm5vdGlmaWNhdGlvbmdyb3VwLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAubm90aWZpY2F0aW9uZ3JvdXAtZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm5vdGlmaWNhdGlvbmdyb3VwLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/notificationmessage/edit/notificationgroup.edit.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/notificationmessage/edit/notificationgroup.edit.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: NotificationGroupEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationGroupEditComponent", function() { return NotificationGroupEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/notification/notification.service */ "./src/app/services/notification/notification.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var NotificationGroupEditComponent = /** @class */ (function () {
    function NotificationGroupEditComponent(notificationService) {
        this.notificationService = notificationService;
        this.loading = false;
        this.notificationStatus = [
            { label: "ACTIVE", value: "ACTIVE" },
            { label: "INACTIVE", value: "INACTIVE" }
        ];
        this.notificationTopic = [
            { label: "yes", value: "yes" },
            { label: "no", value: "no" }
        ];
        this.errorLabel = false;
    }
    NotificationGroupEditComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    NotificationGroupEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.detail.previous_status = this.detail.status;
                this.notificationStatus.forEach(function (element, index) {
                    if (element.value == _this.detail.status) {
                        _this.notificationStatus[index].selected = 1;
                    }
                });
                this.detail.previous_topic = this.detail.topic;
                this.notificationTopic.forEach(function (element, index) {
                    if (element.value == _this.detail.topic) {
                        _this.notificationTopic[index].selected = 1;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    NotificationGroupEditComponent.prototype.backToDetail = function () {
        //console.log(this.back);
        this.back[0][this.back[1]]();
    };
    NotificationGroupEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.loading = !this.loading;
                        return [4 /*yield*/, this.notificationService.updateNotificationGroupID(this.detail)];
                    case 1:
                        _a.sent();
                        this.loading = !this.loading;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    NotificationGroupEditComponent.ctorParameters = function () { return [
        { type: _services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__["NotificationService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NotificationGroupEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NotificationGroupEditComponent.prototype, "back", void 0);
    NotificationGroupEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notificationgroup-edit',
            template: __webpack_require__(/*! raw-loader!./notificationgroup.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/notificationmessage/edit/notificationgroup.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./notificationgroup.edit.component.scss */ "./src/app/layout/modules/notificationmessage/edit/notificationgroup.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_notification_notification_service__WEBPACK_IMPORTED_MODULE_2__["NotificationService"]])
    ], NotificationGroupEditComponent);
    return NotificationGroupEditComponent;
}());



/***/ }),

/***/ "./src/app/router.animations.ts":
/*!**************************************!*\
  !*** ./src/app/router.animations.ts ***!
  \**************************************/
/*! exports provided: routerTransition, noTransition, slideToRight, slideToLeft, slideToBottom, slideToTop */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routerTransition", function() { return routerTransition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "noTransition", function() { return noTransition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToRight", function() { return slideToRight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToLeft", function() { return slideToLeft; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToBottom", function() { return slideToBottom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToTop", function() { return slideToTop; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

function routerTransition() {
    return noTransition();
}
function noTransition() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', []);
}
function slideToRight() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(-100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(100%)' }))
        ])
    ]);
}
function slideToLeft() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(-100%)' }))
        ])
    ]);
}
function slideToBottom() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(-100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(100%)' }))
        ])
    ]);
}
function slideToTop() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(-100%)' }))
        ])
    ]);
}


/***/ }),

/***/ "./src/app/services/download/download.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/download/download.service.ts ***!
  \*******************************************************/
/*! exports provided: DownloadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DownloadService", function() { return DownloadService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DownloadService = /** @class */ (function () {
    function DownloadService() {
    }
    DownloadService.prototype.downloadFile = function (data, filename) {
        if (filename === void 0) { filename = 'data'; }
        console.log(data);
        var csvData = this.ConvertToCSV(data, ['name', 'age', 'average', 'approved', 'description']);
        // console.log(csvData)
        var blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
        var dwldLink = document.createElement("a");
        var url = URL.createObjectURL(blob);
        var isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) { //if Safari open in new window to save file with random filename.
            dwldLink.setAttribute("target", "_blank");
        }
        dwldLink.setAttribute("href", url);
        dwldLink.setAttribute("download", filename + ".csv");
        dwldLink.style.visibility = "hidden";
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);
    };
    DownloadService.prototype.ConvertToCSV = function (objArray, headerList) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
        var row = 'S.No,';
        for (var index in headerList) {
            row += headerList[index] + ',';
        }
        row = row.slice(0, -1);
        str += row + '\r\n';
        for (var i = 0; i < array.length; i++) {
            var line = (i + 1) + '';
            for (var index in headerList) {
                var head = headerList[index];
                line += ',' + array[i][head];
            }
            str += line + '\r\n';
        }
        return str;
    };
    DownloadService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], DownloadService);
    return DownloadService;
}());



/***/ }),

/***/ "./src/app/services/master.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/master.service.ts ***!
  \********************************************/
/*! exports provided: MasterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MasterService", function() { return MasterService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var MasterService = /** @class */ (function () {
    function MasterService(httpGet) {
        this.httpGet = httpGet;
        this.api_url = '';
        this.http = httpGet;
        this.api_url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])();
    }
    MasterService.prototype.getToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            var myToken, myStatus;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, localStorage.getItem('tokenlogin')];
                    case 1:
                        myToken = _a.sent();
                        return [4 /*yield*/, localStorage.getItem('isLoggedin')];
                    case 2:
                        myStatus = _a.sent();
                        if (myToken && myStatus) {
                            this.myToken = myToken;
                            this.myStatus = myStatus;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MasterService.prototype.checkingUrl = function (_url) {
        var indexOfHttp = _url.indexOf('http');
        var url;
        url = _url;
        if (indexOfHttp < 0) {
            url = this.api_url + _url;
        }
        return url;
    };
    MasterService.prototype.get = function (_params, _url, headers) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_1, msg;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.getRest(_params, _url, headers).toPromise()];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        this.checkStatus(error_1);
                        if (error_1.status == 401) {
                            console.warn("MASUK SINI NIH");
                            localStorage.removeItem('isLoggedin');
                            localStorage.removeItem('tokenlogin');
                            localStorage.removeItem('devmode');
                            // window.location.href = "/login"
                        }
                        msg = error_1.error ? (error_1.error.error ? error_1.error.error : error_1.message) : error_1.message;
                        throw new Error(JSON.stringify(msg, null, 2));
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MasterService.prototype.post = function (_params, _url, headers) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_2, throwError, myToken, myStatus, msg;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.postRest(_params, _url, headers).toPromise()];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _a.sent();
                        this.checkStatus(error_2);
                        throwError = error_2;
                        myToken = localStorage.getItem('tokenlogin');
                        myStatus = localStorage.getItem('isLoggedin');
                        if (error_2.status == 401 && myToken && myStatus) {
                            throw new Error("you're not allowed to go Forward please check again. call your administrator");
                            // localStorage.removeItem('isLoggedin');
                            // localStorage.removeItem('tokenlogin');
                            // localStorage.removeItem('devmode');
                            // window.location.href = "/login"
                        }
                        if (error_2.error) {
                            throwError = error_2.error; // the result from server is {"error": "error message"}
                        }
                        msg = error_2.error ? (error_2.error.error ? error_2.error.error : error_2.message) : error_2.message;
                        throw new Error(JSON.stringify(msg, null, 2));
                    case 3: return [2 /*return*/, result];
                }
            });
        });
    };
    MasterService.prototype.add = function (_params, _url, headers) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_3, msg;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.postRest(_params, _url, headers).toPromise()];
                    case 1:
                        result = _a.sent();
                        console.log('result = ', result);
                        return [3 /*break*/, 3];
                    case 2:
                        error_3 = _a.sent();
                        this.checkStatus(error_3);
                        if (error_3.status == 401) {
                            throw new Error("you're not aloowed to go Forward please check again");
                            // localStorage.removeItem('isLoggedin');
                            // localStorage.removeItem('tokenlogin');
                            // localStorage.removeItem('devmode');
                            // window.location.href = "/login"
                        }
                        msg = error_3.error ? (error_3.error.error ? error_3.error.error : error_3.message) : error_3.message;
                        throw new Error(JSON.stringify(msg, null, 2));
                    case 3: return [2 /*return*/, result];
                }
            });
        });
    };
    MasterService.prototype.update = function (_params, _url, headers) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_4, msg;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.updateRest(_params, _url, headers).toPromise()];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        this.checkStatus(error_4);
                        if (error_4.status == 401) {
                            localStorage.removeItem('isLoggedin');
                            localStorage.removeItem('tokenlogin');
                            localStorage.removeItem('devmode');
                            // window.location.href = "/login"
                        }
                        msg = error_4.error ? (error_4.error.error ? error_4.error.error : error_4.message) : error_4.message;
                        throw new Error(JSON.stringify(msg, null, 2));
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MasterService.prototype.delete = function (_params, _url, headers) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_5, msg;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.deleteRest(_params, _url, headers).toPromise()];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        this.checkStatus(error_5);
                        if (error_5.status == 401) {
                            localStorage.removeItem('isLoggedin');
                            localStorage.removeItem('tokenlogin');
                            localStorage.removeItem('devmode');
                            // window.location.href = "/login"
                        }
                        msg = error_5.error ? (error_5.error.error ? error_5.error.error : error_5.message) : error_5.message;
                        throw new Error(JSON.stringify(msg, null, 2));
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MasterService.prototype.getRest = function (_params, _url, additionalHeaders) {
        var uri = this.checkingUrl(_url);
        var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        var arrayParams = Object.assign({}, _params);
        httpParams = httpParams.append('request', JSON.stringify(arrayParams));
        var headerParams = {
            'Content-Type': 'application/json',
        };
        if (additionalHeaders) {
            headerParams = __assign({}, headerParams, additionalHeaders);
        }
        var httpOpt = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"](headerParams),
            params: {}
        };
        if (_params) {
            httpOpt.params = _params;
        }
        // console.log('Params 1 : ', httpOpt.params)
        // console.log('Params 2 : ', httpParams)
        return this.http.get(uri, httpOpt);
    };
    MasterService.prototype.postRest = function (_params, _url, additionalHeaders) {
        var uri = this.checkingUrl(_url);
        var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        var arrayParams = Object.assign({}, _params);
        httpParams = httpParams.append('request', JSON.stringify(arrayParams));
        var headerParams = {
            'Content-Type': 'application/json',
        };
        if (additionalHeaders) {
            headerParams = __assign({}, headerParams, additionalHeaders);
        }
        var httpOpt = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"](headerParams),
        };
        // console.log('headers : ', headerParams);
        // console.log('url : ', uri);
        return this.http.post(uri, arrayParams, httpOpt);
    };
    MasterService.prototype.updateRest = function (_params, _url, additionalHeaders) {
        var uri = this.checkingUrl(_url);
        var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        var arrayParams = Object.assign({}, _params);
        httpParams = httpParams.append('request', JSON.stringify(arrayParams));
        var headerParams = {
            'Content-Type': 'application/json',
        };
        if (additionalHeaders) {
            headerParams = __assign({}, headerParams, additionalHeaders);
        }
        var httpOpt = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"](headerParams),
        };
        return this.http.put(uri, arrayParams, httpOpt);
    };
    MasterService.prototype.deleteRest = function (_params, _url, additionalHeaders) {
        var uri = this.checkingUrl(_url);
        var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        var arrayParams = Object.assign({}, _params);
        httpParams = httpParams.append('request', JSON.stringify(arrayParams));
        var headerParams = {
            'Content-Type': 'application/json',
        };
        if (additionalHeaders) {
            headerParams = __assign({}, headerParams, additionalHeaders);
        }
        var httpOpt = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"](headerParams),
            params: httpParams
        };
        return this.http.delete(uri, httpOpt);
    };
    MasterService.prototype.searchResultRest = function (_params, _url, additionalHeaders) {
        var uri = this.checkingUrl(_url);
        var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        _params.search = Object.assign({}, _params.search);
        _params.order_by = Object.assign({}, _params.order_by);
        var arrayParams = Object.assign({}, _params);
        httpParams = httpParams.append('request', JSON.stringify(arrayParams));
        var headerParams = {
            'Content-Type': 'application/json',
        };
        if (additionalHeaders) {
            headerParams = __assign({}, headerParams, additionalHeaders);
        }
        var httpOpt = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"](headerParams),
            params: httpParams
        };
        return this.http.get(uri, httpOpt);
    };
    MasterService.prototype.searchResult = function (_params, _url, additionalHeaders) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_6, msg;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.searchResultRest(_params, _url, additionalHeaders).toPromise()];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 3];
                    case 2:
                        error_6 = _a.sent();
                        console.log(error_6);
                        this.checkStatus(error_6);
                        msg = error_6.error ? (error_6.error.error ? error_6.error.error : error_6.message) : error_6.message;
                        throw new Error(JSON.stringify(msg, null, 2));
                    case 3: return [2 /*return*/, result];
                }
            });
        });
    };
    MasterService.prototype.checkStatus = function (error) {
        if (error.status == 401) {
            //localStorage.removeItem('tokenlogin')
            //window.location.reload()
        }
    };
    MasterService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    MasterService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], MasterService);
    return MasterService;
}());



/***/ }),

/***/ "./src/app/services/member/member.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/member/member.service.ts ***!
  \***************************************************/
/*! exports provided: MemberService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberService", function() { return MemberService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _master_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../master.service */ "./src/app/services/master.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var MemberService = /** @class */ (function () {
    function MemberService(http, myService) {
        this.http = http;
        this.myService = myService;
        this.gerro = 'test';
        this.api_url = '';
        this.httpReq = false;
        this.api_url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["api_url"])();
    }
    MemberService.prototype.getMemberSummaryByDate = function (curDate) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report/summary';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.setNewPin = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/pin/set';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.changePin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/pin/change';
                        return [4 /*yield*/, this.myService.post(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.resetPin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/pin/reset';
                        return [4 /*yield*/, this.myService.post(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getAllMembersLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report?request={"search": {},"limit_per_page":50,"current_page":1}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMembersLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report';
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log("zz", result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMemberLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report?request={"search": {"type":"member"},"limit_per_page":50,"current_page":1}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMemberAllReport = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/all/report?request={"search": {"show_password":1},"limit_per_page":50,"current_page":1}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        throw new TypeError(error_8);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.searchMemberAllReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_9 = _a.sent();
                        throw new TypeError(error_9);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getAutoformMember = function (current_page) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/all/report?request={"search": {},"limit_per_page":50,"current_page":' + current_page + '}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_10 = _a.sent();
                        throw new TypeError(error_10);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getFormMember = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/find';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_11 = _a.sent();
                        throw new TypeError(error_11);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.searchFormMember = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/find';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_12 = _a.sent();
                        throw new TypeError(error_12);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getFormMemberByID = function (id_pel) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/find?request={"search": {"username":"' + String(id_pel) + '"},"limit_per_page":1,"current_page":1, "order_by": {"updated_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_13 = _a.sent();
                        throw new TypeError(error_13);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMemberReportByID = function (id_pel) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/all/report?request={"search": {"username":"' + String(id_pel) + '"},"limit_per_page":1,"current_page":1, "order_by": {"updated_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_14 = _a.sent();
                        throw new TypeError(error_14);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getGuestLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report?request={"search": {"type":"guest"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_15 = _a.sent();
                        throw new TypeError(error_15);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.addNewMember = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_16;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/register';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_16 = _a.sent();
                        throw new TypeError(error_16);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.searchRegion = function (payload) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_17;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'regions/search';
                        return [4 /*yield*/, this.myService.post(payload, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_17 = _a.sent();
                        throw new TypeError(error_17);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.loginRedeemMember = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_18;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'auth/token';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_18 = _a.sent();
                        throw new TypeError(error_18);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getTokenMember = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_19;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'auth/token/member';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_19 = _a.sent();
                        throw new TypeError(error_19);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.addNewMerchantOutlet = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_20;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/newoutlet';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_20 = _a.sent();
                        throw new TypeError(error_20);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.detailMerchant = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_21;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/detail';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_21 = _a.sent();
                        throw new TypeError(error_21);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.updateMember = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_22;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/all';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_22 = _a.sent();
                        throw new TypeError(error_22);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.searchGuestLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_23;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.type = "guest";
                        console.log(params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_23 = _a.sent();
                        throw new TypeError(error_23);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.searchAllMemberLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_24;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // params.search.type="member";
                        console.log(params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_24 = _a.sent();
                        throw new TypeError(error_24);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.searchMemberLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_25;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.type = "member";
                        console.log(params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_25 = _a.sent();
                        throw new TypeError(error_25);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.searchOutletsLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_26;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/all';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_26 = _a.sent();
                        throw new TypeError(error_26);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMemberDetail = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_27;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/detail';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_27 = _a.sent();
                        throw new TypeError(error_27);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getMemberDemographyReport = function (type) {
        if (type === void 0) { type = 'age'; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_28;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/report/summary/demography/' + type;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_28 = _a.sent();
                        throw new TypeError(error_28);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.detailMember = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_29;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/detail/' + id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_29 = _a.sent();
                        throw new TypeError(error_29);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getRegion = function (match, usage) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_30;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'shippingregion/searchregion';
                        return [4 /*yield*/, this.myService.get({ match: match, usage: usage }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_30 = _a.sent();
                        throw new TypeError(error_30);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getProvince = function (match, usage) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_31;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'region/province_list';
                        return [4 /*yield*/, this.myService.get({ province: match, usage: usage }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_31 = _a.sent();
                        throw new TypeError(error_31);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getCity = function (match, usage, province_code) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_32;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'region/city_list';
                        return [4 /*yield*/, this.myService.get({ match: match, usage: usage, province_code: province_code }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_32 = _a.sent();
                        throw new TypeError(error_32);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getSubDistrict = function (match, usage, city_code) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_33;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'region/sub_district_list';
                        return [4 /*yield*/, this.myService.get({ match: match, usage: usage, city_code: city_code }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_33 = _a.sent();
                        throw new TypeError(error_33);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getVillage = function (match, usage, subdistrict_code) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_34;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'region/village_list';
                        return [4 /*yield*/, this.myService.get({ match: match, usage: usage, subdistrict_code: subdistrict_code }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_34 = _a.sent();
                        throw new TypeError(error_34);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.detailOutlet = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_35;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/outlet';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_35 = _a.sent();
                        throw new TypeError(error_35);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.updateMemberID = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_36;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'members/update/' + params.member_id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_36 = _a.sent();
                        throw new TypeError(error_36);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.updateMemberAddress = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_37;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'admin/update_address?method=single';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_37 = _a.sent();
                        throw new TypeError(error_37);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.updateSingleMemberAutoform = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_38;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'autoform/update?form=ps-form-1&method=single';
                        return [4 /*yield*/, this.myService.add(data, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_38 = _a.sent();
                        throw new TypeError(error_38);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.updateSingleFormMember = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_39;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/update';
                        return [4 /*yield*/, this.myService.update(data, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_39 = _a.sent();
                        throw new TypeError(error_39);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.registerBulkMember = function (file, obj, payload) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["api_url"])() + 'member/register/bulk';
                console.log('now', fd);
                this.httpReq = this.http.post(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])(),
                    // headers : new HttpHeaders({
                    //   'Authorization': myToken,
                    //   'app-label': 'retailer_prg'
                    // }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        console.log("event", event);
                        console.log(event);
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            console.log('this is body');
                            //obj.firstLoad();
                            console.warn("c body", c.body);
                            // obj.prodOnUpload = true;
                            if (c.status == 200) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                obj.Report = c.body.url;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                    title: 'Bulk Upload Success',
                                    text: 'Anda akan mengunduh file csv untuk username dan password member : ' + obj.Report,
                                    icon: 'success',
                                    confirmButtonText: 'Ok',
                                }).then(function (result) {
                                    if (obj.Report) {
                                        window.open(obj.Report, "_blank");
                                    }
                                });
                            }
                            //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
                            // obj.process_id = c.body;
                            console.log("process_id", obj.Report);
                            // console.log(c.body.failed);
                            // obj.failed = c.body.failed.length;
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                                // Swal.fire(
                                //   'Success!',
                                //   'Your file has been uploaded.',
                                //   'success'
                                // )
                                // window.open(c.body.url, "_blank");
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    //console.log(result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    MemberService.prototype.updateBulkMember = function (file, obj, payload) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["api_url"])() + 'member/update/bulk';
                console.log('now', fd);
                this.httpReq = this.http.put(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])(),
                    // headers : new HttpHeaders({
                    //   'Authorization': myToken,
                    //   'app-label': 'retailer_prg'
                    // }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        console.log("event", event);
                        console.log(event);
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            console.log('this is body');
                            console.warn(" obj", obj);
                            // obj.firstLoad();
                            obj.Report = c.body;
                            //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
                            obj.process_id = c.body;
                            // console.log("process_id",obj.Report);
                            // obj.prodOnUpload = true;
                            // console.log(c.body.result.failed);
                            // obj.failed = c.body.result.failed.length;
                            if (c.status == 200) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Success!', 'Your file has been uploaded.', 'success');
                            }
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                                // Swal.fire(
                                //   'Success!',
                                //   'Your file has been uploaded.',
                                //   'success'
                                // )
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    //console.log(result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    MemberService.prototype.updateProfile = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_40;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/update_profile';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_40 = _a.sent();
                        throw new TypeError(error_40);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.addBastSk = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_41;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/additional_info/add';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_41 = _a.sent();
                        throw new TypeError(error_41);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.deleteBastSk = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_42;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/additional_info/remove';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_42 = _a.sent();
                        throw new TypeError(error_42);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.deleteMember = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_43;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/' + params._id;
                        return [4 /*yield*/, this.myService.delete(params, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_43 = _a.sent();
                        throw new TypeError(error_43);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.getDownloadFileLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_44;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/?report=csv';
                        console.log(url);
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_44 = _a.sent();
                        throw new TypeError(error_44);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.setCompleteMember = function (payload) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, url, error_45;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/mark_as_complete';
                        return [4 /*yield*/, this.myService.update(payload, url, customHeaders)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_45 = _a.sent();
                        throw new TypeError(error_45);
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MemberService.prototype.getMerchantList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_46;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant_list';
                        console.log(url);
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_46 = _a.sent();
                        throw new TypeError(error_46);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.Courier = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_47;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'courier/list';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_47 = _a.sent();
                        throw new TypeError(error_47);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // public async pagination() {
    //   let result;
    //   const customHeaders = getTokenHeader();
    //   method: GET
    //     "/ v1 / orderhistory / page /@page/@limit "
    //   params: "page" =>, "limit" => limit per page
    //   response: total record and order history data per page
    //   Co
    //   return result;
    // }
    MemberService.prototype.resetPasswordRequest = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_48;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getAPIKeyHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/password_request';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_48 = _a.sent();
                        throw new TypeError(error_48);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.resetPasswordCodeVerification = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_49;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getAPIKeyHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'member/pwd_reset_auth';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_49 = _a.sent();
                        throw new TypeError(error_49);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.prototype.resetPasswordChangePassword = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_50;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = "member/change_password";
                        return [4 /*yield*/, this.myService.update(data, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_50 = _a.sent();
                        throw new TypeError(error_50);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MemberService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _master_service__WEBPACK_IMPORTED_MODULE_1__["MasterService"] }
    ]; };
    MemberService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _master_service__WEBPACK_IMPORTED_MODULE_1__["MasterService"]])
    ], MemberService);
    return MemberService;
}());



/***/ }),

/***/ "./src/app/services/notification/notification.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/notification/notification.service.ts ***!
  \***************************************************************/
/*! exports provided: NotificationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationService", function() { return NotificationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _master_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../master.service */ "./src/app/services/master.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var NotificationService = /** @class */ (function () {
    function NotificationService(myService, http) {
        this.myService = myService;
        this.http = http;
        this.api_url = '';
        this.httpReq = false;
        this.gerro = 'test';
        this.api_url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])();
    }
    NotificationService.prototype.getNotificationLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/all';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.searchMemberIDLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications-group/all_members';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.getMemberIDLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications-group/all_members';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.updateTokenList = function (params, _id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications-group/update_token/' + _id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.addNewNotification = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.updateNotification = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/all';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.detailNotification = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/' + _id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.detailNotificationGroup = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications-group/' + _id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        throw new TypeError(error_8);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.deleteNotification = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/all';
                        return [4 /*yield*/, this.myService.delete(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_9 = _a.sent();
                        throw new TypeError(error_9);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    /* public async searchNotificationLint(params) {
      // console.log(params);
      let result;
      try {
        const url   = 'notifications/all';
        result  = await this.myService.searchResult(params, url);
      } catch (error) {
        throw new TypeError(error);
      }
        return  result;
    } */
    NotificationService.prototype.updateNotificationID = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/update/' + params.notification_id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_10 = _a.sent();
                        throw new TypeError(error_10);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.delete = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var url, result, customHeaders, error_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications/remove/' + params.notification_id;
                        return [4 /*yield*/, this.myService.delete(params, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_11 = _a.sent();
                        throw new TypeError(error_11);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.getNotificationGroupLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications-group/all';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_12 = _a.sent();
                        throw new TypeError(error_12);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.addNewNotificationGroup = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications-group/';
                        console.log(url, params);
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_13 = _a.sent();
                        throw new TypeError(error_13);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // public async detailNotificationGroup(_id) {
    //   let result;
    //   try {
    //     const url   = 'notifications-group/' + _id;
    //     result  = await this.myService.get(null, url);
    //   } catch (error) {
    //     throw new TypeError(error);
    //   }
    //   return  result;
    // }
    NotificationService.prototype.updateNotificationGroupID = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications-group/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_14 = _a.sent();
                        throw new TypeError(error_14);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.getNotificationMessageLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications-message/all';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_15 = _a.sent();
                        throw new TypeError(error_15);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.addNewNotificationMessage = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_16;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'notifications-message/';
                        console.log(url, params);
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_16 = _a.sent();
                        throw new TypeError(error_16);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    NotificationService.prototype.upload = function (file, obj) {
        return __awaiter(this, void 0, void 0, function () {
            var fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                fd = new FormData();
                fd.append('image', file, file.name);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'media/upload';
                console.log(url);
                this.httpReq = this.http.post(url, fd, {
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) {
                    console.log(event);
                    if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].UploadProgress) {
                        var prgval = Math.round(event.loaded / event.total * 100);
                        obj.updateProgressBar(prgval);
                        console.log(obj.cancel);
                        if (obj.cancel == true) {
                            _this.httpReq.unsubscribe();
                        }
                        console.log('upload Progress: ' + prgval + "%");
                    }
                }, function (result) {
                    console.log(result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    NotificationService.ctorParameters = function () { return [
        { type: _master_service__WEBPACK_IMPORTED_MODULE_1__["MasterService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
    ]; };
    NotificationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_master_service__WEBPACK_IMPORTED_MODULE_1__["MasterService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], NotificationService);
    return NotificationService;
}());



/***/ }),

/***/ "./src/app/services/orderhistory/orderhistory.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/orderhistory/orderhistory.service.ts ***!
  \***************************************************************/
/*! exports provided: OrderhistoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryService", function() { return OrderhistoryService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _master_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../master.service */ "./src/app/services/master.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var OrderhistoryService = /** @class */ (function () {
    function OrderhistoryService(http, myService) {
        this.http = http;
        this.myService = myService;
        this.api_url = '';
        this.httpReq = false;
        this.api_url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])();
    }
    OrderhistoryService.prototype.getOrderhistory = function (params) {
        // const uri = 'http://192.168.1.198/f3/v1/orderhistory/all';
        // console.log(uri);
        // return this.http.get<Orderhistory[]>(uri);
        var myToken = localStorage.getItem('tokenlogin');
        var myStatus = localStorage.getItem('isLoggedin');
        if (myStatus == 'true') {
            try {
                if (params) {
                    var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
                    params.search = Object.assign({}, params.search);
                    params.order_by = Object.assign({}, params.order_by);
                    var arrayParams = Object.assign({}, params);
                    httpParams = httpParams.append('request', JSON.stringify(arrayParams));
                    var httpOpt = {
                        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                            'Content-Type': 'application/json',
                            'Authorization': myToken,
                        }),
                        params: httpParams
                    };
                    var uri = this.api_url + 'order-invoice/report';
                    console.log('URI order history', uri);
                    return this.http.get(uri, httpOpt);
                }
            }
            catch (error) {
                throw new TypeError(error);
            }
        }
        else {
            localStorage.removeItem('isLoggedin');
            window.location.reload();
        }
    };
    OrderhistoryService.prototype.getHtmlElement = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var headers, uri;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.warn("masuk");
                        headers = {
                            'Accept': 'text/html',
                            'Access-Control-Allow-Origin': '*',
                            'Content-Type': 'text/html',
                            'Sec-Fetch-Mode': 'no-cors'
                        };
                        uri = url;
                        console.log('URI order history', uri);
                        return [4 /*yield*/, this.myService.get(null, uri, headers)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    OrderhistoryService.prototype.getAllreportSalesOrder = function (status) {
        if (status === void 0) { status = null; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = void 0;
                        if (status == "pending") {
                            url = 'order-invoice/all/report?request={"search":{"status":"PENDING"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        }
                        else if (status == "cancel") {
                            url = 'order-invoice/all/report?request={"search":{"status":"CANCEL"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        }
                        else if (status == "process") {
                            url = 'order-invoice/all/report?request={"search":{"status":"PROCESSED"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        }
                        else {
                            url = 'order-invoice/all/report?request={"search":{},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        }
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderDetail = function (order_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = "order-invoice/detail?order_id=" + order_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchShippingReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.warn("params", params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'delivery/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getShippingDetail = function (order_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = "delivery/report?request={\"search\":{\"order_id\":\"" + order_id + "\"}}";
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getShippingDetailByRef = function (reference_no) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = "delivery/report?request={\"search\":{\"reference_no\":\"" + reference_no + "\"}}";
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getReportBasedOnPO = function (status) {
        if (status === void 0) { status = null; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = void 0;
                        url = 'order-invoice/report/sales-order?request={"search":{"based_on":"po_no"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchReportBasedOnPO = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // console.log(params)
                        params.order_by = { request_date: -1 };
                        params.search.based_on = "po_no";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchAllReportSalesOrder = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // console.log(params)
                        params.order_by = { request_date: -1 };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        throw new TypeError(error_8);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchAllReportSalesOrderPackingList = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.order_by = { request_date: -1 };
                        params.search['additional_info.label_printed'] = 'ya';
                        params.search['status'] = 'PROCESSED';
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_9 = _a.sent();
                        throw new TypeError(error_9);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistoryLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report?request={"search":{},"limit_per_page":"50","current_page":"1"}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_10 = _a.sent();
                        throw new TypeError(error_10);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    /* langkah ke 2 bikin fungsi api */
    OrderhistoryService.prototype.getSalesOrdeReport = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/report?request={"search":{"status":"PAID"},"limit_per_page":"50","current_page":"1"}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_11 = _a.sent();
                        throw new TypeError(error_11);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.processOrder = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/process';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_12 = _a.sent();
                        throw new TypeError(error_12);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateStatusOrder = function (params, status) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/process/' + status + '?method=single';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_13 = _a.sent();
                        throw new TypeError(error_13);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updatDeliveryProcess = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'delivery/process?method=single';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_14 = _a.sent();
                        throw new TypeError(error_14);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateStatusOrderCompleted = function (params, obj) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_15, errorMsg;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/process/complete';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.warn("result??", result);
                        if (result && result.status == "success") {
                            obj.startUploading = false;
                            obj.selectedFile = null;
                            obj.cancel = false;
                            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Your file has been uploaded!', 'success');
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        error_15 = _a.sent();
                        // throw new TypeError(error);
                        obj.startUploading = false;
                        obj.selectedFile = null;
                        errorMsg = error_15.message;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', errorMsg, 'warning');
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateDeliveredProcess = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_16;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'delivery/complete?method=single';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_16 = _a.sent();
                        throw new TypeError(error_16);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.processRedeliver = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_17;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'delivery/return?method=single';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_17 = _a.sent();
                        throw new TypeError(error_17);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.processOrderRedelivery = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_18;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'delivery/redelivery?method=single';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_18 = _a.sent();
                        throw new TypeError(error_18);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.cancelOrder = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_19;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/cancel';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_19 = _a.sent();
                        throw new TypeError(error_19);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.cancelShipping = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_20;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'delivery/pickup/cancel?method=single';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_20 = _a.sent();
                        throw new TypeError(error_20);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.processPackaging = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_21;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'delivery/packaging?method=single';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_21 = _a.sent();
                        throw new TypeError(error_21);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getShippingReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, url3, url2, error_22;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = '';
                        url3 = 'orderhistory/shipping-report?request={"search":{"based_on": "products"},"limit_per_page":50,"current_page":1}';
                        url2 = 'orderhistory/shipping-report?request={"search":{"based_on": "order_id"},"limit_per_page":50,"current_page":1}';
                        if (params == 'order_id') {
                            url = url2;
                        }
                        else {
                            url = url3;
                        }
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_22 = _a.sent();
                        throw new TypeError(error_22);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchShippingProductReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_23;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        params.search.based_on = "products";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/shipping-report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_23 = _a.sent();
                        throw new TypeError(error_23);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchShippingidReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_24;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        params.search.based_on = "order_id";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/shipping-report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_24 = _a.sent();
                        throw new TypeError(error_24);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchShippingReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_25;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/shipping-report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_25 = _a.sent();
                        throw new TypeError(error_25);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getSalesOrderMerchantInt = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_26;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/report?request={"search":{"merchant_username":"' + params + '"},"limit_per_page":50,"current_page":1}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_26 = _a.sent();
                        throw new TypeError(error_26);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchMerchantSalesOrderReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var merchant, result, customHeaders, url, error_27;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, localStorage.getItem('merchantsales')];
                    case 1:
                        merchant = _a.sent();
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        params.search.merchant_username = merchant;
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        url = 'sales-order/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 3:
                        result = _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        error_27 = _a.sent();
                        throw new TypeError(error_27);
                    case 5: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getSalesOrderReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, url3, url2, error_28;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = '';
                        url3 = 'sales-order/admin?request={"search":{"based_on": "products"},"limit_per_page":50,"current_page":1}';
                        url2 = 'sales-order/admin?request={"search":{"based_on": "invoice_id"},"limit_per_page":50,"current_page":1}';
                        if (params == 'order_id') {
                            url = url2;
                        }
                        else {
                            url = url3;
                        }
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_28 = _a.sent();
                        throw new TypeError(error_28);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchSalesOrderReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_29;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        params.search.based_on = "products";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/admin';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_29 = _a.sent();
                        throw new TypeError(error_29);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchSalesOrderReport2int = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_30;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        params.search.based_on = "invoice_id";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/admin';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_30 = _a.sent();
                        throw new TypeError(error_30);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchSalesOrderReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_31;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_31 = _a.sent();
                        throw new TypeError(error_31);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistoryLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                try {
                    // const url = 'order-invoice/report'
                    // result = await this.getOrderhistory(params).toPromise();
                }
                catch (error) {
                    throw new TypeError(error);
                }
                return [2 /*return*/, result];
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistory = function (historyID) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_32;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/detail';
                        return [4 /*yield*/, this.myService.post({ order_id: historyID }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_32 = _a.sent();
                        throw new TypeError(error_32);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getDetailOrderhistory = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_33;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/booking_detail/' + params;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // console.log(url)
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_33 = _a.sent();
                        throw new TypeError(error_33);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderHistoryMerchant = function () {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_34;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/merchant?request={%22search%22:{},%22current_page%22:1,%22order_by%22:{%22order_date%22:-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_34 = _a.sent();
                        throw new TypeError(error_34);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderHistoryMerchantFilter = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_35;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/merchant';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_35 = _a.sent();
                        throw new TypeError(error_35);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistory = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_36;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update_status/' + params.status;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result, url);
                        return [3 /*break*/, 4];
                    case 3:
                        error_36 = _a.sent();
                        throw new TypeError(error_36);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getMerchantOrderHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, params, result, url, error_37;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        params = { 'status': 'waiting for confirmation' };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_37 = _a.sent();
                        throw new TypeError(error_37);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistorysummaryLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_38;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/alls';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log('orderhistory/alls', 'result');
                        return [3 /*break*/, 4];
                    case 3:
                        error_38 = _a.sent();
                        throw new TypeError(error_38);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorysummaryLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_39;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/alls';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_39 = _a.sent();
                        throw new TypeError(error_39);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistorysummary = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_40;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_40 = _a.sent();
                        throw new TypeError(error_40);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistorySummary = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, result_1, error_41;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result_1 = _a.sent();
                        console.log(result_1, url);
                        return [3 /*break*/, 4];
                    case 3:
                        error_41 = _a.sent();
                        throw new TypeError(error_41);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistoryallhistoryLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_42;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/alls';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_42 = _a.sent();
                        throw new TypeError(error_42);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderHistorySummaryByDate = function (curDate) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_43;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/summary';
                        return [4 /*yield*/, this.myService.get({ request: JSON.stringify({ date: curDate }) }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_43 = _a.sent();
                        throw new TypeError(error_43);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getAmountOrderHistorySummaryByDate = function (curDate) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_44;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/summary-amount';
                        return [4 /*yield*/, this.myService.get({ request: JSON.stringify({ date: curDate }) }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_44 = _a.sent();
                        throw new TypeError(error_44);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistoryallhistoryLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_45;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_45 = _a.sent();
                        throw new TypeError(error_45);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistoryallhistory = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_46;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_46 = _a.sent();
                        throw new TypeError(error_46);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryAllHistory = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_47;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_47 = _a.sent();
                        throw new TypeError(error_47);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryAllHistoryCancel = function (orderhistory_id, params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_48;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_48 = _a.sent();
                        throw new TypeError(error_48);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // public async cancelOrder(params) {
    //   let result;
    //   const customHeaders = getTokenHeader();
    //   console.log();
    //   try {
    //     const url = 'orderhistory/cancel';
    //     result = await this.myService.update(params, url, customHeaders);
    //     console.log(url, result);
    //   } catch (error) {
    //     throw new TypeError(error);
    //   }
    //   return result;
    // }
    OrderhistoryService.prototype.requesting = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/courier_pickup';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        throw new TypeError(err_1);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryAllHistoryMerchant = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_49;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/booking_detail/' + params.booking_id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_49 = _a.sent();
                        throw new TypeError(error_49);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistorysuccessLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_50;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report?request={"search": {"status":"PROCESSED"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_50 = _a.sent();
                        throw new TypeError(error_50);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistorycompleteLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_51;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report?request={"search": {"status":"COMPLETED"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_51 = _a.sent();
                        throw new TypeError(error_51);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistoryreturnLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_52;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report?request={"search": {"last_shipping_info":{"in":["return","redelivery"]}},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_52 = _a.sent();
                        throw new TypeError(error_52);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getCourier = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_53;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'courier/list/all';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_53 = _a.sent();
                        throw new TypeError(error_53);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorysuccessLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_54;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "PROCESSED";
                        params.order_by = { request_date: -1 };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_54 = _a.sent();
                        throw new TypeError(error_54);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorycompleteLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_55;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "COMPLETED";
                        params.order_by = { request_date: -1 };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_55 = _a.sent();
                        throw new TypeError(error_55);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistoryreturnLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_56;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.last_shipping_info = {
                            in: ["return", "redelivery"]
                        };
                        params.order_by = { request_date: -1 };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_56 = _a.sent();
                        throw new TypeError(error_56);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistorysuccess = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_57;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_57 = _a.sent();
                        throw new TypeError(error_57);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistorySuccess = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_58;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_58 = _a.sent();
                        throw new TypeError(error_58);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistorypendingLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_59;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log(result);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report?request={"search": {"status":"pending"},"limit_per_page":50,"current_page":1}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_59 = _a.sent();
                        throw new TypeError(error_59);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorypendingLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_60;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // console.log(params);
                        params.search.status = "pending";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_60 = _a.sent();
                        throw new TypeError(error_60);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistorypending = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_61;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_61 = _a.sent();
                        throw new TypeError(error_61);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryPending = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_62;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_62 = _a.sent();
                        throw new TypeError(error_62);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistorycancelLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_63;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(result);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report?request={"search": {"status":"CANCEL"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_63 = _a.sent();
                        throw new TypeError(error_63);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorycancelLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_64;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "CANCEL";
                        params.order_by = { request_date: -1 };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_64 = _a.sent();
                        throw new TypeError(error_64);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchAllreportSalesOrder = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_65;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_65 = _a.sent();
                        throw new TypeError(error_65);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistorycancel = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_66;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_66 = _a.sent();
                        throw new TypeError(error_66);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryCancel = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_67;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log('params', params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + params.id + '/order_status';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_67 = _a.sent();
                        throw new TypeError(error_67);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistoryfailedLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_68;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log(result);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report?request={"search": {"status":"failed"},"limit_per_page":50,"current_page":1}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_68 = _a.sent();
                        throw new TypeError(error_68);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistoryfailedLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_69;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //console.log(params);
                        params.search.status = "failed";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_69 = _a.sent();
                        throw new TypeError(error_69);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistoryfailed = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, url, error_70;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url)];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 3];
                    case 2:
                        error_70 = _a.sent();
                        throw new TypeError(error_70);
                    case 3: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryFailed = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_71;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_71 = _a.sent();
                        throw new TypeError(error_71);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getOrderhistoryerrorLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_72;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log(result);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report?request={"search": {"status":"error"},"limit_per_page":50,"current_page":1}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_72 = _a.sent();
                        throw new TypeError(error_72);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistoryerrorLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_73;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //console.log(params);
                        params.search.status = "error";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_73 = _a.sent();
                        throw new TypeError(error_73);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistoryerror = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_74;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_74 = _a.sent();
                        throw new TypeError(error_74);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryError = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_75;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_75 = _a.sent();
                        throw new TypeError(error_75);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //baru
    OrderhistoryService.prototype.getOrderhistorywaitingLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_76;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        console.log(result);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report?request={"search": {"status":"PENDING"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_76 = _a.sent();
                        throw new TypeError(error_76);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderhistorywaitingLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_77;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //console.log(params);
                        params.search.status = "PENDING";
                        params.order_by = { request_date: -1 };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_77 = _a.sent();
                        throw new TypeError(error_77);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.setPrintedMark = function (payload) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_78;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'delivery/mark_as_printed';
                        return [4 /*yield*/, this.myService.update(payload, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_78 = _a.sent();
                        throw new TypeError(error_78);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.detailOrderhistorywaiting = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_79;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/' + orderhistory_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_79 = _a.sent();
                        throw new TypeError(error_79);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.updateOrderHistoryWaiting = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_80;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_80 = _a.sent();
                        throw new TypeError(error_80);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getCRR = function (period, params) {
        if (params === void 0) { params = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_81;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-history/report/crr/' + period;
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log("DISINI BRO:", result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_81 = _a.sent();
                        throw new TypeError(error_81);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getCNR = function (period, params) {
        if (params === void 0) { params = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_82;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-history/report/cnr/' + period;
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log("CNR", result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_82 = _a.sent();
                        throw new TypeError(error_82);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.holdMerchant = function (booking_id, param) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_83;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/hold/' + booking_id;
                        return [4 /*yield*/, this.myService.post(param, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_83 = _a.sent();
                        throw new TypeError(error_83);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.holdMerchantRefund = function (booking_id, param) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_84;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/refund/' + booking_id;
                        return [4 /*yield*/, this.myService.post(param, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_84 = _a.sent();
                        throw new TypeError(error_84);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.holdMerchantRelease = function (booking_id, param) {
        return __awaiter(this, void 0, void 0, function () {
            var customHeaders, result, url, error_85;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderhistory/release/' + booking_id;
                        return [4 /*yield*/, this.myService.post(param, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_85 = _a.sent();
                        throw new TypeError(error_85);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //processed sales redemption
    OrderhistoryService.prototype.getRedemptionReportint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_86;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order?request={"search":{"product_status":"process", "status":{"in":["PROCESSED","COMPLETED"]}},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_86 = _a.sent();
                        throw new TypeError(error_86);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchRedemptionReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_87;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.order_by = { request_date: -1 };
                        params.search.product_status = "process";
                        params.search.status = {
                            in: ["PROCESSED", "COMPLETED"]
                        };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_87 = _a.sent();
                        throw new TypeError(error_87);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //canceled sales redemption
    OrderhistoryService.prototype.getRedemptionCanceledReportint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_88;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order?request={"search":{"product_status":"cancel", "status":{"in":["PROCESSED","COMPLETED"]}},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_88 = _a.sent();
                        throw new TypeError(error_88);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchRedemptionCanceledReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_89;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.order_by = { request_date: -1 };
                        params.search.product_status = "cancel";
                        params.search.status = {
                            in: ["PROCESSED", "COMPLETED"]
                        };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_89 = _a.sent();
                        throw new TypeError(error_89);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // SEARCH SALES REDEMPTION FROM EWALLET REPORT
    OrderhistoryService.prototype.searchAllRedemptionPerOrder = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_90;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = "order-invoice/report/sales-order?request={\"search\":{\"order_id\":\"" + params + "\"},\"current_page\":1}";
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_90 = _a.sent();
                        throw new TypeError(error_90);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //Sales Report Evoucher
    OrderhistoryService.prototype.getEvoucherSalesint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_91;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order?request={"search":{"product_type":"evoucher"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_91 = _a.sent();
                        throw new TypeError(error_91);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchEvoucherSalesint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_92;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.product_type = "evoucher";
                        params.order_by = { request_date: -1 };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_92 = _a.sent();
                        throw new TypeError(error_92);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //Sales Report Ewallet
    OrderhistoryService.prototype.getEwalletSalesReportint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_93;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order?request={"search":{"product_type":"topup"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_93 = _a.sent();
                        throw new TypeError(error_93);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchEwalletSalesReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_94;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.product_type = "topup";
                        params.order_by = { request_date: -1 };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_94 = _a.sent();
                        throw new TypeError(error_94);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //Sales Report Ewallet based on Receipt
    OrderhistoryService.prototype.getEwalletReceiptReportint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_95;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order?request={"search":{"based_on":"receipt", "product_type":"topup"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_95 = _a.sent();
                        throw new TypeError(error_95);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchEwalletReceiptReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_96;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.based_on = "receipt";
                        params.search.product_type = "topup";
                        params.order_by = { request_date: -1 };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_96 = _a.sent();
                        throw new TypeError(error_96);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // New BAST
    OrderhistoryService.prototype.getBastReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_97;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report?request={"search":{"status":{"in":["PROCESSED","COMPLETED"]}},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_97 = _a.sent();
                        throw new TypeError(error_97);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchBastReportint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_98;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // console.log(params)
                        params.order_by = { request_date: -1 };
                        params.search.status = {
                            in: ["PROCESSED", "COMPLETED"]
                        };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_98 = _a.sent();
                        throw new TypeError(error_98);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchOrderHistory = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_99;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'delivery/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_99 = _a.sent();
                        throw new TypeError(error_99);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.uploadPackagingBulk = function (file, obj, payload, type) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'delivery/packaging?method=bulk';
                console.warn("url", url);
                this.httpReq = this.http.post(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                    // headers : new HttpHeaders({
                    //   'Authorization': myToken,
                    //   'app-label': 'retailer_prg'
                    // }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            obj.Report = c.body._id;
                            // obj.prodOnUpload = true;
                            if (c.status == 200) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Your file has been uploaded!', 'success');
                            }
                            // obj.prodOnUpload = true;
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    // console.warn("RESULT", result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryService.prototype.uploadShippingProcessBulk = function (file, obj, payload, type) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'delivery/process?method=bulk';
                console.warn("url", url);
                this.httpReq = this.http.post(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                    // headers : new HttpHeaders({
                    //   'Authorization': myToken,
                    //   'app-label': 'retailer_prg'
                    // }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            obj.Report = c.body._id;
                            // obj.prodOnUpload = true;
                            if (c.status == 200) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Your file has been uploaded!', 'success');
                            }
                            // obj.prodOnUpload = true;
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    // console.warn("RESULT", result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryService.prototype.uploadCancelPickupBulk = function (file, obj, payload, type) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'delivery/pickup/cancel?method=bulk';
                console.warn("url", url);
                this.httpReq = this.http.post(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                    // headers : new HttpHeaders({
                    //   'Authorization': myToken,
                    //   'app-label': 'retailer_prg'
                    // }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            obj.Report = c.body._id;
                            // obj.prodOnUpload = true;
                            if (c.status == 200) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Your file has been uploaded!', 'success');
                            }
                            // obj.prodOnUpload = true;
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    // console.warn("RESULT", result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryService.prototype.uploadShippingCompleteBulk = function (file, obj, payload, type) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'delivery/complete?method=bulk';
                this.httpReq = this.http.post(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                    // headers : new HttpHeaders({
                    //   'Authorization': myToken,
                    //   'app-label': 'retailer_prg'
                    // }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            obj.Report = c.body._id;
                            // obj.prodOnUpload = true;
                            if (c.status == 200) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Your file has been uploaded!', 'success');
                            }
                            // obj.prodOnUpload = true;
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    // console.warn("RESULT", result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryService.prototype.uploadShippingProcessCompleteBulk = function (payload) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_100, errorMsg;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/process/complete?method=bulk';
                        return [4 /*yield*/, this.myService.add(payload, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Your file has been uploaded!', 'success');
                        return [3 /*break*/, 4];
                    case 3:
                        error_100 = _a.sent();
                        errorMsg = error_100.message;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', errorMsg, 'warning');
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // Finance Invoice Report
    OrderhistoryService.prototype.getFinanceInvoiceReport = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_101;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order?request={"search":{"product_status":"process", "based_on":"bast", "status":"COMPLETED"},"limit_per_page":50,"current_page":1,"order_by":{"additional_info.bast.created_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_101 = _a.sent();
                        throw new TypeError(error_101);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // Finance Invoice Report Complete without BAST based on
    OrderhistoryService.prototype.getCompleteFinanceInvoiceReport = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_102;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order?request={"search":{"product_status":"process", "status":"COMPLETED"},"limit_per_page":50,"current_page":1,"order_by":{"additional_info.bast.created_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_102 = _a.sent();
                        throw new TypeError(error_102);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchFinanceInvoiceReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_103;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.based_on = "bast";
                        params.search.product_status = "process";
                        if (params.order_by) {
                            params.order_by = { "additional_info.bast.created_date": -1 };
                        }
                        // params.order_by.additonal_info.bast = {created_date:-1};
                        params.search.status = "COMPLETED";
                        console.warn("params order by", params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_103 = _a.sent();
                        throw new TypeError(error_103);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchCompleteFinanceInvoiceReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_104;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (params.order_by) {
                            params.order_by = { "additional_info.bast.created_date": -1 };
                        }
                        // params.order_by.additonal_info.bast = {created_date:-1};
                        params.search.status = "COMPLETED";
                        params.search.product_status = "process";
                        console.warn("params order by", params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_104 = _a.sent();
                        throw new TypeError(error_104);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.getFinanceInvoiceReportByID = function (order_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_105;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order?request={"search":{"order_id":"' + String(order_id) + '", "based_on":"bast", "status":"COMPLETED"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_105 = _a.sent();
                        throw new TypeError(error_105);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.uploadAdditionalImage = function (file, obj, payload, orderID, fileValue) {
        return __awaiter(this, void 0, void 0, function () {
            var fd, programName, extens, newFileName, url;
            var _this = this;
            return __generator(this, function (_a) {
                // let myStatus = localStorage.getItem('isLoggedin');
                // let myToken;
                // if(myStatus){
                //   myToken = localStorage.getItem('tokenlogin');
                // }
                // let fileName = file.name.
                // const extentionsName = 
                console.warn("payload", payload);
                fd = new FormData();
                fd.append('type', payload.type);
                if (file) {
                    programName = localStorage.getItem('programName');
                    extens = file.name.split('.');
                    newFileName = orderID + '-' + programName + '-' + fileValue + '-' + Date.now() + '.' + extens[extens.length - 1];
                    if (_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].production == false)
                        newFileName = 'DEV-' + newFileName;
                    fd.append(fileValue, file, newFileName);
                }
                // else {
                //   fd.append(fileValue,"dummy.png");
                // }
                if (payload.option) {
                    fd.append('option', payload.option);
                    if (payload.id) {
                        fd.append('id', payload.id);
                    }
                    else if (payload.receipt_id) {
                        fd.append('receipt_id', payload.receipt_id);
                    }
                }
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'order-invoice/additional_info?order_id=' + orderID;
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        _this.httpReq = _this.http.put(url, fd, {
                            headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                            reportProgress: true,
                            observe: 'events'
                        }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                            var c;
                            return __generator(this, function (_a) {
                                c = JSON.parse(JSON.stringify(event));
                                if (c) {
                                    if (c.status && c.status == 200) {
                                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Success!', payload.option && payload.option == 'delete' ? 'Hapus gambar berhasil' : 'Your file has been uploaded.', 'success');
                                        resolve(true);
                                    }
                                }
                                return [2 /*return*/];
                            });
                        }); }, function (error) {
                            // console.warn("RESULT error", error);
                            if (error.error) {
                                obj.errorFile = error.error.error;
                                obj.cancel = true;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', error.error.error, 'warning');
                                resolve(false);
                            }
                        });
                    })];
            });
        });
    };
    OrderhistoryService.prototype.uploadInvoiceNumber = function (file, obj, payload, order_id) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('invoice_no', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + ("order-invoice/additional_info?order_id=" + order_id);
                console.log('now', fd);
                this.httpReq = this.http.put(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                    // headers : new HttpHeaders({
                    //   'Authorization': myToken,
                    //   'app-label': 'retailer_prg'
                    // }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        console.log("event", event);
                        console.log(event);
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            console.log('this is body');
                            // obj.firstLoad();
                            // console.warn("c body", c.body)
                            // console.warn("obj", obj)
                            obj.invoiceNumber = file;
                            obj.firstLoad();
                            // obj.prodOnUpload = true;
                            if (c.body && c.body.status == "success") {
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                    title: 'Success',
                                    text: 'Berhasil update No. invoice',
                                    icon: 'success',
                                    confirmButtonText: 'Ok',
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        obj.backToDetail();
                                    }
                                });
                            }
                            //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
                            // obj.process_id = c.body;
                            // console.log("process_id",obj.Report);
                            obj.prodOnUpload = true;
                            // console.log(c.body.failed);
                            // obj.failed = c.body.failed.length;
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    // console.warn("RESULT", result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.invoiceNumber = "";
                        obj.cancel = true;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryService.prototype.updateBulkInvoice = function (file, obj, payload, option) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + ("order-invoice/invoice_no?option=" + option);
                this.httpReq = this.http.put(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        console.log("event", event);
                        console.log(event);
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            obj.Report = c.body._id;
                            // obj.prodOnUpload = true;
                            if (c.status == 200) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Your file has been uploaded!', 'success');
                            }
                            // obj.prodOnUpload = true;
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    //console.log(result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryService.prototype.orderBulk = function (file, obj, payload) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + "order-invoice/order/bulk";
                this.httpReq = this.http.post(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        console.log("event", event);
                        console.log(event);
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            obj.Report = c.body._id;
                            // obj.prodOnUpload = true;
                            if (c.status == 200) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Your file has been uploaded!', 'success');
                            }
                            // obj.prodOnUpload = true;
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    //console.log(result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    // PACKING LIST //
    OrderhistoryService.prototype.getPackingListReport = function (param) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_106;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'packing_list/find?request={"search":{"status":"' + param + '"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_106 = _a.sent();
                        throw new TypeError(error_106);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchPackingList = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_107;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "ACTIVE";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'packing_list/find';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_107 = _a.sent();
                        throw new TypeError(error_107);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchPackingList2 = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_108;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "PROCESSED";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'packing_list/find';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_108 = _a.sent();
                        throw new TypeError(error_108);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.createPackingList = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_109;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'packing_list/create';
                        return [4 /*yield*/, this.myService.post(data, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_109 = _a.sent();
                        throw new TypeError(error_109);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.removePackingList = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_110;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'packing_list/remove';
                        return [4 /*yield*/, this.myService.post(data, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_110 = _a.sent();
                        throw new TypeError(error_110);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.processPackingList = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_111;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'packing_list/process/delivery';
                        return [4 /*yield*/, this.myService.post(data, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_111 = _a.sent();
                        throw new TypeError(error_111);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.searchPackingListDetailReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_112;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.warn("params", params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = "packing_list/find?request={\"search\":{\"packing_no\":\"" + params + "\"},\"current_page\":1}";
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_112 = _a.sent();
                        throw new TypeError(error_112);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.prototype.cancelDigitalProductOrder = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_113;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/cancel/products';
                        return [4 /*yield*/, this.myService.post(data, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_113 = _a.sent();
                        throw new TypeError(error_113);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    OrderhistoryService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
        { type: _master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"] }
    ]; };
    OrderhistoryService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"]])
    ], OrderhistoryService);
    return OrderhistoryService;
}());



/***/ }),

/***/ "./src/app/services/program-name.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/program-name.service.ts ***!
  \**************************************************/
/*! exports provided: ProgramNameService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgramNameService", function() { return ProgramNameService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProgramNameService = /** @class */ (function () {
    function ProgramNameService(titleService) {
        this.titleService = titleService;
        this.programName$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]("");
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_3__;
    }
    ProgramNameService.prototype.currentData = function () {
        return this.programName$.asObservable();
    };
    ProgramNameService.prototype.setData = function (program) {
        if (program)
            localStorage.setItem("programName", program);
        var _this = this;
        this.contentList.filter(function (element) {
            if (element.appLabel == program) {
                _this.changeName(element.title);
            }
        });
    };
    ProgramNameService.prototype.changeName = function (value) {
        this.programName$.next(value);
        this.titleService.setTitle(value);
    };
    ProgramNameService.ctorParameters = function () { return [
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"] }
    ]; };
    ProgramNameService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"]])
    ], ProgramNameService);
    return ProgramNameService;
}());



/***/ }),

/***/ "./src/app/services/storage-service-module/storage-service-module.service.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/services/storage-service-module/storage-service-module.service.ts ***!
  \***********************************************************************************/
/*! exports provided: LocalStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStorageService", function() { return LocalStorageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
// key that is used to access the data in local storageconst 
var STORAGE_KEY = 'local_todolist';
var LocalStorageService = /** @class */ (function () {
    function LocalStorageService() {
        this.anotherTodolist = [];
    }
    LocalStorageService.prototype.storeOnLocalStorage = function (taskTitle) {
        var currentTodoList = false;
    };
    LocalStorageService.prototype.getToken = function () {
        return localStorage.getItem('tokenlogin');
    };
    LocalStorageService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], LocalStorageService);
    return LocalStorageService;
}());



/***/ }),

/***/ "./src/app/shared/guard/auth.guard.ts":
/*!********************************************!*\
  !*** ./src/app/shared/guard/auth.guard.ts ***!
  \********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (localStorage.getItem('isLoggedin')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
    ]; };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/shared/guard/auth.interceptor.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/guard/auth.interceptor.ts ***!
  \**************************************************/
/*! exports provided: AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return AuthInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor() {
        this.expired = false;
    }
    AuthInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        var token = localStorage.getItem('token');
        if (token) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        }
        // if (!request.headers.has('Content-Type')) {
        //     request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        // }
        request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
        console.log(" here");
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (event) {
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]) {
                // console.log('event--->>>', event);
                // this.errorDialogService.openDialog(event);
            }
            return event;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (error) {
            var data = {};
            console.warn(error);
            data = {
                reason: error && error.error && error.error.reason ? error.error.reason : '',
                status: error.status
            };
            if (error.status === 401 && error.error.error == "INV_TOKEN") {
                _this.expired = true;
                if (_this.expired = true) {
                    alert("Akun Anda Sedang Dipakai");
                    _this.expired = false;
                }
                window.location.href = "/login";
            }
            else if (error.status === 400 && error.error.error == "Unauthorized, authorization token expired") {
                _this.expired = true;
                if (_this.expired = true) {
                    alert("Unauthorized, login expired. Please re-login");
                    _this.expired = false;
                }
                window.location.href = "/login";
            }
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
        }));
    };
    AuthInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], AuthInterceptor);
    return AuthInterceptor;
}());



/***/ }),

/***/ "./src/app/shared/guard/index.ts":
/*!***************************************!*\
  !*** ./src/app/shared/guard/index.ts ***!
  \***************************************/
/*! exports provided: AuthGuard, AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.guard */ "./src/app/shared/guard/auth.guard.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return _auth_guard__WEBPACK_IMPORTED_MODULE_0__["AuthGuard"]; });

/* harmony import */ var _auth_interceptor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth.interceptor */ "./src/app/shared/guard/auth.interceptor.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return _auth_interceptor__WEBPACK_IMPORTED_MODULE_1__["AuthInterceptor"]; });





/***/ }),

/***/ "./src/app/shared/index.ts":
/*!*********************************!*\
  !*** ./src/app/shared/index.ts ***!
  \*********************************/
/*! exports provided: SharedPipesModule, PageHeaderModule, StatModule, AuthGuard, AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules */ "./src/app/shared/modules/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PageHeaderModule", function() { return _modules__WEBPACK_IMPORTED_MODULE_0__["PageHeaderModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StatModule", function() { return _modules__WEBPACK_IMPORTED_MODULE_0__["StatModule"]; });

/* harmony import */ var _pipes_shared_pipes_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pipes/shared-pipes.module */ "./src/app/shared/pipes/shared-pipes.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SharedPipesModule", function() { return _pipes_shared_pipes_module__WEBPACK_IMPORTED_MODULE_1__["SharedPipesModule"]; });

/* harmony import */ var _guard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./guard */ "./src/app/shared/guard/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return _guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return _guard__WEBPACK_IMPORTED_MODULE_2__["AuthInterceptor"]; });






/***/ }),

/***/ "./src/app/shared/modules/index.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/modules/index.ts ***!
  \*****************************************/
/*! exports provided: PageHeaderModule, StatModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _page_header_page_header_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./page-header/page-header.module */ "./src/app/shared/modules/page-header/page-header.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PageHeaderModule", function() { return _page_header_page_header_module__WEBPACK_IMPORTED_MODULE_0__["PageHeaderModule"]; });

/* harmony import */ var _stat_stat_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./stat/stat.module */ "./src/app/shared/modules/stat/stat.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StatModule", function() { return _stat_stat_module__WEBPACK_IMPORTED_MODULE_1__["StatModule"]; });





/***/ }),

/***/ "./src/app/shared/modules/page-header/page-header.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/shared/modules/page-header/page-header.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".page-header {\n  color: white;\n  margin-top: 35px;\n}\n\n.top-bar-menu {\n  margin-top: 60px;\n}\n\n.top-bar-menu a {\n  font-size: 14px;\n  padding: 20px;\n  height: 26px;\n  display: inline-flex;\n  justify-content: center;\n  align-items: center;\n  color: white;\n  border-radius: 10px;\n  text-decoration: none;\n  font-weight: bold;\n  margin: 5px;\n  letter-spacing: 1px;\n}\n\n.top-bar-menu a.active {\n  background: #3498db;\n}\n\n.top-bar-menu a.inactive {\n  color: rgba(255, 255, 255, 0.7);\n  background: rgba(52, 152, 219, 0.7);\n  border-left: 1px solid rgba(0, 0, 0, 0.5);\n}\n\n.top-bar-menu a:hover {\n  background: #3498db;\n  color: white;\n}\n\n.breadcrumb {\n  background-color: #3a4458;\n}\n\n.breadcrumb .breadcrumb-item > .fa {\n  color: white;\n  margin-right: 5px;\n}\n\n#dash {\n  color: white;\n}\n\n@media screen and (max-width: 991px) {\n  .top-bar-menu {\n    text-align: center;\n    margin-top: 60px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vZHVsZXMvcGFnZS1oZWFkZXIvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcc2hhcmVkXFxtb2R1bGVzXFxwYWdlLWhlYWRlclxccGFnZS1oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2R1bGVzL3BhZ2UtaGVhZGVyL3BhZ2UtaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUdBLGdCQUFBO0FDREo7O0FESUE7RUFFSSxnQkFBQTtBQ0ZKOztBRElJO0VBQ0ksZUFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQ0ZSOztBREtJO0VBQ0ksbUJBQUE7QUNIUjs7QURNSTtFQUNJLCtCQUFBO0VBQ0EsbUNBQUE7RUFDQSx5Q0FBQTtBQ0pSOztBRFFJO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0FDTlI7O0FEVUE7RUFDSSx5QkFBQTtBQ1BKOztBRFVRO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0FDUlo7O0FEYUE7RUFDSSxZQUFBO0FDVko7O0FEYUE7RUFDSTtJQUNJLGtCQUFBO0lBQ0EsZ0JBQUE7RUNWTjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vZHVsZXMvcGFnZS1oZWFkZXIvcGFnZS1oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGFnZS1oZWFkZXIge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG5cclxuICAgIC8vIG1hcmdpbjogMTBweCAwO1xyXG4gICAgbWFyZ2luLXRvcDozNXB4O1xyXG59XHJcblxyXG4udG9wLWJhci1tZW51IHtcclxuICAgIC8vIG1hcmdpbi1ib3R0b206IDQwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiA2MHB4O1xyXG5cclxuICAgIGEge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgIGhlaWdodDogMjZweDtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBtYXJnaW46IDVweDtcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xyXG4gICAgfVxyXG5cclxuICAgIGEuYWN0aXZlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xyXG4gICAgfVxyXG5cclxuICAgIGEuaW5hY3RpdmUge1xyXG4gICAgICAgIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XHJcbiAgICAgICAgYmFja2dyb3VuZDogcmdiYSg1MiwgMTUyLCAyMTksIDAuNyk7XHJcbiAgICAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuNSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGE6aG92ZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMzNDk4ZGI7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG59XHJcblxyXG4uYnJlYWRjcnVtYiB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2E0NDU4O1xyXG5cclxuICAgIC5icmVhZGNydW1iLWl0ZW0ge1xyXG4gICAgICAgID4gLmZhIHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbiNkYXNoIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkxcHgpIHtcclxuICAgIC50b3AtYmFyLW1lbnUge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiA2MHB4O1xyXG4gICAgfVxyXG59XHJcbiIsIi5wYWdlLWhlYWRlciB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luLXRvcDogMzVweDtcbn1cblxuLnRvcC1iYXItbWVudSB7XG4gIG1hcmdpbi10b3A6IDYwcHg7XG59XG4udG9wLWJhci1tZW51IGEge1xuICBmb250LXNpemU6IDE0cHg7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGhlaWdodDogMjZweDtcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG1hcmdpbjogNXB4O1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xufVxuLnRvcC1iYXItbWVudSBhLmFjdGl2ZSB7XG4gIGJhY2tncm91bmQ6ICMzNDk4ZGI7XG59XG4udG9wLWJhci1tZW51IGEuaW5hY3RpdmUge1xuICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDUyLCAxNTIsIDIxOSwgMC43KTtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuNSk7XG59XG4udG9wLWJhci1tZW51IGE6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5icmVhZGNydW1iIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNhNDQ1ODtcbn1cbi5icmVhZGNydW1iIC5icmVhZGNydW1iLWl0ZW0gPiAuZmEge1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4jZGFzaCB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkxcHgpIHtcbiAgLnRvcC1iYXItbWVudSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDYwcHg7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/shared/modules/page-header/page-header.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/shared/modules/page-header/page-header.component.ts ***!
  \*********************************************************************/
/*! exports provided: PageHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageHeaderComponent", function() { return PageHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PageHeaderComponent = /** @class */ (function () {
    function PageHeaderComponent() {
    }
    PageHeaderComponent.prototype.ngOnInit = function () { };
    PageHeaderComponent.prototype.isActiveClass = function (active) {
        if (active) {
            return 'active';
        }
        else {
            return 'inactive';
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], PageHeaderComponent.prototype, "heading", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], PageHeaderComponent.prototype, "icon", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PageHeaderComponent.prototype, "topBarMenu", void 0);
    PageHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-page-header',
            template: __webpack_require__(/*! raw-loader!./page-header.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/modules/page-header/page-header.component.html"),
            styles: [__webpack_require__(/*! ./page-header.component.scss */ "./src/app/shared/modules/page-header/page-header.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PageHeaderComponent);
    return PageHeaderComponent;
}());



/***/ }),

/***/ "./src/app/shared/modules/page-header/page-header.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/shared/modules/page-header/page-header.module.ts ***!
  \******************************************************************/
/*! exports provided: PageHeaderModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageHeaderModule", function() { return PageHeaderModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _page_header_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./page-header.component */ "./src/app/shared/modules/page-header/page-header.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PageHeaderModule = /** @class */ (function () {
    function PageHeaderModule() {
    }
    PageHeaderModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            declarations: [_page_header_component__WEBPACK_IMPORTED_MODULE_3__["PageHeaderComponent"]],
            exports: [_page_header_component__WEBPACK_IMPORTED_MODULE_3__["PageHeaderComponent"]]
        })
    ], PageHeaderModule);
    return PageHeaderModule;
}());



/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2R1bGVzL3N0YXQvc3RhdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.ts ***!
  \*******************************************************/
/*! exports provided: StatComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatComponent", function() { return StatComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StatComponent = /** @class */ (function () {
    function StatComponent() {
        this.event = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    StatComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "bgClass", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "icon", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], StatComponent.prototype, "count", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "label", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], StatComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], StatComponent.prototype, "event", void 0);
    StatComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stat',
            template: __webpack_require__(/*! raw-loader!./stat.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/modules/stat/stat.component.html"),
            styles: [__webpack_require__(/*! ./stat.component.scss */ "./src/app/shared/modules/stat/stat.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StatComponent);
    return StatComponent;
}());



/***/ }),

/***/ "./src/app/shared/modules/stat/stat.module.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.module.ts ***!
  \****************************************************/
/*! exports provided: StatModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatModule", function() { return StatModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _stat_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./stat.component */ "./src/app/shared/modules/stat/stat.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var StatModule = /** @class */ (function () {
    function StatModule() {
    }
    StatModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            declarations: [_stat_component__WEBPACK_IMPORTED_MODULE_2__["StatComponent"]],
            exports: [_stat_component__WEBPACK_IMPORTED_MODULE_2__["StatComponent"]]
        })
    ], StatModule);
    return StatModule;
}());



/***/ }),

/***/ "./src/app/shared/pipes/shared-pipes.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/pipes/shared-pipes.module.ts ***!
  \*****************************************************/
/*! exports provided: SharedPipesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedPipesModule", function() { return SharedPipesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var SharedPipesModule = /** @class */ (function () {
    function SharedPipesModule() {
    }
    SharedPipesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
            ],
            declarations: []
        })
    ], SharedPipesModule);
    return SharedPipesModule;
}());



/***/ }),

/***/ "./src/assets/json/content.json":
/*!**************************************!*\
  !*** ./src/assets/json/content.json ***!
  \**************************************/
/*! exports provided: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, default */
/***/ (function(module) {

module.exports = [{"appLabel":"admin","title":"Member Management","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1631686845/admin/image/Member.png614190bc75c76.png","type":""},{"appLabel":"sig_promotion_2","title":"PROMOTION CORPORATE SALES SEMESTER 2 2021","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1633061901/admin/image/semengrup.png61568c0c4e5e8.png","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"custom_kontraktual"},{"appLabel":"sig_kontraktual","title":"PROMOTION CORPORATE SALES SEMESTER 1 2021","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1633061901/admin/image/semengrup.png61568c0c4e5e8.png","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"custom_kontraktual"},{"appLabel":"dynamix_sbi","title":"Dynamix Extra Poin 2020","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1631515839/admin/image/logo-dynamix-vector.png613ef4beb0a74.png","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"custom"},{"appLabel":"retail_poin_sahabat","title":"PROGRAM TACTICAL SIG","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1633061901/admin/image/semengrup.png61568c0c4e5e8.png","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"custom"},{"appLabel":"retail_mci","title":"MCI","description":"","imageUrl":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631590784/admin/image/mci-logo-8bb8b0f.6140197f82abc.jpg","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"reguler"},{"appLabel":"retail_tum","title":"Timur Usaha Mandiri","description":"","imageUrl":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631591068/admin/image/mstr1-min.61401a9ae74b1.png","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"reguler"},{"appLabel":"retail_home_credit","title":"Home Credit","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1631515767/admin/image/logo-home-credit.jpeg613ef476a5ecf.jpg","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"reguler"},{"appLabel":"retail_icbc","title":"ICBC","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1631515590/admin/image/logo-icbc.jpeg613ef3c4ea617.jpg","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"reguler"},{"appLabel":"retail_jne","title":"JNE","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1631510889/admin/image/logo-jne.jpg613ee167c5ac2.jpg","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"reguler"},{"appLabel":"retail_mondelez","title":"Mondelez","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1631515704/admin/image/logo-mondelez.png613ef43818fc5.png","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"reguler"},{"appLabel":"retail_ngk_busi","title":"Ngk Spark Plugs","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1631510853/admin/image/logo-ngk.jpg613ee14405c92.jpg","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"reguler"},{"appLabel":"retail_uob","title":"UOB","description":"","imageUrl":"https://res.cloudinary.com/pt-cls-system/image/upload/v1631510782/admin/image/logo-uob.png613ee0fccdbdd.png","jumbotron":"https://res.cloudinary.com/dqcj36zfd/image/upload/v1631602851/admin/image/poin-sahabat-bg-1.614048a1e4369.png","type":"reguler"}];

/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment, api_url, getHost, getAPIKeyHeader, getLink, getTokenHeader, getCustomTokenHeader, getTokenHeaderRetailer, errorConvertToMessage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "api_url", function() { return api_url; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHost", function() { return getHost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAPIKeyHeader", function() { return getAPIKeyHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLink", function() { return getLink; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTokenHeader", function() { return getTokenHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCustomTokenHeader", function() { return getCustomTokenHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTokenHeaderRetailer", function() { return getTokenHeaderRetailer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "errorConvertToMessage", function() { return errorConvertToMessage; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// API UNTUK DEVELOPMENT (41418C656335D6BA2B58C9F5B404CCF0B894070466F0D038D6B991F9F67B70B6)
// API UNTUK B2B (825DBDE1417DF646BC636FD53B3F6A1D3979D9FBD17496717DBFBD76F1CBAE17)
// API UNTUK MALAYSIA (FAABE444EF1AC28216096734AF78EFB08B99DE3A8629FED4FA849B7DF9716EF1)
// API SIG QUICKWIN(DEV POIN SAHABAT) A490D9FD3F1DB33C94C7E484BB03CBE93C4830EE9D4389F78070ED24C9AC1DBF
// API SIG POIN SAHABAT 87E12038912E1E8387F99677EA16387B6FB6DE93929055D519561DB4AD5149A6
var environment = {
    production: false
};
function api_url() {
    return getHost() + 'v1/';
    // return 'http://localhost/locard/f3/v1/';
}
function getHost() {
    // return 'http://192.168.56.4/f3/';
    // return 'https://api2.dev.locard.co.id/f3/';
    return 'http://api.dev.retailer1.form-sig.id/';
}
function getAPIKeyHeader() {
    //Locard API Key
    // let Api_Key = '87E12038912E1E8387F99677EA16387B6FB6DE93929055D519561DB4AD5149A6'
    var Api_Key = 'A490D9FD3F1DB33C94C7E484BB03CBE93C4830EE9D4389F78070ED24C9AC1DBF';
    var customHeaders;
    customHeaders = { 'Api_key': Api_Key };
    return customHeaders;
    customHeaders;
}
function getLink() {
    var program = localStorage.getItem('programName');
    return 'http://dev.redemption.reals.cls-indo.co.id';
    // const program = localStorage.getItem('programName');
    // if (program == 'dynamix') {
    // } else if (program == 'MCI') {
    //   return 'http://redemption.reals.cls-indo.co.id';
    //   // return 'http://dev.mci.cls-indo.co.id/';
    // }
}
function getTokenHeader() {
    var myToken = localStorage.getItem('tokenlogin');
    var customHeaders = { 'Authorization': myToken };
    return customHeaders;
}
function getCustomTokenHeader(app_label) {
    var myToken = localStorage.getItem('tokenlogin');
    var customHeaders = { 'Authorization': myToken, 'app-label': app_label };
    return customHeaders;
}
function getTokenHeaderRetailer() {
    var myToken = localStorage.getItem('tokenlogin');
    var program = localStorage.getItem('programName');
    // console.warn("program", program);
    var appLabel;
    if (program == 'dynamix_sbi') {
        appLabel = "retailer_prg";
    }
    else {
        appLabel = program;
    }
    var customHeaders = { 'Authorization': myToken, 'app-label': appLabel };
    return customHeaders;
}
function errorConvertToMessage(errorMessage) {
    errorMessage = errorMessage.replace("Error:", "");
    var errorJSON = JSON.parse(errorMessage);
    return errorJSON.error;
}


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
    if (window) {
        window.console.log = function () { };
    }
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])()
    .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\aisyah\cls\angular-admin-sig-v.2.0\src\main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!************************!*\
  !*** stream (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 4:
/*!**********************!*\
  !*** zlib (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 5:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 6:
/*!**********************!*\
  !*** http (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 7:
/*!***********************!*\
  !*** https (ignored) ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 8:
/*!*********************!*\
  !*** url (ignored) ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map