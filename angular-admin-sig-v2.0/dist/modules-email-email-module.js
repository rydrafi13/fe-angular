(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-email-email-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/email/add/email.add.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/email/add/email.add.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'Add New Email'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  \r\n <div>\r\n    <form #form=\"ngForm\" (ngSubmit)=\"formSubmitAddEmail(form.value)\">\r\n      <div class=\"col-md-6\">\r\n          <label>Sender</label>\r\n          <form-input name=\"sender_email\" [type]=\"'text'\" [placeholder]=\"'Sender'\"  [(ngModel)]=\"sender_email\" autofocus required></form-input>\r\n\r\n          <label>Recipient</label>\r\n          <form-input name=\"recipient_email\" [type]=\"'text'\" [placeholder]=\"'Recipient'\"  [(ngModel)]=\"recipient_email\" autofocus required></form-input>\r\n         \r\n          <label>CC Recipient</label>\r\n          <form-input name=\"CC\" [type]=\"'text'\" [placeholder]=\"'CC Recipient'\"  [(ngModel)]=\"CC\" autofocus required></form-input>\r\n\r\n          <label>Subject</label>\r\n          <form-input name=\"subject\" [type]=\"'text'\" [placeholder]=\"'Subject'\"  [(ngModel)]=\"subject\" autofocus required></form-input>\r\n\r\n          <label>Body</label>\r\n          <form-input name=\"body\" [type]=\"'text'\" [placeholder]=\"'Body'\"  [(ngModel)]=\"body\" autofocus required></form-input>\r\n          \r\n          <label>Email Type</label>\r\n          <form-input name=\"email_type\" [type]=\"'text'\" [placeholder]=\"'Email Type'\"  [(ngModel)]=\"email_type\" autofocus required></form-input>\r\n\r\n        </div>  \r\n      <button class=\"btn\" type=\"submit\" [disabled]=\"form.pristine\">Submit</button> \r\n      </form>\r\n </div>\r\n \r\n</div>\r\n\r\n "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/email/detail/email.detail.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/email/detail/email.detail.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail._id}}</h1>\r\n        <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button>\r\n    </div> \r\n    <div class=\"card-content\">\r\n        <div class=\"email-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> Email Details </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             <label>Email ID</label>\r\n                             <div name=\"_id\" >{{detail._id}}</div>\r\n                             <label>Sender</label>\r\n                             <div name=\"sender_email\" >{{detail.sender_email}}</div>\r\n                             <label>Recipient</label>\r\n                             <div name=\"recipient_email\" >{{detail.recipient_email}}</div>\r\n                             <label>CC Recipient</label>\r\n                             <div name=\"CC\" >{{detail.CC}}</div>\r\n                             <label>Subject</label>\r\n                             <div name=\"subject\" >{{detail.subject}}</div>\r\n                             <label>Body</label>\r\n                             <div name=\"body\" >{{detail.body}}</div>\r\n                             <label>Email Type</label>\r\n                             <div name=\"email_type\" >{{detail.email_type}}</div>\r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                </div>\r\n                <!-- <div class=\" col-md-4\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                           \r\n                             \r\n                         </div>\r\n                       \r\n                      </div> \r\n                 </div>\r\n                 \r\n                </div> -->\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12 bg-delete\"><button class=\"btn delete-button float-right btn-danger\" (click)=\"deleteThis()\">delete this</button></div>\r\n            </div>\r\n         </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-email-edit [back]=\"[this, 'editThis']\" [detail]=\"detail\"></app-email-edit>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/email/edit/email.edit.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/email/edit/email.edit.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail._id}}</h1>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span></button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"email-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\" col-md-8\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2> Edit Email Details </h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             <label>Email ID</label>\r\n                             <form-input  name=\"_id\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Email ID'\"  [(ngModel)]=\"detail._id\" autofocus required></form-input>\r\n             \r\n                             <label>Sender</label>\r\n                             <form-input name=\"sender_email\" [type]=\"'text'\" [placeholder]=\"'Sender'\"  [(ngModel)]=\"detail.sender_email\" autofocus required></form-input>\r\n             \r\n                             <label>Recipient</label>\r\n                             <form-input name=\"recipient_email\" [type]=\"'text'\" [placeholder]=\"'Recipient'\"  [(ngModel)]=\"detail.recipient_email\" autofocus required></form-input>\r\n                            \r\n                             <label>CC Recipient</label>\r\n                             <form-input name=\"CC\" [type]=\"'text'\" [placeholder]=\"'CC Recipient'\"  [(ngModel)]=\"detail.CC\" autofocus required></form-input>\r\n\r\n                             <label>Subject</label>\r\n                             <form-input name=\"subject\" [type]=\"'text'\" [placeholder]=\"'Subject'\"  [(ngModel)]=\"detail.subject\" autofocus required></form-input>\r\n\r\n                             <label>Body</label>\r\n                             <form-input name=\"Body\" [type]=\"'text'\" [placeholder]=\"'Body'\"  [(ngModel)]=\"detail.Body\" autofocus required></form-input>\r\n                             \r\n                             <label>Email Type</label>\r\n                             <form-input name=\"email_type\" [type]=\"'text'\" [placeholder]=\"'Email Type'\"  [(ngModel)]=\"detail.email_type\" autofocus required></form-input>\r\n                         </div>\r\n                         <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                        </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                </div>\r\n                <!-- <div class=\" col-md-4\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Email </label>\r\n                            <form-input name=\"email\" [type]=\"'text'\" [placeholder]=\"'Email'\"  [(ngModel)]=\"detail.email\" autofocus required></form-input>\r\n\r\n                            <label>Cell Phone </label>\r\n                            <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Cell Phone'\"  [(ngModel)]=\"detail.cell_phone\" autofocus required></form-input>\r\n                            \r\n                            <label>Registration Date </label>\r\n                            <form-input  name=\"registration_date\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Registration Date'\"  [(ngModel)]=\"detail.registration_date\" autofocus required></form-input>\r\n\r\n                            <label>Point Balance </label>\r\n                            <form-input  name=\"point_balance\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Point Balance'\"  [(ngModel)]=\"detail.point_balance\" autofocus required></form-input>\r\n\r\n                            <label>Activation Code </label>\r\n                            <form-input  name=\"activation_code\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Activation Code'\"  [(ngModel)]=\"detail.activation_code\" autofocus required></form-input>\r\n\r\n                            <label>Activation Status </label>\r\n                            <div name=\"activation_status\"> {{activation_status}}\r\n                                <form-select name=\"activation_status\" [(ngModel)]=\"detail.activation_status\" [data]=\"activationStatus\" required></form-select> \r\n                             </div> \r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div> \r\n                 \r\n                </div>-->\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    \r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/email/email.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/email/email.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n  <app-page-header [heading]=\"'Email'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n  <div *ngIf=\"Email&&emailDetail==false\">\r\n        <app-form-builder-table\r\n        [table_data]  = \"Email\"\r\n        [searchCallback]= \"[service, 'searchEmailLint',this]\"\r\n        [tableFormat] = \"tableFormat\"\r\n        >\r\n\r\n        </app-form-builder-table>\r\n  </div>\r\n  <div *ngIf=\"emailDetail\">\r\n    <app-email-detail [back]=\"[this,backToHere]\" [detail]=\"emailDetail\"></app-email-detail>\r\n</div>\r\n\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/modules/email/add/email.add.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/modules/email/add/email.add.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n.pb-framer .clr-white {\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZW1haWwvYWRkL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZW1haWxcXGFkZFxcZW1haWwuYWRkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9lbWFpbC9hZGQvZW1haWwuYWRkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDQ0o7QURBSTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDRVI7QURBSTtFQUNJLHFCQUFBO0FDRVI7QURBSTtFQUNJLFlBQUE7QUNFUiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2VtYWlsL2FkZC9lbWFpbC5hZGQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGItZnJhbWVye1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgY29sb3I6ICNlNjdlMjI7XHJcbiAgICAucHJvZ3Jlc3NiYXJ7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgei1pbmRleDogLTE7XHJcbiAgICB9XHJcbiAgICAudGVybWluYXRlZC5wcm9ncmVzc2JhcntcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbiAgICB9XHJcbiAgICAuY2xyLXdoaXRle1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxufSIsIi5wYi1mcmFtZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiAyNXB4O1xuICBjb2xvcjogI2U2N2UyMjtcbn1cbi5wYi1mcmFtZXIgLnByb2dyZXNzYmFyIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYmFja2dyb3VuZDogIzM0OThkYjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDI1cHg7XG4gIHotaW5kZXg6IC0xO1xufVxuLnBiLWZyYW1lciAudGVybWluYXRlZC5wcm9ncmVzc2JhciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbn1cbi5wYi1mcmFtZXIgLmNsci13aGl0ZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/email/add/email.add.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/modules/email/add/email.add.component.ts ***!
  \*****************************************************************/
/*! exports provided: EmailAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailAddComponent", function() { return EmailAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_email_email_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/email/email.service */ "./src/app/services/email/email.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var EmailAddComponent = /** @class */ (function () {
    function EmailAddComponent(emailService) {
        this.emailService = emailService;
        this.name = "";
        this.Email = [];
        this.email_type = [
            { label: "ACTIVE", value: "ACTIVE", selected: 1 },
            { label: "INACTIVE", value: "INACTIVE" }
        ];
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
        this.errorLabel = false;
        var form_add = [
            { label: "sender_email", type: "text", value: "", data_binding: 'sender_email' },
            { label: "recipient_email", type: "text", value: "", data_binding: 'recipient_email' },
            { label: "CC", type: "text", value: "", data_binding: 'CC' },
            { label: "subject", type: "text", value: "", data_binding: 'subject' },
            { label: "body", type: "text", value: "", data_binding: 'body' },
            { label: "email_type", type: "text", value: "", data_binding: 'email_type' },
        ];
    }
    EmailAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EmailAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    EmailAddComponent.prototype.formSubmitAddEmail = function (form) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.emailService;
                        return [4 /*yield*/, this.emailService.addNewEmail(form)];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        this.Email = result.result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /* onFileSelected(event){
      try
      {
        this.errorFile = false;
        let fileMaxSize = 3000000;// let say 3Mb
        Array.from(event.target.files).forEach((file: any) => {
            if(file.size > fileMaxSize){
              this.errorFile="Maximum File Upload is 3MB";
            }
        });
        this.selectedFile = event.target.files[0];
      }
      catch (e)
      {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
    }  */
    EmailAddComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    EmailAddComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    EmailAddComponent.ctorParameters = function () { return [
        { type: _services_email_email_service__WEBPACK_IMPORTED_MODULE_2__["EmailService"] }
    ]; };
    EmailAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-email-add',
            template: __webpack_require__(/*! raw-loader!./email.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/email/add/email.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./email.add.component.scss */ "./src/app/layout/modules/email/add/email.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_email_email_service__WEBPACK_IMPORTED_MODULE_2__["EmailService"]])
    ], EmailAddComponent);
    return EmailAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/email/detail/email.detail.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/layout/modules/email/detail/email.detail.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .email-detail label {\n  margin-top: 10px;\n}\n.card-detail .email-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .email-detail .card-header {\n  background-color: #555;\n}\n.card-detail .email-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZW1haWwvZGV0YWlsL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZW1haWxcXGRldGFpbFxcZW1haWwuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9lbWFpbC9kZXRhaWwvZW1haWwuZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksa0JBQUE7QUNBUjtBRENRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0FaO0FEQ1k7RUFDSSxpQkFBQTtBQ0NoQjtBREVRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTFo7QURFWTtFQUNJLGlCQUFBO0FDQWhCO0FES0k7RUFDSSxhQUFBO0FDSFI7QURJUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNGWjtBRGlCSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDZlI7QURrQlE7RUFDSSxnQkFBQTtBQ2hCWjtBRGtCUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNoQlo7QURtQlE7RUFDSSxzQkFBQTtBQ2pCWjtBRGtCWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDaEJoQiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2VtYWlsL2RldGFpbC9lbWFpbC5kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmVkaXRfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIC5pbWFnZXtcclxuICAgIC8vICAgICBoM3tcclxuICAgIC8vICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgLy8gICAgIH1cclxuICAgIC8vICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgLy8gICAgICAgICBpbWd7XHJcbiAgICAvLyAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAvLyAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAvLyAgICAgICAgIH1cclxuICAgIC8vICAgICB9XHJcbiAgICAvLyB9XHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5lbWFpbC1kZXRhaWx7XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLmVtYWlsLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLmVtYWlsLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAuZW1haWwtZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLmVtYWlsLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/email/detail/email.detail.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/email/detail/email.detail.component.ts ***!
  \***********************************************************************/
/*! exports provided: EmailDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailDetailComponent", function() { return EmailDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_email_email_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/email/email.service */ "./src/app/services/email/email.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var EmailDetailComponent = /** @class */ (function () {
    function EmailDetailComponent(emailService) {
        this.emailService = emailService;
        this.errorLabel = false;
        this.edit = false;
    }
    EmailDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EmailDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    EmailDetailComponent.prototype.editThis = function () {
        console.log(this.edit);
        this.edit = !this.edit;
        console.log(this.edit);
    };
    EmailDetailComponent.prototype.backToTable = function () {
        console.log(this.back);
        this.back[1](this.back[0]);
    };
    EmailDetailComponent.prototype.deleteThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var delResult, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.emailService.delete(this.detail)];
                    case 1:
                        delResult = _a.sent();
                        //console.log(delResult);
                        if (delResult.error == false) {
                            console.log(this.back[0]);
                            this.back[0].emailDetail = false;
                            this.back[0].firstLoad();
                            // delete this.back[0].emailDetail;
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EmailDetailComponent.ctorParameters = function () { return [
        { type: _services_email_email_service__WEBPACK_IMPORTED_MODULE_2__["EmailService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EmailDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EmailDetailComponent.prototype, "back", void 0);
    EmailDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-email-detail',
            template: __webpack_require__(/*! raw-loader!./email.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/email/detail/email.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./email.detail.component.scss */ "./src/app/layout/modules/email/detail/email.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_email_email_service__WEBPACK_IMPORTED_MODULE_2__["EmailService"]])
    ], EmailDetailComponent);
    return EmailDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/email/edit/email.edit.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/email/edit/email.edit.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .email-detail label {\n  margin-top: 10px;\n}\n.card-detail .email-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .email-detail .card-header {\n  background-color: #555;\n}\n.card-detail .email-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZW1haWwvZWRpdC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGVtYWlsXFxlZGl0XFxlbWFpbC5lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9lbWFpbC9lZGl0L2VtYWlsLmVkaXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtBQ0FSO0FEQ1E7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDQVo7QURDWTtFQUNJLGlCQUFBO0FDQ2hCO0FERVE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUtBLFdBQUE7QUNMWjtBREVZO0VBQ0ksaUJBQUE7QUNBaEI7QURJUTtFQUNJLHlCQUFBO0FDRlo7QURLSTtFQUNJLGFBQUE7QUNIUjtBREtRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0haO0FET0k7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0xSO0FEU1E7RUFDSSxnQkFBQTtBQ1BaO0FEU1E7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDUFo7QURvQ1E7RUFDSSxzQkFBQTtBQ2xDWjtBRG1DWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDakNoQiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2VtYWlsL2VkaXQvZW1haWwuZWRpdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9uLnRydWV7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgXHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5lbWFpbC1kZXRhaWx7XHJcbiAgICAgICBcclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gLmltYWdle1xyXG4gICAgICAgIC8vICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIC8vICAgICA+ZGl2e1xyXG4gICAgICAgIC8vICAgICAgICAgd2lkdGg6IDMzLjMzMzMlO1xyXG4gICAgICAgIC8vICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgIC8vICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgICAgLmNhcmQtaGVhZGVye1xyXG4gICAgICAgIC8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vICAgICBoM3tcclxuICAgICAgICAvLyAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAvLyAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAvLyAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgLy8gICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIC8vICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgLy8gICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgLy8gICAgICAgICBpbWd7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICAgICAgLy8gICAgICAgICB9XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5lbWFpbC1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5lbWFpbC1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLmVtYWlsLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5lbWFpbC1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/email/edit/email.edit.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/layout/modules/email/edit/email.edit.component.ts ***!
  \*******************************************************************/
/*! exports provided: EmailEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailEditComponent", function() { return EmailEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_email_email_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/email/email.service */ "./src/app/services/email/email.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var EmailEditComponent = /** @class */ (function () {
    function EmailEditComponent(emailService) {
        this.emailService = emailService;
        this.loading = false;
        this.transactionStatus = [
            { label: "PENDING", value: "PENDING" },
            { label: "CANCEL", value: "CANCEL" },
            { label: "FAILED", value: "FAILED" },
            { label: "SUCCESS", value: "SUCCESS" },
            { label: "ERROR", value: "ERROR" }
        ];
        this.errorLabel = false;
    }
    EmailEditComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EmailEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.detail.previous_status = this.detail.status;
                this.transactionStatus.forEach(function (element, index) {
                    if (element.value == _this.detail.transaction_status) {
                        _this.transactionStatus[index].selected = 1;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    EmailEditComponent.prototype.backToDetail = function () {
        //console.log(this.back);
        this.back[0][this.back[1]]();
    };
    EmailEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.loading = !this.loading;
                        return [4 /*yield*/, this.emailService.updateEmailID(this.detail)];
                    case 1:
                        _a.sent();
                        this.loading = !this.loading;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EmailEditComponent.ctorParameters = function () { return [
        { type: _services_email_email_service__WEBPACK_IMPORTED_MODULE_2__["EmailService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EmailEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EmailEditComponent.prototype, "back", void 0);
    EmailEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-email-edit',
            template: __webpack_require__(/*! raw-loader!./email.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/email/edit/email.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./email.edit.component.scss */ "./src/app/layout/modules/email/edit/email.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_email_email_service__WEBPACK_IMPORTED_MODULE_2__["EmailService"]])
    ], EmailEditComponent);
    return EmailEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/email/email-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/modules/email/email-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: EmailRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailRoutingModule", function() { return EmailRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _email_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./email.component */ "./src/app/layout/modules/email/email.component.ts");
/* harmony import */ var _add_email_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/email.add.component */ "./src/app/layout/modules/email/add/email.add.component.ts");
/* harmony import */ var _detail_email_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detail/email.detail.component */ "./src/app/layout/modules/email/detail/email.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '', component: _email_component__WEBPACK_IMPORTED_MODULE_2__["EmailComponent"],
    },
    {
        path: 'add', component: _add_email_add_component__WEBPACK_IMPORTED_MODULE_3__["EmailAddComponent"]
    },
    {
        path: 'detail', component: _detail_email_detail_component__WEBPACK_IMPORTED_MODULE_4__["EmailDetailComponent"]
    }
];
var EmailRoutingModule = /** @class */ (function () {
    function EmailRoutingModule() {
    }
    EmailRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], EmailRoutingModule);
    return EmailRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/email/email.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/layout/modules/email/email.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2VtYWlsL2VtYWlsLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/modules/email/email.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/layout/modules/email/email.component.ts ***!
  \*********************************************************/
/*! exports provided: EmailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailComponent", function() { return EmailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_email_email_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/email/email.service */ "./src/app/services/email/email.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var EmailComponent = /** @class */ (function () {
    function EmailComponent(emailService) {
        this.emailService = emailService;
        this.Email = [];
        this.tableFormat = {
            title: 'Active Email Detail',
            label_headers: [
                { label: 'Sender', visible: true, type: 'string', data_row_name: 'sender_email' },
                { label: 'Recipient', visible: true, type: 'string', data_row_name: 'recipient_email' },
                { label: 'CC Recipient', visible: true, type: 'string', data_row_name: 'CC' },
                { label: 'Subject', visible: true, type: 'string', data_row_name: 'subject' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'Email',
                detail_function: [this, 'callDetail'],
            }
        };
        this.row_id = "_id";
        this.errorLabel = false;
        this.emailDetail = false;
    }
    EmailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EmailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.emailService;
                        return [4 /*yield*/, this.emailService.getEmailLint()];
                    case 1:
                        result = _a.sent();
                        //console.log(result.result);
                        this.Email = result.result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EmailComponent.prototype.callDetail = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.emailService;
                        return [4 /*yield*/, this.emailService.detailEmail(_id)];
                    case 1:
                        result = _a.sent();
                        //console.log(result);
                        this.emailDetail = result.result[0];
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EmailComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.emailDetail = false;
                return [2 /*return*/];
            });
        });
    };
    EmailComponent.ctorParameters = function () { return [
        { type: _services_email_email_service__WEBPACK_IMPORTED_MODULE_2__["EmailService"] }
    ]; };
    EmailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-email',
            template: __webpack_require__(/*! raw-loader!./email.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/email/email.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./email.component.scss */ "./src/app/layout/modules/email/email.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_email_email_service__WEBPACK_IMPORTED_MODULE_2__["EmailService"]])
    ], EmailComponent);
    return EmailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/email/email.module.ts":
/*!******************************************************!*\
  !*** ./src/app/layout/modules/email/email.module.ts ***!
  \******************************************************/
/*! exports provided: EmailModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailModule", function() { return EmailModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _email_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./email.component */ "./src/app/layout/modules/email/email.component.ts");
/* harmony import */ var _add_email_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/email.add.component */ "./src/app/layout/modules/email/add/email.add.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _detail_email_detail_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./detail/email.detail.component */ "./src/app/layout/modules/email/detail/email.detail.component.ts");
/* harmony import */ var _email_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./email-routing.module */ "./src/app/layout/modules/email/email-routing.module.ts");
/* harmony import */ var _edit_email_edit_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./edit/email.edit.component */ "./src/app/layout/modules/email/edit/email.edit.component.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var EmailModule = /** @class */ (function () {
    function EmailModule() {
    }
    EmailModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _email_routing_module__WEBPACK_IMPORTED_MODULE_9__["EmailRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_11__["BsComponentModule"]
            ],
            declarations: [
                _email_component__WEBPACK_IMPORTED_MODULE_2__["EmailComponent"], _add_email_add_component__WEBPACK_IMPORTED_MODULE_3__["EmailAddComponent"], _detail_email_detail_component__WEBPACK_IMPORTED_MODULE_8__["EmailDetailComponent"], _edit_email_edit_component__WEBPACK_IMPORTED_MODULE_10__["EmailEditComponent"]
            ]
        })
    ], EmailModule);
    return EmailModule;
}());



/***/ }),

/***/ "./src/app/services/email/email.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/email/email.service.ts ***!
  \*************************************************/
/*! exports provided: EmailService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailService", function() { return EmailService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _master_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../master.service */ "./src/app/services/master.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var EmailService = /** @class */ (function () {
    function EmailService(myService) {
        this.myService = myService;
        this.gerro = 'test';
    }
    EmailService.prototype.getEmailLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'emails/all';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EmailService.prototype.addNewEmail = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'emails/';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EmailService.prototype.updateEmail = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'emails/all';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EmailService.prototype.detailEmail = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'emails/' + _id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EmailService.prototype.deleteEmail = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'emails/all';
                        return [4 /*yield*/, this.myService.delete(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EmailService.prototype.searchEmailLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'emails/all';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EmailService.prototype.updateEmailID = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'emails/update/' + params.email_id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    EmailService.prototype.delete = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var url, result, customHeaders, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'emails/remove/' + params.email_id;
                        return [4 /*yield*/, this.myService.delete(params, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        throw new TypeError(error_8);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EmailService.ctorParameters = function () { return [
        { type: _master_service__WEBPACK_IMPORTED_MODULE_1__["MasterService"] }
    ]; };
    EmailService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_master_service__WEBPACK_IMPORTED_MODULE_1__["MasterService"]])
    ], EmailService);
    return EmailService;
}());



/***/ })

}]);
//# sourceMappingURL=modules-email-email-module.js.map