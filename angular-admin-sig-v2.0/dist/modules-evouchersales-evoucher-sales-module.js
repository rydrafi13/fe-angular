(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-evouchersales-evoucher-sales-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evouchersales/detail/evoucher-sales.detail.component.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/evouchersales/detail/evoucher-sales.detail.component.html ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"salesRedemptionDetail\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToHere()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n    </div>\r\n\r\n    <ng-template #empty_image><span>\r\n        empty\r\n    </span></ng-template>\r\n  \r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Order</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <label>Tanggal Proses</label>\r\n                                <div name=\"order_id\">{{salesRedemptionDetail.approve_date}}</div>\r\n\r\n                                <label>Order ID</label>\r\n                                <div name=\"order_id\">{{salesRedemptionDetail.order_id}}</div>\r\n\r\n                                <label>Total Quantity</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.total_quantity}}</div>\r\n\r\n                                <label>Total Points Redeem</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.sum_total}}</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Pelanggan</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>ID Pelanggan</label>\r\n                                <div name=\"order_id\">{{salesRedemptionDetail.id_toko}}</div>\r\n\r\n                                <label>Nama Entitas</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.nama_toko}}</div>\r\n\r\n                                <label>Nama PIC</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.nama_pemilik}}</div>\r\n\r\n                                <label>Alamat Kirim</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.address_detail.address}}</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Status Order</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>Status</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.status}}</div>\r\n                                \r\n                                <!-- <div class=\"need-approval-text\" *ngIf=\"salesRedemptionDetail.status == 'REQUESTED' && !processNow && !declineNow\">\r\n                                    <span>This request needs approval</span>\r\n                                </div> -->\r\n\r\n                                <div class=\"need-approval-text\" *ngIf=\"salesRedemptionDetail.status == 'PROCESSED' && processNow && !declineNow\">\r\n                                    <span>Are you sure to complete this order?</span>\r\n                                </div>\r\n\r\n                                <!-- <div class=\"need-approval-text\" *ngIf=\"salesRedemptionDetail.status == 'REQUESTED' && !processNow && declineNow\">\r\n                                    <span>Are you sure to decline this request?</span>\r\n                                </div> -->\r\n\r\n                                <div style=\"display: flex;\">\r\n                                    <div class=\"btn-action-stock\" *ngIf=\"salesRedemptionDetail.status == 'PROCESSED' && !processNow && !declineNow\">\r\n                                        <button class=\"btn btn-approve\" (click)=\"isProcessNow()\">Complete this Order</button>\r\n                                    </div>\r\n    \r\n                                    <!-- <div class=\"btn-action-stock\" *ngIf=\"salesRedemptionDetail.status == 'REQUESTED' && !processNow && !declineNow\">\r\n                                        <button class=\"btn btn-decline\" (click)=\"isDeclineNow()\">Decline</button>\r\n                                    </div> -->\r\n                                </div>\r\n\r\n                                <div class=\"btn-action-stock\" *ngIf=\"salesRedemptionDetail.status == 'PROCESSED' && processNow && !declineNow\">\r\n                                    <button class=\"btn btn-approve\" (click)=\"completeOrder(salesRedemptionDetail.order_id)\">Complete Now</button>\r\n                                    <button class=\"btn btn-cancel\" (click)=\"isProcessNow()\">Cancel</button>\r\n                                </div>\r\n\r\n                                <!-- <div class=\"btn-action-stock\" *ngIf=\"salesRedemptionDetail.status == 'REQUESTED' && !processNow && declineNow\">\r\n                                    <button class=\"btn btn-decline\" (click)=\"completeOrder(salesRedemptionDetail.order_id, 'cancel')\">Decline Now</button>\r\n                                    <button class=\"btn btn-cancel\" (click)=\"isDeclineNow()\">Cancel</button>\r\n                                </div> -->\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n\r\n                </div>\r\n               \r\n            </div>\r\n         </div>\r\n    </div>\r\n    \r\n</div>\r\n\r\n<div *ngIf=\"salesRedemption&&redemptionDetail==false &&!errorMessage\">\r\n    <app-form-builder-table \r\n    [tableFormat] = \"tableFormat\" \r\n    [table_data]  = \"salesRedemption\" \r\n    [searchCallback]= \"[service, 'searchRedemptionReportint',this]\" \r\n    [total_page]=\"totalPage\"\r\n    >\r\n    </app-form-builder-table>\r\n</div>\r\n\r\n<div *ngIf=\"showLoadingSpinner\" id=\"cover-spin\">\r\n    <div class=\"spin-content\">\r\n        <div class=\"spin-text\">processing ...</div>\r\n        <progressbar [max]=\"processTotal\" [value]=\"processCount\" [striped]=\"true\" [animate]=\"true\"><i>{{processCount}} / {{processTotal}}</i></progressbar>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evouchersales/evoucher-sales.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/evouchersales/evoucher-sales.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n    <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n      \r\n      <!-- <div class=\"container-swapper-option\" *ngIf=\"SalesRedemptionDetail==false&&!errorMessage\">\r\n            <span style=\"margin-right: 10px;\">View</span> \r\n            <div class=\"button-container\">\r\n            <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n            <span class=\"fa fa-table\"> All</span>\r\n            </button>\r\n            <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n            <span class=\"fa fa-table\"> With Receipt</span>\r\n            </button>\r\n            </div>\r\n      </div> -->\r\n    \r\n      <div *ngIf=\"SalesRedemption && SalesRedemptionDetail==false && !mci_project && programType!='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, swaper == true ?'searchEwalletSalesReportint':'searchEwalletReceiptReportint',this]\"\r\n            [tableFormat]=\"tableFormat\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n  \r\n            </app-form-builder-table>\r\n      </div>\r\n      <div *ngIf=\"SalesRedemption && SalesRedemptionDetail==false && mci_project && programType!='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, 'searchEwalletSalesReportint',this]\"\r\n            [tableFormat]=\"tableFormat2\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n\r\n            </app-form-builder-table>\r\n      </div>\r\n\r\n      <div *ngIf=\"SalesRedemption && SalesRedemptionDetail==false && !mci_project && programType=='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, 'searchEwalletSalesReportint',this]\"\r\n            [tableFormat]=\"tableFormat3\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n\r\n            </app-form-builder-table>\r\n      </div>\r\n        <!-- <div *ngIf=\"SalesRedemptionDetail\"> -->\r\n              <!-- <app-bast [back]=\"[this,backToHere]\" [detail]=\"SalesRedemptionDetail\"></app-bast> -->\r\n        <!-- </div> -->\r\n      <div *ngIf=\"SalesRedemptionDetail\">\r\n            <app-evoucher-sales-detail [back]=\"[this,backToHere]\" [detail]=\"SalesRedemptionDetail\"></app-evoucher-sales-detail>\r\n      </div>\r\n  \r\n    </div>"

/***/ }),

/***/ "./src/app/layout/modules/evouchersales/detail/evoucher-sales.detail.component.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/layout/modules/evouchersales/detail/evoucher-sales.detail.component.scss ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.need-approval-text {\n  color: red;\n  font-size: 14px;\n  margin-bottom: 15px;\n}\n\n.btn-action-stock {\n  display: flex;\n}\n\n.btn-action-stock .btn-approve {\n  background-color: #28a745;\n  color: white;\n  margin-right: 10px;\n  font-size: 14px;\n}\n\n.btn-action-stock .btn-decline {\n  font-size: 14px;\n  margin-right: 10px;\n  background-color: red;\n  color: white;\n}\n\n.btn-action-stock .btn-cancel {\n  font-size: 14px;\n}\n\n#cover-spin {\n  position: fixed;\n  width: 100%;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  background-color: rgba(255, 255, 255, 0.7);\n  z-index: 9999;\n}\n\n#cover-spin .spin-content {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-direction: column;\n  height: 100%;\n}\n\n#cover-spin .spin-content .spin-text {\n  margin-bottom: 12px;\n  font-weight: bold;\n  color: #095eed;\n  font-size: 17px;\n}\n\n#cover-spin .spin-content .progress {\n  width: 300px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXZvdWNoZXJzYWxlcy9kZXRhaWwvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxldm91Y2hlcnNhbGVzXFxkZXRhaWxcXGV2b3VjaGVyLXNhbGVzLmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXZvdWNoZXJzYWxlcy9kZXRhaWwvZXZvdWNoZXItc2FsZXMuZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FER0k7RUFDSSxrQkFBQTtBQ0FSOztBRENRO0VBRUksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNEWjs7QURFWTtFQUNJLGlCQUFBO0FDQWhCOztBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBRUEsV0FBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBSUEsV0FBQTtBQ0xaOztBREVZO0VBQ0ksaUJBQUE7QUNBaEI7O0FESVE7RUFDSSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDRlo7O0FES0k7RUFDSSxhQUFBO0FDSFI7O0FESVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDRlo7O0FEaUJJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNmUjs7QURrQlE7RUFDSSxrQkFBQTtBQ2hCWjs7QURrQlE7RUFDSSxnQkFBQTtBQ2hCWjs7QURrQlE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDaEJaOztBRG1CUTtFQUNJLHNCQUFBO0FDakJaOztBRGtCWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDaEJoQjs7QUR1QkE7RUFDSSxVQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDcEJKOztBRHVCQTtFQUNJLGFBQUE7QUNwQko7O0FEcUJJO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDbkJSOztBRHFCSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtBQ25CUjs7QURxQkk7RUFDSSxlQUFBO0FDbkJSOztBRHVCQTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUFPLFFBQUE7RUFBUSxNQUFBO0VBQU0sU0FBQTtFQUNyQiwwQ0FBQTtFQUNBLGFBQUE7QUNqQko7O0FEbUJJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7QUNqQlI7O0FEbUJRO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDakJaOztBRG9CUTtFQUNJLFlBQUE7QUNsQloiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9ldm91Y2hlcnNhbGVzL2RldGFpbC9ldm91Y2hlci1zYWxlcy5kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmctZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgLy8gcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuZWRpdF9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYnRuLXJpZ2h0IHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gLmltYWdle1xyXG4gICAgLy8gICAgIGgze1xyXG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAvLyAgICAgICAgIGltZ3tcclxuICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICAgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLm5lZWQtYXBwcm92YWwtdGV4dCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxufVxyXG5cclxuLmJ0bi1hY3Rpb24tc3RvY2sge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIC5idG4tYXBwcm92ZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzI4YTc0NTtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIH1cclxuICAgIC5idG4tZGVjbGluZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG4gICAgLmJ0bi1jYW5jZWwge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIH1cclxufVxyXG5cclxuI2NvdmVyLXNwaW4ge1xyXG4gICAgcG9zaXRpb246Zml4ZWQ7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgbGVmdDowO3JpZ2h0OjA7dG9wOjA7Ym90dG9tOjA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwyNTUsMjU1LDAuNyk7XHJcbiAgICB6LWluZGV4Ojk5OTk7XHJcblxyXG4gICAgLnNwaW4tY29udGVudCB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG5cclxuICAgICAgICAuc3Bpbi10ZXh0IHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTJweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjMDk1ZWVkO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAucHJvZ3Jlc3Mge1xyXG4gICAgICAgICAgICB3aWR0aDogMzAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiLmJnLWRlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG5cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYnRuLXJpZ2h0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLm5lZWQtYXBwcm92YWwtdGV4dCB7XG4gIGNvbG9yOiByZWQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLmJ0bi1hY3Rpb24tc3RvY2sge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLmJ0bi1hY3Rpb24tc3RvY2sgLmJ0bi1hcHByb3ZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI4YTc0NTtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5idG4tYWN0aW9uLXN0b2NrIC5idG4tZGVjbGluZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5idG4tYWN0aW9uLXN0b2NrIC5idG4tY2FuY2VsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4jY292ZXItc3BpbiB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDEwMCU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xuICB6LWluZGV4OiA5OTk5O1xufVxuI2NvdmVyLXNwaW4gLnNwaW4tY29udGVudCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4jY292ZXItc3BpbiAuc3Bpbi1jb250ZW50IC5zcGluLXRleHQge1xuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICMwOTVlZWQ7XG4gIGZvbnQtc2l6ZTogMTdweDtcbn1cbiNjb3Zlci1zcGluIC5zcGluLWNvbnRlbnQgLnByb2dyZXNzIHtcbiAgd2lkdGg6IDMwMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/evouchersales/detail/evoucher-sales.detail.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/layout/modules/evouchersales/detail/evoucher-sales.detail.component.ts ***!
  \****************************************************************************************/
/*! exports provided: EvoucherSalesDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherSalesDetailComponent", function() { return EvoucherSalesDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var EvoucherSalesDetailComponent = /** @class */ (function () {
    function EvoucherSalesDetailComponent(route, router, OrderhistoryService) {
        this.route = route;
        this.router = router;
        this.OrderhistoryService = OrderhistoryService;
        this.processNow = false;
        this.cancelNow = false;
        this.declineNow = false;
        this.declineCancel = false;
        this.cancelRemarks = "";
        this.edit = false;
        this.errorLabel = false;
        this.salesRedemption = [];
        this.row_id = "order_id";
        this.errorMessage = false;
        this.showLoadingSpinner = false;
        this.processCount = 0;
        this.processTotal = 0;
        this.redemptionDetail = false;
        this.tableFormat = {
            title: 'All Redemptions in the Order',
            label_headers: [
                // {label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date'},
                // {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
                // {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
                // {label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
                // {label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail'},
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                // {label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail'},
                // {label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
                // {label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail'},
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Status Pengiriman', visible: true, type: 'last_shipping_info', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Kirim', visible: true, type: 'delivery_date', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Terima', visible: true, type: 'delivered_date', data_row_name: 'delivery_detail' },
                { label: 'Nama Penerima Langsung', visible: true, type: 'receiver_name_sap', data_row_name: 'delivery_detail' },
                { label: 'Status Penerima', visible: true, type: 'relation_name', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Cancel', visible: true, type: 'date', data_row_name: 'cancel_date' },
                { label: 'Cancel Remarks', visible: true, type: 'string', data_row_name: 'cancel_remarks' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: this.row_id,
                this: this,
                result_var_name: 'salesRedemption',
                detail_function: [this, 'callDetail'],
                cancelProduct: true
            }
        };
    }
    EvoucherSalesDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EvoucherSalesDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, i, cancel_detail, j, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.salesRedemptionDetail = this.detail;
                        console.warn("detail", this.salesRedemptionDetail);
                        this.orderID = this.detail.order_id;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        this.service = this.OrderhistoryService;
                        return [4 /*yield*/, this.OrderhistoryService.searchAllRedemptionPerOrder(this.orderID)];
                    case 2:
                        result = _a.sent();
                        this.totalPage = result.total_page;
                        this.salesRedemption = result.values;
                        for (i = 0; i < this.salesRedemption.length; i++) {
                            cancel_detail = [];
                            if (this.salesRedemption[i].products.cancel_detail) {
                                cancel_detail.push(this.salesRedemption[i].products.cancel_detail);
                                for (j = 0; j < cancel_detail.length; j++) {
                                    this.salesRedemption[i].cancel_remarks = cancel_detail[j].remarks;
                                    this.salesRedemption[i].cancel_date = cancel_detail[j].cancel_date;
                                    // console.warn("result after??", this.salesRedemption);
                                }
                            }
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        console.warn("ERROR");
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    EvoucherSalesDetailComponent.prototype.backToHere = function () {
        // this.evoucherStockDetail = false;
        this.back[1](this.back[0]);
        // this.firstLoad();
    };
    EvoucherSalesDetailComponent.prototype.isProcessNow = function () {
        this.processNow = !this.processNow;
    };
    EvoucherSalesDetailComponent.prototype.isCancelNow = function () {
        this.cancelNow = !this.cancelNow;
    };
    EvoucherSalesDetailComponent.prototype.isDeclineCancel = function () {
        this.declineCancel = !this.declineCancel;
    };
    EvoucherSalesDetailComponent.prototype.isDeclineNow = function () {
        this.declineNow = !this.declineNow;
    };
    EvoucherSalesDetailComponent.prototype.completeOrder = function (orderID) {
        return __awaiter(this, void 0, void 0, function () {
            var payloadData, result, e_2, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        payloadData = {
                            "order_id": [
                                orderID
                            ]
                        };
                        return [4 /*yield*/, this.OrderhistoryService.updateStatusOrderCompleted(payloadData, this)];
                    case 1:
                        result = _a.sent();
                        console.warn("result process", result);
                        if (result) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                                title: 'Success',
                                text: 'Order is Completed',
                                icon: 'success',
                                confirmButtonText: 'Ok',
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    _this.backToHere();
                                }
                            });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        console.warn("error appove", e_2);
                        this.errorLabel = (e_2.message);
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EvoucherSalesDetailComponent.prototype.processSelectedProducts = function (obj, products) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                // console.warn("products??", products);
                // console.warn("obj??", obj);
                if (products.length > 0) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                        title: 'Konfirmasi',
                        text: 'Alasan Pembatalan Produk : ',
                        // icon: 'success',
                        confirmButtonText: 'Ok',
                        cancelButtonText: "Kembali",
                        showCancelButton: true,
                        input: 'text',
                    }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                        var payload, e_3, message;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!result.isConfirmed) return [3 /*break*/, 5];
                                    this.showLoadingSpinner = true;
                                    this.processTotal = products.length;
                                    this.processCount = 0;
                                    payload = {
                                        "order_id": this.orderID,
                                        "product_sku": products,
                                        "remarks": result.value
                                    };
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    return [4 /*yield*/, obj.OrderhistoryService.cancelDigitalProductOrder(payload).then(function () {
                                            _this.processCount++;
                                            if (_this.processCount >= products.length) {
                                                _this.showLoadingSpinner = false;
                                                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                                                    title: 'Success',
                                                    text: 'Produk terpilih telah dibatalkan',
                                                    icon: 'success',
                                                    confirmButtonText: 'Ok',
                                                    showCancelButton: false
                                                }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        if (result.isConfirmed) {
                                                            console.log("confirmed");
                                                            obj.firstLoad();
                                                        }
                                                        return [2 /*return*/];
                                                    });
                                                }); });
                                            }
                                        })];
                                case 2:
                                    _a.sent();
                                    return [3 /*break*/, 4];
                                case 3:
                                    e_3 = _a.sent();
                                    this.errorLabel = (e_3.message); //conversion to Error type
                                    this.showLoadingSpinner = false;
                                    message = this.errorLabel;
                                    if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                                        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                                    }
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                                        icon: 'error',
                                        title: message,
                                    });
                                    return [3 /*break*/, 4];
                                case 4:
                                    ;
                                    _a.label = 5;
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); });
                }
                return [2 /*return*/];
            });
        });
    };
    EvoucherSalesDetailComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__["OrderhistoryService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EvoucherSalesDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EvoucherSalesDetailComponent.prototype, "back", void 0);
    EvoucherSalesDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-evoucher-sales-detail',
            template: __webpack_require__(/*! raw-loader!./evoucher-sales.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evouchersales/detail/evoucher-sales.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_3__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./evoucher-sales.detail.component.scss */ "./src/app/layout/modules/evouchersales/detail/evoucher-sales.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__["OrderhistoryService"]])
    ], EvoucherSalesDetailComponent);
    return EvoucherSalesDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/evouchersales/evoucher-sales-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/evouchersales/evoucher-sales-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: EvoucherSalesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherSalesRoutingModule", function() { return EvoucherSalesRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _evoucher_sales_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./evoucher-sales.component */ "./src/app/layout/modules/evouchersales/evoucher-sales.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { BastComponent} from './bast/bast.component'
var routes = [
    {
        path: '', component: _evoucher_sales_component__WEBPACK_IMPORTED_MODULE_2__["EvoucherSalesComponent"]
    },
];
var EvoucherSalesRoutingModule = /** @class */ (function () {
    function EvoucherSalesRoutingModule() {
    }
    EvoucherSalesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], EvoucherSalesRoutingModule);
    return EvoucherSalesRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/evouchersales/evoucher-sales.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/layout/modules/evouchersales/evoucher-sales.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-swapper-option {\n  display: flex;\n  justify-content: center;\n  flex-direction: row;\n  align-items: center;\n}\n.container-swapper-option .button-container {\n  border: 2px solid #ddd;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button {\n  color: #ddd;\n  background: white;\n  font-size: 14px;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button.true {\n  background: #2480fb;\n  border-radius: 8px;\n  color: #ddd;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXZvdWNoZXJzYWxlcy9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGV2b3VjaGVyc2FsZXNcXGV2b3VjaGVyLXNhbGVzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9ldm91Y2hlcnNhbGVzL2V2b3VjaGVyLXNhbGVzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0NGO0FEQUU7RUFFRSxzQkFBQTtFQUNBLGtCQUFBO0FDQ0o7QURBSTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0VOO0FEQUk7RUFFRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0NOIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXZvdWNoZXJzYWxlcy9ldm91Y2hlci1zYWxlcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXItc3dhcHBlci1vcHRpb24ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIC5idXR0b24tY29udGFpbmVyIHtcclxuXHJcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjZGRkO1xyXG4gICAgYm9yZGVyLXJhZGl1czo4cHg7XHJcbiAgICBidXR0b24ge1xyXG4gICAgICBjb2xvcjogI2RkZDtcclxuICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgfVxyXG4gICAgYnV0dG9uLnRydWUge1xyXG4gICAgIFxyXG4gICAgICBiYWNrZ3JvdW5kOiAjMjQ4MGZiO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgIGNvbG9yOiAjZGRkO1xyXG4gICAgfVxyXG4gIH1cclxufSIsIi5jb250YWluZXItc3dhcHBlci1vcHRpb24ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5jb250YWluZXItc3dhcHBlci1vcHRpb24gLmJ1dHRvbi1jb250YWluZXIge1xuICBib3JkZXI6IDJweCBzb2xpZCAjZGRkO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG59XG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIGJ1dHRvbiB7XG4gIGNvbG9yOiAjZGRkO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG59XG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIGJ1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZDogIzI0ODBmYjtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBjb2xvcjogI2RkZDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/evouchersales/evoucher-sales.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/modules/evouchersales/evoucher-sales.component.ts ***!
  \**************************************************************************/
/*! exports provided: EvoucherSalesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherSalesComponent", function() { return EvoucherSalesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var EvoucherSalesComponent = /** @class */ (function () {
    function EvoucherSalesComponent(OrderhistoryService, router, activatedRoute) {
        this.OrderhistoryService = OrderhistoryService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.SalesRedemption = [];
        this.swaper = true;
        this.tableFormat = {
            title: 'E-wallet Sales report',
            label_headers: [
                { label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                // {label: 'Member', visible: true, type: 'string', data_row_name: 'member_name'},
                { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                { label: 'Group', visible: true, type: 'form_group', data_row_name: 'member_detail' },
                { label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                // {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
            },
            show_checkbox_options: true
        };
        this.tableFormat2 = {
            title: 'E-voucher Sales report',
            label_headers: [
                { label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Program', visible: true, type: 'form_group', data_row_name: 'member_detail' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
            },
            show_checkbox_options: true
        };
        this.tableFormat3 = {
            title: 'E-voucher Sales report',
            label_headers: [
                { label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
                // {label: 'Member', visible: true, type: 'string', data_row_name: 'member_name'},
                { label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                // {label: 'Group', visible: true, type: 'form_group', data_row_name: 'member_detail'},
                { label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                // {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
            },
            show_checkbox_options: true
        };
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__;
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        this.SalesRedemptionDetail = false;
        this.mci_project = false;
        this.programType = "";
    }
    EvoucherSalesComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EvoucherSalesComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params, program_1, _this_1, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        params = {
                            search: {
                                type: {
                                    in: ["product", "voucher"]
                                }
                                // active : '1'
                            },
                            limit_per_page: 50,
                            current_page: 1,
                            order_by: { request_date: -1 }
                        };
                        program_1 = localStorage.getItem('programName');
                        _this_1 = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program_1) {
                                if (element.type == "reguler") {
                                    _this_1.mci_project = true;
                                    _this_1.programType = "reguler";
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this_1.mci_project = false;
                                    _this_1.programType = "custom_kontraktual";
                                }
                                else {
                                    _this_1.mci_project = false;
                                    _this_1.programType = "custom";
                                }
                            }
                        });
                        this.service = this.OrderhistoryService;
                        result = void 0;
                        return [4 /*yield*/, this.OrderhistoryService.getEvoucherSalesint()];
                    case 1:
                        result = _a.sent();
                        // if(this.swaper == true){
                        //   result = await this.OrderhistoryService.getEvoucherSalesint();
                        // }else{
                        //   result = await this.OrderhistoryService.getEwalletReceiptReportint();
                        // }
                        this.totalPage = result.total_page;
                        this.SalesRedemption = result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EvoucherSalesComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    this.SalesRedemptionDetail = this.SalesRedemption.find(function (redemption) { return redemption.order_id == rowData.order_id; });
                    // let result: any;
                    // this.service    = this.OrderhistoryService;
                    // result          = await this.OrderhistoryService.detailPointstransaction(SalesRedemption_id);
                    // console.log(result);
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    EvoucherSalesComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.warn("masuk back awal");
                console.warn("obj", obj);
                obj.SalesRedemptionDetail = false;
                obj.router.navigate([]).then(function () {
                    obj.firstLoad();
                });
                return [2 /*return*/];
            });
        });
    };
    EvoucherSalesComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
        this.firstLoad();
    };
    EvoucherSalesComponent.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    EvoucherSalesComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
    ]; };
    EvoucherSalesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-evoucher-sales',
            template: __webpack_require__(/*! raw-loader!./evoucher-sales.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evouchersales/evoucher-sales.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./evoucher-sales.component.scss */ "./src/app/layout/modules/evouchersales/evoucher-sales.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], EvoucherSalesComponent);
    return EvoucherSalesComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/evouchersales/evoucher-sales.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/evouchersales/evoucher-sales.module.ts ***!
  \***********************************************************************/
/*! exports provided: EvoucherSalesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherSalesModule", function() { return EvoucherSalesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _evoucher_sales_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./evoucher-sales-routing.module */ "./src/app/layout/modules/evouchersales/evoucher-sales-routing.module.ts");
/* harmony import */ var _evoucher_sales_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./evoucher-sales.component */ "./src/app/layout/modules/evouchersales/evoucher-sales.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _detail_evoucher_sales_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detail/evoucher-sales.detail.component */ "./src/app/layout/modules/evouchersales/detail/evoucher-sales.detail.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/progressbar */ "./node_modules/ngx-bootstrap/progressbar/fesm5/ngx-bootstrap-progressbar.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










// import { BastComponent } from './bast/bast.component';

var EvoucherSalesModule = /** @class */ (function () {
    function EvoucherSalesModule() {
    }
    EvoucherSalesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _evoucher_sales_routing_module__WEBPACK_IMPORTED_MODULE_2__["EvoucherSalesRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_10__["BsComponentModule"],
                ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_9__["ProgressbarModule"].forRoot(),
            ],
            declarations: [
                _evoucher_sales_component__WEBPACK_IMPORTED_MODULE_3__["EvoucherSalesComponent"],
                _detail_evoucher_sales_detail_component__WEBPACK_IMPORTED_MODULE_5__["EvoucherSalesDetailComponent"]
            ],
        })
    ], EvoucherSalesModule);
    return EvoucherSalesModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-evouchersales-evoucher-sales-module.js.map