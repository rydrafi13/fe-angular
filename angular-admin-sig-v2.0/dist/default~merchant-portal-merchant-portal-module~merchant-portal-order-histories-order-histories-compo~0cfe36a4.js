(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~merchant-portal-merchant-portal-module~merchant-portal-order-histories-order-histories-compo~0cfe36a4"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/order-histories/order-histories.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/order-histories/order-histories.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"board\">\r\n  <div class=\"nav toolbar\">\r\n    <h3 id=\"h3\"><strong>Your Transactions</strong></h3><br>\r\n    <div class=\"card\">\r\n      <div class=\"card-content\">\r\n        <label id=\"label-header\"><strong>Order Status </strong></label>\r\n        <hr>\r\n        <div class=\"order-status\">\r\n          <button *ngFor=\"let sw of swaper; let i = index;\" class=\"btn toolbar {{sw.val}}\" id=\"button-swapper\"\r\n            (click)=\"swapClick(i)\">\r\n            <span class=\"{{sw.icon}}\"></span>\r\n            <span class=\"status-name\"><span class=\"badge\"\r\n                *ngIf=\"sw.name == 'Incoming' && totalValuePending\">{{totalValuePending}}</span>{{sw.name}}</span>\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"card\">\r\n      <div class=\"card sticky-top\" id=\"card-transaction\">\r\n        <div class=\"div-transaction\">\r\n          <div id=\"transaction\">\r\n            <label id=\"label-header\"><strong>Transaction Filters</strong></label>\r\n          </div>\r\n          <div class=\"searchbar\">\r\n            <div id=\"search\">\r\n              <!-- <label>Filter by</label> -->\r\n              <select class=\"form-control\" [(ngModel)]=\"filter\">\r\n                <option *ngFor=\"let f of filterBy\" [ngValue]=\"f.value\">{{f.name}}</option>\r\n              </select>\r\n            </div>\r\n            <div id=\"search-field\">\r\n              <input id=\"search-text\" type=\"text\" class=\"form-control\" (keyup)=\"searchText($event)\"\r\n                placeholder=\"search\">\r\n              <i class=\"fa fa-search\" id=\"search-icon\"></i>\r\n            </div>\r\n            <div class=\"date-picker-wrapper\">\r\n              <div class=\"date-picker-overlay\" *ngIf=\"toggler && toggler['order_date']\">\r\n                <div class=\"dpicker\">\r\n                  <ngb-datepicker #d (select)=\"datePickerOnDateSelection($event, 'order_date')\" [displayMonths]=\"1\"\r\n                    [dayTemplate]=\"t\" outsideDays=\"hidden\"></ngb-datepicker>\r\n                  <div style=\"float: right;justify-self: right;\">\r\n                    <a class=\"btn ok\" (click)=\"dateToggler('order_date')\">OK</a>\r\n                    <a class=\"btn clear\" (click)=\"dateClear($event, 'order_date')\"> <span class=\"fa fa-trash\"></span>\r\n                      clear</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <ng-template #t let-date=\"date\" let-focused=\"focused\">\r\n                <span class=\"custom-day\" [class.focused]=\"focused\"\r\n                  [class.range]=\"datePickerOnDateIsRange(date, 'order_date')\">\r\n                  {{ date.day }}\r\n                </span>\r\n              </ng-template>\r\n              <em\r\n                *ngIf=\"typeof(form_input['order_date']) != 'object' || (typeof(form_input['order_date']) == 'object' && !form_input['order_date'].from) || toggler['order_date']\">\r\n                <input class=\"form-control\" style=\"padding-right: 22px;\" type=\"text\" [(ngModel)]=\"form_input['date']\"\r\n                  placeholder=\"search date here\" (click)=\"dateToggler('order_date')\">\r\n                <i class=\"fa fa-calendar-alt\" id=\"calendar-icon\"></i>\r\n              </em>\r\n              <a class=\"btn form-control\" style=\"border: 1px solid #ced4da\"\r\n                *ngIf=\"typeof(form_input['order_date']) == 'object' && form_input['order_date'].from && !toggler['order_date'] \"\r\n                (click)=\"dateToggler('order_date')\">\r\n                <span *ngIf=\"form_input['order_date'].from\"> {{form_input['order_date'].from}} </span>\r\n                <span *ngIf=\"form_input['order_date'].to\"> - {{form_input['order_date'].to}} </span>\r\n                <i class=\"fa fa-calendar-alt\"></i>\r\n\r\n              </a>\r\n            </div>\r\n            <div id=\"search\">\r\n              <!-- <label>Sort by</label> -->\r\n              <select class=\"form-control\" [(ngModel)]=\"orderBy\" (change)=\"orderBySelected()\">\r\n                <option *ngFor=\"let s of sortBy\" [ngValue]=\"s.value\">{{s.name}}</option>\r\n              </select>\r\n            </div>\r\n            <button (click)=\"clickToDownloadReport()\" class=\"btn save btn-primary\" id=\"btn-download\">Download\r\n              Report</button>\r\n            <button (click)=\"printLabel()\" class=\"btn save btn-success\" id=\"btn-print\" *ngIf=\"swaper[3].val == true\">Print Label</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"select-all\" *ngIf=\"!allData && allData.length!=0\">\r\n        <label class=\"header-text\" style=\"float: right;margin-right: 35px;\">\r\n          <strong>Select All\r\n            <mat-checkbox (change)=\"checkedAll()\" [(ngModel)]=\"activeCheckbox\" type=\"checkbox\"\r\n              style=\"vertical-align: -webkit-baseline-middle;margin-left: 10px;\">\r\n            </mat-checkbox>\r\n          </strong>\r\n        </label>\r\n      </div>\r\n      <div class=\"loading\" *ngIf='showLoading'>\r\n        <div class=\"sk-fading-circle\">\r\n          <div class=\"sk-circle1 sk-circle\"></div>\r\n          <div class=\"sk-circle2 sk-circle\"></div>\r\n          <div class=\"sk-circle3 sk-circle\"></div>\r\n          <div class=\"sk-circle4 sk-circle\"></div>\r\n          <div class=\"sk-circle5 sk-circle\"></div>\r\n          <div class=\"sk-circle6 sk-circle\"></div>\r\n          <div class=\"sk-circle7 sk-circle\"></div>\r\n          <div class=\"sk-circle8 sk-circle\"></div>\r\n          <div class=\"sk-circle9 sk-circle\"></div>\r\n          <div class=\"sk-circle10 sk-circle\"></div>\r\n          <div class=\"sk-circle11 sk-circle\"></div>\r\n          <div class=\"sk-circle12 sk-circle\"></div>\r\n        </div>\r\n      </div>\r\n      <div class=\"div-content-row\" *ngFor=\"let data of allData; let i = index\">\r\n        <div class=\"div-content-cell div-product\">\r\n          <div *ngIf=\"allData[i].product_status == true; else equalToOne\">\r\n            <!-- <div *ngIf=\"allData[i].product_list.length > 1; else equalToOne\"> -->\r\n            <div class=\"more-product\">\r\n              <div class=\"loop-product\" *ngFor=\"let product of allData[i].product_list;\">\r\n                <div class=\"card\">\r\n                  <div class=\"img-wrapper\" style=\"background-image: url({{product.base_url+product.pic_small_path}});\">\r\n                    <!-- <div class=\"img\" [ngStyle]=\"{'background-image':'url('+(product.base_url+product.pic_small_path)+')'}\"> -->\r\n                    <!-- <img src=\"{{product.base_url+product.pic_small_path}}\" class=\"image\"> -->\r\n                  </div>\r\n                </div>\r\n                <div>\r\n                  <div class=\"form-label\">\r\n                    <label class=\"header-text order-id-text\">Order ID : <span>{{data.order_id}}</span></label>\r\n                    <label class=\"normal-text date-text\">{{data.order_date}}</label>\r\n                  </div>\r\n                  <div class=\"form-label\">\r\n                    <label\r\n                      style=\"font-size: 20px; margin-bottom: 0px;\"><strong>{{product.product_name}}</strong></label>\r\n                  </div>\r\n                  <label class=\"price\"><strong>{{product.total_product_price | currency: 'Rp '}}</strong></label><br />\r\n                  <label class=\"header-text\" style=\"margin-bottom: 0.5rem;\">\r\n                    <strong>Qty : {{product.quantity}}</strong>\r\n                  </label>\r\n                  <label class=\"header-text\"><strong>Notes:</strong></label>\r\n                  <p class=\"normal-text\" style=\"word-break: break-all;\">\r\n                    {{product.note_to_merchant}}</p>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <ng-template #equalToOne>\r\n            <div class=\"div-default-product\">\r\n              <div class=\"default-product\">\r\n                <!-- <div *ngFor=\"let product of allData[i].product_list;\" style=\"display: flex;margin-bottom: 10px;\"> -->\r\n                <div class=\"img-wrapper\"\r\n                  style=\"background-image: url({{allData[i].product_list[0].base_url+allData[i].product_list[0].pic_small_path}});\">\r\n                  <!-- <div class=\"img\" [ngStyle]=\"{'background-image':'url('+(allData[i].product_list[0].base_url+allData[i].product_list[0].pic_small_path)+')'}\"> -->\r\n                  <img src=\"{{allData[i].product_list[0].base_url+allData[i].product_list[0].pic_small_path}}\"\r\n                    class=\"image\">\r\n                </div>\r\n                <div class=\"product-detail\">\r\n                  <div class=\"form-label\">\r\n                    <label class=\"header-text order-id-text\">Order ID : <span>{{data.order_id}}</span></label>\r\n                    <label class=\"normal-text date-text\">{{data.order_date}}</label>\r\n                  </div>\r\n                  <div class=\"form-label\">\r\n                    <label class=\"header-text\"><strong>{{allData[i].product_list[0].product_name}}</strong></label>\r\n                  </div>\r\n                  <div class=\"form-label\">\r\n                    <label\r\n                      class=\"price\"><strong>{{allData[i].product_list[0].total_product_price | currency: 'Rp '}}</strong></label>\r\n                  </div>\r\n                  <label class=\"header-text\" style=\"margin-bottom: 0.5rem;\"><strong>Quantity :\r\n                      {{allData[i].product_list[0].quantity}}</strong></label><br />\r\n                  <div *ngIf=\"allData[i].product_list[0].note_to_merchant\">\r\n                    <label class=\"header-text\"><strong>Notes:</strong></label>\r\n                    <p class=\"normal-text\" style=\"word-break: break-all;\">\r\n                      {{allData[i].product_list[0].note_to_merchant}}</p>\r\n                  </div>\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </ng-template>\r\n          <div *ngIf=\"allData[i].product_list.length > 1\" style=\"text-align: right;\">\r\n            <label class=\"see-more\" (click)=\"setSeeMore(i)\" class=\"normal-text\">See {{allData[i].product_list.length-1}}\r\n              more...</label>\r\n          </div>\r\n        </div>\r\n        <div class=\"div-content-cell \">\r\n          <div class=\"form-label\">\r\n            <label class=\"header-text\">Shipping Address</label>\r\n            <label class=\"normal-text\" *ngIf=\"type(i) == 'product'\">{{data.buyer_detail.address}}</label>\r\n          </div>\r\n          <div class=\"form-label\" *ngIf=\"data.resi_number\">\r\n            <label class=\"header-text\">No. Resi</label>\r\n            <label class=\"normal-text\" *ngIf=\"type(i) == 'product'\">{{data.resi_number}}</label>\r\n          </div>\r\n          <div class=\"form-label\" *ngIf=\"data.type\">\r\n            <label class=\"header-text\">Delivery Service</label>\r\n            <label class=\"normal-text\" *ngIf=\"data.type == 'product'\">{{data.buyer_detail.delivery_request.service}} /\r\n              {{data.buyer_detail.delivery_request.description}}</label>\r\n            <label *ngIf=\"type(i) == 'evoucher'\">no-services</label>\r\n          </div>\r\n\r\n          <div class=\"form-label\" *ngIf=\"data.delivery_courier\">\r\n            <label class=\"header-text\">Courier</label>\r\n            <label class=\"normal-text\">{{data.delivery_courier}}</label>\r\n          </div>\r\n        </div>\r\n        <div class=\"div-content-cell products-left\">\r\n          <div class=\"products-left-board\">\r\n            <div class=\"products-left-data\">\r\n              <label class=\"header-text\">Receiver</label>\r\n              <label class=\"normal-text\">{{data.buyer_detail.name}}</label>\r\n              <label class=\"header-text\">Phone Number</label>\r\n              <label class=\"normal-text\">{{data.buyer_detail.cell_phone}}</label>\r\n              <label class=\"header-text\">Total ({{data.total_item}} Items)</label>\r\n              <label class=\"price-total\"><strong>{{data.total_price | currency: 'Rp '}}</strong></label>\r\n            </div>\r\n            <div class=\"products-left-checkbox\">\r\n              <mat-checkbox (change)='onChecked()' [(ngModel)]=\"checkBox[i]\" type=\"checkbox\"></mat-checkbox>\r\n            </div>\r\n          </div>\r\n          <div class=\"products-left-status\">\r\n            <span class=\"payment-status {{data.payment_status}}\" *ngIf=\"data.payment_status == 'PAID'\">Paid</span>\r\n            <span class=\"payment-status {{data.payment_status}}\" *ngIf=\"data.payment_status == 'CANCEL'\">Cancel</span>\r\n            <label class=\"see-more\" (click)=\"getDetail(data.booking_id)\">More Details &raquo;</label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"div-footer\" *ngIf=\"allData && allData.length!=0\">\r\n        <div>\r\n          <button class=\"btn\" (click)=\"onChangePage(currentPage = 1)\">\r\n            &laquo; </button> <button class=\"btn\" (click)=\"onChangePage(currentPage - 1)\">\r\n            &lsaquo; </button> <button *ngFor=\"let tp of pageNumbering\" class=\"btn\" (click)=\"onChangePage(tp)\">{{tp}}\r\n          </button>\r\n          <button class=\"btn\" (click)=\"onChangePage(currentPage + 1)\">&rsaquo;</button>\r\n          <button class=\"btn\" (click)=\"onChangePage(currentPage = total_page)\">&raquo;</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n\r\n</div>\r\n<table style=\"display: none;\" id=\"reportTable\" #reportTable>\r\n  <thead>\r\n    <tr>\r\n      <th>NO.</th>\r\n      <th>Order ID</th>\r\n      <th>Order Date</th>\r\n      <th>Product Name</th>\r\n      <th>Product Price</th>\r\n      <th>Quantity</th>\r\n      <th>Notes</th>\r\n      <th>Shopping Address</th>\r\n      <th>No. Resi</th>\r\n      <th>Service</th>\r\n      <th>Courier</th>\r\n      <th>Receiver</th>\r\n      <th>Phone Number</th>\r\n      <th>Total</th>\r\n      <th>Payment Status</th>\r\n    </tr>\r\n  </thead>\r\n  <tbody>\r\n    <tr *ngFor=\"let data of allData; let i = index\">\r\n      <td>{{i+1}}</td>\r\n      <td>{{data.order_id}}</td>\r\n      <td>{{data.order_date}}</td>\r\n      <td>\r\n        <div *ngFor=\"let product of allData[i].product_list;\">\r\n          <span>{{product.product_name}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of allData[i].product_list;\">\r\n          <span>Rp. {{product.total_product_price}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of allData[i].product_list;\">\r\n          <span>{{product.quantity}}</span>\r\n        </div>\r\n      </td>\r\n      <td>\r\n        <div *ngFor=\"let product of allData[i].product_list;\">\r\n          <span>{{product.note_to_merchant}}</span>\r\n        </div>\r\n      </td>\r\n      <td>{{data.buyer_detail.address}}</td>\r\n      <td>{{data.resi_number}}</td>\r\n      <!-- <td >Empty</td> -->\r\n      <!-- <td>{{data.buyer_detail.delivery_request.service}}</td> -->\r\n      <td>{{data.delivery_courier}}</td>\r\n      <td>{{data.buyer_detail.name}}</td>\r\n      <td>{{data.buyer_detail.cell_phone}}</td>\r\n      <td>{{data.total_price}}</td>\r\n      <td>{{data.payment_status}}</td>\r\n    </tr>\r\n  </tbody>\r\n</table>"

/***/ }),

/***/ "./src/app/layout/merchant-portal/order-histories/order-histories.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/order-histories/order-histories.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  left: 0px;\n  top: 50px;\n}\n\n*:focus {\n  box-shadow: unset;\n}\n\ndiv,\nspan:not(.fa) {\n  font-family: \"Poppins\";\n}\n\n.board {\n  height: 100%;\n  width: 100%;\n  min-width: 720px;\n}\n\n.nav.toolbar {\n  background: #fcfcfc;\n  height: 150px;\n  padding-top: 20px;\n  padding-bottom: 20px;\n  justify-content: center;\n  align-items: center;\n}\n\n.nav.toolbar .select-all {\n  width: 100%;\n  align-items: right;\n}\n\n.nav.toolbar .card {\n  padding: 20px;\n  width: 95%;\n  align-items: center;\n  margin-bottom: 20px;\n}\n\n.nav.toolbar .card h3 {\n  text-align: center;\n}\n\n.nav.toolbar .card tr,\n.nav.toolbar .card td {\n  color: black;\n  vertical-align: top;\n}\n\n.nav.toolbar .card tr select,\n.nav.toolbar .card td select {\n  height: 30px;\n}\n\n.nav.toolbar .card .card-content {\n  width: 100%;\n}\n\n.nav.toolbar .card .card-content hr {\n  margin-top: 0rem;\n  margin-bottom: 0rem;\n  border: 0;\n  border-top: 1px solid rgba(0, 0, 0, 0.1);\n}\n\n.nav.toolbar .card .card-content table {\n  border-collapse: collapse;\n  border-radius: 5px;\n  width: 95%;\n}\n\n.nav.toolbar .card .card-content button {\n  color: #34495e;\n  border-radius: 10px;\n  background: white;\n  font-size: 15px;\n  margin-top: 10px;\n  border: 2px solid #eaeaea;\n  width: 95%;\n  padding: 16px;\n}\n\n.nav.toolbar .card .card-content button.true {\n  background: #2c3e50;\n  color: white;\n}\n\n.form-control {\n  font-size: 11px;\n  width: 150px;\n  display: inline-block;\n  margin: 2px 0px 2px 0px;\n}\n\n.div-transaction {\n  display: flex;\n  flex-wrap: wrap;\n  font-size: 11px;\n  width: 100%;\n}\n\n.div-transaction label {\n  margin-bottom: 0px;\n}\n\n.div-transaction div.searchbar {\n  flex-wrap: wrap;\n  display: inherit;\n  width: calc(100% - 140px);\n  padding-left: 10px;\n  justify-content: flex-end;\n}\n\n.div-transaction div.searchbar div {\n  display: flex;\n  align-items: center;\n}\n\n.div-transaction div.searchbar div label {\n  margin-right: 5px;\n}\n\n.div-footer {\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  width: 100%;\n  text-align: -webkit-right;\n  margin-bottom: 10px;\n  display: flex;\n  float: right;\n}\n\n.div-footer div {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: auto;\n  padding: 0px;\n  margin-bottom: 5px;\n  border: 1px solid rgba(0, 0, 0, 0.1);\n  border-radius: 10px;\n}\n\n.div-footer div button {\n  color: #34495e;\n  background: white;\n  border-radius: 10px;\n}\n\n.div-footer div button:hover {\n  background: #2c3e50;\n  color: white;\n  border-radius: 10px;\n}\n\n.div-footer div button:active {\n  background: #eaeaea;\n  color: black;\n  border-radius: 10px;\n}\n\n.div-content-row {\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  border: 1px solid #ccc;\n  align-items: unset;\n  justify-content: unset;\n  border-radius: 10px;\n  margin-bottom: 15px;\n  padding: 10px;\n}\n\n.div-content-row .div-content-cell {\n  padding: 5px 12px;\n  max-width: unset;\n  border-right: 1px solid #ccc;\n  width: calc(100% - 560px);\n}\n\n.div-content-row .div-content-cell:last-child {\n  border-right: 0px solid transparent;\n  width: 280px;\n}\n\n.div-content-row > .div-content-cell:nth-child(2) {\n  width: 280px;\n}\n\n.default-product .product-detail .date-text {\n  font-size: 11px;\n  margin-bottom: 15px;\n  margin-top: 10px;\n}\n\n.more-product {\n  overflow-y: auto;\n  overflow-x: hidden;\n  display: grid;\n  height: 200px;\n  padding: 0px;\n}\n\n.more-product .loop-product {\n  display: flex;\n  margin-bottom: 10px;\n  padding: 0px;\n}\n\n.more-product .loop-product .card {\n  margin-left: 0px;\n  margin-right: 10px;\n  border-radius: 10px;\n  height: 165px;\n  width: 200px;\n  padding: 0px;\n}\n\n.more-product .loop-product .card .img {\n  height: -webkit-fill-available;\n  background-position: center;\n  -o-object-fit: fill;\n     object-fit: fill;\n  border-radius: 10px;\n}\n\n.div-default-product {\n  display: grid;\n  padding: 0px;\n}\n\n.div-default-product .default-product {\n  display: flex;\n  margin-bottom: 10px;\n  padding: 0px;\n}\n\n.div-default-product .default-product .img-wrapper {\n  padding: 0px 20px 0px 0px;\n}\n\n.div-default-product .default-product .img-wrapper .image {\n  border-radius: 5%;\n  width: 120px;\n  height: auto;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.products-left {\n  width: 20%;\n}\n\n.products-left .products-left-board {\n  display: flex;\n  padding: 0px;\n  overflow: hidden;\n  width: 100%;\n}\n\n.products-left .products-left-board .products-left-data {\n  padding: 0px;\n  width: calc(100% - 20px);\n  display: grid;\n}\n\n.products-left .products-left-board .products-left-checkbox {\n  width: 20px;\n  display: flex;\n  justify-content: center;\n  padding: 0px;\n}\n\n.products-left .products-left-status {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  padding-right: 0px;\n  padding-left: 0px;\n  flex-direction: row;\n  font-size: 13px;\n  color: #0ea8da;\n  padding-top: 10px;\n}\n\n.products-left .products-left-status .see-more {\n  flex: 1;\n  color: #0ea8da;\n  cursor: pointer;\n  text-align: right;\n}\n\n.products-left .products-left-status .see-more:hover {\n  color: #0275d8;\n}\n\n.products-left .products-left-status .col-md-3 {\n  position: relative;\n  justify-items: left;\n  padding-left: 0px;\n  padding-bottom: 0px;\n}\n\n.products-left .products-left-status .col-md-9 {\n  text-align: right;\n  width: 100%;\n  max-width: unset;\n  display: block;\n  padding-right: 0px;\n  padding-top: 0px;\n}\n\n#h3 {\n  width: 95%;\n  margin-bottom: 24px;\n  color: black;\n}\n\n#sku {\n  font-size: 14px;\n  color: #bebebe;\n  margin-bottom: 0px;\n}\n\n.price {\n  color: #0ea8da;\n}\n\n.price-total {\n  color: #0ea8da;\n}\n\n.normal-text {\n  font-size: 13px;\n  color: #777777;\n}\n\n.header-text {\n  font-size: 14px;\n  font-weight: bold;\n  margin-bottom: 1px;\n}\n\n.header-text.order-id-text {\n  color: #0ea8da;\n}\n\n.header-text.order-id-text span {\n  color: #777777;\n}\n\n.form-label > label {\n  display: block;\n}\n\n#transaction {\n  width: 130px;\n}\n\n#download {\n  vertical-align: middle;\n}\n\n#label-header {\n  font-size: 20px;\n  color: black;\n}\n\n#button-swapper {\n  margin-left: 5px;\n  margin-right: 5px;\n  width: calc((100% / 6) - 10px);\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\n#card-transaction {\n  width: 100%;\n  align-items: baseline;\n}\n\n#search {\n  margin-right: 20px;\n}\n\n#search-field {\n  margin-right: 10px;\n}\n\n#search-text {\n  padding-right: 23px;\n}\n\n#search-icon {\n  margin-left: -20px;\n  margin-right: 20px;\n}\n\n#btn-print {\n  font-size: 11px;\n  width: 130px;\n  margin: 15px 20px 2px 0px;\n  height: 30px;\n  border-radius: 15px;\n}\n\n#btn-download {\n  font-size: 11px;\n  width: 130px;\n  margin: 15px 20px 2px 0px;\n  height: 30px;\n  border-radius: 15px;\n}\n\n#calendar-icon {\n  margin-left: -20px;\n  margin-right: 20px;\n}\n\n.payment-status {\n  padding: 2px 5px;\n  display: flex;\n  text-align: center;\n  align-items: center;\n  justify-content: center;\n  margin-bottom: 5px;\n  width: 97px;\n  height: 34px;\n  font-size: 14px;\n  border-radius: 35px;\n}\n\n.payment-status.PAID {\n  color: white;\n  background-color: #0ea8da;\n  text-align: center;\n}\n\n.payment-status.CANCEL {\n  color: white;\n  background-color: #e74c3c;\n  text-align: center;\n}\n\n.date-picker-wrapper {\n  position: relative;\n  margin-right: 10px;\n}\n\n.date-picker-wrapper .date-picker-overlay {\n  z-index: 99;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker {\n  background: white;\n  overflow: hidden;\n  border: 1px solid #dfdfdf;\n  position: absolute;\n  top: 30px;\n  display: grid;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker .btn.ok {\n  clear: both;\n  background-color: #3498db;\n  color: white;\n  margin: 0px 5px 10px;\n  border-color: transparent;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker .btn.clear {\n  clear: both;\n  background-color: #ac0a0a;\n  color: white;\n  margin: 0px 10px 10px;\n  border-color: transparent;\n}\n\n.date-picker-wrapper .date-picker-overlay .dpicker ngb-datepicker {\n  float: left;\n  clear: both;\n  margin: 10px;\n  right: 0%;\n}\n\n.date-picker-wrapper .date-picker-overlay.false {\n  display: none;\n}\n\n.custom-day {\n  text-align: center;\n  padding: 0.185rem 0.25rem;\n  display: inline-block;\n  height: 2rem;\n  width: 2rem;\n}\n\n.custom-day.focused {\n  background-color: #e6e6e6;\n}\n\n.custom-day.range,\n.custom-day:hover {\n  background-color: #0275d8;\n  color: white;\n}\n\n.custom-day.faded {\n  background-color: rgba(2, 117, 216, 0.5);\n}\n\n.order-status {\n  display: flex;\n  flex-wrap: wrap;\n  place-content: center;\n}\n\n.order-status .status-name {\n  position: relative;\n  display: inline-flex;\n  justify-content: center;\n  flex-direction: row;\n}\n\n.order-status .status-name .badge {\n  display: flex;\n  position: absolute;\n  justify-content: center;\n  align-items: center;\n  font-size: 10px;\n  color: white;\n  border-radius: 50px;\n  left: -15px;\n  width: 23px;\n  height: 23px;\n  background-color: rgba(231, 76, 60, 0.8);\n  border: 1px solid #e74c3c;\n  top: -14px;\n}\n\n.loading {\n  margin-bottom: 20px;\n}\n\n.sk-fading-circle {\n  margin: 10px auto;\n  width: 40px;\n  height: 40px;\n  position: relative;\n}\n\n.sk-fading-circle .sk-circle {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  left: 0;\n  top: 0;\n}\n\n.sk-fading-circle .sk-circle:before {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 15%;\n  height: 15%;\n  background-color: #333;\n  border-radius: 100%;\n  -webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;\n  animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;\n}\n\n.sk-fading-circle .sk-circle2 {\n  -webkit-transform: rotate(30deg);\n  transform: rotate(30deg);\n}\n\n.sk-fading-circle .sk-circle3 {\n  -webkit-transform: rotate(60deg);\n  transform: rotate(60deg);\n}\n\n.sk-fading-circle .sk-circle4 {\n  -webkit-transform: rotate(90deg);\n  transform: rotate(90deg);\n}\n\n.sk-fading-circle .sk-circle5 {\n  -webkit-transform: rotate(120deg);\n  transform: rotate(120deg);\n}\n\n.sk-fading-circle .sk-circle6 {\n  -webkit-transform: rotate(150deg);\n  transform: rotate(150deg);\n}\n\n.sk-fading-circle .sk-circle7 {\n  -webkit-transform: rotate(180deg);\n  transform: rotate(180deg);\n}\n\n.sk-fading-circle .sk-circle8 {\n  -webkit-transform: rotate(210deg);\n  transform: rotate(210deg);\n}\n\n.sk-fading-circle .sk-circle9 {\n  -webkit-transform: rotate(240deg);\n  transform: rotate(240deg);\n}\n\n.sk-fading-circle .sk-circle10 {\n  -webkit-transform: rotate(270deg);\n  transform: rotate(270deg);\n}\n\n.sk-fading-circle .sk-circle11 {\n  -webkit-transform: rotate(300deg);\n  transform: rotate(300deg);\n}\n\n.sk-fading-circle .sk-circle12 {\n  -webkit-transform: rotate(330deg);\n  transform: rotate(330deg);\n}\n\n.sk-fading-circle .sk-circle2:before {\n  -webkit-animation-delay: -1.1s;\n  animation-delay: -1.1s;\n}\n\n.sk-fading-circle .sk-circle3:before {\n  -webkit-animation-delay: -1s;\n  animation-delay: -1s;\n}\n\n.sk-fading-circle .sk-circle4:before {\n  -webkit-animation-delay: -0.9s;\n  animation-delay: -0.9s;\n}\n\n.sk-fading-circle .sk-circle5:before {\n  -webkit-animation-delay: -0.8s;\n  animation-delay: -0.8s;\n}\n\n.sk-fading-circle .sk-circle6:before {\n  -webkit-animation-delay: -0.7s;\n  animation-delay: -0.7s;\n}\n\n.sk-fading-circle .sk-circle7:before {\n  -webkit-animation-delay: -0.6s;\n  animation-delay: -0.6s;\n}\n\n.sk-fading-circle .sk-circle8:before {\n  -webkit-animation-delay: -0.5s;\n  animation-delay: -0.5s;\n}\n\n.sk-fading-circle .sk-circle9:before {\n  -webkit-animation-delay: -0.4s;\n  animation-delay: -0.4s;\n}\n\n.sk-fading-circle .sk-circle10:before {\n  -webkit-animation-delay: -0.3s;\n  animation-delay: -0.3s;\n}\n\n.sk-fading-circle .sk-circle11:before {\n  -webkit-animation-delay: -0.2s;\n  animation-delay: -0.2s;\n}\n\n.sk-fading-circle .sk-circle12:before {\n  -webkit-animation-delay: -0.1s;\n  animation-delay: -0.1s;\n}\n\n@media screen and (max-width: 1280px) {\n  .default-product {\n    flex-direction: column;\n  }\n  .default-product .product-detail .header-text.order-id-text {\n    margin-top: 16px;\n  }\n  .default-product .product-detail .header-text span {\n    font-size: 12px;\n    display: block;\n  }\n}\n\n@media screen and (max-width: 1080px) {\n  .default-product {\n    width: 100%;\n    overflow: hidden;\n  }\n  .default-product .product-detail .header-text span {\n    display: block;\n  }\n  .default-product .product-detail .date-text {\n    font-size: 11px;\n  }\n\n  .div-default-product .default-product .img-wrapper {\n    width: 100%;\n  }\n}\n\n@-webkit-keyframes sk-circleFadeDelay {\n  0%, 39%, 100% {\n    opacity: 0;\n  }\n  40% {\n    opacity: 1;\n  }\n}\n\n@keyframes sk-circleFadeDelay {\n  0%, 39%, 100% {\n    opacity: 0;\n  }\n  40% {\n    opacity: 1;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9vcmRlci1oaXN0b3JpZXMvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtZXJjaGFudC1wb3J0YWxcXG9yZGVyLWhpc3Rvcmllc1xcb3JkZXItaGlzdG9yaWVzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL29yZGVyLWhpc3Rvcmllcy9vcmRlci1oaXN0b3JpZXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUE7RUFDQyxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7QUNIRDs7QURLQTtFQUNDLGlCQUFBO0FDRkQ7O0FESUE7O0VBRUMsc0JBQUE7QUNERDs7QURJQTtFQUNDLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUNERDs7QURJQTtFQUNDLG1CQUFBO0VBRUEsYUFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDRkQ7O0FER0M7RUFDQyxXQUFBO0VBQ0Esa0JBQUE7QUNERjs7QURHQztFQUNDLGFBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0RGOztBREVFO0VBQ0Msa0JBQUE7QUNBSDs7QURFRTs7RUFFQyxZQUFBO0VBQ0EsbUJBQUE7QUNBSDs7QURDRzs7RUFDQyxZQUFBO0FDRUo7O0FEQ0U7RUFDQyxXQUFBO0FDQ0g7O0FEQUc7RUFDQyxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsU0FBQTtFQUNBLHdDQUFBO0FDRUo7O0FEQUc7RUFDQyx5QkFBQTtFQUVBLGtCQUFBO0VBQ0EsVUFBQTtBQ0NKOztBRENHO0VBQ0MsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0FDQ0o7O0FEQ0c7RUFDQyxtQkFBQTtFQUNBLFlBQUE7QUNDSjs7QURLQTtFQUNDLGVBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7RUFDQSx1QkFBQTtBQ0ZEOztBREtBO0VBQ0MsYUFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ0ZEOztBREdDO0VBQ0Msa0JBQUE7QUNERjs7QURHQztFQUNDLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtBQ0RGOztBREVFO0VBQ0MsYUFBQTtFQUNBLG1CQUFBO0FDQUg7O0FEQ0c7RUFDQyxpQkFBQTtBQ0NKOztBREtBO0VBQ0MsMkJBQUE7RUFBQSx3QkFBQTtFQUFBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ0ZEOztBREdDO0VBQ0MsMEJBQUE7RUFBQSx1QkFBQTtFQUFBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLG9DQUFBO0VBQ0EsbUJBQUE7QUNERjs7QURFRTtFQUNDLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FDQUg7O0FERUU7RUFDQyxtQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ0FIOztBREdFO0VBQ0MsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUNESDs7QURNQTtFQUNDLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQ0hEOztBREtDO0VBR0MsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLDRCQUFBO0VBQ0EseUJBQUE7QUNMRjs7QURRQztFQUNDLG1DQUFBO0VBQ0EsWUFBQTtBQ05GOztBRFFDO0VBQ0MsWUFBQTtBQ05GOztBRFlFO0VBQ0MsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUNUSDs7QURjQTtFQUNDLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUNYRDs7QURZQztFQUNDLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNWRjs7QURXRTtFQUNDLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQ1RIOztBRFVHO0VBQ0MsOEJBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0tBQUEsZ0JBQUE7RUFDQSxtQkFBQTtBQ1JKOztBRGNBO0VBQ0MsYUFBQTtFQUNBLFlBQUE7QUNYRDs7QURZQztFQUNDLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNWRjs7QURXRTtFQUNDLHlCQUFBO0FDVEg7O0FEZUc7RUFDQyxpQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQ2JKOztBRG1CQTtFQUNDLFVBQUE7QUNoQkQ7O0FEaUJDO0VBQ0MsYUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNmRjs7QURnQkU7RUFDQyxZQUFBO0VBQ0Esd0JBQUE7RUFDQSxhQUFBO0FDZEg7O0FEZ0JFO0VBQ0MsV0FBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7QUNkSDs7QURpQkM7RUFDQyxhQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQ2ZGOztBRGdCRTtFQUNDLE9BQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDZEg7O0FEZ0JFO0VBQ0MsY0FBQTtBQ2RIOztBRGdCRTtFQUNDLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FDZEg7O0FEZ0JFO0VBQ0MsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ2RIOztBRHVEQTtFQUNDLFVBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNwREQ7O0FEdURBO0VBQ0MsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQ3BERDs7QUR1REE7RUFDQyxjQUFBO0FDcEREOztBRHVEQTtFQUNDLGNBQUE7QUNwREQ7O0FEdURBO0VBQ0MsZUFBQTtFQUNBLGNBOVZZO0FDMFNiOztBRHVEQTtFQUNDLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDcEREOztBRHNEQTtFQUNDLGNBeldhO0FDc1RkOztBRG9EQztFQUNDLGNBeldXO0FDdVRiOztBRHVEQztFQUNDLGNBQUE7QUNwREY7O0FEdURBO0VBQ0MsWUFBQTtBQ3BERDs7QUR1REE7RUFDQyxzQkFBQTtBQ3BERDs7QUR1REE7RUFDQyxlQUFBO0VBQ0EsWUFBQTtBQ3BERDs7QUR1REE7RUFDQyxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7QUNwREQ7O0FEdURBO0VBQ0MsV0FBQTtFQUNBLHFCQUFBO0FDcEREOztBRHVEQTtFQUNDLGtCQUFBO0FDcEREOztBRHVEQTtFQUNDLGtCQUFBO0FDcEREOztBRHVEQTtFQUNDLG1CQUFBO0FDcEREOztBRHVEQTtFQUNDLGtCQUFBO0VBQ0Esa0JBQUE7QUNwREQ7O0FEdURBO0VBQ0MsZUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ3BERDs7QUR1REE7RUFDQyxlQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDcEREOztBRHVEQTtFQUNDLGtCQUFBO0VBQ0Esa0JBQUE7QUNwREQ7O0FEdURBO0VBQ0MsZ0JBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ3BERDs7QURzREE7RUFDQyxZQUFBO0VBQ0EseUJBbmNhO0VBb2NiLGtCQUFBO0FDbkREOztBRHNEQTtFQUNDLFlBQUE7RUFDQSx5QkF4Y1U7RUF5Y1Ysa0JBQUE7QUNuREQ7O0FEc0RBO0VBQ0Msa0JBQUE7RUFDQSxrQkFBQTtBQ25ERDs7QURxREM7RUFDQyxXQUFBO0FDbkRGOztBRG9ERTtFQUNDLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGFBQUE7QUNsREg7O0FEb0RHO0VBQ0MsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0VBQ0EseUJBQUE7QUNsREo7O0FEb0RHO0VBQ0MsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7QUNsREo7O0FEb0RHO0VBQ0MsV0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtBQ2xESjs7QUR1REM7RUFDQyxhQUFBO0FDckRGOztBRHlEQTtFQUNDLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDdEREOztBRHlEQTtFQUNDLHlCQUFBO0FDdEREOztBRHdEQTs7RUFFQyx5QkFBQTtFQUNBLFlBQUE7QUNyREQ7O0FEdURBO0VBQ0Msd0NBQUE7QUNwREQ7O0FEdURBO0VBQ0MsYUFBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtBQ3BERDs7QURxREM7RUFDQyxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ25ERjs7QURvREU7RUFDQyxhQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHdDQUFBO0VBQ0EseUJBQUE7RUFDQSxVQUFBO0FDbERIOztBRHdEQTtFQUNDLG1CQUFBO0FDckREOztBRHdEQTtFQUNDLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ3JERDs7QUR3REE7RUFDQyxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7QUNyREQ7O0FEd0RBO0VBQ0MsV0FBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0VBQUE7RUFDQSw0REFBQTtBQ3JERDs7QUR1REE7RUFDQyxnQ0FBQTtFQUVBLHdCQUFBO0FDcEREOztBRHNEQTtFQUNDLGdDQUFBO0VBRUEsd0JBQUE7QUNuREQ7O0FEcURBO0VBQ0MsZ0NBQUE7RUFFQSx3QkFBQTtBQ2xERDs7QURvREE7RUFDQyxpQ0FBQTtFQUVBLHlCQUFBO0FDakREOztBRG1EQTtFQUNDLGlDQUFBO0VBRUEseUJBQUE7QUNoREQ7O0FEa0RBO0VBQ0MsaUNBQUE7RUFFQSx5QkFBQTtBQy9DRDs7QURpREE7RUFDQyxpQ0FBQTtFQUVBLHlCQUFBO0FDOUNEOztBRGdEQTtFQUNDLGlDQUFBO0VBRUEseUJBQUE7QUM3Q0Q7O0FEK0NBO0VBQ0MsaUNBQUE7RUFFQSx5QkFBQTtBQzVDRDs7QUQ4Q0E7RUFDQyxpQ0FBQTtFQUVBLHlCQUFBO0FDM0NEOztBRDZDQTtFQUNDLGlDQUFBO0VBRUEseUJBQUE7QUMxQ0Q7O0FENENBO0VBQ0MsOEJBQUE7RUFDQSxzQkFBQTtBQ3pDRDs7QUQyQ0E7RUFDQyw0QkFBQTtFQUNBLG9CQUFBO0FDeENEOztBRDBDQTtFQUNDLDhCQUFBO0VBQ0Esc0JBQUE7QUN2Q0Q7O0FEeUNBO0VBQ0MsOEJBQUE7RUFDQSxzQkFBQTtBQ3RDRDs7QUR3Q0E7RUFDQyw4QkFBQTtFQUNBLHNCQUFBO0FDckNEOztBRHVDQTtFQUNDLDhCQUFBO0VBQ0Esc0JBQUE7QUNwQ0Q7O0FEc0NBO0VBQ0MsOEJBQUE7RUFDQSxzQkFBQTtBQ25DRDs7QURxQ0E7RUFDQyw4QkFBQTtFQUNBLHNCQUFBO0FDbENEOztBRG9DQTtFQUNDLDhCQUFBO0VBQ0Esc0JBQUE7QUNqQ0Q7O0FEbUNBO0VBQ0MsOEJBQUE7RUFDQSxzQkFBQTtBQ2hDRDs7QURrQ0E7RUFDQyw4QkFBQTtFQUNBLHNCQUFBO0FDL0JEOztBRGtDQTtFQUNDO0lBQ0Msc0JBQUE7RUMvQkE7RURpQ0M7SUFDQyxnQkFBQTtFQy9CRjtFRGtDRTtJQUNDLGVBQUE7SUFDQSxjQUFBO0VDaENIO0FBQ0Y7O0FEc0NBO0VBQ0M7SUFDQyxXQUFBO0lBQ0EsZ0JBQUE7RUNwQ0E7RUR1Q0U7SUFDQyxjQUFBO0VDckNIO0VEd0NDO0lBQ0MsZUFBQTtFQ3RDRjs7RUQ2Q0M7SUFDQyxXQUFBO0VDMUNGO0FBQ0Y7O0FEcURBO0VBQ0M7SUFHQyxVQUFBO0VDckRBO0VEdUREO0lBQ0MsVUFBQTtFQ3JEQTtBQUNGOztBRHdEQTtFQUNDO0lBR0MsVUFBQTtFQ3hEQTtFRDBERDtJQUNDLFVBQUE7RUN4REE7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvb3JkZXItaGlzdG9yaWVzL29yZGVyLWhpc3Rvcmllcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRibHVlLXN0YXR1czogIzBlYThkYTtcclxuJHJlZC10ZXh0OiAjZTc0YzNjO1xyXG4kY29sb3ItZ3JleTogIzc3Nzc3NztcclxuXHJcbjpob3N0IHtcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0aGVpZ2h0OiAxMDAlO1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGxlZnQ6IDBweDtcclxuXHR0b3A6IDUwcHg7XHJcbn1cclxuKjpmb2N1cyB7XHJcblx0Ym94LXNoYWRvdzogdW5zZXQ7XHJcbn1cclxuZGl2LFxyXG5zcGFuOm5vdCguZmEpIHtcclxuXHRmb250LWZhbWlseTogXCJQb3BwaW5zXCI7XHJcbn1cclxuXHJcbi5ib2FyZCB7XHJcblx0aGVpZ2h0OiAxMDAlO1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdG1pbi13aWR0aDogNzIwcHg7XHJcbn1cclxuXHJcbi5uYXYudG9vbGJhciB7XHJcblx0YmFja2dyb3VuZDogI2ZjZmNmYztcclxuXHQvLyBjb2xvcjogd2hpdGU7XHJcblx0aGVpZ2h0OiAxNTBweDtcclxuXHRwYWRkaW5nLXRvcDogMjBweDtcclxuXHRwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdC5zZWxlY3QtYWxsIHtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0YWxpZ24taXRlbXM6IHJpZ2h0O1xyXG5cdH1cclxuXHQuY2FyZCB7XHJcblx0XHRwYWRkaW5nOiAyMHB4O1xyXG5cdFx0d2lkdGg6IDk1JTtcclxuXHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0XHRtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG5cdFx0aDMge1xyXG5cdFx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0XHR9XHJcblx0XHR0cixcclxuXHRcdHRkIHtcclxuXHRcdFx0Y29sb3I6IGJsYWNrO1xyXG5cdFx0XHR2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG5cdFx0XHRzZWxlY3Qge1xyXG5cdFx0XHRcdGhlaWdodDogMzBweDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0LmNhcmQtY29udGVudCB7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRociB7XHJcblx0XHRcdFx0bWFyZ2luLXRvcDogMHJlbTtcclxuXHRcdFx0XHRtYXJnaW4tYm90dG9tOiAwcmVtO1xyXG5cdFx0XHRcdGJvcmRlcjogMDtcclxuXHRcdFx0XHRib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHRhYmxlIHtcclxuXHRcdFx0XHRib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG5cdFx0XHRcdC8vIGJvcmRlcjogMnB4IHNvbGlkICNlYWVhZWE7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogNXB4O1xyXG5cdFx0XHRcdHdpZHRoOiA5NSU7XHJcblx0XHRcdH1cclxuXHRcdFx0YnV0dG9uIHtcclxuXHRcdFx0XHRjb2xvcjogcmdiYSg1MiwgNzMsIDk0LCAxKTtcclxuXHRcdFx0XHRib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6IHdoaXRlO1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMTVweDtcclxuXHRcdFx0XHRtYXJnaW4tdG9wOiAxMHB4O1xyXG5cdFx0XHRcdGJvcmRlcjogMnB4IHNvbGlkICNlYWVhZWE7XHJcblx0XHRcdFx0d2lkdGg6IDk1JTtcclxuXHRcdFx0XHRwYWRkaW5nOiAxNnB4O1xyXG5cdFx0XHR9XHJcblx0XHRcdGJ1dHRvbi50cnVlIHtcclxuXHRcdFx0XHRiYWNrZ3JvdW5kOiAjMmMzZTUwO1xyXG5cdFx0XHRcdGNvbG9yOiB3aGl0ZTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLmZvcm0tY29udHJvbCB7XHJcblx0Zm9udC1zaXplOiAxMXB4O1xyXG5cdHdpZHRoOiAxNTBweDtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0bWFyZ2luOiAycHggMHB4IDJweCAwcHg7XHJcbn1cclxuXHJcbi5kaXYtdHJhbnNhY3Rpb24ge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0ZmxleC13cmFwOiB3cmFwO1xyXG5cdGZvbnQtc2l6ZTogMTFweDtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRsYWJlbCB7XHJcblx0XHRtYXJnaW4tYm90dG9tOiAwcHg7XHJcblx0fVxyXG5cdGRpdi5zZWFyY2hiYXIge1xyXG5cdFx0ZmxleC13cmFwOiB3cmFwO1xyXG5cdFx0ZGlzcGxheTogaW5oZXJpdDtcclxuXHRcdHdpZHRoOiBjYWxjKDEwMCUgLSAxNDBweCk7XHJcblx0XHRwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcblx0XHRqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG5cdFx0ZGl2IHtcclxuXHRcdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRcdFx0bGFiZWwge1xyXG5cdFx0XHRcdG1hcmdpbi1yaWdodDogNXB4O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG4uZGl2LWZvb3RlciB7XHJcblx0aGVpZ2h0OiBmaXQtY29udGVudDtcclxuXHR3aWR0aDogMTAwJTtcclxuXHR0ZXh0LWFsaWduOiAtd2Via2l0LXJpZ2h0O1xyXG5cdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRmbG9hdDpyaWdodDtcclxuXHRkaXYge1xyXG5cdFx0d2lkdGg6IGZpdC1jb250ZW50O1xyXG5cdFx0aGVpZ2h0OiBhdXRvO1xyXG5cdFx0cGFkZGluZzogMHB4O1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogNXB4O1xyXG5cdFx0Ym9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogMTBweDtcclxuXHRcdGJ1dHRvbiB7XHJcblx0XHRcdGNvbG9yOiByZ2JhKDUyLCA3MywgOTQsIDEpO1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czogMTBweDtcclxuXHRcdH1cclxuXHRcdGJ1dHRvbjpob3ZlciB7XHJcblx0XHRcdGJhY2tncm91bmQ6ICMyYzNlNTA7XHJcblx0XHRcdGNvbG9yOiB3aGl0ZTtcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czogMTBweDtcclxuXHRcdH1cclxuXHJcblx0XHRidXR0b246YWN0aXZle1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiAjZWFlYWVhO1xyXG5cdFx0XHRjb2xvcjpibGFjaztcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czoxMHB4O1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLmRpdi1jb250ZW50LXJvdyB7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRmbGV4LWRpcmVjdGlvbjogcm93O1xyXG5cdGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcblx0YWxpZ24taXRlbXM6IHVuc2V0O1xyXG5cdGp1c3RpZnktY29udGVudDogdW5zZXQ7XHJcblx0Ym9yZGVyLXJhZGl1czogMTBweDtcclxuXHRtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG5cdHBhZGRpbmc6IDEwcHg7XHJcblxyXG5cdC5kaXYtY29udGVudC1jZWxsIHtcclxuXHRcdC8vIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuXHRcdC8vIGZsZXg6IDE7XHJcblx0XHRwYWRkaW5nOiA1cHggMTJweDtcclxuXHRcdG1heC13aWR0aDogdW5zZXQ7XHJcblx0XHRib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjY2NjO1xyXG5cdFx0d2lkdGg6IGNhbGMoMTAwJSAtIDU2MHB4KTtcclxuXHR9XHJcblxyXG5cdC5kaXYtY29udGVudC1jZWxsOmxhc3QtY2hpbGQge1xyXG5cdFx0Ym9yZGVyLXJpZ2h0OiAwcHggc29saWQgdHJhbnNwYXJlbnQ7XHJcblx0XHR3aWR0aDogMjgwcHg7XHJcblx0fVxyXG5cdD4gLmRpdi1jb250ZW50LWNlbGw6bnRoLWNoaWxkKDIpIHtcclxuXHRcdHdpZHRoOiAyODBweDtcclxuXHR9XHJcbn1cclxuXHJcbi5kZWZhdWx0LXByb2R1Y3Qge1xyXG5cdC5wcm9kdWN0LWRldGFpbCB7XHJcblx0XHQuZGF0ZS10ZXh0IHtcclxuXHRcdFx0Zm9udC1zaXplOiAxMXB4O1xyXG5cdFx0XHRtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG5cdFx0XHRtYXJnaW4tdG9wOiAxMHB4O1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLm1vcmUtcHJvZHVjdCB7XHJcblx0b3ZlcmZsb3cteTogYXV0bztcclxuXHRvdmVyZmxvdy14OiBoaWRkZW47XHJcblx0ZGlzcGxheTogZ3JpZDtcclxuXHRoZWlnaHQ6IDIwMHB4O1xyXG5cdHBhZGRpbmc6IDBweDtcclxuXHQubG9vcC1wcm9kdWN0IHtcclxuXHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG5cdFx0cGFkZGluZzogMHB4O1xyXG5cdFx0LmNhcmQge1xyXG5cdFx0XHRtYXJnaW4tbGVmdDogMHB4O1xyXG5cdFx0XHRtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcblx0XHRcdGhlaWdodDogMTY1cHg7XHJcblx0XHRcdHdpZHRoOiAyMDBweDtcclxuXHRcdFx0cGFkZGluZzogMHB4O1xyXG5cdFx0XHQuaW1nIHtcclxuXHRcdFx0XHRoZWlnaHQ6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7XHJcblx0XHRcdFx0YmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG5cdFx0XHRcdG9iamVjdC1maXQ6IGZpbGw7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogMTBweDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLmRpdi1kZWZhdWx0LXByb2R1Y3Qge1xyXG5cdGRpc3BsYXk6IGdyaWQ7XHJcblx0cGFkZGluZzogMHB4O1xyXG5cdC5kZWZhdWx0LXByb2R1Y3Qge1xyXG5cdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcblx0XHRwYWRkaW5nOiAwcHg7XHJcblx0XHQuaW1nLXdyYXBwZXIge1xyXG5cdFx0XHRwYWRkaW5nOiAwcHggMjBweCAwcHggMHB4O1xyXG5cclxuXHRcdFx0Ly8gaGVpZ2h0OiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xyXG5cdFx0XHQvLyBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcblx0XHRcdC8vIG9iamVjdC1maXQ6IGZpbGw7XHJcblx0XHRcdC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcblx0XHRcdC5pbWFnZSB7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogNSU7XHJcblx0XHRcdFx0d2lkdGg6IDEyMHB4O1xyXG5cdFx0XHRcdGhlaWdodDogYXV0bztcclxuXHRcdFx0XHRvYmplY3QtZml0OiBjb3ZlcjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLnByb2R1Y3RzLWxlZnQge1xyXG5cdHdpZHRoOiAyMCU7XHJcblx0LnByb2R1Y3RzLWxlZnQtYm9hcmQge1xyXG5cdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdHBhZGRpbmc6IDBweDtcclxuXHRcdG92ZXJmbG93OiBoaWRkZW47XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdC5wcm9kdWN0cy1sZWZ0LWRhdGEge1xyXG5cdFx0XHRwYWRkaW5nOiAwcHg7XHJcblx0XHRcdHdpZHRoOiBjYWxjKDEwMCUgLSAyMHB4KTtcclxuXHRcdFx0ZGlzcGxheTogZ3JpZDtcclxuXHRcdH1cclxuXHRcdC5wcm9kdWN0cy1sZWZ0LWNoZWNrYm94IHtcclxuXHRcdFx0d2lkdGg6IDIwcHg7XHJcblx0XHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdFx0XHRwYWRkaW5nOiAwcHg7XHJcblx0XHR9XHJcblx0fVxyXG5cdC5wcm9kdWN0cy1sZWZ0LXN0YXR1cyB7XHJcblx0XHRkaXNwbGF5OiBmbGV4O1xyXG5cdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdFx0cGFkZGluZy1yaWdodDogMHB4O1xyXG5cdFx0cGFkZGluZy1sZWZ0OiAwcHg7XHJcblx0XHRmbGV4LWRpcmVjdGlvbjogcm93O1xyXG5cdFx0Zm9udC1zaXplOiAxM3B4O1xyXG5cdFx0Y29sb3I6ICMwZWE4ZGE7XHJcblx0XHRwYWRkaW5nLXRvcDogMTBweDtcclxuXHRcdC5zZWUtbW9yZSB7XHJcblx0XHRcdGZsZXg6IDE7XHJcblx0XHRcdGNvbG9yOiAjMGVhOGRhO1xyXG5cdFx0XHRjdXJzb3I6IHBvaW50ZXI7XHJcblx0XHRcdHRleHQtYWxpZ246IHJpZ2h0O1xyXG5cdFx0fVxyXG5cdFx0LnNlZS1tb3JlOmhvdmVyIHtcclxuXHRcdFx0Y29sb3I6IHJnYigyLCAxMTcsIDIxNik7XHJcblx0XHR9XHJcblx0XHQuY29sLW1kLTMge1xyXG5cdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdGp1c3RpZnktaXRlbXM6IGxlZnQ7XHJcblx0XHRcdHBhZGRpbmctbGVmdDogMHB4O1xyXG5cdFx0XHRwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG5cdFx0fVxyXG5cdFx0LmNvbC1tZC05IHtcclxuXHRcdFx0dGV4dC1hbGlnbjogcmlnaHQ7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRtYXgtd2lkdGg6IHVuc2V0O1xyXG5cdFx0XHRkaXNwbGF5OiBibG9jaztcclxuXHRcdFx0cGFkZGluZy1yaWdodDogMHB4O1xyXG5cdFx0XHRwYWRkaW5nLXRvcDogMHB4O1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLy8gQG1lZGlhIChtaW4td2lkdGg6IDc2OXB4KSBhbmQgKG1heC13aWR0aDogMTA3NHB4KSB7XHJcbi8vICAgICAuZ3JpZC12aWV3e1xyXG4vLyAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuLy8gICAgICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgICAgICBoZWlnaHQ6MTAwJTtcclxuLy8gICAgICAgICAuZ3JpZC12aWV3LWl0ZW17XHJcbi8vICAgICAgICAgICAgIHdpZHRoOiBjYWxjKDI1dncgLSA1NnB4KTtcclxuLy8gICAgICAgICAgICAgaGVpZ2h0OiBjYWxjKDI1dncgIC0gNTZweCArIDUwcHgpO1xyXG4vLyAgICAgICAgIH1cclxuLy8gICAgIH1cclxuLy8gfVxyXG5cclxuLy8gQG1lZGlhIChtaW4td2lkdGg6IDQ2MHB4KSBhbmQgKG1heC13aWR0aDogNzY5cHgpIHtcclxuLy8gICAgIC5ncmlkLXZpZXd7XHJcbi8vICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4vLyAgICAgICAgIGhlaWdodDoxMDAlO1xyXG4vLyAgICAgICAgIC5ncmlkLXZpZXctaXRlbXtcclxuLy8gICAgICAgICAgICAgd2lkdGg6IGNhbGMoMzMuMzMzdncgLSA3M3B4KTtcclxuLy8gICAgICAgICAgICAgaGVpZ2h0OiBjYWxjKDMzLjMzM3Z3ICAtIDczcHggKyA1MHB4KTtcclxuLy8gICAgICAgICB9XHJcbi8vICAgICB9XHJcbi8vIH1cclxuXHJcbi8vIEBtZWRpYSAobWluLXdpZHRoOiAzMjBweCkgYW5kIChtYXgtd2lkdGg6IDQ2MHB4KSB7XHJcbi8vICAgICAuZ3JpZC12aWV3e1xyXG4vLyAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuLy8gICAgICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgICAgICBoZWlnaHQ6MTAwJTtcclxuLy8gICAgICAgICAuZ3JpZC12aWV3LWl0ZW17XHJcbi8vICAgICAgICAgICAgIHdpZHRoOiBjYWxjKDUwdncgLSAxMDdweCk7XHJcbi8vICAgICAgICAgICAgIGhlaWdodDogY2FsYyg1MHZ3ICAtIDEwN3B4ICsgNTBweCk7XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgfVxyXG4vLyB9XHJcblxyXG4jaDMge1xyXG5cdHdpZHRoOiA5NSU7XHJcblx0bWFyZ2luLWJvdHRvbTogMjRweDtcclxuXHRjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbiNza3Uge1xyXG5cdGZvbnQtc2l6ZTogMTRweDtcclxuXHRjb2xvcjogI2JlYmViZTtcclxuXHRtYXJnaW4tYm90dG9tOiAwcHg7XHJcbn1cclxuXHJcbi5wcmljZSB7XHJcblx0Y29sb3I6ICMwZWE4ZGE7XHJcbn1cclxuXHJcbi5wcmljZS10b3RhbCB7XHJcblx0Y29sb3I6ICMwZWE4ZGE7XHJcbn1cclxuXHJcbi5ub3JtYWwtdGV4dCB7XHJcblx0Zm9udC1zaXplOiAxM3B4O1xyXG5cdGNvbG9yOiAkY29sb3ItZ3JleTtcclxufVxyXG5cclxuLmhlYWRlci10ZXh0IHtcclxuXHRmb250LXNpemU6IDE0cHg7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0bWFyZ2luLWJvdHRvbTogMXB4O1xyXG59XHJcbi5oZWFkZXItdGV4dC5vcmRlci1pZC10ZXh0IHtcclxuXHRjb2xvcjogJGJsdWUtc3RhdHVzO1xyXG5cdHNwYW4ge1xyXG5cdFx0Y29sb3I6ICRjb2xvci1ncmV5O1xyXG5cdH1cclxufVxyXG5cclxuLmZvcm0tbGFiZWwge1xyXG5cdD4gbGFiZWwge1xyXG5cdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0fVxyXG59XHJcbiN0cmFuc2FjdGlvbiB7XHJcblx0d2lkdGg6IDEzMHB4O1xyXG59XHJcblxyXG4jZG93bmxvYWQge1xyXG5cdHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbiNsYWJlbC1oZWFkZXIge1xyXG5cdGZvbnQtc2l6ZTogMjBweDtcclxuXHRjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbiNidXR0b24tc3dhcHBlciB7XHJcblx0bWFyZ2luLWxlZnQ6IDVweDtcclxuXHRtYXJnaW4tcmlnaHQ6IDVweDtcclxuXHR3aWR0aDogY2FsYygoMTAwJSAvIDYpIC0gMTBweCk7XHJcblx0d2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG59XHJcblxyXG4jY2FyZC10cmFuc2FjdGlvbiB7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0YWxpZ24taXRlbXM6IGJhc2VsaW5lO1xyXG59XHJcblxyXG4jc2VhcmNoIHtcclxuXHRtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbiNzZWFyY2gtZmllbGQge1xyXG5cdG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuI3NlYXJjaC10ZXh0IHtcclxuXHRwYWRkaW5nLXJpZ2h0OiAyM3B4O1xyXG59XHJcblxyXG4jc2VhcmNoLWljb24ge1xyXG5cdG1hcmdpbi1sZWZ0OiAtMjBweDtcclxuXHRtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbiNidG4tcHJpbnQge1xyXG5cdGZvbnQtc2l6ZTogMTFweDtcclxuXHR3aWR0aDogMTMwcHg7XHJcblx0bWFyZ2luOiAxNXB4IDIwcHggMnB4IDBweDtcclxuXHRoZWlnaHQ6IDMwcHg7XHJcblx0Ym9yZGVyLXJhZGl1czogMTVweDtcclxufVxyXG5cclxuI2J0bi1kb3dubG9hZCB7XHJcblx0Zm9udC1zaXplOiAxMXB4O1xyXG5cdHdpZHRoOiAxMzBweDtcclxuXHRtYXJnaW46IDE1cHggMjBweCAycHggMHB4O1xyXG5cdGhlaWdodDogMzBweDtcclxuXHRib3JkZXItcmFkaXVzOiAxNXB4O1xyXG59XHJcblxyXG4jY2FsZW5kYXItaWNvbiB7XHJcblx0bWFyZ2luLWxlZnQ6IC0yMHB4O1xyXG5cdG1hcmdpbi1yaWdodDogMjBweDtcclxufVxyXG5cclxuLnBheW1lbnQtc3RhdHVzIHtcclxuXHRwYWRkaW5nOiAycHggNXB4O1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblx0bWFyZ2luLWJvdHRvbTogNXB4O1xyXG5cdHdpZHRoOiA5N3B4O1xyXG5cdGhlaWdodDogMzRweDtcclxuXHRmb250LXNpemU6IDE0cHg7XHJcblx0Ym9yZGVyLXJhZGl1czogMzVweDtcclxufVxyXG4ucGF5bWVudC1zdGF0dXMuUEFJRCB7XHJcblx0Y29sb3I6IHdoaXRlO1xyXG5cdGJhY2tncm91bmQtY29sb3I6ICRibHVlLXN0YXR1cztcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5wYXltZW50LXN0YXR1cy5DQU5DRUwge1xyXG5cdGNvbG9yOiB3aGl0ZTtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiAkcmVkLXRleHQ7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uZGF0ZS1waWNrZXItd3JhcHBlciB7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdG1hcmdpbi1yaWdodDogMTBweDtcclxuXHJcblx0LmRhdGUtcGlja2VyLW92ZXJsYXkge1xyXG5cdFx0ei1pbmRleDogOTk7XHJcblx0XHQuZHBpY2tlciB7XHJcblx0XHRcdGJhY2tncm91bmQ6IHdoaXRlO1xyXG5cdFx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdFx0XHRib3JkZXI6IDFweCBzb2xpZCAjZGZkZmRmO1xyXG5cdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRcdHRvcDogMzBweDtcclxuXHRcdFx0ZGlzcGxheTogZ3JpZDtcclxuXHJcblx0XHRcdC5idG4ub2sge1xyXG5cdFx0XHRcdGNsZWFyOiBib3RoO1xyXG5cdFx0XHRcdGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcblx0XHRcdFx0Y29sb3I6IHdoaXRlO1xyXG5cdFx0XHRcdG1hcmdpbjogMHB4IDVweCAxMHB4O1xyXG5cdFx0XHRcdGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcblx0XHRcdH1cclxuXHRcdFx0LmJ0bi5jbGVhciB7XHJcblx0XHRcdFx0Y2xlYXI6IGJvdGg7XHJcblx0XHRcdFx0YmFja2dyb3VuZC1jb2xvcjogcmdiKDE3MiwgMTAsIDEwKTtcclxuXHRcdFx0XHRjb2xvcjogd2hpdGU7XHJcblx0XHRcdFx0bWFyZ2luOiAwcHggMTBweCAxMHB4O1xyXG5cdFx0XHRcdGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcblx0XHRcdH1cclxuXHRcdFx0bmdiLWRhdGVwaWNrZXIge1xyXG5cdFx0XHRcdGZsb2F0OiBsZWZ0O1xyXG5cdFx0XHRcdGNsZWFyOiBib3RoO1xyXG5cdFx0XHRcdG1hcmdpbjogMTBweDtcclxuXHRcdFx0XHRyaWdodDogMCU7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdC5kYXRlLXBpY2tlci1vdmVybGF5LmZhbHNlIHtcclxuXHRcdGRpc3BsYXk6IG5vbmU7XHJcblx0fVxyXG59XHJcblxyXG4uY3VzdG9tLWRheSB7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdHBhZGRpbmc6IDAuMTg1cmVtIDAuMjVyZW07XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMnJlbTtcclxuXHR3aWR0aDogMnJlbTtcclxufVxyXG5cclxuLmN1c3RvbS1kYXkuZm9jdXNlZCB7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogI2U2ZTZlNjtcclxufVxyXG4uY3VzdG9tLWRheS5yYW5nZSxcclxuLmN1c3RvbS1kYXk6aG92ZXIge1xyXG5cdGJhY2tncm91bmQtY29sb3I6IHJnYigyLCAxMTcsIDIxNik7XHJcblx0Y29sb3I6IHdoaXRlO1xyXG59XHJcbi5jdXN0b20tZGF5LmZhZGVkIHtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIsIDExNywgMjE2LCAwLjUpO1xyXG59XHJcblxyXG4ub3JkZXItc3RhdHVzIHtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGZsZXgtd3JhcDogd3JhcDtcclxuXHRwbGFjZS1jb250ZW50OiBjZW50ZXI7XHJcblx0LnN0YXR1cy1uYW1lIHtcclxuXHRcdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG5cdFx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblx0XHRmbGV4LWRpcmVjdGlvbjogcm93O1xyXG5cdFx0LmJhZGdlIHtcclxuXHRcdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHRcdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRcdFx0Zm9udC1zaXplOiAxMHB4O1xyXG5cdFx0XHRjb2xvcjogd2hpdGU7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcblx0XHRcdGxlZnQ6IC0xNXB4O1xyXG5cdFx0XHR3aWR0aDogMjNweDtcclxuXHRcdFx0aGVpZ2h0OiAyM3B4O1xyXG5cdFx0XHRiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIzMSwgNzYsIDYwLCAwLjgpO1xyXG5cdFx0XHRib3JkZXI6IDFweCBzb2xpZCAjZTc0YzNjO1xyXG5cdFx0XHR0b3A6IC0xNHB4O1xyXG5cdFx0XHQvLyB0cmFuc2Zvcm06IHNjYWxlKDAuOCk7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG4ubG9hZGluZyB7XHJcblx0bWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLnNrLWZhZGluZy1jaXJjbGUge1xyXG5cdG1hcmdpbjogMTBweCBhdXRvO1xyXG5cdHdpZHRoOiA0MHB4O1xyXG5cdGhlaWdodDogNDBweDtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUge1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGhlaWdodDogMTAwJTtcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0bGVmdDogMDtcclxuXHR0b3A6IDA7XHJcbn1cclxuXHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU6YmVmb3JlIHtcclxuXHRjb250ZW50OiBcIlwiO1xyXG5cdGRpc3BsYXk6IGJsb2NrO1xyXG5cdG1hcmdpbjogMCBhdXRvO1xyXG5cdHdpZHRoOiAxNSU7XHJcblx0aGVpZ2h0OiAxNSU7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogIzMzMztcclxuXHRib3JkZXItcmFkaXVzOiAxMDAlO1xyXG5cdC13ZWJraXQtYW5pbWF0aW9uOiBzay1jaXJjbGVGYWRlRGVsYXkgMS4ycyBpbmZpbml0ZSBlYXNlLWluLW91dCBib3RoO1xyXG5cdGFuaW1hdGlvbjogc2stY2lyY2xlRmFkZURlbGF5IDEuMnMgaW5maW5pdGUgZWFzZS1pbi1vdXQgYm90aDtcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMiB7XHJcblx0LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzMGRlZyk7XHJcblx0LW1zLXRyYW5zZm9ybTogcm90YXRlKDMwZGVnKTtcclxuXHR0cmFuc2Zvcm06IHJvdGF0ZSgzMGRlZyk7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTMge1xyXG5cdC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNjBkZWcpO1xyXG5cdC1tcy10cmFuc2Zvcm06IHJvdGF0ZSg2MGRlZyk7XHJcblx0dHJhbnNmb3JtOiByb3RhdGUoNjBkZWcpO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU0IHtcclxuXHQtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcclxuXHQtbXMtdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xyXG5cdHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNSB7XHJcblx0LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxMjBkZWcpO1xyXG5cdC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgxMjBkZWcpO1xyXG5cdHRyYW5zZm9ybTogcm90YXRlKDEyMGRlZyk7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTYge1xyXG5cdC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTUwZGVnKTtcclxuXHQtbXMtdHJhbnNmb3JtOiByb3RhdGUoMTUwZGVnKTtcclxuXHR0cmFuc2Zvcm06IHJvdGF0ZSgxNTBkZWcpO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU3IHtcclxuXHQtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XHJcblx0LW1zLXRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XHJcblx0dHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlOCB7XHJcblx0LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgyMTBkZWcpO1xyXG5cdC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgyMTBkZWcpO1xyXG5cdHRyYW5zZm9ybTogcm90YXRlKDIxMGRlZyk7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTkge1xyXG5cdC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMjQwZGVnKTtcclxuXHQtbXMtdHJhbnNmb3JtOiByb3RhdGUoMjQwZGVnKTtcclxuXHR0cmFuc2Zvcm06IHJvdGF0ZSgyNDBkZWcpO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMCB7XHJcblx0LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgyNzBkZWcpO1xyXG5cdC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgyNzBkZWcpO1xyXG5cdHRyYW5zZm9ybTogcm90YXRlKDI3MGRlZyk7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTExIHtcclxuXHQtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDMwMGRlZyk7XHJcblx0LW1zLXRyYW5zZm9ybTogcm90YXRlKDMwMGRlZyk7XHJcblx0dHJhbnNmb3JtOiByb3RhdGUoMzAwZGVnKTtcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTIge1xyXG5cdC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzMwZGVnKTtcclxuXHQtbXMtdHJhbnNmb3JtOiByb3RhdGUoMzMwZGVnKTtcclxuXHR0cmFuc2Zvcm06IHJvdGF0ZSgzMzBkZWcpO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUyOmJlZm9yZSB7XHJcblx0LXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0xLjFzO1xyXG5cdGFuaW1hdGlvbi1kZWxheTogLTEuMXM7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTM6YmVmb3JlIHtcclxuXHQtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTFzO1xyXG5cdGFuaW1hdGlvbi1kZWxheTogLTFzO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU0OmJlZm9yZSB7XHJcblx0LXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjlzO1xyXG5cdGFuaW1hdGlvbi1kZWxheTogLTAuOXM7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTU6YmVmb3JlIHtcclxuXHQtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuOHM7XHJcblx0YW5pbWF0aW9uLWRlbGF5OiAtMC44cztcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNjpiZWZvcmUge1xyXG5cdC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC43cztcclxuXHRhbmltYXRpb24tZGVsYXk6IC0wLjdzO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU3OmJlZm9yZSB7XHJcblx0LXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjZzO1xyXG5cdGFuaW1hdGlvbi1kZWxheTogLTAuNnM7XHJcbn1cclxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTg6YmVmb3JlIHtcclxuXHQtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuNXM7XHJcblx0YW5pbWF0aW9uLWRlbGF5OiAtMC41cztcclxufVxyXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlOTpiZWZvcmUge1xyXG5cdC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC40cztcclxuXHRhbmltYXRpb24tZGVsYXk6IC0wLjRzO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMDpiZWZvcmUge1xyXG5cdC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC4zcztcclxuXHRhbmltYXRpb24tZGVsYXk6IC0wLjNzO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMTpiZWZvcmUge1xyXG5cdC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC4ycztcclxuXHRhbmltYXRpb24tZGVsYXk6IC0wLjJzO1xyXG59XHJcbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUxMjpiZWZvcmUge1xyXG5cdC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC4xcztcclxuXHRhbmltYXRpb24tZGVsYXk6IC0wLjFzO1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjgwcHgpIHtcclxuXHQuZGVmYXVsdC1wcm9kdWN0IHtcclxuXHRcdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0XHQucHJvZHVjdC1kZXRhaWwge1xyXG5cdFx0XHQuaGVhZGVyLXRleHQub3JkZXItaWQtdGV4dCB7XHJcblx0XHRcdFx0bWFyZ2luLXRvcDogMTZweDtcclxuXHRcdFx0fVxyXG5cdFx0XHQuaGVhZGVyLXRleHQge1xyXG5cdFx0XHRcdHNwYW4ge1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdFx0XHRcdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMDgwcHgpIHtcclxuXHQuZGVmYXVsdC1wcm9kdWN0IHtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHRcdC5wcm9kdWN0LWRldGFpbCB7XHJcblx0XHRcdC5oZWFkZXItdGV4dCB7XHJcblx0XHRcdFx0c3BhbiB7XHJcblx0XHRcdFx0XHRkaXNwbGF5OiBibG9jaztcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0LmRhdGUtdGV4dCB7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxMXB4O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQuZGl2LWRlZmF1bHQtcHJvZHVjdCB7XHJcblx0XHQuZGVmYXVsdC1wcm9kdWN0IHtcclxuXHRcdFx0LmltZy13cmFwcGVyIHtcclxuXHRcdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0LmRpdi1jb250ZW50LXJvdyB7XHJcblx0XHQuZGl2LWNvbnRlbnQtY2VsbDpmaXJzdC1jaGlsZCB7XHJcblx0XHRcdC8vIHdpZHRoOlxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuQC13ZWJraXQta2V5ZnJhbWVzIHNrLWNpcmNsZUZhZGVEZWxheSB7XHJcblx0MCUsXHJcblx0MzklLFxyXG5cdDEwMCUge1xyXG5cdFx0b3BhY2l0eTogMDtcclxuXHR9XHJcblx0NDAlIHtcclxuXHRcdG9wYWNpdHk6IDE7XHJcblx0fVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIHNrLWNpcmNsZUZhZGVEZWxheSB7XHJcblx0MCUsXHJcblx0MzklLFxyXG5cdDEwMCUge1xyXG5cdFx0b3BhY2l0eTogMDtcclxuXHR9XHJcblx0NDAlIHtcclxuXHRcdG9wYWNpdHk6IDE7XHJcblx0fVxyXG59XHJcbiIsIjpob3N0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBsZWZ0OiAwcHg7XG4gIHRvcDogNTBweDtcbn1cblxuKjpmb2N1cyB7XG4gIGJveC1zaGFkb3c6IHVuc2V0O1xufVxuXG5kaXYsXG5zcGFuOm5vdCguZmEpIHtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xufVxuXG4uYm9hcmQge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBtaW4td2lkdGg6IDcyMHB4O1xufVxuXG4ubmF2LnRvb2xiYXIge1xuICBiYWNrZ3JvdW5kOiAjZmNmY2ZjO1xuICBoZWlnaHQ6IDE1MHB4O1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm5hdi50b29sYmFyIC5zZWxlY3QtYWxsIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGFsaWduLWl0ZW1zOiByaWdodDtcbn1cbi5uYXYudG9vbGJhciAuY2FyZCB7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIHdpZHRoOiA5NSU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG4ubmF2LnRvb2xiYXIgLmNhcmQgaDMge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubmF2LnRvb2xiYXIgLmNhcmQgdHIsXG4ubmF2LnRvb2xiYXIgLmNhcmQgdGQge1xuICBjb2xvcjogYmxhY2s7XG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XG59XG4ubmF2LnRvb2xiYXIgLmNhcmQgdHIgc2VsZWN0LFxuLm5hdi50b29sYmFyIC5jYXJkIHRkIHNlbGVjdCB7XG4gIGhlaWdodDogMzBweDtcbn1cbi5uYXYudG9vbGJhciAuY2FyZCAuY2FyZC1jb250ZW50IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubmF2LnRvb2xiYXIgLmNhcmQgLmNhcmQtY29udGVudCBociB7XG4gIG1hcmdpbi10b3A6IDByZW07XG4gIG1hcmdpbi1ib3R0b206IDByZW07XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xKTtcbn1cbi5uYXYudG9vbGJhciAuY2FyZCAuY2FyZC1jb250ZW50IHRhYmxlIHtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICB3aWR0aDogOTUlO1xufVxuLm5hdi50b29sYmFyIC5jYXJkIC5jYXJkLWNvbnRlbnQgYnV0dG9uIHtcbiAgY29sb3I6ICMzNDQ5NWU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNlYWVhZWE7XG4gIHdpZHRoOiA5NSU7XG4gIHBhZGRpbmc6IDE2cHg7XG59XG4ubmF2LnRvb2xiYXIgLmNhcmQgLmNhcmQtY29udGVudCBidXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQ6ICMyYzNlNTA7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmZvcm0tY29udHJvbCB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgd2lkdGg6IDE1MHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbjogMnB4IDBweCAycHggMHB4O1xufVxuXG4uZGl2LXRyYW5zYWN0aW9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBmb250LXNpemU6IDExcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmRpdi10cmFuc2FjdGlvbiBsYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5kaXYtdHJhbnNhY3Rpb24gZGl2LnNlYXJjaGJhciB7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgZGlzcGxheTogaW5oZXJpdDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDE0MHB4KTtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuLmRpdi10cmFuc2FjdGlvbiBkaXYuc2VhcmNoYmFyIGRpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZGl2LXRyYW5zYWN0aW9uIGRpdi5zZWFyY2hiYXIgZGl2IGxhYmVsIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5kaXYtZm9vdGVyIHtcbiAgaGVpZ2h0OiBmaXQtY29udGVudDtcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IC13ZWJraXQtcmlnaHQ7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsb2F0OiByaWdodDtcbn1cbi5kaXYtZm9vdGVyIGRpdiB7XG4gIHdpZHRoOiBmaXQtY29udGVudDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuLmRpdi1mb290ZXIgZGl2IGJ1dHRvbiB7XG4gIGNvbG9yOiAjMzQ0OTVlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cbi5kaXYtZm9vdGVyIGRpdiBidXR0b246aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMmMzZTUwO1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG4uZGl2LWZvb3RlciBkaXYgYnV0dG9uOmFjdGl2ZSB7XG4gIGJhY2tncm91bmQ6ICNlYWVhZWE7XG4gIGNvbG9yOiBibGFjaztcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cblxuLmRpdi1jb250ZW50LXJvdyB7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBhbGlnbi1pdGVtczogdW5zZXQ7XG4gIGp1c3RpZnktY29udGVudDogdW5zZXQ7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG4uZGl2LWNvbnRlbnQtcm93IC5kaXYtY29udGVudC1jZWxsIHtcbiAgcGFkZGluZzogNXB4IDEycHg7XG4gIG1heC13aWR0aDogdW5zZXQ7XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNjY2M7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA1NjBweCk7XG59XG4uZGl2LWNvbnRlbnQtcm93IC5kaXYtY29udGVudC1jZWxsOmxhc3QtY2hpbGQge1xuICBib3JkZXItcmlnaHQ6IDBweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgd2lkdGg6IDI4MHB4O1xufVxuLmRpdi1jb250ZW50LXJvdyA+IC5kaXYtY29udGVudC1jZWxsOm50aC1jaGlsZCgyKSB7XG4gIHdpZHRoOiAyODBweDtcbn1cblxuLmRlZmF1bHQtcHJvZHVjdCAucHJvZHVjdC1kZXRhaWwgLmRhdGUtdGV4dCB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cblxuLm1vcmUtcHJvZHVjdCB7XG4gIG92ZXJmbG93LXk6IGF1dG87XG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgZGlzcGxheTogZ3JpZDtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgcGFkZGluZzogMHB4O1xufVxuLm1vcmUtcHJvZHVjdCAubG9vcC1wcm9kdWN0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgcGFkZGluZzogMHB4O1xufVxuLm1vcmUtcHJvZHVjdCAubG9vcC1wcm9kdWN0IC5jYXJkIHtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBoZWlnaHQ6IDE2NXB4O1xuICB3aWR0aDogMjAwcHg7XG4gIHBhZGRpbmc6IDBweDtcbn1cbi5tb3JlLXByb2R1Y3QgLmxvb3AtcHJvZHVjdCAuY2FyZCAuaW1nIHtcbiAgaGVpZ2h0OiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIG9iamVjdC1maXQ6IGZpbGw7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi5kaXYtZGVmYXVsdC1wcm9kdWN0IHtcbiAgZGlzcGxheTogZ3JpZDtcbiAgcGFkZGluZzogMHB4O1xufVxuLmRpdi1kZWZhdWx0LXByb2R1Y3QgLmRlZmF1bHQtcHJvZHVjdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIHBhZGRpbmc6IDBweDtcbn1cbi5kaXYtZGVmYXVsdC1wcm9kdWN0IC5kZWZhdWx0LXByb2R1Y3QgLmltZy13cmFwcGVyIHtcbiAgcGFkZGluZzogMHB4IDIwcHggMHB4IDBweDtcbn1cbi5kaXYtZGVmYXVsdC1wcm9kdWN0IC5kZWZhdWx0LXByb2R1Y3QgLmltZy13cmFwcGVyIC5pbWFnZSB7XG4gIGJvcmRlci1yYWRpdXM6IDUlO1xuICB3aWR0aDogMTIwcHg7XG4gIGhlaWdodDogYXV0bztcbiAgb2JqZWN0LWZpdDogY292ZXI7XG59XG5cbi5wcm9kdWN0cy1sZWZ0IHtcbiAgd2lkdGg6IDIwJTtcbn1cbi5wcm9kdWN0cy1sZWZ0IC5wcm9kdWN0cy1sZWZ0LWJvYXJkIHtcbiAgZGlzcGxheTogZmxleDtcbiAgcGFkZGluZzogMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbn1cbi5wcm9kdWN0cy1sZWZ0IC5wcm9kdWN0cy1sZWZ0LWJvYXJkIC5wcm9kdWN0cy1sZWZ0LWRhdGEge1xuICBwYWRkaW5nOiAwcHg7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAyMHB4KTtcbiAgZGlzcGxheTogZ3JpZDtcbn1cbi5wcm9kdWN0cy1sZWZ0IC5wcm9kdWN0cy1sZWZ0LWJvYXJkIC5wcm9kdWN0cy1sZWZ0LWNoZWNrYm94IHtcbiAgd2lkdGg6IDIwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBwYWRkaW5nOiAwcHg7XG59XG4ucHJvZHVjdHMtbGVmdCAucHJvZHVjdHMtbGVmdC1zdGF0dXMge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgZm9udC1zaXplOiAxM3B4O1xuICBjb2xvcjogIzBlYThkYTtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4ucHJvZHVjdHMtbGVmdCAucHJvZHVjdHMtbGVmdC1zdGF0dXMgLnNlZS1tb3JlIHtcbiAgZmxleDogMTtcbiAgY29sb3I6ICMwZWE4ZGE7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG4ucHJvZHVjdHMtbGVmdCAucHJvZHVjdHMtbGVmdC1zdGF0dXMgLnNlZS1tb3JlOmhvdmVyIHtcbiAgY29sb3I6ICMwMjc1ZDg7XG59XG4ucHJvZHVjdHMtbGVmdCAucHJvZHVjdHMtbGVmdC1zdGF0dXMgLmNvbC1tZC0zIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBqdXN0aWZ5LWl0ZW1zOiBsZWZ0O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cbi5wcm9kdWN0cy1sZWZ0IC5wcm9kdWN0cy1sZWZ0LXN0YXR1cyAuY29sLW1kLTkge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1heC13aWR0aDogdW5zZXQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG59XG5cbiNoMyB7XG4gIHdpZHRoOiA5NSU7XG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuI3NrdSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICNiZWJlYmU7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cblxuLnByaWNlIHtcbiAgY29sb3I6ICMwZWE4ZGE7XG59XG5cbi5wcmljZS10b3RhbCB7XG4gIGNvbG9yOiAjMGVhOGRhO1xufVxuXG4ubm9ybWFsLXRleHQge1xuICBmb250LXNpemU6IDEzcHg7XG4gIGNvbG9yOiAjNzc3Nzc3O1xufVxuXG4uaGVhZGVyLXRleHQge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBtYXJnaW4tYm90dG9tOiAxcHg7XG59XG5cbi5oZWFkZXItdGV4dC5vcmRlci1pZC10ZXh0IHtcbiAgY29sb3I6ICMwZWE4ZGE7XG59XG4uaGVhZGVyLXRleHQub3JkZXItaWQtdGV4dCBzcGFuIHtcbiAgY29sb3I6ICM3Nzc3Nzc7XG59XG5cbi5mb3JtLWxhYmVsID4gbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuI3RyYW5zYWN0aW9uIHtcbiAgd2lkdGg6IDEzMHB4O1xufVxuXG4jZG93bmxvYWQge1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuXG4jbGFiZWwtaGVhZGVyIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBjb2xvcjogYmxhY2s7XG59XG5cbiNidXR0b24tc3dhcHBlciB7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICB3aWR0aDogY2FsYygoMTAwJSAvIDYpIC0gMTBweCk7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuXG4jY2FyZC10cmFuc2FjdGlvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogYmFzZWxpbmU7XG59XG5cbiNzZWFyY2gge1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG5cbiNzZWFyY2gtZmllbGQge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbiNzZWFyY2gtdGV4dCB7XG4gIHBhZGRpbmctcmlnaHQ6IDIzcHg7XG59XG5cbiNzZWFyY2gtaWNvbiB7XG4gIG1hcmdpbi1sZWZ0OiAtMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xufVxuXG4jYnRuLXByaW50IHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICB3aWR0aDogMTMwcHg7XG4gIG1hcmdpbjogMTVweCAyMHB4IDJweCAwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbn1cblxuI2J0bi1kb3dubG9hZCB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgd2lkdGg6IDEzMHB4O1xuICBtYXJnaW46IDE1cHggMjBweCAycHggMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG59XG5cbiNjYWxlbmRhci1pY29uIHtcbiAgbWFyZ2luLWxlZnQ6IC0yMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG5cbi5wYXltZW50LXN0YXR1cyB7XG4gIHBhZGRpbmc6IDJweCA1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgd2lkdGg6IDk3cHg7XG4gIGhlaWdodDogMzRweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiAzNXB4O1xufVxuXG4ucGF5bWVudC1zdGF0dXMuUEFJRCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzBlYThkYTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ucGF5bWVudC1zdGF0dXMuQ0FOQ0VMIHtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTc0YzNjO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5kYXRlLXBpY2tlci13cmFwcGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uZGF0ZS1waWNrZXItd3JhcHBlciAuZGF0ZS1waWNrZXItb3ZlcmxheSB7XG4gIHotaW5kZXg6IDk5O1xufVxuLmRhdGUtcGlja2VyLXdyYXBwZXIgLmRhdGUtcGlja2VyLW92ZXJsYXkgLmRwaWNrZXIge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2RmZGZkZjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDMwcHg7XG4gIGRpc3BsYXk6IGdyaWQ7XG59XG4uZGF0ZS1waWNrZXItd3JhcHBlciAuZGF0ZS1waWNrZXItb3ZlcmxheSAuZHBpY2tlciAuYnRuLm9rIHtcbiAgY2xlYXI6IGJvdGg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luOiAwcHggNXB4IDEwcHg7XG4gIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG4uZGF0ZS1waWNrZXItd3JhcHBlciAuZGF0ZS1waWNrZXItb3ZlcmxheSAuZHBpY2tlciAuYnRuLmNsZWFyIHtcbiAgY2xlYXI6IGJvdGg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhYzBhMGE7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luOiAwcHggMTBweCAxMHB4O1xuICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xufVxuLmRhdGUtcGlja2VyLXdyYXBwZXIgLmRhdGUtcGlja2VyLW92ZXJsYXkgLmRwaWNrZXIgbmdiLWRhdGVwaWNrZXIge1xuICBmbG9hdDogbGVmdDtcbiAgY2xlYXI6IGJvdGg7XG4gIG1hcmdpbjogMTBweDtcbiAgcmlnaHQ6IDAlO1xufVxuLmRhdGUtcGlja2VyLXdyYXBwZXIgLmRhdGUtcGlja2VyLW92ZXJsYXkuZmFsc2Uge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4uY3VzdG9tLWRheSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZzogMC4xODVyZW0gMC4yNXJlbTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBoZWlnaHQ6IDJyZW07XG4gIHdpZHRoOiAycmVtO1xufVxuXG4uY3VzdG9tLWRheS5mb2N1c2VkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2ZTZlNjtcbn1cblxuLmN1c3RvbS1kYXkucmFuZ2UsXG4uY3VzdG9tLWRheTpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMjc1ZDg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmN1c3RvbS1kYXkuZmFkZWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIsIDExNywgMjE2LCAwLjUpO1xufVxuXG4ub3JkZXItc3RhdHVzIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBwbGFjZS1jb250ZW50OiBjZW50ZXI7XG59XG4ub3JkZXItc3RhdHVzIC5zdGF0dXMtbmFtZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xufVxuLm9yZGVyLXN0YXR1cyAuc3RhdHVzLW5hbWUgLmJhZGdlIHtcbiAgZGlzcGxheTogZmxleDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIGxlZnQ6IC0xNXB4O1xuICB3aWR0aDogMjNweDtcbiAgaGVpZ2h0OiAyM3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIzMSwgNzYsIDYwLCAwLjgpO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTc0YzNjO1xuICB0b3A6IC0xNHB4O1xufVxuXG4ubG9hZGluZyB7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIHtcbiAgbWFyZ2luOiAxMHB4IGF1dG87XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDAgYXV0bztcbiAgd2lkdGg6IDE1JTtcbiAgaGVpZ2h0OiAxNSU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzMzM7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBzay1jaXJjbGVGYWRlRGVsYXkgMS4ycyBpbmZpbml0ZSBlYXNlLWluLW91dCBib3RoO1xuICBhbmltYXRpb246IHNrLWNpcmNsZUZhZGVEZWxheSAxLjJzIGluZmluaXRlIGVhc2UtaW4tb3V0IGJvdGg7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUyIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzMGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgzMGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDMwZGVnKTtcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTMge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDYwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDYwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoNjBkZWcpO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNCB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU1IHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxMjBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMTIwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTIwZGVnKTtcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTYge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE1MGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgxNTBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgxNTBkZWcpO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNyB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU4IHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgyMTBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMjEwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMjEwZGVnKTtcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTkge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDI0MGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgyNDBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgyNDBkZWcpO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTAge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDI3MGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgyNzBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgyNzBkZWcpO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTEge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDMwMGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgzMDBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgzMDBkZWcpO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTIge1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDMzMGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgzMzBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgzMzBkZWcpO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMjpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTEuMXM7XG4gIGFuaW1hdGlvbi1kZWxheTogLTEuMXM7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGUzOmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMXM7XG4gIGFuaW1hdGlvbi1kZWxheTogLTFzO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNDpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuOXM7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuOXM7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU1OmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC44cztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC44cztcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTY6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjdzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjdzO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlNzpiZWZvcmUge1xuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogLTAuNnM7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuNnM7XG59XG5cbi5zay1mYWRpbmctY2lyY2xlIC5zay1jaXJjbGU4OmJlZm9yZSB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAtMC41cztcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC41cztcbn1cblxuLnNrLWZhZGluZy1jaXJjbGUgLnNrLWNpcmNsZTk6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjRzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjRzO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTA6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjNzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjNzO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTE6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjJzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjJzO1xufVxuXG4uc2stZmFkaW5nLWNpcmNsZSAuc2stY2lyY2xlMTI6YmVmb3JlIHtcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IC0wLjFzO1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjFzO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjgwcHgpIHtcbiAgLmRlZmF1bHQtcHJvZHVjdCB7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuICAuZGVmYXVsdC1wcm9kdWN0IC5wcm9kdWN0LWRldGFpbCAuaGVhZGVyLXRleHQub3JkZXItaWQtdGV4dCB7XG4gICAgbWFyZ2luLXRvcDogMTZweDtcbiAgfVxuICAuZGVmYXVsdC1wcm9kdWN0IC5wcm9kdWN0LWRldGFpbCAuaGVhZGVyLXRleHQgc3BhbiB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMDgwcHgpIHtcbiAgLmRlZmF1bHQtcHJvZHVjdCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgfVxuICAuZGVmYXVsdC1wcm9kdWN0IC5wcm9kdWN0LWRldGFpbCAuaGVhZGVyLXRleHQgc3BhbiB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cbiAgLmRlZmF1bHQtcHJvZHVjdCAucHJvZHVjdC1kZXRhaWwgLmRhdGUtdGV4dCB7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICB9XG5cbiAgLmRpdi1kZWZhdWx0LXByb2R1Y3QgLmRlZmF1bHQtcHJvZHVjdCAuaW1nLXdyYXBwZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59XG5ALXdlYmtpdC1rZXlmcmFtZXMgc2stY2lyY2xlRmFkZURlbGF5IHtcbiAgMCUsIDM5JSwgMTAwJSB7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICA0MCUge1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbn1cbkBrZXlmcmFtZXMgc2stY2lyY2xlRmFkZURlbGF5IHtcbiAgMCUsIDM5JSwgMTAwJSB7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICA0MCUge1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/order-histories/order-histories.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/order-histories/order-histories.component.ts ***!
  \*************************************************************************************/
/*! exports provided: OrderHistoriesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderHistoriesComponent", function() { return OrderHistoriesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var OrderHistoriesComponent = /** @class */ (function () {
    function OrderHistoriesComponent(orderhistoryService, router, route, memberService) {
        this.orderhistoryService = orderhistoryService;
        this.router = router;
        this.route = route;
        this.memberService = memberService;
        this.onSearchActive = false;
        this.showLoading = false;
        this.allData = [];
        this.form_input = {};
        this.errorLabel = false;
        this.allMemberDetail = false;
        this.swaper = [
            { name: 'All Order', val: true, value: 'all order' },
            { name: 'Incoming', val: false, value: 'on process', values: 'process' },
            { name: 'Packaging', val: false, value: 'on packaging', values: 'packaging' },
            { name: 'On Delivery', val: false, value: 'on delivery', values: 'delivery' },
            { name: 'Delivered', val: false, value: 'delivered', values: 'delivered' },
            { name: 'Canceled', val: false, value: 'cancel', values: 'cancel' }
        ];
        this.filterBy = [
            { name: 'Order ID', value: 'order_id' },
            { name: 'Buyer Name', value: 'buyer_name' },
            { name: 'Product Name', value: 'product_name' },
            { name: 'No. Resi', value: 'no_resi' }
        ];
        this.sortBy = [
            { name: 'Newest', value: 'newest' },
            { name: 'Highest Transactions', value: 'highest transactions' },
            { name: 'Lowest Transactions', value: 'lowest transactions' }
        ];
        this.toggler = {};
        this.pageNumbering = [];
        this.currentPage = 1;
        this.pageLimits = '20';
        this.pagesLimiter = [];
        this.orderBy = "newest";
        this.filter = "order_id";
        this.checkBox = [];
        this.activeCheckbox = false;
        this.filterResult = {};
        this.orderByResult = { order_date: -1 };
        this.getDataCheck = [];
        this.service = this.orderhistoryService;
    }
    OrderHistoriesComponent.prototype.callDetail = function (order_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    OrderHistoriesComponent.prototype.openDetail = function (order_id) {
        // this.router.navigate(['merchant-portal/order-detail/edit'],  {queryParams: {id: order_id }})
    };
    //Shipping Status Button Swapping Function
    OrderHistoriesComponent.prototype.swapClick = function (index) {
        return __awaiter(this, void 0, void 0, function () {
            var status, params, process_1, process_2, swaper;
            return __generator(this, function (_a) {
                if (this.form_input.transaction_status) {
                    delete this.form_input.transaction_status;
                }
                this.swaper.forEach(function (e) {
                    e.val = false;
                });
                this.swaper[index].val = true;
                status = this.swaper[index].values;
                console.log("Status", status);
                if (status == "all order") {
                    this.firstLoad();
                }
                else if (status == "process") {
                    process_1 = {
                        transaction_status: 'PAID',
                        shipping_status: 'process'
                    };
                    Object.assign(this.form_input, process_1);
                }
                else if (status == "cancel") {
                    process_2 = {
                        transaction_status: 'CANCEL',
                    };
                    Object.assign(this.form_input, process_2);
                }
                else {
                    swaper = {
                        shipping_status: status,
                        payment_status: 'PAID',
                    };
                    Object.assign(this.form_input, swaper);
                }
                this.valuechange({}, false, false);
                return [2 /*return*/];
            });
        });
    };
    OrderHistoriesComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderHistoriesComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var member, admin, result, e_1, filter, filter_pending, result, resultPending, productStatus_1, e_2;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.memberService.getMemberDetail()];
                    case 1:
                        member = _a.sent();
                        this.currentPermission = member.result.permission;
                        if (!(this.currentPermission == 'admin')) return [3 /*break*/, 5];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        admin = {
                            search: { status: "paid", merchant_username: "loca" },
                            limit_per_page: 10,
                            page: 1,
                            order_by: this.orderByResult,
                        };
                        return [4 /*yield*/, this.orderhistoryService.searchOrderhistorysummaryLint(admin)];
                    case 3:
                        result = _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5:
                        if (!(this.currentPermission == 'merchant')) return [3 /*break*/, 10];
                        _a.label = 6;
                    case 6:
                        _a.trys.push([6, 9, , 10]);
                        filter = {
                            search: {},
                            order_by: this.orderByResult,
                            limit_per_page: this.pageLimits,
                            current_page: this.currentPage,
                            download: false
                        };
                        filter_pending = {
                            search: {
                                shipping_status: "process",
                                transaction_status: "PAID"
                            },
                            order_by: this.orderByResult,
                            limit_per_page: this.pageLimits,
                            current_page: this.currentPage,
                            download: false
                        };
                        this.showLoading = true;
                        return [4 /*yield*/, this.orderhistoryService.getOrderHistoryMerchantFilter(filter)];
                    case 7:
                        result = _a.sent();
                        return [4 /*yield*/, this.orderhistoryService.getOrderHistoryMerchantFilter(filter_pending)];
                    case 8:
                        resultPending = _a.sent();
                        this.totalValuePending = resultPending.result.total_all_values;
                        this.showLoading = false;
                        this.allData = result.result.values;
                        this.totalPage = result.result.total_page;
                        console.log(this.allData);
                        this.buildPagesNumbers(this.totalPage);
                        productStatus_1 = {
                            product_status: false
                        };
                        this.allData.forEach(function (el, i) {
                            Object.assign(_this_1.allData[i], productStatus_1);
                        });
                        return [3 /*break*/, 10];
                    case 9:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    OrderHistoriesComponent.prototype.type = function (array) {
        var res = [];
        this.allData.forEach(function (el, i) {
            el.product_list.forEach(function (element) {
                res.push(element.type);
            });
            // Object.assign(this.allData[i], this.prodType)
        });
        // console.log("hasil",res[0])
        return res[array];
    };
    //When Page Changed
    OrderHistoriesComponent.prototype.onChangePage = function (pageNumber) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (pageNumber <= 0) {
                    pageNumber = 1;
                }
                if (pageNumber >= this.total_page) {
                    pageNumber = this.total_page;
                }
                this.currentPage = pageNumber;
                console.log(pageNumber, this.currentPage, this.total_page);
                this.valuechange({}, false, false);
                return [2 /*return*/];
            });
        });
    };
    //Order by Function
    OrderHistoriesComponent.prototype.orderBySelected = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                if (this.orderBy == "highest transactions") {
                    this.orderByResult = {
                        total_price: -1
                    };
                    this.valuechange({}, false, false);
                }
                else if (this.orderBy == "lowest transactions") {
                    this.orderByResult = {
                        total_price: 1
                    };
                    this.valuechange({}, false, false);
                }
                else if (this.orderBy == "newest") {
                    this.orderByResult = {
                        order_date: -1
                    };
                    this.valuechange({}, false, false);
                }
                return [2 /*return*/];
            });
        });
    };
    //Start of Pagination
    OrderHistoriesComponent.prototype.pagination = function (c, m) {
        var current = c, last = m, delta = 2, left = current - delta, right = current + delta + 1, range = [], rangeWithDots = [], l;
        for (var i = 1; i <= last; i++) {
            if (i == 1 || i == last || i >= left && i < right) {
                range.push(i);
            }
        }
        for (var _i = 0, range_1 = range; _i < range_1.length; _i++) {
            var i = range_1[_i];
            if (l) {
                if (i - l === 2) {
                    rangeWithDots.push(l + 1);
                }
                else if (i - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(i);
            l = i;
        }
        return rangeWithDots;
    };
    OrderHistoriesComponent.prototype.buildPagesNumbers = function (totalPage) {
        this.pageNumbering = [];
        this.total_page = totalPage;
        this.pageNumbering = this.pagination(this.currentPage, totalPage);
    };
    //End of Pagination
    //Search by text function
    OrderHistoriesComponent.prototype.searchText = function ($event) {
        return __awaiter(this, void 0, void 0, function () {
            var result, params, order_id, product_name, buyer_name, no_resi;
            return __generator(this, function (_a) {
                result = $event.target.value;
                console.log("Search", result);
                if (result.length > 2) {
                    params = void 0;
                    if (this.filter == "order_id") {
                        order_id = {
                            order_id: result,
                            get_detail: true
                        };
                        Object.assign(this.form_input, order_id);
                    }
                    else if (this.filter == "product_name") {
                        product_name = {
                            product_name: result,
                            get_detail: true
                        };
                        Object.assign(this.form_input, product_name);
                    }
                    else if (this.filter == "buyer_name") {
                        buyer_name = {
                            buyer_name: result,
                            get_detail: true
                        };
                        Object.assign(this.form_input, buyer_name);
                    }
                    else if (this.filter == "no_resi") {
                        no_resi = {
                            no_resi: result,
                            get_detail: true
                        };
                        Object.assign(this.form_input, no_resi);
                    }
                    this.valuechange({}, false, false);
                }
                else if (!result) {
                    this.form_input = {};
                    this.valuechange({}, false, false);
                }
                return [2 /*return*/];
            });
        });
    };
    //Get the detail Function
    OrderHistoriesComponent.prototype.getDetail = function (idDetail) {
        console.log(idDetail);
        this.router.navigate(['merchant-portal/order-detail/edit'], {
            queryParams: {
                id: idDetail
            }
        });
    };
    //When value change
    OrderHistoriesComponent.prototype.valuechange = function (event, input_name, download) {
        var _this_1 = this;
        if (this.onSearchActive == false) {
            this.onSearchActive = true;
            var myVar = setTimeout(function () { return __awaiter(_this_1, void 0, void 0, function () {
                var clearFormInput, searchDate, order_date_from_to, order_date_from, filter, result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!this.form_input) return [3 /*break*/, 2];
                            clearFormInput = this.form_input;
                            searchDate = {};
                            if (clearFormInput.order_date) {
                                if (clearFormInput.order_date.to) {
                                    order_date_from_to = {
                                        order_date_from: clearFormInput.order_date.from,
                                        order_date_to: clearFormInput.order_date.to
                                    };
                                    Object.assign(clearFormInput, order_date_from_to);
                                }
                                else {
                                    order_date_from = {
                                        order_date_from: clearFormInput.order_date.from,
                                    };
                                    Object.assign(clearFormInput, order_date_from);
                                }
                                delete clearFormInput.order_date;
                            }
                            this.showLoading = true;
                            console.log("clearFormInput", clearFormInput);
                            filter = {
                                search: clearFormInput,
                                order_by: this.orderByResult,
                                limit_per_page: this.pageLimits,
                                current_page: this.currentPage,
                                download: false
                            };
                            return [4 /*yield*/, this.orderhistoryService.getOrderHistoryMerchantFilter(filter)];
                        case 1:
                            result = _a.sent();
                            this.allData = result.result.values;
                            this.buildPagesNumbers(result.result.total_page);
                            return [3 /*break*/, 3];
                        case 2:
                            this.firstLoad();
                            _a.label = 3;
                        case 3:
                            this.onSearchActive = false;
                            this.showLoading = false;
                            return [2 /*return*/];
                    }
                });
            }); }, 2000);
        }
    };
    //Start of DatePicker Function
    OrderHistoriesComponent.prototype.datePickerOnDateSelection = function (date, formName) {
        var _this = this.make_This(formName);
        if (!_this.fromDate && !_this.toDate) {
            _this.fromDate = date;
        }
        else if (_this.fromDate && !_this.toDate && this.datePickerDateAfter(date, _this.fromDate)) {
            _this.toDate = date;
        }
        else {
            _this.toDate = null;
            _this.fromDate = date;
        }
        _this.from = (_this.fromDate != undefined && _this.fromDate != null) ? this.convertToDateString(_this.fromDate) : '';
        _this.to = (_this.toDate != undefined && _this.toDate != null) ? this.convertToDateString(_this.toDate) : '';
    };
    OrderHistoriesComponent.prototype.typeof = function (object) {
        return typeof object;
    };
    OrderHistoriesComponent.prototype.make_This = function (formName) {
        if (typeof formName == 'object') {
            return formName;
        }
        if (this.form_input[formName] == undefined || this.form_input[formName] == '') {
            this.form_input[formName] = {};
        }
        return this.form_input[formName];
    };
    OrderHistoriesComponent.prototype.dateToggler = function (formName) {
        this.toggler[formName] = (typeof this.toggler == undefined) ? true : !this.toggler[formName];
        if (this.toggler[formName] == false && this.form_input[formName] !== '') {
            this.valuechange({}, false, false);
        }
    };
    OrderHistoriesComponent.prototype.dateClear = function (event, dataInput, deleteInput) {
        this.form_input[dataInput] = '';
        this.valuechange(event, deleteInput);
    };
    OrderHistoriesComponent.prototype.datePickerOnDateIsInside = function (date, formName) {
        var _this = this.make_This(formName);
        return this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.toDate);
    };
    OrderHistoriesComponent.prototype.datePickerOnDateIsRange = function (date, formName) {
        var _this = this.make_This(formName);
        return this.datePickerDateEquals(date, _this.fromDate) || this.datePickerDateEquals(date, _this.toDate) || this.datePickerOnDateIsInside(date, _this) || this.datePickerOnDateIsHovered(date, _this);
    };
    OrderHistoriesComponent.prototype.datePickerOnDateIsHovered = function (date, formName) {
        var _this = this.make_This(formName);
        return _this.fromDate && !_this.toDate && _this.hoveredDate && this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.hoveredDate);
    };
    OrderHistoriesComponent.prototype.datePickerDateAfter = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day);
        if (iToDate.getTime() < iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    OrderHistoriesComponent.prototype.convertToDateString = function (datePrev) {
        console.log(datePrev);
        return datePrev.year.toString().padStart(2, "0") + '-' + datePrev.month.toString().padStart(2, "0") + '-' + datePrev.day.toString().padStart(2, "0");
    };
    OrderHistoriesComponent.prototype.datePickerDateEquals = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day);
        if (iToDate.getTime() == iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    OrderHistoriesComponent.prototype.datePickerDateBefore = function (datePrev, dateAfter) {
        if (datePrev == undefined || dateAfter == undefined)
            return false;
        var iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day);
        var iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day);
        if (iToDate.getTime() > iFromDate.getTime()) {
            return true;
        }
        return false;
    };
    //End of DatePicker Function
    //Start of Checkbox Function
    OrderHistoriesComponent.prototype.onChecked = function () {
        var _this_1 = this;
        setTimeout(function () {
            var getData = [];
            _this_1.checkBox.forEach(function (el, i) {
                var data = _this_1.allData[i];
                console.log(data);
                if (!_this_1.activeCheckbox) {
                    getData.push(data.booking_id);
                }
                else {
                    var index = getData.indexOf(data.booking_id);
                    if (index !== -1)
                        getData.splice(index, 1);
                }
            });
            _this_1.getDataCheck = getData;
            console.log(getData);
        }, 100);
    };
    OrderHistoriesComponent.prototype.checkedAll = function () {
        var _this_1 = this;
        setTimeout(function () {
            var getData = [];
            _this_1.allData.forEach(function (e, i) {
                console.log("result", e);
                if (_this_1.activeCheckbox) {
                    _this_1.checkBox[i] = _this_1.activeCheckbox;
                    getData.push(e.order_id);
                    console.log("result", e.order_id);
                }
                else {
                    _this_1.checkBox = [];
                }
            });
            _this_1.getDataCheck = getData;
            console.log(getData);
        }, 100);
    };
    OrderHistoriesComponent.prototype.printLabel = function () {
        console.log("test");
        this.router.navigate(['merchant-portal/order-histories/shipping-label'], { queryParams: { data: this.getDataCheck } });
        // window.print();
    };
    OrderHistoriesComponent.prototype.clickToDownloadReport = function () {
        //reportTable ID
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById('reportTable');
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        // Specify file name
        var filename = 'excel_data.xls';
        var downloadLink = document.createElement("a");
        document.body.appendChild(downloadLink);
        if (navigator.msSaveOrOpenBlob) {
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob(blob, filename);
        }
        else {
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
            // Setting the file name
            downloadLink.download = filename;
            //triggering the function
            downloadLink.click();
        }
    };
    //see more detail function
    OrderHistoriesComponent.prototype.setSeeMore = function (index) {
        this.allData[index].product_status = true;
        console.log(this.allData);
    };
    OrderHistoriesComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] }
    ]; };
    OrderHistoriesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-histories',
            template: __webpack_require__(/*! raw-loader!./order-histories.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/order-histories/order-histories.component.html"),
            styles: [__webpack_require__(/*! ./order-histories.component.scss */ "./src/app/layout/merchant-portal/order-histories/order-histories.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"]])
    ], OrderHistoriesComponent);
    return OrderHistoriesComponent;
}());



/***/ })

}]);
//# sourceMappingURL=default~merchant-portal-merchant-portal-module~merchant-portal-order-histories-order-histories-compo~0cfe36a4.js.map