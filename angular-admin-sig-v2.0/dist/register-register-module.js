(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["register-register-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/register/open-merchant/open-merchant.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/register/open-merchant/open-merchant.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<input #inputFile2 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'image_owner_url')\"\r\naccept=\"image/x-png,image/gif,image/jpeg\">\r\n<input #inputFile1 type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event, 'background_image')\"\r\naccept=\"image/x-png,image/gif,image/jpeg\">\r\n<div class=\"header\">\r\n<h3>LOCARD Merchant Portal</h3>\r\n</div>\r\n<div *ngIf=\"!success\">\r\n<h3 class=\"title-diff\">Open Merchant</h3>\r\n\r\n<div class=\"container\">\r\n    <div class=\"container-header\">\r\n        Business Profile\r\n    </div>\r\n    <div class=\"col-md-12\">\r\n        <div class=\"form-group\">\r\n            <label>Store Name <span class=\"estar\">*</span></label>\r\n            <input class=\"form-control {{formEditable.merchant_name}}\" (focus)=\"editableForm('merchant_name',true)\"\r\n                (blur)=\"editableForm('merchant_name', false)\" type=\"text\" name=\"store name\"\r\n                (input)=\"nameInput($event)\" [(ngModel)]=\"form.merchant_name\" />\r\n            <div *ngIf=\"!formEditable.merchant_name\" class=\"iconic\" (click)=\"editableForm('merchant_name', true)\">\r\n\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label>Store Info (Description)<span class=\"estar\">*</span></label>\r\n            <!-- <textarea class=\"form-control {{formEditable.description}}\" id=\"description\" name=\"description\" \r\n                [(ngModel)]=\"form.description\" (focus)=\"editableForm('description', true)\"\r\n                (blur)=\"editableForm('description', false)\" style=\"min-height: 200px;color:black\"></textarea> -->\r\n                <ckeditor [editor]=\"Editor\" [config]=\"config\" class=\"{{formEditable.description}}\"\r\n                (focus)=\"editableForm('description', true)\" (blur)=\"editableForm('description', false)\"\r\n                type=\"text\" name=\"description\" [(ngModel)]=\"form.description\"></ckeditor>\r\n        </div> \r\n        <div class=\"form-group\">\r\n            <label>Store Address<span class=\"estar\">*</span></label>\r\n            <input class=\"form-control {{formEditable.maddress}}\" (focus)=\"editableForm('maddress',true)\"\r\n                (blur)=\"editableForm('maddress',false)\" type=\"text\" name=\"maddress\" [(ngModel)]=\"form.maddress\" />\r\n            <div *ngIf=\"!formEditable.maddress\" class=\"iconic\" (click)=\"editableForm('maddress', true)\">\r\n\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"row\">\r\n           \r\n            \r\n                <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                    <label>Province <span class=\"estar\">*</span></label>\r\n                    <input type=\"text\" class=\"form-control {{formEditable.mprovince}}\"\r\n                        (focus)=\"editableForm('mprovince', true)\" list=\"dynmicProvinceList\" \r\n                        (blur)=\"editableForm('mprovince', false)\"\r\n                        name=\"state\" [(ngModel)]=\"form.mprovince\" (keyup)=\"getProvinceList($event)\" autocomplete=\"off\"\r\n                        />\r\n                        <datalist id=\"dynmicProvinceList\" name=\"province_code\" ngDefaultControl>\r\n                            <option *ngFor=\"let item of provinceList\" [ngValue]=\"item.province\">\r\n                                {{item.province}}\r\n                            </option>\r\n                        </datalist>\r\n                </div>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <label>City(Kota)/District (Kabupaten) <span class=\"estar\">*</span></label>\r\n                        <input type=\"text\" class=\"form-control {{formEditable.mcity}}\"\r\n                            (focus)=\"editableForm('mcity', true)\"  list=\"dynmicCityList\"  (blur)=\"editableForm('mcity', false)\" name=\"city\"\r\n                            [(ngModel)]=\"form.mcity\" (keyup)=\"getCityList($event)\" (change)=\"onCityChange()\" autocomplete=\"off\"  />\r\n                            <datalist id=\"dynmicCityList\" name=\"city_code\" ngDefaultControl>\r\n                                <option *ngFor=\"let item of cityList\" [ngValue]=\"item.city\">\r\n                                    {{item.city}}\r\n                                </option>\r\n                            </datalist>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <label> Sub District (Kecamatan) <span class=\"estar\">*</span> </label>\r\n                        <input type=\"text\" class=\"form-control {{formEditable.msubdistrict}}\"\r\n                            (focus)=\"editableForm('msubdistrict', true)\" list=\"dynmicSubDistrictList\"  (blur)=\"editableForm('msubdistrict', false)\"\r\n                            name=\"country\" [(ngModel)]=\"form.msubdistrict\" (keyup)=\"getSubDistrictList($event)\" (change)=\"onSubDistrictChange()\" autocomplete=\"off\"/>\r\n                            <datalist id=\"dynmicSubDistrictList\" name=\"subdistrict_code\" ngDefaultControl>\r\n                                <option *ngFor=\"let item of subDistrictList\" [ngValue]=\"item.subdistrict\">\r\n                                    {{item.subdistrict}}\r\n                                </option>\r\n                            </datalist>\r\n                    </div>\r\n                    </div>\r\n        <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                    <label> Kelurahan/Desa <span class=\"estar\">*</span> </label>\r\n                    <input type=\"text\" class=\"form-control {{formEditable.mdistrict}}\"\r\n                        (focus)=\"editableForm('mvillage', true)\" list=\"dynmicVillageList\"   (blur)=\"editableForm('mvillage', false)\"\r\n                        name=\"country\" [(ngModel)]=\"form.mvillage\" (keyup)=\"getVillageList($event)\" (change)=\"onVillageChange()\" autocomplete=\"off\"/>\r\n                        <datalist id=\"dynmicVillageList\" name=\"mvillage_code\" ngDefaultControl>\r\n                            <option *ngFor=\"let item of villageList\" [ngValue]=\"item.village\">\r\n                                {{item.village}}\r\n                            </option>\r\n                        </datalist>\r\n                </div>\r\n        </div>\r\n\r\n        <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                    <label> Postal Code <span class=\"estar\">*</span></label>\r\n                    <input type=\"number\" [attr.maxLength]=\"5\" class=\"form-control {{formEditable.mpostal_code}}\"\r\n                        (focus)=\"editableForm('mpostal_code', true)\" (blur)=\"editableForm('mpostal_code', false)\"\r\n                        name=\"postal code\" [(ngModel)]=\"form.mpostal_code\" />\r\n                    <div *ngIf=\"!formEditable.mpostal_code\" class=\"iconic\"\r\n                        (click)=\"editableForm('mpostal_code', true)\">\r\n\r\n                    </div>\r\n                </div>\r\n        </div>\r\n\r\n        <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                    <label>Another Contact Person<span class=\"estar\">*</span></label>\r\n                    <input type=\"number\" class=\"form-control {{formEditable.contact_person}}\"\r\n                        (focus)=\"editableForm('contact_person', true)\"\r\n                        (blur)=\"editableForm('contact_person', false)\" name=\"contact_person\"\r\n                        [(ngModel)]=\"form.contact_person\" />\r\n                    <div *ngIf=\"!formEditable.contact_person\" class=\"iconic\"\r\n                        (click)=\"editableForm('contact_person', true)\">\r\n\r\n                    </div>\r\n                </div>\r\n        </div>\r\n\r\n        </div>\r\n        \r\n            <div class=\"form-group\">\r\n                <label>Store Slogan</label>\r\n                <textarea class=\"form-control {{formEditable.slogan}}\" (focus)=\"editableForm('slogan', true)\"\r\n                    (blur)=\"editableForm('slogan', false)\" type=\"text\" name=\"slogan\"\r\n                    [(ngModel)]=\"form.slogan\"></textarea>\r\n\r\n            </div>\r\n\r\n        \r\n            <div class=\"mr-5\" >\r\n                <div class=\"form-group courier-service\">\r\n                    <label>Courier Service <span class=\"estar\">*</span></label>\r\n                    <div>\r\n                    <span class=\"d-inline-block\" tabindex=\"0\" data-toggle=\"tooltip\" title=\"Lakukan Pengisian Provinsi sampai Desa\">\r\n                    <button class=\"common-button\" [disabled]=\"!complete\" [class.darkwhite]= \"!complete\"\r\n                    [class.blue-fill]=\"complete\" (click)=\"seeAvailableCourier()\" >See Available Courier</button>\r\n                </span>\r\n                    </div>\r\n                    \r\n                    <div class=\"form-group uploaded\" *ngIf=\"showLoading\">\r\n                        <div class=\"loading\">\r\n                            <div class=\"sk-fading-circle\">\r\n                              <div class=\"sk-circle1 sk-circle\"></div>\r\n                              <div class=\"sk-circle2 sk-circle\"></div>\r\n                              <div class=\"sk-circle3 sk-circle\"></div>\r\n                              <div class=\"sk-circle4 sk-circle\"></div>\r\n                              <div class=\"sk-circle5 sk-circle\"></div>\r\n                              <div class=\"sk-circle6 sk-circle\"></div>\r\n                              <div class=\"sk-circle7 sk-circle\"></div>\r\n                              <div class=\"sk-circle8 sk-circle\"></div>\r\n                              <div class=\"sk-circle9 sk-circle\"></div>\r\n                              <div class=\"sk-circle10 sk-circle\"></div>\r\n                              <div class=\"sk-circle11 sk-circle\"></div>\r\n                              <div class=\"sk-circle12 sk-circle\"></div>\r\n                            </div>\r\n                          </div>\r\n                    </div>\r\n                    <div *ngIf=\"formcourier\">\r\n                        <div *ngFor=\"let cr of courierServices; let i = index\">\r\n                            <mat-checkbox [(ngModel)]=\"cr.value\">\r\n                                {{cr.courier}} </mat-checkbox>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        \r\n        <div class=\"image-upload-group\">\r\n            <div class=\"img-upload logo\">\r\n                <div class=\"card\">\r\n                    <div class=\"card-title\">\r\n                        <h2>Store Logo<span class=\"estar\">*</span></h2>\r\n                    </div>\r\n                    <div class=\"card-content\" *ngIf=\"!form.image_owner_url && !showLoading1\">\r\n                        <div class=\"image\">\r\n                            <div class=\"card-content custom\">\r\n                                <div>\r\n                                    <img *ngIf=\"form.image_owner_url\" class=\"resize\"\r\n                                        src=\"{{form.image_owner_url}}\" />\r\n                                    <img *ngIf=\"!form.image_owner_url\" class=\"set-opacity\"\r\n                                        src=\"/assets/images/cloud.png\" height=\"200\" width=\"200\"> <br />\r\n                                </div>\r\n                                <div class=\"browse-files\">\r\n                                    <div class=\"pb-framer\">\r\n                                        <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                            [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                        </div>\r\n                                        <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                            {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                        <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                            {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n                                    </div>\r\n\r\n                                    <button class=\"common-button blue-fill fileupload\"\r\n                                        (click)=\"inputFile2.click()\">Browse File</button>\r\n                                    <br>\r\n                                    <br>\r\n                                 \r\n                                </div>\r\n\r\n                            </div>\r\n\r\n                        </div>\r\n                       \r\n                    </div>\r\n                    <div class=\"form-group uploaded\" *ngIf=\"showLoading1\">\r\n                        <div class=\"loading\">\r\n                            <div class=\"sk-fading-circle\">\r\n                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n           \r\n                    <div class=\"form-group uploaded\" *ngIf=\"form.image_owner_url && !showLoading1\">\r\n                        <div class=\"uploaded-image\">\r\n                            <div class=\"img\">\r\n          \r\n                                <img class=\"resize\" src=\"{{form.image_owner_url}}\">\r\n                            </div>\r\n                       \r\n                        </div>\r\n                        <a class=\"common-button blue-fill text-fileupload\" (click)=\"inputFile2.click()\">Re-Upload Image</a>\r\n                    </div>\r\n                    <div class=\"notif error-message\" *ngIf=\"errorLabel\" [class.error-margin]=\"!showLoading1\">\r\n                        {{errorLabel}}\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"img-upload bg\">\r\n                <div class=\"card\">\r\n                    <div class=\"card-title\">\r\n                        <h2>Background Store Logo<span class=\"estar\">*</span></h2>\r\n                    </div>\r\n                    <div class=\"card-content\" *ngIf=\"!form.background_image && !showLoading2\">\r\n                        <div class=\"image\">\r\n                            <div class=\" custom\">\r\n                                <div>\r\n                                    <img *ngIf=\"form.background_image\" class=\"resize\"\r\n                                        src=\"{{form.background_image}}\" />\r\n                                    <img *ngIf=\"!form.background_image\" class=\"set-opacity\"\r\n                                        src=\"/assets/images/cloud.png\"> <br />\r\n                                </div>\r\n                                <div class=\"browse-files\">\r\n                                    <div class=\"pb-framer\">\r\n                                        <div class=\"progressbar {{cancel?'terminated':''}}\"\r\n                                            [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                                        </div>\r\n                                        <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                                            {{cancel?'terminated':progressBar+'%'}} </span>\r\n                                        <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                                            {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n                                    </div>\r\n\r\n                                    <button class=\"common-button blue-fill fileupload\"\r\n                                        (click)=\"inputFile1.click()\">Browse File</button>\r\n\r\n                                    <br>\r\n                                    <br>\r\n                                   \r\n                                </div>\r\n\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                    \r\n                    <div class=\"form-group uploaded\" *ngIf=\"showLoading2\">\r\n                        <div class=\"loading\">\r\n                            <div class=\"sk-fading-circle\">\r\n                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n               \r\n                    <div class=\"form-group uploaded\" *ngIf=\"form.background_image && !showLoading2\">\r\n                        <div class=\"uploaded-image\">\r\n                            <div class=\"img\">\r\n                               \r\n                                <img class=\"resize\" src=\"{{form.background_image}}\">\r\n                            </div>\r\n                            \r\n                        </div>\r\n                        <a class=\"common-button blue-fill text-fileupload\" (click)=\"inputFile1.click()\">Re-Upload Image</a>\r\n                    </div>\r\n                    <div class=\"notif error-message\" *ngIf=\"errorLabel2\">\r\n                        {{errorLabel2}}\r\n                    </div>\r\n                </div>\r\n             \r\n\r\n            </div>\r\n        </div>\r\n        <div>\r\n            <div class=\"notif error-message\">{{errorMessage}}</div>\r\n\r\n\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n<div>\r\n</div>\r\n\r\n\r\n<div class=\"save-container\">\r\n    <button class=\"common-button save\" (click)=\"saveProfile()\">Save\r\n        Profile</button>\r\n</div>\r\n</div>\r\n\r\n<div *ngIf=\"success\" class=\"success\">\r\n<div class=\"header-success\">Congratulations!</div>\r\n<div class=\"sub-header\">\r\n    Your Store has been successfully opened\r\n</div>\r\n<img class=\"img-success\" src=\"/assets/images/successmerchant.png\">\r\n<br>\r\n<label> Please relogin to enter your merchant dashboard</label>\r\n<div class=\"success-container\">\r\n    <button class=\"common-button add\" (click)=\"goToAdd()\">Login</button>\r\n</div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/register/register.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/register/register.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\r\n\r\n<body>\r\n    <div class=\"pg-container\">\r\n        <div class=\"container-form\">\r\n            <img id=\"page-regis-img\" src=\"/assets/images/page-regis3.png\">\r\n            <img id=\"form-regis-img\" src=\"/assets/images/form-regis2.png\" style=\"visibility: hidden;\">\r\n\r\n            <!-- <img id=\"form-regis-img\"> -->\r\n            <div class=\"regis-body\">\r\n                <div class=\"regis-title\">\r\n                    <img id=\"locard_icon_img\" src=\"/assets/images/locard_icon_circle.png\" alt=\"Locard Icon\">\r\n                    <p style=\"font-weight: 900\"> LOCARD </p>\r\n\r\n                </div>\r\n\r\n                <div class=\"register-page-container\">\r\n                    <h1 class=\"form-title\"><strong>Register</strong></h1>\r\n                    <span class=\"form-desc\"><strong>Join us to our merchant club and feel more lucky with awareness,\r\n                            retention, and acquisition </strong></span>\r\n                    <div class=\"col-md-4 col-sm-12 register-form-body\">\r\n\r\n\r\n                        <!-- <form #form=\"ngForm\" class=\"register-form\" ngNativeValidate (ngSubmit)=\"register(form)\"> -->\r\n                        <div [formGroup]=\"registerForm\" class=\"register-form\" ngNativeValidate>\r\n                            <mat-list>\r\n                                <mat-list-item>\r\n                                    <mat-icon mat-list-icon>perm_identity</mat-icon>\r\n                                    <mat-form-field mat-line>\r\n                                        <input matInput placeholder=\"Name\" formControlName=\"full_name\"\r\n                                            [(ngModel)]=\"form.full_name\" required>\r\n                                        <mat-error *ngIf=\"registerForm.controls.full_name.hasError('required')\">You must\r\n                                            enter a value</mat-error>\r\n                                        <mat-error *ngIf=\"registerForm.controls.full_name.hasError('maxlength')\">You\r\n                                            reach the maximum input value</mat-error>\r\n                                    </mat-form-field>\r\n                                </mat-list-item>\r\n                                <mat-list-item>\r\n                                    <mat-icon mat-list-icon> mail_outline</mat-icon>\r\n                                    <mat-form-field mat-line>\r\n                                        <input matInput placeholder=\"Email\" formControlName=\"email\" required\r\n                                            [(ngModel)]=\"form.email\">\r\n                                        <mat-error *ngIf=\"registerForm.controls.email.hasError('required')\">You must\r\n                                            enter a value</mat-error>\r\n                                        <mat-error *ngIf=\"registerForm.controls.email.hasError('email')\">Not a valid\r\n                                            email format</mat-error>\r\n                                        <!-- <mat-error *ngIf=\"email.invalid\">{{getErrorMessage()}}</mat-error> -->\r\n                                    </mat-form-field>\r\n                                </mat-list-item>\r\n                                <mat-list-item>\r\n                                    <mat-icon mat-list-icon>lock_open</mat-icon>\r\n                                    <mat-form-field mat-line>\r\n                                        <input matInput placeholder=\"Password\" [type]=\"hide ? 'password' : 'text'\"\r\n                                            [(ngModel)]=\"form.password\" formControlName=\"password\">\r\n                                        <button mat-icon-button matSuffix (click)=\"hide = !hide\"\r\n                                            [attr.aria-label]=\"'Hide password'\" [attr.aria-pressed]=\"hide\">\r\n                                            <mat-icon>{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"registerForm.controls.password.hasError('required')\">You must\r\n                                            enter a value</mat-error>\r\n                                        <mat-error *ngIf=\"registerForm.controls.password.hasError('minlength')\">Minimum\r\n                                            8 length of character</mat-error>\r\n                                    </mat-form-field>\r\n                                </mat-list-item>\r\n\r\n                                <mat-list-item>\r\n                                    <mat-icon mat-list-icon>phone_iphone</mat-icon>\r\n                                    <mat-form-field mat-line>\r\n                                        <input matInput placeholder=\"Phone Number\" [(ngModel)]=\"form.cell_phone\"\r\n                                            type=\"number\" formControlName=\"cell_phone\">\r\n                                        <mat-error *ngIf=\"registerForm.controls.cell_phone.hasError('required')\">You\r\n                                            must enter a value</mat-error>\r\n                                    </mat-form-field>\r\n                                </mat-list-item>\r\n                                <mat-list-item>\r\n                                    <mat-icon mat-list-icon style=\"visibility: hidden;\">phone_iphone</mat-icon>\r\n                                    <mat-form-field mat-line>\r\n                                        <mat-select placeholder=\"Select Gender\" [(ngModel)]=\"form.gender\"\r\n                                            formControlName=\"gender\">\r\n                                            <mat-option value=\"male\">Male</mat-option>\r\n                                            <mat-option value=\"female\">Female</mat-option>\r\n                                        </mat-select>\r\n                                    </mat-form-field>\r\n                                </mat-list-item>\r\n\r\n                                <label class=\"date-birth-label\">Date of Birth</label>\r\n                                <div class=\"date-birth-responsive\" style=\"display: flex;\">\r\n\r\n                                    <mat-list-item>\r\n                                        <mat-icon mat-list-icon style=\"visibility: hidden;\">phone_iphone</mat-icon>\r\n                                        <mat-form-field id=\"mat-form-field\" appearance=\"outline\" mat-line>\r\n                                 \r\n                                            <input matInput [matDatepicker]=\"picker\" [(ngModel)]=\"form.dob\"\r\n                                                formControlName=\"dob\">\r\n                                            <mat-error *ngIf=\"registerForm.controls.dob.hasError('required')\">You must\r\n                                                enter a value</mat-error>\r\n                                            <mat-datepicker-toggle matSuffix [for]=\"picker\">\r\n                                                <mat-icon matDatepickerToggleIcon>calendar_today</mat-icon>\r\n                                            </mat-datepicker-toggle>\r\n                                            <mat-datepicker #picker></mat-datepicker>\r\n                                        </mat-form-field>\r\n                                    </mat-list-item>\r\n\r\n\r\n                                </div>\r\n                                <div class=\"notif error-message\" *ngIf=\"errorMessage\">\r\n                                    *{{errorMessage}}\r\n                                </div>\r\n                                <br>\r\n                                <mat-checkbox [(ngModel)]=\"checked\" formControlName=\"checked\"\r\n                                    style=\"font-size: 14px;text-align: center;margin-left: 66px;color:grey\">\r\n                                </mat-checkbox><span> I Agree to the LOCARD <span style=\"color:#0871b2;cursor: pointer;\"\r\n                                        (click)=\"openTnc();\">Terms & Conditions</span></span>\r\n\r\n                                        <div class=\"loading\" *ngIf='showLoading'>\r\n                                            <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                          </div>\r\n\r\n                                <br>\r\n                                <div class=\"button-container\">\r\n                                    <button type=\"submit\" class=\"common-button blue-fill\" (click)=\"register()\">\r\n                                        Register</button>\r\n                                    <div class=\"divider\"></div>\r\n                                    <button class=\"common-button\" style=\"background-color: white;\"\r\n                                        (click)=\"loginClicked()\"><strong>Login</strong>\r\n                                    </button>\r\n                                </div>\r\n                            </mat-list>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"text-middle-page\">\r\n            <p class=\"join-custom-title\">JOIN TO OUR <strong>MERCHANT CLUB</strong></p>\r\n        </div>\r\n        <div class=\"container\">\r\n            <div class=\"row reverse-center\">\r\n                <div class=\"col-lg-5\">\r\n                    <img class=\"img-merchant\" src=\"/assets/images/awareness.png\">\r\n                </div>\r\n                <div class=\"col-lg-7 align-self-center\">\r\n                    <h3 class=\"bottom-title\"><span style=\"color:#0871b2;\">#1.</span> <strong> AWARENESS</strong></h3>\r\n                    <p style=\"font-size: 18px;\">Melalui media promosi, bisnis anda akan selalu terekspos</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-lg-5\">\r\n                    <img class=\"img-merchant\" src=\"/assets/images/retention.png\">\r\n                </div>\r\n                <div class=\"col-lg-7 align-self-center\">\r\n                    <h3 class=\"bottom-title\"><span style=\"color:#0871b2;\">#2.</span> <strong> RETENTION</strong></h3>\r\n                    <p style=\"font-size: 18px;\">Kami membantu meningkatkan trafic kunjungan dengan hubungan anda dengan\r\n                        jaringan member LOCARD</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"row reverse-center\">\r\n                <div class=\"col-lg-5\">\r\n                    <img class=\"img-merchant\" src=\"/assets/images/aquisition.png\">\r\n                </div>\r\n                <div class=\"col-lg-7 align-self-center\">\r\n                    <h3 class=\"bottom-title\"><span style=\"color:#0871b2;\">#3.</span> <strong> ACQUISITION </strong></h3>\r\n                    <p style=\"font-size: 18px;\">Kami membantu bisnis anda untuk mendapatkan\r\n                        customer baru dan menjalin hubungan jangka panjang dengan customer anda</p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"bottom-container\">\r\n            <div class=\"bottom-page\">\r\n                <div class=\"text\">\r\n                    EASY WAY TO BECOME A MERCHANT LOCARD\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"middle-regis-page\">\r\n                <div class=\"row box-middle-page\">\r\n                    <div class=\"col-md-4\">\r\n                        <div class=\"box\">\r\n                            <div class=\"row1\">\r\n                                <img class=\"img-box\" src=\"/assets/images/Group_5855.png\">\r\n                            </div>\r\n                            <div class=\"row2\">\r\n                                <b> Daftar Online</b>\r\n                                <p>\r\n                                    Join Club Merchant LOCARD sangat mudah,\r\n                                    hanya dengan mengisi form Online pada website\r\n                                </p>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <div class=\"box\">\r\n                            <div class=\"row1\">\r\n                                <img class=\"img-box\" src=\"/assets/images/Group_5856.png\">\r\n                            </div>\r\n                            <div class=\"row2\">\r\n                                <b> Negosiasi dan Deal </b>\r\n                                <br>\r\n                                <p>\r\n                                    Setelah mendaftar, team kami akan segera menghubungi anda.\r\n                                </p>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <div class=\"box\">\r\n                            <div class=\"row1\">\r\n                                <img class=\"img-box\" src=\"/assets/images/Group_5857.png\">\r\n                            </div>\r\n                            <div class=\"row2\">\r\n                                <b> Mulai Jalankan Transaksi </b>\r\n                                <p>\r\n                                    Ajak pelanggan setia anda untuk Join Member LoCard untuk mendapatkan\r\n                                    Locky Poin setiap bertransaksi.\r\n                                </p>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n         <div id=\"myModal\" class=\"modal {{showTnc}}\">\r\n             <!-- Modal content -->\r\n             <div class=\"modal-content\">\r\n                 <div class=\"modal-header\">\r\n                     <span class=\"close\" (click)=\"closeTnc()\">&times;</span>\r\n                 </div>\r\n                 <div class=\"modal-body\">\r\n                     <h4 style=\"color:#0871b2\"> Terms And Conditions </h4>\r\n                     <br />\r\n\r\n                 </div>\r\n                 <br />\r\n                 <div class=\"modal-body\">\r\n                     <!-- <textarea placeholder=\"Write your reason here...\" rows=\"4\" cols=\"50\"></textarea> -->\r\n                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque orci laoreet ipsum\r\n                         accumsan, non tincidunt felis aliquam. In venenatis mi sodales purus efficitur vehicula. Nam\r\n                         varius diam urna, a porttitor mi blandit ut. Suspendisse luctus libero sed purus scelerisque,\r\n                         et suscipit nisl varius. Suspendisse iaculis rhoncus quam vel blandit. Nam pretium vel quam sit\r\n                         amet blandit. Maecenas et nisi sodales, cursus diam id, vehicula nulla. Aliquam quam sapien,\r\n                         efficitur at imperdiet ut, fermentum laoreet ligula. Sed finibus condimentum augue, id placerat\r\n                         urna placerat id. Sed malesuada in neque in rutrum. Donec sollicitudin egestas arcu sit amet\r\n                         egestas. Maecenas euismod, neque quis tempus lacinia, massa augue convallis quam, vel facilisis\r\n                         tortor odio et arcu. Aliquam ut cursus elit, et aliquet eros.\r\n\r\n                         Quisque tortor nisi, elementum vel cursus eu, tempus tincidunt massa. Nulla nisl ipsum,\r\n                         suscipit eu sapien quis, rhoncus lobortis est. Sed vehicula quam odio. Curabitur dui enim,\r\n                         placerat a vestibulum vitae, tristique a eros. Vivamus ullamcorper sagittis turpis, eget\r\n                         eleifend nisi vulputate congue. Ut pulvinar lorem enim, a posuere mauris placerat quis. Sed nec\r\n                         dolor dui. Mauris sit amet auctor sapien. Donec sed cursus eros. Mauris nisl orci, accumsan ac\r\n                         feugiat ut, mollis eget turpis.</p>\r\n\r\n                 </div>\r\n                 <div class=\"modal-body\">\r\n                     <!-- <button class=\"btn btn-primary\" (click)=\"cancelOrders()\">Submit</button> -->\r\n                 </div>\r\n                 <div class=\"modal-footer\">\r\n\r\n                 </div>\r\n             </div>\r\n             <div class=\"buttons\" style=\"text-align: -webkit-center;margin-top:10px;\">\r\n             <button class=\"btn btn-primary\" style=\"margin-right: 10px;\"  >Accept</button>\r\n             <button class=\"btn btn-outline-primary\" (click)=\"closeTnc();\" >Decline</button>\r\n             </div>\r\n         </div>\r\n    </div>\r\n\r\n    <footer class=\"footer\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-3 first-footer-logo\">\r\n                <img src=\"/assets/images/locard_icon_circle.png\">\r\n            </div>\r\n            <div class=\"col-md-9 first-footer-content\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-3 content-1\">\r\n                        <h3>\r\n                            <strong>Contact Information</strong>\r\n                        </h3>\r\n                        <p>\r\n                            Lorem Ipsum<br>\r\n                            Lorem Ipsum<br>\r\n                            Lorem Ipsum<br>\r\n                            Lorem Ipsum Dolor Sit<br>\r\n                            Lorem Ipsum<br>\r\n                            Lorem Ipsum Dolor Sit<br>\r\n                            Lorem Ipsum<br>\r\n                        </p>\r\n                    </div>\r\n                    <div class=\"col-md-3 content-1\">\r\n                        <h3>\r\n                            <strong>Lorem</strong>\r\n                        </h3>\r\n                        <p>\r\n                            Lorem Ipsum<br>\r\n                            Lorem Ipsum<br>\r\n                            Lorem Ipsum<br>\r\n                            Lorem Ipsum Dolor Sit<br>\r\n                            Lorem Ipsum<br>\r\n                            Lorem Ipsum Dolor Sit<br>\r\n                            Lorem Ipsum<br>\r\n                        </p>\r\n                    </div>\r\n                    <div class=\"col-md-3 content-3\">\r\n                        <h3>\r\n                            <strong>Follow Us On</strong>\r\n                        </h3>\r\n                        <div class=\"social-media\">\r\n                            <img src=\"/assets/images/twitter.png\">\r\n                            <img src=\"/assets/images/facebook.png\">\r\n                            <img src=\"/assets/images/instagram.png\">\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-3 content-4\">\r\n                        <div class=\"application\">\r\n                            <div class=\"row up\">\r\n                                Get The Applications on\r\n                            </div>\r\n                            <div class=\"row bottom\">\r\n                                <a href=\"#googleplay\"><img src=\"/assets/images/googleplay.png\"></a>\r\n                                <a href=\"#appstore\"><img src=\"/assets/images/appstore.png\"\r\n                                        style=\"padding: 12px;margin-top: -15px;\"></a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"second-footer\">\r\n            <img src=\"/assets/images/locard_icon_circle.png\">\r\n            <p>Copyright © 2019 PT CLS System. All rights reserved </p>\r\n        </div>\r\n    </footer>\r\n\r\n      \r\n\r\n</body>\r\n"

/***/ }),

/***/ "./src/app/register/open-merchant/open-merchant.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/register/open-merchant/open-merchant.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  font-family: \"Poppins\", sans-serif;\n  transition: ease all 0.3s;\n}\n\n.header {\n  background-color: #3a4458;\n  width: 100%;\n  height: 80px;\n  display: flex;\n  text-align: left;\n  justify-content: left;\n  align-items: center;\n  color: white;\n}\n\n.header h3 {\n  margin-left: 8%;\n}\n\n.success {\n  text-align: center;\n}\n\n.success .img-success {\n  width: 25vw;\n  height: auto;\n  margin-bottom: 2%;\n  min-width: 250px;\n}\n\n.success .header-success {\n  margin-top: 1%;\n  font-weight: bold;\n  font-size: 35px;\n  width: 100%;\n}\n\n.success .sub-header {\n  margin-bottom: 2%;\n  color: grey;\n  font-weight: bold;\n  font-size: 25px;\n}\n\n.title-diff {\n  font-weight: bold;\n  font-size: 35px;\n  text-align: center;\n  margin-top: 7%;\n  margin-bottom: 2%;\n}\n\n.estar {\n  color: red;\n}\n\n.save-container {\n  margin: 4% 0%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.save-container .save {\n  width: 35%;\n  outline: none;\n  height: 60px;\n  color: white;\n  background-color: #ff7301;\n}\n\n.darkwhite {\n  background-color: #707070;\n  color: white;\n  font-size: 15px;\n}\n\n.success-container {\n  margin: 1% 0%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.success-container .add {\n  width: 25%;\n  outline: none;\n  height: 50px;\n  color: white;\n  border: none;\n  background-color: #ff7301;\n}\n\n.success-container .dashboard {\n  width: 25%;\n  outline: none;\n  border-color: #ff7301;\n  height: 50px;\n  color: #ff7301;\n  background-color: white;\n}\n\n.container {\n  padding: 3% 5% 3%;\n  width: 75%;\n  box-shadow: 0px 2px 5px 3px rgba(0, 0, 0, 0.1);\n}\n\n.container .container-header {\n  margin-bottom: 6%;\n  font-size: 25px;\n  text-align: center;\n  font-weight: 900;\n  color: grey;\n  margin-top: 3%;\n}\n\n.image-upload-group {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  overflow: hidden;\n  margin-bottom: 25px;\n}\n\n.image-upload-group .bg {\n  width: 65%;\n}\n\n.image-upload-group .logo {\n  width: 35%;\n}\n\n.image-upload-group .img-upload {\n  height: 400px;\n  position: relative;\n}\n\n.image-upload-group .img-upload > .card {\n  height: 300px;\n  border: none;\n  display: block;\n}\n\n.image-upload-group .img-upload > .card > .card-title h2 {\n  font-size: 24px;\n}\n\n.image-upload-group .img-upload > .card > .card-content {\n  height: 100%;\n  position: relative;\n  height: 250px;\n}\n\n.image-upload-group .img-upload > .card > .card-content .image {\n  display: block;\n  float: left;\n  height: 100%;\n  width: 100%;\n}\n\n.image-upload-group .img-upload > .card > .card-content .image .custom {\n  margin: 20px;\n  background-color: rgba(0, 0, 0, 0.03);\n  border: 2px solid #ebebeb;\n  border-radius: 10px;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n  padding: 40px;\n}\n\n.image-upload-group .img-upload > .card > .card-content .image .custom .set-opacity {\n  opacity: 0.1;\n  height: 60px;\n  width: auto;\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 250px;\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded .loading {\n  height: 100%;\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded .uploaded-image {\n  height: 100%;\n  padding: 20px;\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded .uploaded-image .img {\n  height: 100%;\n  width: auto;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded .uploaded-image .img > img {\n  height: 100%;\n  width: auto;\n  max-height: 300px;\n}\n\n.image-upload-group .img-upload > .card > .form-group.uploaded .uploaded-image .editor-tool {\n  height: 100%;\n}\n\n.image-upload-group .img-upload:last-child {\n  margin-left: 16px;\n}\n\n.form-body .or-group {\n  margin-left: 0px;\n  margin-right: 0px;\n}\n\n.form-body .or-group .col-md-6 {\n  padding-left: 0px;\n}\n\n.form-body .or-group .col-md-6:last-child {\n  padding-right: 0px;\n}\n\n.courier-service .mat-checkbox:not(:first-child) {\n  margin-left: 15px;\n}\n\n.btn.save {\n  margin: 15px 15px 0px 0px;\n}\n\n.form {\n  padding: 25px;\n  overflow: hidden;\n  position: relative;\n}\n\n.form .browse-files .fileupload {\n  margin-top: 25px;\n}\n\n.form .error-message {\n  float: left;\n  position: relative;\n}\n\n.form .error-margin {\n  margin-top: 40px;\n}\n\n.form .head-form {\n  overflow: hidden;\n  height: 11vw;\n}\n\n.form .head-form .photo-profile {\n  display: block;\n  width: 10vw;\n  height: 10vw;\n  float: left;\n}\n\n.form .head-form .detail {\n  display: flex;\n  flex-direction: column;\n  float: left;\n  justify-content: center;\n  align-items: flex-start;\n  font-size: 15px;\n  height: 65px;\n  line-height: 17px;\n  padding: 5px;\n  margin-top: 3vw;\n  margin-left: 2vw;\n}\n\n.form .head-form .detail .name {\n  font-size: 27px;\n  font-weight: bold;\n}\n\n.form .editor-group {\n  margin-top: 25px;\n}\n\n.form .form-group {\n  position: relative;\n}\n\n.form .form-group label {\n  text-transform: capitalize;\n}\n\n.form .form-group .form-control {\n  font-size: 12px;\n  background-color: transparent;\n  font-weight: bold;\n}\n\n.form .form-group .form-control.true {\n  background-color: white;\n  font-weight: normal;\n}\n\n.form .form-group .form-control ~ .iconic {\n  position: absolute;\n  right: 10px;\n  top: 37px;\n  -webkit-transform: scale(0.7);\n          transform: scale(0.7);\n}\n\n.form .form-group .description {\n  height: 110px;\n}\n\n.form .vl {\n  border-left: 1px solid #eaeaea;\n}\n\n.form img.resize {\n  max-width: 100%;\n  max-height: 100%;\n}\n\n.card-title {\n  margin: 20px 0px;\n  border-bottom: 1px solid #333;\n  border-left: 5px solid #8fd6ff;\n}\n\n.card-title h2 {\n  margin: 0px 0px 5px 5px;\n  font-size: 23px;\n  padding: 5px;\n}\n\n.check {\n  font-size: 12px;\n  padding-left: 12px;\n}\n\n.uploaded-image {\n  margin: 0px auto;\n  width: auto;\n  padding: 50px;\n  height: auto;\n  cursor: pointer;\n}\n\n.uploaded-image .text-fileupload {\n  display: block;\n  text-align: center;\n  cursor: pointer;\n  color: #2480fb;\n  margin-top: 10px;\n}\n\n.uploaded-image .img {\n  width: 100%;\n  background-size: cover;\n  position: relative;\n  background-position: center center;\n  background-repeat: no-repeat;\n}\n\n.uploaded-image .img .editor-tool {\n  background: rgba(0, 0, 0, 0.2);\n  width: 100%;\n  height: 100%;\n  position: relative;\n  opacity: 0;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.uploaded-image .img:hover .editor-tool {\n  opacity: 0.9;\n}\n\ninput[type=file] {\n  display: none;\n}\n\n@media (max-width: 850px) {\n  .image-upload-group .img-upload > .card > .card-content .image .custom img.set-opacity {\n    height: 25px;\n  }\n  .image-upload-group .img-upload > .card > .card-content .image .custom .browse-files .pb-framer .common-button {\n    margin-top: 8px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0ZXIvb3Blbi1tZXJjaGFudC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxyZWdpc3Rlclxcb3Blbi1tZXJjaGFudFxcb3Blbi1tZXJjaGFudC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcmVnaXN0ZXIvb3Blbi1tZXJjaGFudC9vcGVuLW1lcmNoYW50LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0Usa0NBQUE7RUFDQSx5QkFBQTtBQ0FGOztBREdBO0VBQ0UseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FDQUY7O0FEQ0U7RUFDRSxlQUFBO0FDQ0o7O0FERUE7RUFDRSxrQkFBQTtBQ0NGOztBREFFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDRUo7O0FEQUU7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ0VKOztBREFFO0VBQ0UsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDRUo7O0FEQ0E7RUFDRSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQ0VGOztBREFBO0VBQ0UsVUFBQTtBQ0dGOztBRERBO0VBQ0UsYUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDSUY7O0FESEU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7QUNLSjs7QURGQTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNLRjs7QURIQTtFQUNFLGFBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ01GOztBREpFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtBQ01KOztBREpFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsdUJBQUE7QUNNSjs7QURIQTtFQUNFLGlCQUFBO0VBQ0EsVUFBQTtFQUdBLDhDQUFBO0FDTUY7O0FETEU7RUFDRSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7QUNPSjs7QURKQTtFQUNFLGFBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFFQSxtQkFBQTtBQ01GOztBRExFO0VBQ0UsVUFBQTtBQ09KOztBRExFO0VBQ0UsVUFBQTtBQ09KOztBRExFO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0FDT0o7O0FETEk7RUFDRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUNPTjs7QURMUTtFQUNFLGVBQUE7QUNPVjs7QURKTTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUNNUjs7QURMUTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNPVjs7QUROVTtFQUVFLFlBQUE7RUFDQSxxQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtBQ09aOztBRE5ZO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDUWQ7O0FESE07RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0QsYUFBQTtBQ0tQOztBREpRO0VBQ0UsWUFBQTtBQ01WOztBRExVO0VBQ0UsUUFBQTtBQ09aOztBREpRO0VBQ0UsWUFBQTtFQUNBLGFBQUE7QUNNVjs7QURMVTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNPWjs7QURMVTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUNPWjs7QURMVTtFQUNFLFlBQUE7QUNPWjs7QURERTtFQUNFLGlCQUFBO0FDR0o7O0FEQ0U7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0FDRUo7O0FEREk7RUFDRSxpQkFBQTtBQ0dOOztBRERJO0VBQ0Usa0JBQUE7QUNHTjs7QURFRTtFQUNFLGlCQUFBO0FDQ0o7O0FERUE7RUFDRSx5QkFBQTtBQ0NGOztBRENBO0VBQ0UsYUFBQTtFQUNBLGdCQUFBO0VBRUEsa0JBQUE7QUNDRjs7QURFSTtFQUNFLGdCQUFBO0FDQU47O0FER0U7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7QUNESjs7QURHRTtFQUNFLGdCQUFBO0FDREo7O0FER0U7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUNESjs7QURHSTtFQUNFLGNBQUE7RUFDQSxXQWhQQztFQWlQRCxZQWpQQztFQWtQRCxXQUFBO0FDRE47O0FESUk7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDRk47O0FESU07RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUNGUjs7QURPRTtFQUNFLGdCQUFBO0FDTEo7O0FEUUU7RUFDRSxrQkFBQTtBQ05KOztBRFFJO0VBQ0UsMEJBQUE7QUNOTjs7QURTSTtFQUNFLGVBQUE7RUFDQSw2QkFBQTtFQUNBLGlCQUFBO0FDUE47O0FEVUk7RUFDRSx1QkFBQTtFQUNBLG1CQUFBO0FDUk47O0FEV0k7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsNkJBQUE7VUFBQSxxQkFBQTtBQ1ROOztBRFlJO0VBQ0UsYUFBQTtBQ1ZOOztBRGNFO0VBQ0UsOEJBQUE7QUNaSjs7QURlRTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtBQ2JKOztBRGlCQTtFQUNFLGdCQUFBO0VBQ0EsNkJBQUE7RUFDQSw4QkFBQTtBQ2RGOztBRGVFO0VBQ0UsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ2JKOztBRGlCQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBQ2RGOztBRGlCQTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ2RGOztBRGdCRTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUNkSjs7QURpQkU7RUFDRSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7QUNmSjs7QURpQkk7RUFDRSw4QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNmTjs7QURtQkU7RUFDRSxZQUFBO0FDakJKOztBRHFCQTtFQUNFLGFBQUE7QUNsQkY7O0FEcUJBO0VBT2M7SUFDRSxZQUFBO0VDeEJkO0VENEJnQjtJQUNFLGVBQUE7RUMxQmxCO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9yZWdpc3Rlci9vcGVuLW1lcmNoYW50L29wZW4tbWVyY2hhbnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkc2l6ZTogMTB2dztcclxuKiB7XHJcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiLCBzYW5zLXNlcmlmO1xyXG4gIHRyYW5zaXRpb246IGVhc2UgYWxsIDAuM3M7XHJcbn1cclxuXHJcbi5oZWFkZXJ7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNhNDQ1ODtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDgwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGp1c3RpZnktY29udGVudDogbGVmdDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBoMyB7XHJcbiAgICBtYXJnaW4tbGVmdDogOCU7XHJcbiAgfVxyXG59XHJcbi5zdWNjZXNzIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgLmltZy1zdWNjZXNzIHtcclxuICAgIHdpZHRoOiAyNXZ3O1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMiU7XHJcbiAgICBtaW4td2lkdGg6IDI1MHB4O1xyXG4gIH1cclxuICAuaGVhZGVyLXN1Y2Nlc3Mge1xyXG4gICAgbWFyZ2luLXRvcDoxJTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIC5zdWItaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIlO1xyXG4gICAgY29sb3I6Z3JleTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gIH1cclxufVxyXG4udGl0bGUtZGlmZiB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAzNXB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiA3JTtcclxuICBtYXJnaW4tYm90dG9tOiAyJTtcclxufVxyXG4uZXN0YXIge1xyXG4gIGNvbG9yOiByZWQ7XHJcbn1cclxuLnNhdmUtY29udGFpbmVyIHtcclxuICBtYXJnaW46IDQlIDAlO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAuc2F2ZSB7XHJcbiAgICB3aWR0aDogMzUlO1xyXG4gICAgb3V0bGluZTpub25lO1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmNzMwMTtcclxuICB9XHJcbn1cclxuLmRhcmt3aGl0ZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzcwNzA3MCA7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG4uc3VjY2Vzcy1jb250YWluZXIge1xyXG4gIG1hcmdpbjogMSUgMCU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIFxyXG4gIC5hZGQge1xyXG4gICAgd2lkdGg6IDI1JTtcclxuICAgIG91dGxpbmU6bm9uZTtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZjczMDE7XHJcbiAgfVxyXG4gIC5kYXNoYm9hcmQge1xyXG4gICAgd2lkdGg6IDI1JTtcclxuICAgIG91dGxpbmU6bm9uZTtcclxuICAgIGJvcmRlci1jb2xvcjogI2ZmNzMwMTtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGNvbG9yOiAjZmY3MzAxO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgfVxyXG59XHJcbi5jb250YWluZXIge1xyXG4gIHBhZGRpbmc6IDMlIDUlIDMlO1xyXG4gIHdpZHRoOiA3NSU7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gIC1tb3otYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gIC5jb250YWluZXItaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDYlO1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC13ZWlnaHQ6IDkwMDtcclxuICAgIGNvbG9yOiBncmV5O1xyXG4gICAgbWFyZ2luLXRvcDogMyU7XHJcbiAgfVxyXG59XHJcbi5pbWFnZS11cGxvYWQtZ3JvdXAge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcclxuICAuYmcgeyBcclxuICAgIHdpZHRoOiA2NSU7XHJcbiAgfVxyXG4gIC5sb2dvIHsgXHJcbiAgICB3aWR0aDogMzUlO1xyXG4gIH1cclxuICAuaW1nLXVwbG9hZCB7XHJcbiAgICBoZWlnaHQ6IDQwMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICBcclxuICAgID4gLmNhcmQge1xyXG4gICAgICBoZWlnaHQ6IDMwMHB4O1xyXG4gICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICA+IC5jYXJkLXRpdGxlIHtcclxuICAgICAgICBoMiB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDI0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgID4gLmNhcmQtY29udGVudCB7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBoZWlnaHQ6IDI1MHB4O1xyXG4gICAgICAgIC5pbWFnZSB7XHJcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAuY3VzdG9tIHtcclxuICAgICAgICBcclxuICAgICAgICAgICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMDMpO1xyXG4gICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCAjZWJlYmViO1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICAgcGFkZGluZzogNDBweDtcclxuICAgICAgICAgICAgLnNldC1vcGFjaXR5IHtcclxuICAgICAgICAgICAgICBvcGFjaXR5OiAwLjE7XHJcbiAgICAgICAgICAgICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgICAgICAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgID4gLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICBoZWlnaHQ6IDI1MHB4O1xyXG4gICAgICAgIC5sb2FkaW5nIHtcclxuICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgIC5zay1mYWRpbmctY2lyY2xlIHtcclxuICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC51cGxvYWRlZC1pbWFnZSB7XHJcbiAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgICAgLmltZyB7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLmltZyA+IGltZyB7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDMwMHB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLmVkaXRvci10b29sIHtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAuaW1nLXVwbG9hZDpsYXN0LWNoaWxkIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNnB4O1xyXG4gIH1cclxufVxyXG4uZm9ybS1ib2R5IHtcclxuICAub3ItZ3JvdXAge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgLmNvbC1tZC02IHtcclxuICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICB9XHJcbiAgICAuY29sLW1kLTY6bGFzdC1jaGlsZCB7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLmNvdXJpZXItc2VydmljZSB7XHJcbiAgLm1hdC1jaGVja2JveDpub3QoOmZpcnN0LWNoaWxkKSB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICB9XHJcbn1cclxuLmJ0bi5zYXZlIHtcclxuICBtYXJnaW46IDE1cHggMTVweCAwcHggMHB4O1xyXG59XHJcbi5mb3JtIHtcclxuICBwYWRkaW5nOiAyNXB4O1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgLy8gdG9wOiA1dnc7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAuYnJvd3NlLWZpbGVzIHtcclxuICAgIC5maWxldXBsb2FkIHtcclxuICAgICAgbWFyZ2luLXRvcDogMjVweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmVycm9yLW1lc3NhZ2Uge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG4gIC5lcnJvci1tYXJnaW4ge1xyXG4gICAgbWFyZ2luLXRvcDogNDBweDtcclxuICB9XHJcbiAgLmhlYWQtZm9ybSB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgaGVpZ2h0OiAxMXZ3O1xyXG5cclxuICAgIC5waG90by1wcm9maWxlIHtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIHdpZHRoOiAkc2l6ZTtcclxuICAgICAgaGVpZ2h0OiAkc2l6ZTtcclxuICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLmRldGFpbCB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgaGVpZ2h0OiA2NXB4O1xyXG4gICAgICBsaW5lLWhlaWdodDogMTdweDtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICBtYXJnaW4tdG9wOiAzdnc7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAydnc7XHJcblxyXG4gICAgICAubmFtZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAyN3B4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuZWRpdG9yLWdyb3VwIHtcclxuICAgIG1hcmdpbi10b3A6IDI1cHg7XHJcbiAgfVxyXG5cclxuICAuZm9ybS1ncm91cCB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBcclxuICAgIGxhYmVsIHtcclxuICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICB9XHJcblxyXG4gICAgLmZvcm0tY29udHJvbCB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtLWNvbnRyb2wudHJ1ZSB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtLWNvbnRyb2wgfiAuaWNvbmljIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICByaWdodDogMTBweDtcclxuICAgICAgdG9wOiAzN3B4O1xyXG4gICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgICB9XHJcblxyXG4gICAgLmRlc2NyaXB0aW9uIHtcclxuICAgICAgaGVpZ2h0OiAxMTBweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC52bCB7XHJcbiAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNlYWVhZWE7XHJcbiAgfVxyXG5cclxuICBpbWcucmVzaXplIHtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIG1heC1oZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG59XHJcblxyXG4uY2FyZC10aXRsZSB7XHJcbiAgbWFyZ2luOiAyMHB4IDBweDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzMzMztcclxuICBib3JkZXItbGVmdDogNXB4IHNvbGlkICM4ZmQ2ZmY7XHJcbiAgaDIge1xyXG4gICAgbWFyZ2luOiAwcHggMHB4IDVweCA1cHg7XHJcbiAgICBmb250LXNpemU6IDIzcHg7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgfVxyXG59XHJcblxyXG4uY2hlY2sge1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBwYWRkaW5nLWxlZnQ6IDEycHg7XHJcbn1cclxuXHJcbi51cGxvYWRlZC1pbWFnZSB7XHJcbiAgbWFyZ2luOiAwcHggYXV0bztcclxuICB3aWR0aDogYXV0bztcclxuICBwYWRkaW5nOiA1MHB4O1xyXG4gIGhlaWdodDogYXV0bztcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcblxyXG4gIC50ZXh0LWZpbGV1cGxvYWQge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBjb2xvcjogIzI0ODBmYjtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgfVxyXG5cclxuICAuaW1nIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG5cclxuICAgIC5lZGl0b3ItdG9vbCB7XHJcbiAgICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICBvcGFjaXR5OiAwO1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5pbWc6aG92ZXIgLmVkaXRvci10b29sIHtcclxuICAgIG9wYWNpdHk6IDAuOTtcclxuICB9XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9XCJmaWxlXCJdIHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogODUwcHgpIHtcclxuICAuaW1hZ2UtdXBsb2FkLWdyb3VwIHtcclxuICAgIC5pbWctdXBsb2FkIHtcclxuICAgICAgPiAuY2FyZCB7XHJcbiAgICAgICAgPiAuY2FyZC1jb250ZW50IHtcclxuICAgICAgICAgIC5pbWFnZSB7XHJcbiAgICAgICAgICAgIC5jdXN0b20ge1xyXG4gICAgICAgICAgICAgIGltZy5zZXQtb3BhY2l0eSB7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIC5icm93c2UtZmlsZXMge1xyXG4gICAgICAgICAgICAgICAgLnBiLWZyYW1lciB7XHJcbiAgICAgICAgICAgICAgICAgIC5jb21tb24tYnV0dG9uIHtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiKiB7XG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIiwgc2Fucy1zZXJpZjtcbiAgdHJhbnNpdGlvbjogZWFzZSBhbGwgMC4zcztcbn1cblxuLmhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzYTQ0NTg7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDgwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGp1c3RpZnktY29udGVudDogbGVmdDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmhlYWRlciBoMyB7XG4gIG1hcmdpbi1sZWZ0OiA4JTtcbn1cblxuLnN1Y2Nlc3Mge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uc3VjY2VzcyAuaW1nLXN1Y2Nlc3Mge1xuICB3aWR0aDogMjV2dztcbiAgaGVpZ2h0OiBhdXRvO1xuICBtYXJnaW4tYm90dG9tOiAyJTtcbiAgbWluLXdpZHRoOiAyNTBweDtcbn1cbi5zdWNjZXNzIC5oZWFkZXItc3VjY2VzcyB7XG4gIG1hcmdpbi10b3A6IDElO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAzNXB4O1xuICB3aWR0aDogMTAwJTtcbn1cbi5zdWNjZXNzIC5zdWItaGVhZGVyIHtcbiAgbWFyZ2luLWJvdHRvbTogMiU7XG4gIGNvbG9yOiBncmV5O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG4udGl0bGUtZGlmZiB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDM1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogNyU7XG4gIG1hcmdpbi1ib3R0b206IDIlO1xufVxuXG4uZXN0YXIge1xuICBjb2xvcjogcmVkO1xufVxuXG4uc2F2ZS1jb250YWluZXIge1xuICBtYXJnaW46IDQlIDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5zYXZlLWNvbnRhaW5lciAuc2F2ZSB7XG4gIHdpZHRoOiAzNSU7XG4gIG91dGxpbmU6IG5vbmU7XG4gIGhlaWdodDogNjBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY3MzAxO1xufVxuXG4uZGFya3doaXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzcwNzA3MDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5zdWNjZXNzLWNvbnRhaW5lciB7XG4gIG1hcmdpbjogMSUgMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnN1Y2Nlc3MtY29udGFpbmVyIC5hZGQge1xuICB3aWR0aDogMjUlO1xuICBvdXRsaW5lOiBub25lO1xuICBoZWlnaHQ6IDUwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyOiBub25lO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY3MzAxO1xufVxuLnN1Y2Nlc3MtY29udGFpbmVyIC5kYXNoYm9hcmQge1xuICB3aWR0aDogMjUlO1xuICBvdXRsaW5lOiBub25lO1xuICBib3JkZXItY29sb3I6ICNmZjczMDE7XG4gIGhlaWdodDogNTBweDtcbiAgY29sb3I6ICNmZjczMDE7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4uY29udGFpbmVyIHtcbiAgcGFkZGluZzogMyUgNSUgMyU7XG4gIHdpZHRoOiA3NSU7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xufVxuLmNvbnRhaW5lciAuY29udGFpbmVyLWhlYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDYlO1xuICBmb250LXNpemU6IDI1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgY29sb3I6IGdyZXk7XG4gIG1hcmdpbi10b3A6IDMlO1xufVxuXG4uaW1hZ2UtdXBsb2FkLWdyb3VwIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBtYXJnaW4tYm90dG9tOiAyNXB4O1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAuYmcge1xuICB3aWR0aDogNjUlO1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAubG9nbyB7XG4gIHdpZHRoOiAzNSU7XG59XG4uaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkIHtcbiAgaGVpZ2h0OiA0MDBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAuaW1nLXVwbG9hZCA+IC5jYXJkIHtcbiAgaGVpZ2h0OiAzMDBweDtcbiAgYm9yZGVyOiBub25lO1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQgPiAuY2FyZCA+IC5jYXJkLXRpdGxlIGgyIHtcbiAgZm9udC1zaXplOiAyNHB4O1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAuaW1nLXVwbG9hZCA+IC5jYXJkID4gLmNhcmQtY29udGVudCB7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDI1MHB4O1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAuaW1nLXVwbG9hZCA+IC5jYXJkID4gLmNhcmQtY29udGVudCAuaW1hZ2Uge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZmxvYXQ6IGxlZnQ7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4uaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkID4gLmNhcmQgPiAuY2FyZC1jb250ZW50IC5pbWFnZSAuY3VzdG9tIHtcbiAgbWFyZ2luOiAyMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMDMpO1xuICBib3JkZXI6IDJweCBzb2xpZCAjZWJlYmViO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgcGFkZGluZzogNDBweDtcbn1cbi5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQgPiAuY2FyZCA+IC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20gLnNldC1vcGFjaXR5IHtcbiAgb3BhY2l0eTogMC4xO1xuICBoZWlnaHQ6IDYwcHg7XG4gIHdpZHRoOiBhdXRvO1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAuaW1nLXVwbG9hZCA+IC5jYXJkID4gLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgaGVpZ2h0OiAyNTBweDtcbn1cbi5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQgPiAuY2FyZCA+IC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAuaW1nLXVwbG9hZCA+IC5jYXJkID4gLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcgLnNrLWZhZGluZy1jaXJjbGUge1xuICB0b3A6IDUwJTtcbn1cbi5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQgPiAuY2FyZCA+IC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZSB7XG4gIGhlaWdodDogMTAwJTtcbiAgcGFkZGluZzogMjBweDtcbn1cbi5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQgPiAuY2FyZCA+IC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZSAuaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogYXV0bztcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkID4gLmNhcmQgPiAuZm9ybS1ncm91cC51cGxvYWRlZCAudXBsb2FkZWQtaW1hZ2UgLmltZyA+IGltZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IGF1dG87XG4gIG1heC1oZWlnaHQ6IDMwMHB4O1xufVxuLmltYWdlLXVwbG9hZC1ncm91cCAuaW1nLXVwbG9hZCA+IC5jYXJkID4gLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlIC5lZGl0b3ItdG9vbCB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQ6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1sZWZ0OiAxNnB4O1xufVxuXG4uZm9ybS1ib2R5IC5vci1ncm91cCB7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xufVxuLmZvcm0tYm9keSAub3ItZ3JvdXAgLmNvbC1tZC02IHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG59XG4uZm9ybS1ib2R5IC5vci1ncm91cCAuY29sLW1kLTY6bGFzdC1jaGlsZCB7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cblxuLmNvdXJpZXItc2VydmljZSAubWF0LWNoZWNrYm94Om5vdCg6Zmlyc3QtY2hpbGQpIHtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG59XG5cbi5idG4uc2F2ZSB7XG4gIG1hcmdpbjogMTVweCAxNXB4IDBweCAwcHg7XG59XG5cbi5mb3JtIHtcbiAgcGFkZGluZzogMjVweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmZvcm0gLmJyb3dzZS1maWxlcyAuZmlsZXVwbG9hZCB7XG4gIG1hcmdpbi10b3A6IDI1cHg7XG59XG4uZm9ybSAuZXJyb3ItbWVzc2FnZSB7XG4gIGZsb2F0OiBsZWZ0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZm9ybSAuZXJyb3ItbWFyZ2luIHtcbiAgbWFyZ2luLXRvcDogNDBweDtcbn1cbi5mb3JtIC5oZWFkLWZvcm0ge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBoZWlnaHQ6IDExdnc7XG59XG4uZm9ybSAuaGVhZC1mb3JtIC5waG90by1wcm9maWxlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMHZ3O1xuICBoZWlnaHQ6IDEwdnc7XG4gIGZsb2F0OiBsZWZ0O1xufVxuLmZvcm0gLmhlYWQtZm9ybSAuZGV0YWlsIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgZmxvYXQ6IGxlZnQ7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBoZWlnaHQ6IDY1cHg7XG4gIGxpbmUtaGVpZ2h0OiAxN3B4O1xuICBwYWRkaW5nOiA1cHg7XG4gIG1hcmdpbi10b3A6IDN2dztcbiAgbWFyZ2luLWxlZnQ6IDJ2dztcbn1cbi5mb3JtIC5oZWFkLWZvcm0gLmRldGFpbCAubmFtZSB7XG4gIGZvbnQtc2l6ZTogMjdweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uZm9ybSAuZWRpdG9yLWdyb3VwIHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbn1cbi5mb3JtIC5mb3JtLWdyb3VwIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmZvcm0gLmZvcm0tZ3JvdXAgbGFiZWwge1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cbi5mb3JtIC5mb3JtLWdyb3VwIC5mb3JtLWNvbnRyb2wge1xuICBmb250LXNpemU6IDEycHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mb3JtIC5mb3JtLWdyb3VwIC5mb3JtLWNvbnRyb2wudHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuLmZvcm0gLmZvcm0tZ3JvdXAgLmZvcm0tY29udHJvbCB+IC5pY29uaWMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxMHB4O1xuICB0b3A6IDM3cHg7XG4gIHRyYW5zZm9ybTogc2NhbGUoMC43KTtcbn1cbi5mb3JtIC5mb3JtLWdyb3VwIC5kZXNjcmlwdGlvbiB7XG4gIGhlaWdodDogMTEwcHg7XG59XG4uZm9ybSAudmwge1xuICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNlYWVhZWE7XG59XG4uZm9ybSBpbWcucmVzaXplIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBtYXgtaGVpZ2h0OiAxMDAlO1xufVxuXG4uY2FyZC10aXRsZSB7XG4gIG1hcmdpbjogMjBweCAwcHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMzMzO1xuICBib3JkZXItbGVmdDogNXB4IHNvbGlkICM4ZmQ2ZmY7XG59XG4uY2FyZC10aXRsZSBoMiB7XG4gIG1hcmdpbjogMHB4IDBweCA1cHggNXB4O1xuICBmb250LXNpemU6IDIzcHg7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuLmNoZWNrIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBwYWRkaW5nLWxlZnQ6IDEycHg7XG59XG5cbi51cGxvYWRlZC1pbWFnZSB7XG4gIG1hcmdpbjogMHB4IGF1dG87XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nOiA1MHB4O1xuICBoZWlnaHQ6IGF1dG87XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi51cGxvYWRlZC1pbWFnZSAudGV4dC1maWxldXBsb2FkIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBjb2xvcjogIzI0ODBmYjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi51cGxvYWRlZC1pbWFnZSAuaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cbi51cGxvYWRlZC1pbWFnZSAuaW1nIC5lZGl0b3ItdG9vbCB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvcGFjaXR5OiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi51cGxvYWRlZC1pbWFnZSAuaW1nOmhvdmVyIC5lZGl0b3ItdG9vbCB7XG4gIG9wYWNpdHk6IDAuOTtcbn1cblxuaW5wdXRbdHlwZT1maWxlXSB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA4NTBweCkge1xuICAuaW1hZ2UtdXBsb2FkLWdyb3VwIC5pbWctdXBsb2FkID4gLmNhcmQgPiAuY2FyZC1jb250ZW50IC5pbWFnZSAuY3VzdG9tIGltZy5zZXQtb3BhY2l0eSB7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICB9XG4gIC5pbWFnZS11cGxvYWQtZ3JvdXAgLmltZy11cGxvYWQgPiAuY2FyZCA+IC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20gLmJyb3dzZS1maWxlcyAucGItZnJhbWVyIC5jb21tb24tYnV0dG9uIHtcbiAgICBtYXJnaW4tdG9wOiA4cHg7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/register/open-merchant/open-merchant.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/register/open-merchant/open-merchant.component.ts ***!
  \*******************************************************************/
/*! exports provided: OpenMerchantComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OpenMerchantComponent", function() { return OpenMerchantComponent; });
/* harmony import */ var _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../services/merchant/merchant.service */ "./src/app/services/merchant/merchant.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_login_login_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/login/login.service */ "./src/app/services/login/login.service.ts");
/* harmony import */ var _services_courier_courier_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/courier/courier.service */ "./src/app/services/courier/courier.service.ts");
/* harmony import */ var _services_country_country_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/country/country.service */ "./src/app/services/country/country.service.ts");
/* harmony import */ var _services_paymentgateway_paymentgateway_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/paymentgateway/paymentgateway.service */ "./src/app/services/paymentgateway/paymentgateway.service.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var OpenMerchantComponent = /** @class */ (function () {
    function OpenMerchantComponent(memberService, countryService, courierService, paymentGatewayService, loginService, router, route, merchantService) {
        this.memberService = memberService;
        this.countryService = countryService;
        this.courierService = courierService;
        this.paymentGatewayService = paymentGatewayService;
        this.loginService = loginService;
        this.router = router;
        this.route = route;
        this.merchantService = merchantService;
        // @Input() public detail: any;
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4___default.a;
        this.config = {
            toolbar: ['heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote'],
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
                ]
            }
        };
        this.errorLabel = false;
        this.autoFill = true;
        this.edit = false;
        this.merchant_group = [];
        this.available_courier = [];
        this.success = false;
        this.complete = false;
        this.bgUrl = '';
        this.countryList = [];
        this.showLoading = false;
        this.showLoading1 = false;
        this.showLoading2 = false;
        this.openCourier = false;
        this.form = {
            cell_phone: '',
            dob: '',
            email: '',
            full_name: '',
            gender: '',
            merchant_name: '',
            merchant_username: '',
            description: '',
            image_owner_url: '',
            background_image: '',
            mcountry: 'Indonesia',
            mcity: '',
            mcity_code: '',
            mstate: '',
            mprovince: '',
            mprovince_code: '',
            mdistrict: '',
            mpostal_code: '',
            mvillage: '',
            mvillage_code: '',
            region_code: '',
            region_name: '',
            msubdistrict: '',
            msubdistrict_code: '',
            address: '',
            maddress: '',
            contact_person: '',
            email_address: '',
            work_phone: '',
            handphone: '',
            fax_number: '',
            active: 0,
            _id: '',
            slogan: '',
            available_courier: undefined
        };
        this.formEditable = {};
        this.courierServices = [];
        this.regionList = [];
        this.provinceList = [];
        this.cityList = [];
        this.subDistrictList = [];
        this.villageList = [];
        this.merchantGroup = [];
        this.selectedFile = null;
        this.selectedFile2 = null;
        this.selectedFile3 = null;
        this.selectedFile4 = null;
        this.cancel = false;
        this.hasToko = false;
        this.bukaTokoBtn = true;
        this.upload = false;
        this.addMerchantDetail = false;
        this.storeInfoBtn = true;
        this.storeProfile = false;
        this.open = false;
        this.openStore = false;
        this.keyname = [];
        this.activeCheckbox = false;
        this.checkBox = [];
    }
    OpenMerchantComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OpenMerchantComponent.prototype.getCountryList = function () {
        var n = this.countryService.getCountryList();
        var r = [];
        n.forEach(function (element) {
            r.push({ value: element, name: element });
        });
        this.countryList = r;
        this.form.mcountry = 'Indonesia';
    };
    OpenMerchantComponent.prototype.valueCheck = function () {
        console.log("ke value check");
        if (this.form.mprovince_code && this.form.mcity_code && this.form.msubdistrict_code && this.form.mvillage_code)
            this.complete = true;
        else
            this.complete = false;
    };
    OpenMerchantComponent.prototype.editableForm = function (label, val) {
        this.formEditable[label] = val;
    };
    OpenMerchantComponent.prototype.goToAdd = function () {
        // this.router.navigate(['/merchant-portal/add'])
        this.router.navigate(['/login-merchant']);
    };
    OpenMerchantComponent.prototype.goToDashboard = function () {
        this.router.navigate(['/merchant-portal']);
    };
    OpenMerchantComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var member, data, member_1, data;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.memberService.getMemberDetail()];
                    case 1:
                        member = _a.sent();
                        if (member.result) {
                            data = member.result;
                            this.form = __assign({}, this.form, data);
                        }
                        if (!(member.result.permission == 'merchant')) return [3 /*break*/, 3];
                        this.success = true;
                        return [4 /*yield*/, this.merchantService.getDetail()];
                    case 2:
                        member_1 = _a.sent();
                        data = member_1.result;
                        this.form = __assign({}, this.form, data);
                        _a.label = 3;
                    case 3:
                        if (this.form.available_courier && this.form.available_courier.length) {
                            this.form.available_courier.forEach(function (value) {
                                _this.setCourierService(value);
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    OpenMerchantComponent.prototype.getCourierSupported = function () {
        return __awaiter(this, void 0, void 0, function () {
            var form, result, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        form = {
                            province_code: this.form.mprovince_code,
                            city_code: this.form.mcity_code,
                            subdistrict_code: this.form.msubdistrict_code,
                            village_code: this.form.mvillage_code
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        console.log("ini form ", form);
                        return [4 /*yield*/, this.courierService.getCourier(form)];
                    case 2:
                        result = _a.sent();
                        if (result) {
                            this.courierServices = result.result;
                            this.courierServices.forEach(function (el, i) {
                                Object.assign(el.courier, { value: false });
                            });
                            console.log("get courier", this.courierService);
                            this.showLoading = false;
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        setTimeout(function () {
                            _this.errorMessage = (e_1.message);
                        }, 500);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    OpenMerchantComponent.prototype.seeAvailableCourier = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.showLoading = true;
                        return [4 /*yield*/, this.getCourierSupported()];
                    case 1:
                        result = _a.sent();
                        this.formcourier = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    OpenMerchantComponent.prototype.setCourierService = function (value) {
        for (var n in this.courierServices) {
            if (value == this.courierServices[n].courier_code) {
                this.courierServices[n].value = true;
                return true;
            }
        }
        return false;
    };
    OpenMerchantComponent.prototype.getValueCheckbox = function () {
        var getData = [];
        this.courierServices.forEach(function (el, i) {
            if (el.value == true) {
                getData.push(el);
            }
        });
        return getData;
    };
    OpenMerchantComponent.prototype.saveProfile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, valid, resultMerchant, result, e_2;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.errorMessage = '';
                        this.form.contact_person = this.form.contact_person.toString();
                        this.form.mpostal_code = this.form.mpostal_code.toString();
                        data = JSON.parse(JSON.stringify(this.form));
                        data.email_address = data.email;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, , 7]);
                        if (this.form.withdrawal_bank_name && this.form.withdrawal_bank_name != "none") {
                            data.withdrawal_bank_name = JSON.parse(this.form.withdrawal_bank_name);
                        }
                        else {
                            data.withdrawal_bank_name = '';
                        }
                        data.available_courier = this.getValueCheckbox();
                        if (!(this.form.permission == 'member')) return [3 /*break*/, 3];
                        valid = this.formValidation();
                        console.log("ini data", data);
                        return [4 /*yield*/, this.merchantService.addMerchant(data)];
                    case 2:
                        resultMerchant = _a.sent();
                        this.success = true;
                        localStorage.removeItem('isLoggedin');
                        localStorage.removeItem('tokenlogin');
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.memberService.updateProfile(data)];
                    case 4:
                        result = _a.sent();
                        _a.label = 5;
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        e_2 = _a.sent();
                        setTimeout(function () {
                            _this.errorMessage = (e_2.message);
                        }, 500);
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    OpenMerchantComponent.prototype.autoLogin = function (login_data) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(login_data);
                        return [4 /*yield*/, this.loginService.memberLogin(login_data)];
                    case 1:
                        result = _a.sent();
                        console.log('here');
                        if (!result.error) {
                            console.log(result);
                            localStorage.setItem('isLoggedin', 'true');
                            localStorage.setItem('tokenlogin', result.result.token);
                            // this.router.navigate(['/merchant-portal/profile'])
                            this.router.navigate(['/register/open-merchant']);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    OpenMerchantComponent.prototype.nameInput = function ($event) {
        if (this.autoFill == true) {
            var fill = this.form.merchant_name.toLowerCase().replace(/[\s\?\*]/g, '-');
            this.form.merchant_username = fill;
        }
    };
    OpenMerchantComponent.prototype.formValidation = function () {
        console.log(this.form);
        return true;
    };
    OpenMerchantComponent.prototype.openUploadLegalDocument = function () {
        this.upload = true;
        this.bukaTokoBtn = false;
    };
    OpenMerchantComponent.prototype.openAddMerchantDetail = function () {
        this.upload = false;
        this.addMerchantDetail = true;
    };
    OpenMerchantComponent.prototype.openStoreInfo = function () {
        this.openStore = true;
        this.storeInfoBtn = false;
    };
    OpenMerchantComponent.prototype.openStoreProfile = function () {
        this.openStore = false;
        this.storeProfile = true;
    };
    OpenMerchantComponent.prototype.getVillageList = function ($event) {
        var _this = this;
        var subdistrict_code = this.subdistrict_code;
        var result = $event.target.value;
        console.log('ini biang kerok', $event);
        if (result.length > 2) {
            this.memberService.getVillage(result, 'origin', subdistrict_code).then(function (result) {
                _this.villageList = result.result;
                console.log('INI village', _this.villageList);
                _this.onVillageChange();
            });
        }
    };
    OpenMerchantComponent.prototype.onVillageChange = function () {
        var _this = this;
        console.log('ON village Changed', this.form.mvillage, this.villageList);
        this.villageList.forEach(function (element) {
            if (_this.form.mvillage.toLowerCase() == element.village.toLowerCase()) {
                _this.form.mvillage_code = _this.village_code = element.village_code;
                _this.valueCheck();
            }
        });
        console.log("kode village", this.form.mvillage_code);
    };
    OpenMerchantComponent.prototype.getSubDistrictList = function ($event) {
        var _this = this;
        var city_code = this.city_code;
        var result = $event.target.value;
        console.log('ini biang kerok', $event);
        if (result.length > 2) {
            this.memberService.getSubDistrict(result, 'origin', city_code).then(function (result) {
                _this.subDistrictList = result.result;
                console.log('INI Subdistrict', _this.subDistrictList);
                _this.onSubDistrictChange();
            });
        }
    };
    OpenMerchantComponent.prototype.onSubDistrictChange = function () {
        var _this = this;
        console.log('ON Subdistrict Changed', this.form.msubdistrict, this.subDistrictList);
        this.subDistrictList.forEach(function (element) {
            if (_this.form.msubdistrict.toLowerCase() == element.subdistrict.toLowerCase()) {
                _this.subdistrict_code = _this.form.msubdistrict_code = element.subdistrict_code;
            }
        });
        console.log("kode subdistrict", this.city_code);
    };
    OpenMerchantComponent.prototype.getCityList = function ($event) {
        var _this = this;
        console.log('sekarang di city list');
        var province_code = this.province_code;
        var city = $event.target.value;
        console.log();
        if (city.length > 2) {
            this.memberService.getCity(city, 'origin', province_code).then(function (result) {
                _this.cityList = result.result;
                console.log('INI City', _this.cityList);
                _this.onCityChange();
            });
        }
    };
    OpenMerchantComponent.prototype.onCityChange = function () {
        var _this = this;
        console.log('ON City Changed', this.form.mcity, this.cityList);
        this.cityList.forEach(function (element) {
            if (_this.form.mcity.toLowerCase() == element.city.toLowerCase()) {
                _this.city_code = _this.form.mcity_code = element.city_code;
            }
        });
        console.log("kode kota", this.city_code);
    };
    OpenMerchantComponent.prototype.getProvinceList = function ($event) {
        var _this = this;
        var result = $event.target.value;
        console.log('ini biang kerok', $event.target.value);
        if (result.length > 2)
            this.memberService.getProvince(result, 'origin').then(function (result) {
                _this.provinceList = result.result;
                console.log('INI PROVINCE', _this.provinceList);
                _this.onProvinceChange();
            });
    };
    OpenMerchantComponent.prototype.onProvinceChange = function () {
        var _this = this;
        console.log('ON province Changed', this.form.mprovince, this.provinceList);
        this.provinceList.forEach(function (element) {
            if (_this.form.mprovince.toLowerCase() == element.province.toLowerCase()) {
                _this.form.mprovince_code = _this.province_code = element.province_code;
            }
        });
        console.log(this.form.mprovince_code);
    };
    OpenMerchantComponent.prototype.getRegionList = function ($event) {
        var _this = this;
        var result = $event.target.value;
        if (result.length > 2)
            this.memberService.getRegion(result, 'origin').then(function (result) {
                _this.regionList = result.result;
                console.log('hasil reg', _this.regionList);
                _this.onRegionChange();
            });
    };
    OpenMerchantComponent.prototype.onRegionChange = function () {
        var _this = this;
        console.log('ON Region Changed', this.form.region_name, this.regionList);
        this.regionList.forEach(function (element) {
            if (_this.form.region_name.toLowerCase() == element.region_name.toLowerCase()) {
                _this.form.region_code = element.region_code;
            }
        });
        console.log(this.form.region_code);
    };
    OpenMerchantComponent.prototype.onFileSelected = function (event, img) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_1, logo, e_3;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = this.errorLabel = this.errorLabel2 = false;
                        this.errorMessage = undefined;
                        fileMaxSize_1 = 3000000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_1) {
                                _this.errorFile = 'Maximum File Upload is 3MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!(this.selectedFile && this.errorFile == false)) return [3 /*break*/, 3];
                        switch (img) {
                            case 'image_owner_url':
                                this.showLoading1 = true;
                                console.log("ini2", img);
                                break;
                            case 'background_image':
                                this.showLoading2 = true;
                                console.log("ini3", img);
                                break;
                        }
                        return [4 /*yield*/, this.merchantService.upload(this.selectedFile, this, 'image', function (result) {
                                console.log("hasil", result);
                                _this.callImage(result, img);
                            })];
                    case 2:
                        logo = _a.sent();
                        _a.label = 3;
                    case 3:
                        if (this.errorFile != false) {
                            switch (img) {
                                case 'image_owner_url':
                                    this.errorLabel = this.errorFile;
                                    this.errorLabel2 = false;
                                    break;
                                case 'background_image':
                                    this.errorLabel2 = this.errorFile;
                                    this.errorLabel = false;
                                    break;
                            }
                            console.log(this.errorLabel, this.errorLabel2);
                            console.log("test lah", this.callImage);
                        }
                        return [3 /*break*/, 5];
                    case 4:
                        e_3 = _a.sent();
                        this.errorLabel = e_3.message;
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    OpenMerchantComponent.prototype.allfalse = function () {
        this.showLoading = this.showLoading2 = this.showLoading1
            = false;
    };
    OpenMerchantComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    OpenMerchantComponent.prototype.callImage = function (result, img) {
        console.log('result upload logo ', result);
        this.form[img] = result.base_url + result.pic_big_path;
        console.log('This detail', this.form);
        this.allfalse();
        // this.detail = {...this.detail, ...result}
    };
    OpenMerchantComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_country_country_service__WEBPACK_IMPORTED_MODULE_7__["CountryService"] },
        { type: _services_courier_courier_service__WEBPACK_IMPORTED_MODULE_6__["CourierServiceService"] },
        { type: _services_paymentgateway_paymentgateway_service__WEBPACK_IMPORTED_MODULE_8__["PaymentGatewayService"] },
        { type: _services_login_login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_0__["MerchantService"] }
    ]; };
    OpenMerchantComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-open-merchant',
            template: __webpack_require__(/*! raw-loader!./open-merchant.component.html */ "./node_modules/raw-loader/index.js!./src/app/register/open-merchant/open-merchant.component.html"),
            styles: [__webpack_require__(/*! ./open-merchant.component.scss */ "./src/app/register/open-merchant/open-merchant.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"],
            _services_country_country_service__WEBPACK_IMPORTED_MODULE_7__["CountryService"],
            _services_courier_courier_service__WEBPACK_IMPORTED_MODULE_6__["CourierServiceService"],
            _services_paymentgateway_paymentgateway_service__WEBPACK_IMPORTED_MODULE_8__["PaymentGatewayService"],
            _services_login_login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_0__["MerchantService"]])
    ], OpenMerchantComponent);
    return OpenMerchantComponent;
}());



/***/ }),

/***/ "./src/app/register/register-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/register/register-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: RegisterRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterRoutingModule", function() { return RegisterRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _register_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _open_merchant_open_merchant_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./open-merchant/open-merchant.component */ "./src/app/register/open-merchant/open-merchant.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        component: _register_component__WEBPACK_IMPORTED_MODULE_2__["RegisterComponent"]
    },
    {
        path: 'open-merchant',
        component: _open_merchant_open_merchant_component__WEBPACK_IMPORTED_MODULE_3__["OpenMerchantComponent"]
    }
];
var RegisterRoutingModule = /** @class */ (function () {
    function RegisterRoutingModule() {
    }
    RegisterRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], RegisterRoutingModule);
    return RegisterRoutingModule;
}());



/***/ }),

/***/ "./src/app/register/register.component.scss":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  font-family: \"Poppins\", sans-serif;\n  transition: ease all 0.3s;\n}\n\nmat-list-item {\n  flex-direction: row;\n  height: 65px !important;\n}\n\nbody {\n  width: 100%;\n  height: 100%;\n  margin: 0px;\n  padding: 0px;\n  overflow-x: hidden;\n}\n\n#form-regis-img {\n  margin-top: 20vw;\n  width: 34%;\n}\n\n.container {\n  -webkit-transform: scale(0.85);\n          transform: scale(0.85);\n}\n\n.divider {\n  width: 10%;\n  height: auto;\n  display: inline-block;\n}\n\n#page-regis-img {\n  width: 52%;\n  margin-left: 8vh;\n  margin-top: -6vw;\n}\n\n#locard_icon_img {\n  height: 45px;\n  margin-right: 5px;\n}\n\n.register-form-body {\n  width: 100vw !important;\n  margin-top: 3vh;\n  padding: 3% 5% 3%;\n  box-shadow: 0px 2px 5px 3px rgba(0, 0, 0, 0.1);\n}\n\n.register-form-body .register-form {\n  display: flex;\n  flex-direction: column;\n  font-size: 17px;\n}\n\n.register-form-body .register-form .date-birth-label {\n  margin-left: 66px;\n  margin-bottom: 20px;\n  color: gray;\n}\n\nmat-datepicker-toggle {\n  color: #0871b2 !important;\n  width: 20px;\n  background-color: #0871b2 !important;\n}\n\n.register-page-container {\n  margin-top: 6vw;\n  width: 35%;\n  -webkit-transform: scale(0.85);\n          transform: scale(0.85);\n  -webkit-transform-origin: top left;\n          transform-origin: top left;\n}\n\n.button-container {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-top: 3vh;\n}\n\n.register-btn {\n  width: 100%;\n  border-radius: 50px;\n  border-style: unset;\n  background: #0871b2;\n  padding: 20px;\n  font-size: 20px;\n}\n\n.login-btn {\n  width: 100%;\n  border-style: solid;\n  border-color: #0871b2;\n  color: #0871b2;\n  background: #ffffff;\n  border-radius: 50px;\n  padding: 20px;\n  font-size: 20px;\n}\n\n.or-line {\n  text-align: center;\n  margin: 10px;\n  font-size: 18px;\n}\n\n.or-line .orline {\n  width: 40%;\n}\n\n.common-button {\n  font-size: 16px;\n  border-radius: 5px;\n  display: inline-flex;\n  min-width: 169px;\n  height: 150px;\n  align-items: center;\n  justify-content: center;\n  cursor: pointer;\n  height: 53px;\n  padding: 3px 7px;\n  text-decoration: none !important;\n}\n\n.pg-container {\n  background-size: 100%;\n  background-repeat: no-repeat;\n  position: relative;\n}\n\n/* Container holding the image and the text */\n\n.container-form {\n  position: relative;\n  text-align: left;\n  color: black;\n}\n\n.bottom-container {\n  -webkit-transform: scale(0.85);\n          transform: scale(0.85);\n  margin: 10vh 5vw;\n  box-shadow: 0px 2px 5px 3px rgba(0, 0, 0, 0.1);\n}\n\n.regis-body {\n  position: absolute;\n  top: 8px;\n  left: 16px;\n  display: flex;\n  flex-direction: row;\n}\n\n.regis-body .regis-title {\n  width: 48%;\n  padding: 1em;\n  margin-left: 6%;\n  font-size: 28px;\n}\n\n.regis-body .regis-title p {\n  display: inline;\n  color: #085685;\n}\n\n/* ======================================== */\n\n.text-middle-page {\n  text-align: center;\n  height: 8vw;\n  color: black;\n  font-size: 40px;\n  padding-top: 50px;\n}\n\n.text-middle-page .join-custom-title {\n  border-bottom: 1px solid white;\n  width: 100%;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.middle-regis-page {\n  margin-top: 5%;\n}\n\n.middle-regis-page .col-md-4 {\n  text-align: center;\n  width: 10vw;\n}\n\n.middle-regis-page .col-md-4 .box {\n  border-radius: 22px;\n  width: 100%;\n  height: 22vw;\n  margin: 0 auto;\n  background: #ffffff;\n}\n\n.bottom-page {\n  height: 100px;\n  margin-top: 10vw;\n  text-align: center;\n}\n\n.bottom-page .text {\n  text-align: center;\n  color: black;\n  font-size: 40px;\n  border-top-left-radius: 50px;\n  border-bottom-left-radius: 50px;\n  line-height: 50px;\n  padding: 30px;\n}\n\n.container .row {\n  width: 100%;\n  padding: 5% 0 0 0;\n}\n\n.container .row .img-merchant {\n  width: 100%;\n}\n\n.bottom-title {\n  padding-bottom: 10px;\n  font-size: 30px;\n  margin-bottom: 20px;\n}\n\n.reverse-center {\n  display: flex;\n  flex-direction: row-reverse;\n}\n\n.middle-regis-page .row .col-md-4 .box .row1 {\n  height: 40%;\n  display: block;\n  margin-left: auto;\n  margin-top: auto;\n}\n\n.middle-regis-page .row .col-md-4 .box .row2 {\n  height: 40%;\n  text-align: center;\n}\n\n.middle-regis-page .row .col-md-4 .box .img-box {\n  width: 25%;\n}\n\n.middle-regis-page .row .col-md-4 .box b {\n  font-size: 20px;\n}\n\n.middle-regis-page .row .col-md-4 .box p {\n  margin-left: 10%;\n  margin-top: 0%;\n  margin-right: 10%;\n  font-size: 14px;\n}\n\n.footer {\n  background-image: linear-gradient(#049ed2, #07598a);\n  padding: 60px 30px 0px;\n}\n\n.footer .first-footer-logo {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-bottom: 30px;\n}\n\n.footer .first-footer-logo img {\n  width: 250px;\n}\n\n.footer .first-footer-content {\n  color: white;\n  margin-bottom: 30px;\n}\n\n.footer .first-footer-content .row .content-3 {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n}\n\n.footer .first-footer-content .row .content-3 .social-media {\n  flex-direction: column;\n}\n\n.footer .first-footer-content .row .content-3 .social-media img {\n  width: 40px;\n  margin: 10px;\n}\n\n.footer .first-footer-content .content-4 .application {\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  width: 70%;\n}\n\n.footer .first-footer-content .content-4 .application .up {\n  background-color: #085686;\n  border-top-right-radius: 10px;\n  border-top-left-radius: 10px;\n  padding: 12px 20px;\n  text-align: center;\n  font-size: 19px;\n}\n\n.footer .first-footer-content .content-4 .application .bottom {\n  background-color: white;\n  border-bottom-right-radius: 10px;\n  border-bottom-left-radius: 10px;\n  align-items: center;\n  justify-content: center;\n  display: flex;\n}\n\n.footer .first-footer-content .content-4 .application .bottom img {\n  width: 180px;\n  margin: 10px;\n}\n\n.footer .second-footer {\n  border-top: 1px solid #049ed2;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  padding-top: 20px;\n  padding-bottom: 20px;\n  flex-wrap: wrap;\n}\n\n.footer .second-footer p {\n  margin-bottom: 0px;\n  color: white;\n}\n\n.footer .second-footer img {\n  width: 45px;\n  margin-right: 20px;\n}\n\n@media (min-width: 768px) {\n  .col-md-4 {\n    max-width: 100% !important;\n  }\n}\n\n@media screen and (max-width: 700px) {\n  mat-icon {\n    display: none;\n  }\n\n  .pg-container {\n    background-size: 47cm;\n  }\n\n  .bottom-page .text {\n    font-size: 20px;\n    padding: 5px;\n    width: 100%;\n  }\n\n  .register-page-container {\n    margin-top: 6vw;\n    width: 75%;\n  }\n\n  .bottom-title {\n    font-size: 25px;\n  }\n\n  .middle-regis-page .col-md-4 {\n    width: 100%;\n  }\n\n  #form-regis-img {\n    width: 100vw;\n    display: inline;\n    visibility: hidden;\n    margin-left: -9vw;\n    margin-top: -3vw;\n    height: 20cm;\n  }\n\n  .regis-body {\n    position: absolute;\n    top: 8px;\n    left: 16px;\n    justify-content: center;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n  }\n  .regis-body .regis-title {\n    width: 100%;\n    padding-bottom: 10px;\n    font-size: 20px;\n  }\n\n  #page-regis-img {\n    width: 35%;\n    display: none;\n    margin-left: 10%;\n    margin-bottom: 10%;\n    float: right;\n  }\n\n  .col-md-4 {\n    margin: 10px 0px;\n  }\n  .col-md-4 .row1,\n.col-md-4 .row2 {\n    padding: 10px;\n  }\n\n  .join-custom-title {\n    font-size: 30px;\n    width: 80% !important;\n  }\n\n  .box-middle-page {\n    padding: 0px 5%;\n  }\n\n  .register-form-body {\n    width: 90vw !important;\n    padding: 3% 5% 3%;\n    box-shadow: 0px 2px 5px 3px rgba(0, 0, 0, 0.1);\n  }\n  .register-form-body .form-title {\n    font-size: 26px;\n  }\n  .register-form-body .form-desc {\n    font-size: 15px;\n  }\n  .register-form-body .register-form {\n    display: flex;\n    flex-direction: column;\n    font-size: 18px;\n  }\n  .register-form-body .register-form .date-birth-label {\n    margin-left: 33px;\n    margin-bottom: 20px;\n    color: gray;\n  }\n  .register-form-body .register-form .form-input-container .date-birth-responsive .mat-form-field {\n    width: 30%;\n  }\n  .register-form-body .register-form .form-input-container .date-birth-responsive #mat-form-field {\n    margin-right: 10px;\n    -moz-text-align-last: center;\n         text-align-last: center;\n    width: 100px;\n  }\n\n  .wave-second {\n    display: none;\n  }\n\n  .middle-regis-page .row .col-md-4 .box {\n    width: 70%;\n    height: 100%;\n    margin-left: auto;\n    margin-right: auto;\n  }\n  .middle-regis-page .row .col-md-4 .box .row1 {\n    height: 50%;\n    display: block;\n    margin-left: auto;\n    margin-top: auto;\n    padding: 10px;\n  }\n  .middle-regis-page .row .col-md-4 .box .row2 {\n    height: 50%;\n    text-align: center;\n    padding: 10px;\n  }\n  .middle-regis-page .row .col-md-4 .box .img-box {\n    margin-top: 15%;\n    width: 100px;\n    margin-bottom: 5%;\n  }\n  .middle-regis-page .row .col-md-4 .box b {\n    font-size: 20px;\n  }\n  .middle-regis-page .row .col-md-4 .box p {\n    margin-left: 5%;\n    margin-top: 5%;\n    margin-right: 5%;\n    font-size: 13px;\n  }\n\n  .footer .first-footer-content .content-4 {\n    display: flex;\n    justify-content: center;\n    margin-left: auto;\n    margin-right: auto;\n  }\n  .footer .first-footer-content .content-4 img {\n    width: 230px !important;\n  }\n  .footer .first-footer-content .content-3 {\n    align-items: center;\n    margin-bottom: 10px;\n  }\n  .footer .second-footer img {\n    margin-bottom: 10px !important;\n  }\n  .footer .second-footer p {\n    text-align: center;\n    font-size: 14px;\n  }\n}\n\n@media screen and (min-width: 700px) and (max-width: 800px) {\n  .pg-container {\n    background-size: 43cm;\n  }\n\n  .bottom-page .text {\n    font-size: 22px;\n    padding: 12px;\n    width: 100%;\n  }\n\n  .bottom-title {\n    font-size: 25px;\n  }\n\n  .middle-regis-page .col-md-4 {\n    width: 100%;\n  }\n\n  .register-page-container {\n    margin-top: 6vw;\n    width: 100%;\n    -webkit-transform: scale(0.75);\n            transform: scale(0.75);\n    -webkit-transform-origin: top left;\n            transform-origin: top left;\n  }\n\n  #form-regis-img {\n    width: 55%;\n    display: inline;\n    margin-left: -9vw;\n    margin-top: -3vw;\n    height: 20cm;\n  }\n\n  .regis-body {\n    position: absolute;\n    top: 8px;\n    left: 16px;\n    justify-content: center;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n  }\n  .regis-body .regis-title {\n    width: 100%;\n    padding-bottom: 10px;\n    font-size: 20px;\n  }\n\n  #page-regis-img {\n    width: 40%;\n    display: none;\n    float: right;\n    margin-left: 10%;\n    margin-bottom: 10%;\n  }\n\n  .col-md-4 {\n    margin: 10px 0px;\n  }\n  .col-md-4 .row1,\n.col-md-4 .row2 {\n    padding: 10px;\n  }\n\n  .join-custom-title {\n    font-size: 30px;\n    width: 80% !important;\n  }\n\n  .box-middle-page {\n    margin-top: 5vw;\n  }\n\n  .register-form-body {\n    width: 90vw !important;\n    padding: 3% 5% 3%;\n    box-shadow: 0px 2px 5px 3px rgba(0, 0, 0, 0.1);\n  }\n  .register-form-body .form-title {\n    font-size: 26 px;\n  }\n  .register-form-body .form-desc {\n    font-size: 15px;\n  }\n  .register-form-body .register-form {\n    display: flex;\n    flex-direction: column;\n    font-size: 18px;\n  }\n  .register-form-body .register-form .date-birth-label {\n    margin-left: 66px;\n    margin-bottom: 20px;\n    color: gray;\n  }\n  .register-form-body .register-form .form-input-container .date-birth-responsive .mat-form-field {\n    width: 30%;\n  }\n\n  .wave-second {\n    display: none;\n  }\n\n  .middle-regis-page .row .col-md-4 .box {\n    width: 50%;\n    height: 100%;\n    margin-left: auto;\n    margin-right: auto;\n  }\n  .middle-regis-page .row .col-md-4 .box .row1 {\n    height: 50%;\n    display: block;\n    margin-left: auto;\n    margin-top: auto;\n    padding: 10px;\n  }\n  .middle-regis-page .row .col-md-4 .box .row2 {\n    height: 50%;\n    text-align: center;\n    padding: 10px;\n  }\n  .middle-regis-page .row .col-md-4 .box .img-box {\n    margin-top: 5%;\n    width: 100px;\n    margin-bottom: 5%;\n  }\n  .middle-regis-page .row .col-md-4 .box b {\n    font-size: 20px;\n  }\n  .middle-regis-page .row .col-md-4 .box p {\n    margin-left: 5%;\n    margin-top: 5%;\n    margin-right: 5%;\n    font-size: 13px;\n  }\n\n  .footer .first-footer-content .content-1,\n.footer .first-footer-content .content-2 {\n    text-align: center;\n  }\n  .footer .first-footer-content .content-4 {\n    display: flex;\n    justify-content: center;\n    margin-left: auto;\n    margin-right: auto;\n  }\n  .footer .first-footer-content .content-4 img {\n    width: 230px !important;\n  }\n  .footer .first-footer-content .content-4 .application {\n    width: 41% !important;\n  }\n  .footer .first-footer-content .content-3 {\n    align-items: center;\n    margin-bottom: 10px;\n  }\n  .footer .second-footer img {\n    margin-bottom: 10px !important;\n  }\n  .footer .second-footer p {\n    text-align: center;\n    font-size: 14px;\n  }\n}\n\n@media screen and (min-width: 800px) and (max-width: 1024px) {\n  .pg-container {\n    background-size: 30cm;\n  }\n\n  .bottom-page .text {\n    font-size: 22px;\n    padding: 12px;\n    width: 100%;\n  }\n\n  .bottom-title {\n    font-size: 25px;\n  }\n\n  .middle-regis-page .col-md-4 {\n    width: 100%;\n  }\n\n  #form-regis-img {\n    width: 55%;\n    display: inline;\n    margin-left: -9vw;\n    margin-top: -3vw;\n    height: 20cm;\n  }\n\n  .regis-body {\n    position: absolute;\n    top: 8px;\n    left: 16px;\n  }\n  .regis-body .regis-title {\n    width: 40%;\n    padding-bottom: 10px;\n    font-size: 20px;\n  }\n\n  #page-regis-img {\n    width: 50%;\n    margin-top: 4vw;\n    display: inline;\n  }\n\n  .col-md-4 {\n    margin: 10px 0px;\n  }\n  .col-md-4 .row1,\n.col-md-4 .row2 {\n    padding: 10px;\n  }\n\n  .join-custom-title {\n    font-size: 30px;\n    width: 80% !important;\n  }\n\n  .box-middle-page {\n    margin-top: 0vw;\n  }\n\n  .register-form-body {\n    width: 120vw !important;\n    padding: 3% 5% 3%;\n    box-shadow: 0px 2px 5px 3px rgba(0, 0, 0, 0.1);\n  }\n  .register-form-body .form-title {\n    font-size: 26px;\n  }\n  .register-form-body .form-desc {\n    font-size: 15px;\n  }\n  .register-form-body .register-form {\n    display: flex;\n    flex-direction: column;\n    font-size: 18px;\n  }\n  .register-form-body .register-form .date-birth-label {\n    margin-left: 66px;\n    margin-bottom: 20px;\n    color: gray;\n  }\n  .register-form-body .register-form .form-input-container .date-birth-responsive .mat-form-field {\n    width: 30%;\n  }\n\n  .wave-second {\n    display: none;\n  }\n\n  .middle-regis-page .row .col-md-4 .box {\n    width: 90%;\n    height: 100%;\n    margin-left: auto;\n    margin-right: auto;\n  }\n  .middle-regis-page .row .col-md-4 .box .row1 {\n    height: 50%;\n    display: block;\n    margin-left: auto;\n    margin-top: auto;\n    padding: 10px;\n  }\n  .middle-regis-page .row .col-md-4 .box .row2 {\n    height: 50%;\n    text-align: center;\n    padding: 10px;\n  }\n  .middle-regis-page .row .col-md-4 .box .img-box {\n    margin-top: 10%;\n    width: 100px;\n    margin-bottom: 5%;\n  }\n  .middle-regis-page .row .col-md-4 .box b {\n    font-size: 20px;\n  }\n  .middle-regis-page .row .col-md-4 .box p {\n    margin-left: 5%;\n    margin-top: 5%;\n    margin-right: 5%;\n    font-size: 13px;\n  }\n\n  .footer .first-footer-content .content-1,\n.footer .first-footer-content .content-2 {\n    text-align: center;\n  }\n  .footer .first-footer-content .content-4 {\n    display: flex;\n    justify-content: center;\n    margin-left: auto;\n    margin-right: auto;\n  }\n  .footer .first-footer-content .content-4 img {\n    width: 230px !important;\n  }\n  .footer .first-footer-content .content-4 .application {\n    width: 41% !important;\n  }\n  .footer .first-footer-content .content-3 {\n    align-items: center;\n    margin-bottom: 10px;\n  }\n  .footer .second-footer img {\n    margin-bottom: 10px !important;\n  }\n  .footer .second-footer p {\n    text-align: center;\n    font-size: 14px;\n  }\n}\n\n.modal.false {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n\n.modal.true {\n  display: block;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0px;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: white;\n  /* Black w/ opacity */\n}\n\n/* Modal Header */\n\n.modal-header {\n  padding: 2px 16px;\n  color: white;\n}\n\n/* Modal Body */\n\n.modal-body {\n  padding: 2px 16px;\n}\n\n/* Modal Footer */\n\n.modal-footer {\n  padding: 2px 16px;\n  color: white;\n}\n\n/* Modal Content */\n\n.modal-content {\n  margin-left: 256px;\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  padding: 0;\n  width: 50%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n\n/* The Close Button */\n\n.close {\n  color: white;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n\n.close:hover,\n.close:focus {\n  color: #000;\n  text-decoration: none;\n  cursor: pointer;\n}\n\n/* Add Animation */\n\n@-webkit-keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n\n@keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0ZXIvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxccmVnaXN0ZXJcXHJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtDQUFBO0VBQ0EseUJBQUE7QUNDRjs7QURDQTtFQUNFLG1CQUFBO0VBQ0EsdUJBQUE7QUNFRjs7QURBQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ0dGOztBRERBO0VBQ0UsZ0JBQUE7RUFDQSxVQUFBO0FDSUY7O0FEREE7RUFDRSw4QkFBQTtVQUFBLHNCQUFBO0FDSUY7O0FERkE7RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0FDS0Y7O0FESEE7RUFDRSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ01GOztBREVBO0VBQ0UsWUFBQTtFQUNBLGlCQUFBO0FDQ0Y7O0FESUE7RUFDRSx1QkFBQTtFQUVBLGVBQUE7RUFDQSxpQkFBQTtFQUlBLDhDQUFBO0FDSEY7O0FETUU7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0FDSko7O0FES0k7RUFDRSxpQkFBQTtFQUFrQixtQkFBQTtFQUFvQixXQUFBO0FDRDVDOztBRE1BO0VBQ0UseUJBQUE7RUFDQSxXQUFBO0VBQ0Esb0NBQUE7QUNIRjs7QURVQTtFQUNFLGVBQUE7RUFDQSxVQUFBO0VBQ0EsOEJBQUE7VUFBQSxzQkFBQTtFQUNBLGtDQUFBO1VBQUEsMEJBQUE7QUNQRjs7QURTQTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ05GOztBRFFBO0VBQ0UsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0FDTEY7O0FEUUE7RUFDRSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7QUNMRjs7QURRQTtFQUNFLGtCQUFBO0VBSUEsWUFBQTtFQUNBLGVBQUE7QUNSRjs7QURJRTtFQUNFLFVBQUE7QUNGSjs7QURRQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQ0FBQTtBQ0xGOztBRFFBO0VBRUUscUJBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0FDTkY7O0FEU0EsNkNBQUE7O0FBQ0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQ05GOztBRFFBO0VBQ0UsOEJBQUE7VUFBQSxzQkFBQTtFQUNBLGdCQUFBO0VBR0EsOENBQUE7QUNMRjs7QURPQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUNKRjs7QURNRTtFQUNFLFVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7QUNKSjs7QURLSTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FDSE47O0FET0EsNkNBQUE7O0FBRUE7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDTEY7O0FET0U7RUFDRSw4QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDTEo7O0FEU0E7RUFDRSxjQUFBO0FDTkY7O0FET0U7RUFDRSxrQkFBQTtFQUNBLFdBQUE7QUNMSjs7QURNSTtFQUNFLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUNKTjs7QURTQTtFQUNFLGFBQUE7RUFDQSxnQkFBQTtFQUVBLGtCQUFBO0FDUEY7O0FEUUU7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBR0EsNEJBQUE7RUFDQSwrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtBQ1JKOztBRGFBO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0FDVkY7O0FEV0U7RUFDRSxXQUFBO0FDVEo7O0FEYUE7RUFFRSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ1hGOztBRGNBO0VBQ0UsYUFBQTtFQUNBLDJCQUFBO0FDWEY7O0FEZUU7RUFDRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNaSjs7QURlRTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtBQ2JKOztBRGdCRTtFQUNFLFVBQUE7QUNkSjs7QURrQkU7RUFDRSxlQUFBO0FDaEJKOztBRG1CRTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ2pCSjs7QURxQkE7RUFDRSxtREFBQTtFQUNBLHNCQUFBO0FDbEJGOztBRG9CRTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNsQko7O0FEbUJJO0VBQ0UsWUFBQTtBQ2pCTjs7QURxQkU7RUFDRSxZQUFBO0VBQ0EsbUJBQUE7QUNuQko7O0FEb0JJO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7QUNsQk47O0FEbUJNO0VBQ0Usc0JBQUE7QUNqQlI7O0FEa0JRO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUNoQlY7O0FEc0JNO0VBQ0UsNEVBQUE7RUFDQSxVQUFBO0FDcEJSOztBRHFCUTtFQUNFLHlCQUFBO0VBQ0EsNkJBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDbkJWOztBRHFCUTtFQUNFLHVCQUFBO0VBQ0EsZ0NBQUE7RUFDQSwrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxhQUFBO0FDbkJWOztBRG9CVTtFQUNFLFlBQUE7RUFDQSxZQUFBO0FDbEJaOztBRHlCRTtFQUNFLDZCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZUFBQTtBQ3ZCSjs7QUR3Qkk7RUFDRSxrQkFBQTtFQUNBLFlBQUE7QUN0Qk47O0FEd0JJO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0FDdEJOOztBRDBCQTtFQUNFO0lBQ0UsMEJBQUE7RUN2QkY7QUFDRjs7QUQwQkE7RUFDRTtJQUNFLGFBQUE7RUN4QkY7O0VEMEJBO0lBQ0UscUJBQUE7RUN2QkY7O0VEMEJFO0lBQ0UsZUFBQTtJQUNBLFlBQUE7SUFDQSxXQUFBO0VDdkJKOztFRDBCQTtJQUNFLGVBQUE7SUFDQSxVQUFBO0VDdkJGOztFRDJCQTtJQUNFLGVBQUE7RUN4QkY7O0VEMkJFO0lBQ0UsV0FBQTtFQ3hCSjs7RUQ0QkE7SUFDRSxZQUFBO0lBQ0EsZUFBQTtJQUNBLGtCQUFBO0lBQ0EsaUJBQUE7SUFDQSxnQkFBQTtJQUNBLFlBQUE7RUN6QkY7O0VENEJBO0lBQ0Usa0JBQUE7SUFDQSxRQUFBO0lBQ0EsVUFBQTtJQUVBLHVCQUFBO0lBQ0EsYUFBQTtJQUNBLHNCQUFBO0lBQ0EsbUJBQUE7RUMxQkY7RUQyQkU7SUFDRSxXQUFBO0lBQ0Esb0JBQUE7SUFDQSxlQUFBO0VDekJKOztFRDZCQTtJQUNFLFVBQUE7SUFDQSxhQUFBO0lBQ0EsZ0JBQUE7SUFDQSxrQkFBQTtJQUNBLFlBQUE7RUMxQkY7O0VENkJBO0lBQ0UsZ0JBQUE7RUMxQkY7RUQyQkU7O0lBRUUsYUFBQTtFQ3pCSjs7RUQ0QkE7SUFDRSxlQUFBO0lBQ0EscUJBQUE7RUN6QkY7O0VEMkJBO0lBRUUsZUFBQTtFQ3pCRjs7RUQ0QkE7SUFDRSxzQkFBQTtJQUNBLGlCQUFBO0lBR0EsOENBQUE7RUN6QkY7RUQ0QkU7SUFDRSxlQUFBO0VDMUJKO0VENEJFO0lBQ0UsZUFBQTtFQzFCSjtFRDZCRTtJQUNFLGFBQUE7SUFDQSxzQkFBQTtJQUNBLGVBQUE7RUMzQko7RUQ0Qkk7SUFDRSxpQkFBQTtJQUFrQixtQkFBQTtJQUFvQixXQUFBO0VDeEI1QztFRDZCUTtJQUNFLFVBQUE7RUMzQlY7RUQrQlE7SUFDRSxrQkFBQTtJQUNBLDRCQUFBO1NBQUEsdUJBQUE7SUFDQSxZQUFBO0VDN0JWOztFRG9DQTtJQUNFLGFBQUE7RUNqQ0Y7O0VEb0NBO0lBQ0UsVUFBQTtJQUNBLFlBQUE7SUFDQSxpQkFBQTtJQUNBLGtCQUFBO0VDakNGO0VEa0NFO0lBQ0UsV0FBQTtJQUNBLGNBQUE7SUFDQSxpQkFBQTtJQUNBLGdCQUFBO0lBQ0EsYUFBQTtFQ2hDSjtFRG1DRTtJQUNFLFdBQUE7SUFDQSxrQkFBQTtJQUNBLGFBQUE7RUNqQ0o7RURvQ0U7SUFDRSxlQUFBO0lBQ0EsWUFBQTtJQUNBLGlCQUFBO0VDbENKO0VEcUNFO0lBQ0UsZUFBQTtFQ25DSjtFRHNDRTtJQUNFLGVBQUE7SUFDQSxjQUFBO0lBQ0EsZ0JBQUE7SUFDQSxlQUFBO0VDcENKOztFRHlDSTtJQUNFLGFBQUE7SUFDQSx1QkFBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7RUN0Q047RUR1Q007SUFDRSx1QkFBQTtFQ3JDUjtFRHdDSTtJQUNFLG1CQUFBO0lBQ0EsbUJBQUE7RUN0Q047RUQyQ0k7SUFDRSw4QkFBQTtFQ3pDTjtFRDJDSTtJQUNFLGtCQUFBO0lBQ0EsZUFBQTtFQ3pDTjtBQUNGOztBRDhDQTtFQUNFO0lBQ0UscUJBQUE7RUM1Q0Y7O0VEK0NFO0lBQ0UsZUFBQTtJQUNBLGFBQUE7SUFDQSxXQUFBO0VDNUNKOztFRCtDQTtJQUNFLGVBQUE7RUM1Q0Y7O0VEK0NFO0lBQ0UsV0FBQTtFQzVDSjs7RUQrQ0E7SUFDRSxlQUFBO0lBQ0EsV0FBQTtJQUNBLDhCQUFBO1lBQUEsc0JBQUE7SUFDQSxrQ0FBQTtZQUFBLDBCQUFBO0VDNUNGOztFRDhDQTtJQUNFLFVBQUE7SUFDQSxlQUFBO0lBQ0EsaUJBQUE7SUFDQSxnQkFBQTtJQUNBLFlBQUE7RUMzQ0Y7O0VEOENBO0lBQ0Usa0JBQUE7SUFDQSxRQUFBO0lBQ0EsVUFBQTtJQUNBLHVCQUFBO0lBQ0EsYUFBQTtJQUNBLHNCQUFBO0lBQ0EsbUJBQUE7RUMzQ0Y7RUQ0Q0U7SUFDRSxXQUFBO0lBQ0Esb0JBQUE7SUFDQSxlQUFBO0VDMUNKOztFRDhDQTtJQUNFLFVBQUE7SUFDQSxhQUFBO0lBQ0EsWUFBQTtJQUNBLGdCQUFBO0lBQ0Esa0JBQUE7RUMzQ0Y7O0VEOENBO0lBQ0UsZ0JBQUE7RUMzQ0Y7RUQ0Q0U7O0lBRUUsYUFBQTtFQzFDSjs7RUQ2Q0E7SUFDRSxlQUFBO0lBQ0EscUJBQUE7RUMxQ0Y7O0VENENBO0lBQ0UsZUFBQTtFQ3pDRjs7RUQ0Q0E7SUFDRSxzQkFBQTtJQUNBLGlCQUFBO0lBR0EsOENBQUE7RUN6Q0Y7RUQ0Q0U7SUFDRSxnQkFBQTtFQzFDSjtFRDRDRTtJQUNFLGVBQUE7RUMxQ0o7RUQ2Q0U7SUFDRSxhQUFBO0lBQ0Esc0JBQUE7SUFDQSxlQUFBO0VDM0NKO0VENENJO0lBQ0UsaUJBQUE7SUFBa0IsbUJBQUE7SUFBb0IsV0FBQTtFQ3hDNUM7RUQ4Q1E7SUFDRSxVQUFBO0VDNUNWOztFRGtEQTtJQUNFLGFBQUE7RUMvQ0Y7O0VEa0RBO0lBQ0UsVUFBQTtJQUNBLFlBQUE7SUFDQSxpQkFBQTtJQUNBLGtCQUFBO0VDL0NGO0VEZ0RFO0lBQ0UsV0FBQTtJQUNBLGNBQUE7SUFDQSxpQkFBQTtJQUNBLGdCQUFBO0lBQ0EsYUFBQTtFQzlDSjtFRGlERTtJQUNFLFdBQUE7SUFDQSxrQkFBQTtJQUNBLGFBQUE7RUMvQ0o7RURrREU7SUFDRSxjQUFBO0lBQ0EsWUFBQTtJQUNBLGlCQUFBO0VDaERKO0VEbURFO0lBQ0UsZUFBQTtFQ2pESjtFRG9ERTtJQUNFLGVBQUE7SUFDQSxjQUFBO0lBQ0EsZ0JBQUE7SUFDQSxlQUFBO0VDbERKOztFRHVESTs7SUFFRSxrQkFBQTtFQ3BETjtFRHNESTtJQUNFLGFBQUE7SUFDQSx1QkFBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7RUNwRE47RURxRE07SUFDRSx1QkFBQTtFQ25EUjtFRHFETTtJQUNFLHFCQUFBO0VDbkRSO0VEc0RJO0lBQ0UsbUJBQUE7SUFDQSxtQkFBQTtFQ3BETjtFRHlESTtJQUNFLDhCQUFBO0VDdkROO0VEeURJO0lBQ0Usa0JBQUE7SUFDQSxlQUFBO0VDdkROO0FBQ0Y7O0FENERBO0VBQ0U7SUFDRSxxQkFBQTtFQzFERjs7RUQ2REU7SUFDRSxlQUFBO0lBQ0EsYUFBQTtJQUNBLFdBQUE7RUMxREo7O0VENkRBO0lBQ0UsZUFBQTtFQzFERjs7RUQ2REU7SUFDRSxXQUFBO0VDMURKOztFRDhEQTtJQUNFLFVBQUE7SUFDQSxlQUFBO0lBQ0EsaUJBQUE7SUFDQSxnQkFBQTtJQUNBLFlBQUE7RUMzREY7O0VEOERBO0lBQ0Usa0JBQUE7SUFDQSxRQUFBO0lBQ0EsVUFBQTtFQzNERjtFRDZERTtJQUNFLFVBQUE7SUFDQSxvQkFBQTtJQUNBLGVBQUE7RUMzREo7O0VEK0RBO0lBQ0UsVUFBQTtJQUNBLGVBQUE7SUFHQSxlQUFBO0VDOURGOztFRGlFQTtJQUNFLGdCQUFBO0VDOURGO0VEK0RFOztJQUVFLGFBQUE7RUM3REo7O0VEZ0VBO0lBQ0UsZUFBQTtJQUNBLHFCQUFBO0VDN0RGOztFRCtEQTtJQUNFLGVBQUE7RUM1REY7O0VEK0RBO0lBQ0UsdUJBQUE7SUFFQSxpQkFBQTtJQUdBLDhDQUFBO0VDN0RGO0VEK0RFO0lBQ0UsZUFBQTtFQzdESjtFRCtERTtJQUNFLGVBQUE7RUM3REo7RURnRUU7SUFDRSxhQUFBO0lBQ0Esc0JBQUE7SUFDQSxlQUFBO0VDOURKO0VEK0RJO0lBQ0UsaUJBQUE7SUFBa0IsbUJBQUE7SUFBb0IsV0FBQTtFQzNENUM7RURnRVE7SUFDRSxVQUFBO0VDOURWOztFRHFFQTtJQUNFLGFBQUE7RUNsRUY7O0VEcUVBO0lBQ0UsVUFBQTtJQUNBLFlBQUE7SUFDQSxpQkFBQTtJQUNBLGtCQUFBO0VDbEVGO0VEbUVFO0lBQ0UsV0FBQTtJQUNBLGNBQUE7SUFDQSxpQkFBQTtJQUNBLGdCQUFBO0lBQ0EsYUFBQTtFQ2pFSjtFRG9FRTtJQUNFLFdBQUE7SUFDQSxrQkFBQTtJQUNBLGFBQUE7RUNsRUo7RURxRUU7SUFDRSxlQUFBO0lBQ0EsWUFBQTtJQUNBLGlCQUFBO0VDbkVKO0VEc0VFO0lBQ0UsZUFBQTtFQ3BFSjtFRHVFRTtJQUNFLGVBQUE7SUFDQSxjQUFBO0lBQ0EsZ0JBQUE7SUFDQSxlQUFBO0VDckVKOztFRDBFSTs7SUFFRSxrQkFBQTtFQ3ZFTjtFRHlFSTtJQUNFLGFBQUE7SUFDQSx1QkFBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7RUN2RU47RUR3RU07SUFDRSx1QkFBQTtFQ3RFUjtFRHdFTTtJQUNFLHFCQUFBO0VDdEVSO0VEeUVJO0lBQ0UsbUJBQUE7SUFDQSxtQkFBQTtFQ3ZFTjtFRDRFSTtJQUNFLDhCQUFBO0VDMUVOO0VENEVJO0lBQ0Usa0JBQUE7SUFDQSxlQUFBO0VDMUVOO0FBQ0Y7O0FEK0VBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSw0QkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQ0FBQTtFQUNBLHFCQUFBO0FDN0VKOztBRGdGQTtFQUNJLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUNBLFNBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsNEJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQkFBQTtBQzdFSjs7QURpRkEsaUJBQUE7O0FBQ0E7RUFDSSxpQkFBQTtFQUVBLFlBQUE7QUMvRUo7O0FEa0ZBLGVBQUE7O0FBQ0E7RUFDSSxpQkFBQTtBQy9FSjs7QURrRkEsaUJBQUE7O0FBQ0E7RUFDSSxpQkFBQTtFQUVBLFlBQUE7QUNoRko7O0FEbUZBLGtCQUFBOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFFQSxVQUFBO0VBQ0EsNEVBQUE7RUFDQSxrQ0FBQTtVQUFBLDBCQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLGtCQUFBO0FDakZKOztBRG9GQSxxQkFBQTs7QUFDQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDakZKOztBRG9GQTs7RUFFSSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0FDakZKOztBRHFGQSxrQkFBQTs7QUFDQTtFQUNJO0lBQ0ksV0FBQTtJQUNBLFVBQUE7RUNsRk47RURxRkU7SUFDSSxNQUFBO0lBQ0EsVUFBQTtFQ25GTjtBQUNGOztBRDBFQTtFQUNJO0lBQ0ksV0FBQTtJQUNBLFVBQUE7RUNsRk47RURxRkU7SUFDSSxNQUFBO0lBQ0EsVUFBQTtFQ25GTjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqIHtcclxuICBmb250LWZhbWlseTogXCJQb3BwaW5zXCIsIHNhbnMtc2VyaWY7XHJcbiAgdHJhbnNpdGlvbjogZWFzZSBhbGwgLjNzO1xyXG59XHJcbm1hdC1saXN0LWl0ZW17XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBoZWlnaHQ6IDY1cHggIWltcG9ydGFudDtcclxufVxyXG5ib2R5IHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbiAgcGFkZGluZzogMHB4O1xyXG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcclxufVxyXG4jZm9ybS1yZWdpcy1pbWcge1xyXG4gIG1hcmdpbi10b3A6IDIwdnc7XHJcbiAgd2lkdGg6IDM0JTtcclxuICAvLyBkaXNwbGF5OiBpbmxpbmU7XHJcbn1cclxuLmNvbnRhaW5lciB7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgwLjg1KTtcclxufVxyXG4uZGl2aWRlcntcclxuICB3aWR0aDogMTAlO1xyXG4gIGhlaWdodDphdXRvO1xyXG4gIGRpc3BsYXk6aW5saW5lLWJsb2NrO1xyXG59XHJcbiNwYWdlLXJlZ2lzLWltZyB7XHJcbiAgd2lkdGg6IDUyJTtcclxuICBtYXJnaW4tbGVmdDogOHZoO1xyXG4gIG1hcmdpbi10b3A6IC02dnc7XHJcbiAgLy8gZGlzcGxheTogaW5saW5lO1xyXG59XHJcbi8vIG1hdC1jaGVja2JveCAge1xyXG4vLyAgIGJvcmRlci1jb2xvcjogYmxhY2s7XHJcbi8vICAgYmFja2dyb3VuZC1jb2xvcjogI2RkZGRkZFxyXG4vLyB9XHJcblxyXG4jbG9jYXJkX2ljb25faW1nIHtcclxuICBoZWlnaHQ6IDQ1cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuLy8gLmRvYi1maWVsZCB7XHJcbi8vICAgYm9yZGVyLXRvcDogMHB4ICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuLnJlZ2lzdGVyLWZvcm0tYm9keSB7XHJcbiAgd2lkdGg6IDEwMHZ3ICFpbXBvcnRhbnQ7XHJcbiAgLy8gYm9yZGVyOjFweCBzb2xpZDtcclxuICBtYXJnaW4tdG9wOiAzdmg7XHJcbiAgcGFkZGluZzogMyUgNSUgMyU7XHJcbiAgXHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gIC1tb3otYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gIC8vIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaW1hZ2VzL2Zvcm0tcmVnaXMyLnBuZ1wiKTtcclxuICAvLyBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIC5yZWdpc3Rlci1mb3JtIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgLmRhdGUtYmlydGgtbGFiZWwge1xyXG4gICAgICBtYXJnaW4tbGVmdDogNjZweDttYXJnaW4tYm90dG9tIDoyMHB4O2NvbG9yOmdyYXlcclxuICAgIH1cclxuICAgIFxyXG4gIH1cclxufVxyXG5tYXQtZGF0ZXBpY2tlci10b2dnbGUge1xyXG4gIGNvbG9yOiAjMDg3MWIyICFpbXBvcnRhbnQ7XHJcbiAgd2lkdGg6IDIwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA4NzFiMiAhaW1wb3J0YW50O1xyXG59XHJcbi8vIG1hdC1zZWxlY3Qge1xyXG4vLyAgIG1hdC1vcHRpb24ge1xyXG4vLyAgICAgdHJhbnNmb3JtOiBzY2FsZSgwLjc1KVxyXG4vLyAgIH1cclxuLy8gfVxyXG4ucmVnaXN0ZXItcGFnZS1jb250YWluZXIge1xyXG4gIG1hcmdpbi10b3A6IDZ2dztcclxuICB3aWR0aDogMzUlO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMC44NSk7XHJcbiAgdHJhbnNmb3JtLW9yaWdpbjogdG9wIGxlZnQ7XHJcbn1cclxuLmJ1dHRvbi1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAzdmg7XHJcbn1cclxuLnJlZ2lzdGVyLWJ0biB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICBib3JkZXItc3R5bGU6IHVuc2V0O1xyXG4gIGJhY2tncm91bmQ6ICMwODcxYjI7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuXHJcbi5sb2dpbi1idG4ge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgYm9yZGVyLWNvbG9yOiAjMDg3MWIyO1xyXG4gIGNvbG9yOiAjMDg3MWIyO1xyXG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxufVxyXG5cclxuLm9yLWxpbmUge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAub3JsaW5lIHtcclxuICAgIHdpZHRoOiA0MCU7XHJcbiAgfVxyXG4gIG1hcmdpbjogMTBweDtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuXHJcbi5jb21tb24tYnV0dG9uIHtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gIG1pbi13aWR0aDogMTY5cHg7XHJcbiAgaGVpZ2h0OiAxNTBweDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBoZWlnaHQ6IDUzcHg7XHJcbiAgcGFkZGluZzogM3B4IDdweDtcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnBnLWNvbnRhaW5lciB7XHJcbiAgLy8gYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvY29udGFpbmVyLWZvcm0ucG5nXCIpO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuLyogQ29udGFpbmVyIGhvbGRpbmcgdGhlIGltYWdlIGFuZCB0aGUgdGV4dCAqL1xyXG4uY29udGFpbmVyLWZvcm0ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGNvbG9yOiBibGFjaztcclxufVxyXG4uYm90dG9tLWNvbnRhaW5lciB7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgwLjg1KTtcclxuICBtYXJnaW46IDEwdmggNXZ3O1xyXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICAtbW96LWJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XHJcbiAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxufVxyXG4ucmVnaXMtYm9keSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogOHB4O1xyXG4gIGxlZnQ6IDE2cHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG5cclxuICAucmVnaXMtdGl0bGUge1xyXG4gICAgd2lkdGg6IDQ4JTtcclxuICAgIHBhZGRpbmc6IDFlbTtcclxuICAgIG1hcmdpbi1sZWZ0OiA2JTtcclxuICAgIGZvbnQtc2l6ZTogMjhweDtcclxuICAgIHAge1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmU7XHJcbiAgICAgIGNvbG9yOiAjMDg1Njg1O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4vKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXHJcblxyXG4udGV4dC1taWRkbGUtcGFnZSB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGhlaWdodDogOHZ3O1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBmb250LXNpemU6IDQwcHg7XHJcbiAgcGFkZGluZy10b3A6IDUwcHg7XHJcblxyXG4gIC5qb2luLWN1c3RvbS10aXRsZSB7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIH1cclxufVxyXG5cclxuLm1pZGRsZS1yZWdpcy1wYWdlIHtcclxuICBtYXJnaW4tdG9wOiA1JTtcclxuICAuY29sLW1kLTQge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwdnc7XHJcbiAgICAuYm94IHtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMjJweDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGhlaWdodDogMjJ2dztcclxuICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4uYm90dG9tLXBhZ2Uge1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgbWFyZ2luLXRvcDogMTB2dztcclxuICAvLyBtYXJnaW4tYm90dG9tOiAzdnc7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIC50ZXh0IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgIC8vIHdpZHRoOiA2MCU7XHJcbiAgICAvLyBmbG9hdDogcmlnaHQ7XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1MHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNTBweDtcclxuICAgIGxpbmUtaGVpZ2h0OiA1MHB4O1xyXG4gICAgcGFkZGluZzogMzBweDtcclxuICAgIC8vIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgtOTBkZWcsICMwOTVlOTQsICMwNjk3Y2UpO1xyXG4gIH1cclxufVxyXG5cclxuLmNvbnRhaW5lciAucm93ICB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcGFkZGluZzogNSUgMCAwIDA7XHJcbiAgLmltZy1tZXJjaGFudHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufVxyXG5cclxuLmJvdHRvbS10aXRsZSB7XHJcbiAgLy8gYm9yZGVyLWJvdHRvbTogM3B4IHNvbGlkICMxZjZhOTk7XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5yZXZlcnNlLWNlbnRlciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XHJcbn1cclxuXHJcbi5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IHtcclxuICAucm93MSB7XHJcbiAgICBoZWlnaHQ6IDQwJTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBtYXJnaW4tdG9wOiBhdXRvO1xyXG4gIH1cclxuXHJcbiAgLnJvdzIge1xyXG4gICAgaGVpZ2h0OiA0MCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG5cclxuICAuaW1nLWJveCB7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgLy8gbWFyZ2luLXRvcDogMTUlO1xyXG4gIH1cclxuXHJcbiAgYiB7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgfVxyXG5cclxuICBwIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbiAgICBtYXJnaW4tdG9wOiAwJTtcclxuICAgIG1hcmdpbi1yaWdodDogMTAlO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gIH1cclxufVxyXG5cclxuLmZvb3RlciB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KCMwNDllZDIsICMwNzU5OGEpO1xyXG4gIHBhZGRpbmc6IDYwcHggMzBweCAwcHg7XHJcblxyXG4gIC5maXJzdC1mb290ZXItbG9nbyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiAyNTBweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5maXJzdC1mb290ZXItY29udGVudCB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgLnJvdyAuY29udGVudC0zIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgIC5zb2NpYWwtbWVkaWEge1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgICAgICAgbWFyZ2luOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5jb250ZW50LTQge1xyXG4gICAgICAuYXBwbGljYXRpb24ge1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XHJcbiAgICAgICAgd2lkdGg6IDcwJTtcclxuICAgICAgICAudXAge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzA4NTY4NjtcclxuICAgICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTBweDtcclxuICAgICAgICAgIHBhZGRpbmc6IDEycHggMjBweDtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTlweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmJvdHRvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICB3aWR0aDogMTgwcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMTBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5zZWNvbmQtZm9vdGVyIHtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjMDQ5ZWQyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBwIHtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogNDVweDtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuICAuY29sLW1kLTQge1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3MDBweCkge1xyXG4gIG1hdC1pY29uIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG4gIC5wZy1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiA0N2NtO1xyXG4gIH1cclxuICAuYm90dG9tLXBhZ2Uge1xyXG4gICAgLnRleHQge1xyXG4gICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5yZWdpc3Rlci1wYWdlLWNvbnRhaW5lciB7XHJcbiAgICBtYXJnaW4tdG9wOiA2dnc7XHJcbiAgICB3aWR0aDogNzUlO1xyXG4gICAgLy8gdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICAgIC8vIHRyYW5zZm9ybS1vcmlnaW46IHRvcCBsZWZ0O1xyXG4gIH1cclxuICAuYm90dG9tLXRpdGxlIHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICB9XHJcbiAgLm1pZGRsZS1yZWdpcy1wYWdlIHtcclxuICAgIC5jb2wtbWQtNCB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgI2Zvcm0tcmVnaXMtaW1nIHtcclxuICAgIHdpZHRoOiAxMDB2dztcclxuICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxuICAgIG1hcmdpbi1sZWZ0OiAtOXZ3O1xyXG4gICAgbWFyZ2luLXRvcDogLTN2dztcclxuICAgIGhlaWdodDogMjBjbTtcclxuICB9XHJcblxyXG4gIC5yZWdpcy1ib2R5IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogOHB4O1xyXG4gICAgbGVmdDogMTZweDtcclxuICAgIFxyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAucmVnaXMtdGl0bGUge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gICNwYWdlLXJlZ2lzLWltZyB7XHJcbiAgICB3aWR0aDogMzUlO1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMCU7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgfVxyXG5cclxuICAuY29sLW1kLTQge1xyXG4gICAgbWFyZ2luOiAxMHB4IDBweDtcclxuICAgIC5yb3cxLFxyXG4gICAgLnJvdzIge1xyXG4gICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuam9pbi1jdXN0b20tdGl0bGUge1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgd2lkdGg6IDgwJSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAuYm94LW1pZGRsZS1wYWdlIHtcclxuICAgIC8vIG1hcmdpbi10b3A6IDI1dnc7XHJcbiAgICBwYWRkaW5nOiAwcHggNSU7XHJcbiAgfVxyXG5cclxuICAucmVnaXN0ZXItZm9ybS1ib2R5IHtcclxuICAgIHdpZHRoOiA5MHZ3ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nOiAzJSA1JSAzJTtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICAgIC1tb3otYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICAgIGJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9mb3JtLXJlZ2lzMi5wbmdcIik7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgLmZvcm0tdGl0bGUge1xyXG4gICAgICBmb250LXNpemU6IDI2cHg7XHJcbiAgICB9XHJcbiAgICAuZm9ybS1kZXNjIHtcclxuICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5yZWdpc3Rlci1mb3JtIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAuZGF0ZS1iaXJ0aC1sYWJlbCB7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDMzcHg7bWFyZ2luLWJvdHRvbSA6MjBweDtjb2xvcjpncmF5XHJcbiAgICAgIH1cclxuICAgICAgLmZvcm0taW5wdXQtY29udGFpbmVyIHtcclxuICAgICAgICBcclxuICAgICAgICAuZGF0ZS1iaXJ0aC1yZXNwb25zaXZlIHtcclxuICAgICAgICAgICYgLm1hdC1mb3JtLWZpZWxkIHtcclxuICAgICAgICAgICAgd2lkdGg6IDMwJTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmRhdGUtYmlydGgtcmVzcG9uc2l2ZSB7XHJcbiAgICAgICAgICAjbWF0LWZvcm0tZmllbGQge1xyXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ24tbGFzdDogY2VudGVyO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAud2F2ZS1zZWNvbmQge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcblxyXG4gIC5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IHtcclxuICAgIHdpZHRoOiA3MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIC5yb3cxIHtcclxuICAgICAgaGVpZ2h0OiA1MCU7XHJcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgbWFyZ2luLXRvcDogYXV0bztcclxuICAgICAgcGFkZGluZzogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICAucm93MiB7XHJcbiAgICAgIGhlaWdodDogNTAlO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmltZy1ib3gge1xyXG4gICAgICBtYXJnaW4tdG9wOiAxNSU7XHJcbiAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNSU7XHJcbiAgICB9XHJcblxyXG4gICAgYiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIH1cclxuXHJcbiAgICBwIHtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gICAgICBtYXJnaW4tdG9wOiA1JTtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiA1JTtcclxuICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuZm9vdGVyIHtcclxuICAgIC5maXJzdC1mb290ZXItY29udGVudCB7XHJcbiAgICAgIC5jb250ZW50LTQge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogMjMwcHggIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLmNvbnRlbnQtMyB7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnNlY29uZC1mb290ZXIge1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHggIWltcG9ydGFudDtcclxuICAgICAgfVxyXG4gICAgICBwIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3MDBweCkgYW5kIChtYXgtd2lkdGg6IDgwMHB4KSB7XHJcbiAgLnBnLWNvbnRhaW5lciB7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDQzY207XHJcbiAgfVxyXG4gIC5ib3R0b20tcGFnZSB7XHJcbiAgICAudGV4dCB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgcGFkZGluZzogMTJweDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5ib3R0b20tdGl0bGUge1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gIH1cclxuICAubWlkZGxlLXJlZ2lzLXBhZ2Uge1xyXG4gICAgLmNvbC1tZC00IHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5yZWdpc3Rlci1wYWdlLWNvbnRhaW5lciB7XHJcbiAgICBtYXJnaW4tdG9wOiA2dnc7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHRyYW5zZm9ybTogc2NhbGUoMC43NSk7XHJcbiAgICB0cmFuc2Zvcm0tb3JpZ2luOiB0b3AgbGVmdDtcclxuICB9XHJcbiAgI2Zvcm0tcmVnaXMtaW1nIHtcclxuICAgIHdpZHRoOiA1NSU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmU7XHJcbiAgICBtYXJnaW4tbGVmdDogLTl2dztcclxuICAgIG1hcmdpbi10b3A6IC0zdnc7XHJcbiAgICBoZWlnaHQ6IDIwY207XHJcbiAgfVxyXG5cclxuICAucmVnaXMtYm9keSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDhweDtcclxuICAgIGxlZnQ6IDE2cHg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC5yZWdpcy10aXRsZSB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgI3BhZ2UtcmVnaXMtaW1nIHtcclxuICAgIHdpZHRoOiA0MCU7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwJTtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwJTtcclxuICB9XHJcblxyXG4gIC5jb2wtbWQtNCB7XHJcbiAgICBtYXJnaW46IDEwcHggMHB4O1xyXG4gICAgLnJvdzEsXHJcbiAgICAucm93MiB7XHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5qb2luLWN1c3RvbS10aXRsZSB7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB3aWR0aDogODAlICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIC5ib3gtbWlkZGxlLXBhZ2Uge1xyXG4gICAgbWFyZ2luLXRvcDogNXZ3O1xyXG4gIH1cclxuXHJcbiAgLnJlZ2lzdGVyLWZvcm0tYm9keSB7XHJcbiAgICB3aWR0aDogOTB2dyAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogMyUgNSUgMyU7XHJcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XHJcbiAgICAtbW96LWJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gICAgLy8gYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvZm9ybS1yZWdpczIucG5nXCIpO1xyXG4gICAgLy8gYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIC5mb3JtLXRpdGxlIHtcclxuICAgICAgZm9udC1zaXplOiAyNiBweDtcclxuICAgIH1cclxuICAgIC5mb3JtLWRlc2Mge1xyXG4gICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnJlZ2lzdGVyLWZvcm0ge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgIC5kYXRlLWJpcnRoLWxhYmVsIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNjZweDttYXJnaW4tYm90dG9tIDoyMHB4O2NvbG9yOmdyYXlcclxuICAgICAgfVxyXG4gICAgICAuZm9ybS1pbnB1dC1jb250YWluZXIge1xyXG4gICAgICAgIFxyXG4gICAgICAgXHJcbiAgICAgICAgLmRhdGUtYmlydGgtcmVzcG9uc2l2ZSB7XHJcbiAgICAgICAgICAmIC5tYXQtZm9ybS1maWVsZCB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAzMCU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC53YXZlLXNlY29uZCB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuXHJcbiAgLm1pZGRsZS1yZWdpcy1wYWdlIC5yb3cgLmNvbC1tZC00IC5ib3gge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgLnJvdzEge1xyXG4gICAgICBoZWlnaHQ6IDUwJTtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICBtYXJnaW4tdG9wOiBhdXRvO1xyXG4gICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5yb3cyIHtcclxuICAgICAgaGVpZ2h0OiA1MCU7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgcGFkZGluZzogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICAuaW1nLWJveCB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDUlO1xyXG4gICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDUlO1xyXG4gICAgfVxyXG5cclxuICAgIGIge1xyXG4gICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgcCB7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiA1JTtcclxuICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogNSU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmZvb3RlciB7XHJcbiAgICAuZmlyc3QtZm9vdGVyLWNvbnRlbnQge1xyXG4gICAgICAuY29udGVudC0xLFxyXG4gICAgICAuY29udGVudC0yIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIH1cclxuICAgICAgLmNvbnRlbnQtNCB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyMzBweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYXBwbGljYXRpb24ge1xyXG4gICAgICAgICAgd2lkdGg6IDQxJSAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAuY29udGVudC0zIHtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuc2Vjb25kLWZvb3RlciB7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICAgIHAge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDgwMHB4KSBhbmQgKG1heC13aWR0aDogMTAyNHB4KSB7XHJcbiAgLnBnLWNvbnRhaW5lciB7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDMwY207XHJcbiAgfVxyXG4gIC5ib3R0b20tcGFnZSB7XHJcbiAgICAudGV4dCB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgcGFkZGluZzogMTJweDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5ib3R0b20tdGl0bGUge1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gIH1cclxuICAubWlkZGxlLXJlZ2lzLXBhZ2Uge1xyXG4gICAgLmNvbC1tZC00IHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAjZm9ybS1yZWdpcy1pbWcge1xyXG4gICAgd2lkdGg6IDU1JTtcclxuICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgIG1hcmdpbi1sZWZ0OiAtOXZ3O1xyXG4gICAgbWFyZ2luLXRvcDogLTN2dztcclxuICAgIGhlaWdodDogMjBjbTtcclxuICB9XHJcblxyXG4gIC5yZWdpcy1ib2R5IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogOHB4O1xyXG4gICAgbGVmdDogMTZweDtcclxuXHJcbiAgICAucmVnaXMtdGl0bGUge1xyXG4gICAgICB3aWR0aDogNDAlO1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgI3BhZ2UtcmVnaXMtaW1nIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBtYXJnaW4tdG9wOiA0dnc7XHJcbiAgICAvLyBtYXJnaW4tbGVmdDogMTAlO1xyXG4gICAgLy8gbWFyZ2luLWJvdHRvbTogMTAlO1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gIH1cclxuXHJcbiAgLmNvbC1tZC00IHtcclxuICAgIG1hcmdpbjogMTBweCAwcHg7XHJcbiAgICAucm93MSxcclxuICAgIC5yb3cyIHtcclxuICAgICAgcGFkZGluZzogMTBweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmpvaW4tY3VzdG9tLXRpdGxlIHtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHdpZHRoOiA4MCUgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmJveC1taWRkbGUtcGFnZSB7XHJcbiAgICBtYXJnaW4tdG9wOiAwdnc7XHJcbiAgfVxyXG5cclxuICAucmVnaXN0ZXItZm9ybS1ib2R5IHtcclxuICAgIHdpZHRoOiAxMjB2dyAhaW1wb3J0YW50O1xyXG4gICAgLy8gcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICBwYWRkaW5nOiAzJSA1JSAzJTtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICAgIC1tb3otYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICAgIGJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9mb3JtLXJlZ2lzMi5wbmdcIik7XHJcbiAgICAuZm9ybS10aXRsZSB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMjZweDtcclxuICAgIH1cclxuICAgIC5mb3JtLWRlc2Mge1xyXG4gICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnJlZ2lzdGVyLWZvcm0ge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgIC5kYXRlLWJpcnRoLWxhYmVsIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNjZweDttYXJnaW4tYm90dG9tIDoyMHB4O2NvbG9yOmdyYXlcclxuICAgICAgfVxyXG4gICAgICAuZm9ybS1pbnB1dC1jb250YWluZXIge1xyXG4gICAgICAgIFxyXG4gICAgICAgIC5kYXRlLWJpcnRoLXJlc3BvbnNpdmUge1xyXG4gICAgICAgICAgJiAubWF0LWZvcm0tZmllbGQge1xyXG4gICAgICAgICAgICB3aWR0aDogMzAlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLndhdmUtc2Vjb25kIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAubWlkZGxlLXJlZ2lzLXBhZ2UgLnJvdyAuY29sLW1kLTQgLmJveCB7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICAucm93MSB7XHJcbiAgICAgIGhlaWdodDogNTAlO1xyXG4gICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgIG1hcmdpbi10b3A6IGF1dG87XHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnJvdzIge1xyXG4gICAgICBoZWlnaHQ6IDUwJTtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5pbWctYm94IHtcclxuICAgICAgbWFyZ2luLXRvcDogMTAlO1xyXG4gICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDUlO1xyXG4gICAgfVxyXG5cclxuICAgIGIge1xyXG4gICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgcCB7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiA1JTtcclxuICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogNSU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmZvb3RlciB7XHJcbiAgICAuZmlyc3QtZm9vdGVyLWNvbnRlbnQge1xyXG4gICAgICAuY29udGVudC0xLFxyXG4gICAgICAuY29udGVudC0yIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIH1cclxuICAgICAgLmNvbnRlbnQtNCB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyMzBweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYXBwbGljYXRpb24ge1xyXG4gICAgICAgICAgd2lkdGg6IDQxJSAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAuY29udGVudC0zIHtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuc2Vjb25kLWZvb3RlciB7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICAgIHAge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5tb2RhbC5mYWxzZSB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIC8qIFN0YXkgaW4gcGxhY2UgKi9cclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICAvKiBTaXQgb24gdG9wICovXHJcbiAgICBwYWRkaW5nLXRvcDogMTAwcHg7XHJcbiAgICAvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICAvKiBGdWxsIHdpZHRoICovXHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAvKiBGdWxsIGhlaWdodCAqL1xyXG4gICAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDAsIDAsIDApO1xyXG4gICAgLyogRmFsbGJhY2sgY29sb3IgKi9cclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcclxuICAgIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cclxufVxyXG5cclxuLm1vZGFsLnRydWUge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgLyogU3RheSBpbiBwbGFjZSAqL1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIC8qIFNpdCBvbiB0b3AgKi9cclxuICAgIHBhZGRpbmctdG9wOiAxMDBweDtcclxuICAgIC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cclxuICAgIGxlZnQ6IDBweDtcclxuICAgIHRvcDogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLyogRnVsbCB3aWR0aCAqL1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLyogRnVsbCBoZWlnaHQgKi9cclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgLyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigwLCAwLCAwKTtcclxuICAgIC8qIEZhbGxiYWNrIGNvbG9yICovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cclxufVxyXG5cclxuXHJcbi8qIE1vZGFsIEhlYWRlciAqL1xyXG4ubW9kYWwtaGVhZGVyIHtcclxuICAgIHBhZGRpbmc6IDJweCAxNnB4O1xyXG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzVjYjg1YztcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLyogTW9kYWwgQm9keSAqL1xyXG4ubW9kYWwtYm9keSB7XHJcbiAgICBwYWRkaW5nOiAycHggMTZweDtcclxufVxyXG5cclxuLyogTW9kYWwgRm9vdGVyICovXHJcbi5tb2RhbC1mb290ZXIge1xyXG4gICAgcGFkZGluZzogMnB4IDE2cHg7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjNWNiODVjO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4vKiBNb2RhbCBDb250ZW50ICovXHJcbi5tb2RhbC1jb250ZW50IHtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNTZweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgLy8gYm9yZGVyOiAxcHggc29saWQgIzg4ODtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDAuNHM7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi8qIFRoZSBDbG9zZSBCdXR0b24gKi9cclxuLmNsb3NlIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGZvbnQtc2l6ZTogMjhweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4uY2xvc2U6aG92ZXIsXHJcbi5jbG9zZTpmb2N1cyB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuXHJcbi8qIEFkZCBBbmltYXRpb24gKi9cclxuQGtleWZyYW1lcyBhbmltYXRldG9wIHtcclxuICAgIGZyb20ge1xyXG4gICAgICAgIHRvcDogLTMwMHB4O1xyXG4gICAgICAgIG9wYWNpdHk6IDBcclxuICAgIH1cclxuXHJcbiAgICB0byB7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIG9wYWNpdHk6IDFcclxuICAgIH1cclxufVxyXG4iLCIqIHtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiLCBzYW5zLXNlcmlmO1xuICB0cmFuc2l0aW9uOiBlYXNlIGFsbCAwLjNzO1xufVxuXG5tYXQtbGlzdC1pdGVtIHtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgaGVpZ2h0OiA2NXB4ICFpbXBvcnRhbnQ7XG59XG5cbmJvZHkge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xuICBvdmVyZmxvdy14OiBoaWRkZW47XG59XG5cbiNmb3JtLXJlZ2lzLWltZyB7XG4gIG1hcmdpbi10b3A6IDIwdnc7XG4gIHdpZHRoOiAzNCU7XG59XG5cbi5jb250YWluZXIge1xuICB0cmFuc2Zvcm06IHNjYWxlKDAuODUpO1xufVxuXG4uZGl2aWRlciB7XG4gIHdpZHRoOiAxMCU7XG4gIGhlaWdodDogYXV0bztcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4jcGFnZS1yZWdpcy1pbWcge1xuICB3aWR0aDogNTIlO1xuICBtYXJnaW4tbGVmdDogOHZoO1xuICBtYXJnaW4tdG9wOiAtNnZ3O1xufVxuXG4jbG9jYXJkX2ljb25faW1nIHtcbiAgaGVpZ2h0OiA0NXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLnJlZ2lzdGVyLWZvcm0tYm9keSB7XG4gIHdpZHRoOiAxMDB2dyAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiAzdmg7XG4gIHBhZGRpbmc6IDMlIDUlIDMlO1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gIC1tb3otYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbn1cbi5yZWdpc3Rlci1mb3JtLWJvZHkgLnJlZ2lzdGVyLWZvcm0ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBmb250LXNpemU6IDE3cHg7XG59XG4ucmVnaXN0ZXItZm9ybS1ib2R5IC5yZWdpc3Rlci1mb3JtIC5kYXRlLWJpcnRoLWxhYmVsIHtcbiAgbWFyZ2luLWxlZnQ6IDY2cHg7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gIGNvbG9yOiBncmF5O1xufVxuXG5tYXQtZGF0ZXBpY2tlci10b2dnbGUge1xuICBjb2xvcjogIzA4NzFiMiAhaW1wb3J0YW50O1xuICB3aWR0aDogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA4NzFiMiAhaW1wb3J0YW50O1xufVxuXG4ucmVnaXN0ZXItcGFnZS1jb250YWluZXIge1xuICBtYXJnaW4tdG9wOiA2dnc7XG4gIHdpZHRoOiAzNSU7XG4gIHRyYW5zZm9ybTogc2NhbGUoMC44NSk7XG4gIHRyYW5zZm9ybS1vcmlnaW46IHRvcCBsZWZ0O1xufVxuXG4uYnV0dG9uLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAzdmg7XG59XG5cbi5yZWdpc3Rlci1idG4ge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgYm9yZGVyLXN0eWxlOiB1bnNldDtcbiAgYmFja2dyb3VuZDogIzA4NzFiMjtcbiAgcGFkZGluZzogMjBweDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuXG4ubG9naW4tYnRuIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci1jb2xvcjogIzA4NzFiMjtcbiAgY29sb3I6ICMwODcxYjI7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLm9yLWxpbmUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMTBweDtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuLm9yLWxpbmUgLm9ybGluZSB7XG4gIHdpZHRoOiA0MCU7XG59XG5cbi5jb21tb24tYnV0dG9uIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBtaW4td2lkdGg6IDE2OXB4O1xuICBoZWlnaHQ6IDE1MHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBoZWlnaHQ6IDUzcHg7XG4gIHBhZGRpbmc6IDNweCA3cHg7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4ucGctY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi8qIENvbnRhaW5lciBob2xkaW5nIHRoZSBpbWFnZSBhbmQgdGhlIHRleHQgKi9cbi5jb250YWluZXItZm9ybSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4uYm90dG9tLWNvbnRhaW5lciB7XG4gIHRyYW5zZm9ybTogc2NhbGUoMC44NSk7XG4gIG1hcmdpbjogMTB2aCA1dnc7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCA1cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICBib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xufVxuXG4ucmVnaXMtYm9keSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA4cHg7XG4gIGxlZnQ6IDE2cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG59XG4ucmVnaXMtYm9keSAucmVnaXMtdGl0bGUge1xuICB3aWR0aDogNDglO1xuICBwYWRkaW5nOiAxZW07XG4gIG1hcmdpbi1sZWZ0OiA2JTtcbiAgZm9udC1zaXplOiAyOHB4O1xufVxuLnJlZ2lzLWJvZHkgLnJlZ2lzLXRpdGxlIHAge1xuICBkaXNwbGF5OiBpbmxpbmU7XG4gIGNvbG9yOiAjMDg1Njg1O1xufVxuXG4vKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG4udGV4dC1taWRkbGUtcGFnZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgaGVpZ2h0OiA4dnc7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiA0MHB4O1xuICBwYWRkaW5nLXRvcDogNTBweDtcbn1cbi50ZXh0LW1pZGRsZS1wYWdlIC5qb2luLWN1c3RvbS10aXRsZSB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG5cbi5taWRkbGUtcmVnaXMtcGFnZSB7XG4gIG1hcmdpbi10b3A6IDUlO1xufVxuLm1pZGRsZS1yZWdpcy1wYWdlIC5jb2wtbWQtNCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDEwdnc7XG59XG4ubWlkZGxlLXJlZ2lzLXBhZ2UgLmNvbC1tZC00IC5ib3gge1xuICBib3JkZXItcmFkaXVzOiAyMnB4O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyMnZ3O1xuICBtYXJnaW46IDAgYXV0bztcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLmJvdHRvbS1wYWdlIHtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgbWFyZ2luLXRvcDogMTB2dztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJvdHRvbS1wYWdlIC50ZXh0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogNDBweDtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogNTBweDtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNTBweDtcbiAgbGluZS1oZWlnaHQ6IDUwcHg7XG4gIHBhZGRpbmc6IDMwcHg7XG59XG5cbi5jb250YWluZXIgLnJvdyB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiA1JSAwIDAgMDtcbn1cbi5jb250YWluZXIgLnJvdyAuaW1nLW1lcmNoYW50IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5ib3R0b20tdGl0bGUge1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4ucmV2ZXJzZS1jZW50ZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XG59XG5cbi5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IC5yb3cxIHtcbiAgaGVpZ2h0OiA0MCU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXRvcDogYXV0bztcbn1cbi5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IC5yb3cyIHtcbiAgaGVpZ2h0OiA0MCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IC5pbWctYm94IHtcbiAgd2lkdGg6IDI1JTtcbn1cbi5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IGIge1xuICBmb250LXNpemU6IDIwcHg7XG59XG4ubWlkZGxlLXJlZ2lzLXBhZ2UgLnJvdyAuY29sLW1kLTQgLmJveCBwIHtcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgbWFyZ2luLXRvcDogMCU7XG4gIG1hcmdpbi1yaWdodDogMTAlO1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5mb290ZXIge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzA0OWVkMiwgIzA3NTk4YSk7XG4gIHBhZGRpbmc6IDYwcHggMzBweCAwcHg7XG59XG4uZm9vdGVyIC5maXJzdC1mb290ZXItbG9nbyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuLmZvb3RlciAuZmlyc3QtZm9vdGVyLWxvZ28gaW1nIHtcbiAgd2lkdGg6IDI1MHB4O1xufVxuLmZvb3RlciAuZmlyc3QtZm9vdGVyLWNvbnRlbnQge1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG4uZm9vdGVyIC5maXJzdC1mb290ZXItY29udGVudCAucm93IC5jb250ZW50LTMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cbi5mb290ZXIgLmZpcnN0LWZvb3Rlci1jb250ZW50IC5yb3cgLmNvbnRlbnQtMyAuc29jaWFsLW1lZGlhIHtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cbi5mb290ZXIgLmZpcnN0LWZvb3Rlci1jb250ZW50IC5yb3cgLmNvbnRlbnQtMyAuc29jaWFsLW1lZGlhIGltZyB7XG4gIHdpZHRoOiA0MHB4O1xuICBtYXJnaW46IDEwcHg7XG59XG4uZm9vdGVyIC5maXJzdC1mb290ZXItY29udGVudCAuY29udGVudC00IC5hcHBsaWNhdGlvbiB7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG4gIHdpZHRoOiA3MCU7XG59XG4uZm9vdGVyIC5maXJzdC1mb290ZXItY29udGVudCAuY29udGVudC00IC5hcHBsaWNhdGlvbiAudXAge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDg1Njg2O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTBweDtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTBweDtcbiAgcGFkZGluZzogMTJweCAyMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTlweDtcbn1cbi5mb290ZXIgLmZpcnN0LWZvb3Rlci1jb250ZW50IC5jb250ZW50LTQgLmFwcGxpY2F0aW9uIC5ib3R0b20ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLmZvb3RlciAuZmlyc3QtZm9vdGVyLWNvbnRlbnQgLmNvbnRlbnQtNCAuYXBwbGljYXRpb24gLmJvdHRvbSBpbWcge1xuICB3aWR0aDogMTgwcHg7XG4gIG1hcmdpbjogMTBweDtcbn1cbi5mb290ZXIgLnNlY29uZC1mb290ZXIge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgIzA0OWVkMjtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgZmxleC13cmFwOiB3cmFwO1xufVxuLmZvb3RlciAuc2Vjb25kLWZvb3RlciBwIHtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogd2hpdGU7XG59XG4uZm9vdGVyIC5zZWNvbmQtZm9vdGVyIGltZyB7XG4gIHdpZHRoOiA0NXB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAuY29sLW1kLTQge1xuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3MDBweCkge1xuICBtYXQtaWNvbiB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIC5wZy1jb250YWluZXIge1xuICAgIGJhY2tncm91bmQtc2l6ZTogNDdjbTtcbiAgfVxuXG4gIC5ib3R0b20tcGFnZSAudGV4dCB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5yZWdpc3Rlci1wYWdlLWNvbnRhaW5lciB7XG4gICAgbWFyZ2luLXRvcDogNnZ3O1xuICAgIHdpZHRoOiA3NSU7XG4gIH1cblxuICAuYm90dG9tLXRpdGxlIHtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gIH1cblxuICAubWlkZGxlLXJlZ2lzLXBhZ2UgLmNvbC1tZC00IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gICNmb3JtLXJlZ2lzLWltZyB7XG4gICAgd2lkdGg6IDEwMHZ3O1xuICAgIGRpc3BsYXk6IGlubGluZTtcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gICAgbWFyZ2luLWxlZnQ6IC05dnc7XG4gICAgbWFyZ2luLXRvcDogLTN2dztcbiAgICBoZWlnaHQ6IDIwY207XG4gIH1cblxuICAucmVnaXMtYm9keSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogOHB4O1xuICAgIGxlZnQ6IDE2cHg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cbiAgLnJlZ2lzLWJvZHkgLnJlZ2lzLXRpdGxlIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gIH1cblxuICAjcGFnZS1yZWdpcy1pbWcge1xuICAgIHdpZHRoOiAzNSU7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBtYXJnaW4tbGVmdDogMTAlO1xuICAgIG1hcmdpbi1ib3R0b206IDEwJTtcbiAgICBmbG9hdDogcmlnaHQ7XG4gIH1cblxuICAuY29sLW1kLTQge1xuICAgIG1hcmdpbjogMTBweCAwcHg7XG4gIH1cbiAgLmNvbC1tZC00IC5yb3cxLFxuLmNvbC1tZC00IC5yb3cyIHtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICB9XG5cbiAgLmpvaW4tY3VzdG9tLXRpdGxlIHtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgd2lkdGg6IDgwJSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmJveC1taWRkbGUtcGFnZSB7XG4gICAgcGFkZGluZzogMHB4IDUlO1xuICB9XG5cbiAgLnJlZ2lzdGVyLWZvcm0tYm9keSB7XG4gICAgd2lkdGg6IDkwdncgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAzJSA1JSAzJTtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgLW1vei1ib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gIH1cbiAgLnJlZ2lzdGVyLWZvcm0tYm9keSAuZm9ybS10aXRsZSB7XG4gICAgZm9udC1zaXplOiAyNnB4O1xuICB9XG4gIC5yZWdpc3Rlci1mb3JtLWJvZHkgLmZvcm0tZGVzYyB7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICB9XG4gIC5yZWdpc3Rlci1mb3JtLWJvZHkgLnJlZ2lzdGVyLWZvcm0ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gIH1cbiAgLnJlZ2lzdGVyLWZvcm0tYm9keSAucmVnaXN0ZXItZm9ybSAuZGF0ZS1iaXJ0aC1sYWJlbCB7XG4gICAgbWFyZ2luLWxlZnQ6IDMzcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBjb2xvcjogZ3JheTtcbiAgfVxuICAucmVnaXN0ZXItZm9ybS1ib2R5IC5yZWdpc3Rlci1mb3JtIC5mb3JtLWlucHV0LWNvbnRhaW5lciAuZGF0ZS1iaXJ0aC1yZXNwb25zaXZlIC5tYXQtZm9ybS1maWVsZCB7XG4gICAgd2lkdGg6IDMwJTtcbiAgfVxuICAucmVnaXN0ZXItZm9ybS1ib2R5IC5yZWdpc3Rlci1mb3JtIC5mb3JtLWlucHV0LWNvbnRhaW5lciAuZGF0ZS1iaXJ0aC1yZXNwb25zaXZlICNtYXQtZm9ybS1maWVsZCB7XG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgIHRleHQtYWxpZ24tbGFzdDogY2VudGVyO1xuICAgIHdpZHRoOiAxMDBweDtcbiAgfVxuXG4gIC53YXZlLXNlY29uZCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIC5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IHtcbiAgICB3aWR0aDogNzAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIH1cbiAgLm1pZGRsZS1yZWdpcy1wYWdlIC5yb3cgLmNvbC1tZC00IC5ib3ggLnJvdzEge1xuICAgIGhlaWdodDogNTAlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi10b3A6IGF1dG87XG4gICAgcGFkZGluZzogMTBweDtcbiAgfVxuICAubWlkZGxlLXJlZ2lzLXBhZ2UgLnJvdyAuY29sLW1kLTQgLmJveCAucm93MiB7XG4gICAgaGVpZ2h0OiA1MCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gIH1cbiAgLm1pZGRsZS1yZWdpcy1wYWdlIC5yb3cgLmNvbC1tZC00IC5ib3ggLmltZy1ib3gge1xuICAgIG1hcmdpbi10b3A6IDE1JTtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNSU7XG4gIH1cbiAgLm1pZGRsZS1yZWdpcy1wYWdlIC5yb3cgLmNvbC1tZC00IC5ib3ggYiB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICB9XG4gIC5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IHAge1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICBtYXJnaW4tdG9wOiA1JTtcbiAgICBtYXJnaW4tcmlnaHQ6IDUlO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgfVxuXG4gIC5mb290ZXIgLmZpcnN0LWZvb3Rlci1jb250ZW50IC5jb250ZW50LTQge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICB9XG4gIC5mb290ZXIgLmZpcnN0LWZvb3Rlci1jb250ZW50IC5jb250ZW50LTQgaW1nIHtcbiAgICB3aWR0aDogMjMwcHggIWltcG9ydGFudDtcbiAgfVxuICAuZm9vdGVyIC5maXJzdC1mb290ZXItY29udGVudCAuY29udGVudC0zIHtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIH1cbiAgLmZvb3RlciAuc2Vjb25kLWZvb3RlciBpbWcge1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHggIWltcG9ydGFudDtcbiAgfVxuICAuZm9vdGVyIC5zZWNvbmQtZm9vdGVyIHAge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDcwMHB4KSBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcbiAgLnBnLWNvbnRhaW5lciB7XG4gICAgYmFja2dyb3VuZC1zaXplOiA0M2NtO1xuICB9XG5cbiAgLmJvdHRvbS1wYWdlIC50ZXh0IHtcbiAgICBmb250LXNpemU6IDIycHg7XG4gICAgcGFkZGluZzogMTJweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5ib3R0b20tdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgfVxuXG4gIC5taWRkbGUtcmVnaXMtcGFnZSAuY29sLW1kLTQge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG5cbiAgLnJlZ2lzdGVyLXBhZ2UtY29udGFpbmVyIHtcbiAgICBtYXJnaW4tdG9wOiA2dnc7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwLjc1KTtcbiAgICB0cmFuc2Zvcm0tb3JpZ2luOiB0b3AgbGVmdDtcbiAgfVxuXG4gICNmb3JtLXJlZ2lzLWltZyB7XG4gICAgd2lkdGg6IDU1JTtcbiAgICBkaXNwbGF5OiBpbmxpbmU7XG4gICAgbWFyZ2luLWxlZnQ6IC05dnc7XG4gICAgbWFyZ2luLXRvcDogLTN2dztcbiAgICBoZWlnaHQ6IDIwY207XG4gIH1cblxuICAucmVnaXMtYm9keSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogOHB4O1xuICAgIGxlZnQ6IDE2cHg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cbiAgLnJlZ2lzLWJvZHkgLnJlZ2lzLXRpdGxlIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gIH1cblxuICAjcGFnZS1yZWdpcy1pbWcge1xuICAgIHdpZHRoOiA0MCU7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgICBtYXJnaW4tYm90dG9tOiAxMCU7XG4gIH1cblxuICAuY29sLW1kLTQge1xuICAgIG1hcmdpbjogMTBweCAwcHg7XG4gIH1cbiAgLmNvbC1tZC00IC5yb3cxLFxuLmNvbC1tZC00IC5yb3cyIHtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICB9XG5cbiAgLmpvaW4tY3VzdG9tLXRpdGxlIHtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgd2lkdGg6IDgwJSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmJveC1taWRkbGUtcGFnZSB7XG4gICAgbWFyZ2luLXRvcDogNXZ3O1xuICB9XG5cbiAgLnJlZ2lzdGVyLWZvcm0tYm9keSB7XG4gICAgd2lkdGg6IDkwdncgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAzJSA1JSAzJTtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgLW1vei1ib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gIH1cbiAgLnJlZ2lzdGVyLWZvcm0tYm9keSAuZm9ybS10aXRsZSB7XG4gICAgZm9udC1zaXplOiAyNiBweDtcbiAgfVxuICAucmVnaXN0ZXItZm9ybS1ib2R5IC5mb3JtLWRlc2Mge1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgfVxuICAucmVnaXN0ZXItZm9ybS1ib2R5IC5yZWdpc3Rlci1mb3JtIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICB9XG4gIC5yZWdpc3Rlci1mb3JtLWJvZHkgLnJlZ2lzdGVyLWZvcm0gLmRhdGUtYmlydGgtbGFiZWwge1xuICAgIG1hcmdpbi1sZWZ0OiA2NnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgY29sb3I6IGdyYXk7XG4gIH1cbiAgLnJlZ2lzdGVyLWZvcm0tYm9keSAucmVnaXN0ZXItZm9ybSAuZm9ybS1pbnB1dC1jb250YWluZXIgLmRhdGUtYmlydGgtcmVzcG9uc2l2ZSAubWF0LWZvcm0tZmllbGQge1xuICAgIHdpZHRoOiAzMCU7XG4gIH1cblxuICAud2F2ZS1zZWNvbmQge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAubWlkZGxlLXJlZ2lzLXBhZ2UgLnJvdyAuY29sLW1kLTQgLmJveCB7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICB9XG4gIC5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IC5yb3cxIHtcbiAgICBoZWlnaHQ6IDUwJTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tdG9wOiBhdXRvO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gIH1cbiAgLm1pZGRsZS1yZWdpcy1wYWdlIC5yb3cgLmNvbC1tZC00IC5ib3ggLnJvdzIge1xuICAgIGhlaWdodDogNTAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICB9XG4gIC5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IC5pbWctYm94IHtcbiAgICBtYXJnaW4tdG9wOiA1JTtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNSU7XG4gIH1cbiAgLm1pZGRsZS1yZWdpcy1wYWdlIC5yb3cgLmNvbC1tZC00IC5ib3ggYiB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICB9XG4gIC5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IHAge1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICBtYXJnaW4tdG9wOiA1JTtcbiAgICBtYXJnaW4tcmlnaHQ6IDUlO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgfVxuXG4gIC5mb290ZXIgLmZpcnN0LWZvb3Rlci1jb250ZW50IC5jb250ZW50LTEsXG4uZm9vdGVyIC5maXJzdC1mb290ZXItY29udGVudCAuY29udGVudC0yIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmZvb3RlciAuZmlyc3QtZm9vdGVyLWNvbnRlbnQgLmNvbnRlbnQtNCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIH1cbiAgLmZvb3RlciAuZmlyc3QtZm9vdGVyLWNvbnRlbnQgLmNvbnRlbnQtNCBpbWcge1xuICAgIHdpZHRoOiAyMzBweCAhaW1wb3J0YW50O1xuICB9XG4gIC5mb290ZXIgLmZpcnN0LWZvb3Rlci1jb250ZW50IC5jb250ZW50LTQgLmFwcGxpY2F0aW9uIHtcbiAgICB3aWR0aDogNDElICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmZvb3RlciAuZmlyc3QtZm9vdGVyLWNvbnRlbnQgLmNvbnRlbnQtMyB7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICB9XG4gIC5mb290ZXIgLnNlY29uZC1mb290ZXIgaW1nIHtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4ICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmZvb3RlciAuc2Vjb25kLWZvb3RlciBwIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA4MDBweCkgYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xuICAucGctY29udGFpbmVyIHtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDMwY207XG4gIH1cblxuICAuYm90dG9tLXBhZ2UgLnRleHQge1xuICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICBwYWRkaW5nOiAxMnB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG5cbiAgLmJvdHRvbS10aXRsZSB7XG4gICAgZm9udC1zaXplOiAyNXB4O1xuICB9XG5cbiAgLm1pZGRsZS1yZWdpcy1wYWdlIC5jb2wtbWQtNCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cblxuICAjZm9ybS1yZWdpcy1pbWcge1xuICAgIHdpZHRoOiA1NSU7XG4gICAgZGlzcGxheTogaW5saW5lO1xuICAgIG1hcmdpbi1sZWZ0OiAtOXZ3O1xuICAgIG1hcmdpbi10b3A6IC0zdnc7XG4gICAgaGVpZ2h0OiAyMGNtO1xuICB9XG5cbiAgLnJlZ2lzLWJvZHkge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDhweDtcbiAgICBsZWZ0OiAxNnB4O1xuICB9XG4gIC5yZWdpcy1ib2R5IC5yZWdpcy10aXRsZSB7XG4gICAgd2lkdGg6IDQwJTtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gIH1cblxuICAjcGFnZS1yZWdpcy1pbWcge1xuICAgIHdpZHRoOiA1MCU7XG4gICAgbWFyZ2luLXRvcDogNHZ3O1xuICAgIGRpc3BsYXk6IGlubGluZTtcbiAgfVxuXG4gIC5jb2wtbWQtNCB7XG4gICAgbWFyZ2luOiAxMHB4IDBweDtcbiAgfVxuICAuY29sLW1kLTQgLnJvdzEsXG4uY29sLW1kLTQgLnJvdzIge1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gIH1cblxuICAuam9pbi1jdXN0b20tdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICB3aWR0aDogODAlICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuYm94LW1pZGRsZS1wYWdlIHtcbiAgICBtYXJnaW4tdG9wOiAwdnc7XG4gIH1cblxuICAucmVnaXN0ZXItZm9ybS1ib2R5IHtcbiAgICB3aWR0aDogMTIwdncgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAzJSA1JSAzJTtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgLW1vei1ib3gtc2hhZG93OiAwcHggMnB4IDVweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGJveC1zaGFkb3c6IDBweCAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gIH1cbiAgLnJlZ2lzdGVyLWZvcm0tYm9keSAuZm9ybS10aXRsZSB7XG4gICAgZm9udC1zaXplOiAyNnB4O1xuICB9XG4gIC5yZWdpc3Rlci1mb3JtLWJvZHkgLmZvcm0tZGVzYyB7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICB9XG4gIC5yZWdpc3Rlci1mb3JtLWJvZHkgLnJlZ2lzdGVyLWZvcm0ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gIH1cbiAgLnJlZ2lzdGVyLWZvcm0tYm9keSAucmVnaXN0ZXItZm9ybSAuZGF0ZS1iaXJ0aC1sYWJlbCB7XG4gICAgbWFyZ2luLWxlZnQ6IDY2cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBjb2xvcjogZ3JheTtcbiAgfVxuICAucmVnaXN0ZXItZm9ybS1ib2R5IC5yZWdpc3Rlci1mb3JtIC5mb3JtLWlucHV0LWNvbnRhaW5lciAuZGF0ZS1iaXJ0aC1yZXNwb25zaXZlIC5tYXQtZm9ybS1maWVsZCB7XG4gICAgd2lkdGg6IDMwJTtcbiAgfVxuXG4gIC53YXZlLXNlY29uZCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIC5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIH1cbiAgLm1pZGRsZS1yZWdpcy1wYWdlIC5yb3cgLmNvbC1tZC00IC5ib3ggLnJvdzEge1xuICAgIGhlaWdodDogNTAlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi10b3A6IGF1dG87XG4gICAgcGFkZGluZzogMTBweDtcbiAgfVxuICAubWlkZGxlLXJlZ2lzLXBhZ2UgLnJvdyAuY29sLW1kLTQgLmJveCAucm93MiB7XG4gICAgaGVpZ2h0OiA1MCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gIH1cbiAgLm1pZGRsZS1yZWdpcy1wYWdlIC5yb3cgLmNvbC1tZC00IC5ib3ggLmltZy1ib3gge1xuICAgIG1hcmdpbi10b3A6IDEwJTtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNSU7XG4gIH1cbiAgLm1pZGRsZS1yZWdpcy1wYWdlIC5yb3cgLmNvbC1tZC00IC5ib3ggYiB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICB9XG4gIC5taWRkbGUtcmVnaXMtcGFnZSAucm93IC5jb2wtbWQtNCAuYm94IHAge1xuICAgIG1hcmdpbi1sZWZ0OiA1JTtcbiAgICBtYXJnaW4tdG9wOiA1JTtcbiAgICBtYXJnaW4tcmlnaHQ6IDUlO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgfVxuXG4gIC5mb290ZXIgLmZpcnN0LWZvb3Rlci1jb250ZW50IC5jb250ZW50LTEsXG4uZm9vdGVyIC5maXJzdC1mb290ZXItY29udGVudCAuY29udGVudC0yIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmZvb3RlciAuZmlyc3QtZm9vdGVyLWNvbnRlbnQgLmNvbnRlbnQtNCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIH1cbiAgLmZvb3RlciAuZmlyc3QtZm9vdGVyLWNvbnRlbnQgLmNvbnRlbnQtNCBpbWcge1xuICAgIHdpZHRoOiAyMzBweCAhaW1wb3J0YW50O1xuICB9XG4gIC5mb290ZXIgLmZpcnN0LWZvb3Rlci1jb250ZW50IC5jb250ZW50LTQgLmFwcGxpY2F0aW9uIHtcbiAgICB3aWR0aDogNDElICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmZvb3RlciAuZmlyc3QtZm9vdGVyLWNvbnRlbnQgLmNvbnRlbnQtMyB7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICB9XG4gIC5mb290ZXIgLnNlY29uZC1mb290ZXIgaW1nIHtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4ICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmZvb3RlciAuc2Vjb25kLWZvb3RlciBwIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICB9XG59XG4ubW9kYWwuZmFsc2Uge1xuICBkaXNwbGF5OiBub25lO1xuICAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgei1pbmRleDogMTtcbiAgLyogU2l0IG9uIHRvcCAqL1xuICBwYWRkaW5nLXRvcDogMTAwcHg7XG4gIC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgLyogRnVsbCB3aWR0aCAqL1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIEZ1bGwgaGVpZ2h0ICovXG4gIG92ZXJmbG93OiBhdXRvO1xuICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG59XG5cbi5tb2RhbC50cnVlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xuICB6LWluZGV4OiAxO1xuICAvKiBTaXQgb24gdG9wICovXG4gIHBhZGRpbmctdG9wOiAxMDBweDtcbiAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xuICBsZWZ0OiAwcHg7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIC8qIEZ1bGwgd2lkdGggKi9cbiAgaGVpZ2h0OiAxMDAlO1xuICAvKiBGdWxsIGhlaWdodCAqL1xuICBvdmVyZmxvdzogYXV0bztcbiAgLyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIC8qIEZhbGxiYWNrIGNvbG9yICovXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG59XG5cbi8qIE1vZGFsIEhlYWRlciAqL1xuLm1vZGFsLWhlYWRlciB7XG4gIHBhZGRpbmc6IDJweCAxNnB4O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi8qIE1vZGFsIEJvZHkgKi9cbi5tb2RhbC1ib2R5IHtcbiAgcGFkZGluZzogMnB4IDE2cHg7XG59XG5cbi8qIE1vZGFsIEZvb3RlciAqL1xuLm1vZGFsLWZvb3RlciB7XG4gIHBhZGRpbmc6IDJweCAxNnB4O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi8qIE1vZGFsIENvbnRlbnQgKi9cbi5tb2RhbC1jb250ZW50IHtcbiAgbWFyZ2luLWxlZnQ6IDI1NnB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XG4gIG1hcmdpbjogYXV0bztcbiAgcGFkZGluZzogMDtcbiAgd2lkdGg6IDUwJTtcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vKiBUaGUgQ2xvc2UgQnV0dG9uICovXG4uY2xvc2Uge1xuICBjb2xvcjogd2hpdGU7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAyOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmNsb3NlOmhvdmVyLFxuLmNsb3NlOmZvY3VzIHtcbiAgY29sb3I6ICMwMDA7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4vKiBBZGQgQW5pbWF0aW9uICovXG5Aa2V5ZnJhbWVzIGFuaW1hdGV0b3Age1xuICBmcm9tIHtcbiAgICB0b3A6IC0zMDBweDtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG4gIHRvIHtcbiAgICB0b3A6IDA7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_register_register_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/register/register.service */ "./src/app/services/register/register.service.ts");
/* harmony import */ var _services_login_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/login/login.service */ "./src/app/services/login/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(registerService, icon, memberService, loginService, router, route, formBuilder) {
        this.registerService = registerService;
        this.icon = icon;
        this.memberService = memberService;
        this.loginService = loginService;
        this.router = router;
        this.route = route;
        this.formBuilder = formBuilder;
        this.email = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]);
        this.hide = true;
        this.showLoading = false;
        this.showTnc = false;
        this.form = {
            full_name: "",
            email: "",
            cell_phone: "",
            password: "",
            gender: "",
            dob: ""
        };
        this.data = [];
        this.day = "";
        this.month = "";
        this.year = "";
        this.dayArray = [];
        this.monthArray = [];
        this.yearArray = [];
    }
    RegisterComponent.prototype.getErrorMessage = function () {
        return this.email.hasError('required') ? 'You must enter a value' :
            this.email.hasError('email') ? 'Not a valid email' :
                '';
    };
    RegisterComponent.prototype.ngOnInit = function () {
        this.dobCount();
        this.registerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            full_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(255)]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]),
            cell_phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(8)]),
            gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            dob: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            checked: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].requiredTrue]),
        });
    };
    // toggleDOBField(type) {
    //   this.DOBField.nativeElement.type = type;
    // }
    RegisterComponent.prototype.dobCount = function () {
        for (var i = 1; i <= 31; i++) {
            if (i < 10) {
                this.dayArray.push({ value: '0' + i });
            }
            else {
                this.dayArray.push({ value: i });
            }
        }
        for (var i = 1; i <= 12; i++) {
            if (i < 10) {
                this.monthArray.push({ value: '0' + i });
            }
            else {
                this.monthArray.push({ value: i });
            }
        }
        for (var i = 1940; i <= 2015; i++) {
            this.yearArray.push({ value: i });
        }
    };
    RegisterComponent.prototype.register = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, cell, result, login_data, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.showLoading = true;
                        console.log("Before", this.form);
                        data = this.form;
                        this.form.dob = new _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]('en').transform(this.form.dob, 'yyyy-MM-dd');
                        // this.form.dob = this.year+"-"+this.month+"-"+this.day;
                        console.log('date', this.form.dob);
                        // let cellPhone = this.form.cell_phone;
                        // String(cellPhone);
                        console.log("Before Num", typeof this.form.cell_phone);
                        cell = this.form.cell_phone.toString();
                        this.form.cell_phone = cell;
                        // console.log("TEST" typeof this.form.cell_phone)
                        // this.form.cell_phone = toString();
                        // form.value.push(this.dob)
                        console.log("After Num", typeof this.form.cell_phone);
                        console.log(this.form.dob);
                        console.log("After", this.form);
                        if (!this.registerForm.invalid) return [3 /*break*/, 1];
                        console.log('ERROR BRAY');
                        this.errorMessage = "Please fill the form and indicate that you have read and agree to the Terms and Conditions";
                        this.showLoading = false;
                        return [3 /*break*/, 7];
                    case 1:
                        _a.trys.push([1, 6, , 7]);
                        return [4 /*yield*/, this.registerService.registerOwner(data)];
                    case 2:
                        // console.log('kesini');
                        // this.errorMessage = false;
                        result = _a.sent();
                        data = result;
                        if (!!result.error) return [3 /*break*/, 4];
                        login_data = {
                            "email": this.form.email,
                            "password": this.form.password
                        };
                        return [4 /*yield*/, this.autoLogin(login_data)];
                    case 3:
                        _a.sent();
                        this.showLoading = false;
                        return [3 /*break*/, 5];
                    case 4:
                        this.errorMessage = result.error;
                        this.showLoading = false;
                        _a.label = 5;
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        e_1 = _a.sent();
                        this.errorMessage = (e_1.message);
                        this.showLoading = false;
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    RegisterComponent.prototype.autoLogin = function (login_data) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(login_data);
                        return [4 /*yield*/, this.loginService.memberLogin(login_data)];
                    case 1:
                        result = _a.sent();
                        console.log('here');
                        if (!result.error) {
                            console.log(result);
                            localStorage.setItem('isLoggedin', 'true');
                            localStorage.setItem('tokenlogin', result.result.token);
                            // this.router.navigate(['/merchant-portal/profile'])
                            this.router.navigate(['/register/open-merchant']);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterComponent.prototype.loginClicked = function () {
        this.router.navigate(['/login-merchant']);
    };
    RegisterComponent.prototype.openTnc = function () {
        console.log("test");
        this.showTnc = true;
        console.log(this.showTnc);
    };
    RegisterComponent.prototype.closeTnc = function () {
        this.showTnc = false;
    };
    RegisterComponent.ctorParameters = function () { return [
        { type: _services_register_register_service__WEBPACK_IMPORTED_MODULE_1__["RegisterService"] },
        { type: _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"] },
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_5__["MemberService"] },
        { type: _services_login_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('DOBField', { static: false }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], RegisterComponent.prototype, "DOBField", void 0);
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/index.js!./src/app/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.scss */ "./src/app/register/register.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_register_register_service__WEBPACK_IMPORTED_MODULE_1__["RegisterService"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"],
            _services_member_member_service__WEBPACK_IMPORTED_MODULE_5__["MemberService"],
            _services_login_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/register/register.module.ts":
/*!*********************************************!*\
  !*** ./src/app/register/register.module.ts ***!
  \*********************************************/
/*! exports provided: RegisterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterModule", function() { return RegisterModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _register_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./register-routing.module */ "./src/app/register/register-routing.module.ts");
/* harmony import */ var _register_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _layout_modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../layout/modules/bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ckeditor/ckeditor5-angular */ "./node_modules/@ckeditor/ckeditor5-angular/fesm5/ckeditor-ckeditor5-angular.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _open_merchant_open_merchant_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./open-merchant/open-merchant.component */ "./src/app/register/open-merchant/open-merchant.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var RegisterModule = /** @class */ (function () {
    function RegisterModule() {
    }
    RegisterModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_register_component__WEBPACK_IMPORTED_MODULE_3__["RegisterComponent"], _open_merchant_open_merchant_component__WEBPACK_IMPORTED_MODULE_17__["OpenMerchantComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _register_routing_module__WEBPACK_IMPORTED_MODULE_2__["RegisterRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _layout_modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_7__["BsComponentModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_11__["MatSelectModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_12__["MatCheckboxModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_14__["MatButtonModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_15__["MatFormFieldModule"],
                // MatFormField
                _angular_material_list__WEBPACK_IMPORTED_MODULE_10__["MatListModule"],
                _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_13__["CKEditorModule"],
                // <----- this module will be deprecated in the future version.
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatNativeDateModule"],
            ],
        })
    ], RegisterModule);
    return RegisterModule;
}());



/***/ })

}]);
//# sourceMappingURL=register-register-module.js.map