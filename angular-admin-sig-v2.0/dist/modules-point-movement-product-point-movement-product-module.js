(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-point-movement-product-point-movement-product-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/point-movement-product/point-movement-product.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/point-movement-product/point-movement-product.component.html ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n    <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n    <div *ngIf=\"PointMovementProduct && mci_project == false && programType !='custom_kontraktual' && !errorMessage\">\r\n          <app-form-builder-table\r\n          [searchCallback]= \"[service, 'searchPointMovementBasedOnProduct',this]\"\r\n          [tableFormat]=\"tableFormat\"\r\n          [table_data]=\"PointMovementProduct\"\r\n          [total_page]=\"totalPage\"\r\n        >\r\n  \r\n          </app-form-builder-table>\r\n    </div>\r\n\r\n    <div *ngIf=\"PointMovementProduct && mci_project == true && programType !='custom_kontraktual' && !errorMessage\">\r\n      <app-form-builder-table\r\n          [searchCallback]= \"[service, 'searchPointMovementBasedOnProduct',this]\"\r\n          [tableFormat]=\"tableFormat2\"\r\n          [table_data]=\"PointMovementProduct\"\r\n          [total_page]=\"totalPage\"\r\n        >\r\n\r\n          </app-form-builder-table>\r\n    </div>\r\n\r\n    <div *ngIf=\"PointMovementProduct && mci_project == false && programType =='custom_kontraktual' && !errorMessage\">\r\n      <app-form-builder-table\r\n          [searchCallback]= \"[service, 'searchPointMovementBasedOnProduct',this]\"\r\n          [tableFormat]=\"tableFormat3\"\r\n          [table_data]=\"PointMovementProduct\"\r\n          [total_page]=\"totalPage\"\r\n        >\r\n\r\n          </app-form-builder-table>\r\n    </div>\r\n\r\n      <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n        <div class=\"text-message\">\r\n        <i class=\"fas fa-ban\"></i>\r\n        {{errorMessage}}\r\n        </div>\r\n      </div>\r\n        <!-- <div *ngIf=\"SalesRedemptionDetail\"> -->\r\n              <!-- <app-bast [back]=\"[this,backToHere]\" [detail]=\"SalesRedemptionDetail\"></app-bast> -->\r\n        <!-- </div> -->\r\n  \r\n    </div>\r\n  "

/***/ }),

/***/ "./src/app/layout/modules/point-movement-product/point-movement-product-routing.module.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/layout/modules/point-movement-product/point-movement-product-routing.module.ts ***!
  \************************************************************************************************/
/*! exports provided: PointMovementProductRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointMovementProductRoutingModule", function() { return PointMovementProductRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _point_movement_product_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./point-movement-product.component */ "./src/app/layout/modules/point-movement-product/point-movement-product.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { BastComponent} from './bast/bast.component'
var routes = [
    {
        path: '', component: _point_movement_product_component__WEBPACK_IMPORTED_MODULE_2__["PointMovementProductComponent"]
    },
];
var PointMovementProductRoutingModule = /** @class */ (function () {
    function PointMovementProductRoutingModule() {
    }
    PointMovementProductRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], PointMovementProductRoutingModule);
    return PointMovementProductRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/point-movement-product/point-movement-product.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/point-movement-product/point-movement-product.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcG9pbnQtbW92ZW1lbnQtcHJvZHVjdC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXHBvaW50LW1vdmVtZW50LXByb2R1Y3RcXHBvaW50LW1vdmVtZW50LXByb2R1Y3QuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3BvaW50LW1vdmVtZW50LXByb2R1Y3QvcG9pbnQtbW92ZW1lbnQtcHJvZHVjdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDQ0o7QURDSTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUNDUiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3BvaW50LW1vdmVtZW50LXByb2R1Y3QvcG9pbnQtbW92ZW1lbnQtcHJvZHVjdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn0iLCIuZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/point-movement-product/point-movement-product.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/modules/point-movement-product/point-movement-product.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: PointMovementProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointMovementProductComponent", function() { return PointMovementProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/points-transaction/points-transaction.service */ "./src/app/services/points-transaction/points-transaction.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var PointMovementProductComponent = /** @class */ (function () {
    function PointMovementProductComponent(PointsTransactionService) {
        this.PointsTransactionService = PointsTransactionService;
        this.PointMovementProduct = [];
        this.tableFormat = {
            title: 'Order Report (On Delivery and Delivered Shipping)',
            label_headers: [
                { label: 'Order Date', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'On Process Date', visible: true, type: 'date', data_row_name: 'approve_date' },
                // {label: 'On Process Date', visible: true, type: 'on_process_date', data_row_name: 'shipping_info'},
                { label: 'Shipping Status', visible: true, type: 'string', data_row_name: 'last_shipping_info' },
                { label: 'On Delivery Date', visible: true, type: 'on_delivery_date', data_row_name: 'shipping_info' },
                // {label: 'Input AWB Date', visible: true, type: 'input_awb_date', data_row_name: 'shipping_info'},
                { label: 'ID Toko', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Toko', visible: true, type: 'string', data_row_name: 'nama_toko' },
                { label: 'Alamat Toko', visible: true, type: 'string', data_row_name: 'alamat_toko' },
                { label: 'Nama Pemilik', visible: true, type: 'string', data_row_name: 'nama_pemilik' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Product Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'SKU', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Product Price', visible: true, type: 'product_price_redeem', data_row_name: 'products' },
                // {label: 'Previous Point Balance', visible: true, type: 'string', data_row_name: 'prev_points_balance'},
                // {label: 'Mutasi Point', visible: true, type: 'string', data_row_name: 'points'},
                // {label: 'Current Points Balance', visible: true, type: 'string', data_row_name: 'current_points_balance'},
                // {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                { label: 'Group', visible: true, type: 'string', data_row_name: 'group' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'PointMovementProduct',
                detail_function: [this, 'callDetail'],
                pointMovementProduct: true,
            },
            show_checkbox_options: true
        };
        this.tableFormat2 = {
            title: 'Order Report (On Delivery and Delivered Shipping)',
            label_headers: [
                { label: 'Order Date', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'On Process Date', visible: true, type: 'date', data_row_name: 'approve_date' },
                // {label: 'On Process Date', visible: true, type: 'on_process_date', data_row_name: 'shipping_info'},
                { label: 'Shipping Status', visible: true, type: 'string', data_row_name: 'last_shipping_info' },
                { label: 'On Delivery Date', visible: true, type: 'on_delivery_date', data_row_name: 'shipping_info' },
                // {label: 'Input AWB Date', visible: true, type: 'input_awb_date', data_row_name: 'shipping_info'},
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Pelanggan', visible: true, type: 'string', data_row_name: 'nama_pemilik' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Product Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'SKU', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Product Price', visible: true, type: 'product_price_redeem', data_row_name: 'products' },
                // {label: 'Previous Point Balance', visible: true, type: 'string', data_row_name: 'prev_points_balance'},
                // {label: 'Mutasi Point', visible: true, type: 'string', data_row_name: 'points'},
                // {label: 'Current Points Balance', visible: true, type: 'string', data_row_name: 'current_points_balance'},
                // {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                { label: 'Group', visible: true, type: 'string', data_row_name: 'group' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'PointMovementProduct',
                detail_function: [this, 'callDetail'],
                pointMovementProduct: true,
            },
            show_checkbox_options: true
        };
        this.tableFormat3 = {
            title: 'Order Report (On Delivery and Delivered Shipping)',
            label_headers: [
                { label: 'Order Date', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'On Process Date', visible: true, type: 'date', data_row_name: 'approve_date' },
                // {label: 'On Process Date', visible: true, type: 'on_process_date', data_row_name: 'shipping_info'},
                { label: 'Shipping Status', visible: true, type: 'string', data_row_name: 'last_shipping_info' },
                { label: 'On Delivery Date', visible: true, type: 'on_delivery_date', data_row_name: 'shipping_info' },
                // {label: 'Input AWB Date', visible: true, type: 'input_awb_date', data_row_name: 'shipping_info'},
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
                { label: 'Nama Entitas', visible: true, type: 'string', data_row_name: 'nama_toko' },
                { label: 'Alamat', visible: true, type: 'string', data_row_name: 'alamat_toko' },
                { label: 'Nama PIC', visible: true, type: 'string', data_row_name: 'nama_pemilik' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Product Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'SKU', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Product Price', visible: true, type: 'product_price_redeem', data_row_name: 'products' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'PointMovementProduct',
                detail_function: [this, 'callDetail'],
                pointMovementProduct: true,
            },
            show_checkbox_options: true
        };
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        this.SalesRedemptionDetail = false;
        this.mci_project = false;
        this.programType = "";
        this.errorMessage = false;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2__;
    }
    PointMovementProductComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    PointMovementProductComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program_1, _this_1, params, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        program_1 = localStorage.getItem('programName');
                        _this_1 = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program_1) {
                                if (element.type == "reguler") {
                                    _this_1.mci_project = true;
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this_1.programType = "custom_kontraktual";
                                }
                                else {
                                    _this_1.mci_project = false;
                                }
                            }
                        });
                        params = {
                            search: { based_on: "order_id" },
                            current_page: 1,
                            limit_per_page: 50
                        };
                        this.service = this.PointsTransactionService;
                        return [4 /*yield*/, this.PointsTransactionService.getPointMovementBasedOnProduct()];
                    case 1:
                        result = _a.sent();
                        // console.warn("RESULT", result)
                        this.totalPage = result.total_page;
                        this.PointMovementProduct = result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PointMovementProductComponent.prototype.callDetail = function (SalesRedemption_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    // let result: any;
                    // this.service    = this.PointsTransactionService;
                    // result          = await this.PointsTransactionService.detailPointstransaction(SalesRedemption_id);
                    // console.log(result);
                    // this.SalesRedemptionDetail = result.result[0];
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    PointMovementProductComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.SalesRedemptionDetail = false;
                return [2 /*return*/];
            });
        });
    };
    PointMovementProductComponent.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    PointMovementProductComponent.ctorParameters = function () { return [
        { type: _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_1__["PointsTransactionService"] }
    ]; };
    PointMovementProductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-point-movement-product',
            template: __webpack_require__(/*! raw-loader!./point-movement-product.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/point-movement-product/point-movement-product.component.html"),
            styles: [__webpack_require__(/*! ./point-movement-product.component.scss */ "./src/app/layout/modules/point-movement-product/point-movement-product.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_1__["PointsTransactionService"]])
    ], PointMovementProductComponent);
    return PointMovementProductComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/point-movement-product/point-movement-product.module.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/layout/modules/point-movement-product/point-movement-product.module.ts ***!
  \****************************************************************************************/
/*! exports provided: PointMovementProductModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointMovementProductModule", function() { return PointMovementProductModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _point_movement_product_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./point-movement-product-routing.module */ "./src/app/layout/modules/point-movement-product/point-movement-product-routing.module.ts");
/* harmony import */ var _point_movement_product_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./point-movement-product.component */ "./src/app/layout/modules/point-movement-product/point-movement-product.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// import { BastComponent } from './bast/bast.component';

var PointMovementProductModule = /** @class */ (function () {
    function PointMovementProductModule() {
    }
    PointMovementProductModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _point_movement_product_routing_module__WEBPACK_IMPORTED_MODULE_2__["PointMovementProductRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"]
            ],
            declarations: [_point_movement_product_component__WEBPACK_IMPORTED_MODULE_3__["PointMovementProductComponent"]],
        })
    ], PointMovementProductModule);
    return PointMovementProductModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-point-movement-product-point-movement-product-module.js.map